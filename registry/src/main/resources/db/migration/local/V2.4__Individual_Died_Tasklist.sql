INSERT INTO task_list (name, web_icon, app_icon, description, extra_data) VALUES ('individual_died', 'fa fa-bed', '{md-bed}', 'tasklist_desc_individual_died', '{"name":"Individual Died","showSummary":false,"defaultSortField":"Id","defaultSortOrder":false,"hideAddPatients":true,"extraFields":[],"disabledFields":[],"showCallButton":false,"allowToggleSwitch":false,"showBlankSummary":false,"removePriorityColor":true,"lastDosageTwoDays":false,"lastDosageThreePlusDays":false,"lastDosageOneDay":false}');

INSERT INTO filters (name) VALUES ('TreatmentOutcome');

INSERT INTO filter_values (filter_id, value) VALUES ((SELECT id FROM filters WHERE name = 'TreatmentOutcome'), 'EXTRA_OUTCOME');


