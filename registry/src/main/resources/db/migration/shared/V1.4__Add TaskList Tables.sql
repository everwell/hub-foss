create table deployment_task_list (
    id  bigserial not null,
    deployment_id bigint,
    name text,
    web_icon text,
    app_icon text,
    description text,
    display_columns text,
    extra_data text,
    display_order bigint,
    primary key (id)
);

create table task_list_filter_map (
    id  bigserial not null,
    deployment_task_list_id bigint,
    filter_name text,
    value text,
    primary key (id)
);

create table task_list_column (
    id  bigserial not null,
    name text,
    label text,
    sortable boolean,
    primary key (id)
);