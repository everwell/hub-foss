create table if not exists deployment_disease_map
(
    id            bigserial
        constraint table_name_pk
            primary key,
    deployment_id bigint not null
        constraint table_name_deployment_id_fk
            references deployment,
    disease_id    bigint not null,
    created_at    timestamp,
    stopped_at    timestamp
);