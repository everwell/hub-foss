package com.everwell.registryservice.models.db;

import lombok.*;

import javax.persistence.*;

@Entity
@Table(name = "engagement")
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Engagement {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column(name = "hierarchy_id")
    private Long hierarchyId;
    @Column(name = "default_lang")
    private Long defaultLang;
    @Column(name = "default_time")
    private String defaultTime;
    private String languages;
    @Column(name = "dose_timings")
    private String doseTimings;
    @Column(name = "consent_mandatory")
    private Boolean consentMandatory;
    @Column(name = "notification_type")
    private String notificationType;
    @Column(name = "disabled_languages")
    private String disabledLanguages;

    public Engagement(Long hierarchyId, Long defaultLang, String defaultTime, String languages, String doseTimings, Boolean consentMandatory, String notificationType, String disabledLanguages) {
        this.hierarchyId = hierarchyId;
        this.defaultLang = defaultLang;
        this.defaultTime = defaultTime;
        this.languages = languages;
        this.doseTimings = doseTimings;
        this.consentMandatory = consentMandatory;
        this.notificationType = notificationType;
        this.disabledLanguages = disabledLanguages;
    }
}
