package com.everwell.registryservice.models.http.requests;

import com.everwell.registryservice.constants.FieldValidationMessages;
import com.everwell.registryservice.models.dto.HierarchyRequestData;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class AddHierarchyRequest extends HierarchyRequest {
    @Valid
    @NotNull(message = FieldValidationMessages.HIERARCHY_DATA_MANDATORY)
    private HierarchyRequestData hierarchy;
}
