package com.everwell.registryservice.models.dto;

import com.everwell.registryservice.constants.EventFlowConstants;
import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public class GenericEvents<T> {
    private T data;
    private EventFlowConstants event;

    public GenericEvents(EventFlowConstants event) {
        this.event = event;
    }
}
