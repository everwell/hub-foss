package com.everwell.registryservice.models.http.response;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class HierarchySummaryResponseData {
    private Long id;
    private Long level;
    private String name;
    private String type;
    private String code;
    private Long parentId;
    private Long cases;
    private Long children;
    private Long siblings;
    private boolean hasMERM;
    private boolean hasVOT;
    private boolean hasNone;
    private boolean has99D;
    private boolean has99DL;
}
