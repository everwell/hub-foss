package com.everwell.registryservice.models.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.lang.Nullable;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class GenericExchange {
    private String clientId;
    private String url;
    private HttpMethod method;
    private ParameterizedTypeReference typeReference;
    @Nullable
    private Object body;

    public GenericExchange(String clientId, String url, HttpMethod method, ParameterizedTypeReference typeReference) {
        this.clientId = clientId;
        this.url = url;
        this.method = method;
        this.typeReference = typeReference;
    }
}
