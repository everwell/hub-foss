package com.everwell.registryservice.models.dto;

import com.everwell.registryservice.models.http.response.HierarchySummaryResponseData;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class UserHierarchySummaryResponse {

    HierarchySummaryResponseData basicDetails;

    List<UserSummaryDto> loginDetails;
}
