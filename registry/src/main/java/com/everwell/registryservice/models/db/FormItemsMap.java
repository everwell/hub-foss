package com.everwell.registryservice.models.db;

import lombok.*;

import javax.persistence.*;

@Entity
@Table(name = "form_items_map")
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class FormItemsMap {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column(name = "deployment_id")
    private Long deploymentId;
    @Column(name = "form_name")
    private String formName;
    @Column(name = "item_type")
    private String itemType;
    @Column(name = "item_id")
    private Long itemId;
    @Column(name = "parent_type")
    private String parentType;
    @Column(name = "parent_id")
    private Long parentId;
}
