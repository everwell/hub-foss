package com.everwell.registryservice.models.db;

import lombok.*;

import javax.persistence.*;

@Entity
@Table(name = "form")
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class Form {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column(name = "form_label")
    private String formLabel;
    @Column(name = "form_name")
    private String formName;
    @Column(name = "save_endpoint")
    private String saveEndpoint;
    @Column(name = "save_text")
    private String saveText;
}
