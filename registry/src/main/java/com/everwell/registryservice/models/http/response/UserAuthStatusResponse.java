package com.everwell.registryservice.models.http.response;


import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
@Getter
@JsonInclude(JsonInclude.Include.NON_NULL)
public class UserAuthStatusResponse {
    @Setter
    boolean userAuthenticated;
    @Setter
    String userName;
}
