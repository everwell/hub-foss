package com.everwell.registryservice.models.dto;

import com.everwell.registryservice.models.http.response.adherence.AdherenceTechnologyDto;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;
import java.util.Map;

@Data
@NoArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class UnifiedListFilterResponse {

    private List<Map<String, String>> stageOptions;

    private List<List<MinHierarchyDetails>> hierarchyOptions;

    private List<AdherenceTechnologyDto> technologyOptions;

    private String earliestEnrollment;
}
