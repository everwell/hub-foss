package com.everwell.registryservice.models.http.requests;

import com.everwell.registryservice.exceptions.ValidationException;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.springframework.util.StringUtils;

import java.util.ArrayList;
import java.util.List;

@AllArgsConstructor
@Getter
@NoArgsConstructor
public class CreateDeploymentRequest
{
    private String code;
    private String countryName;
    private Integer phonePrefix;
    private String timeZone;
    private String timeZoneAbbreviation;
    private String timeZoneDetails;
    private String shortCode;
    private String projectName;
    private String askForHelpUrl;
    private String defaultTech;
    private Long defaultLanguageId;
    private String defaultTime;
    private List<Long> allowedLanguages = new ArrayList<>();
    private List<Long> diseaseMappings = new ArrayList<>();

    public void validate()
    {
        if(!StringUtils.hasText(this.code))
        {
            throw new ValidationException("Code cannot be empty");
        }

        if(!StringUtils.hasText(this.countryName))
        {
            throw new ValidationException("Country name cannot be empty");
        }

        if(!StringUtils.hasText(this.timeZone))
        {
            throw new ValidationException("Time zone cannot be empty");
        }

        if(null == this.defaultLanguageId)
        {
            throw new ValidationException("Default Language cannot be empty");
        }

        if((StringUtils.hasText(this.askForHelpUrl)) && !StringUtils.hasText(this.projectName))
        {
            throw new ValidationException("Invalid request");
        }
    }
}
