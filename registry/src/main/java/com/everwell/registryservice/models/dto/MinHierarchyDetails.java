package com.everwell.registryservice.models.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class MinHierarchyDetails {
    private Long id;
    private String name;
    private String code;
    private String type;
    private Long level;
    private Long parentId;
}
