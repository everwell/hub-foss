package com.everwell.registryservice.models.dto;

import lombok.AllArgsConstructor;
import lombok.Data;

@AllArgsConstructor
@Data
public class  GenericEvent<T> {
    private T field;
    private String eventName;
}
