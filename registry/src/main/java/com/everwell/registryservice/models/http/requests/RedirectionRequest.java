package com.everwell.registryservice.models.http.requests;

import com.everwell.registryservice.constants.FieldValidationMessages;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotBlank;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class RedirectionRequest {

    @Setter
    @Getter
    @NotBlank(message = FieldValidationMessages.INVALID_REQUEST_BODY)
    String returnUrl;
}
