package com.everwell.registryservice.models.http.response;

import com.everwell.registryservice.models.db.FilterValue;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class TaskListFilterResponseEntry {
    private Long id;
    private String name;
    private List<FilterValue> values;
}
