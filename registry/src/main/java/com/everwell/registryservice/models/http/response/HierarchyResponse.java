package com.everwell.registryservice.models.http.response;

import com.everwell.registryservice.models.dto.HierarchyResponseData;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Map;

@Data
@NoArgsConstructor
@AllArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class HierarchyResponse {
    private HierarchyResponseData hierarchy;
    private Map<String, String> associations;

    public HierarchyResponse(HierarchyResponseData hierarchy) {
        this.hierarchy = hierarchy;
    }
}
