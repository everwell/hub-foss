package com.everwell.registryservice.models.http.requests;

import lombok.*;

import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class UpdateTriggerDetailsRequest{

    @Valid
    @NotNull(message = "Id must be provided")
    private Long id;

    @Valid
    @NotNull(message = "Trigger id must be provided")
    private Long triggerId;

    @Valid
    @NotNull(message = "Default Template id must be provided")
    private Long defaultTemplateId;

    private String templateIds;

    @Valid
    @NotBlank(message = "Cron time must be provided")
    private String cronTime;

    @Valid
    @NotBlank(message = "Event Name must be provided")
    private String eventName;

    @Valid
    @NotBlank(message = "Function name must be provided")
    private String functionName;

    @Valid
    @NotNull(message = "Active state must be provided")
    private Boolean isActive;

    @Valid
    @NotNull(message = "Mandatory must be provided")
    private Boolean mandatory;

    @Valid
    @NotNull(message = "Entity time related must be provided")
    private Boolean entityTimeRelated;

    @Valid
    @NotBlank(message = "Notification type must be provided")
    private String notificationType;

}
