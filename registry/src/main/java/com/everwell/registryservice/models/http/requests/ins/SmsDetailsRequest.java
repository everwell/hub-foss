package com.everwell.registryservice.models.http.requests.ins;

import com.everwell.registryservice.models.dto.UserSmsDetails;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.*;

import java.io.Serializable;
import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class SmsDetailsRequest extends TriggerDefaultRequest implements Serializable {
    List<UserSmsDetails> personList;

    public SmsDetailsRequest(List<UserSmsDetails> personList, Long vendorId, Long triggerId, Long templateId, List<Long> templateIds, Boolean isMandatory, Boolean defaultConsent, Boolean isDefaultTime, String timeOfSms) {
        super(vendorId, triggerId, templateId, templateIds, isMandatory, defaultConsent, isDefaultTime, timeOfSms);
        this.personList = personList;
    }
}
