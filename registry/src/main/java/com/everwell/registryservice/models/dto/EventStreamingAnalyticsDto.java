package com.everwell.registryservice.models.dto;

import com.everwell.registryservice.utils.Utils;
import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class EventStreamingAnalyticsDto {
    private String eventName;
    private String eventAction;
    private String eventCategory;
    private Long eventTime;

    public EventStreamingAnalyticsDto(String eventCategory,String eventName, String eventAction) {
        this.eventName = eventName;
        this.eventAction = eventAction;
        this.eventCategory = eventCategory;
        this.eventTime = Utils.getCurrentTime();
    }
}
