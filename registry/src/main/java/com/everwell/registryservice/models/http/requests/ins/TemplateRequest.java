package com.everwell.registryservice.models.http.requests.ins;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.*;

import javax.validation.Valid;
import javax.validation.constraints.NotBlank;

@AllArgsConstructor
@NoArgsConstructor
@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class TemplateRequest {
    @Valid
    @NotBlank(message = "Content must be provided")
    String content;
    @Valid
    @NotBlank(message = "Language Id must be provided")
    Long languageId;
    @Valid
    @NotBlank(message = "Type Id must be provided")
    Long typeId;
    String parameters;
    Boolean unicode;
}
