package com.everwell.registryservice.models.http.requests;

import com.everwell.registryservice.models.dto.AddTriggerDetails;
import lombok.*;

import javax.validation.Valid;
import javax.validation.constraints.NotEmpty;
import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class AddTriggerDetailsRequest {

    @Valid
    @NotEmpty(message = "Trigger Details must be provided")
    List<AddTriggerDetails> triggerDetails;
}
