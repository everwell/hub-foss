package com.everwell.registryservice.models.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Setter;
import lombok.Getter;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class SidebarItemResponse {

    private Long id;

    private String name;

    private String icon;

    private String link;

    @JsonProperty(value = "relative_path")
    private Boolean relativePath;
}
