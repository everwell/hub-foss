package com.everwell.registryservice.models.http.response;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@NoArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class PrefixResponse {
    private String Key;
    private String Value;

    public PrefixResponse(String key, String value) {
        this.Key = key;
        this.Value = value;
    }
}
