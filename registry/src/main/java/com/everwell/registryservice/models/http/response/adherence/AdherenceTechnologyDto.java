package com.everwell.registryservice.models.http.response.adherence;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@Data
@AllArgsConstructor
public class AdherenceTechnologyDto {
    private String key;
    private String value;
    private String adherenceTechOptions;
}
