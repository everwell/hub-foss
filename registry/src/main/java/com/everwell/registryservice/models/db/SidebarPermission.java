package com.everwell.registryservice.models.db;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "sidebar_permission")
public class SidebarPermission {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Column(name = "deployment_id")
    private Long deploymentId;

    private String designation;

    @Getter
    @Column(name = "sidebar_id")
    private Long sidebarId;

    @Column(name = "is_active")
    private boolean isActive;
}
