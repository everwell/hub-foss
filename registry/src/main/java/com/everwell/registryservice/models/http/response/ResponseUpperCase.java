package com.everwell.registryservice.models.http.response;


import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.io.Serializable;

@JsonInclude(JsonInclude.Include.NON_NULL)
@ToString
public class ResponseUpperCase<T> implements Serializable {

    @Getter
    @JsonProperty(value = "Success")
    private boolean success;

    @Getter
    @JsonProperty(value = "Message")
    private String message;

    @Getter
    @JsonProperty(value = "Data")
    private T data;

    public ResponseUpperCase(boolean success, T data, String message) {
        this.success = success;
        this.message = message;
        this.data = data;
    }
}
