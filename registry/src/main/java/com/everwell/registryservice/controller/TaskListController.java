package com.everwell.registryservice.controller;

import com.everwell.registryservice.exceptions.NotFoundException;
import com.everwell.registryservice.models.db.*;
import com.everwell.registryservice.models.dto.FilterDetails;
import com.everwell.registryservice.models.dto.UnifiedListFilterResponse;
import com.everwell.registryservice.models.http.requests.TaskListItemRequest;
import com.everwell.registryservice.models.http.response.Response;
import com.everwell.registryservice.models.http.response.TaskListFilterResponseEntry;
import com.everwell.registryservice.models.http.response.TaskListItemResponse;
import com.everwell.registryservice.models.http.response.adherence.AdherenceTechnologyDto;
import com.everwell.registryservice.service.DeploymentService;
import com.everwell.registryservice.service.TaskListService;
import com.everwell.registryservice.service.UserService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.util.Pair;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.*;
import java.util.stream.Collectors;

@RestController
public class TaskListController {
    private static Logger LOGGER = LoggerFactory.getLogger(TaskListController.class);

    @Autowired
    private DeploymentService deploymentService;

    @Autowired
    private TaskListService taskListService;

    @RequestMapping(value = "/v1/taskList", method = RequestMethod.POST)
    public ResponseEntity<Response<Long>> addTasklist(@RequestBody TaskListItemRequest taskListItemRequest) {
        taskListItemRequest.validate(taskListItemRequest);

        String deploymentCode = taskListItemRequest.getTaskListItem().getDeploymentCode();
        LOGGER.info("[addTasklist] add task list request for deployment " + deploymentCode);

        Deployment deployment = deploymentService.getByCode(deploymentCode);
        HttpStatus status = HttpStatus.OK;
        boolean success = true;
        Long taskListId = 0L;
        String message = null;

        if (!taskListService.validateTaskList(taskListItemRequest, deployment.getId())) {
            status = HttpStatus.BAD_REQUEST;
            success = false;
            message = "Unable to add task list as required parameters missing";
            LOGGER.info("[addTasklist] task list for deployment " + deploymentCode + " is invalid ");
        } else if (taskListService.taskListAlreadyMappedToDeployment(deployment.getId(), taskListItemRequest.getTaskListItem().getTaskListId())) {
            status = HttpStatus.CONFLICT;
            success = false;
            message = "Task List already exists";
            LOGGER.info("[addTasklist] task list already exists ");
        } else {
            taskListId = taskListService.addTaskList(taskListItemRequest, deployment.getId());
            message = "Added task list with id " + taskListId;
            LOGGER.info("[addTasklist] task list added for deployment " + deploymentCode);
        }

        Response<Long> response = new Response<>(success, taskListId, message);
        return new ResponseEntity<>(response, status);
    }

    @RequestMapping(value = "/v1/taskList/id/{id}", method = RequestMethod.GET)
    public ResponseEntity<Response<TaskListItemResponse>> getTaskList(@PathVariable Long id) {
        LOGGER.info("[getTaskList] request received: taskListItemId " + id);
        DeploymentTaskList taskList = taskListService.getTaskList(id);
        List<TaskListColumn> displayColumns = new ArrayList<>();
        if (null != taskList.getDisplayColumns()) {
            List<Long> displayColumnIds = Arrays.stream(taskList.getDisplayColumns().split(",")).map(Long::parseLong).collect(Collectors.toList());
            List<TaskListColumn> displayColumnsInitial = taskListService.getDisplayColumns(displayColumnIds);
            Map<Long, TaskListColumn> columnMap = displayColumnsInitial.stream().collect(Collectors.toMap(TaskListColumn::getId, column -> column));
            for (Long columnId : displayColumnIds) {
                displayColumns.add(columnMap.get(columnId));
            }
        }

        List<TaskListFilterMap> filterMaps = taskListService.getTaskListFiltersByTaskListIds(Collections.singletonList(id));

        TaskList taskListItem = taskListService.getTaskListItem(taskList.getTaskListId());
        if (null == taskListItem) {
            throw new NotFoundException("Tasklist item not found");
        }

        TaskListItemResponse taskListItemResponse = new TaskListItemResponse(taskList, taskListItem);
        taskListItemResponse.setFilters(taskListService.getFilterDetails(filterMaps.stream().map(TaskListFilterMap::getFilterValueId).collect(Collectors.toList())));
        taskListItemResponse.setDisplayColumns(displayColumns);

        LOGGER.info("[getTaskList] response generated: " + taskListItemResponse);
        return new ResponseEntity<>(new Response<>(true, taskListItemResponse), HttpStatus.OK);
    }

    @RequestMapping(value = "/v1/taskList/id/{id}", method = RequestMethod.PUT)
    public ResponseEntity<Response<String>> updateTaskList(@PathVariable Long id, @RequestBody TaskListItemRequest taskListItemRequest) {
        LOGGER.info("[updateTaskList] update task list request for id " + id);

        taskListService.updateTaskList(taskListItemRequest, id);

        LOGGER.info("[updateTaskList] task list updated for id " + id);
        return new ResponseEntity<>(new Response<>(null, "Tasklist updated successfully"), HttpStatus.OK);
    }

    @RequestMapping(value = "/v1/taskList/deployment/{code}", method = RequestMethod.GET)
    public ResponseEntity<Response<List<TaskListItemResponse>>> getTaskListsByDeploymentCode(@PathVariable String code) {
        LOGGER.info("[getTaskListsByDeploymentCode] request received: code " + code);

        Deployment deployment = deploymentService.getByCode(code);
        List<TaskListItemResponse> formattedResponse = taskListService.getTaskListFilterResponseByDeploymentId(deployment.getId(), true);

        LOGGER.info("[getTaskListsByDeploymentCode] response generated: " + formattedResponse);
        return new ResponseEntity<>(new Response<>(true, formattedResponse), HttpStatus.OK);
    }

    @RequestMapping(value = "/v1/taskList/id/{id}", method = RequestMethod.DELETE)
    public ResponseEntity<Response<String>> deleteTaskList(@PathVariable Long id) {
        LOGGER.info("[deleteTaskList] request received: taskListItemId " + id);
        taskListService.deleteTaskList(id);
        LOGGER.info("[deleteTaskList] task list deleted for id " + id);
        return new ResponseEntity<>(new Response<>(true, null), HttpStatus.OK);
    }

    @RequestMapping(value = "/v1/taskList", method = RequestMethod.GET)
    public ResponseEntity<Response<List<TaskList>>> getAllTaskList() {
        LOGGER.info("[getAllTaskList] request received");

        List<TaskList> taskLists = taskListService.getAllTaskLists();

        LOGGER.info("[getAllTaskList] response generated: " + taskLists);
        return new ResponseEntity<>(new Response<>(true, taskLists), HttpStatus.OK);
    }

    @RequestMapping(value = "/v1/taskList/filters", method = RequestMethod.GET)
    public ResponseEntity<Response<List<TaskListFilterResponseEntry>>> getAllFilters() {
        LOGGER.info("[getAllFilters] request received");

        List<Filter> filters = taskListService.getAllFilters();

        List<FilterValue> filterValues = taskListService.getAllFilterValues();
        Map<Long, List<FilterValue>> filterValueMap = new HashMap<>();
        for (FilterValue filterValue : filterValues) {
            if (!filterValueMap.containsKey(filterValue.getFilterId())) {
                filterValueMap.put(filterValue.getFilterId(), new ArrayList<>());
            }

            filterValueMap.get(filterValue.getFilterId()).add(filterValue);
        }

        List<TaskListFilterResponseEntry> response = filters
                .stream()
                .map(filter -> new TaskListFilterResponseEntry(
                        filter.getId(),
                        filter.getName(),
                        filterValueMap.get(filter.getId()))
                )
                .collect(Collectors.toList());


        LOGGER.info("[getAllFilters] response generated: " + response);
        return new ResponseEntity<>(new Response<>(true, response), HttpStatus.OK);
    }

    @RequestMapping(value = "/v1/tasklist/columns", method = RequestMethod.GET)
    public ResponseEntity<Response<List<TaskListColumn>>> GetAllDisplayColumnDetails() {
        LOGGER.info("[GetAllDisplayColumnDetails] request received: taskListItemId ");
        List<TaskListColumn> columnDetailsResponse = taskListService.getAllDisplayColumns();
        LOGGER.info("[GetAllDisplayColumnDetails] response generated: " + columnDetailsResponse);
        return new ResponseEntity<>(new Response<>(true, columnDetailsResponse), HttpStatus.OK);

    }
}
