package com.everwell.registryservice.controller;

import com.everwell.registryservice.models.http.requests.ins.TriggerDetailsRequest;
import com.everwell.registryservice.models.http.requests.UpdateTriggerDetailsRequest;
import com.everwell.registryservice.models.http.response.DeploymentResponse;
import com.everwell.registryservice.models.http.requests.AddTriggerDetailsRequest;
import com.everwell.registryservice.models.http.response.AddTriggerResponse;
import com.everwell.registryservice.models.http.response.Response;
import com.everwell.registryservice.models.http.response.TriggersAndTemplatesResponse;
import com.everwell.registryservice.service.TriggerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
public class TriggerController {

    @Autowired
    TriggerService triggerService;

    @RequestMapping(value = "/v1/trigger", method = RequestMethod.GET)
    public ResponseEntity<Response<List<TriggersAndTemplatesResponse>>> getAll()
    {
        return ResponseEntity.ok(new Response<>(true, triggerService.getAllTriggers()));
    }
    @RequestMapping(value = "/v1/triggers", method = RequestMethod.GET)
    public ResponseEntity<Response<List<TriggerDetailsRequest>>> getTriggersByFunctionName(@RequestParam Long hierarchyId, @RequestParam String functionName)
    {
        return ResponseEntity.ok(new Response<>(true, triggerService.getActiveTriggersByFunctionName(hierarchyId, functionName)));
    }

    @RequestMapping(value = "/v1/trigger", method = RequestMethod.PUT)
    public ResponseEntity<Response<String>> updateTrigger(@Valid @RequestBody UpdateTriggerDetailsRequest updateTriggerDetailsRequest){
        triggerService.updateTrigger(updateTriggerDetailsRequest);
        return new ResponseEntity<>(new Response<>(true,null), HttpStatus.OK);
    }

    @RequestMapping(value = "/v1/trigger", method = RequestMethod.POST)
    public ResponseEntity<Response<AddTriggerResponse>> addTrigger(@Valid @RequestBody AddTriggerDetailsRequest addTriggerDetailsRequest){
        AddTriggerResponse triggerResponse = triggerService.addTrigger(addTriggerDetailsRequest);
        return new ResponseEntity<>(new Response<>(true, triggerResponse),HttpStatus.CREATED);
    }
}
