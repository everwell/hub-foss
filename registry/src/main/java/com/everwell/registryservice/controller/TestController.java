package com.everwell.registryservice.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class TestController {

    @RequestMapping(value = "/v1/test", method = RequestMethod.GET)
    public String testController() {
        return "Test controller working!";
    }

}
