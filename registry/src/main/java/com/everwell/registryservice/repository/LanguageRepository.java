package com.everwell.registryservice.repository;

import com.everwell.registryservice.models.db.Language;
import org.springframework.data.jpa.repository.JpaRepository;

public interface LanguageRepository extends JpaRepository<Language, Long>
{
}
