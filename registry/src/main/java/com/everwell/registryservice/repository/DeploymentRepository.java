package com.everwell.registryservice.repository;

import com.everwell.registryservice.models.db.Deployment;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface DeploymentRepository extends JpaRepository<Deployment, Long>
{
    Deployment getDeploymentByCode(String code);

    Deployment getDeploymentById(Long id);

    Boolean existsAllByCode(String code);

    Boolean existsAllByCountryNameAndProjectName(String countryName, String projectName);

    List<Deployment> getAllByCountryNameNotNullAndProjectNameNotNull();
}
