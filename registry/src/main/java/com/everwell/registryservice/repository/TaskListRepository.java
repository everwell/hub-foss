package com.everwell.registryservice.repository;

import com.everwell.registryservice.models.db.TaskList;
import org.springframework.data.jpa.repository.JpaRepository;

public interface TaskListRepository extends JpaRepository<TaskList, Long> {
}
