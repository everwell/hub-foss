package com.everwell.registryservice.repository;

import com.everwell.registryservice.models.db.Config;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

public interface ConfigRepository extends JpaRepository<Config, Long> {

    Optional<Config> findByName(String name);

    @Transactional
    Integer deleteByName(String name);

    Integer countByNameIn(List<String> names);

    List<Config> findByNameIn(List<String> names);

}
