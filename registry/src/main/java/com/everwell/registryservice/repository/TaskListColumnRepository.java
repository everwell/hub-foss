package com.everwell.registryservice.repository;

import com.everwell.registryservice.models.db.TaskListColumn;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface TaskListColumnRepository extends JpaRepository<TaskListColumn, Long> {

    @Query(value = "SELECT t.id FROM task_list_column t", nativeQuery = true)
    List<Long> getAllTaskListColumnIds();

}
