package com.everwell.registryservice.repository;

import com.everwell.registryservice.models.db.UserAccess;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

public interface UserAccessRepository extends JpaRepository<UserAccess, Long> {
    Optional<UserAccess> findBySsoId(Long ssoId);

    Optional<UserAccess> findBySsoIdOrUsername(Long ssoId, String username);

    List<UserAccess> findByHierarchyIdAndActiveTrue(Long hierarchyId);
}
