package com.everwell.registryservice.repository;

import com.everwell.registryservice.models.db.DeploymentLanguageMap;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface DeploymentLanguageRepository extends JpaRepository<DeploymentLanguageMap, Long>
{
    List<DeploymentLanguageMap> getAllByDeploymentIdAndStoppedAtIsNull(Long deploymentId);
}
