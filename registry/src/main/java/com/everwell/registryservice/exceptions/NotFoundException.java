package com.everwell.registryservice.exceptions;

import com.everwell.registryservice.constants.ValidationStatusEnum;

public class NotFoundException extends RuntimeException {

    public NotFoundException(String message) {
        super(message);
    }

    public NotFoundException(ValidationStatusEnum validationError) {
        super(validationError.getMessage());
    }
}
