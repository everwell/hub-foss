package com.everwell.registryservice.exceptions;

import com.everwell.registryservice.constants.ValidationStatusEnum;

public class UnauthorizedException extends RuntimeException {

    public UnauthorizedException(String exception) {
        super(exception);
    }

    public UnauthorizedException(ValidationStatusEnum validationError) {
        super(validationError.getMessage());
    }
}
