package com.everwell.registryservice.exceptions;

import com.everwell.registryservice.constants.ValidationStatusEnum;

public class InternalServerErrorException extends RuntimeException {

    public InternalServerErrorException(String message) {
        super(message);
    }
    public InternalServerErrorException(ValidationStatusEnum validationError) {
        super(validationError.getMessage());
    }

}
