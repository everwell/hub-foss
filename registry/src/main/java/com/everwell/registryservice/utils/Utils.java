package com.everwell.registryservice.utils;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.util.Date;

public class Utils {
    private static ObjectMapper objectMapper = new ObjectMapper();

    public static LocalDateTime getCurrentDate() {
        return LocalDateTime.now();
    }
    public static Long getCurrentTime() { return new Date().getTime(); }

    public static Date convertStringToDate(String date, String format) {
        try {
            SimpleDateFormat df = new SimpleDateFormat(format);
            df.setLenient(false);
            return df.parse(date);
        } catch (ParseException e) {
            throw new IllegalArgumentException("[convertStringToDate] Unable to parse date: " + date + " format: " + format);
        }
    }

    public static Date convertStringToDate(String date) {
        return convertStringToDate(date, "yyyy-MM-dd HH:mm:ss");
    }

    public static String asJsonString(final Object obj) {
        try {
            return objectMapper.writeValueAsString(obj);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    public static <T> T jsonToObject(String json, TypeReference<T> typeReference) throws IOException {
        return objectMapper.readValue(json, typeReference);
    }

    public static <T> T convertObject(String from, Class<T> to) throws Exception {
        return objectMapper.readValue(from, to);
    }

    public static Object convertObject(String from, TypeReference typeReference) throws Exception {
        objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
        return objectMapper.readValue(from, typeReference);
    }

}
