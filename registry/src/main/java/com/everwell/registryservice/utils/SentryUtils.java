package com.everwell.registryservice.utils;

import com.everwell.registryservice.constants.Constants;
import io.sentry.Sentry;
import io.sentry.event.EventBuilder;
import io.sentry.event.interfaces.ExceptionInterface;
import io.sentry.event.interfaces.HttpInterface;
import org.springframework.web.context.request.ServletWebRequest;
import org.springframework.web.context.request.WebRequest;

public class SentryUtils {

    public static void captureException(Exception e, WebRequest request) {
        EventBuilder builder = new EventBuilder().withSentryInterface(new HttpInterface(((ServletWebRequest) request).getRequest()))
                .withSentryInterface(new ExceptionInterface(e));
        Sentry.capture(builder);
    }

    public static void captureException(Exception e) {
        Sentry.capture(e.toString());
    }
    
    public static void captureException(Exception e, String data) {
        EventBuilder builder = new EventBuilder().withSentryInterface(new ExceptionInterface(e));
        if (!data.isEmpty()) {
            builder.withExtra(Constants.DATA, data);
        }
        Sentry.capture(builder);
    }


}
