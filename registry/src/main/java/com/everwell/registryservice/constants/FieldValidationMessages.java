package com.everwell.registryservice.constants;

public class FieldValidationMessages {
    public static final String CONFIG_NAME_MANDATORY = "Config name must be provided";
    public static final String CONFIG_VALUE_MANDATORY = "Config value must be provided";
    public static final String CONFIG_TYPE_MANDATORY = "Config type must be provided";
    public static final String HIERARCHY_ID_MANDATORY = "Hierarchy Id must be provided";
    public static final String HIERARCHY_DATA_MANDATORY = "Hierarchy data must be provided";
    public static final String HIERARCHY_NAME_MANDATORY = "Hierarchy name must be provided";
    public static final String HIERARCHY_CODE_MANDATORY = "Hierarchy code must be provided";
    public static final String HIERARCHY_LEVEL_MANDATORY = "Hierarchy level must be provided";
    public static final String HIERARCHY_LEVEL_LESSER_THAN_SUPPORTED = "Hierarchy Level cannot be less than 1";
    public static final String HIERARCHY_LEVEL_GREATER_THAN_SUPPORTED = "Hierarchy Level greater than 6 not supported";
    public static final String HIERARCHY_TYPE_MANDATORY = "Hierarchy type must be provided";
    public static final String USER_NAME_MANDATORY = "User name must be provided";
    public static final String SSO_ID_MANDATORY = "SSO Id must be provided";
    public static final String PASSWORD_MANDATORY = "Password must be provided";
    public static final String LANGUAGE_ID_MANDATORY = "Language Id's must be provided";
    public static final String REQUEST_MISSING_DATA = "Request Data must be provided";
    public static final String CONFIG_MAPPING_ID_MANDATORY = "Config Mapping Id must be provided";
    public static final String INVALID_REQUEST_BODY = "Invalid request body";
}
