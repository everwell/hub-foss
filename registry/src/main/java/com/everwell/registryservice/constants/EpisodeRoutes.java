package com.everwell.registryservice.constants;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum EpisodeRoutes {
    GET_ALL_DISEASES("/v1/disease"),
    GET_TOKEN("/v1/client"),
    POST_HEADER_SEARCH("/v1/episode/header");

    private final String path;
}
