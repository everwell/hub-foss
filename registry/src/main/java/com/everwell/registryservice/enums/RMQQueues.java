package com.everwell.registryservice.enums;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public enum RMQQueues {
    Q_INS_PROCESS_SMS("q.ins.process_sms");

    @Getter
    private final String queue;
}
