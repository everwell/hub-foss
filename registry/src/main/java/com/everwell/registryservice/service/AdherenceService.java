package com.everwell.registryservice.service;

import com.everwell.registryservice.models.http.response.adherence.AdherenceTechnologyDto;

import java.util.List;

public interface AdherenceService {

    /**
     * Function to fetch all the adherence technology options
     *
     * @return A list of adherence technology dtos
     */
    List<AdherenceTechnologyDto> getAllTechnologyOptions();
}
