package com.everwell.registryservice.service;

import com.everwell.registryservice.models.db.Config;
import com.everwell.registryservice.models.dto.HierarchyLineage;
import com.everwell.registryservice.models.http.requests.AddConfigRequest;
import com.everwell.registryservice.models.http.requests.GenerateConfigHierarchyMappingRequest;
import com.everwell.registryservice.models.http.requests.GetConfigHierarchyMappingsRequest;
import com.everwell.registryservice.models.http.response.ConfigHierarchyMappingRecordResponse;
import com.everwell.registryservice.models.http.response.ConfigHierarchyMappingResponse;
import com.everwell.registryservice.models.http.response.ConfigHierarchyMappingBaseResponse;

import java.util.List;
import java.util.Map;

/**
 * Class Type - Service Interface
 * Purpose - Provide business logic for config management
 * Scope - Modules interacting with configs
 *
 * @author Ashish Shrivastava
 */
public interface ConfigService {

    /**
     * API to add a new Config into DB
     *
     * @param addConfigRequest - Input Config details
     * @return Added Config Details
     */
    Config addNewConfig(AddConfigRequest addConfigRequest);

    /**
     * API to retrieve Config details by Config Name
     *
     * @param configName - Input config name
     * @return Details of matching config from DB
     */
    Config fetchConfigDetails(String configName);

    /**
     * API to retrieve all configs from DB
     *
     * @return All configs from DB
     */
    List<Config> getAllConfigs();

    /**
     * API to delete particular Config from DB
     *
     * @param configName - Input config name
     * @return if delete config operation is successful
     */
    boolean deleteConfig(String configName);

    /**
     * API to validate the Configs provided in request
     *
     * @param inputConfigs - Input Configs
     */
    void validateInputConfigs(Map<String, Object> inputConfigs);

    /**
     * API to fetch Config Hierarchy Mappings from DB
     * <p>
     * Bubbling logic - If a particular config is not directly mapped to hierarchy,
     * then the ancestors of hierarchy will be queried,
     * if none of the ancestors have mapping then default value from Config master will be considered.
     * <p>
     * If no config name passed for a hierarchy then mappings corresponding to given hierarchy
     * for all configs present in system will be fetched
     *
     * @param getConfigHierarchyMappingsRequest - Request containing Hierarchy Ids and their corresponding Config names to be searched
     * @param hierarchyLineageList              - Hierarchy tree data to query parents if mapping not found for input hierarchy
     * @return - List of Configs List mapped to each hierarchy
     */
    List<ConfigHierarchyMappingResponse> fetchConfigHierarchyMappings(GetConfigHierarchyMappingsRequest getConfigHierarchyMappingsRequest, List<HierarchyLineage> hierarchyLineageList);

    /**
     * API to add new Config Hierarchy Mapping into DB
     *
     * @param configHierarchyMappingRequest - Input Config Hierarchy mapping details
     * @param hierarchyId                   - ID of Hierarchy for which mapping is to be added
     * @return Saved Config Hierarchy mapping details
     */
    ConfigHierarchyMappingRecordResponse addConfigHierarchyMapping(GenerateConfigHierarchyMappingRequest configHierarchyMappingRequest, Long hierarchyId);

    /**
     * Method to map configs with provided Hierarchy
     *
     * @param hierarchyId  - Input Hierarchy ID
     * @param inputConfigs - Configs to be mapped to Hierarchy
     */
    void mapConfigsWithHierarchy(Long hierarchyId, Map<String, Object> inputConfigs);

    /**
     * API to delete a particular Config Hierarchy Mapping from DB
     *
     * @param configName  - Name of Config Hierarchy Mapping to be deleted
     * @param hierarchyId - ID of Hierarchy For which mapping is to be deleted
     * @return Whether Config Hierarchy mapping deletion is successful
     */
    Boolean deleteConfigHierarchyMapping(String configName, Long hierarchyId);

    /**
     * API to update an existing Config Hierarchy Mapping in DB
     *
     * @param configHierarchyMappingRequest - Input Config Hierarchy mapping details
     * @param hierarchyId                   - ID of Hierarchy for which mapping is to be updated
     * @return Saved Config Hierarchy mapping details
     */
    ConfigHierarchyMappingRecordResponse updateConfigHierarchyMapping(GenerateConfigHierarchyMappingRequest configHierarchyMappingRequest, Long hierarchyId);

    /**
     * API to fetch config hierarchy mappings corresponding to input config hierarchy mapping Ids
     *
     * @param mappingIds - Input hierarchy config mapping Ids
     * @return List of hierarchy config mappings
     */
    List<ConfigHierarchyMappingBaseResponse> fetchConfigHierarchyMappingsById(List<Long> mappingIds);
}
