package com.everwell.registryservice.service;


import com.everwell.registryservice.models.http.requests.ins.TemplateRequest;
import com.everwell.registryservice.models.http.response.Response;
import com.everwell.registryservice.models.http.response.ins.LanguageResponse;
import com.everwell.registryservice.models.http.response.ins.TemplateResponse;
import org.springframework.http.ResponseEntity;

import java.util.List;

public interface INSService {

    ResponseEntity<Response<List<TemplateResponse>>> getAllTemplates(String clientId);

    ResponseEntity<Response<Long>> saveTemplate(String clientId, TemplateRequest templateRequest);

    ResponseEntity<Response<LanguageResponse>> getAllLanguages(String clientId);
}
