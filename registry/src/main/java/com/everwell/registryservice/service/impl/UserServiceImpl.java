package com.everwell.registryservice.service.impl;

import com.everwell.registryservice.constants.AuthConstants;
import com.everwell.registryservice.constants.Constants;
import com.everwell.registryservice.constants.ValidationStatusEnum;
import com.everwell.registryservice.exceptions.ConflictException;
import com.everwell.registryservice.exceptions.NotFoundException;
import com.everwell.registryservice.exceptions.UnauthorizedException;
import com.everwell.registryservice.mappers.SidebarItemMapper;
import com.everwell.registryservice.mappers.UserMapper;
import com.everwell.registryservice.models.db.*;
import com.everwell.registryservice.models.dto.SidebarItemResponse;
import com.everwell.registryservice.models.dto.UserSummaryDto;
import com.everwell.registryservice.models.dto.UnifiedListFilterResponse;
import com.everwell.registryservice.models.http.requests.CreateUserRequest;
import com.everwell.registryservice.models.http.response.HeaderResponse;
import com.everwell.registryservice.models.http.response.adherence.AdherenceTechnologyDto;
import com.everwell.registryservice.repository.SidebarItemRepository;
import com.everwell.registryservice.repository.SidebarPermissionRepository;
import com.everwell.registryservice.repository.UserAccessHierarchyMapRepository;
import com.everwell.registryservice.repository.UserAccessRepository;
import com.everwell.registryservice.service.UserService;
import com.everwell.registryservice.validators.UserRequestValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.util.Pair;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * Class Type - Service Class
 * Purpose - Provide business logic for user access management
 * Scope - Modules interacting with user access
 *
 * @author Ashish Shrivastava
 */
@Service
public class UserServiceImpl implements UserService {

    @Autowired
    private UserRequestValidator userRequestValidator;

    @Autowired
    private UserAccessRepository userAccessRepository;

    @Autowired
    private UserAccessHierarchyMapRepository userAccessHierarchyMapRepository;

    @Autowired
    private SidebarItemRepository sidebarItemRepository;

    @Autowired
    private SidebarPermissionRepository sidebarPermissionRepository;

    /**
     * API to create new user in system
     *
     * @param createUserRequest - Details of user to be created
     * @return Created User Data
     */
    @Override
    public UserAccess addNewUser(CreateUserRequest createUserRequest, Long clientId) {
        // Validate create user request
        userRequestValidator.validateCreateUserRequest(createUserRequest);
        // Create new user login data from request
        UserAccess newUser = UserMapper.INSTANCE.createUserRequestToUserAccess(createUserRequest);
        // Activate user
        newUser.setClientId(clientId);
        newUser.setActive(true);
        // Save new user login
        userAccessRepository.save(newUser);
        // Create record for User Access Hierarchy Map table
        UserAccessHierarchyMap newUserMappingRecord = new UserAccessHierarchyMap(newUser);
        newUserMappingRecord.setRelation(Constants.DEFAULT_USER_HIERARCHY_RELATIONSHIP);
        // Insert records in user access hierarchy map
        userAccessHierarchyMapRepository.save(newUserMappingRecord);
        // Return user details
        return newUser;
    }

    /**
     * API to fetch user data from DB
     *
     * @param ssoId - SSO ID to be searched
     * @return User Data
     */
    public UserAccess fetchUserData(Long ssoId) {
        UserAccess user = userAccessRepository.findBySsoId(ssoId).orElse(null);
        // If user does not exist in system, throw error
        if (user == null) {
            throw new NotFoundException(ValidationStatusEnum.USER_NOT_FOUND);
        }
        return user;
    }

    /**
     * API to fetch all associated hierarchies of the user
     *
     * @param userid - Input user id
     * @return List of associated hierarchy Ids
     */
    @Override
    public List<Long> fetchHierarchiesAssociatedWithUser(Long userid) {
        List<Long> hierarchyIdList = null;
        // Check if any hierarchy mapping present for user
        List<UserAccessHierarchyMap> userHierarchyMappings = userAccessHierarchyMapRepository.findByUserId(userid);
        if (!CollectionUtils.isEmpty(userHierarchyMappings)) {
            // If mappings present then capture all hierarchy id of mappings
            hierarchyIdList = userHierarchyMappings.stream().map(UserAccessHierarchyMap::getHierarchyId).collect(Collectors.toList());
        }
        // Return response
        return hierarchyIdList;
    }

    /**
     * API to soft delete a user from system
     *
     * @param ssoId - Input SSO ID to be deleted
     * @return Whether deletion is successful
     */
    @Override
    public Boolean deleteUser(Long ssoId) {
        // Fetch user data
        UserAccess user = fetchUserData(ssoId);
        // If user is already inactive throw conflict exception
        if (!user.isActive()) {
            throw new ConflictException(ValidationStatusEnum.USER_ALREADY_INACTIVE);
        }
        // Soft Delete user from system
        user.setActive(false);
        userAccessRepository.save(user);
        // Return current state of User
        return !user.isActive();
    }

    /**
     * API to get the pair of user and client based on the bearer token passed
     *
     * @return pair of client and useraccess objects
     */
    @Override
    public Pair<Client, UserAccess> getUserClientByToken() {
        Pair<Client, UserAccess> userAccessClient = null;
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        Object userPrincipal = auth.getPrincipal();
        if(!Constants.ANNONYMOUS_USER.equals(Objects.toString(userPrincipal))) {
            try {
                Client client = (Client) userPrincipal;
                UserAccess user = (UserAccess) auth.getCredentials();
                userAccessClient = Pair.of(client, user);
            } catch (Exception e) {
                throw new UnauthorizedException(ValidationStatusEnum.USER_SESSION_NOT_FOUND);
            }
        } else {
            throw new NotFoundException(ValidationStatusEnum.USER_NOT_FOUND);
        }
        return userAccessClient;
    }

    /**
     * API to build header response based on logged in user details
     *
     * @return header response object passed by reference
     */
    @Override
    public HeaderResponse buildUserHeaderDetails(UserAccess userAccess, HeaderResponse headerResponse) {
        headerResponse.setUserLoggedIn(true);

        //Hardcoded business logic for search criteria; Todo: define the business criteria for generalisation
        headerResponse.addSearchCriteriaOption(Pair.of("Name", "name"));
        headerResponse.addSearchCriteriaOption(Pair.of("Patient Id", "99dotsId"));
        headerResponse.addSearchCriteriaOption(Pair.of("TB Number", "ExternalId1"));

        //Hardcoded business logic for dropdown criteria; Todo: define the business criteria for generalisation
        headerResponse.addDropdownOption(Pair.of("Home", "/Overview"));
        headerResponse.addDropdownOption(Pair.of("Logout", "/Logout"));
        return headerResponse;
    }

    @Override
    public List<SidebarItemResponse> getSidebarItems(UserAccess user, Deployment deployment) {
        List<SidebarItem> sidebarItems = sidebarItemRepository.findAll();
        List<SidebarPermission> sidebarPermissions = sidebarPermissionRepository.findByDeploymentIdAndDesignationAndIsActiveTrue(deployment.getId(), AuthConstants.DEFAULT_DESIGNATION);
        if(CollectionUtils.isEmpty(sidebarPermissions)) {
            sidebarPermissions = sidebarPermissionRepository.findByDesignationAndIsActiveTrue(AuthConstants.DEFAULT_DESIGNATION);
        }
        List<Long> sidebarPermissionIds = sidebarPermissions.stream().map(SidebarPermission::getSidebarId).collect(Collectors.toList());

        //Todo: Configure the actual permissions based on designation once staff is in place
        List<SidebarItem> permissionEnabledSidebarItems = sidebarItems.stream().filter(item -> sidebarPermissionIds.contains(item.getId())).collect(Collectors.toList());
        List<SidebarItemResponse> sidebarItemResponse = SidebarItemMapper.INSTANCE.sidebarItemListToSidebarItemResponseList(permissionEnabledSidebarItems);
        return sidebarItemResponse;
    }

    @Override
    public List<UserSummaryDto> getUserSummaryResponseByHierarchyId(Long hierarchyId) {
        List<UserSummaryDto> summaryDtoList = new ArrayList<>();
        List<UserAccess> userAccessList = userAccessRepository.findByHierarchyIdAndActiveTrue(hierarchyId);
        if(!CollectionUtils.isEmpty(userAccessList)) {
            summaryDtoList = userAccessList.stream().map(userAccess -> new UserSummaryDto(userAccess.getUsername(), userAccess.getId())).collect(Collectors.toList());
        }
        return summaryDtoList;
    }

    @Override
    public UnifiedListFilterResponse getUppFilterOptions(UserAccess userAccess, List<AdherenceTechnologyDto> adherenceTechnologyDtoList) {
        //Todo: Filter based on the hasMERM, has99D, has99DL, hasVOT, hasNone configuration from HierarchyConfig
        UnifiedListFilterResponse filterResponse = new UnifiedListFilterResponse();
        filterResponse.setTechnologyOptions(adherenceTechnologyDtoList);

        //Todo: fetch stages based on diseases mapped from episode
        Map<String, String> onTreatmentStage = Map.of("hasCalendar", "true", "label", "on_treatment", "value", "ON_TREATMENT");
        Map<String, String> offTreatmentStage = Map.of("hasCalendar", "false", "label", "outcome_assigned", "value", "OFF_TREATMENT");
        filterResponse.setStageOptions(Arrays.asList(onTreatmentStage, offTreatmentStage));

        //Todo: Get first enrolment date as per episode in the hierarchy

        return filterResponse;

    }
}
