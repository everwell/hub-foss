package com.everwell.registryservice.service.impl;

import com.everwell.registryservice.constants.Constants;
import com.everwell.registryservice.constants.ValidationStatusEnum;
import com.everwell.registryservice.exceptions.ConflictException;
import com.everwell.registryservice.exceptions.NotFoundException;
import com.everwell.registryservice.exceptions.ValidationException;
import com.everwell.registryservice.mappers.ConfigMapper;
import com.everwell.registryservice.models.db.Config;
import com.everwell.registryservice.models.db.HierarchyConfigMap;
import com.everwell.registryservice.models.dto.HierarchyLineage;
import com.everwell.registryservice.models.http.requests.AddConfigRequest;
import com.everwell.registryservice.models.http.requests.GenerateConfigHierarchyMappingRequest;
import com.everwell.registryservice.models.http.requests.GetConfigHierarchyMappingsRequest;
import com.everwell.registryservice.models.http.response.ConfigHierarchyMappingRecordResponse;
import com.everwell.registryservice.models.http.response.ConfigHierarchyMappingResponse;
import com.everwell.registryservice.models.http.response.ConfigHierarchyMappingBaseResponse;
import com.everwell.registryservice.repository.ConfigRepository;
import com.everwell.registryservice.repository.HierarchyConfigMapRepository;
import com.everwell.registryservice.service.ConfigService;
import com.everwell.registryservice.utils.SentryUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.util.*;
import java.util.stream.Collectors;

/**
 * Class Type - Service Class
 * Purpose - Provide business logic for config management
 * Scope - Modules interacting with configs
 *
 * @author Ashish Shrivastava
 */
@Service
public class ConfigServiceImpl implements ConfigService {

    @Autowired
    private ConfigRepository configRepository;

    @Autowired
    private HierarchyConfigMapRepository hierarchyConfigMapRepository;

    /**
     * API to add a new Config into DB
     *
     * @param addConfigRequest - Input Config details
     * @return Added Config Details
     */
    @Override
    public Config addNewConfig(AddConfigRequest addConfigRequest) {
        if (isExistingConfig(addConfigRequest.getName())) {
            throw new ConflictException(ValidationStatusEnum.CONFIG_ALREADY_EXISTS);
        }
        Config config = ConfigMapper.INSTANCE.addConfigRequestToConfig(addConfigRequest);
        return configRepository.save(config);
    }

    /**
     * API to retrieve Config details by Config Name
     *
     * @param configName - Input config name
     * @return Details of matching config from DB
     */
    @Override
    public Config fetchConfigDetails(String configName) {
        Config config = fetchConfigFromDB(configName);
        if (null == config) {
            throw new NotFoundException(ValidationStatusEnum.CONFIG_NOT_FOUND);
        }
        return config;
    }

    /**
     * API to retrieve all configs from DB
     *
     * @return All configs from DB
     */
    @Override
    public List<Config> getAllConfigs() {
        List<Config> allConfigs = configRepository.findAll();
        if (CollectionUtils.isEmpty(allConfigs)) {
            throw new NotFoundException(ValidationStatusEnum.NO_CONFIGS_FOUND);
        }
        return allConfigs;
    }

    /**
     * API to delete particular Config from DB
     *
     * @param configName - Input config name
     * @return if delete config operation is successful
     */
    @Override
    public boolean deleteConfig(String configName) {
        // Fetch Config details from DB, internal check to validate if Config exists
        Config config = fetchConfigDetails(configName);
        // Check if Config has mappings present in DB, as we should not allow deletion of mapped configs
        Integer hierarchyMappingsPresentForConfig = hierarchyConfigMapRepository.countByConfigMappingId(config.getId());
        if (hierarchyMappingsPresentForConfig > 0) {
            throw new ConflictException(ValidationStatusEnum.MAPPED_CONFIG_CANNOT_BE_DELETED);
        }
        Integer deletedRecords = configRepository.deleteByName(configName);
        return deletedRecords > 0;
    }

    /**
     * API to validate the Configs provided in request
     *
     * @param inputConfigs - Input Configs
     */
    @Override
    public void validateInputConfigs(Map<String, Object> inputConfigs) {
        if (!CollectionUtils.isEmpty(inputConfigs)) {
            // The total number of configs received in request
            Integer numberOfConfigsFromRequest = inputConfigs.size();
            List<String> configNames = new ArrayList<>(inputConfigs.keySet());
            // Get count of configs from config table matching with input config names and values
            Integer configsInDb = configRepository.countByNameIn(configNames);
            // If the input and db config count has mismatched,
            // then either some config does not exist or there is duplication, throw validation error
            if (!Objects.equals(configsInDb, numberOfConfigsFromRequest)) {
                throw new ValidationException(ValidationStatusEnum.CONFIG_ASSOCIATED_WITH_HIERARCHY_DOES_NOT_EXIST);
            }
        }
    }

    /**
     * API to fetch Config Hierarchy Mappings from DB
     * <p>
     * Bubbling logic - If a particular config is not directly mapped to hierarchy,
     * then the ancestors of hierarchy will be queried,
     * if none of the ancestors have mapping then default value from Config master will be considered.
     * <p>
     * If no config name passed for a hierarchy then mappings corresponding to given hierarchy
     * for all configs present in system will be fetched
     *
     * @param getConfigHierarchyMappingsRequest - Request containing Hierarchy Ids and their corresponding Config names to be searched
     * @param hierarchyLineageList              - Hierarchy tree data to query parents if mapping not found for input hierarchy
     * @return - List of Configs List mapped to each hierarchy
     */
    @Override
    public List<ConfigHierarchyMappingResponse> fetchConfigHierarchyMappings(GetConfigHierarchyMappingsRequest getConfigHierarchyMappingsRequest, List<HierarchyLineage> hierarchyLineageList) {
        // Take data from request
        Map<Long, List<String>> requestedHierarchyConfigMappings = getConfigHierarchyMappingsRequest.getHierarchies();
        List<ConfigHierarchyMappingResponse> responseList = new ArrayList<>(requestedHierarchyConfigMappings.size());
        // Query Config Master table to get all default config values,
        // To be used when any of the config requested does not have mapping with the hierarchy's lineage
        List<Config> configTableMasterData = getAllConfigs();
        // Prepare maps of config table master data for easy access
        Map<String, Config> configTableMasterDataMapByName = new HashMap<>(configTableMasterData.size());
        Map<Long, Config> configTableMasterDataMapById = new HashMap<>(configTableMasterData.size());
        configTableMasterData.forEach(configMasterRecord -> {
            configTableMasterDataMapByName.put(configMasterRecord.getName(), configMasterRecord);
            configTableMasterDataMapById.put(configMasterRecord.getId(), configMasterRecord);
        });
        // Iterate the hierarchy lineages for which config mappings are to be fetched
        hierarchyLineageList.forEach(hierarchyLineage -> {
            // Fetch config names to be searched for the particular hierarchy from request
            List<String> configNames = requestedHierarchyConfigMappings.get(hierarchyLineage.getId());
            List<Long> configIds;
            // If config names not passed in request then search for all config names
            // Extract config Ids corresponding to the config names from Config master data
            if (CollectionUtils.isEmpty(configNames)) {
                configIds = new ArrayList<>(configTableMasterDataMapById.keySet());
            } else {
                configIds = configNames.stream()
                        .map(configName -> {
                            Config config = configTableMasterDataMapByName.get(configName);
                            if (null == config) {
                                throw new NotFoundException(ValidationStatusEnum.CONFIG_NOT_FOUND);
                            }
                            return config.getId();
                        }).collect(Collectors.toList());
            }
            // Fetch requested Config Mappings of current hierarchy
            List<ConfigHierarchyMappingRecordResponse> configHierarchyMappings = fetchConfigMappingsForHierarchy(hierarchyLineage, configIds, configTableMasterDataMapById);
            // Add Config Hierarchy Mapping records in response
            responseList.add(new ConfigHierarchyMappingResponse(hierarchyLineage.getId(), configHierarchyMappings));
        });
        return responseList;
    }

    /**
     * API to add new Config HierarchyMapping into DB
     *
     * @param configHierarchyMappingRequest - Input Config Hierarchy mapping details
     * @param hierarchyId                   - ID of Hierarchy for which mapping is to be added
     * @return Saved Config Hierarchy mapping details
     */
    @Override
    public ConfigHierarchyMappingRecordResponse addConfigHierarchyMapping(GenerateConfigHierarchyMappingRequest configHierarchyMappingRequest, Long hierarchyId) {
        // Prepare Config Hierarchy Mapping Data for insertion into DB, internal validation to check if hierarchy and config exists in DB
        HierarchyConfigMap hierarchyConfigMap = buildHierarchyConfigMapping(configHierarchyMappingRequest.getConfigName(), false, hierarchyId);
        // Save Config Hierarchy Mapping
        storeHierarchyConfigMappingInDB(hierarchyConfigMap, configHierarchyMappingRequest.getValue());
        // Prepare Config Hierarchy Mapping Response data
        ConfigHierarchyMappingRecordResponse configHierarchyMappingRecordResponse = ConfigMapper.INSTANCE.hierarchyConfigMapToConfigHierarchyMappingRecordResponse(hierarchyConfigMap);
        // Update Config Name in Config Hierarchy Mapping Response Data
        configHierarchyMappingRecordResponse.setConfigName(configHierarchyMappingRequest.getConfigName());
        return configHierarchyMappingRecordResponse;
    }

    /**
     * Method to map configs with provided Hierarchy
     *
     * @param hierarchyId  - Input Hierarchy ID
     * @param inputConfigs - Configs to be mapped to Hierarchy
     */
    public void mapConfigsWithHierarchy(Long hierarchyId, Map<String, Object> inputConfigs) {
        List<String> inputConfigsNames = new ArrayList<>(inputConfigs.keySet());
        // List of Hierarchy Config Mappings to be stored in DB
        List<HierarchyConfigMap> hierarchyConfigs = new ArrayList<>(inputConfigs.size());
        // Query Config table to fetch all Configs corresponding to input Config names
        List<Config> configs = getAllConfigs(inputConfigsNames);
        // List of required config names which already exist in master config table
        List<String> existingConfigNames = new ArrayList<>();
        if (!CollectionUtils.isEmpty(configs)) {
            // Prepare Hierarchy Config Mappings
            hierarchyConfigs.addAll(configs.stream().map(config -> {
                // Add mapping name to existing config list
                existingConfigNames.add(config.getName());
                return new HierarchyConfigMap(hierarchyId, config.getId(), String.valueOf(inputConfigs.get(config.getName())));
            }).collect(Collectors.toList()));
        }
        // Call Config Service to create Configs which are passed in request but not exist in DB currently,
        // Should not happen in update Hierarchy flow as the validation for Configs should have happened earlier
        List<Config> newConfigs = createNewConfigs(inputConfigsNames, existingConfigNames, inputConfigs);
        if (!CollectionUtils.isEmpty(newConfigs)) {
            // Prepare Hierarchy Config Mappings
            hierarchyConfigs.addAll(newConfigs.stream().map(config -> new HierarchyConfigMap(hierarchyId, config.getId(), String.valueOf(inputConfigs.get(config.getName())))).collect(Collectors.toList()));
        }
        if (!CollectionUtils.isEmpty(hierarchyConfigs)) {
            // Store Hierarchy Config Mappings in DB
            hierarchyConfigMapRepository.saveAll(hierarchyConfigs);
        }
    }

    /**
     * API to delete a particular Config Hierarchy Mapping from DB
     *
     * @param configName  - Name of Config Hierarchy Mapping to be deleted
     * @param hierarchyId - ID of Hierarchy For which mapping is to be deleted
     * @return Whether Config Hierarchy mapping deletion is successful
     */
    @Override
    public Boolean deleteConfigHierarchyMapping(String configName, Long hierarchyId) {
        // Fetch Config Hierarchy Mapping from DB, internal validation to check if data exists in DB
        HierarchyConfigMap hierarchyConfigMap = buildHierarchyConfigMapping(configName, true, hierarchyId);
        // Delete Config Hierarchy Mapping from DB
        boolean isDeletionSuccessful = false;
        try {
            hierarchyConfigMapRepository.deleteById(hierarchyConfigMap.getId());
            isDeletionSuccessful = true;
        } catch (Exception e) {
            SentryUtils.captureException(e);
        }
        // Return the status of config hierarchy mapping deletion
        return isDeletionSuccessful;
    }

    /**
     * API to update an existing Config Hierarchy Mapping in DB
     *
     * @param configHierarchyMappingRequest - Input Config Hierarchy mapping details
     * @param hierarchyId                   - ID of Hierarchy for which mapping is to be updated
     * @return Saved Config Hierarchy mapping details
     */
    @Override
    public ConfigHierarchyMappingRecordResponse updateConfigHierarchyMapping(GenerateConfigHierarchyMappingRequest configHierarchyMappingRequest, Long hierarchyId) {
        // Fetch Config Hierarchy Mapping from DB, internal validation to check if data exists in DB
        HierarchyConfigMap hierarchyConfigMap = buildHierarchyConfigMapping(configHierarchyMappingRequest.getConfigName(), true, hierarchyId);
        // If there is no change in value of config hierarchy mapping then no need to update
        if (!configHierarchyMappingRequest.getValue().equalsIgnoreCase(hierarchyConfigMap.getValue())) {
            // Save updated Mapping
            storeHierarchyConfigMappingInDB(hierarchyConfigMap, configHierarchyMappingRequest.getValue());
        }
        // Prepare Config Hierarchy Mapping Response data
        ConfigHierarchyMappingRecordResponse configHierarchyMappingRecordResponse = ConfigMapper.INSTANCE.hierarchyConfigMapToConfigHierarchyMappingRecordResponse(hierarchyConfigMap);
        // Update Config Name in Config Hierarchy Mapping Response Data
        configHierarchyMappingRecordResponse.setConfigName(configHierarchyMappingRequest.getConfigName());
        return configHierarchyMappingRecordResponse;
    }

    /**
     * API to fetch config hierarchy mappings corresponding to input config hierarchy mapping Ids
     *
     * @param mappingIds - Input hierarchy config mapping Ids
     * @return List of hierarchy config mappings
     */
    @Override
    public List<ConfigHierarchyMappingBaseResponse> fetchConfigHierarchyMappingsById(List<Long> mappingIds) {
        // Query Repository to fetch all config hierarchy mappings as per provided Ids
        List<HierarchyConfigMap> hierarchyConfigMappingsList = hierarchyConfigMapRepository.findAllById(mappingIds);
        // If no mapping found in DB, then return error
        if(CollectionUtils.isEmpty(hierarchyConfigMappingsList)){
            throw new NotFoundException(ValidationStatusEnum.HIERARCHY_CONFIG_MAPPING_NOT_FOUND);
        }
        // Prepare response object from DB entity.
        return hierarchyConfigMappingsList.stream().map(ConfigMapper.INSTANCE::hierarchyConfigMapToConfigHierarchyMappingBaseResponse).collect(Collectors.toList());
    }

    /**
     * API to fetch details of a Config from DB
     *
     * @param configName - config name to be searched
     * @return Details of Config from DB
     */
    private Config fetchConfigFromDB(String configName) {
        return configRepository.findByName(configName).orElse(null);
    }

    /**
     * API to check if a particular Config already exists in DB
     *
     * @param configName - Name of Config to be searched
     * @return Config exists or not
     */
    private boolean isExistingConfig(String configName) {
        Config config = fetchConfigFromDB(configName);
        return null != config;
    }

    /**
     * Common method to retrieve/create Hierarchy mapping data for DB update
     *
     * @param configName       - Name of hierarchy config mapping to be created/modified
     * @param isExistingConfig - Whether mapping is new or existing
     * @param hierarchyId      - ID of Hierarchy to which config is mapped
     * @return Hierarchy Config Mapping Data
     */
    private HierarchyConfigMap buildHierarchyConfigMapping(String configName, boolean isExistingConfig, Long hierarchyId) {
        HierarchyConfigMap hierarchyConfigMap;
        // Get Config details, internal validation to check if Config exists
        Config config = fetchConfigDetails(configName);
        // Fetch Hierarchy Config Mapping details from DB
        List<HierarchyConfigMap> hierarchyConfigMapList = hierarchyConfigMapRepository.getByHierarchyIdAndConfigMappingId(hierarchyId, config.getId()).orElse(null);
        // If existing config then check if data for mapping present in DB
        if (isExistingConfig) {
            // If Hierarchy Config Mapping does not exist, throw error
            if (CollectionUtils.isEmpty(hierarchyConfigMapList)) {
                throw new NotFoundException(ValidationStatusEnum.HIERARCHY_CONFIG_MAPPING_NOT_FOUND);
            }// If Multiple Hierarchy Config Mappings are present then isolating record for update/delete is not possible, throw error
            else if (hierarchyConfigMapList.size() > 1) {
                throw new ConflictException(ValidationStatusEnum.MULTIPLE_HIERARCHY_CONFIG_MAPPING_PRESENT);
            }
            // Read the hierarchy config mapping from list
            hierarchyConfigMap = hierarchyConfigMapList.get(0);
        } else { // If new Config then create new Hierarchy Config Mapping Object
            if (!CollectionUtils.isEmpty(hierarchyConfigMapList) && !config.isMultiMapped()) {
                // If Hierarchy Config Mapping already exists and the config is not multi-mapped, throw error
                throw new ConflictException(ValidationStatusEnum.HIERARCHY_CONFIG_MAPPING_REDUNDANT);
            }
            hierarchyConfigMap = new HierarchyConfigMap();
            hierarchyConfigMap.setHierarchyId(hierarchyId);
            hierarchyConfigMap.setConfigMappingId(config.getId());
            hierarchyConfigMap.setActive(true);
        }
        // Return Hierarchy Config Mapping Data
        return hierarchyConfigMap;
    }

    /**
     * Method to store/update Hierarchy Config Mapping in DB with required value
     *
     * @param hierarchyConfigMap - Hierarchy Config Mapping object to be stored/updated
     * @param value              - New value of Hierarchy Config Mapping
     */
    private void storeHierarchyConfigMappingInDB(HierarchyConfigMap hierarchyConfigMap, String value) {
        // Update value of Hierarchy Config Mapping
        hierarchyConfigMap.setValue(value);
        // Save Hierarchy Config Mapping in DB
        hierarchyConfigMapRepository.save(hierarchyConfigMap);
    }

    /**
     * API to create new configs and store in DB if missing from existing Config list
     *
     * @param inputConfigsNames   - names of Config received from Request
     * @param existingConfigNames - matching names of Configs already existing in DB
     * @param inputConfigs        - Input data for Configs received from Request
     * @return New Configs created in DB
     */
    private List<Config> createNewConfigs(List<String> inputConfigsNames, List<String> existingConfigNames, Map<String, Object> inputConfigs) {
        // List of configs which are not present in DB currently, these Configs will be created and inserted into DB
        List<String> newConfigNames = inputConfigsNames.stream().filter(inputConfig -> !existingConfigNames.contains(inputConfig)).collect(Collectors.toList());
        // Prepare Data for new Configs to be created
        List<Config> newConfigsToBeCreated = newConfigNames.stream().map(newConfigName -> {
            Config newConfig = new Config();
            newConfig.setName(newConfigName);
            newConfig.setDefaultValue(String.valueOf(inputConfigs.get(newConfigName)));
            newConfig.setType(Constants.DEFAULT_CONFIG_TYPE);
            return newConfig;
        }).collect(Collectors.toList());
        // Store New Configs in DB
        return configRepository.saveAll(newConfigsToBeCreated);
    }

    /**
     * API to get All Config details from DB for provided list of Config Names
     *
     * @param configNames - Input List of Config Names
     * @return Details of matching Configs From DB
     */
    private List<Config> getAllConfigs(List<String> configNames) {
        return configRepository.findByNameIn(configNames);
    }

    private List<ConfigHierarchyMappingRecordResponse> fetchConfigMappingsForHierarchy(HierarchyLineage hierarchyLineage, List<Long> configIds, Map<Long, Config> configTableMasterDataMapById) {
        List<ConfigHierarchyMappingRecordResponse> mappingsList = new ArrayList<>();
        Map<Long, List<HierarchyConfigMap>> multiMappedHierarchyConfigs = new HashMap<>();
        // Take out ancestral data of hierarchy
        Map<Long, Long> hierarchyLineageAncestralLevelMap = hierarchyLineage.getAncestralLevelMap();
        // Prepare list of hierarchy ids including ancestral and base id
        List<Long> hierarchyIds = new ArrayList<>(hierarchyLineageAncestralLevelMap.keySet());
        // Call DB to fetch all possible mappings for provided config ids corresponding to base hierarchy and its ancestral hierarchies
        List<HierarchyConfigMap> configHierarchyMappingsForHierarchyTree = hierarchyConfigMapRepository.getByHierarchyIdInAndConfigMappingIdIn(hierarchyIds, configIds);
        // Map of hierarchy config mappings which we need
        Map<Long, HierarchyConfigMap> filteredHierarchyConfigMappings = new HashMap<>();
        Map<Long, Long> filteredConfigLevelMap = new HashMap<>();
        // Loop over the retrieved config mappings to filter out mappings not needed
        configHierarchyMappingsForHierarchyTree.forEach(configHierarchyMapping -> {
            Long hierarchyId = configHierarchyMapping.getHierarchyId();
            Long configMappingId = configHierarchyMapping.getConfigMappingId();
            boolean isMultiMappingEligibleConfig = configTableMasterDataMapById.get(configMappingId).isMultiMapped();
            // Check if any mapping for this config id is already considered
            HierarchyConfigMap storedConfigHierarchyMapping = filteredHierarchyConfigMappings.get(configMappingId);
            if (null == storedConfigHierarchyMapping) {
                // If not considered then add this config mapping to filtered mappings and store the level of hierarchy for comparison
                filteredHierarchyConfigMappings.put(configMappingId, configHierarchyMapping);
                filteredConfigLevelMap.put(configMappingId, hierarchyLineageAncestralLevelMap.get(hierarchyId));
                // Check if Config has multi mapping support
                if (isMultiMappingEligibleConfig) {
                    // Config has multi mapping support, store all mappings separately as well
                    List<HierarchyConfigMap> hierarchyConfigMappingsList = new ArrayList<>();
                    hierarchyConfigMappingsList.add(configHierarchyMapping);
                    multiMappedHierarchyConfigs.put(configMappingId, hierarchyConfigMappingsList);
                }
            } else {
                // If a mapping corresponding to this config id is already present in filtered map then we need to select which mapping is more relevant
                Long currentMappingLevel = hierarchyLineageAncestralLevelMap.get(hierarchyId);
                Long storedMappingLevel = filteredConfigLevelMap.get(configMappingId);
                // Compare the level of hierarchies of this mapping with the previously considered mapping in store
                if (currentMappingLevel < storedMappingLevel) {
                    // If current mapping level is less than stored mapping level,
                    // it means there is mapping present at child level of previously considered hierarchy
                    // We need to prioritize mappings from child to parent (bubbling logic) hence we will replace the stored mapping with current mapping
                    filteredHierarchyConfigMappings.put(configMappingId, configHierarchyMapping);
                    filteredConfigLevelMap.put(configMappingId, hierarchyLineageAncestralLevelMap.get(hierarchyId));
                    // If Config is multi mapping eligible then clear the previously stored mappings from multi mapping record
                    if (isMultiMappingEligibleConfig) {
                        multiMappedHierarchyConfigs.remove(configMappingId);
                    }
                } else if (currentMappingLevel.equals(storedMappingLevel)) {
                    // Multiple mappings found at same hierarchy level for the config
                    // Verify if Config has multi mapping support
                    if (isMultiMappingEligibleConfig) {
                        // Config has multi mapping support, add the mapping to mapping list
                        multiMappedHierarchyConfigs.get(configMappingId).add(configHierarchyMapping);
                    } else {
                        // For a non multi-mapping eligible config two mappings at same hierarchy level should not exist, throw error
                        throw new ConflictException(ValidationStatusEnum.MULTIPLE_HIERARCHY_CONFIG_MAPPING_PRESENT);
                    }
                }
            }
        });
        // Prepare response data for all config Ids requested
        configIds.forEach(configId -> {
            Config configMasterDetail = configTableMasterDataMapById.get(configId);
            ConfigHierarchyMappingRecordResponse configHierarchyMappingRecordResponse;
            // Check if mappings found for each config
            HierarchyConfigMap configHierarchyMapping = filteredHierarchyConfigMappings.get(configId);
            if (null != configHierarchyMapping) {
                // If mapping found then convert the mapping into response format
                configHierarchyMappingRecordResponse = ConfigMapper.INSTANCE.hierarchyConfigMapToConfigHierarchyMappingRecordResponse(configHierarchyMapping);
                configHierarchyMappingRecordResponse.setConfigName(configMasterDetail.getName());
                // Check if Config is multi mapping eligible and multiple hierarchy config mappings present for config
                if (configMasterDetail.isMultiMapped()) {
                    List<HierarchyConfigMap> hierarchyConfigMapList = multiMappedHierarchyConfigs.get(configId);
                    if (!CollectionUtils.isEmpty(hierarchyConfigMapList)) {
                        // Multiple mappings present for the config, pass all mapping values as part of values field
                        configHierarchyMappingRecordResponse.setMultiMapped(true);
                        configHierarchyMappingRecordResponse.setValue(null);
                        configHierarchyMappingRecordResponse.setValues(hierarchyConfigMapList.stream().map(HierarchyConfigMap::getValue).collect(Collectors.toList()));
                    }

                }
            } else {
                // If for any requested config name a corresponding config mapping is not found,
                // then we need to consider the default master values for that config
                // Prepare response from default config value
                configHierarchyMappingRecordResponse = new ConfigHierarchyMappingRecordResponse();
                configHierarchyMappingRecordResponse.setHierarchyId(hierarchyLineage.getId());
                configHierarchyMappingRecordResponse.setConfigName(configMasterDetail.getName());
                configHierarchyMappingRecordResponse.setActive(true);
                configHierarchyMappingRecordResponse.setMultiMapped(configMasterDetail.isMultiMapped());
                if (configHierarchyMappingRecordResponse.isMultiMapped()) {
                    configHierarchyMappingRecordResponse.setValues(Collections.singletonList(configMasterDetail.getDefaultValue()));
                } else {
                    configHierarchyMappingRecordResponse.setValue(configMasterDetail.getDefaultValue());
                }
            }
            mappingsList.add(configHierarchyMappingRecordResponse);
        });
        return mappingsList;
    }
}
