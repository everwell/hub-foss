package com.everwell.registryservice.service.impl;

import com.everwell.registryservice.constants.Constants;
import com.everwell.registryservice.constants.ValidationStatusEnum;
import com.everwell.registryservice.exceptions.ConflictException;
import com.everwell.registryservice.exceptions.NotFoundException;
import com.everwell.registryservice.exceptions.ValidationException;
import com.everwell.registryservice.mappers.HierarchyMapper;
import com.everwell.registryservice.models.db.Hierarchy;
import com.everwell.registryservice.models.dto.*;
import com.everwell.registryservice.models.http.requests.UserUpdateHierarchyRequest;
import com.everwell.registryservice.models.http.response.HierarchyResponse;
import com.everwell.registryservice.models.http.response.HierarchySummaryResponseData;
import com.everwell.registryservice.repository.HierarchyRepository;
import com.everwell.registryservice.service.HierarchyService;
import com.everwell.registryservice.validators.HierarchyRequestValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.util.*;
import java.util.stream.Collectors;

import static java.util.stream.Collectors.groupingBy;

/**
 * Class Type - Service Class
 * Purpose - Provide business logic for hierarchy management
 * Scope - Modules interacting with hierarchies
 *
 * @author Ashish Shrivastava
 */
@Service
public class HierarchyServiceImpl implements HierarchyService {

    @Autowired
    private HierarchyRequestValidator hierarchyRequestValidator;

    @Autowired
    private HierarchyRepository hierarchyRepository;

    /**
     * API to add new Hierarchy, and it's related mappings in DB
     *
     * @param hierarchyRequestData - Details of Hierarchy to be added
     * @param clientId             - Client Id fetched from request
     * @param deploymentId         - Deployment ID corresponding to input code for level 1 hierarchy
     * @return Added Hierarchy Details
     */
    @Override
    public HierarchyResponseData addNewHierarchy(HierarchyRequestData hierarchyRequestData, Long clientId, Long deploymentId) {
        // Validate Add Hierarchy Request, internally throws validation error for validation failure
        hierarchyRequestValidator.validateAddHierarchyRequest(hierarchyRequestData);
        // Read hierarchy base data from request
        Hierarchy newHierarchy = HierarchyMapper.INSTANCE.hierarchyRequestDataToHierarchy(hierarchyRequestData);
        // For a level 1 hierarchy the deployment id will be the id in deployment table corresponding to input code
        if (newHierarchy.getLevel() == 1L) {
            // We cannot have 2 level 1 hierarchies for same deployment code
            Hierarchy level1HierarchyForDeployment = hierarchyRepository.findByClientIdAndDeploymentIdAndLevelAndActiveTrue(clientId, deploymentId, 1L);
            if (null != level1HierarchyForDeployment) {
                throw new ConflictException(ValidationStatusEnum.LEVEL_1_HIERARCHY_EXISTS_FOR_DEPLOYMENT);
            }
            // Set deployment Id
            newHierarchy.setDeploymentId(deploymentId);
        }
        if (null != hierarchyRequestData.getParentId()) {
            // Populate parent and ancestors data for hierarchy and copy deployment id from parent
            Hierarchy parentHierarchy = fetchHierarchyByIdFromDB(hierarchyRequestData.getParentId(), clientId);
            populateHierarchyTreeFromParent(newHierarchy, parentHierarchy);
        }
        // Enable hierarchy
        newHierarchy.setActive(true);
        // Set Client ID in hierarchy
        newHierarchy.setClientId(clientId);
        // Save hierarchy
        hierarchyRepository.save(newHierarchy);
        // Return Created Hierarchy to subscriber
        return HierarchyMapper.INSTANCE.hierarchyToHierarchyResponseData(newHierarchy);
    }

    /**
     * API to fetch Hierarchy details by Hierarchy ID
     *
     * @param hierarchyId - Hierarchy ID to be searched
     * @param clientId    - Client Id fetched from request
     * @return Hierarchy Details
     */
    @Override
    public HierarchyResponseData fetchHierarchyById(Long hierarchyId, Long clientId) {
        Hierarchy hierarchy = fetchHierarchyByIdFromDB(hierarchyId, clientId);
        return HierarchyMapper.INSTANCE.hierarchyToHierarchyResponseData(hierarchy);
    }

    /**
     * API to fetch Hierarchy details by Hierarchy ID along with details of its ancestors
     *
     * @param hierarchyId - Hierarchy ID to be searched
     * @param clientId    - Client Id fetched from request
     * @return Hierarchy and its ancestors
     */
    @Override
    public List<HierarchyResponse> fetchHierarchyByIdWithAncestors(Long hierarchyId, Long clientId) {
        List<HierarchyResponse> hierarchyList = new ArrayList<>();
        // Call recursive method to fetch current hierarchy details along with its ancestors
        addHierarchyWithAncestorsToList(hierarchyId, hierarchyList, clientId);
        // Return populated Hierarchy Response List
        return hierarchyList;
    }

    /**
     * API to fetch Hierarchy details by Hierarchy ID along with details of its descendants
     *
     * @param hierarchyId   - Hierarchy ID to be searched
     * @param fetchFullTree - If full tree of hierarchies to be fetched with input hierarchyId as root
     * @param clientId      - Client Id fetched from request
     * @return Hierarchy and it's descendants Details
     */
    @Override
    public List<HierarchyResponse> fetchHierarchyByIdWithDescendants(Long hierarchyId, boolean fetchFullTree, Long clientId) {
        List<HierarchyResponse> hierarchyResponseList = new ArrayList<>();
        // Fetch base hierarchy Details
        HierarchyResponseData baseHierarchy = fetchHierarchyById(hierarchyId, clientId);
        // Add base hierarchy to hierarchy list
        hierarchyResponseList.add(new HierarchyResponse(baseHierarchy));
        List<Hierarchy> childHierarchies;
        if (fetchFullTree) {
            // Fetch all descendants of base hierarchy
            childHierarchies = hierarchyRepository.findAllDescendants(baseHierarchy.getLevel(), hierarchyId).orElse(null);
        } else {
            // Fetch all immediate children of base hierarchy
            childHierarchies = hierarchyRepository.findByClientIdAndParentId(clientId, hierarchyId);
        }
        // If descendants found add them to hierarchy list
        if (!CollectionUtils.isEmpty(childHierarchies)) {
            childHierarchies.parallelStream().forEach(childHierarchy -> hierarchyResponseList.add(new HierarchyResponse(HierarchyMapper.INSTANCE.hierarchyToHierarchyResponseData(childHierarchy))));
        }
        return hierarchyResponseList;
    }


    /**
     * API to update existing Hierarchy, and it's associated configs and mappings in DB
     *
     * @param hierarchyUpdateData - Hierarchy details to be updated
     * @param clientId            - Client Id fetched from request
     * @return Updated Hierarchy details
     */
    @Override
    public HierarchyResponseData updateHierarchy(HierarchyUpdateData hierarchyUpdateData, Long clientId) {
        boolean isHierarchyDataUpdated = false;
        // Fetch Current Hierarchy details from DB
        Hierarchy currentHierarchyData = fetchHierarchyByIdFromDB(hierarchyUpdateData.getHierarchyId(), clientId);
        // Update Hierarchy Name if required
        String newName = hierarchyUpdateData.getName();
        if (null != newName && !newName.equalsIgnoreCase(currentHierarchyData.getName())) {
            currentHierarchyData.setName(newName);
            // Update the new name in ancestral data of descendant hierarchies
            updateAncestorNameOfDescendants(newName, currentHierarchyData.getLevel(), currentHierarchyData.getId());
            isHierarchyDataUpdated = true;
        }
        // Update parent hierarchy details if required
        Long newParentId = hierarchyUpdateData.getParentId();
        if (null != newParentId && !newParentId.equals(currentHierarchyData.getParentId())) {
            currentHierarchyData.setParentId(newParentId);
            // Populate parent and ancestors data for hierarchy
            Hierarchy parentHierarchy = fetchHierarchyByIdFromDB(newParentId, clientId);
            populateHierarchyTreeFromParent(currentHierarchyData, parentHierarchy);
            isHierarchyDataUpdated = true;
        }
        // Update Active flag if applicable
        if (null != hierarchyUpdateData.getActive() && hierarchyUpdateData.getActive() != currentHierarchyData.isActive()) {
            currentHierarchyData.setActive(hierarchyUpdateData.getActive());
            isHierarchyDataUpdated = true;
        }
        // Update Code if applicable
        if (null != hierarchyUpdateData.getCode() && !hierarchyUpdateData.getCode().equalsIgnoreCase(currentHierarchyData.getCode())) {
            currentHierarchyData.setCode(hierarchyUpdateData.getCode());
            isHierarchyDataUpdated = true;
        }
        // Save modified hierarchy details if required
        if (isHierarchyDataUpdated) {
            hierarchyRepository.save(currentHierarchyData);
        }
        // Return Updated Hierarchy details
        return HierarchyMapper.INSTANCE.hierarchyToHierarchyResponseData(currentHierarchyData);
    }

    /**
     * API to delete a particular Hierarchy from DB
     *
     * @param hierarchyId - Hierarchy to be deleted
     * @param clientId    - Client Id fetched from request
     * @return if delete hierarchy operation is successful
     */
    @Override
    public Boolean deleteHierarchy(Long hierarchyId, Long clientId) {
        // Fetch Hierarchy details from DB
        Hierarchy hierarchy = fetchHierarchyByIdFromDB(hierarchyId, clientId);
        // Check if Hierarchy is Active
        if (!hierarchy.isActive()) {
            // Hierarchy already inactive, throw error
            throw new NotFoundException(ValidationStatusEnum.HIERARCHY_NOT_ACTIVE);
        } else {
            // Set Active flag false for hierarchy
            hierarchy.setActive(false);
        }
        // Save hierarchy details
        hierarchyRepository.save(hierarchy);
        // Verify and return whether hierarchy is now inactive
        return !hierarchy.isActive();
    }

    /**
     * API to fetch all hierarchies by provided hierarchyId list
     *
     * @param hierarchyIdList - Input hierarchy id list
     * @param clientId        - Client Id fetched from request
     * @return List of hierarchies Response Data
     */
    @Override
    public List<HierarchyResponse> fetchAllHierarchies(List<Long> hierarchyIdList, Long clientId) {
        List<Hierarchy> hierarchies = hierarchyRepository.findByIdInAndClientIdAndActiveTrue(hierarchyIdList, clientId);
        if (hierarchyIdList.size() != hierarchies.size()) {
            throw new NotFoundException(ValidationStatusEnum.HIERARCHY_NOT_FOUND);
        }
        List<HierarchyResponse> hierarchiesResponseList = null;
        if (!CollectionUtils.isEmpty(hierarchies)) {
            hierarchiesResponseList = hierarchies.stream().map(hierarchy -> new HierarchyResponse(HierarchyMapper.INSTANCE.hierarchyToHierarchyResponseData(hierarchy))).collect(Collectors.toList());
        }
        return hierarchiesResponseList;
    }

    /**
     * API to fetch the lineage of all hierarchies by provided hierarchyId list
     *
     * @param hierarchyIdList - Input hierarchy id list
     * @param clientId        - Client Id fetched from request
     * @return List of hierarchies lineage data
     */
    @Override
    public List<HierarchyLineage> fetchAllHierarchiesLineages(List<Long> hierarchyIdList, Long clientId) {
        // Fetch the hierarchy details for all hierarchy ids
        List<HierarchyResponse> hierarchyResponseList = fetchAllHierarchies(hierarchyIdList, clientId);
        // Loop over the hierarchies to extract ancestral data
        return hierarchyResponseList.stream().map(hierarchyResponse -> {
            HierarchyResponseData baseHierarchy = hierarchyResponse.getHierarchy();
            // Create a map to store ancestral ids and level of each hierarchy
            Map<Long, Long> hierarchyLineageMap = new HashMap<>();
            hierarchyLineageMap.put(baseHierarchy.getId(), baseHierarchy.getLevel());
            if (null != baseHierarchy.getLevel1Id()) {
                hierarchyLineageMap.put(baseHierarchy.getLevel1Id(), 1L);
                if (null != baseHierarchy.getLevel2Id()) {
                    hierarchyLineageMap.put(baseHierarchy.getLevel2Id(), 2L);
                    if (null != baseHierarchy.getLevel3Id()) {
                        hierarchyLineageMap.put(baseHierarchy.getLevel3Id(), 3L);
                        if (null != baseHierarchy.getLevel4Id()) {
                            hierarchyLineageMap.put(baseHierarchy.getLevel4Id(), 4L);
                            if (null != baseHierarchy.getLevel5Id()) {
                                hierarchyLineageMap.put(baseHierarchy.getLevel5Id(), 5L);
                            }
                        }
                    }
                }
            }
            // Return ancestral lineage detail of hierarchy
            return new HierarchyLineage(baseHierarchy.getId(), hierarchyLineageMap);
        }).collect(Collectors.toList());
    }

    /**
     * Method to fetch hierarchy from DB By ID
     *
     * @param hierarchyId - Input Hierarchy ID
     * @param clientId    - Client Id fetched from request
     * @return Hierarchy data From DB
     */
    private Hierarchy fetchHierarchyByIdFromDB(Long hierarchyId, Long clientId) {
        Hierarchy hierarchy = hierarchyRepository.findByClientIdAndIdAndActiveTrue(clientId, hierarchyId).orElse(null);
        if (null == hierarchy) {
            throw new NotFoundException(ValidationStatusEnum.HIERARCHY_NOT_FOUND);
        }
        return hierarchy;
    }

    /**
     * Recursive method to add base hierarchy and it's ancestors to List of hierarchies
     *
     * @param hierarchyId   - Hierarchy ID of current hierarchy
     * @param hierarchyList - List of hierarchies to add current hierarchy to
     * @param clientId      - Client Id fetched from request
     */
    private void addHierarchyWithAncestorsToList(Long hierarchyId, List<HierarchyResponse> hierarchyList, Long clientId) {
        // Fetch Hierarchy details
        HierarchyResponseData parentHierarchy = fetchHierarchyById(hierarchyId, clientId);
        // Store Hierarchy details in List
        hierarchyList.add(new HierarchyResponse(parentHierarchy));
        if (null != parentHierarchy.getParentId()) {
            // If Parent ID of hierarchy present then re-call same method to add parent details to list
            addHierarchyWithAncestorsToList(parentHierarchy.getParentId(), hierarchyList, clientId);
        }
    }

    /**
     * Method to populate the hierarchy trr for current hierarchy from the parent hierarchy
     *
     * @param hierarchy       - Current Hierarchy details
     * @param parentHierarchy - Parent hierarchy details
     */
    private void populateHierarchyTreeFromParent(Hierarchy hierarchy, Hierarchy parentHierarchy) {
        Long parentLevel = parentHierarchy.getLevel();
        if (Constants.MAXIMUM_SUPPORTED_HIERARCHY_LEVEL.equals(parentLevel)) {
            throw new ValidationException(ValidationStatusEnum.CANNOT_CREATE_CHILD_HIERARCHY);
        }
        if (hierarchy.getLevel() <= parentLevel) {
            throw new ValidationException(ValidationStatusEnum.CHILD_HIERARCHY_LEVEL_GREATER_THAN_PARENT);
        }
        // Populate ancestors data from parent
        copyAncestorsHierarchyDataFromParent(hierarchy, parentHierarchy);
        // Populate direct parent data
        copyParentHierarchyData(hierarchy, parentHierarchy);
        // If parent hierarchy does not have has children true then update it
        if (!parentHierarchy.isHasChildren()) {
            parentHierarchy.setHasChildren(true);
            hierarchyRepository.save(parentHierarchy);
        }
    }

    /**
     * Method to copy ancestral hierarchy data from parent hierarchy to current hierarchy
     *
     * @param hierarchy       - Current Hierarchy being added/modified
     * @param parentHierarchy - Parent Hierarchy Data
     */
    private void copyAncestorsHierarchyDataFromParent(Hierarchy hierarchy, Hierarchy parentHierarchy) {
        // Copy Level 1 data from parent if present
        if (null != parentHierarchy.getLevel1Id()) {
            hierarchy.setLevel1Id(parentHierarchy.getLevel1Id());
            hierarchy.setLevel1Code(parentHierarchy.getLevel1Code());
            hierarchy.setLevel1Name(parentHierarchy.getLevel1Name());
            hierarchy.setLevel1Type(parentHierarchy.getLevel1Type());
        }
        // Copy Level 2 data from parent if present
        if (null != parentHierarchy.getLevel2Id()) {
            hierarchy.setLevel2Id(parentHierarchy.getLevel2Id());
            hierarchy.setLevel2Code(parentHierarchy.getLevel2Code());
            hierarchy.setLevel2Name(parentHierarchy.getLevel2Name());
            hierarchy.setLevel2Type(parentHierarchy.getLevel2Type());
        }
        // Copy Level 3 data from parent if present
        if (null != parentHierarchy.getLevel3Id()) {
            hierarchy.setLevel3Id(parentHierarchy.getLevel3Id());
            hierarchy.setLevel3Code(parentHierarchy.getLevel3Code());
            hierarchy.setLevel3Name(parentHierarchy.getLevel3Name());
            hierarchy.setLevel3Type(parentHierarchy.getLevel3Type());
        }
        // Copy Level 4 data from parent if present
        if (null != parentHierarchy.getLevel4Id()) {
            hierarchy.setLevel4Id(parentHierarchy.getLevel4Id());
            hierarchy.setLevel4Code(parentHierarchy.getLevel4Code());
            hierarchy.setLevel4Name(parentHierarchy.getLevel4Name());
            hierarchy.setLevel4Type(parentHierarchy.getLevel4Type());
        }
    }

    /**
     * Method to copy parent hierarchy data to child
     *
     * @param hierarchy       - Current hierarchy being added/updated
     * @param parentHierarchy - Parent hierarchy data
     */
    private void copyParentHierarchyData(Hierarchy hierarchy, Hierarchy parentHierarchy) {
        // Get Parent Hierarchy Level
        Long parentLevel = parentHierarchy.getLevel();
        // Copy parent's data directly to corresponding level of base hierarchy
        if (1L == parentLevel) {
            hierarchy.setLevel1Id(parentHierarchy.getId());
            hierarchy.setLevel1Code(parentHierarchy.getCode());
            hierarchy.setLevel1Name(parentHierarchy.getName());
            hierarchy.setLevel1Type(parentHierarchy.getType());
        } else if (2L == parentLevel) {
            hierarchy.setLevel2Id(parentHierarchy.getId());
            hierarchy.setLevel2Code(parentHierarchy.getCode());
            hierarchy.setLevel2Name(parentHierarchy.getName());
            hierarchy.setLevel2Type(parentHierarchy.getType());
        } else if (3L == parentLevel) {
            hierarchy.setLevel3Id(parentHierarchy.getId());
            hierarchy.setLevel3Code(parentHierarchy.getCode());
            hierarchy.setLevel3Name(parentHierarchy.getName());
            hierarchy.setLevel3Type(parentHierarchy.getType());
        } else if (4L == parentLevel) {
            hierarchy.setLevel4Id(parentHierarchy.getId());
            hierarchy.setLevel4Code(parentHierarchy.getCode());
            hierarchy.setLevel4Name(parentHierarchy.getName());
            hierarchy.setLevel4Type(parentHierarchy.getType());
        } else if (5L == parentLevel) {
            hierarchy.setLevel5Id(parentHierarchy.getId());
            hierarchy.setLevel5Code(parentHierarchy.getCode());
            hierarchy.setLevel5Name(parentHierarchy.getName());
            hierarchy.setLevel5Type(parentHierarchy.getType());
        }
        // Copy parent deploymentId to current Hierarchy
        hierarchy.setDeploymentId(parentHierarchy.getDeploymentId());
    }

    /**
     * Method to fetch all descendants of a hierarchy and update the name of their ancestor as per ancestor's level
     *
     * @param newName    - New name of ancestor hierarchy
     * @param level      - Level of ancestor hierarchy
     * @param ancestorId - ID of ancestor Hierarchy
     */
    private void updateAncestorNameOfDescendants(String newName, Long level, Long ancestorId) {
        List<Hierarchy> allDescendants = hierarchyRepository.findAllDescendants(level, ancestorId).orElse(null);
        if (!CollectionUtils.isEmpty(allDescendants)) {
            switch (level.intValue()) {
                case 1:
                    allDescendants.forEach(descendant -> descendant.setLevel1Name(newName));
                    break;
                case 2:
                    allDescendants.forEach(descendant -> descendant.setLevel2Name(newName));
                    break;
                case 3:
                    allDescendants.forEach(descendant -> descendant.setLevel3Name(newName));
                    break;
                case 4:
                    allDescendants.forEach(descendant -> descendant.setLevel4Name(newName));
                    break;
                case 5:
                    allDescendants.forEach(descendant -> descendant.setLevel5Name(newName));
                    break;
                default:
                    break;
            }
            hierarchyRepository.saveAll(allDescendants);
        }
    }

    /**
     * API to fetch Hierarchy Summary by Hierarchy ID
     *
     * @param hierarchyId - Hierarchy ID to be searched
     * @param clientId    - Client Id fetched from request
     * @return Hierarchy Summary Information including cases within
     */
    @Override
    public HierarchySummaryResponseData fetchHierarchySummaryById(Long hierarchyId, Long clientId) {
        Hierarchy hierarchy = fetchHierarchyByIdFromDB(hierarchyId, clientId);
        HierarchySummaryResponseData summaryResponseData = HierarchyMapper.INSTANCE.hierarchyToHierarchySummaryResponseData(hierarchy);

        Long children = 0L;
        if(hierarchy.isHasChildren()) {
            children = hierarchyRepository.countByClientIdAndParentId(clientId, hierarchyId);
        }
        summaryResponseData.setChildren(children);
        // Exclude the current node when calculating siblings
        Long siblings = 0L;
        if(null != hierarchy.getParentId()) {
            siblings = hierarchyRepository.countByClientIdAndParentId(clientId, hierarchy.getParentId()) - 1;
        }
        summaryResponseData.setSiblings(siblings);

        //Todo: Query hm_initiaion and hm_residence through episode id
        summaryResponseData.setCases(0L);

        //Todo: Populate data for monitoring method configuration

        return summaryResponseData;
    }

    /**
     * API to validate if the hierarchyId2 passed is within the jurisdiction of hierarchyId1
     *
     * @param rootHierarchyId - Hierarchy Id under which the other should contain
     * @param clientId - Client Id under which the hierarchy would be contained
     * @param supposedChildHierarchyId - Hierarchy Id which is to be contained
     * @return Boolean response of whether it is contained.
     */
    @Override
    public boolean validateIsDescendant(Long rootHierarchyId, Long clientId, Long supposedChildHierarchyId) {
        boolean isValidDescendant = false;
        if(rootHierarchyId.equals(supposedChildHierarchyId)) {
            isValidDescendant = true;
        } else {
            isValidDescendant = isValidStrictDescendant(rootHierarchyId, clientId, supposedChildHierarchyId);
        }
        return isValidDescendant;
    }

    private boolean isValidStrictDescendant(Long tophierarchyId, Long clientId, Long supposedChildHierarchyId) {
        boolean isValidDescendant = false;
        Hierarchy topHierarchy = fetchHierarchyByIdFromDB(tophierarchyId, clientId);
        Hierarchy suspectedChildHierarchy = fetchHierarchyByIdFromDB(supposedChildHierarchyId, clientId);
        if(topHierarchy.getLevel() < suspectedChildHierarchy.getLevel()) {
            switch (topHierarchy.getLevel().intValue()) {
                case 1:
                    isValidDescendant = suspectedChildHierarchy.getLevel1Id().equals(tophierarchyId);
                    break;
                case 2:
                    isValidDescendant = suspectedChildHierarchy.getLevel2Id().equals(tophierarchyId);
                    break;
                case 3:
                    isValidDescendant = suspectedChildHierarchy.getLevel3Id().equals(tophierarchyId);
                    break;
                case 4:
                    isValidDescendant = suspectedChildHierarchy.getLevel4Id().equals(tophierarchyId);
                    break;
                case 5:
                    isValidDescendant = suspectedChildHierarchy.getLevel5Id().equals(tophierarchyId);
                    break;
                default:
                    isValidDescendant = false;
            }
        }
        return isValidDescendant;
    }

    public HierarchySummaryResponseData userUpdateHierarchy(UserUpdateHierarchyRequest userUpdateHierarchyRequest, Long clientId) {
        HierarchyUpdateData hierarchyUpdateData = HierarchyMapper.INSTANCE.userUpdateHierarchyRequestToHierarchyUpdateData(userUpdateHierarchyRequest);
        HierarchyResponseData responseData = updateHierarchy(hierarchyUpdateData, clientId);

        HierarchySummaryResponseData summaryResponseData = HierarchyMapper.INSTANCE.hierarchyResponseDataToHierarchySummaryResponseData(responseData);

        //Todo: Update the hierarchy configuration based data (hasMERM, hasVOT, has99D, has99DL, hasNone) before returning response
        return summaryResponseData;
    }

    @Override
    public UnifiedListFilterResponse addHierarchyOptionsToUppFilter(Long hierarchyId, Long clientId, UnifiedListFilterResponse filterResponse) {
        Hierarchy requestedHierarchy = fetchHierarchyByIdFromDB(hierarchyId, clientId);
        List<List<MinHierarchyDetails>> hierarchyOptionsList = getHierarchyOptionsListWithParents(requestedHierarchy);
        Optional<List<Hierarchy>> childrenHierarchyOptional = hierarchyRepository.findAllDescendants(requestedHierarchy.getLevel(), hierarchyId);
        if(childrenHierarchyOptional.isPresent()) {
            List<Hierarchy> childrenHierarchyList = childrenHierarchyOptional.get();
            Map<Long, List<MinHierarchyDetails>> childrenHierarchyListGroupedByLevel = childrenHierarchyList.parallelStream().map(hierarchy -> {
                MinHierarchyDetails hierarchyDetails = new MinHierarchyDetails(hierarchy.getId(), hierarchy.getName(), hierarchy.getCode(), hierarchy.getType(), hierarchy.getLevel(), hierarchy.getParentId());
                return hierarchyDetails;
            }).collect(groupingBy(MinHierarchyDetails::getLevel));
            hierarchyOptionsList.addAll(childrenHierarchyListGroupedByLevel.values());
        }
        filterResponse.setHierarchyOptions(hierarchyOptionsList);
        return filterResponse;
    }

    private List<List<MinHierarchyDetails>> getHierarchyOptionsListWithParents(Hierarchy requestedHierarchy) {
        List<List<MinHierarchyDetails>> hierarchyOptionsList = new ArrayList<>();
        if (requestedHierarchy.getLevel() > 1) {
            hierarchyOptionsList.add(Arrays.asList(new MinHierarchyDetails(requestedHierarchy.getLevel1Id(), requestedHierarchy.getLevel1Name(), requestedHierarchy.getLevel1Code(), requestedHierarchy.getLevel1Type(), null, null)));
        }
        if (requestedHierarchy.getLevel() > 2) {
            hierarchyOptionsList.add(Arrays.asList(new MinHierarchyDetails(requestedHierarchy.getLevel2Id(), requestedHierarchy.getLevel2Name(), requestedHierarchy.getLevel2Code(), requestedHierarchy.getLevel2Type(), null, requestedHierarchy.getLevel1Id())));
        }
        if (requestedHierarchy.getLevel() > 3) {
            hierarchyOptionsList.add(Arrays.asList(new MinHierarchyDetails(requestedHierarchy.getLevel3Id(), requestedHierarchy.getLevel3Name(), requestedHierarchy.getLevel3Code(), requestedHierarchy.getLevel3Type(), null, requestedHierarchy.getLevel2Id())));
        }
        if (requestedHierarchy.getLevel() > 4) {
            hierarchyOptionsList.add(Arrays.asList(new MinHierarchyDetails(requestedHierarchy.getLevel4Id(), requestedHierarchy.getLevel4Name(), requestedHierarchy.getLevel4Code(), requestedHierarchy.getLevel4Type(), null, requestedHierarchy.getLevel3Id())));
        }
        if (requestedHierarchy.getLevel() > 5) {
            hierarchyOptionsList.add(Arrays.asList(new MinHierarchyDetails(requestedHierarchy.getLevel5Id(), requestedHierarchy.getLevel5Name(), requestedHierarchy.getLevel5Code(), requestedHierarchy.getLevel5Type(), null, requestedHierarchy.getLevel5Id())));
        }
        hierarchyOptionsList.add(Arrays.asList(new MinHierarchyDetails(requestedHierarchy.getId(), requestedHierarchy.getName(), requestedHierarchy.getCode(), requestedHierarchy.getType(), null, requestedHierarchy.getParentId())));
        return hierarchyOptionsList;
    }
}
