package com.everwell.registryservice.service.impl;

import com.everwell.registryservice.constants.AuthConstants;
import com.everwell.registryservice.constants.Constants;
import com.everwell.registryservice.enums.Routes;
import com.everwell.registryservice.exceptions.ValidationException;
import com.everwell.registryservice.models.dto.GenericExchange;
import com.everwell.registryservice.models.dto.ParameterExchange;
import com.everwell.registryservice.models.http.response.Response;
import com.everwell.registryservice.service.DataGatewayService;
import com.everwell.registryservice.utils.Utils;
import com.fasterxml.jackson.core.type.TypeReference;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.*;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

@Service
public class DataGatewayServiceImpl implements DataGatewayService {

    public RestTemplate restTemplateForGateway = new RestTemplate();

    protected static HttpEntity httpEntity;

    private static String AUTH_TOKEN = "";

    @Value("${dataGateway.url}")
    public String dataGatewayURL;

    @Value("${dataGateway.username}")
    public String username;

    @Value("${dataGateway.password}")
    public String password;

    @Override
    public HttpHeaders headers(String clientId) {
        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.add(AuthConstants.AUTH_HEADER_AUTHORIZATION, AuthConstants.AUTH_TOKEN_BEARER_PREFIX + AUTH_TOKEN);
        httpHeaders.add(Constants.ACCEPT, MediaType.APPLICATION_JSON.toString());
        httpHeaders.add(Constants.CONTENT_TYPE, MediaType.APPLICATION_JSON.toString());
        httpHeaders.add(Constants.CLIENT_ID, clientId);
        return httpHeaders;
    }

    @Override
    public void authenticate() {
        String endPoint = Routes.TOKEN.getRoute();
        Map<String, String> map = new HashMap<>();
        map.put(AuthConstants.USERNAME, username);
        map.put(AuthConstants.PASSWORD, password);
        HttpHeaders headers = new HttpHeaders();
        headers.add(Constants.ACCEPT, MediaType.APPLICATION_JSON.toString());
        headers.add(Constants.CONTENT_TYPE, MediaType.APPLICATION_JSON.toString());
        httpEntity = new HttpEntity<>(Utils.asJsonString(map), headers);
        ResponseEntity<Map> responseEntity = restTemplateForGateway.exchange(dataGatewayURL + endPoint, HttpMethod.POST, httpEntity, Map.class);

        if (responseEntity.getStatusCode() == HttpStatus.OK) {
            AUTH_TOKEN = responseEntity.getBody().get(Constants.DATA).toString();
        }
    }

    @Override
    public <T> ResponseEntity<Response<T>> genericExchange(GenericExchange exchange) {
        ResponseEntity response = null;
        int retryCount = 3;
        boolean retry = false;
        do {
            try {
                HttpHeaders httpHeaders = headers(exchange.getClientId());
                HttpEntity requestEntity = new HttpEntity<>(exchange.getBody(), httpHeaders);
                response = restTemplateForGateway.exchange(exchange.getUrl(), exchange.getMethod(), requestEntity, exchange.getTypeReference());
                retry = false;
            } catch (HttpClientErrorException ex) {
                if (ex.getStatusCode() == HttpStatus.FORBIDDEN) {
                    authenticate();
                    retry = true;
                } else {
                    try {
                        Response<T> failedResponse = Utils.jsonToObject(ex.getResponseBodyAsString(), new TypeReference<Response<T>>() {
                        });
                        return new ResponseEntity<>(failedResponse, ex.getStatusCode());
                    } catch (IOException e) {
                        throw new ValidationException("Invalid response type received for " + exchange.getUrl());
                    }
                }
            }
        } while (retry && retryCount-- > 0);
        return response;
    }

    @Override
    public <T> ResponseEntity<Response<T>> getExchange(ParameterExchange exchange) {
        String requestUrl = dataGatewayURL + exchange.getEndPoint();
        UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(requestUrl);
        if (exchange.getPathVariable() != null) {
            builder.path("/" + exchange.getPathVariable());
        }
        if (!CollectionUtils.isEmpty(exchange.getQueryParams())) {
            exchange.getQueryParams().forEach(builder::queryParam);
        }
        GenericExchange genericExchangeDto = new GenericExchange(exchange.getClientId(), builder.toUriString(), HttpMethod.GET, exchange.getTypeReference());
        return genericExchange(genericExchangeDto);
    }

    @Override
    public <T> ResponseEntity<Response<T>> deleteExchange(ParameterExchange exchange) {
        String requestUrl = dataGatewayURL + exchange.getEndPoint();
        UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(requestUrl);
        if (exchange.getPathVariable() != null) {
            builder.path("/" + exchange.getPathVariable());
        }
        GenericExchange genericExchangeDto = new GenericExchange(exchange.getClientId(), builder.toUriString(), HttpMethod.DELETE, exchange.getTypeReference());
        return genericExchange(genericExchangeDto);
    }

    @Override
    public <T> ResponseEntity<Response<T>> postExchange(ParameterExchange exchange) {
        String requestUrl = dataGatewayURL + exchange.getEndPoint();
        UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(requestUrl);
        GenericExchange genericExchangeDto = new GenericExchange(exchange.getClientId(), builder.toUriString(), HttpMethod.POST, exchange.getTypeReference(), exchange.getBody());
        return genericExchange(genericExchangeDto);
    }

    @Override
    public <T> ResponseEntity<Response<T>> putExchange(ParameterExchange exchange) {
        String requestUrl = dataGatewayURL + exchange.getEndPoint();
        UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(requestUrl);
        GenericExchange genericExchangeDto = new GenericExchange(exchange.getClientId(), builder.toUriString(), HttpMethod.PUT, exchange.getTypeReference(), exchange.getBody());
        return genericExchange(genericExchangeDto);
    }
}
