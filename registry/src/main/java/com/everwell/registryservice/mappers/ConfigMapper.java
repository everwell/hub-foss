package com.everwell.registryservice.mappers;

import com.everwell.registryservice.models.db.Config;
import com.everwell.registryservice.models.db.HierarchyConfigMap;
import com.everwell.registryservice.models.http.requests.AddConfigRequest;
import com.everwell.registryservice.models.http.response.ConfigHierarchyMappingRecordResponse;
import com.everwell.registryservice.models.http.response.ConfigHierarchyMappingBaseResponse;
import com.everwell.registryservice.models.http.response.ConfigResponse;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

@Mapper
public interface ConfigMapper {

    ConfigMapper INSTANCE = Mappers.getMapper(ConfigMapper.class);

    ConfigResponse configToConfigResponse(Config config);

    Config addConfigRequestToConfig(AddConfigRequest addConfigRequest);

    ConfigHierarchyMappingRecordResponse hierarchyConfigMapToConfigHierarchyMappingRecordResponse(HierarchyConfigMap hierarchyConfigMap);

    @Mapping(target = "configId", source = "configMappingId")
    ConfigHierarchyMappingBaseResponse hierarchyConfigMapToConfigHierarchyMappingBaseResponse(HierarchyConfigMap hierarchyConfigMap);
}
