package com.everwell.registryservice.mappers;

import com.everwell.registryservice.models.db.UserAccess;
import com.everwell.registryservice.models.dto.UserDetails;
import com.everwell.registryservice.models.http.requests.CreateUserRequest;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

@Mapper
public interface UserMapper {

    UserMapper INSTANCE = Mappers.getMapper(UserMapper.class);

    UserAccess createUserRequestToUserAccess(CreateUserRequest createUserRequest);

    UserDetails userAccessToUserDetails(UserAccess userAccess);
}
