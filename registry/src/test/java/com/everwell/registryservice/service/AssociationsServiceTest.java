package com.everwell.registryservice.service;

import com.everwell.registryservice.BaseTest;
import com.everwell.registryservice.constants.ValidationStatusEnum;
import com.everwell.registryservice.exceptions.ValidationException;
import com.everwell.registryservice.models.db.AssociationsMaster;
import com.everwell.registryservice.models.db.HierarchyAssociations;
import com.everwell.registryservice.models.dto.HierarchyResponseData;
import com.everwell.registryservice.models.http.response.HierarchyResponse;
import com.everwell.registryservice.repository.AssociationsMasterRepository;
import com.everwell.registryservice.repository.HierarchyAssociationsRepository;
import com.everwell.registryservice.service.impl.AssociationsServiceImpl;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.junit.Assert.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

public class AssociationsServiceTest extends BaseTest {
    private final Long testHierarchyId = 10L;
    private final Long testAssociationId = 1L;
    private final String testAssociationType = "testType";
    private final String testAssociationValue = "testValue";
    @Spy
    @InjectMocks
    private AssociationsServiceImpl associationsService;
    @Mock
    private AssociationsMasterRepository associationsMasterRepository;
    @Mock
    private HierarchyAssociationsRepository hierarchyAssociationsRepository;

    @Test
    public void validateInputAssociations_validationFailure_Error() {
        when(associationsMasterRepository.countByTypeIn(any(List.class))).thenReturn(0);
        ValidationException validationException = assertThrows(ValidationException.class, () -> associationsService.validateInputAssociations(getInputAssociationsMap()));
        assertEquals(ValidationStatusEnum.ASSOCIATIONS_ASSOCIATED_WITH_HIERARCHY_DOES_NOT_EXIST.getMessage(), validationException.getMessage());
    }

    @Test
    public void validateInputAssociations_validationSuccess() {
        when(associationsMasterRepository.countByTypeIn(any(List.class))).thenReturn(1);
        associationsService.validateInputAssociations(getInputAssociationsMap());
    }

    @Test
    public void addHierarchyAssociationsTest() {
        when(associationsMasterRepository.findByTypeIn(any(List.class))).thenReturn(getLimitedMasterAssociationData());
        Map<String, Object> inputAssociationsMap = getInputAssociationsMap();
        Map<String, String> mappedHierarchyAssociations = associationsService.addHierarchyAssociations(testHierarchyId, inputAssociationsMap);
        assertEquals(inputAssociationsMap.size(), mappedHierarchyAssociations.size());
    }

    @Test
    public void updateHierarchyAssociationsTest() {
        when(associationsMasterRepository.findAll()).thenReturn(getMasterAssociationData());
        Map<String, Object> inputAssociationsMap = getInputAssociationsMap();
        int numberOfNewMappings = inputAssociationsMap.size();
        inputAssociationsMap.putAll(getInputAssociationsMapForExistingMapping());
        List<HierarchyAssociations> hierarchyAssociations = getHierarchyAssociations();
        int numberOfCurrentMappings = getHierarchyAssociations().size();
        when(hierarchyAssociationsRepository.getByHierarchyId(testHierarchyId)).thenReturn(hierarchyAssociations);
        Map<String, String> mappedHierarchyAssociations = associationsService.updateHierarchyAssociations(testHierarchyId, inputAssociationsMap);
        assertEquals(numberOfNewMappings + numberOfCurrentMappings, mappedHierarchyAssociations.size());
        assertEquals(testAssociationValue + 100, mappedHierarchyAssociations.get(testAssociationType + 21));
    }

    @Test
    public void fetchHierarchyAssociationsTest() {
        when(associationsMasterRepository.findByIdIn(any(List.class))).thenReturn(getLimitedMasterAssociationData());
        when(hierarchyAssociationsRepository.getByHierarchyId(testHierarchyId)).thenReturn(getHierarchyAssociations());
        associationsService.fetchHierarchyAssociations(testHierarchyId);
    }

    @Test
    public void populateAssociationsForHierarchiesTest() {
        when(associationsMasterRepository.findAll()).thenReturn(getMasterAssociationData());
        when(hierarchyAssociationsRepository.getByHierarchyId(any(Long.class))).thenReturn(getHierarchyAssociations());
        List<HierarchyResponse> hierarchyResponseList = getInputHierarchyResponseList();
        associationsService.populateAssociationsForHierarchies(hierarchyResponseList);
        hierarchyResponseList.forEach(hierarchyResponse -> assertNotNull(hierarchyResponse.getAssociations()));
    }

    private Map<String, Object> getInputAssociationsMap() {
        Map<String, Object> inputAssociationsMap = new HashMap<>();
        inputAssociationsMap.put(testAssociationType, testAssociationValue);
        return inputAssociationsMap;
    }

    private Map<String, Object> getInputAssociationsMapForExistingMapping() {
        Map<String, Object> inputAssociationsMap = new HashMap<>();
        inputAssociationsMap.put(testAssociationType + 21, testAssociationValue + 100);
        return inputAssociationsMap;
    }

    private List<HierarchyResponse> getInputHierarchyResponseList() {
        List<HierarchyResponse> hierarchyResponseList = new ArrayList<>();
        for (long i = 1; i <= 5; i++) {
            HierarchyResponseData hierarchyResponseData = new HierarchyResponseData();
            hierarchyResponseData.setId(i);
            hierarchyResponseList.add(new HierarchyResponse(hierarchyResponseData));
        }
        return hierarchyResponseList;
    }

    private List<AssociationsMaster> getLimitedMasterAssociationData() {
        List<AssociationsMaster> masterDataList = new ArrayList<>(1);
        masterDataList.add(new AssociationsMaster(testAssociationId, testAssociationType));
        return masterDataList;
    }

    private List<AssociationsMaster> getMasterAssociationData() {
        List<AssociationsMaster> masterDataList = new ArrayList<>();
        masterDataList.add(new AssociationsMaster(1L, testAssociationType));
        for (long i = 2; i <= 30; i++) {
            masterDataList.add(new AssociationsMaster(i, testAssociationType + i));
        }
        return masterDataList;
    }

    private List<HierarchyAssociations> getHierarchyAssociations() {
        List<HierarchyAssociations> hierarchyAssociationsList = new ArrayList<>();
        for (long i = 20; i <= 25; i++) {
            hierarchyAssociationsList.add(new HierarchyAssociations(i, testHierarchyId, i, testAssociationValue + 1));
        }
        return hierarchyAssociationsList;
    }
}
