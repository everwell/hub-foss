package com.everwell.registryservice.service;

import com.everwell.registryservice.BaseTest;
import com.everwell.registryservice.constants.AuthConstants;
import com.everwell.registryservice.constants.Constants;
import com.everwell.registryservice.constants.ValidationStatusEnum;
import com.everwell.registryservice.exceptions.ConflictException;
import com.everwell.registryservice.exceptions.NotFoundException;
import com.everwell.registryservice.exceptions.UnauthorizedException;
import com.everwell.registryservice.models.db.*;
import com.everwell.registryservice.models.dto.SidebarItemResponse;
import com.everwell.registryservice.models.dto.UnifiedListFilterResponse;
import com.everwell.registryservice.models.dto.UserSummaryDto;
import com.everwell.registryservice.models.http.requests.CreateUserRequest;
import com.everwell.registryservice.models.http.response.HeaderResponse;
import com.everwell.registryservice.models.http.response.adherence.AdherenceTechnologyDto;
import com.everwell.registryservice.repository.SidebarItemRepository;
import com.everwell.registryservice.repository.SidebarPermissionRepository;
import com.everwell.registryservice.repository.UserAccessHierarchyMapRepository;
import com.everwell.registryservice.repository.UserAccessRepository;
import com.everwell.registryservice.service.impl.UserServiceImpl;
import com.everwell.registryservice.validators.UserRequestValidator;
import io.sentry.event.User;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.Spy;
import org.springframework.data.util.Pair;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;

import java.util.*;

import static org.junit.Assert.*;
import static org.mockito.Mockito.when;

public class UserServiceTest extends BaseTest {
    private final Long testUserId = 1L;
    private final String testUserName = "testUser";
    private final String testCountryName = "testCountry";
    private final Long testSsoId = 123L;
    private final Long testDeploymentId = 3L;
    private final Long testClientId = 23L;
    private final Long testHierarchyMappingId = 123456L;

    @Spy
    @InjectMocks
    private UserServiceImpl userService;

    @Mock
    private UserAccessRepository userAccessRepository;

    @Mock
    private UserAccessHierarchyMapRepository userAccessHierarchyMapRepository;

    @Mock
    private SidebarItemRepository sidebarItemRepository;

    @Mock
    private SidebarPermissionRepository sidebarPermissionrepository;

    @Mock
    private UserRequestValidator userRequestValidator;

    @Mock
    private SecurityContextHolder fauxContextHolder;


    @Test
    public void addNewUserTest() {
        UserAccess userDetails = userService.addNewUser(getValidCreateUserRequest(), testClientId);
        assertTrue(userDetails.isActive());
        assertEquals(testUserName, userDetails.getUsername());
        assertEquals(Optional.of(testSsoId), Optional.of(userDetails.getSsoId()));
        assertEquals(Optional.of(testHierarchyMappingId), Optional.of(userDetails.getHierarchyId()));
        assertEquals(Optional.of(testClientId), Optional.of(userDetails.getClientId()));
    }

    @Test
    public void fetchUserData_UserNotExist_Exception() {
        when(userAccessRepository.findBySsoId(testSsoId)).thenReturn(Optional.empty());
        NotFoundException notFoundException = assertThrows(NotFoundException.class, () -> userService.fetchUserData(testSsoId));
        assertEquals(ValidationStatusEnum.USER_NOT_FOUND.getMessage(), notFoundException.getMessage());
    }

    @Test
    public void fetchUserData_UserExist_Success() {
        when(userAccessRepository.findBySsoId(testSsoId)).thenReturn(Optional.of(getValidUserData()));
        UserAccess userAccess = userService.fetchUserData(testSsoId);
        assertEquals(testUserName, userAccess.getUsername());
        assertEquals(Optional.of(testSsoId), Optional.of(userAccess.getSsoId()));
        assertEquals(Optional.of(testHierarchyMappingId), Optional.of(userAccess.getHierarchyId()));
    }

    @Test
    public void fetchHierarchiesAssociatedWithUser_NoMappingsFound_EmptyResponse() {
        when(userAccessHierarchyMapRepository.findByUserId(testUserId)).thenReturn(Collections.emptyList());
        List<Long> hierarchyResponses = userService.fetchHierarchiesAssociatedWithUser(testUserId);
        assertNull(hierarchyResponses);
    }

    @Test
    public void fetchHierarchiesAssociatedWithUser_ValidData_Success() {
        when(userAccessHierarchyMapRepository.findByUserId(testUserId)).thenReturn(Collections.singletonList(new UserAccessHierarchyMap()));
        List<Long> hierarchyResponses = userService.fetchHierarchiesAssociatedWithUser(testUserId);
        assertEquals(1, hierarchyResponses.size());
    }

    @Test
    public void deleteUser_userNotExist_Error() {
        when(userAccessRepository.findBySsoId(testSsoId)).thenReturn(Optional.empty());
        NotFoundException notFoundException = assertThrows(NotFoundException.class, () -> userService.deleteUser(testSsoId));
        assertEquals(ValidationStatusEnum.USER_NOT_FOUND.getMessage(), notFoundException.getMessage());
    }

    @Test
    public void deleteUser_userInactive_Error() {
        when(userAccessRepository.findBySsoId(testSsoId)).thenReturn(Optional.of(getValidUserData()));
        ConflictException conflictException = assertThrows(ConflictException.class, () -> userService.deleteUser(testSsoId));
        assertEquals(ValidationStatusEnum.USER_ALREADY_INACTIVE.getMessage(), conflictException.getMessage());
    }

    @Test
    public void deleteUser_validUser_Success() {
        UserAccess validUserData = getValidUserData();
        validUserData.setActive(true);
        when(userAccessRepository.findBySsoId(testSsoId)).thenReturn(Optional.of(validUserData));
        assertTrue(userService.deleteUser(testSsoId));
    }

    @Test
    public void buildUserHeaderDetails_Success() {
        HeaderResponse response = userService.buildUserHeaderDetails(getValidUserData(), new HeaderResponse());
        assertTrue(response.isUserLoggedIn());
        assertEquals(2, response.getDropdownItems().keySet().size());
        assertEquals(3, response.getSearchCriteria().keySet().size());
    }

    @Test(expected = NotFoundException.class)
    public void getUserClientByToken_anonymousUser_Error() {
        Authentication authentication = Mockito.mock(Authentication.class);
        SecurityContext securityContext = Mockito.mock(SecurityContext.class);
        Mockito.when(securityContext.getAuthentication()).thenReturn(authentication);
        Mockito.when(authentication.getPrincipal()).thenReturn(Constants.ANNONYMOUS_USER);
        SecurityContextHolder.setContext(securityContext);

        userService.getUserClientByToken();
    }

    @Test(expected = UnauthorizedException.class)
    public void getUserClientByToken_clientToken_Error() {
        String text = "Test";
        Client client = new Client(text, text);
        Authentication authentication = Mockito.mock(Authentication.class);
        SecurityContext securityContext = Mockito.mock(SecurityContext.class);
        Mockito.when(securityContext.getAuthentication()).thenReturn(authentication);
        Mockito.when(authentication.getPrincipal()).thenReturn(client);
        SecurityContextHolder.setContext(securityContext);

        userService.getUserClientByToken();
    }

    @Test
    public void getUserClientByToken_userToken_Success() {
        String text = "Test";
        Client client = new Client(text, text);
        UserAccess user = getValidUserData();
        Authentication authentication = Mockito.mock(Authentication.class);
        SecurityContext securityContext = Mockito.mock(SecurityContext.class);
        Mockito.when(securityContext.getAuthentication()).thenReturn(authentication);
        Mockito.when(authentication.getPrincipal()).thenReturn(client);
        Mockito.when(authentication.getCredentials()).thenReturn(user);
        SecurityContextHolder.setContext(securityContext);

        Pair<Client, UserAccess> userAccessClientPair = userService.getUserClientByToken();
        assertEquals(userAccessClientPair.getFirst(), client);
        assertEquals(userAccessClientPair.getSecond(), user);
    }

    @Test
    public void testSidebarFiltering_Success() {
        UserAccess userAccess = getValidUserData();
        Deployment deployment = getValidDeploymentData();
        when(sidebarItemRepository.findAll()).thenReturn(getAllSidebarItems());
        when(sidebarPermissionrepository.findByDeploymentIdAndDesignationAndIsActiveTrue(deployment.getId(), AuthConstants.DEFAULT_DESIGNATION)).thenReturn(getMappedSidebarPermissions());
        List<SidebarItemResponse> sidebarItems = userService.getSidebarItems(userAccess, deployment);

        assertEquals(sidebarItems.size(), 2);
    }

    @Test
    public void testSidebarUnmappedFiltering_Success() {
        UserAccess userAccess = getValidUserData();
        Deployment deployment = getValidDeploymentData();
        when(sidebarItemRepository.findAll()).thenReturn(getAllSidebarItems());
        when(sidebarPermissionrepository.findByDeploymentIdAndDesignationAndIsActiveTrue(deployment.getId(), AuthConstants.DEFAULT_DESIGNATION)).thenReturn(new ArrayList<>());
        when(sidebarPermissionrepository.findByDesignationAndIsActiveTrue(AuthConstants.DEFAULT_DESIGNATION)).thenReturn(getMappedSidebarPermissions());
        List<SidebarItemResponse> sidebarItems = userService.getSidebarItems(userAccess, deployment);

        assertEquals(sidebarItems.size(), 2);
    }

    @Test
    public void validateUppFilterOptions() {
        UserAccess userAccess = getValidUserData();
        UnifiedListFilterResponse response = userService.getUppFilterOptions(userAccess, Arrays.asList(new AdherenceTechnologyDto("key", "value", "options")));
        assertNotNull(response);
        assertEquals(1l, response.getTechnologyOptions().size());
        assertEquals(2l, response.getStageOptions().size());
    }

    @Test
    public void getUserSummary_NoLinkedLoginsSuccess() {
        when(userAccessRepository.findByHierarchyIdAndActiveTrue(testHierarchyMappingId)).thenReturn(new ArrayList<>());
        List<UserSummaryDto> summaryDtoList = userService.getUserSummaryResponseByHierarchyId(testHierarchyMappingId);
        assertNotNull(summaryDtoList);
        assertEquals(0, summaryDtoList.size());
    }

    @Test
    public void getUserSummary_AvailableLinkedLoginsSuccess() {
        List<UserAccess> userAccessList = getValidUserLoginData();
        when(userAccessRepository.findByHierarchyIdAndActiveTrue(testHierarchyMappingId)).thenReturn(userAccessList);
        List<UserSummaryDto> summaryDtoList = userService.getUserSummaryResponseByHierarchyId(testHierarchyMappingId);
        assertNotNull(summaryDtoList);
        assertEquals(2, summaryDtoList.size());
        assertEquals(userAccessList.get(0).getId(), summaryDtoList.get(0).getId().longValue());
        assertEquals(userAccessList.get(0).getUsername(), summaryDtoList.get(0).getText());
    }

    private CreateUserRequest getValidCreateUserRequest() {
        CreateUserRequest createUserRequest = new CreateUserRequest();
        createUserRequest.setUsername(testUserName);
        createUserRequest.setHierarchyId(testHierarchyMappingId);
        createUserRequest.setSsoId(testSsoId);
        return createUserRequest;
    }

    private UserAccess getValidUserData() {
        UserAccess userAccess = new UserAccess();
        userAccess.setUsername(testUserName);
        userAccess.setSsoId(testSsoId);
        userAccess.setHierarchyId(testHierarchyMappingId);
        return userAccess;
    }

    private List<UserAccess> getValidUserLoginData() {
        UserAccess userAccess = new UserAccess();
        userAccess.setUsername(testUserName);
        userAccess.setSsoId(testSsoId);
        userAccess.setHierarchyId(testHierarchyMappingId);

        UserAccess anotherUserAccess = new UserAccess();
        userAccess.setUsername(testUserName + "1");
        userAccess.setSsoId(testSsoId + '1');
        userAccess.setHierarchyId(testHierarchyMappingId);
        return Arrays.asList(userAccess, anotherUserAccess);
    }

    private Deployment getValidDeploymentData() {
        Deployment deployment = new Deployment();
        deployment.setCountryName(testCountryName);
        deployment.setId(testDeploymentId);
        return deployment;
    }

    private List<SidebarItem> getAllSidebarItems() {
        List<SidebarItem> sidebarItems = new ArrayList<>();
        sidebarItems.add(new SidebarItem(1L, "item1", "fa-fa-1", "http://item1.org", false));
        sidebarItems.add(new SidebarItem(2L, "item2", "fa-fa-2", "http://item2.org", true));
        sidebarItems.add(new SidebarItem(3L, "item3", "fa-fa-3", "http://item3.org", false));
        return sidebarItems;
    }

    private List<SidebarPermission> getMappedSidebarPermissions() {
        List<SidebarPermission> sidebarPermissions = new ArrayList<>();
        sidebarPermissions.add(new SidebarPermission(1L, testDeploymentId, AuthConstants.DEFAULT_DESIGNATION, 1L, true));
        sidebarPermissions.add(new SidebarPermission(21L, testDeploymentId, AuthConstants.DEFAULT_DESIGNATION, 2L, true));
        return sidebarPermissions;
    }
}
