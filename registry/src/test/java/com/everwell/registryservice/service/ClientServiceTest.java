package com.everwell.registryservice.service;


import com.everwell.registryservice.BaseTest;
import com.everwell.registryservice.exceptions.NotFoundException;
import com.everwell.registryservice.exceptions.UnauthorizedException;
import com.everwell.registryservice.exceptions.ValidationException;
import com.everwell.registryservice.models.db.Client;
import com.everwell.registryservice.models.http.requests.RegisterClientRequest;
import com.everwell.registryservice.models.http.response.ClientResponse;
import com.everwell.registryservice.repository.ClientRepository;
import com.everwell.registryservice.service.impl.ClientServiceImpl;
import com.everwell.registryservice.utils.JwtUtils;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.Spy;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.test.util.ReflectionTestUtils;

import java.util.Optional;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class ClientServiceTest extends BaseTest {

    @Spy
    JwtUtils jwtUtils;
    @Spy
    @InjectMocks
    private ClientServiceImpl clientService;
    @Mock
    private ClientRepository clientRepository;

    @Test(expected = ValidationException.class)
    public void testGetClientWithoutClientId() {
        clientService.getClient(null);
    }

    @Test(expected = NotFoundException.class)
    public void testGetClientNotFoundClient() {
        Long id = 1L;

        when(clientRepository.findById(id)).thenReturn(Optional.empty());

        clientService.getClient(id);
    }

    @Test
    public void testGetClientSuccess() {
        Long id = 1L;
        String text = "Test";
        Client client = new Client(text, text, null, "www.google.com");

        when(clientRepository.findById(id)).thenReturn(Optional.of((client)));

        clientService.getClient(id);
    }

    @Test
    public void testGetClientWithToken() {
        Long id = 1L;
        String text = "abcdefhijklmnopqrstuvwxyzabcdefhijklmnopqrstuvwxyzabcdefhijklmnopqrstuvwxyzabcdefhijklmnopqrstuvwxyzabcdefhijklmnopqrstuvwxyz";
        Client client = new Client(text, text, null, "www.google.com");

        jwtUtils.setSecretKey("sercret");
        when(clientRepository.findById(id)).thenReturn(Optional.of((client)));

        ClientResponse clientResponse = clientService.getClientWithToken(id);
        assertEquals(client.getName(), clientResponse.getName());
        assertEquals(client.getAskforhelpurl(), clientResponse.getAskForHelpUrl());
        verify(clientRepository, Mockito.times(1)).findById(any());
    }

    @Test(expected = ValidationException.class)
    public void testRegisterClientExistingClient() {
        String text = "Test";
        Client client = new Client(text, text, null, "www.google.com");
        RegisterClientRequest clientRequest = new RegisterClientRequest(text, text, 1L, "www.google.com");

        when(clientRepository.findByName(any())).thenReturn(client);

        clientService.registerClient(clientRequest);
    }

    @Test
    public void testRegisterClientSuccess() {
        String text = "Test";
        RegisterClientRequest clientRequest = new RegisterClientRequest(text, text, 1L, "www.google.com");

        when(clientRepository.findByName(any())).thenReturn(null);
        when(clientRepository.save(any())).thenReturn(null);

        ClientResponse clientResponse = clientService.registerClient(clientRequest);
        assertEquals(clientRequest.getName(), clientResponse.getName());
        assertEquals(clientRequest.getAskForHelpUrl(), clientResponse.getAskForHelpUrl());
        verify(clientRepository, Mockito.times(1)).findByName(any());
        verify(clientRepository, Mockito.times(1)).save(any());
    }

    @Test
    public void testGetClientByToken() {
        String text = "Test";
        Client client = new Client(text, text);
        Authentication authentication = Mockito.mock(Authentication.class);
        SecurityContext securityContext = Mockito.mock(SecurityContext.class);
        Mockito.when(securityContext.getAuthentication()).thenReturn(authentication);
        Mockito.when(authentication.getPrincipal()).thenReturn(client);
        SecurityContextHolder.setContext(securityContext);

        Client clientResponse = clientService.getClientByToken();
        assertEquals(clientResponse.getName(), text);
    }

    @Test(expected = UnauthorizedException.class)
    public void testGetNonExistingClientByToken() {
        Authentication authentication = Mockito.mock(Authentication.class);
        SecurityContext securityContext = Mockito.mock(SecurityContext.class);

        Mockito.when(securityContext.getAuthentication()).thenReturn(authentication);
        Mockito.when(authentication.getPrincipal()).thenReturn(null);

        clientService.getClientByToken();
    }

    @Test(expected = UnauthorizedException.class)
    public void testGetNonExistingClientRedirectUrl() {
        Authentication authentication = Mockito.mock(Authentication.class);
        SecurityContext securityContext = Mockito.mock(SecurityContext.class);

        Mockito.when(securityContext.getAuthentication()).thenReturn(authentication);
        Mockito.when(authentication.getPrincipal()).thenReturn(null);

        clientService.getRedirectUrl("returnUrl");
    }

    @Test
    public void testRedirectUrl_Success() {
        String serverUrl = "https://sample.com";
        Client client = new Client(clientId_Hub, "test", "test", null);
        client.setSsoUrl(serverUrl);
        Authentication authentication = Mockito.mock(Authentication.class);
        SecurityContext securityContext = Mockito.mock(SecurityContext.class);
        Mockito.when(securityContext.getAuthentication()).thenReturn(authentication);
        Mockito.when(authentication.getPrincipal()).thenReturn(client);
        SecurityContextHolder.setContext(securityContext);

        String redirectUrl = clientService.getRedirectUrl("returnUrl");
        assertEquals(redirectUrl, serverUrl + "/v1/sso/login?returnUrl=" + "returnUrl" + "&clientId=" + clientId_Hub);
    }

    @Test(expected = UnauthorizedException.class)
    public void testGetNonExistingLogoutUrl_Success() {
        Authentication authentication = Mockito.mock(Authentication.class);
        SecurityContext securityContext = Mockito.mock(SecurityContext.class);

        Mockito.when(securityContext.getAuthentication()).thenReturn(authentication);
        Mockito.when(authentication.getPrincipal()).thenReturn(null);

        clientService.getLogoutUrl("returnUrl");
    }

    @Test
    public void testLogoutUrl_Success() {
        String serverUrl = "https://sample.com";
        Client client = new Client(clientId_Hub, "test", "test", null);
        client.setSsoUrl(serverUrl);
        Authentication authentication = Mockito.mock(Authentication.class);
        SecurityContext securityContext = Mockito.mock(SecurityContext.class);
        Mockito.when(securityContext.getAuthentication()).thenReturn(authentication);
        Mockito.when(authentication.getPrincipal()).thenReturn(client);
        SecurityContextHolder.setContext(securityContext);
        String redirectUrl = clientService.getLogoutUrl("returnUrl");
        assertEquals(redirectUrl, serverUrl + "/v1/sso/logout?returnUrl=" + "returnUrl" + "&clientId=" + clientId_Hub);
    }

    @Test(expected = UnauthorizedException.class)
    public void testGetNonExistingClientAskForHelpUrl() {
        Authentication authentication = Mockito.mock(Authentication.class);
        SecurityContext securityContext = Mockito.mock(SecurityContext.class);

        Mockito.when(securityContext.getAuthentication()).thenReturn(authentication);
        Mockito.when(authentication.getPrincipal()).thenReturn(null);

        clientService.getDefaultAskForUrl();
    }

    @Test
    public void testAskForHelpurl_ClientUrl_Success() {
        String notServerUrl = "https://not.sample.com";
        Client client = new Client(clientId_Hub, "test", "test", null);
        client.setAskforhelpurl(notServerUrl);
        Authentication authentication = Mockito.mock(Authentication.class);
        SecurityContext securityContext = Mockito.mock(SecurityContext.class);
        Mockito.when(securityContext.getAuthentication()).thenReturn(authentication);
        Mockito.when(authentication.getPrincipal()).thenReturn(client);
        SecurityContextHolder.setContext(securityContext);
        String serverUrl = "https://sample.com";
        ReflectionTestUtils.setField(clientService, "defaultAskForHelpUrl", serverUrl);

        String url = clientService.getDefaultAskForUrl();
        assertEquals(notServerUrl, url);
    }

    @Test
    public void testAskForHelpurl_DefaultUrl_Success() {
        Client client = new Client(clientId_Hub, "test", "test", null);
        Authentication authentication = Mockito.mock(Authentication.class);
        SecurityContext securityContext = Mockito.mock(SecurityContext.class);
        Mockito.when(securityContext.getAuthentication()).thenReturn(authentication);
        Mockito.when(authentication.getPrincipal()).thenReturn(client);
        SecurityContextHolder.setContext(securityContext);
        String serverUrl = "https://sample.com";
        ReflectionTestUtils.setField(clientService, "defaultAskForHelpUrl", serverUrl);

        String url = clientService.getDefaultAskForUrl();
        assertEquals(serverUrl, url);
    }

    @Test
    public void testFAQUrl_Success() {
        String serverUrl = "https://sample.com";
        ReflectionTestUtils.setField(clientService, "faqUrl", serverUrl);

        String url = clientService.getFaqUrl();
        assertEquals(serverUrl, url);
    }

    @Test(expected = ValidationException.class)
    public void testGetClientByNullCookie_Failure() {
        clientService.getClientByCookieName(null);
    }

    @Test
    public void testGetClientByValidCookie_Success() {
        String testAuthCookie = "SSOCookie";
        Client client = new Client(clientId_Hub, "test", "test", null);
        when(clientRepository.findBySsoAuthorizationCookieName(eq(testAuthCookie))).thenReturn(Optional.of(client));
        Client responseClient = clientService.getClientByCookieName(testAuthCookie);
        assertNotNull(responseClient);
        assertEquals(client.getId(), responseClient.getId());
        assertEquals(client.getName(), responseClient.getName());
    }

    @Test
    public void testgetClientByTokenOrDefault() {
        String text = "Test";
        Client client = new Client(text, text);
        Authentication authentication = Mockito.mock(Authentication.class);
        SecurityContext securityContext = Mockito.mock(SecurityContext.class);
        Mockito.when(securityContext.getAuthentication()).thenReturn(authentication);
        Mockito.when(authentication.getPrincipal()).thenReturn(client);
        SecurityContextHolder.setContext(securityContext);

        Client clientResponse = clientService.getClientByTokenOrDefault();
        assertEquals(clientResponse.getName(), text);
    }

    @Test
    public void testgetClientByTokenOrDefaultDefaultId() {
        String text = "Test";
        Client client = new Client(text, text);
        Authentication authentication = Mockito.mock(Authentication.class);
        SecurityContext securityContext = Mockito.mock(SecurityContext.class);
        Mockito.when(securityContext.getAuthentication()).thenReturn(authentication);
        when(clientRepository.findById(any())).thenReturn(Optional.of(client));
        Client clientResponse = clientService.getClientByTokenOrDefault();
        assertEquals(clientResponse.getName(), text);
    }
}
