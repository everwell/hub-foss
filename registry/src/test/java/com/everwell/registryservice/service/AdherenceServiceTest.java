package com.everwell.registryservice.service;

import com.everwell.registryservice.BaseTest;
import com.everwell.registryservice.models.http.response.adherence.AdherenceTechnologyDto;
import com.everwell.registryservice.service.impl.AdherenceServiceImpl;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Spy;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

public class AdherenceServiceTest extends BaseTest {

    @Spy
    @InjectMocks
    private AdherenceServiceImpl adherenceService;

    @Test
    public void testGetAllTechnologyOptions_Success() {
        List<AdherenceTechnologyDto> technologyDtoList = adherenceService.getAllTechnologyOptions();
        assertNotNull(technologyDtoList);
        assertEquals(5, technologyDtoList.size());
        assertNotNull(technologyDtoList.get(0).getKey());
        assertNotNull(technologyDtoList.get(0).getValue());
        assertNotNull(technologyDtoList.get(0).getAdherenceTechOptions());
    }
}
