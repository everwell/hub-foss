package com.everwell.registryservice.validators;

import com.everwell.registryservice.BaseTest;
import com.everwell.registryservice.constants.ValidationStatusEnum;
import com.everwell.registryservice.exceptions.ValidationException;
import com.everwell.registryservice.models.dto.HierarchyRequestData;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Spy;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThrows;


public class HierarchyRequestValidatorTest extends BaseTest {
    @Spy
    @InjectMocks
    private HierarchyRequestValidator hierarchyRequestValidator;

    @Test
    public void validateAddHierarchyRequest_ParentIdMissing() {
        ValidationException validationException = assertThrows(ValidationException.class, () -> hierarchyRequestValidator.validateAddHierarchyRequest(getInvalidLevel2AddHierarchyRequestParentIdMissing()));
        assertEquals(ValidationStatusEnum.PARENT_ID_REQUIRED_FOR_HIERARCHY_NOT_LEVEL_ONE.getMessage(), validationException.getMessage());
    }

    private HierarchyRequestData getInvalidLevel2AddHierarchyRequestParentIdMissing() {
        HierarchyRequestData hierarchyRequestData = new HierarchyRequestData();
        hierarchyRequestData.setLevel(2L);
        return hierarchyRequestData;
    }
}
