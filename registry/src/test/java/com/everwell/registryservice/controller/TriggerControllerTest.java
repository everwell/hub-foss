package com.everwell.registryservice.controller;

import com.everwell.registryservice.BaseTest;
import com.everwell.registryservice.exceptions.CustomExceptionHandler;
import com.everwell.registryservice.models.db.Trigger;
import com.everwell.registryservice.models.http.requests.ins.TriggerDetailsRequest;
import com.everwell.registryservice.models.http.response.TriggersAndTemplatesResponse;
import com.everwell.registryservice.service.TriggerService;
import org.junit.Before;
import com.everwell.registryservice.models.http.requests.UpdateTriggerDetailsRequest;
import com.everwell.registryservice.utils.Utils;
import com.everwell.registryservice.models.dto.AddTriggerDetails;
import com.everwell.registryservice.models.http.requests.AddTriggerDetailsRequest;
import com.everwell.registryservice.models.http.response.AddTriggerResponse;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import org.mockito.Mockito;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.verify;

import static org.mockito.ArgumentMatchers.any;

public class TriggerControllerTest extends BaseTest {

    private MockMvc mockMvc;

    @Mock
    TriggerService triggerService;

    @InjectMocks
    private TriggerController triggerController;

    public static final String TRIGGER_DETAILS_REQUIRED = "Trigger Details must be provided";

    @Before
    public void setup()
    {
        mockMvc = MockMvcBuilders.standaloneSetup(triggerController)
                .setControllerAdvice(new CustomExceptionHandler())
                .build();
    }

    @Test
    public void getAllTriggers() throws Exception {
        Trigger trigger = new Trigger(1L, 1L, 1L, 1L, 1L, "", "", "", "", false, "", "", false, false);
        List<TriggersAndTemplatesResponse> response = new ArrayList<>(Arrays.asList(
                new TriggersAndTemplatesResponse(trigger, "", null, Collections.emptyList())
        ));
        String uri = "/v1/trigger";

        when(triggerService.getAllTriggers()).thenReturn(response);


        mockMvc.
                perform(
                        MockMvcRequestBuilders
                                .get(uri)
                                .accept(MediaType.APPLICATION_JSON_VALUE)
                )
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.success").value("true"))
                .andExpect(MockMvcResultMatchers.jsonPath("$.data[0].id").value(response.get(0).getId()));
    }

    @Test
    public void getTriggersByFunctionName() throws Exception {
        Trigger trigger = new Trigger(1L, 1L, 1L, 1L, 1L, "", "", "SampleJob", "", false, "", "", false, false);
        List<TriggerDetailsRequest> response = new ArrayList<>(Arrays.asList(
                new TriggerDetailsRequest(1L, 1L, 1L, Arrays.asList(1L),true,true,true, "test", 1L, "test", "test")
        ));
        String uri = "/v1/triggers?hierarchyId=1&functionName=SampleJob";

        when(triggerService.getActiveTriggersByFunctionName(anyLong(), anyString())).thenReturn(response);


        mockMvc.
                perform(
                        MockMvcRequestBuilders
                                .get(uri)
                                .accept(MediaType.APPLICATION_JSON_VALUE)
                )
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.success").value("true"));
    }

    @Test
    public void updateTriggerSuccess() throws Exception{
        UpdateTriggerDetailsRequest updateTriggerDetailsRequest = new UpdateTriggerDetailsRequest(1L,1L,1L,"2,3","* * * * *","event","function",true,true,true,"sms");

        String uri = "/v1/trigger";

        doNothing().when(triggerService).updateTrigger(updateTriggerDetailsRequest);

        mockMvc.perform(
                MockMvcRequestBuilders
                        .put(uri)
                        .content(Utils.asJsonString(updateTriggerDetailsRequest))
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON_VALUE)
                )
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.success").value(true));
        verify(triggerService, Mockito.times(1)).updateTrigger(any());

    }

    @Test
    public void addTriggerSuccess() throws Exception {
        AddTriggerDetails addTriggerDetails = new AddTriggerDetails(1L, 1L, 1L, 1L, "registry", null, "2,3", "* * * * *", "event", "function", true, true, true, "sms", true, "content", 1L, 1L, "p1,p2", false);
        List<AddTriggerDetails> triggerDetails = new ArrayList<>();
        triggerDetails.add(addTriggerDetails);
        AddTriggerDetailsRequest addTriggerDetailsRequest = new AddTriggerDetailsRequest(triggerDetails);

        String uri = "/v1/trigger";
        List<AddTriggerDetails> failedEntries = new ArrayList<>();
        AddTriggerResponse response = new AddTriggerResponse(failedEntries);

        when(triggerService.addTrigger(addTriggerDetailsRequest)).thenReturn(response);
        mockMvc.perform(
                        MockMvcRequestBuilders
                                .post(uri)
                                .content(Utils.asJsonString(addTriggerDetailsRequest))
                                .contentType(MediaType.APPLICATION_JSON)
                                .accept(MediaType.APPLICATION_JSON_VALUE)
                )
                .andExpect(status().isCreated())
                .andExpect(MockMvcResultMatchers.jsonPath("$.success").value("true"))
                .andExpect(MockMvcResultMatchers.jsonPath("$.message").value(""));
    }

    @Test
    public void addTriggerFailure() throws Exception {
        AddTriggerDetails addTriggerDetails = new AddTriggerDetails(1L, 1L, 1L, 1L, "registry", null, "2,3", "* * * * *", "event", "function", true, true, true, "sms", true, "content", 1L, 1L, "p1,p2", false);
        List<AddTriggerDetails> triggerDetails = new ArrayList<>();
        triggerDetails.add(addTriggerDetails);
        AddTriggerDetailsRequest addTriggerDetailsRequest = new AddTriggerDetailsRequest(triggerDetails);

        String uri = "/v1/trigger";

        AddTriggerResponse response = new AddTriggerResponse(triggerDetails);
        when(triggerService.addTrigger(any())).thenReturn(response);
        mockMvc.perform(
                        MockMvcRequestBuilders
                                .post(uri)
                                .content(Utils.asJsonString(addTriggerDetailsRequest))
                                .contentType(MediaType.APPLICATION_JSON)
                                .accept(MediaType.APPLICATION_JSON_VALUE)
                )
                .andExpect(status().isCreated())
                .andExpect(MockMvcResultMatchers.jsonPath("$.success").value("true"));
    }

    @Test
    public void addTriggerTestWithoutTriggerDetails() throws Exception{
        String uri = "/v1/trigger";
        AddTriggerDetailsRequest addTriggerDetailsRequest = new AddTriggerDetailsRequest();
        mockMvc
                .perform(
                        MockMvcRequestBuilders
                                .post(uri)
                                .contentType(MediaType.APPLICATION_JSON)
                                .content(Utils.asJsonString(addTriggerDetailsRequest))
                                .accept(MediaType.APPLICATION_JSON_VALUE)
                )
                .andExpect(status().isBadRequest())
                .andExpect(MockMvcResultMatchers.jsonPath("$.message").value(TRIGGER_DETAILS_REQUIRED));
    }
}
