package com.everwell.registryservice.controller;

import com.everwell.registryservice.BaseTest;
import com.everwell.registryservice.exceptions.CustomExceptionHandler;
import com.everwell.registryservice.exceptions.NotFoundException;
import com.everwell.registryservice.models.db.*;
import com.everwell.registryservice.models.dto.FilterDetails;
import com.everwell.registryservice.models.dto.TaskListItem;
import com.everwell.registryservice.models.http.requests.TaskListItemRequest;
import com.everwell.registryservice.models.http.response.TaskListItemResponse;
import com.everwell.registryservice.service.DeploymentService;
import com.everwell.registryservice.service.TaskListService;
import com.everwell.registryservice.utils.Utils;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

import static org.hamcrest.Matchers.hasSize;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

public class TaskListControllerTest extends BaseTest {
    private MockMvc mockMvc;

    @Mock
    private DeploymentService deploymentService;

    @Mock
    private TaskListService taskListService;

    @InjectMocks
    private TaskListController taskListController;

    private static String deploymentCode = "IND";
    private static String name = "_end_date_passed";
    private static String webIcon = "fa fa-clock-";
    private static String appIcon = "{md-alarm}";
    private static String description = "tasklist_desc_end_date_passed";
    private static String displayColumns = "1,2,4";
    private static String extraData = "{extra_data}";
    private static Long displayOrder = 1L;
    private String filterName = "EndDatePassed";
    private String value = "true";
    private String labelName = "PatientType";
    private String label = "PatientType";
    private Long id = 1L;
    private Long deploymentId = 1L;
    private Deployment deployment = new Deployment(
            1L,
            "IND",
            123,
            "India Standard Time",
            "IST",
            "Asia/Kolkata",
            1L,
            null,
            "",
            "India",
            "-",
            "https://help.url",
            "10:00 AM"
    );

    private static final String MESSAGE_TASK_LIST_ITEM_REQUIRED = "task list item is required";
    private static final String MESSAGE_DEPLOYMENT_CODE_REQUIRED = "deployment code is required";
    private static final String MESSAGE_FILTERS_REQUIRED = "filters are required";
    private static final String MESSAGE_NAME_REQUIRED = "name is required";
    private static final String MESSAGE_WEB_ICON_REQUIRED = "web icon is required";
    private static final String MESSAGE_APP_ICON_REQUIRED = "app icon is required";
    private static final String MESSAGE_DESCRIPTION_REQUIRED = "description is required";
    private static final String MESSAGE_DISPLAY_COLUMNS_REQUIRED = "display columns are required";
    private static final String MESSAGE_EXTRA_DATA_REQUIRED = "extra data is required";
    private static final String FILTER_NAME_REQUIRED = "Filter name is required";
    private static final String FILTER_VALUE_REQUIRED = "Filter value is required";

    @Before
    public void setup() {
        mockMvc = MockMvcBuilders.standaloneSetup(taskListController)
                .setControllerAdvice(new CustomExceptionHandler())
                .build();
    }

    @Test
    public void testAddTaskList() throws Exception {
        String uri = "/v1/taskList";
        TaskListItem taskListItem = new TaskListItem(deploymentCode, 1L, displayColumns, displayOrder);
        TaskListItemRequest taskListItemRequest = new TaskListItemRequest(taskListItem, Collections.singletonList(1L));

        when(deploymentService.getByCode(any())).thenReturn(deployment);
        when(taskListService.validateTaskList(any(), any())).thenReturn(true);
        when(taskListService.addTaskList(taskListItemRequest, deployment.getId())).thenReturn(id);

        mockMvc
                .perform(
                        MockMvcRequestBuilders
                                .post(uri)
                                .contentType(MediaType.APPLICATION_JSON)
                                .content(Utils.asJsonString(taskListItemRequest))
                                .accept(MediaType.APPLICATION_JSON_VALUE)
                )
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.success").value(true));
        verify(deploymentService, Mockito.times(1)).getByCode(any());
        verify(taskListService, Mockito.times(1)).addTaskList(any(), any());
    }

    @Test
    public void testAddTaskListAlreadyExists() throws Exception {
        String uri = "/v1/taskList";
        TaskListItem taskListItem = new TaskListItem(deploymentCode, 1L, displayColumns, displayOrder);
        TaskListItemRequest taskListItemRequest = new TaskListItemRequest(taskListItem, Collections.singletonList(1L));

        when(deploymentService.getByCode(any())).thenReturn(deployment);
        when(taskListService.validateTaskList(any(), any())).thenReturn(true);
        when(taskListService.taskListAlreadyMappedToDeployment(any(), any())).thenReturn(true);

        mockMvc
                .perform(
                        MockMvcRequestBuilders
                                .post(uri)
                                .contentType(MediaType.APPLICATION_JSON)
                                .content(Utils.asJsonString(taskListItemRequest))
                                .accept(MediaType.APPLICATION_JSON_VALUE)
                )
                .andExpect(status().isConflict())
                .andExpect(MockMvcResultMatchers.jsonPath("$.success").value(false));
        verify(deploymentService, Mockito.times(1)).getByCode(any());
    }

    @Test
    public void testAddTaskListWithoutExistingParameters() throws Exception {
        String uri = "/v1/taskList";
        TaskListItem taskListItem = new TaskListItem(deploymentCode, 1L, displayColumns, displayOrder);
        TaskListItemRequest taskListItemRequest = new TaskListItemRequest(taskListItem, Collections.singletonList(1L));

        when(deploymentService.getByCode(any())).thenReturn(deployment);
        when(taskListService.validateTaskList(any(), any())).thenReturn(false);

        mockMvc
                .perform(
                        MockMvcRequestBuilders
                                .post(uri)
                                .contentType(MediaType.APPLICATION_JSON)
                                .content(Utils.asJsonString(taskListItemRequest))
                                .accept(MediaType.APPLICATION_JSON_VALUE)
                )
                .andExpect(status().isBadRequest())
                .andExpect(MockMvcResultMatchers.jsonPath("$.success").value(false));
    }

    @Test
    public void testAddTaskListWithoutTaskListItem() throws Exception {
        String uri = "/v1/taskList";
        TaskListItemRequest taskListItemRequest = new TaskListItemRequest(null, Collections.singletonList(1L));

        mockMvc
                .perform(
                        MockMvcRequestBuilders
                                .post(uri)
                                .contentType(MediaType.APPLICATION_JSON)
                                .content(Utils.asJsonString(taskListItemRequest))
                                .accept(MediaType.APPLICATION_JSON_VALUE)
                )
                .andExpect(status().isBadRequest())
                .andExpect(MockMvcResultMatchers.jsonPath("$.message").value(MESSAGE_TASK_LIST_ITEM_REQUIRED));
    }

    @Test
    public void testAddTaskListWithoutFilters() throws Exception {
        String uri = "/v1/taskList";
        TaskListItem taskListItem = new TaskListItem(deploymentCode, 1L, displayColumns, displayOrder);
        TaskListItemRequest taskListItemRequest = new TaskListItemRequest(taskListItem, null);

        mockMvc
                .perform(
                        MockMvcRequestBuilders
                                .post(uri)
                                .contentType(MediaType.APPLICATION_JSON)
                                .content(Utils.asJsonString(taskListItemRequest))
                                .accept(MediaType.APPLICATION_JSON_VALUE)
                )
                .andExpect(status().isBadRequest())
                .andExpect(MockMvcResultMatchers.jsonPath("$.message").value(MESSAGE_FILTERS_REQUIRED));
    }

    @Test
    public void testAddTaskListWithEmptyFilters() throws Exception {
        String uri = "/v1/taskList";
        TaskListItem taskListItem = new TaskListItem(deploymentCode, 1L, displayColumns, displayOrder);
        TaskListItemRequest taskListItemRequest = new TaskListItemRequest(taskListItem, new ArrayList<>());

        mockMvc
                .perform(
                        MockMvcRequestBuilders
                                .post(uri)
                                .contentType(MediaType.APPLICATION_JSON)
                                .content(Utils.asJsonString(taskListItemRequest))
                                .accept(MediaType.APPLICATION_JSON_VALUE)
                )
                .andExpect(status().isBadRequest())
                .andExpect(MockMvcResultMatchers.jsonPath("$.message").value(MESSAGE_FILTERS_REQUIRED));
    }

    @Test
    public void testAddTaskListWithoutDeploymentCode() throws Exception {
        String uri = "/v1/taskList";
        TaskListItem taskListItem = new TaskListItem(null, 1L, displayColumns, displayOrder);
        TaskListItemRequest taskListItemRequest = new TaskListItemRequest(taskListItem, Collections.singletonList(1L));

        mockMvc
                .perform(
                        MockMvcRequestBuilders
                                .post(uri)
                                .contentType(MediaType.APPLICATION_JSON)
                                .content(Utils.asJsonString(taskListItemRequest))
                                .accept(MediaType.APPLICATION_JSON_VALUE)
                )
                .andExpect(status().isBadRequest())
                .andExpect(MockMvcResultMatchers.jsonPath("$.message").value(MESSAGE_DEPLOYMENT_CODE_REQUIRED));
    }

    @Test
    public void testAddTaskListWithoutDisplayColumns() throws Exception {
        String uri = "/v1/taskList";
        TaskListItem taskListItem = new TaskListItem(deploymentCode, 1L, null, displayOrder);
        TaskListItemRequest taskListItemRequest = new TaskListItemRequest(taskListItem, Collections.singletonList(1L));

        mockMvc
                .perform(
                        MockMvcRequestBuilders
                                .post(uri)
                                .contentType(MediaType.APPLICATION_JSON)
                                .content(Utils.asJsonString(taskListItemRequest))
                                .accept(MediaType.APPLICATION_JSON_VALUE)
                )
                .andExpect(status().isBadRequest())
                .andExpect(MockMvcResultMatchers.jsonPath("$.message").value(MESSAGE_DISPLAY_COLUMNS_REQUIRED));
    }

    @Test
    public void testGetTaskList() throws Exception {
        String uri = "/v1/taskList/id/" + id;
        DeploymentTaskList taskList = new DeploymentTaskList(id, deploymentId, 1L, displayColumns, displayOrder);
        TaskListColumn taskListColumn = new TaskListColumn(id, labelName, label, true);
        List<TaskListColumn> taskListColumnsList = new ArrayList<>();
        taskListColumnsList.add(taskListColumn);

        when(taskListService.getTaskList(any())).thenReturn(taskList);
        when(taskListService.getDisplayColumns(any())).thenReturn(taskListColumnsList);

        List<TaskListFilterMap> filterMaps = Arrays.asList(
                new TaskListFilterMap(1L, deploymentId, 1L),
                new TaskListFilterMap(2L, deploymentId, 2L)
        );

        List<FilterDetails> filterDetails = Arrays.asList(
                new FilterDetails("Name1", "Value1", 1L),
                new FilterDetails("Name2", "Value2", 2L)
        );

        when(taskListService.getTaskListFiltersByTaskListIds(Collections.singletonList(1L))).thenReturn(filterMaps);
        when(taskListService.getTaskListItem(1L)).thenReturn(new TaskList(1L, name, webIcon, appIcon, description, extraData));
        when(taskListService.getFilterDetails(Arrays.asList(1L, 2L))).thenReturn(filterDetails);

        mockMvc
                .perform(
                        MockMvcRequestBuilders
                                .get(uri)
                                .contentType(MediaType.APPLICATION_JSON)
                                .accept(MediaType.APPLICATION_JSON_VALUE)
                )
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.data.id").value(id));
        verify(taskListService, Mockito.times(1)).getTaskList(any());
        verify(taskListService, Mockito.times(1)).getDisplayColumns(any());
    }

    @Test
    public void testGetTaskListWithInvalidTaskList() throws Exception {
        String uri = "/v1/taskList/id/" + id;
        DeploymentTaskList taskList = new DeploymentTaskList(id, deploymentId, 1L, displayColumns, displayOrder);
        TaskListColumn taskListColumn = new TaskListColumn(id, labelName, label, true);
        List<TaskListColumn> taskListColumnsList = new ArrayList<>();
        taskListColumnsList.add(taskListColumn);

        when(taskListService.getTaskList(any())).thenReturn(taskList);
        when(taskListService.getDisplayColumns(any())).thenReturn(taskListColumnsList);

        List<TaskListFilterMap> filterMaps = Arrays.asList(
                new TaskListFilterMap(1L, deploymentId, 1L),
                new TaskListFilterMap(2L, deploymentId, 2L)
        );

        List<FilterDetails> filterDetails = Arrays.asList(
                new FilterDetails("Name1", "Value1", 1L),
                new FilterDetails("Name2", "Value2", 2L)
        );

        when(taskListService.getTaskListFiltersByTaskListIds(Collections.singletonList(1L))).thenReturn(filterMaps);
        when(taskListService.getTaskListItem(1L)).thenReturn(null);

        mockMvc
                .perform(
                        MockMvcRequestBuilders
                                .get(uri)
                                .contentType(MediaType.APPLICATION_JSON)
                                .accept(MediaType.APPLICATION_JSON_VALUE)
                )
                .andExpect(status().isNotFound())
                .andExpect(MockMvcResultMatchers.jsonPath("$.message").value("Tasklist item not found"));
    }

    @Test
    public void testGetTaskListWithoutDisplayColumns() throws Exception {
        String uri = "/v1/taskList/id/" + id;
        DeploymentTaskList taskList = new DeploymentTaskList(id, deploymentId, 1L, null, displayOrder);

        when(taskListService.getTaskList(any())).thenReturn(taskList);

        List<TaskListFilterMap> filterMaps = Arrays.asList(
                new TaskListFilterMap(1L, deploymentId, 1L),
                new TaskListFilterMap(2L, deploymentId, 2L)
        );

        List<FilterDetails> filterDetails = Arrays.asList(
                new FilterDetails("Name1", "Value1", 1L),
                new FilterDetails("Name2", "Value2", 2L)
        );

        when(taskListService.getTaskListFiltersByTaskListIds(Collections.singletonList(1L))).thenReturn(filterMaps);
        when(taskListService.getTaskListItem(1L)).thenReturn(new TaskList(1L, name, webIcon, appIcon, description, extraData));
        when(taskListService.getFilterDetails(Arrays.asList(1L, 2L))).thenReturn(filterDetails);

        mockMvc
                .perform(
                        MockMvcRequestBuilders
                                .get(uri)
                                .contentType(MediaType.APPLICATION_JSON)
                                .accept(MediaType.APPLICATION_JSON_VALUE)
                )
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.data.id").value(id));
        verify(taskListService, Mockito.times(1)).getTaskList(any());
    }

    @Test
    public void testUpdateTaskList() throws Exception {
        Long taskListId = 1L;
        String uri = "/v1/taskList/id/" + taskListId;
        TaskListItem taskListItem = new TaskListItem(deploymentCode, 1L, displayColumns, displayOrder);
        TaskListItemRequest taskListItemRequest = new TaskListItemRequest(taskListItem, Collections.singletonList(1L));

        doNothing().when(taskListService).updateTaskList(taskListItemRequest, taskListId);

        mockMvc
                .perform(
                        MockMvcRequestBuilders
                                .put(uri)
                                .contentType(MediaType.APPLICATION_JSON)
                                .content(Utils.asJsonString(taskListItemRequest))
                                .accept(MediaType.APPLICATION_JSON_VALUE)
                )
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.success").value(true));
        verify(taskListService, Mockito.times(1)).updateTaskList(any(), anyLong());
    }

    @Test
    public void testGetTaskListsByDeployment() throws Exception {
        String code = "DEMOC";
        Long deploymentId = 1L;
        String uri = "/v1/taskList/deployment/" + code;

        Deployment deployment = new Deployment(
                deploymentId,
                code,
                123,
                "India Standard Time",
                "IST",
                "Asia/Kolkata",
                1L,
                null,
                "",
                "India",
                "-",
                "https://help.url",
                "10:00 AM"
        );

        when(deploymentService.getByCode(deployment.getCode())).thenReturn(deployment);
        when(taskListService.getTaskListFilterResponseByDeploymentId(eq(deploymentId), eq(true))).thenReturn(getDefaultTaskListItemResponses());

        List<DeploymentTaskList> taskLists = Arrays.asList(
                new DeploymentTaskList(
                        1L,
                        deploymentId,
                        1L,
                        displayColumns,
                        displayOrder
                ),
                new DeploymentTaskList(
                        2L,
                        deploymentId,
                        2L,
                        displayColumns,
                        displayOrder
                )
        );
        mockMvc
                .perform(
                        MockMvcRequestBuilders
                                .get(uri)
                                .accept(MediaType.APPLICATION_JSON_VALUE)
                )
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.data", hasSize(taskLists.size())))
                .andExpect(MockMvcResultMatchers.jsonPath("$.data[0].id").value(taskLists.get(0).getId()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.data[0].filters", hasSize(2)))
                .andExpect(MockMvcResultMatchers.jsonPath("$.data[1].filters", hasSize(1)));
    }

    @Test
    public void testGetTaskListsByDeploymentInvalidTaskListItem() throws Exception {
        String code = "DEMOC";
        Long deploymentId = 1L;
        String uri = "/v1/taskList/deployment/" + code;

        Deployment deployment = new Deployment(
                deploymentId,
                code,
                123,
                "India Standard Time",
                "IST",
                "Asia/Kolkata",
                1L,
                null,
                "",
                "India",
                "-",
                "https://help.url",
                "10:00 AM"
        );

        when(deploymentService.getByCode(deployment.getCode())).thenReturn(deployment);
        when(taskListService.getTaskListFilterResponseByDeploymentId(eq(deploymentId), eq(true))).thenThrow(new NotFoundException("TaskList Item not found"));

        mockMvc
                .perform(
                        MockMvcRequestBuilders
                                .get(uri)
                                .accept(MediaType.APPLICATION_JSON_VALUE)
                )
                .andExpect(status().isNotFound())
                .andExpect(MockMvcResultMatchers.jsonPath("$.message").value("TaskList Item not found"));
    }

    @Test
    public void testGetTaskListsByDeploymentWithNoDisplayColumns() throws Exception {
        String code = "DEMOC";
        Long deploymentId = 1L;
        String uri = "/v1/taskList/deployment/" + code;

        Deployment deployment = new Deployment(
                deploymentId,
                code,
                123,
                "India Standard Time",
                "IST",
                "Asia/Kolkata",
                1L,
                null,
                "",
                "India",
                "-",
                "https://help.url",
                "10:00 AM"
        );

        when(deploymentService.getByCode(deployment.getCode())).thenReturn(deployment);
        when(taskListService.getTaskListFilterResponseByDeploymentId(eq(deploymentId), eq(true))).thenReturn(getDefaultTaskListItemResponsesNoDisplayColumns());

        List<DeploymentTaskList> taskLists = Collections.singletonList(
                new DeploymentTaskList(
                        1L,
                        deploymentId,
                        1L,
                        null,
                        displayOrder
                )
        );

        List<TaskListFilterMap> filterMaps = Arrays.asList(
                new TaskListFilterMap(1L, deploymentId, 1L),
                new TaskListFilterMap(2L, deploymentId, 2L)
        );

        mockMvc
                .perform(
                        MockMvcRequestBuilders
                                .get(uri)
                                .accept(MediaType.APPLICATION_JSON_VALUE)
                )
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.data", hasSize(taskLists.size())))
                .andExpect(MockMvcResultMatchers.jsonPath("$.data[0].id").value(taskLists.get(0).getId()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.data[0].filters", hasSize(filterMaps.size())));
    }

    @Test
    public void testDeleteTaskList() throws Exception {
        String uri = "/v1/taskList/id/" + id;

        doNothing().when(taskListService).deleteTaskList(any());

        mockMvc
                .perform(
                        MockMvcRequestBuilders
                                .delete(uri)
                                .contentType(MediaType.APPLICATION_JSON)
                                .accept(MediaType.APPLICATION_JSON_VALUE)
                )
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.success").value(true));
    }

    @Test
    public void testGetAllTaskLists() throws Exception {
        String uri = "/v1/taskList";

        List<TaskList> taskLists = Arrays.asList(
                new TaskList(1L, name, webIcon, appIcon, description, extraData)
        );


        when(taskListService.getAllTaskLists()).thenReturn(taskLists);

        mockMvc
                .perform(
                        MockMvcRequestBuilders
                                .get(uri)
                                .accept(MediaType.APPLICATION_JSON_VALUE)
                )
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.data", hasSize(taskLists.size())))
                .andExpect(MockMvcResultMatchers.jsonPath("$.data[0].name").value(name));
    }

    @Test
    public void testGetAlFilters() throws Exception {
        String uri = "/v1/taskList/filters";

        Filter filter1 = new Filter(1L, "Filter1");
        Filter filter2 = new Filter(2L, "Filter2");

        FilterValue filterValue1 = new FilterValue(1L, 1L, "Value1");
        FilterValue filterValue2 = new FilterValue(1L, 1L, "Value2");
        FilterValue filterValue3 = new FilterValue(1L, 2L, "Value3");

        when(taskListService.getAllFilters()).thenReturn(Arrays.asList(filter1, filter2));
        when(taskListService.getAllFilterValues()).thenReturn(Arrays.asList(filterValue1, filterValue2, filterValue3));

        mockMvc
                .perform(
                        MockMvcRequestBuilders
                                .get(uri)
                                .accept(MediaType.APPLICATION_JSON_VALUE)
                )
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.data", hasSize(2)))
                .andExpect(MockMvcResultMatchers.jsonPath("$.data[0].name").value("Filter1"))
                .andExpect(MockMvcResultMatchers.jsonPath("$.data[1].name").value("Filter2"))
                .andExpect(MockMvcResultMatchers.jsonPath("$.data[0].values", hasSize(2)))
                .andExpect(MockMvcResultMatchers.jsonPath("$.data[1].values", hasSize(1)));
    }
    
    @Test
    public void testGetAllDisplayColumnDetails() throws Exception {
        String uri = "/v1/tasklist/columns";
        TaskListColumn taskListColumn = new TaskListColumn(id, labelName, label, true);
        List<TaskListColumn> taskListColumnsList = new ArrayList<>();
        taskListColumnsList.add(taskListColumn);
        when(taskListService.getAllDisplayColumns()).thenReturn(taskListColumnsList);
        mockMvc
                .perform(
                        MockMvcRequestBuilders
                                .get(uri)
                                .contentType(MediaType.APPLICATION_JSON)
                                .accept(MediaType.APPLICATION_JSON_VALUE)
                )
                .andExpect(status().isOk());
        verify(taskListService, Mockito.times(1)).getAllDisplayColumns();
    }

    private List<TaskListItemResponse> getDefaultTaskListItemResponses() {
        List<DeploymentTaskList> taskLists = Arrays.asList(
                new DeploymentTaskList(
                        1L,
                        deploymentId,
                        1L,
                        displayColumns,
                        displayOrder
                ),
                new DeploymentTaskList(
                        2L,
                        deploymentId,
                        2L,
                        displayColumns,
                        displayOrder
                )
        );
        TaskListItemResponse itemResponse = new TaskListItemResponse(taskLists.get(0), new TaskList(1L, name, webIcon, appIcon, description, extraData));
        List<FilterDetails> filterDetails = Arrays.asList(
                new FilterDetails("Name1", "Value1", 1L),
                new FilterDetails("Name2", "Value2", 2L)
        );
        itemResponse.setFilters(filterDetails);

        TaskListItemResponse itemResponse1 = new TaskListItemResponse(taskLists.get(1), new TaskList(2L, "TaskList 2", webIcon, appIcon, description, extraData));
        itemResponse1.setFilters(Collections.singletonList(filterDetails.get(0)));
        return Arrays.asList(itemResponse, itemResponse1);
    }

    private List<TaskListItemResponse> getDefaultTaskListItemResponsesNoDisplayColumns() {
        List<DeploymentTaskList> taskLists = Arrays.asList(
                new DeploymentTaskList(
                        1L,
                        deploymentId,
                        1L,
                        null,
                        displayOrder
                )
        );
        TaskListItemResponse itemResponse = new TaskListItemResponse(taskLists.get(0), new TaskList(1L, name, webIcon, appIcon, description, extraData));
        List<FilterDetails> filterDetails = Arrays.asList(
                new FilterDetails("Name1", "Value1", 1L),
                new FilterDetails("Name2", "Value2", 2L)
        );
        itemResponse.setFilters(filterDetails);

        return Arrays.asList(itemResponse);
    }

}
