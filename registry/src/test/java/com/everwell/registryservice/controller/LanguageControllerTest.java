package com.everwell.registryservice.controller;

import com.everwell.registryservice.BaseTest;
import com.everwell.registryservice.exceptions.CustomExceptionHandler;
import com.everwell.registryservice.models.db.Language;
import com.everwell.registryservice.service.LanguageService;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

public class LanguageControllerTest extends BaseTest
{
    private MockMvc mockMvc;

    @Mock
    private LanguageService languageService;

    @Mock
    private ApplicationEventPublisher applicationEventPublisher;

    @InjectMocks
    private LanguageController languageController;

    @Before
    public void setup() {
        mockMvc = MockMvcBuilders.standaloneSetup(languageController)
                .setControllerAdvice(new CustomExceptionHandler())
                .build();
    }

    @Test
    public void testGetAll_Success() throws Exception {
        List<Language> languages = new ArrayList<>(Arrays.asList(
                new Language(1L, "English", "en" , "English"),
                new Language(2L, "Hi", "hi", "hi")
        ));
        String uri = "/v1/languages";

        when(languageService.getAll()).thenReturn(languages);
        doNothing().when(applicationEventPublisher).publishEvent(any());
        mockMvc.
                perform(
                        MockMvcRequestBuilders
                                .get(uri)
                                .accept(MediaType.APPLICATION_JSON_VALUE)
                )
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.success").value("true"))
                .andExpect(MockMvcResultMatchers.jsonPath("$.data[0].id").value(languages.get(0).getId()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.data[1].id").value(languages.get(1).getId()));
    }
}
