package com.everwell.registryservice.controller;

import com.everwell.registryservice.BaseTest;
import com.everwell.registryservice.exceptions.CustomExceptionHandler;
import com.everwell.registryservice.exceptions.NotFoundException;
import com.everwell.registryservice.models.db.Client;
import com.everwell.registryservice.models.http.requests.RegisterClientRequest;
import com.everwell.registryservice.models.http.response.ClientResponse;
import com.everwell.registryservice.service.ClientService;
import com.everwell.registryservice.utils.Utils;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

public class ClientControllerTest extends BaseTest {

    private MockMvc mockMvc;

    @Mock
    private ClientService clientService;

    @InjectMocks
    private ClientController clientController;

    private static Long id = 1L;
    private static String name = "Dummy Client";
    private static String password = "Dummy Password";

    private static final String MESSAGE_CLIENT_REGISTERED_SUCCESSFULLY = "client registered successfully";
    private static final String MESSAGE_NAME_REQUIRED = "name is required";
    private static final String MESSAGE_PASSWORD_REQUIRED = "password is required";

    @Before
    public void setup() {
        mockMvc = MockMvcBuilders.standaloneSetup(clientController)
            .setControllerAdvice(new CustomExceptionHandler())
            .build();
    }

    @Test
    public void testGetClientSuccess() throws Exception {
        String uri = "/v1/client";

        when(clientService.getClientWithToken(any())).thenReturn(null);

        mockMvc.
            perform(
                MockMvcRequestBuilders
                    .get(uri)
                    .header("x-client-id", id)
                    .accept(MediaType.APPLICATION_JSON_VALUE)
            )
            .andExpect(status().isOk())
            .andExpect(MockMvcResultMatchers.jsonPath("$.success").value("true"));
    }

    @Test
    public void testPostClientSuccess() throws Exception {
        String uri = "/v1/client";
        RegisterClientRequest clientRequest = new RegisterClientRequest(name, password, null, "www.google.com");

        when(clientService.registerClient(any())).thenReturn(null);

        mockMvc.
            perform(
                MockMvcRequestBuilders
                    .post(uri)
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(Utils.asJsonString(clientRequest))
                    .accept(MediaType.APPLICATION_JSON_VALUE)
            )
            .andExpect(status().isCreated())
            .andExpect(MockMvcResultMatchers.jsonPath("$.message").value(MESSAGE_CLIENT_REGISTERED_SUCCESSFULLY));
        ;
    }

    @Test
    public void testPostClientWithoutName() throws Exception {
        String uri = "/v1/client";
        RegisterClientRequest clientRequest = new RegisterClientRequest(null, password, null, "www.google.com");

        mockMvc.
            perform(
                MockMvcRequestBuilders
                    .post(uri)
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(Utils.asJsonString(clientRequest))
                    .accept(MediaType.APPLICATION_JSON_VALUE)
            )
            .andExpect(status().isBadRequest())
            .andExpect(MockMvcResultMatchers.jsonPath("$.message").value(MESSAGE_NAME_REQUIRED));
        ;
    }

    @Test
    public void testPostClientWithoutPassword() throws Exception {
        String uri = "/v1/client";
        RegisterClientRequest clientRequest = new RegisterClientRequest(name, null, null, "www.google.com");

        mockMvc.
            perform(
                MockMvcRequestBuilders
                    .post(uri)
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(Utils.asJsonString(clientRequest))
                    .accept(MediaType.APPLICATION_JSON_VALUE)
            )
            .andExpect(status().isBadRequest())
            .andExpect(MockMvcResultMatchers.jsonPath("$.message").value(MESSAGE_PASSWORD_REQUIRED));
        ;
    }

}
