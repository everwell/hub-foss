package com.everwell.registryservice.handler;

import com.everwell.registryservice.BaseTest;
import com.everwell.registryservice.binders.EventFlowBinder;
import com.everwell.registryservice.constants.EventFlowConstants;
import com.everwell.registryservice.handlers.SpringEventsAnalyticsHandler;
import com.everwell.registryservice.models.db.Client;
import com.everwell.registryservice.models.dto.GenericEvents;
import com.everwell.registryservice.service.ClientService;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.messaging.MessageChannel;

import java.time.LocalDateTime;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

public class SpringEventsAnalyticsHandlerTest extends BaseTest {

    @InjectMocks
    SpringEventsAnalyticsHandler springEventsAnalyticsHandler;

    @Mock
    EventFlowBinder eventFlowBinder;
    @Mock
    MessageChannel eventOutput;

    @Mock
    ClientService clientService;

    @Before
    public void init()
    {
        Mockito.when(eventFlowBinder.eventOutput()).thenReturn(eventOutput);
    }

    @Test
    public void trackEvents_GET_ALL_Deployments() {
        GenericEvents<String> genericEvent = new GenericEvents(EventFlowConstants.GET_ALL_DEPLOYMENTS);
        when(clientService.getClientByToken()).thenReturn(new Client(1L, "", "", LocalDateTime.now(), 1L, "www.google.com", ssoUrl, null));
        springEventsAnalyticsHandler.trackEvents(genericEvent);

        Mockito.verify(eventOutput, Mockito.times(1)).send(any());
    }

    @Test
    public void trackEvents_GET_ALL_Deployments_NonNullData() {
        GenericEvents<String> genericEvent = new GenericEvents<String>("Test", EventFlowConstants.GET_ALL_DEPLOYMENTS);
        when(clientService.getClientByToken()).thenReturn(new Client(1L, "", "", LocalDateTime.now(), 1L, "www.google.com", ssoUrl, null));
        springEventsAnalyticsHandler.trackEvents(genericEvent);

        Mockito.verify(eventOutput, Mockito.times(1)).send(any());
    }

}
