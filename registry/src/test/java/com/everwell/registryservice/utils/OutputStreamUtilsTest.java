package com.everwell.registryservice.utils;

import com.everwell.registryservice.BaseTest;
import com.everwell.registryservice.models.http.response.Response;
import com.everwell.registryservice.utils.OutputStreamUtils;
import org.junit.Test;
import org.mockito.InjectMocks;

import org.springframework.http.HttpStatus;
import org.springframework.mock.web.MockHttpServletResponse;

import java.io.IOException;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class OutputStreamUtilsTest extends BaseTest {

    @InjectMocks
    OutputStreamUtils outStreamUtils;

    @Test
    public void testWriteToOutputStream() throws IOException {
        String testMessage = "test response";
        Response<String> customResponse = new Response<>(testMessage);
        HttpStatus status = HttpStatus.OK;
        MockHttpServletResponse httpServletResponse = new MockHttpServletResponse();
        outStreamUtils.writeCustomResponseToOutputStream(httpServletResponse, customResponse, status);
        assertEquals(httpServletResponse.getStatus(),status.value());
        assertEquals(httpServletResponse.getContentType(),"application/json");
        assertTrue(httpServletResponse.getContentAsString().contains(testMessage));
    }

}
