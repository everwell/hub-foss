package com.everwell.registryservice.utils;

import com.everwell.registryservice.models.db.Language;
import com.everwell.registryservice.models.http.response.Response;
import com.everwell.registryservice.utils.Utils;
import org.junit.Assert;
import org.junit.Test;
import org.mockito.Mock;

import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

public class UtilsTest
{
    private static class ClassThatJacksonCannotSerialize {}

    @Test
    public void utils_constructorInvocation() {
        Utils utils = new Utils();
    }

    @Test
    public void asJsonString_Success() {
        String jsonString = Utils.asJsonString(new Response<String>(true, "data", "message"));
        Assert.assertEquals("{\"success\":true,\"message\":\"message\",\"data\":\"data\"}", jsonString);
    }

    @Test(expected = RuntimeException.class)
    public void asJsonString_Exception() {
        //if a class has no public fields/method, we can see exception from jackson
        Utils.asJsonString(new ClassThatJacksonCannotSerialize());
    }

    @Test
    public void formatDate_Success() {
        Date date = Utils.convertStringToDate("2021-11-01", "yyyy-MM-dd");
        Calendar calendar = new GregorianCalendar();
        calendar.setTime(date);
        Assert.assertEquals(2021, calendar.get(Calendar.YEAR));
        Assert.assertEquals(11, calendar.get(Calendar.MONTH) + 1);
        Assert.assertEquals(1, calendar.get(Calendar.DATE));
    }

    @Test(expected = IllegalArgumentException.class)
    public void formatDate_incorrectDate() {
        Utils.convertStringToDate("x", "yydd");
    }

    @Test
    public void convertObject() throws Exception
    {
        String jsonString = "{\"id\":1,\"name\":\"English\",\"translationKey\":\"en\"}";
        Language convertedObject = Utils.convertObject(jsonString, Language.class);
        Assert.assertEquals(1, convertedObject.getId().longValue());
        Assert.assertEquals("English", convertedObject.getName());
        Assert.assertEquals("en", convertedObject.getTranslationKey());
    }
}
