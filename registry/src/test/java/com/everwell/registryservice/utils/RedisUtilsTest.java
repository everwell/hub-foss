package com.everwell.registryservice.utils;

import com.everwell.registryservice.BaseTest;
import com.everwell.registryservice.utils.RedisUtils;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.ValueOperations;

import java.util.concurrent.TimeUnit;

import static org.mockito.Mockito.*;

public class RedisUtilsTest extends BaseTest
{
    RedisUtils redisUtils;

    @Mock
    RedisTemplate<String, Object> redisTemplate;

    @Mock
    private ValueOperations<String, Object> valueOperations;

    @Before
    public void init() {
        redisUtils = new RedisUtils(redisTemplate);
        when(redisTemplate.opsForValue()).thenReturn(valueOperations);
    }


    @Test
    public void getFromCache()
    {
        String cachedValue = "CachedValue";
        String key = "Key";
        doReturn(cachedValue).when(valueOperations).get(key);

        String obtainedCacheValue = redisUtils.getFromCache(key);
        Assert.assertEquals(cachedValue, obtainedCacheValue);
    }

    @Test
    public void getFromCache_NotExists()
    {
        String cachedValue = null;
        String key = "Key";
        doReturn(null).when(valueOperations).get(key);

        String obtainedCacheValue = redisUtils.getFromCache(key);
        Assert.assertEquals(cachedValue, obtainedCacheValue);
    }

    @Test
    public void hasKey()
    {
        String key = "Key";
        doReturn(true).when(redisTemplate).hasKey(key);
        Boolean exists = redisUtils.hasKey(key);
        Assert.assertTrue(exists);
    }

    @Test
    public void hasKey_NotExists()
    {
        String key = "Key";
        doReturn(false).when(redisTemplate).hasKey(key);
        Boolean exists = redisUtils.hasKey(key);
        Assert.assertFalse(exists);
    }

    @Test
    public void putIntoCache()
    {
        String key = "Key";
        String cachedValue = "CachedValue";
        doNothing().when(valueOperations).set(key, cachedValue, 1000L, TimeUnit.SECONDS);
        redisUtils.putIntoCache(key, cachedValue, 1000L);
        verify(valueOperations, Mockito.times(1)).set(key, cachedValue, 1000L, TimeUnit.SECONDS);
    }
}
