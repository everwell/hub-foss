package com.everwell.registryservice.utils;

import com.everwell.registryservice.BaseTest;
import io.jsonwebtoken.Claims;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;


public class JwtUtilsTest extends BaseTest {

    JwtUtils jwtUtils;

    private String jwtToken;

    @Before
    public void init() {
        jwtUtils = new JwtUtils();
        jwtUtils.setSecretKey("1sEI9V56AIYVZjTTLdhX4UihXo331XJBzDHhEGk3a197ahcli4017kbjkcad7TCelRuk9paHmylASTHgI5");
        jwtUtils.setSsoPublicKey("MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAgHAl0ifXDAhN28NvkNLAUc+ZECr5IiFY2iv/RSVV7m7lb/hosA7YAWhq7VbqjztkMPRzUzFyPZooIG3hdgu6uho4gqPfACmk5O6uotU+zHQ1ADzclKjfGfb3uaCU53w379Ajf26Ic6hy9OwOnSZYw9BZ2xF3EUdQS7QjtvpiW50Ok14qcQbRdiJia+yWpl4dwyexGACkKAPEI1+6SFhq7xkTFytsBHvdeIeiEw3dchqGOAKj5m7lVt3BJZhnmnn3dr67BQf41mL2L2atFJl9MARJu7XyFgyScX4iPnXM7ySJrpJrAcGOvjFJdnOTq1s6zWnewLNaJl//+mgvRG+CawIDAQAB");

        jwtToken = jwtUtils.generateToken(String.valueOf(clientId_Nikshay));
    }

    @Test
    public void testJwtParsingClientToken_Success() {
        Claims claims = jwtUtils.getParsedClaims(jwtToken);
        Assert.assertEquals(claims.getSubject(), clientId_Nikshay.toString());
    }

}
