package com.everwell.registryservice.mappers;

import com.everwell.registryservice.models.dto.SidebarItemResponse;
import org.junit.Before;
import org.junit.Test;

import java.util.List;

import static junit.framework.TestCase.assertNull;

public class SidebarMapperTest {

    private SidebarItemMapper sidebarItemMapper;

    @Before
    public void before() {
        sidebarItemMapper = new SidebarItemMapperImpl();
    }

    @Test
    public void testSidebarItemResponseNull() {
        SidebarItemResponse sidebaritemResponse = sidebarItemMapper.sidebarItemToSidebarItemResponse(null);
        assertNull(sidebaritemResponse);
    }

    @Test
    public void testSidebarItemResponseListNull() {
        List<SidebarItemResponse> sidebaritemResponseList = sidebarItemMapper.sidebarItemListToSidebarItemResponseList(null);
        assertNull(sidebaritemResponseList);
    }

}
