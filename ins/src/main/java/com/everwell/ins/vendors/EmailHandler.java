package com.everwell.ins.vendors;

import com.everwell.ins.constants.Constants;
import com.everwell.ins.enums.EmailGateway;
import com.everwell.ins.exceptions.InternalServerErrorException;
import com.everwell.ins.exceptions.ValidationException;
import com.everwell.ins.models.db.EmailLogs;
import com.everwell.ins.models.dto.EmailTemplateDto;
import com.everwell.ins.models.dto.SendgridResponseDto;
import com.everwell.ins.models.http.requests.EmailRequest;
import com.everwell.ins.repositories.EmailLogsRepository;
import com.everwell.ins.repositories.EmailTemplateRepository;
import com.everwell.ins.services.VendorService;
import com.google.common.collect.Lists;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.scheduling.annotation.Async;

import java.io.IOException;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CompletableFuture;


public abstract class EmailHandler {

    private static Logger LOGGER = LoggerFactory.getLogger(EmailHandler.class);

    @Value("${ins.is.mock}")
    protected Boolean isMock;

    @Value("${ins.base.url}")
    protected String baseUrl;

    @Autowired
    protected VendorService vendorService;

    @Autowired
    protected EmailLogsRepository emailLogsRepository;

    @Autowired
    protected EmailTemplateRepository emailTemplateRepository;

    @Autowired
    private ApplicationEventPublisher applicationEventPublisher;

    public void sendEmail(EmailRequest emailRequest) throws ValidationException, IOException {
        setVendorParams(emailRequest.getVendorId());
        applicationEventPublisher.publishEvent(emailRequest);
        List<List<EmailTemplateDto>> batches = createBatches(emailRequest);
        List<EmailLogs> emailLogCollection = new ArrayList<>();
        List<CompletableFuture<List<EmailLogs>>> completableFutureList = new ArrayList<>();
        for (List<EmailTemplateDto> batch : batches) {
            completableFutureList.add(processEmail(batch, emailRequest));
        }
        CompletableFuture.allOf(completableFutureList.toArray(new CompletableFuture[0])).join();

        completableFutureList.forEach(f -> {
            try {
                emailLogCollection.addAll(f.get());
            } catch (Exception e) {
                LOGGER.error("Unable to add to logs", e.toString());
                throw new InternalServerErrorException("Internal Server Error, Try again later!");
            }
        });
        emailLogsRepository.saveAll(emailLogCollection);
    }

    public abstract void setVendorParams(Long vendorId);

    public List<List<EmailTemplateDto>> createBatches(EmailRequest emailRequest) {
        return Lists.partition(emailRequest.getRecipientList(), getBatchSize(emailRequest.getIsEmailCommon()));
    }

    public Integer getBatchSize(Boolean isEmailCommon) {
        return Constants.BATCH_SIZE_UNIQUE_EMAIL;
    }

    @Async
    public CompletableFuture<List<EmailLogs>> processEmail(List<EmailTemplateDto> emailTemplateDtos, EmailRequest emailRequest) throws IOException {
        List<EmailLogs> emailLogs = new ArrayList<>();
        SendgridResponseDto responseDto = new SendgridResponseDto();
        if (!isMock) {
            responseDto = sendgridCall(emailTemplateDtos, emailRequest);
        }
        emailLogs.addAll(createLogs(emailTemplateDtos, responseDto, emailRequest));
        return CompletableFuture.completedFuture(emailLogs);
    }

    protected abstract SendgridResponseDto sendgridCall(List<EmailTemplateDto> recipients, EmailRequest emailRequest) throws IOException;

    public abstract EmailGateway getVendorGatewayEnumerator();

    public List<EmailLogs> createLogs(List<EmailTemplateDto> recipients, SendgridResponseDto responseDto, EmailRequest emailRequest) {
        List<EmailLogs> emailLogs = new ArrayList<>();
        if (isMock) {
            responseDto.setSendgridResponse(Constants.TEST);
        }
        LocalDateTime addedOn = LocalDateTime.now();
        recipients.forEach(r -> {
            String sendgridResponse = responseDto.getSendgridResponse();
            emailLogs.add(new EmailLogs(emailRequest.getVendorId(), emailRequest.getTemplateId(), emailRequest.getTriggerId(), r.getRecipient(),emailRequest.getSender(),r.getBody(),r.getSubject(),sendgridResponse,addedOn));
        });
        return emailLogs;
    }
}
