package com.everwell.ins.vendors;

import com.everwell.ins.analytics.SpringEventAnalytics;
import com.everwell.ins.constants.Constants;
import com.everwell.ins.enums.Language;
import com.everwell.ins.enums.SmsGateway;
import com.everwell.ins.exceptions.InternalServerErrorException;
import com.everwell.ins.models.db.SmsLogs;
import com.everwell.ins.models.dto.SmsDto;
import com.everwell.ins.models.http.requests.SmsRequest;
import com.everwell.ins.models.dto.VendorResponseDto;
import com.everwell.ins.repositories.SmsLogsRepository;
import com.everwell.ins.services.VendorService;
import com.everwell.ins.utils.Utils;
import com.google.common.collect.Lists;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.scheduling.annotation.Async;
import org.springframework.util.StringUtils;

import java.util.*;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;

import static com.everwell.ins.constants.Constants.DEFAULT_TEMPLATE_ID;
import static com.everwell.ins.constants.Constants.TEST_PREFIX;

public abstract class SmsHandler {

    private static Logger LOGGER = LoggerFactory.getLogger(SmsHandler.class);

    @Value("${ins.is.mock}")
    protected Boolean isMock;

    @Value("${ins.base.url}")
    protected String baseUrl;

    @Autowired
    protected VendorService vendorService;

    @Autowired
    protected SmsLogsRepository smsLogsRepository;

    @Autowired
    private ApplicationEventPublisher applicationEventPublisher;


    public void sendSms(SmsRequest smsRequest) {
        setVendorParams(smsRequest.getVendorId());
        preprocessParams(smsRequest);
        applicationEventPublisher.publishEvent(smsRequest);
        List<List<SmsDto>> batches = createBatches(smsRequest);
        List<SmsLogs> smsLogCollection = new ArrayList<>();

        List<CompletableFuture<List<SmsLogs>>> completableFutureList = new ArrayList<>();
        for (List<SmsDto> batch : batches) {
            completableFutureList.add(processSms(batch, smsRequest.getLanguage(), smsRequest.getVendorId(), smsRequest.getTemplateId(), smsRequest.getTriggerId()));
        }
        CompletableFuture.allOf(completableFutureList.toArray(new CompletableFuture[0])).join();

        completableFutureList.forEach(f -> {
            try {
                smsLogCollection.addAll(f.get());
            } catch (InterruptedException | ExecutionException e) {
                String errorMessage = "Unable to save SMS Logs,  exception: " + e;
                LOGGER.error("[SaveLogs] " + errorMessage);
                throw new InternalServerErrorException("Internal Server Error, Try again later!");
            } catch(Exception e) {
                String errorMessage = "SMS Logs Not Stored,  exception: " + e;
                LOGGER.error("[SaveLogs] " + errorMessage);
                throw new InternalServerErrorException("Internal Server Error, Try again later!");
            }
        });
        smsLogsRepository.saveAll(smsLogCollection);

    }

    @Async
    public CompletableFuture<List<SmsLogs>> processSms(List<SmsDto> smsDto, Language language, Long vendorId, Long templateId, Long triggerId) {
        List<SmsLogs> smsLogCollection = new ArrayList<>();
        VendorResponseDto responseDto = new VendorResponseDto();
        Boolean startsWithTest = smsDto.get(0).getMessage().startsWith(TEST_PREFIX);
        if (!isMock || startsWithTest) {
            responseDto = vendorCall(smsDto, language, templateId);
        }
        smsLogCollection.addAll(createLogs(smsDto, responseDto, vendorId, templateId, triggerId));
        return CompletableFuture.completedFuture(smsLogCollection);
    }

    public SmsRequest preprocessParams(SmsRequest smsRequest) {
        smsRequest.setMessage(Utils.trimMessage(smsRequest.getMessage()));
        smsRequest.getPersonList().forEach(p -> {
            p.setMessage((smsRequest.getIsMessageCommon()) ? smsRequest.getMessage() : Utils.trimMessage(p.getMessage()));
        });
        return smsRequest;
    }

    public abstract SmsGateway getVendorGatewayEnumerator();

    public abstract void setVendorParams(Long vendorId);

    public abstract VendorResponseDto vendorCall(List<SmsDto> persons, Language language, Long templateId);

    public List<List<SmsDto>> createBatches(SmsRequest smsRequest) {
        return Lists.partition(smsRequest.getPersonList(), getBatchSize(smsRequest.getIsMessageCommon()));
    }

    public abstract String getLanguageMapping(Language language);

    public Integer getBatchSize(Boolean isMessageCommon) {
        return Constants.BATCH_SIZE_UNIQUE_SMS;
    }

    public List<SmsLogs> createLogs(List<SmsDto> persons, VendorResponseDto responseDto, Long vendorId, Long templateId, Long triggerId) {
        List<SmsLogs> smsLogCollection = new ArrayList<>();
        if (isMock) {
            responseDto.setApiResponse(Constants.TEST);
            responseDto.setMessageId(Constants.TEST);
        }
        Date addedOn = new Date();
        persons.forEach(p -> {
            String apiResponse = responseDto.getApiResponse();
            smsLogCollection.add(new SmsLogs(vendorId, templateId, triggerId, p.getId(), p.getPhone(), p.getMessage(), addedOn, apiResponse, responseDto.getMessageId()));
        });
        return smsLogCollection;
    }

    public Map<String, String> fetchExtraData(String vendor, String phone, String message, Long templateId) {
        String template = templateId != null ? templateId.toString() : DEFAULT_TEMPLATE_ID ;
        Map<String, String> extraData = new HashMap<String, String>() {{
            put("vendor", vendor);
            put("phone", phone);
            put("message", message);
            put("templateId", template);
        }};
        return extraData;
    }

}
