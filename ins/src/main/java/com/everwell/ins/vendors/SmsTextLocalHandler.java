package com.everwell.ins.vendors;

import com.everwell.ins.enums.Language;
import com.everwell.ins.enums.SmsGateway;
import com.everwell.ins.exceptions.ValidationException;
import com.everwell.ins.exceptions.VendorException;
import com.everwell.ins.models.db.SmsLogs;
import com.everwell.ins.models.dto.SmsDto;
import com.everwell.ins.models.dto.vendorConfigs.TextLocalConfigDto;
import com.everwell.ins.models.dto.vendorCreds.ApiKeySenderDto;
import com.everwell.ins.models.dto.VendorResponseDto;
import com.everwell.ins.utils.HttpUtils;
import com.everwell.ins.exceptions.NotImplementedException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.stereotype.Component;

import java.net.URLEncoder;
import java.time.Duration;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.stream.Collectors;

@Component
@Scope(value = "prototype", proxyMode = ScopedProxyMode.TARGET_CLASS)
public class SmsTextLocalHandler extends SmsHandler {

    private static Logger LOGGER = LoggerFactory.getLogger(SmsTextLocalHandler.class);

    private TextLocalConfigDto config;

    private ApiKeySenderDto creds;

    @Override
    public void setVendorParams(Long vendorId) {
        config = vendorService.getVendorConfig(vendorId, TextLocalConfigDto.class);
        creds = vendorService.getVendorCredentials(vendorId, ApiKeySenderDto.class);
    }

    @Override
    public VendorResponseDto vendorCall(List<SmsDto> persons, Language language, Long templateId) {
        return null;
    }

    @Override
    public SmsGateway getVendorGatewayEnumerator() {
        return SmsGateway.TEXTLOCAL;
    }

    @Override
    public String getLanguageMapping(Language language) {
        throw new NotImplementedException("Unimplemented method");
    }

    @Override
    public CompletableFuture<List<SmsLogs>> processSms(List<SmsDto> smsDto, Language language, Long vendorId, Long templateId, Long triggerId) {
        List<SmsLogs> smsLogCollection = new ArrayList<>();
        VendorResponseDto responseDto = new VendorResponseDto();
        smsLogCollection.addAll(createLogs(smsDto, responseDto, vendorId, templateId, triggerId));
        smsLogCollection = smsLogsRepository.saveAll(smsLogCollection);
        if (!isMock) {
            responseDto = vendorCallTextLocal(smsDto, language, smsLogCollection.get(0).getId());
        }
        final VendorResponseDto finalResponseDto = responseDto;
        smsLogCollection.forEach(smsLogs -> {
            smsLogs.setApiResponse(finalResponseDto.getApiResponse());
            smsLogs.setMessageId(finalResponseDto.getMessageId());
        });
        return CompletableFuture.completedFuture(smsLogCollection);
    }

    public VendorResponseDto vendorCallTextLocal(List<SmsDto> persons, Language language, Long customId) {
        if (null == persons) {
            LOGGER.error("[vendorCall] The person list is empty");
            throw new ValidationException("The person list is empty");
        }
        VendorResponseDto responseDto = new VendorResponseDto();
        String phone = persons.stream().map(SmsDto::getPhone)
                .collect(Collectors.joining(","));
        String message = persons.get(0).getMessage();
        Boolean isUnicode = language == Language.UNICODE ? Boolean.TRUE : Boolean.FALSE;
        try {
            String apiKey = "?apikey=" + URLEncoder.encode(creds.getApiKey(), "UTF-8");
            String finalMessage = "&message=" + URLEncoder.encode(message, "UTF-8");
            String sender = "&sender=" + URLEncoder.encode(creds.getSender(), "UTF-8");
            String numbers = "&numbers=" + URLEncoder.encode(phone, "UTF-8");
            String custom = "&custom=" + URLEncoder.encode(customId.toString(), "UTF-8");
            String unicode = "&unicode=" + URLEncoder.encode(isUnicode.toString(), "UTF-8");

            LocalDateTime start = LocalDateTime.now();
            String response = HttpUtils.getRequest(creds.getUrl() + apiKey + numbers + finalMessage + sender + custom + unicode, null);
            responseDto.setApiResponse(response);
            responseDto.setMessageId(customId.toString());
            Long elapsed = Duration.between(start, LocalDateTime.now()).toMillis();
            LOGGER.info("[VendorCall] :  Sent SMS " + message + " to " + phone + " took " + elapsed + "ms, response = " + responseDto.getApiResponse());
        } catch (Exception e) {
            String errorMessage = "TextLocal unable to send SMS to " + phone + ", exception: " + e;
            LOGGER.error("[vendorCall] " + errorMessage);
            throw new VendorException(errorMessage, fetchExtraData(getVendorGatewayEnumerator().toString(), phone, message, customId));
        }
        return responseDto;
    }
}

