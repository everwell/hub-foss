package com.everwell.ins.vendors;

import com.everwell.ins.constants.Constants;
import com.everwell.ins.enums.Language;
import com.everwell.ins.enums.SmsGateway;
import com.everwell.ins.exceptions.ValidationException;
import com.everwell.ins.exceptions.VendorException;
import com.everwell.ins.models.dto.SmsDto;
import com.everwell.ins.models.dto.VendorResponseDto;
import com.everwell.ins.models.dto.vendorConfigs.ADNConfigDto;
import com.everwell.ins.models.dto.vendorCreds.ApiKeySecretDto;
import com.everwell.ins.models.http.responses.ADNSmsResponse;
import com.everwell.ins.utils.HttpUtils;
import com.everwell.ins.utils.Utils;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.stereotype.Component;

import java.time.Duration;
import java.time.LocalDateTime;
import java.util.*;
import java.util.stream.Collectors;

@Component
@Scope(value = "prototype", proxyMode = ScopedProxyMode.TARGET_CLASS)
public class SmsADNHandler extends SmsHandler {

    private static Logger LOGGER = LoggerFactory.getLogger(SmsADNHandler.class);

    private ApiKeySecretDto creds;
    private ADNConfigDto config;


    @Override
    public SmsGateway getVendorGatewayEnumerator() {
        return SmsGateway.ADN;
    }

    @Override
    public void setVendorParams(Long vendorId) {
        config = vendorService.getVendorConfig(vendorId, ADNConfigDto.class);
        creds = vendorService.getVendorCredentials(vendorId, ApiKeySecretDto.class);
    }

    @Override
    public String getLanguageMapping(Language language) {
        return null;
    }

    @Override
    public Integer getBatchSize(Boolean isMessageCommon) {
        return (isMessageCommon && null != config.getBulkSmsLimit()) ? Integer.parseInt(config.getBulkSmsLimit()) : Constants.BATCH_SIZE_UNIQUE_SMS;
    }

    @Override
    public VendorResponseDto vendorCall(List<SmsDto> persons, Language language, Long templateId) {
        if (null == persons) {
            LOGGER.error("[vendorCall] The person list is empty");
            throw new ValidationException("The person list is empty");
        }

        VendorResponseDto responseDto = new VendorResponseDto();
        String phone = Utils.removePrefix(persons.get(0).getPhone(), "+");
        String message = persons.get(0).getMessage();

        try {
            JSONObject values = new JSONObject();
            values.put("message_body", message);
            values.put("mobile", phone);
            values.put("message_type", "TEXT");
            values.put("request_type", "SINGLE_SMS");
            values.put("api_secret", creds.getApiSecret());
            values.put("api_key", creds.getApiKey());
            LocalDateTime start = LocalDateTime.now();
            String response = HttpUtils.postJsonRequest(creds.getUrl(), values.toString(), null);
            ADNSmsResponse responseObject = Utils.jsonToObject(response, ADNSmsResponse.class);
            responseDto.setApiResponse(response);
            responseDto.setMessageId(responseObject.getSmsUID());
            Long elapsed = Duration.between(start, LocalDateTime.now()).toMillis();
            LOGGER.info("[VendorCall] :  Sent SMS " + message + " to " + phone + " took " + elapsed.toString() + "ms, response = " + responseDto.getApiResponse());

        } catch (Exception e) {
            String errorMessage = "ADN unable to send SMS to " + phone + ", exception: " + e;
            LOGGER.error("[vendorCall] " + errorMessage);
            throw new VendorException(errorMessage, fetchExtraData(getVendorGatewayEnumerator().toString(), phone, message, templateId));
        }
        return responseDto;
    }
}
