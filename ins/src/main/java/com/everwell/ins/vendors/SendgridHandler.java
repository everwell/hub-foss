package com.everwell.ins.vendors;

import com.everwell.ins.constants.Constants;
import com.everwell.ins.enums.EmailGateway;
import com.everwell.ins.exceptions.ValidationException;
import com.everwell.ins.models.dto.EmailTemplateDto;
import com.everwell.ins.models.dto.SendgridResponseDto;
import com.everwell.ins.models.dto.vendorConfigs.SendgridConfigDto;
import com.everwell.ins.models.dto.vendorCreds.ApiKeyDto;
import com.everwell.ins.models.http.requests.EmailRequest;
import com.everwell.ins.utils.SentryUtils;
import com.sendgrid.*;
import com.sendgrid.helpers.mail.Mail;
import com.sendgrid.helpers.mail.objects.Attachments;
import com.sendgrid.helpers.mail.objects.Content;
import com.sendgrid.helpers.mail.objects.Email;
import com.sendgrid.helpers.mail.objects.Personalization;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.io.IOException;
import java.util.Base64;
import java.util.List;

@Component
@Scope(value = "prototype", proxyMode = ScopedProxyMode.TARGET_CLASS)
public class SendgridHandler extends EmailHandler {

    private static Logger LOGGER = LoggerFactory.getLogger(SendgridHandler.class);

    private SendgridConfigDto config;

    private ApiKeyDto creds;

    @Value("${server.port}")
    private String serverPort;

    @PostConstruct
    public void setBaseUrl() {
        this.baseUrl = "http://" + baseUrl + ":" + serverPort;
    }

    @Override
    public void setVendorParams(Long vendorId) {
        config = vendorService.getVendorConfig(vendorId, SendgridConfigDto.class);
        creds = vendorService.getVendorCredentials(vendorId, ApiKeyDto.class);
    }

    @Override
    public Integer getBatchSize(Boolean isEmailCommon) {
        return (isEmailCommon && null != config.getBulkEmailLimit()) ? Integer.parseInt(config.getBulkEmailLimit()) : Constants.BATCH_SIZE_UNIQUE_EMAIL;
    }

    @Override
    public EmailGateway getVendorGatewayEnumerator() {
        return EmailGateway.SENDGRID;
    }

    @Override
    public SendgridResponseDto sendgridCall(List<EmailTemplateDto> recipients, EmailRequest emailRequest) throws IOException {
        if (null == recipients) {
            LOGGER.error("[vendorCall] The recipient list is empty");
            throw new ValidationException("The recipient list is empty");
        }
        SendgridResponseDto responseDto = new SendgridResponseDto();
        String subject = recipients.get(0).getSubject();
        String body = recipients.get(0).getBody();
        try
        {
            Content content = new Content("text/html",body);
            SendGrid sendGrid = new SendGrid(creds.getApiKey());
            Personalization personalization = new Personalization();
            recipients.forEach(r -> {
                Email receiver = new Email(r.getRecipient());
                personalization.addTo(receiver);
            });
            Email sender = new Email(emailRequest.getSender());
            Mail mail = new Mail();
            mail.setSubject(subject);
            mail.addContent(content);
            mail.setFrom(sender);
            mail.addPersonalization(personalization);
            if(null != emailRequest.getAttachmentDto())
            {
                byte [] contentStream = emailRequest.getAttachmentDto().getAttachmentContent().getBytes();
                String attachmentContent = Base64.getMimeEncoder().encodeToString(contentStream);
                String filename = emailRequest.getAttachmentDto().getAttachmentName() + emailRequest.getAttachmentDto().getAttachmentType();
                Attachments attachments = new Attachments();
                attachments.setContent(attachmentContent);
                attachments.setFilename(filename);
                attachments.setDisposition("attachment");
                mail.addAttachments(attachments);
            }
            Request request = new Request();
            request.setMethod(Method.POST);
            request.setEndpoint("mail/send");
            request.setBody(mail.build());
            Response response = sendGrid.api(request);
            responseDto.setSendgridResponse(String.valueOf(response.getStatusCode()));
        }
        catch (Exception e) {
            LOGGER.error("[vendorCall] Sendgrid unable to send Email, exception: " + e.toString());
            SentryUtils.captureException(e);
        }
        return responseDto;
    }
}
