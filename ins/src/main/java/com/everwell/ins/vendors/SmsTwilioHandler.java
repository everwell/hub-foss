package com.everwell.ins.vendors;

import com.everwell.ins.enums.Language;
import com.everwell.ins.enums.SmsGateway;
import com.everwell.ins.exceptions.ValidationException;
import com.everwell.ins.exceptions.VendorException;
import com.everwell.ins.models.dto.SmsDto;
import com.everwell.ins.models.dto.vendorConfigs.TwilioConfigDto;
import com.everwell.ins.models.dto.vendorCreds.AuthTokenFromDto;
import com.everwell.ins.models.dto.VendorResponseDto;
import com.twilio.Twilio;
import com.twilio.rest.api.v2010.account.Message;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.net.URI;
import java.time.Duration;
import java.time.LocalDateTime;
import java.util.List;

@Component
@Scope(value = "prototype", proxyMode = ScopedProxyMode.TARGET_CLASS)
public class SmsTwilioHandler extends SmsHandler {

    private static Logger LOGGER = LoggerFactory.getLogger(SmsTwilioHandler.class);

    private TwilioConfigDto config;

    private AuthTokenFromDto creds;

    @Value("${server.port}")
    private String serverPort;

    @PostConstruct
    public void setBaseUrl() {
        this.baseUrl = "https://" + baseUrl;
    }

    @Override
    public void setVendorParams(Long vendorId) {
        config = vendorService.getVendorConfig(vendorId, TwilioConfigDto.class);
        creds = vendorService.getVendorCredentials(vendorId, AuthTokenFromDto.class);
    }

    @Override
    public SmsGateway getVendorGatewayEnumerator() {
        return SmsGateway.TWILIO;
    }

    @Override
    public String getLanguageMapping(Language language) {
        String code = config.getLanguage();
        switch (language) {
            case UNICODE:
                code = config.getUnicodeLanguage();
                break;
        }
        return code;
    }

    @Override
    public VendorResponseDto vendorCall(List<SmsDto> persons, Language language, Long templateId) {
        if (null == persons) {
            LOGGER.error("[vendorCall] The person list is empty");
            throw new ValidationException("The person list is empty");
        }
        VendorResponseDto responseDto = new VendorResponseDto();
        String phone = persons.get(0).getPhone();
        String message = persons.get(0).getMessage();
        try {
            LocalDateTime start = LocalDateTime.now();
            Twilio.init(creds.getAccountId(), creds.getAuthToken());
            Message response = Message.creator(
                            new com.twilio.type.PhoneNumber(phone), // to
                            new com.twilio.type.PhoneNumber(creds.getFrom()), // from
                            message)
                    .setStatusCallback(URI.create(baseUrl + config.getReconciliation()))
                    .setSmartEncoded(Boolean.parseBoolean(getLanguageMapping(language)))
                    .create();

            responseDto.setApiResponse(response.getAccountSid());
            responseDto.setMessageId(response.getSid());
            Long elapsed = Duration.between(start, LocalDateTime.now()).toMillis();
            LOGGER.info("[vendorCall] :  Sent SMS " + message + " to " + phone + " took " + elapsed + "ms, response = " + responseDto.getApiResponse());
        } catch (Exception e) {
            String errorMessage = "Twilio unable to send SMS to " + phone + ", exception: " + e;
            LOGGER.error("[vendorCall] " + errorMessage);
            throw new VendorException(errorMessage, fetchExtraData(getVendorGatewayEnumerator().toString(), phone, message, templateId));
        }
        return responseDto;
    }
}

