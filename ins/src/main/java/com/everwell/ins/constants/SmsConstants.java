package com.everwell.ins.constants;

public class SmsConstants {

    public static String ORIGINATING_NO = "originatingNo";
    public static String DESTINATION_NO = "destinationNo";
    public static String REFERENCE_NO = "referenceNo";
    public static String SENDER_ID = "senderId";
    public static String MESSAGE_ID = "messageId";
    public static String MESSAGE_CONTENT = "messageContent";
    public static String DATE_CREATED = "dateCreated";
    public static String USER_CHANNEL_TYPE = "userChannelType";
    public static String X_AUTH_KEY = "x-authKey";
}
