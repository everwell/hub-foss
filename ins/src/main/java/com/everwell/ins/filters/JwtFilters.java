package com.everwell.ins.filters;

import com.everwell.ins.models.db.Client;
import com.everwell.ins.services.ClientService;
import com.everwell.ins.utils.JwtUtils;
import com.everwell.ins.utils.Utils;
import io.jsonwebtoken.ExpiredJwtException;
import io.jsonwebtoken.UnsupportedJwtException;
import io.jsonwebtoken.security.SignatureException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.WebAuthenticationDetailsSource;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@Component
public class JwtFilters extends OncePerRequestFilter {

    @Autowired
    ClientService clientService;

    @Override
    protected void doFilterInternal(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, FilterChain filterChain) throws ServletException, IOException {
        String authorizationHeader = httpServletRequest.getHeader("Authorization");
        try {
            if (authorizationHeader != null && authorizationHeader.startsWith("Bearer ") && !authorizationHeader.equals("Bearer undefined")) {
                String jwt = authorizationHeader.substring(7);
                String clientId = Utils.getJwtPayload(jwt).getSub();
                Client client = clientService.getClient(Long.valueOf(clientId));
                if (JwtUtils.isValidToken(jwt, client.getPassword() + client.getName() + clientId)) {
                    UsernamePasswordAuthenticationToken usernamePasswordAuthenticationToken = new UsernamePasswordAuthenticationToken(client, null, Collections.emptyList());
                    usernamePasswordAuthenticationToken
                            .setDetails(new WebAuthenticationDetailsSource().buildDetails(httpServletRequest));
                    SecurityContextHolder.getContext().setAuthentication(usernamePasswordAuthenticationToken);
                }
            }
        } catch (IllegalArgumentException | UnsupportedJwtException e) {
            Utils.writeCustomResponseToOutputStream(httpServletResponse, e, HttpStatus.BAD_REQUEST);
            return;
        } catch (ExpiredJwtException | SignatureException | ArrayIndexOutOfBoundsException e) {
            Utils.writeCustomResponseToOutputStream(httpServletResponse, e, HttpStatus.UNAUTHORIZED);
            return;
        } catch (Exception e) {
            Utils.writeCustomResponseToOutputStream(httpServletResponse, e, HttpStatus.INTERNAL_SERVER_ERROR);
            return;
        }
        filterChain.doFilter(httpServletRequest, httpServletResponse);
    }

    @Override
    protected boolean shouldNotFilter(HttpServletRequest request) {
        List<String> shouldNotFilterUrls = new ArrayList<>();
        shouldNotFilterUrls.add("/v1/client");
        return shouldNotFilterUrls.contains(request.getRequestURI());
    }

}
