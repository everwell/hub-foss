package com.everwell.ins.utils;

import com.everwell.ins.constants.Constants;
import io.sentry.Sentry;
import io.sentry.SentryEvent;
import io.sentry.protocol.Request;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.context.request.ServletWebRequest;
import org.springframework.web.context.request.WebRequest;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

public class SentryUtils {

    @Value("${sentry.release.version}")
    private static String releaseVersion;

    static {
        init();
    }

    public static void init() {
        Sentry.init(options -> {
            options.setEnableExternalConfiguration(true);
        });
    }

    /**
     * Method to build SentryEvent which can be then be captured
     *
     * @param ex - Exception which needs to be captured in sentry event
     * @return sentry event corresponding to given error
     */
    private static SentryEvent eventBuilder(Exception ex) {
        SentryEvent sentryEvent = new SentryEvent();
        sentryEvent.setThrowable(ex);
        sentryEvent.setRelease(releaseVersion);
        return sentryEvent;
    }

    /**
     * Method to capture exception
     *
     * @param e - Exception which is to be captured
     */
    public static void captureException(Exception e) {
        SentryEvent event = eventBuilder(e);
        Sentry.captureEvent(event);
    }

    /**
     * Method to capture exception along with extra data
     *
     * @param e    - Exception which is to be captured
     * @param data - Extra data to be captured along with exception
     */
    public static void captureException(Exception e, String data) {
        SentryEvent event = eventBuilder(e);
        if (!StringUtils.isEmpty(data)) {
            event.setExtra(Constants.DATA, data);
        }
        Sentry.captureEvent(event);
    }

    /**
     * Method to capture exception along with the web request
     *
     * @param e       - Exception which is to be captured
     * @param request - webRequest which is received
     * @throws IOException - throws IOException in case of reading invalid data
     */
    public static void captureException(Exception e, WebRequest request) throws IOException {
        SentryEvent sentryEvent = eventBuilder(e);
        sentryEvent.setRequest(formatWebRequest(request));
        Sentry.captureEvent(sentryEvent);
    }

    /**
     * Method to format webRequest to sentry Request object
     *
     * @param webRequest - webRequest which is received
     * @return - Converted Request object
     * @throws IOException - throws IOException in case of reading invalid data
     */
    private static Request formatWebRequest(WebRequest webRequest) throws IOException {
        HttpServletRequest httpServletRequest = ((ServletWebRequest) webRequest).getRequest();
        Request request = new Request();
        Map<String, String> headers = new HashMap<>();
        webRequest.getHeaderNames().forEachRemaining(h -> headers.put(h, webRequest.getHeader(h)));
        request.setUrl(httpServletRequest.getRequestURI());
        request.setQueryString(httpServletRequest.getQueryString());
        request.setHeaders(headers);
        request.setMethod(httpServletRequest.getMethod());
        request.setData(Utils.readRequestBody(httpServletRequest.getInputStream()));
        return request;
    }
}
