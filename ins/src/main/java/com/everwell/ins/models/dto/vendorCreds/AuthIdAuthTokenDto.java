package com.everwell.ins.models.dto.vendorCreds;

import lombok.*;

@Data
public class AuthIdAuthTokenDto extends VendorCredsBase {
    String srcNumber;
    String authId;
    String authToken;
}
