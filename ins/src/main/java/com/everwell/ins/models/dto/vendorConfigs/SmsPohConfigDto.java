package com.everwell.ins.models.dto.vendorConfigs;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class SmsPohConfigDto extends VendorConfigsBase{
    private String bulkSmsLimit;
}
