package com.everwell.ins.models.http.responses;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@ToString
@Getter
@JsonInclude(JsonInclude.Include.NON_NULL)
public class EpisodeUnreadNotificationResponse {

    public List<EpisodeNotificationResponse> unreadNotificationsList = null;
    public int count = 0;
}
