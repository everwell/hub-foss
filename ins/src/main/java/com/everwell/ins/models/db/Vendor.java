package com.everwell.ins.models.db;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

@Entity
@Table(name = "ins_vendor")
@Getter
@AllArgsConstructor
@NoArgsConstructor
public class Vendor {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Setter
    private Long id;
    @Column(name = "gateway")
    private String gateway;
    @Column(name = "credential_type")
    private String credentialType;
    @Column(name = "config",columnDefinition="TEXT")
    private String config;
    @Column(name = "credentials",columnDefinition="TEXT")
    private String credentials;
    @Column(name = "reconciliation_type", columnDefinition = "TEXT")
    private String reconciliationType;

    public Vendor (Long id, String gateway, String credentialType, String config, String credentials) {
        this.id = id;
        this.gateway = gateway;
        this.credentialType = credentialType;
        this.config = config;
        this.credentials = credentials;
    }
}
