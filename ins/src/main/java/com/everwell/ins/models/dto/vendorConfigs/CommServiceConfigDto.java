package com.everwell.ins.models.dto.vendorConfigs;

import lombok.*;

@Data
public class CommServiceConfigDto extends VendorConfigsBase {
    String bulkSmsLimit;
    String deploymentCode;
    String smsGateway;
}
