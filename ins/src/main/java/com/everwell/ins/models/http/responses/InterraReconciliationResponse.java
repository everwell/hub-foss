package com.everwell.ins.models.http.responses;

import com.everwell.ins.models.dto.InterraReconciliationResponseDto;
import com.fasterxml.jackson.annotation.JsonAlias;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@NoArgsConstructor
@Getter
@Setter
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class InterraReconciliationResponse {
    @JsonAlias(value = "tList")
    List<InterraReconciliationResponseDto> tList;
}
