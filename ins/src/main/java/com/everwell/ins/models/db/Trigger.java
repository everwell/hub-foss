package com.everwell.ins.models.db;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

@Entity
@Table(name = "ins_trigger")
@Getter
@AllArgsConstructor
@NoArgsConstructor
public class Trigger {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Setter
    private Long id;
    @Column(name = "trigger_name", columnDefinition = "TEXT")
    private String triggerName;
    @Column(name = "trigger", columnDefinition = "TEXT")
    private String trigger;
    @Column(name = "parameters", columnDefinition = "TEXT")
    private String parameters;
    @Column(name = "template_id")
    private Long templateId;
}
