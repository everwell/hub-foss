package com.everwell.ins.models.dto.vendorConfigs;

import lombok.*;

@Data
public class SendgridConfigDto extends VendorConfigsBase{
    String bulkEmailLimit;
}
