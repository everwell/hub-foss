package com.everwell.ins.models.http.requests;

import com.everwell.ins.constants.Constants;
import com.everwell.ins.exceptions.ValidationException;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;
import java.util.Map;

@Data
@NoArgsConstructor
public class NotificationLogRequest {
    Long episodeId;
    List<Notification> notifications;

    @Data
    public static class Notification {
        String deviceId;
        String firebaseResponse;
        String message;
        Long vendorId;
        Long triggerId;
        Long templateId;
    }

    public void validate() {
        if (episodeId == null || notifications == null) {
            throw new ValidationException("Episode Id and notification data cannot be empty!");
        }
    }
}
