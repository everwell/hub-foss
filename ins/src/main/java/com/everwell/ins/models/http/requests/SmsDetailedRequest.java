package com.everwell.ins.models.http.requests;

import com.everwell.ins.exceptions.ValidationException;
import com.everwell.ins.models.dto.SmsTemplateDto;
import lombok.*;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;

import java.util.List;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@ToString
public class SmsDetailedRequest {
    Long templateId;
    List<Long> templateIds;
    List<SmsTemplateDto> personList;
    Long vendorId;
    Long triggerId;
    Boolean isMandatory;
    Boolean isDefaultTime;
    Boolean defaultConsent;
    String timeOfSms;
    List<Integer> patientList;
    Integer defaultLanguage;
    Long clientId;

     public void validate(SmsDetailedRequest smsTemplateRequest, String clientId) {
        if (CollectionUtils.isEmpty(smsTemplateRequest.getPersonList())) {
            throw new ValidationException("personList should not be empty");
        }
        if (null == smsTemplateRequest.getVendorId()) {
            throw new ValidationException("vendor id is required");
        }
        if (null == smsTemplateRequest.getTemplateId()) {
            throw new ValidationException("template id is required");
        }
         if (null == smsTemplateRequest.getTriggerId()) {
             throw new ValidationException("trigger id is required");
         }
         if (null == smsTemplateRequest.getDefaultConsent()) {
             throw new ValidationException("default consent is required");
         }
         if (null == smsTemplateRequest.getIsMandatory()) {
             throw new ValidationException("is Mandatory required");
         }
         if (null == smsTemplateRequest.getIsDefaultTime()) {
             throw new ValidationException("is Default Time required");
         }
         if (!smsTemplateRequest.getIsDefaultTime() && StringUtils.isEmpty(smsTemplateRequest.getTimeOfSms())) {
             throw new ValidationException("time of sms is required");
         }
         if (clientId == null)
             throw new ValidationException("Client id cannot be null");
    }
}
