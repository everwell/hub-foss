package com.everwell.ins.models.dto.vendorCreds;

import lombok.*;

@Data
public class UserKeyServerUrlDto extends VendorCredsBase {
    String serverUrl;
    String url;
    String userKey;
}
