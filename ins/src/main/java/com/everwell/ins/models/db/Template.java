package com.everwell.ins.models.db;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

@Entity
@Table(name = "ins_template")
@Getter
@AllArgsConstructor
@NoArgsConstructor
public class Template {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Setter
    private Long id;
    @Column(name = "content", columnDefinition = "TEXT")
    private String content;
    @Column(name = "parameters", columnDefinition = "TEXT")
    private String parameters;
    @Column(name = "language_id")
    private Long languageId;
    @Column(name = "type_id")
    private Long typeId;
    @Column(name = "unicode")
    private Boolean unicode;

    public Template(String content, String parameters, Long languageId, Long typeId) {
        this.content = content;
        this.parameters = parameters;
        this.languageId = languageId;
        this.typeId = typeId;
        this.unicode = false;
    }

    public Template(String content, String parameters, Long languageId, Long typeId, Boolean unicode) {
        this.content = content;
        this.parameters = parameters;
        this.languageId = languageId;
        this.typeId = typeId;
        this.unicode = unicode;
    }
}
