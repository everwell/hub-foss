package com.everwell.ins.models.http.responses;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@NoArgsConstructor
@Data
public class SmsPohResponseData {
    private List<SmsPohMessagesResponse> messages;
    private int balance;
    private int credit;
}
