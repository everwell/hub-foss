package com.everwell.ins.models.dto.vendorConfigs;

import lombok.*;

@Data
public class GlobeLabsConfigDto extends VendorConfigsBase {
    String bulkSmsLimit;
}
