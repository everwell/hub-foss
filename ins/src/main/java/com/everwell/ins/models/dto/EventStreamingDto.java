package com.everwell.ins.models.dto;

import com.fasterxml.jackson.annotation.JsonAlias;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class EventStreamingDto<T> {
  @JsonAlias("EventName")
  String eventName;
  @JsonAlias("Field")
  T field;
}

