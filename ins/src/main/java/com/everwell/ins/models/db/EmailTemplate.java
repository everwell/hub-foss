package com.everwell.ins.models.db;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

@Entity
@Table(name = "ins_email_template")
@Getter
@AllArgsConstructor
@NoArgsConstructor
public class EmailTemplate {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Setter
    private Long id;
    @Column(name = "body", columnDefinition = "TEXT")
    private String body;
    @Column(name = "subject", columnDefinition = "TEXT")
    private String subject;
    @Column(name = "body_parameters", columnDefinition = "TEXT")
    private String bodyParameters;
    @Column(name = "subject_parameters", columnDefinition = "TEXT")
    private String subjectParameters;
    @Column(name = "language_id")
    private Long languageId;
    @Column(name = "type_id")
    private Long typeId;

    public EmailTemplate(String body, String subject, String bodyParameters, String subjectParameters, Long languageId, Long typeId)
    {
        this.subject = subject;
        this.body = body;
        this.subjectParameters = subjectParameters;
        this.bodyParameters = bodyParameters;
        this.languageId = languageId;
        this.typeId = typeId;
    }
}
