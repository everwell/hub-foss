package com.everwell.ins.models.dto.vendorCreds;

import lombok.*;

@Data
public class UserNamePinDto extends VendorCredsBase {
    String userName;
    String pin;
    String signature;
    String url;
    String entityId;
}
