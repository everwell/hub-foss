package com.everwell.ins.models.db;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

@Entity
@Table(name = "ins_pn_template")
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class PushNotificationTemplate {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Setter
    private Long id;
    @Column(name = "content", columnDefinition = "TEXT")
    private String content;
    @Column(name = "content_parameters", columnDefinition = "TEXT")
    private String contentParameters;
    @Column(name = "heading_parameters", columnDefinition = "TEXT")
    private String headingParameters;
    @Column(name = "language_id")
    private Long languageId;
    @Column(name = "type_id")
    private Long typeId;
    @Column(name = "heading", columnDefinition = "TEXT")
    private String heading;
    @Column(name = "image")
    private byte[] image;
    @Column(name = "time_to_live")
    private Long timeToLive;
    @Column(name = "class_intent",columnDefinition = "TEXT")
    private String classIntent;
    @Column(name = "intent_extras",columnDefinition = "TEXT")
    private String intentExtras;
    @Column(name = "type")
    private String type;
    @Column(name = "episode_notification")
    private Boolean episodeNotification;


    public PushNotificationTemplate(String content, String contentParameters, String headingParameters, Long languageId, Long typeId, String heading, byte[] image, Long timeToLive) {
        this.content = content;
        this.contentParameters = contentParameters;
        this.headingParameters = headingParameters;
        this.languageId = languageId;
        this.typeId = typeId;
        this.heading = heading;
        this.image = image;
        this.timeToLive = timeToLive;
    }


}
