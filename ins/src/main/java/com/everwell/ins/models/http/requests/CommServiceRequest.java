package com.everwell.ins.models.http.requests;

import com.everwell.ins.models.dto.SmsDto;
import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.List;

@Data
@AllArgsConstructor
public class CommServiceRequest {
    String message;
    String deploymentCode;
    Boolean isMessageCommon;
    String smsGateway;
    List<SmsDto> personList;
}
