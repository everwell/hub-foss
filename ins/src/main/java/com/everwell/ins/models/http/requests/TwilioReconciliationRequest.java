package com.everwell.ins.models.http.requests;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@AllArgsConstructor
@NoArgsConstructor
@Setter
public class TwilioReconciliationRequest {
    String MessageSid;
    String MessageStatus;
    String To;
}
