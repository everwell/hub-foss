package com.everwell.ins.models.http.responses;

import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@Data
public class SmsPohResponse {
    private boolean status;
    private SmsPohResponseData data;

}
