package com.everwell.ins.models.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import java.io.Serializable;

@NoArgsConstructor
@Getter
@Setter
@ToString
public class EmailAttachmentDto implements Serializable {
    String attachmentContent;
    String attachmentName;
    String attachmentType;

    public EmailAttachmentDto(String content, String name, String type) {
        this.attachmentContent = content;
        this.attachmentName = name;
        this.attachmentType = type;
    }
}
