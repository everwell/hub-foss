package com.everwell.ins.models.http.requests;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@AllArgsConstructor
@NoArgsConstructor
@Setter
public class AfricasTalkingReconciliationRequest {
    String id;
    String status;
    String phoneNumber;
}
