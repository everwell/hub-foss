package com.everwell.ins.models.dto.vendorCreds;

import lombok.*;

@Data
public class AuthTokenFromDto extends VendorCredsBase {
    String accountId;
    String authToken;
    String from;
}
