package com.everwell.ins.models.dto.vendorCreds;

import lombok.*;

@Data
public class ApiKeyFromDto extends VendorCredsBase {
    String apiKey;
    String userName;
    String from;
}
