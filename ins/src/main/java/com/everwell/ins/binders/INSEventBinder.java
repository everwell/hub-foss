package com.everwell.ins.binders;

import org.springframework.cloud.stream.annotation.Output;
import org.springframework.messaging.MessageChannel;


public  interface INSEventBinder {
    String EVENTFLOW_EXCHANGE = "event-input";

    @Output(EVENTFLOW_EXCHANGE)
    MessageChannel eventOutput();

}