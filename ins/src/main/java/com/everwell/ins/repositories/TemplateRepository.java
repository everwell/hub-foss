package com.everwell.ins.repositories;

import com.everwell.ins.models.db.Template;
import org.springframework.data.jpa.repository.JpaRepository;

public interface TemplateRepository extends JpaRepository<Template, Long> {

}
