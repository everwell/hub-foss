package com.everwell.ins.repositories;

import com.everwell.ins.models.db.EmailLogs;
import org.springframework.data.jpa.repository.JpaRepository;

public interface EmailLogsRepository extends JpaRepository<EmailLogs, Long> {
}
