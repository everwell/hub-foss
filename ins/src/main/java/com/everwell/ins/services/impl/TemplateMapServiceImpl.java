package com.everwell.ins.services.impl;

import com.everwell.ins.exceptions.NotFoundException;
import com.everwell.ins.models.db.TemplateMap;
import com.everwell.ins.repositories.TemplateMapRepository;
import com.everwell.ins.services.TemplateMapService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class TemplateMapServiceImpl implements TemplateMapService {

    @Autowired
    private TemplateMapRepository templateMapRepository;

    public TemplateMap getTemplateMap(Long templateId) {
        TemplateMap templateMap = templateMapRepository.findByTemplateId(templateId);
        if (null == templateMap) {
            throw new NotFoundException("TemplateMap with id " + templateId + " not found!");
        }
        return templateMap;
    }

}
