package com.everwell.ins.services.impl;

import com.everwell.ins.constants.Constants;
import com.everwell.ins.exceptions.NotImplementedException;
import com.everwell.ins.exceptions.ValidationException;
import com.everwell.ins.models.db.EpisodeNotification;
import com.everwell.ins.models.db.PushNotificationLogs;
import com.everwell.ins.models.db.PushNotificationTemplate;
import com.everwell.ins.models.http.requests.*;
import com.everwell.ins.models.http.responses.EpisodeNotificationResponse;
import com.everwell.ins.models.http.responses.EpisodeUnreadNotificationResponse;
import com.everwell.ins.repositories.EpisodeNotificationRepository;
import com.everwell.ins.repositories.PushLogsRepository;
import com.everwell.ins.services.EpisodeNotificationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Service
public class EpisodeNotificationServiceImpl implements EpisodeNotificationService {

    @Autowired
    private EpisodeNotificationRepository episodeNotificationRepository;

    @Autowired
    protected PushLogsRepository pushLogsRepository;

    @Override
    public void saveNotificationBulk(List<EpisodeNotification> episodeNotificationList) {
        episodeNotificationRepository.saveAll(episodeNotificationList);
    }

    @Override
    public List<EpisodeNotificationResponse> getNotificationForEpisodes(EpisodeNotificationSearchRequest episodeNotificationSearchRequest, Long clientId) {
        List<String> episodeIdList = episodeNotificationSearchRequest.getEpisodeIdList().stream().map(String::valueOf).collect(Collectors.toList());
        List<EpisodeNotification> episodeNotifications = episodeNotificationRepository.findAllByEpisodeIdCustom(episodeIdList, clientId);
        return getEpisodeNotificationResponseFromEpisodeNotifications(episodeNotifications);
    }

    @Override
    public void updateNotification(UpdateEpisodeNotificationRequest episodeNotificationRequest) {
        EpisodeNotification episodeNotification = episodeNotificationRepository.findById(
                episodeNotificationRequest.getNotificationId()
        ).orElse(null);
        if (null == episodeNotification) {
            throw new ValidationException("Notification with id " + episodeNotificationRequest.getNotificationId() + "does not exist");
        }
        if (episodeNotificationRequest.getRead() != null) {
            episodeNotification.setRead(episodeNotificationRequest.getRead());
        }
        if (episodeNotificationRequest.getStatus() != null) {
            episodeNotification.setStatus(episodeNotificationRequest.getStatus());
        }
        episodeNotificationRepository.save(episodeNotification);
    }

    @Override
    public void deleteNotification(DeleteNotificationRequest deleteNotificationRequest) {
        if (deleteNotificationRequest.getNotificationId() != null) {
            episodeNotificationRepository.deleteById(deleteNotificationRequest.getNotificationId());
        } else if (deleteNotificationRequest.getNotificationExtraKeys() != null) {
            Map<String, Object> keysMap = deleteNotificationRequest.getNotificationExtraKeys();
            if (keysMap.containsKey(Constants.PRIMARY_USER_ID) && keysMap.containsKey(Constants.SECONDARY_USER_ID)) {
                Long primaryId = Long.valueOf(String.valueOf(keysMap.get(Constants.PRIMARY_USER_ID)));
                Long secondaryId = Long.valueOf(String.valueOf(keysMap.get(Constants.SECONDARY_USER_ID)));
                EpisodeNotification episodeNotification = episodeNotificationRepository.findByExtraDataUsingPrimaryIdAndSecondaryId(primaryId, secondaryId);
                if (null != episodeNotification)
                    episodeNotificationRepository.delete(episodeNotification);
            }
            else {
                throw new NotImplementedException("[DeleteNotification] NotificationExtraKeys are not correct for request: " + deleteNotificationRequest);
            }
        }
    }
    
    @Override
    public void updateNotificationBulk(UpdateEpisodeNotificationBulkRequest episodeNotificationRequest,  Long clientId) {
        List<String> episodeIdList = episodeNotificationRequest.getEpisodeIdList().stream().map(String::valueOf).collect(Collectors.toList());
        List<EpisodeNotification> episodeNotifications = episodeNotificationRepository.findAllUnreadByEpisodeIdCustom(episodeIdList, clientId);
        episodeNotifications.forEach(episodeNotification -> {
            if (episodeNotificationRequest.getRead() != null) {
                episodeNotification.setRead(episodeNotificationRequest.getRead());
            }
            if (episodeNotificationRequest.getStatus() != null) {
                episodeNotification.setStatus(episodeNotificationRequest.getStatus());
            }
        });
        episodeNotificationRepository.saveAll(episodeNotifications);
    }
    
    @Override
    public EpisodeUnreadNotificationResponse getUnreadNotificationForEpisodes(EpisodeUnreadNotificationSearchRequest episodeNotificationSearchRequest, Long clientId) {
        List<String> episodeIdList = episodeNotificationSearchRequest.getEpisodeIdList().stream().map(String::valueOf).collect(Collectors.toList());
        EpisodeUnreadNotificationResponse episodeUnreadNotificationResponse = new EpisodeUnreadNotificationResponse();
        if(episodeNotificationSearchRequest.getCount()){
            episodeUnreadNotificationResponse.count = episodeNotificationRepository.findCountUnreadByEpisodeIdCustom(episodeIdList,clientId);
        } else {
            List<EpisodeNotification> episodeNotifications = episodeNotificationRepository.findAllUnreadByEpisodeIdCustom(episodeIdList, clientId);
            episodeUnreadNotificationResponse.unreadNotificationsList = getEpisodeNotificationResponseFromEpisodeNotifications(episodeNotifications);
        }
        return episodeUnreadNotificationResponse;
    }

    @Override
    public void addNotificationLog(NotificationLogRequest notificationLogRequest) {
        List<PushNotificationLogs> pushNotificationLogs = new ArrayList<>();
        notificationLogRequest.getNotifications().forEach(notification ->
                pushNotificationLogs.add(new PushNotificationLogs(
                notification.getVendorId(),
                notification.getTemplateId(),
                notification.getTriggerId(),
                notificationLogRequest.getEpisodeId().toString(),
                notification.getMessage(),
                new Date(),
                notification.getFirebaseResponse(),
                notification.getDeviceId()
        )));
        pushLogsRepository.saveAll(pushNotificationLogs);
    }
    private List<EpisodeNotificationResponse> getEpisodeNotificationResponseFromEpisodeNotifications(List<EpisodeNotification> episodeNotifications){
        List<EpisodeNotificationResponse> episodeNotificationResponses = new ArrayList<>();
        if(!episodeNotifications.isEmpty()){
            episodeNotifications.forEach(e -> {
                String message = e.getPushNotificationLogs().getMessage();
                Long episodeId = Long.parseLong(e.getPushNotificationLogs().getEntityId());
                PushNotificationTemplate pushNotificationTemplate = e.getPushNotificationLogs().getPushNotificationTemplate();
                String intentExtras = pushNotificationTemplate.getIntentExtras();
                String notificationType = pushNotificationTemplate.getType();
                episodeNotificationResponses.add(new EpisodeNotificationResponse(e.getId(), e.getCreatedOn(), e.getRead(), e.getStatus(), e.getExtras(), episodeId, intentExtras, message, notificationType));
            });
        }
        return episodeNotificationResponses;
    }
}
