package com.everwell.ins.services.impl;

import com.everwell.ins.exceptions.NotFoundException;
import com.everwell.ins.models.db.Type;
import com.everwell.ins.models.dto.TypeMapping;
import com.everwell.ins.models.http.responses.TypeResponse;
import com.everwell.ins.repositories.TypeRepository;
import com.everwell.ins.services.TypeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;


@Service
public class TypeServiceImpl implements TypeService {

    @Autowired
    private TypeRepository typeRepository;

    public Type getType(Long typeId) {
        Type type = typeRepository.findById(typeId).orElse(null);
        if (null == type) {
            throw new NotFoundException("Type with id " + typeId + " not found!");
        }
        return type;
    }

    public List<TypeMapping> getTypeMappings(List<Long> typeIds) {
        List<TypeMapping> typeResponses = new ArrayList<>();
        List<Type> mappingList = typeRepository.findAllByIdIn(typeIds);
        mappingList.forEach(t -> {
            typeResponses.add(new TypeMapping(t.getId(), t.getType()));
        });
        return typeResponses;
    }

    public List<TypeMapping> getAllTypeMappings() {
        List<TypeMapping> typeResponses = new ArrayList<>();
        List<Type> mappingList = typeRepository.findAll();
        mappingList.forEach(t -> {
            typeResponses.add(new TypeMapping(t.getId(), t.getType()));
        });
        return typeResponses;
    }
}
