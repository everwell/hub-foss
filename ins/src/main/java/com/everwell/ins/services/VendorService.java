package com.everwell.ins.services;

import com.everwell.ins.models.db.Vendor;

import java.util.List;

public interface VendorService {

    List<Long> getVendorIds(String smsGateway);

    Vendor getVendor(Long vendorId);

    List<Vendor> getVendors(String PNGateway);

    <T> T getVendorProfileWithType(String profile, Class<T> valueType);

    <T> T getVendorCredentials(Long vendorId, Class<T> valueType);

    <T> T getVendorConfig(Long vendorId, Class<T> valueType);
}
