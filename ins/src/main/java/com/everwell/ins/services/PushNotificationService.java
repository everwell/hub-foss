package com.everwell.ins.services;

import com.everwell.ins.models.db.PushNotificationLogs;
import com.everwell.ins.models.http.requests.PushNotificationRequest;

import java.util.List;
import java.util.concurrent.ExecutionException;

public interface PushNotificationService {

    void sendNotification(PushNotificationRequest pushNotificationRequest) throws ExecutionException, InterruptedException;

    void sendBulkNotification(List<PushNotificationRequest> pushNotificationRequests);

    PushNotificationRequest constructNotification(PushNotificationRequest pushNotificationRequest);


}