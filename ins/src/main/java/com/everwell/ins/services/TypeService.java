package com.everwell.ins.services;

import com.everwell.ins.models.db.Type;
import com.everwell.ins.models.dto.TypeMapping;
import com.everwell.ins.models.http.responses.TypeResponse;

import java.util.List;
import java.util.Map;

public interface TypeService {
    Type getType(Long typeId);

    List<TypeMapping> getTypeMappings(List<Long> typeIds);

    List<TypeMapping> getAllTypeMappings();
}
