package com.everwell.ins.services.impl;

import com.everwell.ins.enums.PushNotificationGateway;
import com.everwell.ins.exceptions.NotFoundException;
import com.everwell.ins.factory.VendorFactory;
import com.everwell.ins.models.db.PushNotificationLogs;
import com.everwell.ins.models.db.PushNotificationTemplate;
import com.everwell.ins.models.db.Vendor;
import com.everwell.ins.models.dto.PushNotificationRequestDto;
import com.everwell.ins.models.dto.SmsDetailedRequestDto;
import com.everwell.ins.models.http.requests.PushNotificationRequest;
import com.everwell.ins.repositories.PushLogsRepository;
import com.everwell.ins.repositories.VendorRepository;
import com.everwell.ins.services.EngagementService;
import com.everwell.ins.services.PushNotificationService;
import com.everwell.ins.services.TemplateService;
import com.everwell.ins.utils.SentryUtils;
import com.everwell.ins.vendors.NotificationFirebaseHandler;
import com.everwell.ins.vendors.PushNotificationHandler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;
import retrofit2.http.HTTP;

import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.stream.Collectors;

@Service
public class PushNotificationServiceImpl implements PushNotificationService {

    private static Logger LOGGER = LoggerFactory.getLogger(PushNotificationServiceImpl.class);

    @Autowired
    private VendorRepository vendorRepository;

    @Autowired
    private VendorFactory vendorFactory;

    @Autowired
    private TemplateService templateService;

    @Autowired
    private EngagementService engagementService;


    public void sendNotification(PushNotificationRequest pushNotificationRequest) {
        try {
            if (!pushNotificationRequest.getPersonList().isEmpty()) {
                Vendor vendor = vendorRepository.findById(pushNotificationRequest.getVendorId()).orElse(null);
                if (null == vendor) {
                    throw new NotFoundException("Vendor with id " + pushNotificationRequest.getVendorId() + " not found!");
                }

                PushNotificationHandler pushNotificationHandler = vendorFactory.getPushNotificationHandler(PushNotificationGateway.valueOf(vendor.getGateway()));
                pushNotificationHandler.sendNotification(pushNotificationRequest);
            }
        } catch (Exception e) {
            LOGGER.error("Unable to send notification", e);
            SentryUtils.captureException(e);
        }
    }

    public void sendBulkNotification(List<PushNotificationRequest> pushNotificationRequests) {
        pushNotificationRequests.forEach(p -> {
            if (!CollectionUtils.isEmpty(p.getPersonList())) {
                sendNotification(p);
            }
        });
    }

    public PushNotificationRequest constructNotification(PushNotificationRequest pushNotificationRequest) {
        PushNotificationRequestDto pushNotificationRequestDto = null;

        if (!pushNotificationRequest.getIsMandatory()) {
            engagementService.filterPushNotificationList(pushNotificationRequest);
        }

        pushNotificationRequestDto = templateService.constructNotification(pushNotificationRequest);
        return pushNotificationRequestDto.getPushNotificationRequest();
    }

}
