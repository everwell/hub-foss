package com.everwell.ins.services.impl;

import com.everwell.ins.exceptions.NotFoundException;
import com.everwell.ins.models.db.Trigger;
import com.everwell.ins.repositories.TriggerRepository;
import com.everwell.ins.services.TriggerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.util.List;


@Service
public class TriggerServiceImpl implements TriggerService {

    @Autowired
    private TriggerRepository triggerRepository;

    public List<Trigger> getAllTriggers() {
        List<Trigger> triggers = triggerRepository.findAll();
        if (CollectionUtils.isEmpty(triggers)) {
            throw new NotFoundException("Triggers not found!");
        }
        return triggers;
    }

    public Trigger getTrigger(Long triggerId) {
        return triggerRepository.findById(triggerId).orElse(null);
    }

}
