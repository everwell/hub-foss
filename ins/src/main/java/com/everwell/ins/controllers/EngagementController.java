package com.everwell.ins.controllers;


import com.everwell.ins.models.db.Engagement;
import com.everwell.ins.models.db.Type;
import com.everwell.ins.models.db.Language;
import com.everwell.ins.models.http.requests.EngagementBulkRequest;
import com.everwell.ins.models.http.requests.EngagementRequest;
import com.everwell.ins.models.http.responses.EngagementResponse;
import com.everwell.ins.models.http.responses.Response;
import com.everwell.ins.services.EngagementService;
import com.everwell.ins.services.LanguageService;
import com.everwell.ins.services.TypeService;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@RestController
public class EngagementController {

    private static Logger LOGGER = LoggerFactory.getLogger(EngagementController.class);

    @Autowired
    private EngagementService engagementService;

    @Autowired
    private TypeService typeService;

    @Autowired
    private LanguageService languageService;

    @ApiOperation(
            value = "Save Engagement Config",
            notes = "Accepts engagement config details like consent, preferred language & time which is then saved in database"
    )
    @PostMapping(value = "/v1/engagement")
    public ResponseEntity<Response<Long>> saveEngagementConfig(@RequestBody EngagementRequest engagementRequest) {
        LOGGER.info("[saveEngagementConfig] engagement config request accepted");
        engagementRequest.validate(engagementRequest);
        Long engagementId = engagementService.saveEngagementConfig(engagementRequest);
        Response<Long> response = new Response<>(true, engagementId);
        return new ResponseEntity<>(response, HttpStatus.ACCEPTED);
    }

    @ApiOperation(
            value = "Save Engagement Config For Multiple Notification Types",
            notes = "Accepts engagement config details like consent, preferred language & time which is then saved in database"
    )
    @PostMapping(value = "/v1/engagement-bulk")
    public ResponseEntity<Response<String>> saveEngagementConfigBulk(@RequestBody EngagementBulkRequest engagementBulkRequest) {
        LOGGER.info("[saveEngagementConfigBulk] engagement config request accepted " + engagementBulkRequest.toString() );
        engagementBulkRequest.validate();
        engagementService.saveEngagementConfigBulk(engagementBulkRequest);
        return new ResponseEntity<>(new Response<>(true, "Engagement Saved !"), HttpStatus.ACCEPTED);
    }

    @ApiOperation(
            value = "Get Engagement Config",
            notes = "Fetch engagement config, for a particular entity Id"
    )
    @GetMapping(value = "/v1/engagement/{id}")
    public ResponseEntity<Response<EngagementResponse>> getEngagementConfig(@PathVariable String id, @RequestParam(name = "typeId", defaultValue = "1") Long typeId) {
        LOGGER.info("[getEngagementConfig] get config request accepted");
        Engagement engagement = engagementService.getEngagementConfig(id, typeId);
        Response<EngagementResponse> response = new Response<>(true, null);
        if (engagement != null) {
            String type = Optional.ofNullable(engagement.getTypeId())
                    .map(typeService::getType)
                    .map(Type::getType)
                    .orElse(null);
            String language = Optional.ofNullable(engagement.getLanguageId())
                    .map(languageService::getLanguage)
                    .map(Language::getLanguage)
                    .orElse(null);
            response = new Response<>(true, new EngagementResponse(engagement, type, language));
        }
        LOGGER.info("[getEngagementConfig] response generated: " + response);
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @ApiOperation(
            value = "Get Engagement Config For All Types",
            notes = "Fetch engagement config for all types corresponding a particular entity Id"
    )
    @GetMapping(value = "/v1/engagement-bulk/{id}")
    public ResponseEntity<Response<List<EngagementResponse>>> getEngagementConfigBulk(@PathVariable String id) {
        LOGGER.info("[getEngagementConfigBulk] get config request accepted " + id);
        List<Engagement> engagements = engagementService.getEngagementBulk(id);
        List<EngagementResponse> response;
        if (engagements != null) {
            response = new ArrayList<>();
            engagements.forEach(e -> {
                String type = typeService.getType(e.getTypeId()).getType();
                String language = languageService.getLanguage(e.getLanguageId()).getLanguage();
                response.add(new EngagementResponse(e, type, language));
            });
        } else {
            response = null;
        }
        LOGGER.info("[getEngagementConfigBulk] response generated: " + response);
        return new ResponseEntity<>(new Response<>(true, response), HttpStatus.OK);
    }

    @ApiOperation(
            value = "Update Engagement Config",
            notes = "Update engagement config, for particular entity Id"
    )
    @PutMapping(value = "/v1/engagement")
    public ResponseEntity<Response<EngagementResponse>> updateEngagementConfig(@RequestBody EngagementRequest engagementRequest) {
        LOGGER.info("[updateEngagementConfig] update config request accepted");
        engagementRequest.validate(engagementRequest);
        Engagement engagement = engagementService.updateEngagementConfig(engagementRequest);
        String type = engagement.getTypeId() != null ? typeService.getType(engagement.getTypeId()).getType() : null;
        String language = engagement.getLanguageId() != null ? languageService.getLanguage(engagement.getLanguageId()).getLanguage() : null;
        Response<EngagementResponse> response = new Response<>(true, new EngagementResponse(engagement, type, language));
        LOGGER.info("[updateEngagementConfig] response generated: " + response);
        return new ResponseEntity<>(response, HttpStatus.ACCEPTED);
    }

    @ApiOperation(
            value = "Update Engagement Config For Multiple Notification Types",
            notes = "Update engagement config, for particular entity Id"
    )
    @PutMapping(value = "/v1/engagement-bulk")
    public ResponseEntity<Response<List<EngagementResponse>>> updateEngagementConfigBulk(@RequestBody EngagementBulkRequest engagementBulkRequest) {
        LOGGER.info("[updateEngagementConfigBulk] update config request accepted " + engagementBulkRequest.toString());
        engagementBulkRequest.validate();
        List<EngagementRequest> engagementRequestList = engagementBulkRequest.getEngagementRequestList();
        List<EngagementResponse> response = new ArrayList<>();
        engagementRequestList.forEach(engagementRequest -> {
            Engagement engagement = engagementService.updateEngagementConfig(engagementRequest);
            String type =  typeService.getType(engagement.getTypeId()).getType();
            String language = languageService.getLanguage(engagement.getLanguageId()).getLanguage();
            response.add(new EngagementResponse(engagement, type, language));
        });
        LOGGER.info("[updateEngagementConfigBulk] response generated: " + response);
        return new ResponseEntity<>(new Response<>(true, response), HttpStatus.ACCEPTED);
    }

}
