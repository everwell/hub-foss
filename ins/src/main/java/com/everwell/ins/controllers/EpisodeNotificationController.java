package com.everwell.ins.controllers;

import com.everwell.ins.models.db.EpisodeNotification;
import com.everwell.ins.models.db.PushNotificationLogs;
import com.everwell.ins.models.db.PushNotificationTemplate;
import com.everwell.ins.models.http.requests.*;
import com.everwell.ins.models.http.responses.EpisodeNotificationResponse;
import com.everwell.ins.models.http.responses.EpisodeUnreadNotificationResponse;
import com.everwell.ins.models.http.responses.Response;
import com.everwell.ins.services.ClientService;
import com.everwell.ins.services.EpisodeNotificationService;
import com.everwell.ins.services.PushNotificationService;
import com.everwell.ins.services.TemplateService;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;

@RestController
public class EpisodeNotificationController {

    private static Logger LOGGER = LoggerFactory.getLogger(EpisodeNotificationController.class);

    @Autowired
    private EpisodeNotificationService episodeNotificationService;

    @Autowired
    private ClientService clientService;


    @ApiOperation(
            value = "Get Notifications by Episode Id List (multiple episodes of a person)",
            notes = "Get Notifications by Episode Id List (multiple episodes of a person)"
    )
    @PostMapping(value = "/v1/notification/bulk")
    public ResponseEntity<Response<List<EpisodeNotificationResponse>>> getEpisodeNotifications(@RequestBody EpisodeNotificationSearchRequest episodeNotificationSearchRequest) {
        LOGGER.info("[getEpisodeNotifications] request accepted with request " + episodeNotificationSearchRequest);
        episodeNotificationSearchRequest.validate(episodeNotificationSearchRequest);
        Long clientId = clientService.getCurrentClient().getId();
        List<EpisodeNotificationResponse> notifications = episodeNotificationService.getNotificationForEpisodes(episodeNotificationSearchRequest, clientId);
        Response<List<EpisodeNotificationResponse>> response = new Response<>(true, notifications);
        LOGGER.info("[getEpisodeNotifications] response generated: " + response);
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @ApiOperation(
            value = "Update Notification",
            notes = "Update Notification"
    )
    @PutMapping(value = "/v1/notification")
    public ResponseEntity<Response<String>> updateNotification(@RequestBody UpdateEpisodeNotificationRequest updateNotificationRequest) {
        LOGGER.info("[updateNotifications] request accepted " + updateNotificationRequest);
        updateNotificationRequest.validate(updateNotificationRequest);
        episodeNotificationService.updateNotification(updateNotificationRequest);
        Response<String> response = new Response<>(true, "updated!");
        LOGGER.info("[saveEpisodeNotification] response generated: " + response);
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @ApiOperation(
            value = "Delete Notification by Id or Extra data json values",
            notes = "Delete Notification by Id or Extra data json values"
    )
    @DeleteMapping(value = "/v1/notification")
    public ResponseEntity<Response<String>> deleteNotification(@RequestBody DeleteNotificationRequest deleteNotificationRequest) {
        LOGGER.info("[deleteNotification] request accepted " + deleteNotificationRequest);
        deleteNotificationRequest.validate();
        episodeNotificationService.deleteNotification(deleteNotificationRequest);
        Response<String> response = new Response<>(true, "deleted!");
        LOGGER.info("[deleteNotification] response generated: " + response);
        return new ResponseEntity<>(response, HttpStatus.OK);
    }
    @ApiOperation(
            value = "Update Notification Bulk",
            notes = "Update Notification Bulk"
    )
    @PutMapping(value = "/v1/notification/bulk")
    public ResponseEntity<Response<String>> updateNotificationBulk(@RequestBody UpdateEpisodeNotificationBulkRequest updateNotificationBulkRequest) {
        LOGGER.info("[updateNotificationBulk] request accepted " + updateNotificationBulkRequest.toString());
        updateNotificationBulkRequest.validate(updateNotificationBulkRequest);
        Long clientId = clientService.getCurrentClient().getId();
        episodeNotificationService.updateNotificationBulk(updateNotificationBulkRequest, clientId);
        Response<String> response = new Response<>(true, "updated!");
        LOGGER.info("[updateNotificationBulk] response generated: " + response);
        return new ResponseEntity<>(response, HttpStatus.OK);
    }
    
    @ApiOperation(
            value = "Get Unread Notifications Count or List by Episode Id List (multiple episodes of a person)",
            notes = "Get Unread Notifications Count or List by Episode Id List (multiple episodes of a person)"
    )
    @PostMapping(value = "/v1/notification/unread/bulk")
    public ResponseEntity<Response<EpisodeUnreadNotificationResponse>> getUnreadEpisodeNotifications(@RequestBody EpisodeUnreadNotificationSearchRequest episodeNotificationSearchRequest) {
        LOGGER.info("[getUnreadEpisodeNotifications] request accepted with request " + episodeNotificationSearchRequest);
        episodeNotificationSearchRequest.validate(episodeNotificationSearchRequest);
        Long clientId = clientService.getCurrentClient().getId();
        EpisodeUnreadNotificationResponse notifications = episodeNotificationService.getUnreadNotificationForEpisodes(episodeNotificationSearchRequest, clientId);
        Response<EpisodeUnreadNotificationResponse> response = new Response<>(true, notifications);
        LOGGER.info("[getUnreadEpisodeNotifications] response generated: " + response);
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @ApiOperation(
            value = "Add offline notification logs",
            notes = "Add offline notification logs"
    )
    @PostMapping(value = "/v1/notification/logs")
    public ResponseEntity<Response<String>> addNotificationLogs(@RequestBody NotificationLogRequest notificationLogRequest) {
        LOGGER.info("[addNotificationLogs] request accepted " + notificationLogRequest);
        notificationLogRequest.validate();
        episodeNotificationService.addNotificationLog(notificationLogRequest);
        Response<String> response = new Response<>(true, "added!");
        LOGGER.info("[addNotificationLogs] response generated: " + response);
        return new ResponseEntity<>(response, HttpStatus.OK);
    }
}
