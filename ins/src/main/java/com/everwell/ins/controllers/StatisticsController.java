package com.everwell.ins.controllers;

import com.everwell.ins.models.http.requests.StatisticsRequest;
import com.everwell.ins.models.http.responses.Response;
import com.everwell.ins.models.http.responses.StatisticsResponse;
import com.everwell.ins.services.StatisticsService;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class StatisticsController {

    private static Logger LOGGER = LoggerFactory.getLogger(StatisticsController.class);

    @Autowired
    private StatisticsService statisticsService;

    @ApiOperation(
            value = "Get Statistics",
            notes = "Fetch stats of notifications sent from INS filtered on the request parameters"
    )
    @PostMapping(value = "/v1/statistics")
    public ResponseEntity<Response<StatisticsResponse>> getStatistics(@RequestBody StatisticsRequest statisticsRequest) {
        LOGGER.info("[getStatistics] get statistics request accepted");
        statisticsRequest.validate(statisticsRequest);
        Long count = statisticsService.getNotificationsCount(statisticsRequest);
        StatisticsResponse statisticsResponse = new StatisticsResponse(count);
        Response<StatisticsResponse> response = new Response<>(true, statisticsResponse);
        LOGGER.info("[getStatistics] response generated: " + response);
        return new ResponseEntity<>(response, HttpStatus.OK);
    }
}
