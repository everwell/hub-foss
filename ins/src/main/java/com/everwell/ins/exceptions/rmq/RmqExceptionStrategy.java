package com.everwell.ins.exceptions.rmq;

import com.everwell.ins.config.RmqDlxHelper;
import com.everwell.ins.constants.RabbitMQConstants;
import org.springframework.amqp.AmqpRejectAndDontRequeueException;
import org.springframework.amqp.ImmediateAcknowledgeAmqpException;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.support.ListenerExecutionFailedException;
import org.springframework.util.ErrorHandler;

public class RmqExceptionStrategy implements ErrorHandler {

    @Override
    public void handleError(Throwable throwable) {
        if (throwable instanceof ListenerExecutionFailedException) {
            Message message = ((ListenerExecutionFailedException) throwable).getFailedMessage();
            String consumerQueue = message.getMessageProperties().getConsumerQueue();
            RmqDlxHelper.triggerRetryMechanism(message, RabbitMQConstants.DLX_EXCHANGE, consumerQueue, RabbitMQConstants.WAIT_EXCHANGE);
            throw new ImmediateAcknowledgeAmqpException("[RMQ handleError] + " + throwable +" - Handled Error, Triggering Retry Mechanism");
        }
        throw new AmqpRejectAndDontRequeueException("Exception at consumer", throwable);
    }

}
