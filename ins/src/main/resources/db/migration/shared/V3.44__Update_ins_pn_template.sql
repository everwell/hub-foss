alter table ins_pn_template add column if not exists type varchar;
alter table ins_pn_template add column if not exists episode_notification boolean not null default false;

update ins_pn_template
set episode_notification = false, type = 'DEFAULT'
where episode_notification is null;



