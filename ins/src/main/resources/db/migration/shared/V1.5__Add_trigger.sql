/* Add foreign keys */
ALTER TABLE ins_trigger
ADD CONSTRAINT fk_type
FOREIGN KEY (template_id)
REFERENCES ins_template (id);

/* Add trigger Id in sms log */
alter table ins_sms_logs add column if not exists trigger_id bigint;