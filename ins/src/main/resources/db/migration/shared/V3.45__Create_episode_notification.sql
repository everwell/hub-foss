create table if not exists episode_notification
(
    id bigserial not null
    constraint episode_notification_pkey
    primary key,
    pn_log_id bigint,
    client_id bigint,
    created_on timestamp,
    read boolean not null default false,
    status varchar,
    extras jsonb
);

create index if not exists episode_notification_pn_log_id_client_id on
episode_notification(pn_log_id, client_id);

create index if not exists ins_pn_logs_entity_id on
ins_pn_logs(entity_id);