package tests.analytics;

import com.everwell.ins.analytics.SpringEventAnalytics;
import com.everwell.ins.binders.INSEventBinder;
import com.everwell.ins.models.db.Client;
import com.everwell.ins.models.db.Trigger;
import com.everwell.ins.models.db.Vendor;
import com.everwell.ins.models.dto.EmailTemplateDto;
import com.everwell.ins.models.dto.PushNotificationTemplateDto;
import com.everwell.ins.models.dto.SmsDto;
import com.everwell.ins.models.http.requests.EmailRequest;
import com.everwell.ins.models.http.requests.PushNotificationRequest;
import com.everwell.ins.models.http.requests.SmsRequest;
import com.everwell.ins.repositories.ClientRepository;
import com.everwell.ins.services.ClientService;
import com.everwell.ins.services.TriggerService;
import com.everwell.ins.services.VendorService;
import org.junit.Test;
import org.junit.platform.commons.util.ReflectionUtils;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.Spy;
import org.springframework.messaging.MessageChannel;
import org.springframework.test.context.event.annotation.BeforeTestMethod;
import org.springframework.test.util.ReflectionTestUtils;
import tests.BaseTest;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;
import static org.mockito.Mockito.when;

public class SpringEventAnalyticsTest extends BaseTest {

    @Spy
    @InjectMocks
    SpringEventAnalytics springEventAnalytics;

    @Mock
    INSEventBinder insEventBinder;

    @Mock
    MessageChannel eventOutput;

    @Mock
    private TriggerService triggerService;

    @Mock
    private VendorService vendorService;

    @Mock
    private ClientService clientService;

    @Mock
    private ClientRepository clientRepository;

    private static String text = "Test";

    protected List<SmsDto> getSmsDtos() {
        List<SmsDto> smsDtos = new ArrayList<>();
        smsDtos.add(new SmsDto("1", "1", "test1"));
        smsDtos.add(new SmsDto("2", "2", "test2"));
        smsDtos.add(new SmsDto("3", "3", "test3"));
        return smsDtos;
    }

    protected List<PushNotificationTemplateDto> getPNDtos() {
        List<PushNotificationTemplateDto> PNDtos = new ArrayList<>();
        PNDtos.add(new PushNotificationTemplateDto("1", "1", "test", "test"));
        PNDtos.add(new PushNotificationTemplateDto("2", "2", "test", "test"));
        PNDtos.add(new PushNotificationTemplateDto("3", "3", "test", "test"));
        return PNDtos;
    }

    protected List<EmailTemplateDto> getEmailDtos() {
        List<EmailTemplateDto> emailTemplateDtos = new ArrayList<>();
        emailTemplateDtos.add(new EmailTemplateDto("recipient1", "body", "subject"));
        emailTemplateDtos.add(new EmailTemplateDto("recipient2", "body", "subject"));
        emailTemplateDtos.add(new EmailTemplateDto("recipient3","body","subject"));
        return emailTemplateDtos;
    }

    @BeforeTestMethod
    public void init() {
        Mockito.when(insEventBinder.eventOutput()).thenReturn(eventOutput);
    }

    @Test
    public void testTrackEvent() throws IOException {
        Long id = 1L;

        Client client = new Client(id, text, text, new Date(), id);
        List<Client> clients = new ArrayList<>();
        clients.add(client);
        clients.add(client);
        when(clientRepository.findAll()).thenReturn(clients);
        init();
        springEventAnalytics.init();
        springEventAnalytics.trackReconciliationEvent(text);


        Mockito.verify(eventOutput, Mockito.times(1))
                .send(any());
    }

    @Test
    public void testTrackSms() throws IOException {
        Long id = 1L;
        Vendor vendor = new Vendor(id, text, text, text, text);
        Trigger trigger = new Trigger(id, text, text, text, id);
        List<SmsDto> smsDtos = getSmsDtos();
        SmsRequest smsRequest = new SmsRequest(text, false, smsDtos, id, id, id, true);

        Client client = new Client(id, text, text, new Date(), id);
        List<Client> clients = new ArrayList<>();
        clients.add(client);
        clients.add(client);
        when(triggerService.getTrigger(any())).thenReturn(trigger);
        when(vendorService.getVendor(any())).thenReturn(vendor);
        when(clientRepository.findAll()).thenReturn(clients);

        init();
        springEventAnalytics.init();
        springEventAnalytics.trackSmsEvent(smsRequest);
        verify(vendorService, Mockito.times(1)).getVendor(any());
        verify(triggerService, Mockito.times(1)).getTrigger(any());
        Mockito.verify(eventOutput, Mockito.times(9)).send(any());
    }

    @Test
    public void testTrackPNWithDefaultClientId() throws IOException {
        Long id = 1L;
        Vendor vendor = new Vendor(id, text, text, text, text);
        List<PushNotificationTemplateDto> pushNotificationTemplateDtos = getPNDtos();
        PushNotificationRequest pushNotificationRequest = new PushNotificationRequest(pushNotificationTemplateDtos, id, id, id);
        Client client = new Client(id, text, text, new Date(), id);
        List<Client> clients = new ArrayList<>();
        clients.add(client);
        when(vendorService.getVendor(any())).thenReturn(vendor);
        when(clientRepository.findAll()).thenReturn(clients);
        ReflectionTestUtils.setField(springEventAnalytics, "defaultClientId", 1L);


        init();
        springEventAnalytics.init();
        springEventAnalytics.trackPNEvent(pushNotificationRequest);
        verify(vendorService, Mockito.times(1)).getVendor(any());
        Mockito.verify(eventOutput, Mockito.times(9)).send(any());
    }

    @Test
    public void testTrackPNWithClientId() throws IOException {
        Long id = 1L;
        Vendor vendor = new Vendor(id, text, text, text, text);
        List<PushNotificationTemplateDto> pushNotificationTemplateDtos = getPNDtos();
        PushNotificationRequest pushNotificationRequest = new PushNotificationRequest(pushNotificationTemplateDtos, id, id, id);
        pushNotificationRequest.setClientId(1L);
        Client client = new Client(id, text, text, new Date(), id);
        List<Client> clients = new ArrayList<>();
        clients.add(client);
        when(vendorService.getVendor(any())).thenReturn(vendor);
        when(clientRepository.findAll()).thenReturn(clients);


        init();
        springEventAnalytics.init();
        springEventAnalytics.trackPNEvent(pushNotificationRequest);
        verify(vendorService, Mockito.times(1)).getVendor(any());
        Mockito.verify(eventOutput, Mockito.times(9)).send(any());
    }

    @Test
    public void testTrackEmail() throws IOException {
        Long id = 1L;
        Vendor vendor = new Vendor(id, text, text, text, text);
        List<EmailTemplateDto> emailTemplateDtos = getEmailDtos();
        EmailRequest emailRequest = new EmailRequest(1L, 1L, 1L, "sender", emailTemplateDtos);
        Client client = new Client(id, text, text, new Date(), id);
        List<Client> clients = new ArrayList<>();
        clients.add(client);
        clients.add(client);
        when(vendorService.getVendor(any())).thenReturn(vendor);
        when(clientRepository.findAll()).thenReturn(clients);

        init();
        springEventAnalytics.init();
        springEventAnalytics.trackEmailEvent(emailRequest);
        verify(vendorService, Mockito.times(1)).getVendor(any());
        Mockito.verify(eventOutput, Mockito.times(9)).send(any());
    }

}
