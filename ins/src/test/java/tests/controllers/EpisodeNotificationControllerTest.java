package tests.controllers;

import com.everwell.ins.controllers.EpisodeNotificationController;
import com.everwell.ins.exceptions.CustomExceptionHandler;
import com.everwell.ins.models.db.Client;
import com.everwell.ins.models.http.requests.DeleteNotificationRequest;
import com.everwell.ins.models.http.requests.EpisodeNotificationSearchRequest;
import com.everwell.ins.models.http.requests.UpdateEpisodeNotificationBulkRequest;
import com.everwell.ins.models.http.requests.NotificationLogRequest;
import com.everwell.ins.models.http.requests.UpdateEpisodeNotificationRequest;
import com.everwell.ins.models.http.responses.EpisodeNotificationResponse;
import com.everwell.ins.models.http.responses.EpisodeUnreadNotificationResponse;
import com.everwell.ins.services.ClientService;
import com.everwell.ins.services.EpisodeNotificationService;
import com.everwell.ins.utils.Utils;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import tests.BaseTest;

import java.util.Collections;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.doNothing;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

public class EpisodeNotificationControllerTest extends BaseTest {

    private MockMvc mockMvc;

    @Mock
    private EpisodeNotificationService episodeNotificationService;

    @Mock
    private ClientService clientService;

    @InjectMocks
    private EpisodeNotificationController episodeNotificationController;

    @Before
    public void setup() {
        mockMvc = MockMvcBuilders.standaloneSetup(episodeNotificationController)
                .setControllerAdvice(new CustomExceptionHandler())
                .build();
    }


    @Test
    public void getEpisodeNotificationTestSuccess() throws Exception {
        String uri = "/v1/notification/bulk";
        EpisodeNotificationSearchRequest episodeNotificationSearchRequest = new EpisodeNotificationSearchRequest(Collections.singletonList(1L));
        Client client = new Client(171L, "test", "test", null);
        EpisodeNotificationResponse episodeNotificationResponse = new EpisodeNotificationResponse();
        Mockito.when(clientService.getCurrentClient()).thenReturn(client);
        Mockito.when(episodeNotificationService.getNotificationForEpisodes(any(), eq(171L))).thenReturn(Collections.singletonList(episodeNotificationResponse));

        mockMvc
                .perform(
                        MockMvcRequestBuilders
                                .post(uri)
                                .contentType(MediaType.APPLICATION_JSON)
                                .content(Utils.asJsonString(episodeNotificationSearchRequest))
                                .accept(MediaType.APPLICATION_JSON_VALUE)
                )
                .andExpect(status().isOk());
    }

    @Test
    public void getEpisodeNotificationTestEmptyEpisodeIdList() throws Exception {
        String uri = "/v1/notification/bulk";
        EpisodeNotificationSearchRequest episodeNotificationSearchRequest = new EpisodeNotificationSearchRequest();
        mockMvc
                .perform(
                        MockMvcRequestBuilders
                                .post(uri)
                                .contentType(MediaType.APPLICATION_JSON)
                                .content(Utils.asJsonString(episodeNotificationSearchRequest))
                                .accept(MediaType.APPLICATION_JSON_VALUE)
                )
                .andExpect(status().isBadRequest());
    }

    @Test
    public void testUpdateNotificationSuccess() throws Exception {
        String uri = "/v1/notification";
        UpdateEpisodeNotificationRequest updateEpisodeNotificationRequest = new UpdateEpisodeNotificationRequest(1L, "accept", true);
        doNothing().when(episodeNotificationService).updateNotification(any());
        mockMvc
                .perform(
                        MockMvcRequestBuilders
                                .put(uri)
                                .contentType(MediaType.APPLICATION_JSON)
                                .content(Utils.asJsonString(updateEpisodeNotificationRequest))
                                .accept(MediaType.APPLICATION_JSON_VALUE)
                )
                .andExpect(status().isOk());
    }

    @Test
    public void testUpdateNotificationNullId() throws Exception {
        String uri = "/v1/notification";
        UpdateEpisodeNotificationRequest updateEpisodeNotificationRequest = new UpdateEpisodeNotificationRequest(null, "accept", true);
        mockMvc
                .perform(
                        MockMvcRequestBuilders
                                .put(uri)
                                .contentType(MediaType.APPLICATION_JSON)
                                .content(Utils.asJsonString(updateEpisodeNotificationRequest))
                                .accept(MediaType.APPLICATION_JSON_VALUE)
                )
                .andExpect(status().isBadRequest());
    }

    @Test
    public void testUpdateNotificationNoUpdateFields() throws Exception {
        String uri = "/v1/notification";
        UpdateEpisodeNotificationRequest updateEpisodeNotificationRequest = new UpdateEpisodeNotificationRequest(1L, null, null);
        mockMvc
                .perform(
                        MockMvcRequestBuilders
                                .put(uri)
                                .contentType(MediaType.APPLICATION_JSON)
                                .content(Utils.asJsonString(updateEpisodeNotificationRequest))
                                .accept(MediaType.APPLICATION_JSON_VALUE)
                )
                .andExpect(status().isBadRequest());
    }
    @Test
    public void testDeleteNotificationSuccess() throws Exception {
        String uri = "/v1/notification";
        DeleteNotificationRequest deleteNotificationRequest = new DeleteNotificationRequest();
        deleteNotificationRequest.setNotificationId(1L);
        doNothing().when(episodeNotificationService).deleteNotification(any());
        mockMvc
                .perform(
                        MockMvcRequestBuilders
                                .delete(uri)
                                .contentType(MediaType.APPLICATION_JSON)
                                .content(Utils.asJsonString(deleteNotificationRequest))
                                .accept(MediaType.APPLICATION_JSON_VALUE)
                )
                .andExpect(status().isOk());
    }

    @Test
    public void testDeleteNotificationFailed() throws Exception {
        String uri = "/v1/notification";
        DeleteNotificationRequest deleteNotificationRequest = new DeleteNotificationRequest();
        mockMvc
                .perform(
                        MockMvcRequestBuilders
                                .delete(uri)
                                .contentType(MediaType.APPLICATION_JSON)
                                .content(Utils.asJsonString(deleteNotificationRequest))
                                .accept(MediaType.APPLICATION_JSON_VALUE)
                )
                .andExpect(status().isBadRequest());
    }

    @Test
    public void testUpdateNotificationBulkSuccess() throws Exception {
        String uri = "/v1/notification/bulk";
        UpdateEpisodeNotificationBulkRequest updateEpisodeNotificationBulkRequest = new UpdateEpisodeNotificationBulkRequest(Collections.singletonList(1L), null, true);
        Client client = new Client(171L, "test", "test", null);
        doNothing().when(episodeNotificationService).updateNotificationBulk(any(),any());
        Mockito.when(clientService.getCurrentClient()).thenReturn(client);
        mockMvc
                .perform(
                        MockMvcRequestBuilders
                                .put(uri)
                                .contentType(MediaType.APPLICATION_JSON)
                                .content(Utils.asJsonString(updateEpisodeNotificationBulkRequest))
                                .accept(MediaType.APPLICATION_JSON_VALUE)
                )
                .andExpect(status().isOk());
    }

    @Test
    public void getUnreadEpisodeNotificationTestSuccess() throws Exception {
        String uri = "/v1/notification/unread/bulk";
        EpisodeNotificationSearchRequest episodeNotificationSearchRequest = new EpisodeNotificationSearchRequest(Collections.singletonList(1L));
        Client client = new Client(171L, "test", "test", null);
        EpisodeUnreadNotificationResponse episodeNotificationResponse = new EpisodeUnreadNotificationResponse();
        Mockito.when(clientService.getCurrentClient()).thenReturn(client);
        Mockito.when(episodeNotificationService.getUnreadNotificationForEpisodes(any(), eq(171L))).thenReturn(episodeNotificationResponse);

        mockMvc
                .perform(
                        MockMvcRequestBuilders
                                .post(uri)
                                .contentType(MediaType.APPLICATION_JSON)
                                .content(Utils.asJsonString(episodeNotificationSearchRequest))
                                .accept(MediaType.APPLICATION_JSON_VALUE)
                )
                .andExpect(status().isOk());
    }

    @Test
    public void testUpdateNotificationBulkTestEmptyEpisodeIdList() throws Exception {
        String uri = "/v1/notification/bulk";
        UpdateEpisodeNotificationBulkRequest updateEpisodeNotificationBulkRequest = new UpdateEpisodeNotificationBulkRequest();
        mockMvc
                .perform(
                        MockMvcRequestBuilders
                                .put(uri)
                                .contentType(MediaType.APPLICATION_JSON)
                                .content(Utils.asJsonString(updateEpisodeNotificationBulkRequest))
                                .accept(MediaType.APPLICATION_JSON_VALUE)
                )
                .andExpect(status().isBadRequest());
    }
    
    @Test
    public void testAddNotificationLogsFailure() throws Exception {
        String uri = "/v1/notification/logs";
        NotificationLogRequest notificationLogRequest = new NotificationLogRequest();
        notificationLogRequest.setNotifications(Collections.singletonList(new NotificationLogRequest.Notification()));
        mockMvc
                .perform(
                        MockMvcRequestBuilders
                                .post(uri)
                                .contentType(MediaType.APPLICATION_JSON)
                                .content(Utils.asJsonString(notificationLogRequest))
                                .accept(MediaType.APPLICATION_JSON_VALUE)
                )
                .andExpect(status().isBadRequest());
    }

    @Test
    public void getUnreadEpisodeNotificationTestEmptyEpisodeIdList() throws Exception {
        String uri = "/v1/notification/unread/bulk";
        EpisodeNotificationSearchRequest episodeNotificationSearchRequest = new EpisodeNotificationSearchRequest();
        mockMvc
                .perform(
                        MockMvcRequestBuilders
                                .post(uri)
                                .contentType(MediaType.APPLICATION_JSON)
                                .content(Utils.asJsonString(episodeNotificationSearchRequest))
                                .accept(MediaType.APPLICATION_JSON_VALUE)
                )
                .andExpect(status().isBadRequest());
    }
    @Test
    public void testAddNotificationLogsSuccess() throws Exception {
        String uri = "/v1/notification/logs";
        NotificationLogRequest notificationLogRequest = new NotificationLogRequest();
        notificationLogRequest.setEpisodeId(1L);
        notificationLogRequest.setNotifications(Collections.singletonList(new NotificationLogRequest.Notification()));
        mockMvc
                .perform(
                        MockMvcRequestBuilders
                                .post(uri)
                                .contentType(MediaType.APPLICATION_JSON)
                                .content(Utils.asJsonString(notificationLogRequest))
                                .accept(MediaType.APPLICATION_JSON_VALUE)
                )
                .andExpect(status().isOk());
    }
}
