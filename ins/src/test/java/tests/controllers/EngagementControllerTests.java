package tests.controllers;

import com.everwell.ins.controllers.EngagementController;
import com.everwell.ins.exceptions.CustomExceptionHandler;
import com.everwell.ins.models.db.Engagement;
import com.everwell.ins.models.db.Language;
import com.everwell.ins.models.db.Type;
import com.everwell.ins.models.http.requests.EngagementBulkRequest;
import com.everwell.ins.models.http.requests.EngagementRequest;
import com.everwell.ins.services.EngagementService;
import com.everwell.ins.services.LanguageService;
import com.everwell.ins.services.TypeService;
import com.everwell.ins.utils.Utils;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import tests.BaseTest;

import java.util.Collections;
import java.util.Date;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

public class EngagementControllerTests extends BaseTest {

    private MockMvc mockMvc;

    @Mock
    private EngagementService engagementService;

    @Mock
    private TypeService typeService;

    @Mock
    private LanguageService languageService;

    @InjectMocks
    private EngagementController engagementController;

    private static Long id = 1L;
    private static String content = "text";
    private static String entityId = "1";
    private static Long typeId = 1L;
    private static Long languageId = 1L;
    private static Boolean consent = true;
    private static Date time = Utils.getCurrentDate();
    private static String timeOfDay = "2019-08-31 18:30:00";

    private static final String MESSAGE_ENTITY_ID_REQUIRED = "entity id is required";

    private static final String MESSAGE_CONSENT_REQUIRED = "consent is required";

    @Before
    public void setup() {
        mockMvc = MockMvcBuilders.standaloneSetup(engagementController)
                .setControllerAdvice(new CustomExceptionHandler())
                .build();
    }

    @Test
    public void testSaveEngagementConfig() throws Exception {
        String uri = "/v1/engagement";
        EngagementRequest request = new EngagementRequest(entityId, consent, timeOfDay, languageId, typeId);

        when(engagementService.saveEngagementConfig(any())).thenReturn(id);

        mockMvc
                .perform(
                        MockMvcRequestBuilders
                                .post(uri)
                                .contentType(MediaType.APPLICATION_JSON)
                                .content(Utils.asJsonString(request))
                                .accept(MediaType.APPLICATION_JSON_VALUE)
                )
                .andExpect(status().isAccepted())
                .andExpect(MockMvcResultMatchers.jsonPath("$.data").value(id));
        verify(engagementService, Mockito.times(1)).saveEngagementConfig(any());
    }

    @Test
    public void testSaveEngagementConfigBulk() throws Exception {
        String uri = "/v1/engagement-bulk";
        EngagementRequest engagementRequest = new EngagementRequest(entityId, consent, timeOfDay, languageId, typeId);
        EngagementBulkRequest request = new EngagementBulkRequest(Collections.singletonList(engagementRequest));

        doNothing().when(engagementService).saveEngagementConfigBulk(any());

        mockMvc
                .perform(
                        MockMvcRequestBuilders
                                .post(uri)
                                .contentType(MediaType.APPLICATION_JSON)
                                .content(Utils.asJsonString(request))
                                .accept(MediaType.APPLICATION_JSON_VALUE)
                )
                .andExpect(status().isAccepted());
        verify(engagementService, Mockito.times(1)).saveEngagementConfigBulk(any());
    }

    @Test
    public void testSaveEngagementConfigWithoutEntityId() throws Exception {
        String uri = "/v1/engagement";
        EngagementRequest request = new EngagementRequest(null, consent, timeOfDay, languageId, typeId);

        mockMvc
                .perform(
                        MockMvcRequestBuilders
                                .post(uri)
                                .contentType(MediaType.APPLICATION_JSON)
                                .content(Utils.asJsonString(request))
                                .accept(MediaType.APPLICATION_JSON_VALUE)
                )
                .andExpect(status().isBadRequest())
                .andExpect(MockMvcResultMatchers.jsonPath("$.message").value(MESSAGE_ENTITY_ID_REQUIRED));
    }

    @Test
    public void testSaveEngagementConfigWithoutConsent() throws Exception {
        String uri = "/v1/engagement";
        EngagementRequest request = new EngagementRequest(entityId, null, timeOfDay, languageId, typeId);

        mockMvc
                .perform(
                        MockMvcRequestBuilders
                                .post(uri)
                                .contentType(MediaType.APPLICATION_JSON)
                                .content(Utils.asJsonString(request))
                                .accept(MediaType.APPLICATION_JSON_VALUE)
                )
                .andExpect(status().isBadRequest())
                .andExpect(MockMvcResultMatchers.jsonPath("$.message").value(MESSAGE_CONSENT_REQUIRED));
    }

    @Test
    public void testSaveEngagementConfigBulkWithoutEntityId() throws Exception {
        String uri = "/v1/engagement-bulk";
        EngagementRequest engagementRequest = new EngagementRequest(null, consent, timeOfDay, languageId, typeId);
        EngagementBulkRequest request = new EngagementBulkRequest(Collections.singletonList(engagementRequest));
        

        mockMvc
                .perform(
                        MockMvcRequestBuilders
                                .post(uri)
                                .contentType(MediaType.APPLICATION_JSON)
                                .content(Utils.asJsonString(request))
                                .accept(MediaType.APPLICATION_JSON_VALUE)
                )
                .andExpect(status().isBadRequest());
    }

    @Test
    public void testSaveEngagementConfigBulkWithoutList() throws Exception {
        String uri = "/v1/engagement-bulk";
        EngagementBulkRequest request = new EngagementBulkRequest();


        mockMvc
                .perform(
                        MockMvcRequestBuilders
                                .post(uri)
                                .contentType(MediaType.APPLICATION_JSON)
                                .content(Utils.asJsonString(request))
                                .accept(MediaType.APPLICATION_JSON_VALUE)
                )
                .andExpect(status().isBadRequest());
    }


    @Test
    public void testUpdateEngagementConfig() throws Exception {
        String uri = "/v1/engagement";
        Type type = new Type(id, content);
        Language language = new Language(id, content);
        EngagementRequest request = new EngagementRequest(entityId, consent, timeOfDay, languageId, typeId);
        Engagement engagement = new Engagement(entityId, consent, time, languageId, typeId);

        when(engagementService.updateEngagementConfig(any())).thenReturn(engagement);
        when(typeService.getType(any())).thenReturn(type);
        when(languageService.getLanguage(any())).thenReturn(language);

        mockMvc
                .perform(
                        MockMvcRequestBuilders
                                .put(uri)
                                .contentType(MediaType.APPLICATION_JSON)
                                .content(Utils.asJsonString(request))
                                .accept(MediaType.APPLICATION_JSON_VALUE)
                )
                .andExpect(status().isAccepted())
                .andExpect(MockMvcResultMatchers.jsonPath("$.data.entityId").value(entityId));
        ;
    }

    @Test
    public void testUpdateEngagementConfigBulk() throws Exception {
        String uri = "/v1/engagement-bulk";
        Type type = new Type(id, content);
        Language language = new Language(id, content);
        Engagement engagement = new Engagement(entityId, consent, time, languageId, typeId);
        EngagementRequest engagementRequest = new EngagementRequest(entityId, consent, timeOfDay, languageId, typeId);
        EngagementBulkRequest request = new EngagementBulkRequest(Collections.singletonList(engagementRequest));


        when(engagementService.updateEngagementConfig(any())).thenReturn(engagement);
        when(typeService.getType(eq(typeId))).thenReturn(type);
        when(languageService.getLanguage(eq(languageId))).thenReturn(language);

        mockMvc
                .perform(
                        MockMvcRequestBuilders
                                .put(uri)
                                .contentType(MediaType.APPLICATION_JSON)
                                .content(Utils.asJsonString(request))
                                .accept(MediaType.APPLICATION_JSON_VALUE)
                )
                .andExpect(status().isAccepted())
                .andExpect(MockMvcResultMatchers.jsonPath("$.data[0].entityId").value(entityId));
        ;
    }

    @Test
    public void testGetEngagementConfig() throws Exception {
        String uri = "/v1/engagement/" + entityId;
        Type type = new Type(id, content);
        Language language = new Language(id, content);
        Engagement engagement = new Engagement(entityId, consent, time, languageId, typeId);

        when(engagementService.getEngagementConfig(any(), anyLong())).thenReturn(engagement);
        when(typeService.getType(any())).thenReturn(type);
        when(languageService.getLanguage(any())).thenReturn(language);

        mockMvc
                .perform(
                        MockMvcRequestBuilders
                                .get(uri)
                                .contentType(MediaType.APPLICATION_JSON)
                                .accept(MediaType.APPLICATION_JSON_VALUE)
                )
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.data.entityId").value(entityId));
    }

    @Test
    public void testGetEngagementConfigBulk() throws Exception {
        String uri = "/v1/engagement-bulk/" + entityId;
        Type type = new Type(id, content);
        Language language = new Language(id, content);
        Engagement engagement = new Engagement(entityId, consent, time, languageId, typeId);

        when(engagementService.getEngagementBulk(eq(entityId))).thenReturn(Collections.singletonList(engagement));
        when(typeService.getType(eq(typeId))).thenReturn(type);
        when(languageService.getLanguage(eq(languageId))).thenReturn(language);

        mockMvc
                .perform(
                        MockMvcRequestBuilders
                                .get(uri)
                                .contentType(MediaType.APPLICATION_JSON)
                                .accept(MediaType.APPLICATION_JSON_VALUE)
                )
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.data[0].entityId").value(entityId));
    }

    @Test
    public void testGetEngagementConfigWithoutLanguageAndType() throws Exception {
        String uri = "/v1/engagement/" + entityId;
        Engagement engagement = new Engagement(entityId, consent, time, null, null);

        when(engagementService.getEngagementConfig(any(), anyLong())).thenReturn(engagement);

        mockMvc
                .perform(
                        MockMvcRequestBuilders
                                .get(uri)
                                .contentType(MediaType.APPLICATION_JSON)
                                .accept(MediaType.APPLICATION_JSON_VALUE)
                )
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.data.entityId").value(entityId));
    }

    @Test
    public void testUpdateEngagementConfigWithoutLanguageAndType() throws Exception {
        String uri = "/v1/engagement";
        EngagementRequest request = new EngagementRequest(entityId, consent, timeOfDay, null, null);
        Engagement engagement = new Engagement(entityId, consent, time, null, null);

        when(engagementService.updateEngagementConfig(any())).thenReturn(engagement);

        mockMvc
                .perform(
                        MockMvcRequestBuilders
                                .put(uri)
                                .contentType(MediaType.APPLICATION_JSON)
                                .content(Utils.asJsonString(request))
                                .accept(MediaType.APPLICATION_JSON_VALUE)
                )
                .andExpect(status().isAccepted())
                .andExpect(MockMvcResultMatchers.jsonPath("$.data.entityId").value(entityId));
        ;
    }

}
