package tests.vendors;

import com.everwell.ins.enums.Language;
import com.everwell.ins.enums.SmsGateway;
import com.everwell.ins.exceptions.VendorException;
import com.everwell.ins.models.dto.SmsDto;
import com.everwell.ins.models.dto.VendorResponseDto;
import com.everwell.ins.models.dto.vendorConfigs.SmsPohConfigDto;
import com.everwell.ins.models.dto.vendorCreds.ApiKeySecretDto;
import com.everwell.ins.vendors.SmsPohHandler;
import org.asynchttpclient.*;
import org.asynchttpclient.netty.NettyResponseFuture;
import org.junit.Assert;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.powermock.core.classloader.annotations.PrepareForTest;

import java.util.List;
import java.util.concurrent.ExecutionException;

import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;
import static org.powermock.api.mockito.PowerMockito.mockStatic;

public class SmsPohHandlerTest extends SmsHandlerTest{
    @InjectMocks
    SmsPohHandler smsPohHandler;
    @Mock
    private ApiKeySecretDto creds;
    @Mock
    private SmsPohConfigDto config;

    protected String smsPohResponse = "{\"status\":true,\"data\":{\"messages\":[{\"id\":110352868,\"create_at\":1698738909,\"message_to\":\"256334433221\",\"message_text\":\"test message\",\"operator\":\"Unknown\",\"sender\":\"CareConnect\",\"is_delivered\":false,\"is_queuing\":true,\"test\":true,\"num_parts\":null,\"credit\":0},{\"id\":110352869,\"create_at\":1698738909,\"message_to\":\"256334433222\",\"message_text\":\"test message\",\"operator\":\"Unknown\",\"sender\":\"CareConnect\",\"is_delivered\":false,\"is_queuing\":true,\"test\":true,\"num_parts\":null,\"credit\":0}],\"balance\":10332,\"credit\":0}}";


    @Test
    public void testSetVendorParams() {
        Long vendorId = 1L;
        when(vendorService.getVendorConfig(any(), any())).thenReturn(null);
        when(vendorService.getVendorCredentials(any(), any())).thenReturn(null);

        smsPohHandler.setVendorParams(vendorId);
        verify(vendorService, Mockito.times(1)).getVendorConfig(any(), any());
        verify(vendorService, Mockito.times(1)).getVendorCredentials(any(), any());
    }

    @Test
    public void testVendorGateway() {
        SmsGateway gateway = smsPohHandler.getVendorGatewayEnumerator();
        Assert.assertEquals(SmsGateway.SMSPOH.toString(), gateway.toString());
    }

    @Test
    public void testGetBatchSize() {
        Integer size = 5;
        when(config.getBulkSmsLimit()).thenReturn("5");
        Integer batchSize1 = smsPohHandler.getBatchSize(true);
        assertEquals(size, batchSize1);
        size = 1;
        Integer batchSize2 = smsPohHandler.getBatchSize(false);
        assertEquals(size, batchSize2);
    }

    @PrepareForTest({AsyncHttpClient.class, NettyResponseFuture.class, Dsl.class})
    @Test
    public void testVendorCall() throws InterruptedException, ExecutionException {
        List<SmsDto> persons = getSmsDtos();
        mockStatic(Dsl.class);
        DefaultAsyncHttpClient client = mock(DefaultAsyncHttpClient.class);
        Response response = mock(Response.class);
        NettyResponseFuture<Response> future = mock(NettyResponseFuture.class);
        when(creds.getUrl()).thenReturn(testUrl);
        when(creds.getApiKey()).thenReturn(testString);
        when(Dsl.asyncHttpClient()).thenReturn(client);
        when(client.executeRequest(any(Request.class))).thenReturn(future);
        when(future.get()).thenReturn(response);
        when(response.getStatusCode()).thenReturn(200);
        when(response.getResponseBody()).thenReturn(smsPohResponse);

        VendorResponseDto expectedDto = new VendorResponseDto();
        expectedDto.setApiResponse(smsPohResponse);
        VendorResponseDto responseDto = smsPohHandler.vendorCall(persons, Language.NON_UNICODE, 1L);
        assertEquals(expectedDto.getApiResponse(), responseDto.getApiResponse());
    }

    @PrepareForTest({AsyncHttpClient.class, NettyResponseFuture.class, Dsl.class})
    @Test(expected = VendorException.class)
    public void testVendorCallException() throws InterruptedException, ExecutionException {
        List<SmsDto> persons = getSmsDtos();
        mockStatic(Dsl.class);
        DefaultAsyncHttpClient client = mock(DefaultAsyncHttpClient.class);
        Response response = mock(Response.class);
        NettyResponseFuture<Response> future = mock(NettyResponseFuture.class);
        when(creds.getUrl()).thenReturn(testUrl);
        when(creds.getApiKey()).thenReturn(testString);
        when(Dsl.asyncHttpClient()).thenReturn(client);
        when(client.executeRequest(any(Request.class))).thenReturn(future);
        when(future.get()).thenReturn(response);
        when(response.getStatusCode()).thenReturn(200);
        when(response.getResponseBody()).thenReturn(testApiResponse);

        VendorResponseDto expectedDto = new VendorResponseDto();
        expectedDto.setApiResponse(testApiResponse);
        VendorResponseDto responseDto = smsPohHandler.vendorCall(persons, Language.NON_UNICODE, 1L);
        assertEquals(expectedDto.getApiResponse(), responseDto.getApiResponse());
    }





}
