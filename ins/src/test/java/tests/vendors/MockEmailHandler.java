package tests.vendors;

import com.everwell.ins.enums.EmailGateway;
import com.everwell.ins.models.dto.EmailTemplateDto;
import com.everwell.ins.models.dto.SendgridResponseDto;
import com.everwell.ins.models.http.requests.EmailRequest;
import com.everwell.ins.vendors.EmailHandler;

import java.io.IOException;
import java.util.List;

public class MockEmailHandler extends EmailHandler {
    @Override
    public void setVendorParams(Long vendorId) {
    }

    @Override
    protected SendgridResponseDto sendgridCall(List<EmailTemplateDto> recipients, EmailRequest emailRequest) throws IOException {
        return null;
    }

    @Override
    public EmailGateway getVendorGatewayEnumerator() {
        return null;
    }
}
