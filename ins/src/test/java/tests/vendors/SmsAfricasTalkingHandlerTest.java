package tests.vendors;


import com.africastalking.AfricasTalking;
import com.africastalking.SmsService;
import com.africastalking.sms.Recipient;
import com.everwell.ins.enums.Language;
import com.everwell.ins.enums.SmsGateway;
import com.everwell.ins.exceptions.NotImplementedException;
import com.everwell.ins.exceptions.ValidationException;
import com.everwell.ins.exceptions.VendorException;
import com.everwell.ins.models.dto.SmsDto;
import com.everwell.ins.models.dto.VendorResponseDto;
import com.everwell.ins.models.dto.vendorConfigs.AfricasTalkingConfigDto;
import com.everwell.ins.models.dto.vendorCreds.ApiKeyFromDto;
import com.everwell.ins.utils.SentryUtils;
import com.everwell.ins.vendors.SmsAfricasTalkingHandler;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;

import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.*;
import static org.powermock.api.mockito.PowerMockito.mockStatic;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;

@PrepareForTest({AfricasTalking.class, SmsService.class, SentryUtils.class})
public class SmsAfricasTalkingHandlerTest extends SmsHandlerTest {

    @InjectMocks
    private SmsAfricasTalkingHandler smsAfricasTalkingHandler;

    @Mock
    private AfricasTalkingConfigDto config;

    @Mock
    private ApiKeyFromDto creds;

    @Mock
    SmsService sms;

    @Test
    public void testGetBatchSize() {
        Integer size = 5;
        when(config.getBulkSmsLimit()).thenReturn("5");

        Integer batchSize1 = smsAfricasTalkingHandler.getBatchSize(true);
        assertEquals(size, batchSize1);

        size = 1;
        Integer batchSize2 = smsAfricasTalkingHandler.getBatchSize(false);
        assertEquals(size, batchSize2);
    }

    @Test(expected = NotImplementedException.class)
    public void testGetLanguageMapping() {
        smsAfricasTalkingHandler.getLanguageMapping(Language.NON_UNICODE);
    }

    @Test
    public void testSetVendorParams() {
        Long vendorId = 1L;
        when(vendorService.getVendorConfig(any(), any())).thenReturn(null);
        when(vendorService.getVendorCredentials(any(), any())).thenReturn(null);

        smsAfricasTalkingHandler.setVendorParams(vendorId);
        verify(vendorService, Mockito.times(1)).getVendorConfig(any(), any());
        verify(vendorService, Mockito.times(1)).getVendorCredentials(any(), any());
    }

    @Test
    public void testVendorCall() throws IOException {
        List<SmsDto> persons = getSmsDtos();
        List<String> phones = getPhones();
        List<Recipient> response = new ArrayList<>();
        Recipient recipient = new Recipient();
        recipient.status = testApiResponse;
        recipient.messageId = testMessageId;
        response.add(recipient);

        mockStatic(AfricasTalking.class);

        when(AfricasTalking.getService(anyString())).thenReturn(sms);
        when(creds.getFrom()).thenReturn(testString);
        when(sms.send(persons.get(0).getMessage(), testString, phones.toArray(new String[0]), true)).thenReturn(response);

        VendorResponseDto expectedDto = new VendorResponseDto(testApiResponse, testMessageId);
        VendorResponseDto responseDto = smsAfricasTalkingHandler.vendorCall(persons, Language.NON_UNICODE, null);
        assertEquals(expectedDto.getMessageId(), responseDto.getMessageId());
        assertEquals(expectedDto.getApiResponse(), responseDto.getApiResponse());
    }

    @Test
    public void testVendorCallNullResponse() throws IOException {
        List<SmsDto> persons = getSmsDtos();
        mockStatic(AfricasTalking.class);

        mockStatic(SentryUtils.class);
        when(AfricasTalking.getService(anyString())).thenReturn(sms);
        when(creds.getFrom()).thenReturn("string");
        when(sms.send(anyString(), anyString(), any(), anyBoolean())).thenReturn(null);

        smsAfricasTalkingHandler.vendorCall(persons, Language.NON_UNICODE, null);
        PowerMockito.verifyStatic(SentryUtils.class, Mockito.times(1));
        SentryUtils.captureException(any());
    }

//    @Test(expected = VendorException.class)
//    public void testVendorCallException() throws IOException {
//        Long vendorId = 1L;
//        List<SmsDto> persons = getSmsDtos();
//        mockStatic(AfricasTalking.class);
//
//        when(AfricasTalking.getService(anyString())).thenReturn(sms);
//        when(creds.getFrom()).thenReturn("string");
//        when(sms.send(anyString(), anyString(), any(), anyBoolean())).thenThrow(IOException.class);
//       // when(smsAfricasTalkingHandler.getVendorGatewayEnumerator()).thenReturn(SmsGateway.AFRICASTALKING);
//
//        smsAfricasTalkingHandler.vendorCall(persons, Language.NON_UNICODE, vendorId);
//    }

    @Test(expected = ValidationException.class)
    public void testVendorCallNullException() {
        List<SmsDto> persons = null;
        smsAfricasTalkingHandler.vendorCall(persons, Language.NON_UNICODE, null);
    }
}
