package tests.vendors;

import com.everwell.ins.binders.INSEventBinder;
import com.everwell.ins.models.http.requests.DeactivateDeviceIdsRequest;
import com.everwell.ins.services.RabbitMQPublisherService;
import com.everwell.ins.vendors.SpringEventHandler;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.Spy;
import org.springframework.messaging.MessageChannel;
import org.springframework.test.context.event.annotation.BeforeTestMethod;
import tests.BaseTest;

import java.util.ArrayList;
import java.util.List;

import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.verify;

public class SpringEventHandlerTest extends BaseTest {

    @Spy
    @InjectMocks
    SpringEventHandler springEventHandler;

    @Mock
    RabbitMQPublisherService rabbitMQPublisherService;


    @Test
    public void deactivateDeviceIdEvent() {
        List<String> invalidDevice = new ArrayList<>();
        invalidDevice.add("test");
        DeactivateDeviceIdsRequest deactivateDeviceIdsRequest = new DeactivateDeviceIdsRequest(invalidDevice);
        springEventHandler.deactivateDeviceIdEvent(deactivateDeviceIdsRequest);
        verify(rabbitMQPublisherService, Mockito.times(1)).send(any(),any(),anyString(),anyMap());verify(rabbitMQPublisherService, Mockito.times(1)).send(any(),any(),anyString(),anyMap());
    }

}
