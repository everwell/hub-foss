package tests.services;

import com.everwell.ins.constants.Constants;
import com.everwell.ins.controllers.SmsController;
import com.everwell.ins.exceptions.ValidationException;
import com.everwell.ins.services.impl.EmailServiceImpl;
import com.everwell.ins.services.impl.EventServiceConsumerServiceImpl;
import com.everwell.ins.services.impl.PushNotificationServiceImpl;
import com.everwell.ins.services.impl.ReconciliationServiceImpl;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.core.MessageProperties;
import tests.BaseTest;

import java.io.IOException;

import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.*;

public class EventServiceConsumerServiceTest extends BaseTest {

    @InjectMocks
    EventServiceConsumerServiceImpl eventServiceConsumerService;

    @Mock
    SmsController smsController;

    @Mock
    PushNotificationServiceImpl pushNotificationServiceimpl;

    @Mock
    EmailServiceImpl emailService;

    @Mock
    ReconciliationServiceImpl reconciliationService;

    @Test
    public void processSms_Success() throws IOException {
        String jsonPayload = "{\"EventName\":\"process_sms\",\"Field\":{\"triggerId\":1,\"vendorId\":3,\"templateId\":1,\"templateIds\":[1,2],\"personList\":[{\"id\":1,\"phone\":\"9705197814\",\"parameters\":{\"name\":\"v1\",\"phone\":\"v1\"}},{\"id\":2,\"phone\":\"9705197814\",\"parameters\":{\"name\":\"v12\",\"phone\":\"v12\"}},{\"id\":3,\"phone\":\"9705197814\",\"parameters\":{\"name\":\"v1\",\"id\":\"v1\"}}],\"isMandatory\":true,\"isDefaultTime\":true,\"defaultConsent\":true,\"timeOfSms\":\"2019-07-30 18:30:00\"}}";
        Message message = new Message(jsonPayload.getBytes(), new MessageProperties());
        message.getMessageProperties().setHeader(Constants.RMQ_HEADER_CLIENT_ID, "29");
        when(smsController.sendBulkSms(any())).thenReturn(null);
        eventServiceConsumerService.processSmsConsumer(message);
        Mockito.verify(smsController, Mockito.times(1)).sendBulkSms(any());
    }

    @Test(expected = ValidationException.class)
    public void processSms_NoClientId() throws IOException {
        String jsonPayload = "{\"EventName\":\"process_sms\",\"Field\":{\"triggerId\":1,\"vendorId\":3,\"templateId\":1,\"templateIds\":[1,2],\"personList\":[{\"id\":1,\"phone\":\"9705197814\",\"parameters\":{\"name\":\"v1\",\"phone\":\"v1\"}},{\"id\":2,\"phone\":\"9705197814\",\"parameters\":{\"name\":\"v12\",\"phone\":\"v12\"}},{\"id\":3,\"phone\":\"9705197814\",\"parameters\":{\"name\":\"v1\",\"id\":\"v1\"}}],\"isMandatory\":true,\"isDefaultTime\":true,\"defaultConsent\":true,\"timeOfSms\":\"2019-07-30 18:30:00\"}}";
        Message message = new Message(jsonPayload.getBytes(), new MessageProperties());

        eventServiceConsumerService.processSmsConsumer(message);
    }

    @Test(expected = ValidationException.class)
    public void processCall_NoTriggerId() throws IOException {
        String jsonPayload = "{\"EventName\":\"process_sms\",\"Field\":{\"vendorId\":3,\"templateId\":1,\"templateIds\":[1,2],\"personList\":[{\"id\":1,\"phone\":\"9705197814\",\"parameters\":{\"name\":\"v1\",\"phone\":\"v1\"}},{\"id\":2,\"phone\":\"9705197814\",\"parameters\":{\"name\":\"v12\",\"phone\":\"v12\"}},{\"id\":3,\"phone\":\"9705197814\",\"parameters\":{\"name\":\"v1\",\"id\":\"v1\"}}],\"isMandatory\":true,\"isDefaultTime\":true,\"defaultConsent\":true,\"timeOfSms\":\"2019-07-30 18:30:00\"}}";
        Message message = new Message(jsonPayload.getBytes(), new MessageProperties());
        eventServiceConsumerService.processSmsConsumer(message);
    }

    @Test(expected = ValidationException.class)
    public void processCall_NoVendorId() throws IOException {
        String jsonPayload = "{\"EventName\":\"process_sms\",\"Field\":{\"triggerId\":1,\"templateId\":1,\"templateIds\":[1,2],\"personList\":[{\"id\":1,\"phone\":\"9705197814\",\"parameters\":{\"name\":\"v1\",\"phone\":\"v1\"}},{\"id\":2,\"phone\":\"9705197814\",\"parameters\":{\"name\":\"v12\",\"phone\":\"v12\"}},{\"id\":3,\"phone\":\"9705197814\",\"parameters\":{\"name\":\"v1\",\"id\":\"v1\"}}],\"isMandatory\":true,\"isDefaultTime\":true,\"defaultConsent\":true,\"timeOfSms\":\"2019-07-30 18:30:00\"}}";
        Message message = new Message(jsonPayload.getBytes(), new MessageProperties());
        eventServiceConsumerService.processSmsConsumer(message);
    }

    @Test(expected = ValidationException.class)
    public void processCall_NoTemplateId() throws IOException {
        String jsonPayload = "{\"EventName\":\"process_sms\",\"Field\":{\"triggerId\":1,\"vendorId\":3,\"templateIds\":[1,2],\"personList\":[{\"id\":1,\"phone\":\"9705197814\",\"parameters\":{\"name\":\"v1\",\"phone\":\"v1\"}},{\"id\":2,\"phone\":\"9705197814\",\"parameters\":{\"name\":\"v12\",\"phone\":\"v12\"}},{\"id\":3,\"phone\":\"9705197814\",\"parameters\":{\"name\":\"v1\",\"id\":\"v1\"}}],\"isMandatory\":true,\"isDefaultTime\":true,\"defaultConsent\":true,\"timeOfSms\":\"2019-07-30 18:30:00\"}}";
        Message message = new Message(jsonPayload.getBytes(), new MessageProperties());
        eventServiceConsumerService.processSmsConsumer(message);
    }

    @Test(expected = ValidationException.class)
    public void processCall_NoIsMandatory() throws IOException {
        String jsonPayload = "{\"EventName\":\"process_sms\",\"Field\":{\"triggerId\":1,\"vendorId\":3,\"templateId\":1,\"templateIds\":[1,2],\"personList\":[{\"id\":1,\"phone\":\"9705197814\",\"parameters\":{\"name\":\"v1\",\"phone\":\"v1\"}},{\"id\":2,\"phone\":\"9705197814\",\"parameters\":{\"name\":\"v12\",\"phone\":\"v12\"}},{\"id\":3,\"phone\":\"9705197814\",\"parameters\":{\"name\":\"v1\",\"id\":\"v1\"}}],\"isDefaultTime\":true,\"defaultConsent\":true,\"timeOfSms\":\"2019-07-30 18:30:00\"}}";
        Message message = new Message(jsonPayload.getBytes(), new MessageProperties());
        eventServiceConsumerService.processSmsConsumer(message);
    }

    @Test(expected = ValidationException.class)
    public void processCall_NoDefaultConsent() throws IOException {
        String jsonPayload = "{\"EventName\":\"process_sms\",\"Field\":{\"triggerId\":1,\"vendorId\":3,\"templateId\":1,\"templateIds\":[1,2],\"personList\":[{\"id\":1,\"phone\":\"9705197814\",\"parameters\":{\"name\":\"v1\",\"phone\":\"v1\"}},{\"id\":2,\"phone\":\"9705197814\",\"parameters\":{\"name\":\"v12\",\"phone\":\"v12\"}},{\"id\":3,\"phone\":\"9705197814\",\"parameters\":{\"name\":\"v1\",\"id\":\"v1\"}}],\"isMandatory\":true,\"isDefaultTime\":true,\"timeOfSms\":\"2019-07-30 18:30:00\"}}";
        Message message = new Message(jsonPayload.getBytes(), new MessageProperties());
        eventServiceConsumerService.processSmsConsumer(message);
    }

    @Test(expected = ValidationException.class)
    public void processCall_NoDefaultTime() throws IOException {
        String jsonPayload = "{\"EventName\":\"process_sms\",\"Field\":{\"triggerId\":1,\"vendorId\":3,\"templateId\":1,\"templateIds\":[1,2],\"personList\":[{\"id\":1,\"phone\":\"9705197814\",\"parameters\":{\"name\":\"v1\",\"phone\":\"v1\"}},{\"id\":2,\"phone\":\"9705197814\",\"parameters\":{\"name\":\"v12\",\"phone\":\"v12\"}},{\"id\":3,\"phone\":\"9705197814\",\"parameters\":{\"name\":\"v1\",\"id\":\"v1\"}}],\"isMandatory\":true,\"defaultConsent\":true,\"timeOfSms\":\"2019-07-30 18:30:00\"}}";
        Message message = new Message(jsonPayload.getBytes(), new MessageProperties());
        eventServiceConsumerService.processSmsConsumer(message);
    }

    @Test(expected = ValidationException.class)
    public void processCall_NoTimeOfSms() throws IOException {
        String jsonPayload = "{\"EventName\":\"process_sms\",\"Field\":{\"triggerId\":1,\"vendorId\":3,\"templateId\":1,\"templateIds\":[1,2],\"personList\":[{\"id\":1,\"phone\":\"9705197814\",\"parameters\":{\"name\":\"v1\",\"phone\":\"v1\"}},{\"id\":2,\"phone\":\"9705197814\",\"parameters\":{\"name\":\"v12\",\"phone\":\"v12\"}},{\"id\":3,\"phone\":\"9705197814\",\"parameters\":{\"name\":\"v1\",\"id\":\"v1\"}}],\"isMandatory\":true,\"isDefaultTime\":false,\"defaultConsent\":true}}";
        Message message = new Message(jsonPayload.getBytes(), new MessageProperties());
        eventServiceConsumerService.processSmsConsumer(message);
    }

    @Test(expected = ValidationException.class)
    public void processCall_NoPersonList() throws IOException {
        String jsonPayload = "{\"EventName\":\"process_sms\",\"Field\":{\"triggerId\":1,\"vendorId\":3,\"templateId\":1,\"templateIds\":[1,2],\"personList\":[],\"isMandatory\":true,\"isDefaultTime\":true,\"defaultConsent\":true,\"timeOfSms\":\"2019-07-30 18:30:00\"}}";
        Message message = new Message(jsonPayload.getBytes(), new MessageProperties());
        eventServiceConsumerService.processSmsConsumer(message);
    }

    @Test
    public void processNotification_Success() throws IOException {
        String jsonPayload = "{\"EventName\":\"process_push_notification\",\"Field\":{\"triggerId\":1,\"vendorId\":3,\"templateId\":1,\"isMandatory\":true,\"personList\":[{\"id\":1,\"deviceId\":\"9705197814\",\"contentParameters\":{\"name\":\"v1\",\"phone\":\"v1\"}},{\"id\":2,\"deviceId\":\"9705197814\",\"contentParameters\":{\"name\":\"v12\",\"phone\":\"v12\"}},{\"id\":3,\"deviceId\":\"9705197814\",\"contentParameters\":{\"name\":\"v1\",\"id\":\"v1\"}}]}}";
        Message message = new Message(jsonPayload.getBytes(), new MessageProperties());
        eventServiceConsumerService.processNotificationConsumer(message);
        Mockito.verify(pushNotificationServiceimpl, Mockito.times(1)).sendNotification(any());
    }

    @Test
    public void processNotification_Success_clientId() throws IOException {
        String jsonPayload = "{\"EventName\":\"process_push_notification\",\"Field\":{\"triggerId\":1,\"vendorId\":3,\"templateId\":1,\"isMandatory\":true,\"personList\":[{\"id\":1,\"deviceId\":\"9705197814\",\"contentParameters\":{\"name\":\"v1\",\"phone\":\"v1\"}},{\"id\":2,\"deviceId\":\"9705197814\",\"contentParameters\":{\"name\":\"v12\",\"phone\":\"v12\"}},{\"id\":3,\"deviceId\":\"9705197814\",\"contentParameters\":{\"name\":\"v1\",\"id\":\"v1\"}}]}}";
        Message message = new Message(jsonPayload.getBytes(), new MessageProperties());
        message.getMessageProperties().setHeader(Constants.RMQ_HEADER_CLIENT_ID, "171");
        eventServiceConsumerService.processNotificationConsumer(message);
        Mockito.verify(pushNotificationServiceimpl, Mockito.times(1)).sendNotification(any());
    }


    @Test(expected = ValidationException.class)
    public void processNotification_NoTriggerId() throws IOException {
        String jsonPayload = "{\"EventName\":\"process_push_notification\",\"Field\":{\"vendorId\":3,\"templateId\":1,\"isMandatory\":true,\"personList\":[{\"id\":1,\"deviceId\":\"9705197814\",\"contentParameters\":{\"name\":\"v1\",\"phone\":\"v1\"}},{\"id\":2,\"deviceId\":\"9705197814\",\"contentParameters\":{\"name\":\"v12\",\"phone\":\"v12\"}},{\"id\":3,\"deviceId\":\"9705197814\",\"contentParameters\":{\"name\":\"v1\",\"id\":\"v1\"}}]}}";
        Message message = new Message(jsonPayload.getBytes(), new MessageProperties());
        eventServiceConsumerService.processNotificationConsumer(message);
    }

    @Test(expected = ValidationException.class)
    public void processNotification_NoTemplateId() throws IOException {
        String jsonPayload = "{\"EventName\":\"process_push_notification\",\"Field\":{\"vendorId\":3,\"isMandatory\":true,\"personList\":[{\"id\":1,\"deviceId\":\"9705197814\",\"contentParameters\":{\"name\":\"v1\",\"phone\":\"v1\"}},{\"id\":2,\"deviceId\":\"9705197814\",\"contentParameters\":{\"name\":\"v12\",\"phone\":\"v12\"}},{\"id\":3,\"deviceId\":\"9705197814\",\"contentParameters\":{\"name\":\"v1\",\"id\":\"v1\"}}]}}";
        Message message = new Message(jsonPayload.getBytes(), new MessageProperties());
        eventServiceConsumerService.processNotificationConsumer(message);
    }

    @Test(expected = ValidationException.class)
    public void processNotification_NoVendorId() throws IOException {
        String jsonPayload = "{\"EventName\":\"process_push_notification\",\"Field\":{\"isMandatory\":true,\"personList\":[{\"id\":1,\"deviceId\":\"9705197814\",\"contentParameters\":{\"name\":\"v1\",\"phone\":\"v1\"}},{\"id\":2,\"deviceId\":\"9705197814\",\"contentParameters\":{\"name\":\"v12\",\"phone\":\"v12\"}},{\"id\":3,\"deviceId\":\"9705197814\",\"contentParameters\":{\"name\":\"v1\",\"id\":\"v1\"}}]}}";
        Message message = new Message(jsonPayload.getBytes(), new MessageProperties());
        eventServiceConsumerService.processNotificationConsumer(message);
    }

    @Test(expected = ValidationException.class)
    public void processNotification_NoPersonList() throws IOException {
        String jsonPayload = "{\"EventName\":\"process_push_notification\",\"Field\":{\"triggerId\":1,\"vendorId\":3,\"templateId\":1,\"personList\":[]}}";
        Message message = new Message(jsonPayload.getBytes(), new MessageProperties());
        eventServiceConsumerService.processNotificationConsumer(message);
    }

    @Test
    public void proecessEmailSuccess() throws IOException {
        String jsonPayload = "{\"EventName\": \"process_email\",\"Field\": {\"triggerId\": 1,\"vendorId\": 14,\"templateId\": 1,\"sender\":\"bharat@everwell.org\",\"recipientList\": [{\"recipient\": \"bharat@everwell.org\"},{\"recipient\":\"bharat.agl3@gmail.com\"}]}}";
        Message message = new Message(jsonPayload.getBytes(), new MessageProperties());
        message.getMessageProperties().setHeader(Constants.RMQ_HEADER_CLIENT_ID, "171");
        eventServiceConsumerService.processEmailConsumer(message);
        Mockito.verify(emailService, Mockito.times(1)).sendEmail(any());
    }

    @Test(expected = ValidationException.class)
    public void proecessEmailNoVendor() throws IOException {
        String jsonPayload = "{\"EventName\": \"process_email\",\"Field\": {\"triggerId\": 1,\"templateId\": 1,\"sender\":\"bharat@everwell.org\",\"recipientList\": [{\"recipient\": \"bharat@everwell.org\"},{\"recipient\":\"bharat.agl3@gmail.com\"}]}}";
        Message message = new Message(jsonPayload.getBytes(), new MessageProperties());
        eventServiceConsumerService.processEmailConsumer(message);
    }

    @Test(expected = ValidationException.class)
    public void proecessEmailNoTemplate() throws IOException {
        String jsonPayload = "{\"EventName\": \"process_email\",\"Field\": {\"triggerId\": 1,\"vendorId\": 1,\"sender\":\"bharat@everwell.org\",\"recipientList\": [{\"recipient\": \"bharat@everwell.org\"},{\"recipient\":\"bharat.agl3@gmail.com\"}]}}";
        Message message = new Message(jsonPayload.getBytes(), new MessageProperties());
        eventServiceConsumerService.processEmailConsumer(message);
    }

    @Test(expected = ValidationException.class)
    public void proecessEmailNoTrigger() throws IOException {
        String jsonPayload = "{\"EventName\": \"process_email\",\"Field\": {\"templateId\": 1,\"vendorId\": 1,\"sender\":\"bharat@everwell.org\",\"recipientList\": [{\"recipient\": \"bharat@everwell.org\"},{\"recipient\":\"bharat.agl3@gmail.com\"}]}}";
        Message message = new Message(jsonPayload.getBytes(), new MessageProperties());
        eventServiceConsumerService.processEmailConsumer(message);
    }

    @Test(expected = ValidationException.class)
    public void proecessEmailNoSender() throws IOException {
        String jsonPayload = "{\"EventName\": \"process_email\",\"Field\": {\"templateId\": 1,\"vendorId\": 1,\"triggerId\": 1,\"recipientList\": [{\"recipient\": \"bharat@everwell.org\"},{\"recipient\":\"bharat.agl3@gmail.com\"}]}}";
        Message message = new Message(jsonPayload.getBytes(), new MessageProperties());
        eventServiceConsumerService.processEmailConsumer(message);
    }

    @Test(expected = ValidationException.class)
    public void proecessEmailInvalidSender() throws IOException {
        String jsonPayload = "{\"EventName\": \"process_email\",\"Field\": {\"triggerId\": 1,\"vendorId\": 14,\"templateId\": 1,\"sender\":\"bharat@everwell.org.\",\"recipientList\": [{\"recipient\": \"bharat@everwell.org\"},{\"recipient\":\"bharat.agl3@gmail.com\"}]}}";
        Message message = new Message(jsonPayload.getBytes(), new MessageProperties());
        eventServiceConsumerService.processEmailConsumer(message);
    }

    @Test(expected = ValidationException.class)
    public void proecessEmailInvalidRecipient() throws IOException {
        String jsonPayload = "{\"EventName\": \"process_email\",\"Field\": {\"triggerId\": 1,\"vendorId\": 14,\"templateId\": 1,\"sender\":\"bharat@everwell.org\",\"recipientList\": [{\"recipient\": \"bharat@everwell.org.\"},{\"recipient\":\"bharat.agl3@gmail.com\"}]}}";
        Message message = new Message(jsonPayload.getBytes(), new MessageProperties());
        eventServiceConsumerService.processEmailConsumer(message);
    }

    @Test(expected = ValidationException.class)
    public void proecessEmailNoRecipientList() throws IOException {
        String jsonPayload = "{\"EventName\": \"process_email\",\"Field\": {\"templateId\": 1,\"vendorId\": 1,\"triggerId\": 1,\"sender\":\"bharat@everwell.org\"}}";
        Message message = new Message(jsonPayload.getBytes(), new MessageProperties());
        eventServiceConsumerService.processEmailConsumer(message);
    }

    @Test(expected = ValidationException.class)
    public void proecessEmailNoAttachmentContent() throws IOException {
        String jsonPayload = "{\"EventName\": \"process_email\",\"Field\": {\"triggerId\": 1,\"vendorId\": 14,\"templateId\": 1,\"sender\":\"bharat@everwell.org\",\"attachmentDto\":{\"attachmentType\":\".pdf\",\"attachmentName\":\"abc\"},\"recipientList\": [{\"recipient\": \"bharat@everwell.org\"},{\"recipient\":\"bharat.agl3@gmail.com\"}]}}";
        Message message = new Message(jsonPayload.getBytes(), new MessageProperties());
        eventServiceConsumerService.processEmailConsumer(message);
    }

    @Test(expected = ValidationException.class)
    public void proecessEmailNoAttachmentName() throws IOException {
        String jsonPayload = "{\"EventName\": \"process_email\",\"Field\": {\"triggerId\": 1,\"vendorId\": 14,\"templateId\": 1,\"sender\":\"bharat@everwell.org\",\"attachmentDto\":{\"attachmentContent\":\"Hello my lets see email\",\"attachmentType\":\".pdf\"},\"recipientList\": [{\"recipient\": \"bharat@everwell.org\"},{\"recipient\":\"bharat.agl3@gmail.com\"}]}}";
        Message message = new Message(jsonPayload.getBytes(), new MessageProperties());
        eventServiceConsumerService.processEmailConsumer(message);
    }

    @Test(expected = ValidationException.class)
    public void proecessEmailNoAttachmentType() throws IOException {
        String jsonPayload = "{\"EventName\": \"process_email\",\"Field\": {\"triggerId\": 1,\"vendorId\": 14,\"templateId\": 1,\"sender\":\"bharat@everwell.org\",\"attachmentDto\":{\"attachmentContent\":\"Hello\",\"attachmentName\":\"abc\"},\"recipientList\": [{\"recipient\": \"bharat@everwell.org\"},{\"recipient\":\"bharat.agl3@gmail.com\"}]}}";
        Message message = new Message(jsonPayload.getBytes(), new MessageProperties());
        eventServiceConsumerService.processEmailConsumer(message);
    }

    @Test
    public void processSmsReconciliation_Success() throws IOException {
        String jsonPayload = "{\"eventName\":\"process_sms_reconciliation\",\"field\":{\"messageId\":\"1\",\"status\":\"success\",\"to\":\"7123123123\",\"vendorIds\":[1,2],\"retry\":false}}";
        Message message = new Message(jsonPayload.getBytes(), new MessageProperties());
        when(reconciliationService.saveStatus(any())).thenReturn(true);
        eventServiceConsumerService.processSmsReconciliationConsumer(message);
        verify(reconciliationService,Mockito.times(1)).saveStatus(any());
    }
    @Test(expected = ValidationException.class)
    public void processSmsReconciliation_NoVendorId() throws IOException {
        String jsonPayload = "{\"eventName\":\"process_sms_reconciliation\",\"field\":{\"messageId\":\"1\",\"status\":\"success\",\"to\":\"7123123123\",\"retry\":false}}";
        Message message = new Message(jsonPayload.getBytes(), new MessageProperties());
        eventServiceConsumerService.processSmsReconciliationConsumer(message);
    }

    @Test(expected = ValidationException.class)
    public void processSmsReconciliation_NoMessageId() throws IOException {
        String jsonPayload = "{\"eventName\":\"process_sms_reconciliation\",\"field\":{\"status\":\"success\",\"to\":\"7123123123\",\"vendorIds\":[1,2],\"retry\":false}}";
        Message message = new Message(jsonPayload.getBytes(), new MessageProperties());
        eventServiceConsumerService.processSmsReconciliationConsumer(message);
    }

    @Test(expected = ValidationException.class)
    public void processSmsReconciliation_NoStatus() throws IOException {
        String jsonPayload = "{\"eventName\":\"process_sms_reconciliation\",\"field\":{\"messageId\":\"1\",\"to\":\"7123123123\",\"vendorIds\":[1,2],\"retry\":false}}";
        Message message = new Message(jsonPayload.getBytes(), new MessageProperties());
        eventServiceConsumerService.processSmsReconciliationConsumer(message);
    }

    @Test(expected = ValidationException.class)
    public void processSmsReconciliation_NoToId() throws IOException {
        String jsonPayload = "{\"eventName\":\"process_sms_reconciliation\",\"field\":{\"messageId\":\"1\",\"status\":\"success\",\"vendorIds\":[1,2],\"retry\":false}}";
        Message message = new Message(jsonPayload.getBytes(), new MessageProperties());
        eventServiceConsumerService.processSmsReconciliationConsumer(message);
    }

}
