package tests.services;


import com.everwell.ins.enums.SmsGateway;
import com.everwell.ins.exceptions.NotFoundException;
import com.everwell.ins.exceptions.ValidationException;
import com.everwell.ins.models.db.Vendor;
import com.everwell.ins.models.dto.vendorConfigs.TextLocalConfigDto;
import com.everwell.ins.models.dto.vendorCreds.ApiKeySenderDto;
import com.everwell.ins.models.dto.vendorCreds.JSONFormatDto;
import com.everwell.ins.repositories.VendorRepository;
import com.everwell.ins.services.impl.VendorServiceImpl;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.Spy;
import tests.BaseTest;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;


public class VendorServiceTest extends BaseTest {

    @Spy
    @InjectMocks
    private VendorServiceImpl vendorService;

    @Mock
    private VendorRepository vendorRepository;

    @Test
    public void testGetVendorIds() {
        List<Long> vendorIds = new ArrayList<>();
        vendorIds.add(1L);
        String smsGateway = "test";

        when(vendorRepository.getVendorIdByGateway(any())).thenReturn(vendorIds);

        List<Long> response = vendorService.getVendorIds(smsGateway);
        assertEquals(vendorIds, response);
        verify(vendorRepository, Mockito.times(1)).getVendorIdByGateway(any());
    }

    @Test
    public void testGetVendors() {
        List<Vendor> vendors = new ArrayList<>();
        vendors.add(new Vendor(1L,"testGateway","testCredtype","testconfig","testcreds"));
        vendors.add(new Vendor(2L,"testGateway","testCredtype","testconfig","testcreds"));
        String PNGateway = "testGateway";

        when(vendorRepository.getVendorsByGateway(any())).thenReturn(vendors);

        List<Vendor> response = vendorService.getVendors(PNGateway);
        assertEquals(vendors, response);
        verify(vendorRepository, Mockito.times(1)).getVendorsByGateway(any());
    }

    @Test(expected = ValidationException.class)
    public void testGetVendorCredentials() {
        Long vendorId = 5L;
        String text = "test";
        Vendor vendor = new Vendor(vendorId, SmsGateway.TEXTLOCAL.toString(), text, text, text);

        when(vendorRepository.findById(any())).thenReturn(Optional.of(vendor));

        vendorService.getVendorCredentials(vendorId, ApiKeySenderDto.class);
    }

    @Test(expected = ValidationException.class)
    public void testGetVendorProfileWithType() {
        String creds = "test";

        vendorService.getVendorProfileWithType(creds, JSONFormatDto.class);
    }

    @Test(expected = NotFoundException.class)
    public void testGetVendorCredentialsNotFound() {
        Long vendorId = 5L;

        when(vendorRepository.findById(any())).thenReturn(Optional.empty());

        vendorService.getVendorCredentials(vendorId, ApiKeySenderDto.class);
    }

    @Test(expected = ValidationException.class)
    public void testGetVendorConfig() {
        Long vendorId = 5L;
        String text = "test";
        Vendor vendor = new Vendor(vendorId, SmsGateway.TEXTLOCAL.toString(), text, text, text);

        when(vendorRepository.findById(any())).thenReturn(Optional.of(vendor));

        vendorService.getVendorConfig(vendorId, TextLocalConfigDto.class);
    }

    @Test(expected = NotFoundException.class)
    public void testGetVendorConfigNotFound() {
        Long vendorId = 5L;

        when(vendorRepository.findById(any())).thenReturn(Optional.empty());

        vendorService.getVendorConfig(vendorId, TextLocalConfigDto.class);
    }


}
