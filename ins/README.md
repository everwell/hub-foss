# Integrated Notification Service

Integrated Notification Service(referred to as ins) is created to integrate all kinds of communications from hub and nikshay platform. It is responsible for all external notifications to patients.

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. See deployment for notes on how to deploy the project on a live system.

### Prerequisites

Things to be installed to run INS

* Java 11
* IntelliJ IDEA / Eclipse or any editor capable of running Java
* Install Maven
* Add Lombok Plugin

For running pure development environment

* Install Postgres
* Install Rabbitmq

While running the project for the first time, go to application.properties and make spring.flyway.enabled = false. It tries to make migrations on tables which are not created yet. 
Running it as false the first time will allow the creation of tables in local db. After which making spring.flyway.enabled = true will allow the migrations to be performed successfully.
											
### Installing and Deployment

A step by step series of examples that tell you how to get a development env running

Easy deployment shell script files have been created for easy startup options
The options for env are among dev, beta and prod.
```
sh deployment/<env>/deploy.sh
```
The deployment script builds a new package of the java project and runs the unit tests.
Once the deployment is done, we can look at the log to ascertain the application has started
```
2022-10-25 19:23:54,217 [main:]          org.apache.juli.logging.DirectJDKLog.log:173 INFO  - Starting ProtocolHandler ["http-nio-9100"]
2022-10-25 19:23:54,360 [main:]          o.s.b.w.e.tomcat.TomcatWebServer.start:203 INFO  - Tomcat started on port(s): 9100 (http) with context path ''
2022-10-25 19:23:55,517 [main:]          o.s.boot.StartupInfoLogger.logStarted:61 INFO  - Started InsApplication in 29.737 seconds (JVM running for 31.759)
```

### Building, Compilation and Packaging

For individual compiling of the project, we run the maven commands to build and compile

```
mvn clean compile
```

For packaging of the project in an executable format.
This would execute all the compilation build and test commands before packaging
```
mvn clean compile package
```

For individual running of the project, we run the maven commands

```
mvn spring-boot:run
```

## Running the tests

INS uses JUnit to run tests

Run the following command to evaluate the entire test suite
```
mvn test
```

Run the following command to evaluate individual unit tests
```
mvn test -Dtest=<testfilename>
```

## Built With

* [Springboot](https://spring.io/projects/spring-boot) - The Java framework used
* [Maven](https://maven.apache.org/) - Dependency Management
* [Mockito](https://github.com/mockito/mockito) - Used to generate mocks for testing independently
* [PowerMock](https://github.com/powermock/powermock) - Used to mock static, final and other classes that are not supported by mockito.

## License

This project is licensed under the MIT License