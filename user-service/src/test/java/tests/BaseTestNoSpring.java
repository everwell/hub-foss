package tests;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.junit.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class BaseTestNoSpring {
    @Test
    public void BaseTest(){} //without this gives error - no tests in class
}
