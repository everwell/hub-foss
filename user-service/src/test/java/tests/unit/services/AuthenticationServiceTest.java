package tests.unit.services;

import com.everwell.userservice.auth.JwtToken;
import com.everwell.userservice.models.db.Client;
import com.everwell.userservice.services.AuthenticationService;
import com.everwell.userservice.utils.Utils;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import tests.BaseTest;

public class AuthenticationServiceTest extends BaseTest {

    @Autowired
    AuthenticationService authenticationService;

    @Autowired
    JwtToken jwtTokenUtil;

    @Test
    public void test_generate_token_and_validate() {
        Client client = new Client(1L, "Test_1", "Test_1", Utils.getCurrentDate());
        String token = authenticationService.generateToken(client);
        Boolean valid = authenticationService.isValidToken(token, client);
        Assert.assertTrue(valid);
    }
}
