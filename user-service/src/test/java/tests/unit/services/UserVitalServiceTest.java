package tests.unit.services;

import com.everwell.userservice.exceptions.NotFoundException;
import com.everwell.userservice.exceptions.ValidationException;
import com.everwell.userservice.models.db.UserVitalGoal;
import com.everwell.userservice.models.db.Vital;
import com.everwell.userservice.models.dtos.vitals.*;
import com.everwell.userservice.models.http.requests.AddVitalDetailsRequest;
import com.everwell.userservice.models.http.requests.VitalDetailsRequest;
import com.everwell.userservice.models.http.requests.VitalGoalRequest;
import com.everwell.userservice.models.http.responses.VitalResponse;
import com.everwell.userservice.repositories.UserVitalRepository;
import com.everwell.userservice.repositories.VitalAggregateRepository;
import com.everwell.userservice.repositories.VitalAssociatedLogsRepository;
import com.everwell.userservice.repositories.VitalLogsRepository;
import com.everwell.userservice.services.VitalService;
import com.everwell.userservice.services.impl.UserVitalServiceImpl;
import org.junit.Assert;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.Spy;
import tests.BaseTest;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;
import static org.mockito.Mockito.*;

public class UserVitalServiceTest extends BaseTest {

    @Spy
    @InjectMocks
    private UserVitalServiceImpl userVitalService;

    @Mock
    private UserVitalRepository userVitalRepository;

    @Mock
    private VitalService vitalService;

    @Mock
    private VitalLogsRepository vitalLogsRepository;

    @Mock
    private VitalAssociatedLogsRepository vitalAssociatedLogsRepository;

    @Mock
    private VitalAggregateRepository vitalAggregateRepository;

    private List<Vital> getVitalList() {
        List<Vital> vitals = new ArrayList<>();
        vitals.add(new Vital(1L, "water", 10f, 10000f, 1f, 10000f, "uomTest", false));
        vitals.add(new Vital(2L, "calorie", 10f, 10000f, 1f, 10000f, "uomTest", true));
        return vitals;
    }

    private List<VitalResponse> getVitalResponseList() {
        List<VitalResponse> vitalResponse = new ArrayList<>();
        vitalResponse.add(new VitalResponse(1L, "vital name", 10f, 10000f, 1f, 10000f, "uomTest", false));
        return vitalResponse;
    }

    private List<VitalsAggregatedTimeData> getVitalsAggregatedTimeData() {
        List<VitalsAggregatedTimeData> vitalsAggregatedTimeDataList = new ArrayList<>();
        vitalsAggregatedTimeDataList.add(new VitalsAggregatedTimeData("2022-11-29 00:00:00", 1L, 1L, 100f, 10f));
        return vitalsAggregatedTimeDataList;
    }

    private List<VitalsTimeData> getVitalsTimeDataForVitalOne() {
        List<VitalsTimeData> vitalsTimeDataList = new ArrayList<>();
        vitalsTimeDataList.add(new VitalsTimeData("2022-11-29 00:00:00", 1L, 1L, "100"));
        return vitalsTimeDataList;
    }

    private List<VitalsTimeData> getVitalsTimeDataForVitalTwo() {
        List<VitalsTimeData> vitalsTimeDataList = new ArrayList<>();
        vitalsTimeDataList.add(new VitalsTimeData("2022-11-29 00:00:00", 1L, 2L, "{food:pizza}"));
        return vitalsTimeDataList;
    }

    @Test
    public void testGetVitalsGoalByUserId() {
        List<UserVitalGoal> userVitalGoalList = new ArrayList<>();
        userVitalGoalList.add(new UserVitalGoal(1L, 1L, 1L, 10f, LocalDateTime.now(),null));
        List<VitalResponse> vitalResponseList = getVitalResponseList();
        when(userVitalRepository.findAllByUserId(eq(1L))).thenReturn(userVitalGoalList);
        when(vitalService.getAllVitalsResponse()).thenReturn(vitalResponseList);

        List<VitalGoalDto> vitalGoalDtoList = userVitalService.getVitalsGoalByUserId(1L);

        Assert.assertEquals(userVitalGoalList.size(), vitalGoalDtoList.size());
        Assert.assertEquals(vitalResponseList.get(0).getName(), vitalGoalDtoList.get(0).getVitalName());
        Mockito.verify(userVitalRepository, Mockito.times(1)).findAllByUserId(eq(1L));
        Mockito.verify(vitalService, Mockito.times(1)).getAllVitalsResponse();
    }

    @Test
    public void testGetVitalsGoalsForUserIdListAndVitalIdList() {
        List<UserVitalGoal> userVitalGoalList = new ArrayList<>();
        userVitalGoalList.add(new UserVitalGoal(1L, 1L, 1L, 10f, LocalDateTime.now(),null));
        List<Long> userIdList = Collections.singletonList(1L);
        List<Long> vitalIdList = Collections.singletonList(1L);
        when(userVitalRepository.findByUserIdInAndVitalIdIn(any(), any())).thenReturn(userVitalGoalList);

        List<UserVitalGoalDto> userVitalGoalDtoList = userVitalService.getVitalsGoalsForUserIdListAndVitalIdList(userIdList, vitalIdList);

        Assert.assertEquals(userVitalGoalList.size(), userVitalGoalDtoList.size());
        Assert.assertEquals(userVitalGoalList.get(0).getUserId(), userVitalGoalDtoList.get(0).getUserId());
        Assert.assertEquals(userVitalGoalDtoList.get(0).getVitalId(), userVitalGoalDtoList.get(0).getVitalId());
        Mockito.verify(userVitalRepository, Mockito.times(1)).findByUserIdInAndVitalIdIn(any(), any());
    }

    @Test
    public void testSaveUserVitalsGoalForValidVitalId() {
        VitalGoalRequest vitalGoalRequest = new VitalGoalRequest(1L, null, 10f);
        when(vitalService.processAndFetchVitalId(eq(1L), eq(null))).thenReturn(1L);
        when(userVitalRepository.findByUserIdAndVitalId(eq(1L), eq(1L))).thenReturn(null);

        userVitalService.saveUserVitalsGoal(1L, vitalGoalRequest);

        Mockito.verify(vitalService, Mockito.times(1)).processAndFetchVitalId(any(), any());
        Mockito.verify(userVitalRepository, Mockito.times(1)).save(any());
        Mockito.verify(userVitalRepository, Mockito.times(1)).findByUserIdAndVitalId(any(), any());
    }


    @Test(expected = ValidationException.class)
    public void testSaveUserVitalsGoalForValidForExistingUserVital() {
        VitalGoalRequest vitalGoalRequest = new VitalGoalRequest(1L, null, 10f);
        List<UserVitalGoal> userVitalGoalList = new ArrayList<>();
        userVitalGoalList.add(new UserVitalGoal(1L, 1L, 1L, 10f,  LocalDateTime.now(), null));
        when(vitalService.processAndFetchVitalId(eq(1L), eq(null))).thenReturn(1L);
        when(userVitalRepository.findByUserIdAndVitalId(eq(1L), eq(1L))).thenReturn(userVitalGoalList);

        userVitalService.saveUserVitalsGoal(1L, vitalGoalRequest);

        Mockito.verify(vitalService, Mockito.times(1)).processAndFetchVitalId(any(), any());
        Mockito.verify(userVitalRepository, Mockito.times(0)).save(any());
        Mockito.verify(userVitalRepository, Mockito.times(1)).findByUserIdAndVitalId(any(), any());
    }


    @Test
    public void testUpdateUserVitalsGoalForNonExistentEntry() {
        VitalGoalRequest vitalGoalRequest = new VitalGoalRequest(1L, "vital name", 10f);
        when(vitalService.processAndFetchVitalId(eq(1L), eq("vital name"))).thenReturn(1L);
        when(userVitalRepository.findByUserIdAndVitalId(eq(1L), eq(1L))).thenReturn(new ArrayList<>());

        userVitalService.updateUserVitalsGoal(1L, vitalGoalRequest);

        Mockito.verify(vitalService, Mockito.times(1)).processAndFetchVitalId(any(), any());
        Mockito.verify(userVitalRepository, Mockito.times(1)).save(any());
    }

    @Test
    public void testUpdateUserVitalsGoal() {
        VitalGoalRequest vitalGoalRequest = new VitalGoalRequest(1L, "vital name", 10f);
        List<UserVitalGoal> userVitalGoalList = new ArrayList<>();
        userVitalGoalList.add(new UserVitalGoal(1L, 1L, 1L, 10f,  LocalDateTime.now(), LocalDateTime.now()));
        userVitalGoalList.add(new UserVitalGoal(2L, 1L, 1L, 10f,  LocalDateTime.now(), null));
        when(vitalService.processAndFetchVitalId(eq(1L), eq("vital name"))).thenReturn(1L);
        when(userVitalRepository.findByUserIdAndVitalId(eq(1L), eq(1L))).thenReturn(userVitalGoalList);

        userVitalService.updateUserVitalsGoal(1L, vitalGoalRequest);

        Mockito.verify(vitalService, Mockito.times(1)).processAndFetchVitalId(any(), any());
        Mockito.verify(userVitalRepository, Mockito.times(1)).saveAll(any());
    }

    @Test
    public void testUpdateUserVitalsGoalWithNullValue() {
        VitalGoalRequest vitalGoalRequest = new VitalGoalRequest(1L, "vital name", null);
        List<UserVitalGoal> userVitalGoalList = new ArrayList<>();
        userVitalGoalList.add(new UserVitalGoal(1L, 1L, 1L, 10f,  LocalDateTime.now(), null));
        when(vitalService.processAndFetchVitalId(eq(1L), eq("vital name"))).thenReturn(1L);
        when(userVitalRepository.findByUserIdAndVitalId(eq(1L), eq(1L))).thenReturn(userVitalGoalList);

        userVitalService.updateUserVitalsGoal(1L, vitalGoalRequest);

        Mockito.verify(vitalService, Mockito.times(1)).processAndFetchVitalId(any(), any());
        Mockito.verify(userVitalRepository, Mockito.times(0)).saveAll(any());
    }

    @Test
    public void testDeleteUserVitalsGoal() {
        userVitalService.deleteUserVitalsGoal(1L);
        Mockito.verify(userVitalRepository, Mockito.times(1)).deleteAllByUserId(any());
    }

    @Test(expected  = Exception.class)
    public void testGetUserVitalsDetailsWithInvalidStartDateTime() {
        VitalDetailsRequest vitalDetailsRequest = new VitalDetailsRequest(Collections.singletonList(1L), Collections.singletonList(1L), null, false, false, false, false, "hi", "");
        userVitalService.getUserVitalsDetails(vitalDetailsRequest);

        Mockito.verify(vitalService, Mockito.times(0)).getAllVitals();
        Mockito.verify(vitalAggregateRepository, Mockito.times(0)).fetchData(any(), any(), any(), any());
        Mockito.verify(vitalLogsRepository, Mockito.times(0)).fetchLogs(any(), any(), any(), any());
        Mockito.verify(vitalAssociatedLogsRepository, Mockito.times(0)).fetchLogs(any(), any(), any(), any());
    }

    @Test(expected  = Exception.class)
    public void testGetUserVitalsDetailsWithInvalidEndDateTime() {
        VitalDetailsRequest vitalDetailsRequest = new VitalDetailsRequest(Collections.singletonList(1L), Collections.singletonList(1L), null, false, false, false, false, "2022-11-29 00:00:00", "");
        userVitalService.getUserVitalsDetails(vitalDetailsRequest);

        Mockito.verify(vitalService, Mockito.times(0)).getAllVitals();
        Mockito.verify(vitalAggregateRepository, Mockito.times(0)).fetchData(any(), any(), any(), any());
        Mockito.verify(vitalLogsRepository, Mockito.times(0)).fetchLogs(any(), any(), any(), any());
        Mockito.verify(vitalAssociatedLogsRepository, Mockito.times(0)).fetchLogs(any(), any(), any(), any());
    }

    @Test(expected = ValidationException.class)
    public void testGetUserVitalsDetailsWithEndDateLessThanStartDate() {
        VitalDetailsRequest vitalDetailsRequest = new VitalDetailsRequest(Collections.singletonList(1L), Collections.singletonList(1L), null, false, false, false, false, "2022-11-30 00:00:00", "2022-11-29 00:00:00");
        userVitalService.getUserVitalsDetails(vitalDetailsRequest);

        Mockito.verify(vitalService, Mockito.times(0)).getAllVitals();
        Mockito.verify(vitalAggregateRepository, Mockito.times(0)).fetchData(any(), any(), any(), any());
        Mockito.verify(vitalLogsRepository, Mockito.times(0)).fetchLogs(any(), any(), any(), any());
        Mockito.verify(vitalAssociatedLogsRepository, Mockito.times(0)).fetchLogs(any(), any(), any(), any());
    }

    @Test(expected = NotFoundException.class)
    public void testGetUserVitalsDetailsWithAggregatedDataAndInvalidVitalIdData() {
        VitalDetailsRequest vitalDetailsRequest = new VitalDetailsRequest(Collections.singletonList(1L), Collections.singletonList(3L), null, false, true, false, false, "2022-11-28 00:00:00", "2022-11-30 00:00:00");
        List<Vital> vitals = getVitalList();
        when(vitalService.getAllVitals()).thenReturn(vitals);

        userVitalService.getUserVitalsDetails(vitalDetailsRequest);

        Mockito.verify(vitalService, Mockito.times(1)).getAllVitals();
        Mockito.verify(vitalAggregateRepository, Mockito.times(0)).fetchData(any(), any(), any(), any());
        Mockito.verify(vitalLogsRepository, Mockito.times(0)).fetchLogs(any(), any(), any(), any());
        Mockito.verify(vitalAssociatedLogsRepository, Mockito.times(0)).fetchLogs(any(), any(), any(), any());
    }

    @Test(expected = NotFoundException.class)
    public void testGetUserVitalsDetailsWithAggregatedDataAndInvalidVitalNameData() {
        VitalDetailsRequest vitalDetailsRequest = new VitalDetailsRequest(Collections.singletonList(1L), null, Collections.singletonList("vital name"), false, true, false, false, "2022-11-28 00:00:00", "2022-11-30 00:00:00");
        List<Vital> vitals = getVitalList();
        when(vitalService.getAllVitals()).thenReturn(vitals);

        userVitalService.getUserVitalsDetails(vitalDetailsRequest);

        Mockito.verify(vitalService, Mockito.times(1)).getAllVitals();
        Mockito.verify(vitalAggregateRepository, Mockito.times(0)).fetchData(any(), any(), any(), any());
        Mockito.verify(vitalLogsRepository, Mockito.times(0)).fetchLogs(any(), any(), any(), any());
        Mockito.verify(vitalAssociatedLogsRepository, Mockito.times(0)).fetchLogs(any(), any(), any(), any());
    }

    @Test(expected = ValidationException.class)
    public void testGetUserVitalsDetailsWithAggregatedDataForInvalidVitalIdAndVitalNameData() {
        VitalDetailsRequest vitalDetailsRequest = new VitalDetailsRequest(Collections.singletonList(1L),null, null, false, true, false, false, "2022-11-28 00:00:00", "2022-11-30 00:00:00");
        List<Vital> vitals = getVitalList();
        when(vitalService.getAllVitals()).thenReturn(vitals);

        userVitalService.getUserVitalsDetails(vitalDetailsRequest);

        Mockito.verify(vitalService, Mockito.times(1)).getAllVitals();
        Mockito.verify(vitalAggregateRepository, Mockito.times(0)).fetchData(any(), any(), any(), any());
        Mockito.verify(vitalLogsRepository, Mockito.times(0)).fetchLogs(any(), any(), any(), any());
        Mockito.verify(vitalAssociatedLogsRepository, Mockito.times(0)).fetchLogs(any(), any(), any(), any());
    }

    @Test
    public void testGetUserVitalsDetailsWithAggregatedDataForValidVitalIdData() {
        VitalDetailsRequest vitalDetailsRequest = new VitalDetailsRequest(Collections.singletonList(1L), Collections.singletonList(1L), null, false, true, false, false, "2022-11-28 00:00:00", "2022-11-30 00:00:00");
        List<VitalsAggregatedTimeData> vitalsAggregatedTimeDataList = getVitalsAggregatedTimeData();
        List<Vital> vitals = getVitalList();
        when(vitalService.getAllVitals()).thenReturn(vitals);
        when(vitalAggregateRepository.fetchData(any(), any(), any(), any())).thenReturn(vitalsAggregatedTimeDataList);

        VitalDetailsDto vitalDetailsDto = userVitalService.getUserVitalsDetails(vitalDetailsRequest);

        Assert.assertEquals(0, vitalDetailsDto.getUsersVitalGoalData().size());
        Assert.assertEquals(0, vitalDetailsDto.getLogs().size());
        Assert.assertEquals(1L, vitalDetailsDto.getAggregatedData().get(0).getVitalId().longValue());
        Assert.assertEquals("100.0", vitalDetailsDto.getAggregatedData().get(0).getVitalData().get(0).getTotal().toString());
        Assert.assertEquals("10.0", vitalDetailsDto.getAggregatedData().get(0).getVitalData().get(0).getAverage().toString());
        Mockito.verify(vitalService, Mockito.times(1)).getAllVitals();
        Mockito.verify(vitalAggregateRepository, Mockito.times(1)).fetchData(any(), any(), any(), any());
        Mockito.verify(vitalLogsRepository, Mockito.times(0)).fetchLogs(any(), any(), any(), any());
        Mockito.verify(vitalAssociatedLogsRepository, Mockito.times(0)).fetchLogs(any(), any(), any(), any());
    }

    @Test
    public void testGetUserVitalsDetailsWithAggregatedDataForValidVitalNameData() {
        VitalDetailsRequest vitalDetailsRequest = new VitalDetailsRequest(Collections.singletonList(1L), null, Collections.singletonList("water"), false, true, false, false, "2022-11-28 00:00:00", "2022-11-30 00:00:00");
        List<Vital> vitals = getVitalList();
        when(vitalService.getAllVitals()).thenReturn(vitals);
        when(vitalAggregateRepository.fetchData(any(), any(), any(), any())).thenReturn(getVitalsAggregatedTimeData());

        VitalDetailsDto vitalDetailsDto = userVitalService.getUserVitalsDetails(vitalDetailsRequest);

        Assert.assertEquals(0, vitalDetailsDto.getUsersVitalGoalData().size());
        Assert.assertEquals(0, vitalDetailsDto.getLogs().size());
        Assert.assertEquals(1L, vitalDetailsDto.getAggregatedData().get(0).getVitalId().longValue());
        Assert.assertEquals("100.0", vitalDetailsDto.getAggregatedData().get(0).getVitalData().get(0).getTotal().toString());
        Assert.assertEquals("water", vitalDetailsDto.getAggregatedData().get(0).getVitalName());
        Assert.assertEquals("10.0", vitalDetailsDto.getAggregatedData().get(0).getVitalData().get(0).getAverage().toString());
        Mockito.verify(vitalService, Mockito.times(1)).getAllVitals();
        Mockito.verify(vitalAggregateRepository, Mockito.times(1)).fetchData(any(), any(), any(), any());
        Mockito.verify(vitalLogsRepository, Mockito.times(0)).fetchLogs(any(), any(), any(), any());
        Mockito.verify(vitalAssociatedLogsRepository, Mockito.times(0)).fetchLogs(any(), any(), any(), any());
    }

    @Test
    public void testGetUserVitalsDetailsWithAggregatedDataForAssociatedValidVitalIdData() {
        VitalDetailsRequest vitalDetailsRequest = new VitalDetailsRequest(Collections.singletonList(1L), Collections.singletonList(2L), null, false, true, false, false, "2022-11-28 00:00:00", "2022-11-30 00:00:00");
        List<Vital> vitals = getVitalList();
        when(vitalService.getAllVitals()).thenReturn(vitals);

        VitalDetailsDto vitalDetailsDto = userVitalService.getUserVitalsDetails(vitalDetailsRequest);

        Assert.assertEquals(0, vitalDetailsDto.getUsersVitalGoalData().size());
        Assert.assertEquals(0, vitalDetailsDto.getLogs().size());
        Assert.assertEquals(0, vitalDetailsDto.getAggregatedData().size());
        Mockito.verify(vitalService, Mockito.times(1)).getAllVitals();
        Mockito.verify(vitalAggregateRepository, Mockito.times(0)).fetchData(any(), any(), any(), any());
        Mockito.verify(vitalLogsRepository, Mockito.times(0)).fetchLogs(any(), any(), any(), any());
        Mockito.verify(vitalAssociatedLogsRepository, Mockito.times(0)).fetchLogs(any(), any(), any(), any());
    }

    @Test
    public void testGetUserVitalsDetailsWithOnlyUserRelationData() {
        VitalDetailsRequest vitalDetailsRequest = new VitalDetailsRequest(Collections.singletonList(1L), Collections.singletonList(1L), null, false, false, false, true, "2022-11-28 00:00:00", "2022-11-30 00:00:00");
        List<Vital> vitals = getVitalList();
        List<UserVitalGoalDto> userVitalGoalDtoList = new ArrayList<>();
        userVitalGoalDtoList.add(new UserVitalGoalDto( 1L, 1L, 10f, LocalDateTime.now(),null));
        when(vitalService.getAllVitals()).thenReturn(vitals);
        doReturn(userVitalGoalDtoList).when(userVitalService).getVitalsGoalsForUserIdListAndVitalIdList(any(), any());

        VitalDetailsDto vitalDetailsDto = userVitalService.getUserVitalsDetails(vitalDetailsRequest);

        Assert.assertEquals(1, vitalDetailsDto.getUsersVitalGoalData().size());
        Assert.assertEquals(10L, vitalDetailsDto.getUsersVitalGoalData().get(0).getValue().longValue());
        Assert.assertEquals(0, vitalDetailsDto.getLogs().size());
        Assert.assertEquals(0, vitalDetailsDto.getAggregatedData().size());
        Mockito.verify(vitalService, Mockito.times(1)).getAllVitals();
        Mockito.verify(userVitalService, Mockito.times(1)).getVitalsGoalsForUserIdListAndVitalIdList(any(), any());
        Mockito.verify(vitalAggregateRepository, Mockito.times(0)).fetchData(any(), any(), any(), any());
        Mockito.verify(vitalLogsRepository, Mockito.times(0)).fetchLogs(any(), any(), any(), any());
        Mockito.verify(vitalAssociatedLogsRepository, Mockito.times(0)).fetchLogs(any(), any(), any(), any());
    }

    @Test
    public void testGetUserVitalsDetailsWithLogsDataForValidVitalIdData() {
        VitalDetailsRequest vitalDetailsRequest = new VitalDetailsRequest(Collections.singletonList(1L), Collections.singletonList(1L), null, true, false, false, false, "2022-11-28 00:00:00", "2022-11-30 00:00:00");
        List<Vital> vitals = getVitalList();
        when(vitalService.getAllVitals()).thenReturn(vitals);
        when(vitalLogsRepository.fetchLogs(any(), any(), any(), any())).thenReturn(getVitalsTimeDataForVitalOne());

        VitalDetailsDto vitalDetailsDto = userVitalService.getUserVitalsDetails(vitalDetailsRequest);

        Assert.assertEquals(0, vitalDetailsDto.getUsersVitalGoalData().size());
        Assert.assertEquals(1, vitalDetailsDto.getLogs().size());
        Assert.assertEquals(0, vitalDetailsDto.getAggregatedData().size());
        Assert.assertEquals(1L, vitalDetailsDto.getLogs().get(0).getVitalId().longValue());
        Assert.assertEquals("100", vitalDetailsDto.getLogs().get(0).getVitalData().get(0).getValue());
        Mockito.verify(vitalService, Mockito.times(1)).getAllVitals();
        Mockito.verify(userVitalService, Mockito.times(0)).getVitalsGoalsForUserIdListAndVitalIdList(any(), any());
        Mockito.verify(vitalAggregateRepository, Mockito.times(0)).fetchData(any(), any(), any(), any());
        Mockito.verify(vitalLogsRepository, Mockito.times(1)).fetchLogs(any(), any(), any(), any());
        Mockito.verify(vitalAssociatedLogsRepository, Mockito.times(0)).fetchLogs(any(), any(), any(), any());
    }

    @Test
    public void testGetUserVitalsDetailsWithLogsDataForValidVitalNameData() {
        VitalDetailsRequest vitalDetailsRequest = new VitalDetailsRequest(Collections.singletonList(1L), null, Collections.singletonList("water"), true, false, false, false, "2022-11-28 00:00:00", "2022-11-30 00:00:00");
        List<Vital> vitals = getVitalList();
        when(vitalService.getAllVitals()).thenReturn(vitals);
        when(vitalLogsRepository.fetchLogs(any(), any(), any(), any())).thenReturn(getVitalsTimeDataForVitalOne());

        VitalDetailsDto vitalDetailsDto = userVitalService.getUserVitalsDetails(vitalDetailsRequest);

        Assert.assertEquals(0, vitalDetailsDto.getUsersVitalGoalData().size());
        Assert.assertEquals(1, vitalDetailsDto.getLogs().size());
        Assert.assertEquals(0, vitalDetailsDto.getAggregatedData().size());
        Assert.assertEquals(1L, vitalDetailsDto.getLogs().get(0).getVitalId().longValue());
        Assert.assertEquals("water", vitalDetailsDto.getLogs().get(0).getVitalName());
        Assert.assertEquals("100", vitalDetailsDto.getLogs().get(0).getVitalData().get(0).getValue());
        Mockito.verify(vitalService, Mockito.times(1)).getAllVitals();
        Mockito.verify(userVitalService, Mockito.times(0)).getVitalsGoalsForUserIdListAndVitalIdList(any(), any());
        Mockito.verify(vitalAggregateRepository, Mockito.times(0)).fetchData(any(), any(), any(), any());
        Mockito.verify(vitalLogsRepository, Mockito.times(1)).fetchLogs(any(), any(), any(), any());
        Mockito.verify(vitalAssociatedLogsRepository, Mockito.times(0)).fetchLogs(any(), any(), any(), any());
    }

    @Test
    public void testGetUserVitalsDetailsWithLogsDataForSingleValueVitalNameData() {
        VitalDetailsRequest vitalDetailsRequest = new VitalDetailsRequest(Collections.singletonList(1L), null, Collections.singletonList("calorie"), true, false, false, false, "2022-11-28 00:00:00", "2022-11-30 00:00:00");
        List<Vital> vitals = getVitalList();
        when(vitalService.getAllVitals()).thenReturn(vitals);
        when(vitalLogsRepository.fetchLogs(any(), any(), any(), any())).thenReturn(getVitalsTimeDataForVitalOne());
        when(vitalAssociatedLogsRepository.fetchLogs(any(), any(), any(), any())).thenReturn(getVitalsTimeDataForVitalTwo());

        VitalDetailsDto vitalDetailsDto = userVitalService.getUserVitalsDetails(vitalDetailsRequest);

        Assert.assertEquals(0, vitalDetailsDto.getUsersVitalGoalData().size());
        Assert.assertEquals(1, vitalDetailsDto.getLogs().size());
        Assert.assertEquals(0, vitalDetailsDto.getAggregatedData().size());
        Assert.assertEquals(2L, vitalDetailsDto.getLogs().get(0).getVitalId().longValue());
        Assert.assertEquals("calorie", vitalDetailsDto.getLogs().get(0).getVitalName());
        Assert.assertEquals("{food:pizza}", vitalDetailsDto.getLogs().get(0).getVitalData().get(0).getValue());
        Mockito.verify(vitalService, Mockito.times(1)).getAllVitals();
        Mockito.verify(userVitalService, Mockito.times(0)).getVitalsGoalsForUserIdListAndVitalIdList(any(), any());
        Mockito.verify(vitalAggregateRepository, Mockito.times(0)).fetchData(any(), any(), any(), any());
        Mockito.verify(vitalLogsRepository, Mockito.times(0)).fetchLogs(any(), any(), any(), any());
        Mockito.verify(vitalAssociatedLogsRepository, Mockito.times(1)).fetchLogs(any(), any(), any(), any());
    }

    @Test
    public void testGetUserVitalsDetailsWithAllData() {
        List<String> vitalNameList = new ArrayList<>();
        vitalNameList.add("water");
        vitalNameList.add("calorie");
        VitalDetailsRequest vitalDetailsRequest = new VitalDetailsRequest(Collections.singletonList(1L), null, vitalNameList, true, true, false, true, "2022-11-28 00:00:00", "2022-11-30 00:00:00");
        List<Vital> vitals = getVitalList();
        List<UserVitalGoalDto> userVitalGoalDtoList = new ArrayList<>();
        userVitalGoalDtoList.add(new UserVitalGoalDto( 1L, 1L, 10f, LocalDateTime.now(),null));
        when(vitalService.getAllVitals()).thenReturn(vitals);
        doReturn(userVitalGoalDtoList).when(userVitalService).getVitalsGoalsForUserIdListAndVitalIdList(any(), any());
        when(vitalLogsRepository.fetchLogs(any(), any(), any(), any())).thenReturn(getVitalsTimeDataForVitalOne());
        when(vitalAssociatedLogsRepository.fetchLogs(any(), any(), any(), any())).thenReturn(getVitalsTimeDataForVitalTwo());
        when(vitalAggregateRepository.fetchData(any(), any(), any(), any())).thenReturn(getVitalsAggregatedTimeData());

        VitalDetailsDto vitalDetailsDto = userVitalService.getUserVitalsDetails(vitalDetailsRequest);

        Assert.assertEquals(1, vitalDetailsDto.getUsersVitalGoalData().size());
        Assert.assertEquals(2, vitalDetailsDto.getLogs().size());
        Assert.assertEquals(1, vitalDetailsDto.getAggregatedData().size());
        Assert.assertEquals(2L, vitalDetailsDto.getLogs().get(1).getVitalId().longValue());
        Assert.assertEquals("calorie", vitalDetailsDto.getLogs().get(1).getVitalName());
        Assert.assertEquals("{food:pizza}", vitalDetailsDto.getLogs().get(1).getVitalData().get(0).getValue());
        Assert.assertEquals(1L, vitalDetailsDto.getAggregatedData().get(0).getVitalId().longValue());
        Assert.assertEquals("100.0", vitalDetailsDto.getAggregatedData().get(0).getVitalData().get(0).getTotal().toString());
        Assert.assertEquals("10.0", vitalDetailsDto.getAggregatedData().get(0).getVitalData().get(0).getAverage().toString());
        Mockito.verify(vitalService, Mockito.times(1)).getAllVitals();
        Mockito.verify(userVitalService, Mockito.times(1)).getVitalsGoalsForUserIdListAndVitalIdList(any(), any());
        Mockito.verify(vitalAggregateRepository, Mockito.times(1)).fetchData(any(), any(), any(), any());
        Mockito.verify(vitalLogsRepository, Mockito.times(1)).fetchLogs(any(), any(), any(), any());
        Mockito.verify(vitalAssociatedLogsRepository, Mockito.times(1)).fetchLogs(any(), any(), any(), any());
    }

    @Test(expected = NotFoundException.class)
    public void testSaveUserVitalsDetailsWithInvalidVitalId() {
        List<VitalDto> vitalDtoList = new ArrayList<>();
        vitalDtoList.add(new VitalDto(2L, null, "100L"));
        AddVitalDetailsRequest addVitalDetailsRequest = new AddVitalDetailsRequest(vitalDtoList);
        List<Vital> vitals = new ArrayList<>();
        vitals.add(new Vital(1L, "water", 10f, 10000f, 1f, 10000f, "uomTest", false));
        when(vitalService.getAllVitals()).thenReturn(vitals);

        userVitalService.saveUserVitalsDetails(1L, addVitalDetailsRequest);

        Mockito.verify(vitalService, Mockito.times(1)).getAllVitals();
        Mockito.verify(vitalLogsRepository, Mockito.times(0)).saveAll(any());
        Mockito.verify(vitalAssociatedLogsRepository, Mockito.times(0)).saveAll(any());
    }

    @Test(expected = NotFoundException.class)
    public void testSaveUserVitalsDetailsWithInvalidVitalName() {
        List<VitalDto> vitalDtoList = new ArrayList<>();
        vitalDtoList.add(new VitalDto(null, "vital", "100L"));
        AddVitalDetailsRequest addVitalDetailsRequest = new AddVitalDetailsRequest(vitalDtoList);
        List<Vital> vitals = new ArrayList<>();
        vitals.add(new Vital(1L, "water", 10f, 10000f, 1f, 10000f, "uomTest", false));
        when(vitalService.getAllVitals()).thenReturn(vitals);

        userVitalService.saveUserVitalsDetails(1L, addVitalDetailsRequest);

        Mockito.verify(vitalService, Mockito.times(0)).getAllVitals();
        Mockito.verify(vitalLogsRepository, Mockito.times(0)).saveAll(any());
        Mockito.verify(vitalAssociatedLogsRepository, Mockito.times(0)).saveAll(any());
    }

    @Test
    public void testSaveUserVitalsDetailsWithOnlySingleValueVitalIdList() {
        List<VitalDto> vitalDtoList = new ArrayList<>();
        vitalDtoList.add(new VitalDto(1L, null, "100f"));
        AddVitalDetailsRequest addVitalDetailsRequest = new AddVitalDetailsRequest(vitalDtoList);
        List<Vital> vitals = new ArrayList<>();
        vitals.add(new Vital(1L, "water", 10f, 10000f, 1f, 10000f, "uomTest", false));
        when(vitalService.getAllVitals()).thenReturn(vitals);

        userVitalService.saveUserVitalsDetails(1L, addVitalDetailsRequest);

        Mockito.verify(vitalService, Mockito.times(1)).getAllVitals();
        Mockito.verify(vitalLogsRepository, Mockito.times(1)).saveAll(any());
        Mockito.verify(vitalAssociatedLogsRepository, Mockito.times(0)).saveAll(any());
    }

    @Test
    public void testSaveUserVitalsDetailsWithOnlyAssociatedValueVitalIdList() {
        List<VitalDto> vitalDtoList = new ArrayList<>();
        vitalDtoList.add(new VitalDto(1L, null, "{food : pizze}"));
        AddVitalDetailsRequest addVitalDetailsRequest = new AddVitalDetailsRequest(vitalDtoList);
        List<Vital> vitals = new ArrayList<>();
        vitals.add(new Vital(1L, "food calorie", 10f, 10000f, 1f, 10000f, "uomTest", true));
        when(vitalService.getAllVitals()).thenReturn(vitals);

        userVitalService.saveUserVitalsDetails(1L, addVitalDetailsRequest);

        Mockito.verify(vitalService, Mockito.times(1)).getAllVitals();
        Mockito.verify(vitalLogsRepository, Mockito.times(0)).saveAll(any());
        Mockito.verify(vitalAssociatedLogsRepository, Mockito.times(1)).saveAll(any());
    }

    @Test
    public void testSaveUserVitalsDetails() {
        List<VitalDto> vitalDtoList = new ArrayList<>();
        vitalDtoList.add(new VitalDto(1L, null, "100f"));
        vitalDtoList.add(new VitalDto(2L, null, "{food : pizza}"));
        AddVitalDetailsRequest addVitalDetailsRequest = new AddVitalDetailsRequest(vitalDtoList);
        List<Vital> vitals = new ArrayList<>();
        vitals.add(new Vital(1L, "water", 10f, 10000f, 1f, 10000f, "uomTest", false));
        vitals.add(new Vital(2L, "food calorie", 10f, 10000f, 1f, 10000f, "uomTest", true));
        when(vitalService.getAllVitals()).thenReturn(vitals);

        userVitalService.saveUserVitalsDetails(1L, addVitalDetailsRequest);

        Mockito.verify(vitalService, Mockito.times(1)).getAllVitals();
        Mockito.verify(vitalLogsRepository, Mockito.times(1)).saveAll(any());
        Mockito.verify(vitalAssociatedLogsRepository, Mockito.times(1)).saveAll(any());
    }
}
