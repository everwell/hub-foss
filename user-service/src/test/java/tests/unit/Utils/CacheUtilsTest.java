package tests.unit.Utils;

import com.everwell.userservice.utils.CacheUtils;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.ValueOperations;
import tests.BaseTest;

import java.util.concurrent.TimeUnit;

import static org.mockito.Mockito.*;
import static org.mockito.Mockito.verify;

public class CacheUtilsTest extends BaseTest
{
    CacheUtils cacheUtils;

    @Mock
    RedisTemplate<String, Object> redisTemplate;

    @Mock
    private ValueOperations<String, Object> valueOperations;

    @Before
    public void init() {
        cacheUtils = new CacheUtils(redisTemplate);
        when(redisTemplate.opsForValue()).thenReturn(valueOperations);
    }


    @Test
    public void getFromCache()
    {
        String cachedValue = "ABCDE";
        String key = "cacheKey";
        doReturn(cachedValue).when(valueOperations).get(key);

        String obtainedCacheValue = cacheUtils.getFromCache(key);
        Assert.assertEquals(cachedValue, obtainedCacheValue);
    }

    @Test
    public void getFromCache_NotExists()
    {
        String cachedValue = null;
        String key = "cacheKey";
        doReturn(null).when(valueOperations).get(key);

        String obtainedCacheValue = cacheUtils.getFromCache(key);
        Assert.assertEquals(cachedValue, obtainedCacheValue);
    }

    @Test
    public void putIntoCache()
    {
        String key = "cacheKey";
        String cachedValue = "ABCDE";
        doNothing().when(valueOperations).set(key, cachedValue, 1000L, TimeUnit.SECONDS);
        cacheUtils.putIntoCache(key, cachedValue, 1000L);
        verify(valueOperations, Mockito.times(1)).set(key, cachedValue, 1000L, TimeUnit.SECONDS);
    }
}
