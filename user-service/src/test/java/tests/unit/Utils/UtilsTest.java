package tests.unit.Utils;

import com.fasterxml.jackson.databind.util.ByteBufferBackedInputStream;
import org.junit.Test;

import com.everwell.userservice.models.db.UserMobile;
import com.everwell.userservice.models.dtos.UserEmailRequest;
import com.everwell.userservice.models.dtos.UserMobileRequest;
import com.everwell.userservice.utils.Utils;
import com.fasterxml.jackson.core.type.TypeReference;
import org.mockito.InjectMocks;
import org.mockito.Spy;
import org.springframework.http.HttpStatus;
import org.springframework.mock.web.MockHttpServletResponse;
import tests.BaseTest;

import java.io.*;
import java.nio.ByteBuffer;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import static org.junit.Assert.assertEquals;

public class UtilsTest extends BaseTest {

    @InjectMocks
    @Spy
    Utils utils;

    private static class ClassThatJacksonCannotSerialize {}

    @Test
    public void convertObjectTest() throws Exception
    {
        String jsonString = "{ \"id\":1, \"userId\":1,\"number\":\"123456789\", \"primary\": \"true\", \"verified\": true, \"createdAt\":\"2022-01-01\", \"updatedAt\":\"2022-01-01\", \"stoppedAt\":\"2022-01-01\" }";
        UserMobile convertedObject = Utils.convertObject(jsonString, UserMobile.class);
        assertEquals(1, convertedObject.getId().longValue());
        assertEquals("123456789", convertedObject.getNumber());
    }

    @Test
    public void convertObjectTest_withTypeReference() throws Exception
    {
        String jsonString = "{ \"id\":1, \"userId\":1,\"number\":\"123456789\", \"primary\": \"true\", \"verified\": true, \"createdAt\":\"2022-01-01\", \"updatedAt\":\"2022-01-01\", \"stoppedAt\":\"2022-01-01\" }";
        UserMobile convertedObject = Utils.convertObject(jsonString, new TypeReference<UserMobile>(){});
        assertEquals(1, convertedObject.getId().longValue());
        assertEquals("123456789", convertedObject.getNumber());
    }

    @Test
    public void ensureSinglePrimaryMobileTest()
    {
        UserMobileRequest userMobileRequest = new UserMobileRequest("123456789", true, true, "patient");
        List<UserMobileRequest> list = Arrays.asList(userMobileRequest, userMobileRequest);
        Utils.ensureSinglePrimaryMobile(list);
        assertEquals(userMobileRequest.isPrimary(), list.get(0).isPrimary());
    }

    @Test
    public void ensureSinglePrimaryMobileTest2()
    {
        UserMobileRequest userMobileRequest = new UserMobileRequest("123456789", false, true, null);
        List<UserMobileRequest> list = Arrays.asList(userMobileRequest);
        Utils.ensureSinglePrimaryMobile(list);
        assertEquals(userMobileRequest.isPrimary(), list.get(0).isPrimary());
    }

    @Test
    public void ensureSinglePrimaryEmailTest()
    {
        UserEmailRequest userEmailRequest = new UserEmailRequest("123456789", true);
        List<UserEmailRequest> list = Arrays.asList(userEmailRequest, userEmailRequest);
        Utils.ensureSinglePrimaryEmail(list);
        assertEquals(userEmailRequest.isPrimary(), list.get(0).isPrimary());
    }

    @Test
    public void ensureSinglePrimaryEmailTest2()
    {
        UserEmailRequest userEmailRequest = new UserEmailRequest("123456789", false);
        List<UserEmailRequest> list = Arrays.asList(userEmailRequest);
        Utils.ensureSinglePrimaryEmail(list);
        assertEquals(userEmailRequest.isPrimary(), list.get(0).isPrimary());
    }

    @Test(expected = RuntimeException.class)
    public void asJsonString_Exception() {
        //if a class has no public fields/method, we can see exception from jackson
        Utils.asJsonString(new ClassThatJacksonCannotSerialize());
    }

    @Test
    public void formatDate_incorrectDate() {
        Date inp = new Date(2000, 1, 1);
        Date out = Utils.formatDate(inp, "");
        int isEqual = inp.compareTo(out);
        assertEquals(0, isEqual);
    }

    @Test
    public void writeCustomResponseTest() throws IOException {
        MockHttpServletResponse response = new MockHttpServletResponse();
        Utils.writeCustomResponseToOutputStream(response, new Exception("Test"), HttpStatus.OK);
        assertEquals("application/json", response.getHeader("content-type"));
    }

    @Test
    public void readRequestBodyTest() throws IOException {
        String text = "this is text";   // It can be Unicode text
        ByteBuffer buffer = ByteBuffer.wrap(text.getBytes("UTF-8"));
        InputStream is = new ByteBufferBackedInputStream(buffer);
        String out = Utils.readRequestBody(is);
        assertEquals(text, out);
    }
    @Test
    public void readRequestBodyTest_withNullInput() throws IOException {
        InputStream is = null;
        String out = Utils.readRequestBody(is);
        assertEquals(null, out);
    }

}
