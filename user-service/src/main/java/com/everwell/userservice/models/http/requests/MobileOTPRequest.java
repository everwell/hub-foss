package com.everwell.userservice.models.http.requests;

import com.everwell.userservice.exceptions.ValidationException;
import lombok.*;
import org.apache.commons.lang3.StringUtils;

@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class MobileOTPRequest {
    private String mobile;
    private String txnId;

    public void validate() {
        if(StringUtils.isEmpty(mobile)) {
            throw new ValidationException("mobile cannot be null or empty");
        } else if(StringUtils.isEmpty(txnId)) {
            throw new ValidationException("txnId cannot be null or empty");
        }
    }

    public void validateForMobileLogin() {
        if(StringUtils.isEmpty(mobile)) {
            throw new ValidationException("mobile cannot be null or empty");
        }
    }

}
