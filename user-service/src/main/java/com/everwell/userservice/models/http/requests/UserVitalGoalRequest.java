package com.everwell.userservice.models.http.requests;

import com.everwell.userservice.models.dtos.vitals.VitalGoalDto;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Getter
public class UserVitalGoalRequest {
    private Long userId;

    private List<VitalGoalDto> vitalGoalList;
}
