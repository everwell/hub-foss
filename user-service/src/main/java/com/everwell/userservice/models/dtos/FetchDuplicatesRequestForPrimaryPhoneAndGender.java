package com.everwell.userservice.models.dtos;

import com.everwell.userservice.enums.DeduplicationScheme;
import com.everwell.userservice.exceptions.ValidationException;
import com.everwell.userservice.utils.ValidationUtils;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import org.springframework.util.StringUtils;

@Getter
@Setter
@AllArgsConstructor
@JsonIgnoreProperties(ignoreUnknown=true)
public class FetchDuplicatesRequestForPrimaryPhoneAndGender extends FetchDuplicatesRequest
{
    private String primaryPhoneNumber;
    private String gender;

    public void validate()
    {
        super.validate();

        if(StringUtils.isEmpty(primaryPhoneNumber))
        {
            throw new ValidationException("primaryPhoneNumber cannot be empty");
        }

        if(!ValidationUtils.isValidNumber(primaryPhoneNumber))
        {
            throw new ValidationException("Invalid phone number");
        }

        if(StringUtils.isEmpty(gender))
        {
            throw new ValidationException("gender cannot be empty");
        }
    }

    public FetchDuplicatesRequestForPrimaryPhoneAndGender()
    {
        super(DeduplicationScheme.PRIMARYPHONE_GENDER);
    }
}
