package com.everwell.userservice.models.dtos;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ReferenceIdRMQResponse {
    public Long refId;
    public String username;
    public String response;
    public boolean success;

    public ReferenceIdRMQResponse(Long refId, String username, String response, boolean success)
    {
        this.refId = refId;
        this.username = username;
        this.response = response;
        this.success = success;
    }
}
