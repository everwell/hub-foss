package com.everwell.userservice.models.dtos;

import com.everwell.userservice.enums.UserType;
import com.everwell.userservice.exceptions.ValidationException;
import com.everwell.userservice.utils.Utils;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.util.StringUtils;

import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@JsonIgnoreProperties(ignoreUnknown=true)
public class AddUserRequest
{
    private String firstName;

    private String lastName;

    private String gender;

    private String dateOfBirth;

    private String language;

    private String address;

    private Long addedBy;

    private String addedByType;

    private String fatherName;

    private Integer pincode;

    private String city;

    private String maritalStatus;

    private String socioEconomicStatus;

    private String keyPopulation;

    private Integer height;

    private Integer weight;

    private List<UserMobileRequest> mobileList = new ArrayList<>();

    private List<UserEmailRequest> emailList = new ArrayList<>();

    private String source;

    private String entityId;

    private UserType userType;

    private String occupation;

    private String taluka;

    private String town;

    private String ward;

    private String landmark;

    private String area;

    private String contactPersonAddress;

    private String contactPersonName;

    private String contactPersonPhone;

    public void validate() throws ValidationException
    {
        if(StringUtils.isEmpty(firstName))
        {
            throw new ValidationException("First name cannot be empty");
        }

        if((null == mobileList) || mobileList.isEmpty())
        {
            throw new ValidationException("Must have at least one mobile number");
        }

        //making sure there is only 1 primary number
        Utils.ensureSinglePrimaryMobile(mobileList);
        mobileList = Utils.removeDuplicateNumbers(mobileList);

        if(null != emailList)
        {
            //making sure there is only 1 primary email
            Utils.ensureSinglePrimaryEmail(emailList);
            emailList = Utils.removeDuplicateEmails(emailList);
        }
    }
}