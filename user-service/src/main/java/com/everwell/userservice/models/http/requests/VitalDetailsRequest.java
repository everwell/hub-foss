package com.everwell.userservice.models.http.requests;


import com.everwell.userservice.enums.VitalValidation;
import com.everwell.userservice.exceptions.ValidationException;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;

import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Getter
public class VitalDetailsRequest {
    private List<Long> userIds;

    private List<Long> vitalIds;

    private List<String> vitalNames;

    private boolean includeLogs;

    private boolean includeAggregatedData;

    private boolean includeUserRelations;

    private boolean includeUserGoals;

    private String startDateTime;

    private String endDateTime;

    public void validate() {
        if (CollectionUtils.isEmpty(userIds)) {
            throw new ValidationException(VitalValidation.USER_ID_LIST_INVALID.getMessage());
        }
        if (CollectionUtils.isEmpty(vitalIds) && CollectionUtils.isEmpty(vitalNames)) {
            throw new ValidationException(VitalValidation.VITAL_ID_AND_VITAL_NAME_LIST_INVALID.getMessage());
        }
        if (StringUtils.isEmpty(startDateTime)) {
            throw new ValidationException(VitalValidation.START_DATE_TIME_INVALID.getMessage());
        }
    }
}
