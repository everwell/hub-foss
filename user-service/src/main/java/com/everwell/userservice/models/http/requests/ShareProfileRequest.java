package com.everwell.userservice.models.http.requests;

import com.everwell.userservice.exceptions.ValidationException;
import lombok.*;
import org.apache.commons.lang3.StringUtils;

@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class ShareProfileRequest {
    private String requestId;
    private Intent intent;
    private HipDetails hipDetails;
    private String source;
    private Long userId;

    public void validate() {
        if(StringUtils.isEmpty(requestId)) {
            throw new ValidationException("requestId cannot be null or empty");
        }else if(StringUtils.isEmpty(source)) {
            throw new ValidationException("source cannot be null or empty");
        }else if(intent == null) {
            throw new ValidationException("intent cannot be null or empty");
        }else if(hipDetails == null) {
            throw new ValidationException("hipDetails cannot be null or empty");
        }else if(userId==null) {
            throw new ValidationException("userId cannot be null or empty");
        }
    }
}

