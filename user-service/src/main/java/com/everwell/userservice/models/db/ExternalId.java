package com.everwell.userservice.models.db;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "external_ids")
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class ExternalId
{
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column
    private Long userId;

    @Column(length = 25)
    private String type;

    @Column(length = 255)
    private String value;

    @Column
    @JsonFormat(shape = JsonFormat.Shape.NUMBER)
    private Date createdAt;
}
