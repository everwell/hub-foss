package com.everwell.userservice.models.dtos;

import lombok.AllArgsConstructor;
import lombok.Data;

@AllArgsConstructor
@Data
public class DetailsChangedDto <T> {
    Boolean detailsChanged;
    T data;
}
