package com.everwell.userservice.models.http.requests;

import com.everwell.userservice.exceptions.ValidationException;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.ToString;
import org.apache.commons.lang3.StringUtils;

@Getter
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class HealthIdOTPRequest {
    private String authMethod;
    private String healthid;

    public void validate() {
        if(StringUtils.isEmpty(authMethod)) {
            throw new ValidationException("authMethod cannot be null or empty");
        } else if(StringUtils.isEmpty(healthid)) {
            throw new ValidationException("healthid cannot be null or empty");
        }
    }

}
