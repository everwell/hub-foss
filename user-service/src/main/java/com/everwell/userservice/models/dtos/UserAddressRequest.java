package com.everwell.userservice.models.dtos;


import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class UserAddressRequest
{
    private String taluka;

    private String town;

    private String ward;

    private String landmark;

    private String area;
}
