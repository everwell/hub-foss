package com.everwell.userservice.models.dtos;

import com.everwell.userservice.exceptions.ValidationException;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.util.StringUtils;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@JsonIgnoreProperties(ignoreUnknown=true)
public class UserMobileRequest
{
    private String number;
    private boolean primary;
    private boolean verified;
    private String owner;

    public void validate()
    {
        if(StringUtils.isEmpty(number))
        {
            throw new ValidationException("Number cannot be empty");
        }
    }
}
