package com.everwell.userservice.models.dtos;

import com.everwell.userservice.models.db.*;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class UserDetails {

    private User user;

    private List<UserMobile> mobileList;

    private List<UserEmail> emailList;

    private UserAddress userAddress;

    private UserContactPerson userContactPerson;

    public UserDetails(User user, List<UserMobile> mobileList, List<UserEmail> emailList) {
        this.user = user;
        this.mobileList = mobileList;
        this.emailList = emailList;
    }
}
