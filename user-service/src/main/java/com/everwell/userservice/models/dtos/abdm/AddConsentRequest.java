package com.everwell.userservice.models.dtos.abdm;

import com.everwell.userservice.enums.UserValidation;
import com.everwell.userservice.exceptions.ValidationException;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class AddConsentRequest {
    public Long userId;
    public Object abdmConsentInitiationRequest;
    public String consentId;

    public void validate() {
        if(null == userId) {
            throw new ValidationException(UserValidation.USER_ID_NULL_ERROR.getMessage());
        }
    }
}
