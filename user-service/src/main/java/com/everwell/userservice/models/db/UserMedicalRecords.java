package com.everwell.userservice.models.db;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.*;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "user_medical_records")
@Data
public class UserMedicalRecords {

    @Id
    @Column
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "consent_artefact_id")
    private String consentArtefactId;

    @Column(name = "consent_request_id")
    private String consentRequestId;

    @Column(name = "transaction_id")
    private String transactionId;

    @Column(name = "hiu_private_key")
    private String hiuPrivateKey;

    @Column(name = "hiu_nonce")
    private String hiuNonce;

    @Column(name = "encrypted_json", length = 5000)
    private String encryptedJson;

    @Column
    @JsonFormat(shape = JsonFormat.Shape.NUMBER)
    private Date createdAt;

    @Column
    @JsonFormat(shape = JsonFormat.Shape.NUMBER)
    private Date updatedAt;

    public UserMedicalRecords(String consentArtefactId, String consentRequestId, Date now) {
        this.consentArtefactId = consentArtefactId;
        this.consentRequestId = consentRequestId;
        this.createdAt = now;
    }

}
