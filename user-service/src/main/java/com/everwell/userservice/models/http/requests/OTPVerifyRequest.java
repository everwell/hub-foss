package com.everwell.userservice.models.http.requests;

import com.everwell.userservice.exceptions.ValidationException;
import lombok.*;
import org.apache.commons.lang3.StringUtils;

@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class OTPVerifyRequest {
    private String otp;
    private String txnId;
    private Long userId;
    private String entityType;
    private Long entityId;
    private String authMode;

    public void validate() {
        if(StringUtils.isEmpty(otp)) {
            throw new ValidationException("otp cannot be null or empty");
        } else if(StringUtils.isEmpty(txnId)) {
            throw new ValidationException("txnId cannot be null or empty");
        }
    }

    public void validateForUserUpdate() {
        if(StringUtils.isEmpty(otp)) {
            throw new ValidationException("otp cannot be null or empty");
        } else if(StringUtils.isEmpty(txnId)) {
            throw new ValidationException("txnId cannot be null or empty");
        } else if (null == userId) {
            throw new ValidationException("userId cannot be null or empty");
        }
    }

}
