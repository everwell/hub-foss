package com.everwell.userservice.models.db.timescale;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

@AllArgsConstructor
@NoArgsConstructor
@Getter
public class VitalAssociatedLogs {
    private LocalDateTime time;

    private Long userId;

    private Long vitalId;

    private String vitalValue;

    public VitalAssociatedLogs(Long userId, Long vitalId, String vitalValue) {
        this.userId = userId;
        this.vitalId = vitalId;
        this.vitalValue = vitalValue;
    }
}
