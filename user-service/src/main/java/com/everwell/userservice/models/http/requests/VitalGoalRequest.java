package com.everwell.userservice.models.http.requests;

import com.everwell.userservice.enums.VitalValidation;
import com.everwell.userservice.exceptions.ValidationException;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.apache.commons.lang3.StringUtils;


@AllArgsConstructor
@NoArgsConstructor
@Getter
public class VitalGoalRequest {
    private Long vitalId;

    private String vitalName;

    private Float value;

    public void validate() {
        if (null == vitalId && StringUtils.isEmpty(vitalName)) {
            throw new ValidationException(VitalValidation.VITAL_ID_OR_VITAL_NAME.getMessage());
        }
        if (null == value) {
            throw new ValidationException(VitalValidation.VITAL_VALUE_INVALID.getMessage());
        }
    }
}
