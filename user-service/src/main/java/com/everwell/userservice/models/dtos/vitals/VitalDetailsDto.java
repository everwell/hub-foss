package com.everwell.userservice.models.dtos.vitals;


import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Getter
public class VitalDetailsDto {
    private List<UserVitalGoalDto> usersVitalGoalData;
    private List<VitalsData> logs;
    private List<VitalsAggregatedData> aggregatedData;

    @NoArgsConstructor
    @AllArgsConstructor
    @Getter
    public static class VitalsAggregatedData {
        private String vitalName;
        private Long vitalId;
        private List<VitalsAggregatedTimeData> vitalData;
    }

    @NoArgsConstructor
    @AllArgsConstructor
    @Getter
    public static class VitalsData {
        private String vitalName;
        private Long vitalId;
        private List<VitalsTimeData> vitalData;
    }
}
