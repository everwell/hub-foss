package com.everwell.userservice.models.http.requests;

import com.everwell.userservice.exceptions.ValidationException;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.ToString;
import org.apache.commons.lang3.StringUtils;

@Getter
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class ABHASearchRequest {
    private String healthId;

    public void validate() {
        if(StringUtils.isEmpty(healthId)) {
            throw new ValidationException("healthId cannot be null or empty");
        }
    }

}
