package com.everwell.userservice.models.dtos;

import com.everwell.userservice.enums.UserRelationStatus;
import com.everwell.userservice.exceptions.ValidationException;
import com.everwell.userservice.models.db.UserRelation;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.*;

@Data
@AllArgsConstructor
@JsonIgnoreProperties(ignoreUnknown=true)
public class UserRelationDto {
    private Long primaryUserId;

    private Long secondaryUserId;

    private String relation;

    private String role;

    private String nickname;

    private String status;

    private boolean emergencyContact;

    public void validate(UserRelationDto userRelationDto) {
        if (null == userRelationDto.getPrimaryUserId()) {
            throw new ValidationException("Primary user id cannot be null");
        }

        if (null == userRelationDto.getSecondaryUserId()) {
            throw new ValidationException("Secondary user id cannot be null");
        }

        if (userRelationDto.getPrimaryUserId().equals(userRelationDto.getSecondaryUserId())) {
            throw new ValidationException("Users cannot be same");
        }
    }

    public UserRelationDto(UserRelation userRelation) {
        this.primaryUserId = userRelation.getPrimaryUserId();
        this.secondaryUserId = userRelation.getSecondaryUserId();
        this.status = UserRelationStatus.getStatusName(userRelation.getStatus());
        this.relation = userRelation.getRelation();
        this.role = userRelation.getRole();
        this.nickname = userRelation.getNickname();
        this.emergencyContact = userRelation.isEmergencyContact();
    }
}
