package com.everwell.userservice.models.dtos;

import com.everwell.userservice.exceptions.ValidationException;
import com.everwell.userservice.utils.Utils;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.List;

@Getter
@NoArgsConstructor
@AllArgsConstructor
public class UpdateEmailRequest
{
    private Long userId;
    private List<UserEmailRequest> emailList = new ArrayList<>();

    public void validate()
    {
        if((null == emailList) || emailList.isEmpty())
        {
            throw new ValidationException("Must have at least one email id");
        }

        for(UserEmailRequest userEmailRequest: emailList)
        {
            userEmailRequest.validate();
        }

        //ensuring there exists only 1 primary email
        Utils.ensureSinglePrimaryEmail(emailList);
        emailList = Utils.removeDuplicateEmails(emailList);
    }
}
