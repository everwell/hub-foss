package com.everwell.userservice.repositories;

import com.everwell.userservice.models.db.UserMobile;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface UserMobileRepository extends JpaRepository<UserMobile, Long>
{
    List<UserMobile> findAllByUserIdAndStoppedAtNull(Long userId);

    List<UserMobile> findAllByUserIdInAndStoppedAtNull(List<Long> userIds);

    List<UserMobile> findAllByNumberAndIsPrimaryTrue(String number);

    List<UserMobile> findAllByNumber(String number);
}
