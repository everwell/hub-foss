package com.everwell.userservice.repositories.impl;

import com.everwell.userservice.enums.TimescaleQueryEnum;
import com.everwell.userservice.mapper.VitalsTimeDataRowMapper;
import com.everwell.userservice.models.db.timescale.VitalLogs;
import com.everwell.userservice.models.dtos.vitals.VitalsTimeData;
import com.everwell.userservice.repositories.VitalLogsRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSourceUtils;
import org.springframework.stereotype.Repository;

import java.sql.Timestamp;
import java.util.List;

import static com.everwell.userservice.constants.Constants.TIMESCALE_JDBC_BEAN_NAME;

@Repository
public class VitalLogsRepositoryImpl extends UserVitalTimescaleRepositoryImpl implements VitalLogsRepository {

    @Autowired
    @Qualifier(TIMESCALE_JDBC_BEAN_NAME)
    private NamedParameterJdbcTemplate timescaleNamedParameterJdbcTemplate;

    @Autowired
    private VitalsTimeDataRowMapper vitalsTimeDataRowMapper;

    @Override
    public void saveAll(List<VitalLogs> vitalLogsList) {
        String query = TimescaleQueryEnum.INSERT_DATA_INTO_VITAL_LOGS.getQuery();
        SqlParameterSource[] batch = SqlParameterSourceUtils.createBatch(vitalLogsList.toArray());
        timescaleNamedParameterJdbcTemplate.batchUpdate(query, batch);
    }

    @Override
    public List<VitalsTimeData> fetchLogs(List<Long> userIdList, List<Long> vitalIdList, Timestamp startTimestamp, Timestamp endTimestamp) {
        String query = TimescaleQueryEnum.GET_VITALS_LOGS.getQuery();
        return processQueryForResult(query, userIdList, vitalIdList, startTimestamp, endTimestamp, vitalsTimeDataRowMapper);
    }
}
