package com.everwell.userservice.repositories.impl;

import com.everwell.userservice.enums.TimescaleQueryEnum;
import com.everwell.userservice.mapper.VitalsAggregatedTimeDataRowMapper;
import com.everwell.userservice.models.dtos.vitals.VitalsAggregatedTimeData;
import com.everwell.userservice.repositories.VitalAggregateRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.sql.Timestamp;
import java.util.List;


@Repository
public class VitalAggregateRepositoryImpl extends UserVitalTimescaleRepositoryImpl implements VitalAggregateRepository {

    @Autowired
    private VitalsAggregatedTimeDataRowMapper vitalsAggregatedTimeDataRowMapper;

    @Override
    public List<VitalsAggregatedTimeData> fetchData(List<Long> userIdList, List<Long> vitalIdList, Timestamp startTimestamp, Timestamp endTimestamp) {
        String query = TimescaleQueryEnum.GET_VITALS_AGGREGATED_DATA.getQuery();
        return processQueryForResult(query, userIdList, vitalIdList, startTimestamp, endTimestamp, vitalsAggregatedTimeDataRowMapper);
    }
}
