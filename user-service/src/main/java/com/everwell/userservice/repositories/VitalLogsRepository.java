package com.everwell.userservice.repositories;

import com.everwell.userservice.models.db.timescale.VitalLogs;
import com.everwell.userservice.models.dtos.vitals.VitalsTimeData;
import org.springframework.stereotype.Repository;

import java.sql.Timestamp;
import java.util.List;

@Repository
public interface VitalLogsRepository {
    void saveAll(List<VitalLogs> vitalLogsList);

    List<VitalsTimeData> fetchLogs(List<Long> userIdList, List<Long> vitalIdList, Timestamp startTimestamp, Timestamp endTimestamp);
}
