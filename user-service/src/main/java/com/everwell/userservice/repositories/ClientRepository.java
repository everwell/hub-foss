package com.everwell.userservice.repositories;

import com.everwell.userservice.models.db.Client;
import com.everwell.userservice.models.db.ExternalId;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;


@Repository
public interface ClientRepository extends JpaRepository<Client, Long>
{
    Client findClientByName(String clientName);
}
