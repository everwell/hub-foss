package com.everwell.userservice.controllers;

import com.everwell.userservice.models.dtos.Response;
import com.everwell.userservice.models.dtos.abdm.ABDMConsentResponse;
import com.everwell.userservice.models.dtos.abdm.AddConsentRequest;
import com.everwell.userservice.services.ABDMService;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import static com.everwell.userservice.constants.Constants.X_US_CLIENT_ID;

@RestController
public class ABDMConsentController {

    @Autowired
    ABDMService abdmService;

    private static Logger LOGGER = LoggerFactory.getLogger(ABDMConsentController.class);

    @ApiOperation(
            value = "Get Consent details by Consent Id",
            notes = "Used to get abdm consent details by consent id"
    )
    @RequestMapping(value = "/v1/abdm-consent/{consentId}", method = RequestMethod.GET)
    public ResponseEntity<Response<ABDMConsentResponse>> getConsentDetails(@PathVariable("consentId") String consentId, @RequestHeader(name = X_US_CLIENT_ID) Long clientId) throws Exception
    {
        LOGGER.info("[getConsentDetails Request received]: " + consentId);
        return new ResponseEntity<>(new Response<>(abdmService.getByConsentId(consentId),"data fetched successfully"), HttpStatus.OK);
    }

    @RequestMapping(value = "/v1/abdm-consent", method = RequestMethod.POST)
    @ApiOperation(
            value = "Add Consent Request from HIU",
            notes = "Add Consent details"
    )
    public ResponseEntity<Response<String>> addConsentDetails(@RequestBody AddConsentRequest request, @RequestHeader(name = X_US_CLIENT_ID) Long clientId) {
        LOGGER.debug("[addConsentDetails Request received]: " + request);
        request.validate();
        abdmService.addHIUConsentDetails(request);
        return new ResponseEntity<>(new Response<>(true, "ABDM consent details added"),HttpStatus.OK);
    }
}
