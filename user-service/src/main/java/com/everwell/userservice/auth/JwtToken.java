package com.everwell.userservice.auth;

import com.everwell.userservice.utils.Utils;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import org.springframework.stereotype.Component;

import java.util.Base64;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.function.Function;

@Component
public class JwtToken {

    private String getClientIdFromToken(String token, String secret) {
        return getClaimFromToken(token, secret, Claims::getSubject);
    }

    private Date getExpirationDateFromToken(String token, String secret) {
        return getClaimFromToken(token, secret, Claims::getExpiration);
    }

    private <T> T getClaimFromToken(String token, String secret, Function<Claims, T> claimsResolver) {
        final Claims claims = getAllClaimsFromToken(token, secret);
        return claimsResolver.apply(claims);
    }

    private Claims getAllClaimsFromToken(String token, String secret) {
        return Jwts.parser().setSigningKey(Base64.getEncoder().encodeToString(secret.getBytes())).parseClaimsJws(token).getBody();
    }

    private Boolean isTokenExpired(String token, String secret) {
        final Date expiration = getExpirationDateFromToken(token, secret);
        return expiration.before(Utils.getCurrentDate());
    }

    private String doGenerateToken(Map<String, Object> claims, String subject, String secret, Date expiry) {
        return Jwts.builder()
                .setClaims(claims)
                .setSubject(subject)
                .setIssuedAt(Utils.getCurrentDate())
                .setExpiration(expiry)
                .signWith(SignatureAlgorithm.HS512,  Base64.getEncoder().encodeToString(secret.getBytes())).compact();
    }

    public String generateToken(String clientId, String secret, Date expiry) {
        Map<String, Object> claims = new HashMap<>();
        return doGenerateToken(claims, clientId, secret, expiry);
    }

    public Boolean isValidToken(String token, String inputClientId, String secret) {
        final String clientId = getClientIdFromToken(token, secret);
        return (clientId.equals(inputClientId) && !isTokenExpired(token, secret));
    }
}