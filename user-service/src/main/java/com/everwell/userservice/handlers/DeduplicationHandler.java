package com.everwell.userservice.handlers;

import com.everwell.userservice.enums.DeduplicationScheme;
import com.everwell.userservice.models.db.User;
import com.everwell.userservice.models.dtos.FetchDuplicatesRequest;

import java.util.List;

public abstract class DeduplicationHandler
{
    public abstract DeduplicationScheme getDeduplicationScheme();

    public abstract List<String> getDefaultRequiredFields();

    public abstract List<User> fetchDuplicates(FetchDuplicatesRequest fetchDuplicatesRequest, Long clientId);
}
