package com.everwell.userservice.enums;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public enum UserValidation {
    USER_ID_NOT_FOUND("user-ids : %s not found"),
    USER_ID_NULL_ERROR("user id cannot be null"),
    CONSENT_ID_NOT_FOUND("Consent with given id not found");

    @Getter
    private final String message;
}
