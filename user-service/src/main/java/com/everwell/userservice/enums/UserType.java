package com.everwell.userservice.enums;

public enum UserType
{
    STAFF,
    PATIENT;
}
