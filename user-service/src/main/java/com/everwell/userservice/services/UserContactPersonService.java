package com.everwell.userservice.services;

import com.everwell.userservice.models.db.UserContactPerson;
import com.everwell.userservice.models.dtos.DetailsChangedDto;
import com.everwell.userservice.models.dtos.UserContactPersonRequest;

public interface UserContactPersonService {

    UserContactPerson addUserContactPersonDetails(Long userId, UserContactPersonRequest userContactPersonRequest);

    DetailsChangedDto<UserContactPerson> updateUserContactPersonDetails(Long userId, UserContactPersonRequest userContactPersonRequest);

    UserContactPerson getUserContactPersonDetails(Long userId);
}
