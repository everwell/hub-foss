package com.everwell.userservice.services.impl;

import com.everwell.userservice.auth.JwtToken;
import com.everwell.userservice.constants.Constants;
import com.everwell.userservice.models.db.Client;
import com.everwell.userservice.services.AuthenticationService;
import com.everwell.userservice.utils.Utils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;

@Service
public class AuthenticationServiceImpl implements AuthenticationService {

    @Autowired
    private JwtToken jwtTokenUtil;


    @Override
    public String generateToken(Client client) {
        long refreshTime = Utils.getCurrentDate().getTime() + Constants.REFRESH_EXPIRY;
        return jwtTokenUtil.generateToken(client.getId().toString(), client.getName(), new Date(refreshTime));
    }

    @Override
    public Boolean isValidToken(String token, Client client) {
        return jwtTokenUtil.isValidToken(token, client.getId().toString(), client.getName());
    }
}
