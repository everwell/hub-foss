package com.everwell.userservice.services;

import org.springframework.amqp.core.Message;

public interface RabbitMQConsumerService
{
    void addUser(Message message) throws Exception;

    void deleteUser(Message message) throws Exception;

    void updateUserDetails(Message message) throws Exception;

    void updateUserMobiles(Message message) throws Exception;

    void updateUserEmails(Message message) throws Exception;

    void updateConsent(Message message) throws Exception;
}
