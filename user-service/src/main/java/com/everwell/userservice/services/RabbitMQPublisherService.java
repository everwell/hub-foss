package com.everwell.userservice.services;

import java.util.Map;

public interface RabbitMQPublisherService
{
    void send(Object message, String exchangeName, String routingKey);

    void send(Object message, String routingKey, String exchange, Map<String, Object> map);
}