package com.everwell.userservice.utils;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.Base64;

@Service
public class EncryptionUtils {

    private static String ALGO;
    private static String KEY;

    @Value("${ENCRYPTION_ALGO}")
    public void setALGO(String algo) {
        ALGO = algo;
    }

    @Value("${SECRET_KEY}")
    public void setKEY(String key) {
        KEY = key;
    }

    public static String decrypt(String input) {
        return decrypt(input, ALGO, KEY);
    }

    public static String decrypt(String input, String algo, String key) {
        try {
            Cipher cipher = Cipher.getInstance(algo);
            try {
                cipher.init(Cipher.DECRYPT_MODE, new SecretKeySpec(key.getBytes(), "AES"), new IvParameterSpec(key.getBytes()));
                try {
                    byte[] plainText = cipher.doFinal(Base64.getDecoder().decode(input));
                    return new String(plainText);
                } catch (IllegalBlockSizeException | BadPaddingException e) {
                    throw new RuntimeException(e);
                }
            } catch (InvalidKeyException | InvalidAlgorithmParameterException e) {
                throw new RuntimeException(e);
            }
        } catch (NoSuchAlgorithmException | NoSuchPaddingException e) {
            throw new RuntimeException(e);
        }
    }

    public static String encrypt(String input) {
        return encrypt(input, ALGO, KEY);
    }
    public static String encrypt(String input, String algo, String key) {
        try {
            Cipher cipher = Cipher.getInstance(algo);
            try {
                cipher.init(Cipher.ENCRYPT_MODE, new SecretKeySpec(key.getBytes(), "AES"), new IvParameterSpec(key.getBytes()));
                try {
                    byte[] cipherText = cipher.doFinal(input.getBytes());
                    return Base64.getEncoder().encodeToString(cipherText);
                } catch (IllegalBlockSizeException | BadPaddingException e) {
                    throw new RuntimeException(e);
                }
            } catch (InvalidKeyException | InvalidAlgorithmParameterException e) {
                throw new RuntimeException(e);
            }
        } catch (NoSuchAlgorithmException | NoSuchPaddingException e) {
            throw new RuntimeException(e);
        }
    }
}
