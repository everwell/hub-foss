package com.everwell.userservice.utils;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.ValueOperations;
import org.springframework.stereotype.Component;

import java.util.concurrent.TimeUnit;


@Component
public class CacheUtils {
    private static RedisTemplate<String, Object> redisTemplate;

    @Autowired
    public CacheUtils(RedisTemplate<String, Object> redisTemplate) {
        com.everwell.userservice.utils.CacheUtils.redisTemplate = redisTemplate;
    }

    public static <T> T getFromCache(String key) {
        ValueOperations valueOperations = redisTemplate.opsForValue();
        Object value = valueOperations != null ? valueOperations.get(key) : null;
        return (null == value) ? null: (T) value;
    }

    public static void putIntoCache(String key, Object value, Long ttl) {
        redisTemplate.opsForValue().set(key, value, ttl, TimeUnit.SECONDS);
    }
}
