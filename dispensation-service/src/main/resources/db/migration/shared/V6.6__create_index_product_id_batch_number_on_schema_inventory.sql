create index if not exists idx_productid_batchnumber
    on ds_product_inventory (product_id, batch_number);