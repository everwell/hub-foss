ALTER TABLE ds_dispensation
    ADD COLUMN IF NOT EXISTS status int4 default 0;