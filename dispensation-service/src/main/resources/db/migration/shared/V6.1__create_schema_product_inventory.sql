create table if not exists ds_product_inventory
(
    id bigserial not null
        constraint ds_product_inventory_pkey
            primary key,
    product_id bigint not null,
    batch_number varchar(255),
    expiry_date timestamp
);