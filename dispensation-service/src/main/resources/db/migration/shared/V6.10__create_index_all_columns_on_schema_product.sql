create index if not exists idx_composition_dosage_dosageforn_drugcode_manufacturer_product_name
    on ds_product (composition, dosage, dosage_form, drug_code, manufacturer, product_name);