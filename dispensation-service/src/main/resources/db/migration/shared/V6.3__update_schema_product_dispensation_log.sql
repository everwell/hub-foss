ALTER TABLE ds_product_dispensation_log
    RENAME TO ds_product_inventory_log;

ALTER TABLE ds_product_inventory_log
    RENAME COLUMN type TO transaction_type;

ALTER TABLE ds_product_inventory_log
    RENAME COLUMN product_dispensation_mapping_id TO transaction_id;

ALTER TABLE ds_product_inventory_log
    RENAME COLUMN number_of_units TO transaction_quantity;

ALTER TABLE ds_product_inventory_log
    RENAME COLUMN logged_date TO date_of_action;

ALTER TABLE ds_product_inventory_log
    ADD p_inventory_id bigint,
    ADD product_id  bigint,
    ADD transaction_id_type int4,
    ADD updated_by  bigint;

ALTER TABLE ds_product_inventory_log
    DROP CONSTRAINT ds_dispensation_log_pkey;

ALTER TABLE ds_product_inventory_log
    ADD CONSTRAINT ds_product_inventory_log_pkey PRIMARY KEY (id);

ALTER SEQUENCE
    ds_product_dispensation_log_id_seq RENAME TO ds_product_inventory_log_id_seq;
