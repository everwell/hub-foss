package com.everwell.dispensationservice.services;

import com.everwell.dispensationservice.models.db.Client;

public interface AuthenticationService {

    String generateToken(Client clientAuth);

    Boolean isValidToken(String token, Client client);
}
