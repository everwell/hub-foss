package com.everwell.dispensationservice.services;

import com.everwell.dispensationservice.models.db.Dispensation;
import com.everwell.dispensationservice.models.db.ProductInventoryLog;
import com.everwell.dispensationservice.models.db.ProductDispensationMapping;
import com.everwell.dispensationservice.models.dto.*;
import com.everwell.dispensationservice.models.http.requests.DispensationRequest;
import com.everwell.dispensationservice.models.http.requests.ReturnDispensationRequest;
import com.everwell.dispensationservice.models.http.responses.EntityDispensationResponse;
import org.springframework.scheduling.annotation.Async;

import java.io.IOException;
import java.util.*;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;

public interface DispensationService {

    Dispensation create(DispensationRequest dispensationRequest, Long clientId);

    void emitForEpisode(DispensationRequest dispensationRequest, Long clientId);

    Dispensation getDispensation(Long id, Long clientId);

    Dispensation delete(Long id, Long clientId);

    void setRefillDate(Dispensation dispensation, Date refillDate);

    List<Dispensation> getDispensationsForEntity(Long entityId, Long clientId);

    List<ProductDispensationMapping> addDispensationProducts(DispensationRequest dispensationRequest, long dispensationId, Date issuedDate);

    List<DispensationProductDto> getProductDtoForDispensations(Collection<Long> Ids);

    CompletableFuture<Long> getTotalDaysDrugsDispensedForEntity(Long entityId, List<Dispensation> dispensations);

    Date getRefillDateForEntity(Long entityId, List<Dispensation> dispensations);

    List<EntityDispensationResponse> filterList(List<EntityDispensationResponse> responseList, List<RangeValueFilter> filters);

    List<ProductInventoryLog> createReturnDispensation(ReturnDispensationRequest returnDispensationRequest, Long clientId, Boolean writeInventoryLog);

    List<Long> createReturnDispensationWithInventoryData(ReturnDispensationRequest returnDispensationRequest, Long clientId);

    Map<Long, List<ProductInventoryLogDto>> getProductInventoryLogMap(List<Long> productDispensationMappingIds);

    List<ProductInventoryLog> setReturnDispensationStockData(ReturnDispensationRequest returnDispensationRequest, Long clientId);
    
    void setAddDispensationStockData(DispensationRequest dispensationRequest, Long clientId, Long dispensationId, Date issuedDate);

    Dispensation createDispensationWithInventoryData(DispensationRequest dispensationRequest, Long clientId);

    EntityDispensationResponse getAllDispensationDataForEntity(Long entityId, Long clientId, boolean getProductLogs) throws ExecutionException, InterruptedException;

    Map<Long, List<DispensationProductDto>> getDispensationIdToDispensationProductDtoMap(Collection<Long> dispensationIds, Boolean includeProductLog);

    Map<Long, List<StockQuantityDto>> getStockQuantityDtoMap(Map<Long, List<ProductInventoryLogDto>> logsIdToEntityMap);

    Set<ProductMappingInventoryDto> mergeSimilarProductMappings(List<DispensationRequest.ProductData> productDataList, Date issuedDate);

    List<EntityDispensationResponse> getAllDispensationDataForEntityBulk(List<Long> entityIds, Long clientId) throws ExecutionException, InterruptedException, IOException;

}
