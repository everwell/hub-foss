package com.everwell.dispensationservice.services;

import com.everwell.dispensationservice.models.db.Product;
import com.everwell.dispensationservice.models.http.requests.ProductBulkRequest;
import com.everwell.dispensationservice.models.http.requests.ProductRequest;
import com.everwell.dispensationservice.models.http.requests.ProductSearchRequest;
import com.everwell.dispensationservice.models.http.responses.ProductResponse;

import java.util.List;

public interface ProductService {

    Product addProduct(ProductRequest productRequest);

    List<Product> addProductBulk(ProductBulkRequest productBulkRequest);

    void validateProductIds(List<Long> ids);

    Product getProduct(Long id);

    Product getProductByProductData(ProductRequest productRequest);

    List<ProductResponse> getAllProductDetails(ProductSearchRequest productSearchRequest, Long clientId);

    List<Product> getProducts(List<Long> ids);

    List<ProductResponse> getAllProductDetailsFromProducts(List<Product> products);

    Product formatCacheForProduct(String cacheEntry);

    List<Product> getProductsFromCache(List<Long>ids);

    List<ProductResponse> getAllProducts();

    Boolean deleteAllProductIdsCacheEntry();

}
