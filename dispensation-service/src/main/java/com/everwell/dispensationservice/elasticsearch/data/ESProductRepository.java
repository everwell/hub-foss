package com.everwell.dispensationservice.elasticsearch.data;

import com.everwell.dispensationservice.elasticsearch.indices.ProductIndex;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

public interface ESProductRepository extends ElasticsearchRepository<ProductIndex, String> {
}
