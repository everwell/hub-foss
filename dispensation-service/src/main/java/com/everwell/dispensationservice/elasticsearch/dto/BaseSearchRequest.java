package com.everwell.dispensationservice.elasticsearch.dto;

import lombok.*;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class BaseSearchRequest {
    SupportedSearchMethods search;
    int page = 0;
    int size = 9;
}
