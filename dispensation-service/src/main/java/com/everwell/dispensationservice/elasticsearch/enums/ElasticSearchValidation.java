package com.everwell.dispensationservice.elasticsearch.enums;


import lombok.Getter;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public enum ElasticSearchValidation {
    INVALID_SEARCH_KEY_AND_SEARCH_TERM_VALUE("Invalid search value ! search key and search should not be null"),
    INVALID_FUZZINESS_VALUE("Invalid fuzziness value ! Fuzziness value can be only 0, 1 and 2"),
    INVALID_SEARCH_OPERATOR_VALUE("Invalid operator value ! Operator value can only be (on, and)"),
    INVALID_PREFIX_LENGTH_VALUE("Invalid Prefix Length value");

    @Getter
    private  final String message;
}
