package com.everwell.dispensationservice.repositories;

import com.everwell.dispensationservice.models.db.ProductInventory;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface ProductInventoryRepository extends JpaRepository<ProductInventory, Long> {

    @Query(value = "select dsi.id from ds_product_inventory dsi where product_id = :productId and batch_number = :batchNumber",nativeQuery = true)
    Long findIdByProductIdAndBatchNumber(Long productId, String batchNumber);

    List<ProductInventory> findAllByIdIn(List<Long> ids);
}
