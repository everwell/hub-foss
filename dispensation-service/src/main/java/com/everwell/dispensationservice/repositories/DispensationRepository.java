package com.everwell.dispensationservice.repositories;

import com.everwell.dispensationservice.models.db.Dispensation;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Collection;
import java.util.List;
import java.util.Optional;

public interface DispensationRepository extends JpaRepository<Dispensation, Long> {

    List<Dispensation> findAllByEntityIdAndClientIdAndDeletedIsFalse(Long entityId, Long clientId);

    Optional<Dispensation> findByIdAndClientIdAndDeletedIsFalse(Long dispensationId, Long clientId);

    List<Dispensation> findAllByEntityIdInAndClientIdAndDeletedIsFalse(Collection<Long> entityIds, Long clientId);

}
