package com.everwell.dispensationservice.repositories;

import com.everwell.dispensationservice.models.db.ProductDispensationMapping;
import com.everwell.dispensationservice.models.dto.ProductDispensationMinDto;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Lock;
import org.springframework.data.jpa.repository.Query;

import javax.persistence.LockModeType;
import java.util.Collection;
import java.util.List;

public interface ProductDispensationMapRepository extends JpaRepository<ProductDispensationMapping, Long> {
    @Lock(LockModeType.PESSIMISTIC_WRITE)
    List<ProductDispensationMapping> findAllByDispensationIdIn(Collection<Long> ids);

    @Query(value = "select new " + "com.everwell.dispensationservice.models.dto.ProductDispensationMinDto(pdm.id,pdm.productId)" + "from ProductDispensationMapping as pdm where pdm.id in :ids")
    List<ProductDispensationMinDto> findByIdIn(Collection<Long> ids);

}
