package com.everwell.dispensationservice.models.dto;

import com.everwell.dispensationservice.Utils.Utils;
import com.everwell.dispensationservice.models.db.ProductInventoryHierarchyMapping;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.ToString;

import java.util.Date;

@Getter
@ToString
@AllArgsConstructor
public class ProductInventoryHierarchyMappingDto {

    private Long id;

    private Long inventoryId;

    private Long hierarchyMappingId;

    private Long clientId;

    private Long availableQuantity;

    private Long lastUpdatedBy;

    private Date lastUpdatedAt;

    public ProductInventoryHierarchyMappingDto(Long inventoryId, Long hierarchyMappingId, Long clientId, Long availableQuantity, Long lastUpdatedBy) {
        this.inventoryId = inventoryId;
        this.hierarchyMappingId = hierarchyMappingId;
        this.clientId = clientId;
        this.availableQuantity = availableQuantity;
        this.lastUpdatedBy = lastUpdatedBy;
        this.lastUpdatedAt = Utils.getCurrentDate();
    }

}
