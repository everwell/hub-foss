package com.everwell.dispensationservice.models.dto;

import lombok.*;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class HierarchyData{
    private Long hierarchyId;
    private Long quantity;
}
