package com.everwell.dispensationservice.models.dto.eventstreaming;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

@Data
@AllArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class EventDto {

    private String eventCategory;

    public String eventName;

    private String eventAction;

    private Map<String, Long> eventKeyValuePairs;

    private Long eventTime;

    private Long clientId;

    public EventDto(String eventCategory, String eventName, String eventAction) {
        this.eventCategory = eventCategory;
        this.eventName = eventName;
        this.eventAction = eventAction;
        this.eventTime = new Date().getTime();
    }

    public EventDto(String eventCategory, String eventName, String eventAction, Long clientId) {
        this(eventCategory,eventName,eventAction, null,  null, clientId);
    }

    public EventDto(String eventCategory, String eventName, String eventAction, Map<String, Long> eventKeyValuePairs, Long clientId) {
        this(eventCategory, eventName, eventAction);
        this.eventKeyValuePairs = eventKeyValuePairs;
        this.clientId = clientId;
    }
}