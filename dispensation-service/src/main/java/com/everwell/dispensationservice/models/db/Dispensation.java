package com.everwell.dispensationservice.models.db;

import java.util.Date;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

import com.everwell.dispensationservice.Utils.Utils;
import com.everwell.dispensationservice.enums.DispensationStatus;
import com.fasterxml.jackson.annotation.JsonFormat;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Table(name = "DS_Dispensation")
@AllArgsConstructor
@NoArgsConstructor
@Getter
public class Dispensation {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    @Column
    @Setter
    private Long entityId;

    @Column
    private Long clientId;

    @NotNull
    @Column
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy HH:mm:ss")
    private Date addedDate;

    @NotNull
    @Column
    private Long addedBy;

    @Column
    @Setter
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy HH:mm:ss")
    private Date dateOfPrescription;

    @Column
    private Long weight;

    @Column
    private String weightBand;

    @Column
    private String phase;

    @Column
    private Long drugDispensedForDays;

    @Column
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy HH:mm:ss")
    @Setter
    private Date refillDate;

    @Column
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy HH:mm:ss")
    @Setter
    private Date issueDate;

    @Column
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy HH:mm:ss")
    @Setter
    private Date dosingStartDate;

    @Column
    private Long issuingFacility;

    @Column
    @Setter
    private boolean deleted;

    @Column(length=1000)
    private String notes;

    @Setter
    @Column
    private DispensationStatus status;

    public Dispensation(Long entityId, Long clientId, Long addedBy, Long weight, String weightBand, String phase, Long drugDispensedForDays, Long issuingFacility, String notes, DispensationStatus status){
        this.entityId = entityId;
        this.clientId = clientId;
        this.addedBy = addedBy;
        this.weight = weight;
        this.phase = phase;
        this.drugDispensedForDays = drugDispensedForDays;
        this.addedDate = Utils.getCurrentDate();
        this.weightBand = weightBand;
        this.issueDate = Utils.getCurrentDate();
        this.issuingFacility = issuingFacility;
        this.notes = notes;
        this.status = status;
    }

    public Dispensation(Long id, Long entityId, Long clientId, Long addedBy, Long weight, String weightBand, String phase, Long drugDispensedForDays, Long issuingFacility, Date issueDate, Date dosingStartDate, Date addedDate, String notes, DispensationStatus status) {
        this(entityId, clientId, addedBy, weight, weightBand, phase, drugDispensedForDays, issuingFacility, notes, status);
        this.id = id;
        this.issueDate = issueDate;
        this.addedDate = addedDate;
        this.dosingStartDate = dosingStartDate;
    }

}