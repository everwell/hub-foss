package com.everwell.dispensationservice.models.dto;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class ProductDispensationMinDto {
    private Long id;
    private Long productId;
}
