package com.everwell.dispensationservice.models.dto;

import com.everwell.dispensationservice.enums.DispensationStatus;
import com.everwell.dispensationservice.models.db.Dispensation;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.*;

import java.util.Date;
import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@ToString
public class DispensationData {

    private Long dispensationId;

    private Long entityId;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy HH:mm:ss")
    private Date addedDate;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy HH:mm:ss")
    private Date dateOfPrescription;

    private Long weight;

    private String weightBand;

    private String phase;

    private Long drugDispensedForDays;

    private DispensationStatus status;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy HH:mm:ss")
    private Date issuedDate;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy HH:mm:ss")
    private Date dosingStartDate;

    private Long issuingFacility;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy HH:mm:ss")
    private Date refillDate;

    private Boolean deleted;

    private List<DispensationProductDto> productList;

    private String notes;

    public DispensationData(Dispensation dispensation, List<DispensationProductDto> productDataList) {
        this.dispensationId = dispensation.getId();
        this.addedDate = dispensation.getAddedDate();
        this.dateOfPrescription = dispensation.getDateOfPrescription();
        this.weight = dispensation.getWeight();
        this.weightBand = dispensation.getWeightBand();
        this.phase = dispensation.getPhase();
        this.drugDispensedForDays = dispensation.getDrugDispensedForDays();
        this.issuedDate = dispensation.getIssueDate();
        this.dosingStartDate = dispensation.getDosingStartDate();
        this.issuingFacility = dispensation.getIssuingFacility();
        this.refillDate = dispensation.getRefillDate();
        this.deleted = dispensation.isDeleted();
        this.productList = productDataList;
        this.entityId = dispensation.getEntityId();
        this.notes = dispensation.getNotes();
        this.status = dispensation.getStatus();
    }
}
