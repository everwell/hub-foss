package com.everwell.dispensationservice.models.dto;

import com.everwell.dispensationservice.Utils.Utils;
import com.everwell.dispensationservice.enums.RangeFilterType;
import com.everwell.dispensationservice.exceptions.ValidationException;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.apache.commons.lang3.StringUtils;

import java.text.ParseException;

@Getter
@AllArgsConstructor
@NoArgsConstructor
public class RangeValueFilter {

    private RangeFilterType type;

    private String from;

    private String to;

    private Long value;

    public void validate() {
        boolean validFromDate = false;
        boolean validToDate = false;
        try{
            if(!StringUtils.isEmpty(from)) {
                Utils.convertStringToDate(from);
                validFromDate = true;
            }
            if(!StringUtils.isEmpty(to)) {
                Utils.convertStringToDate(to);
                validToDate = true;
            }
        } catch (ParseException ex) {
            String validationMessage = !validFromDate ? "invalid from date format" : !validToDate?  "invalid to date format" : "";
            throw new ValidationException(validationMessage);
        }
    }

    public RangeValueFilter(RangeFilterType type, String from, String to) {
        this.type = type;
        this.from = from;
        this.to = to;
    }

    public RangeValueFilter(RangeFilterType type, Long value) {
        this.type = type;
        this.value = value;
    }
}
