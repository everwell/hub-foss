package com.everwell.dispensationservice.models.db;

import java.util.Date;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

import com.everwell.dispensationservice.Utils.Utils;
import com.fasterxml.jackson.annotation.JsonFormat;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Table(name = "DS_ProductDispensationMapping")
@AllArgsConstructor
@NoArgsConstructor
@Getter
public class ProductDispensationMapping {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    @Column
    private Long productId;

    @NotNull
    @Column
    private Long productConfigId;


    @NotNull
    @Column
    private Long dispensationId;

    @Setter
    @Column
    private Long numberOfUnitsIssued;

    @Column
    private String dosingSchedule;
    
    @Column
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy HH:mm:ss")
    @Setter
    private Date refillDate;

    public ProductDispensationMapping(Long productId, Long productConfigId, Long dispensationId, Long numberOfUnitsIssued, String dosingSchedule){
        this.productId = productId;
        this.productConfigId = productConfigId;
        this.dispensationId = dispensationId;
        this.numberOfUnitsIssued = numberOfUnitsIssued;
        this.dosingSchedule = dosingSchedule;
        this.refillDate = Utils.getCurrentDate();
    }

    public ProductDispensationMapping(Long id, Long productId, Long productConfigId, Long dispensationId, Long numberOfUnitsIssued, String dosingSchedule) {
        this(productId, productConfigId, dispensationId, numberOfUnitsIssued, dosingSchedule);
        this.id = id;
    }
}