package com.everwell.dispensationservice.models.db;

import com.everwell.dispensationservice.Utils.Utils;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.Date;

@Entity
@Table(name = "ds_product_inventory_hierarchy_mapping")
@Getter
@AllArgsConstructor
@NoArgsConstructor
public class ProductInventoryHierarchyMapping {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    @Column(name = "inventory_id")
    private Long inventoryId;

    @NotNull
    @Column(name = "hierarchy_mapping_id")
    private Long hierarchyMappingId;

    @NotNull
    @Column(name = "client_id")
    private Long clientId;

    @Column(name = "available_quantity")
    @Setter
    private Long availableQuantity;

    @Column(name = "last_updated_by")
    @Setter
    private Long lastUpdatedBy;

    @Column(name = "last_updated_at")
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy HH:mm:ss")
    @Setter
    private Date lastUpdatedAt;

    public ProductInventoryHierarchyMapping(Long inventoryId, Long hierarchyMappingId, Long clientId, Long availableQuantity, Long lastUpdatedBy){
        this.inventoryId = inventoryId;
        this.hierarchyMappingId = hierarchyMappingId;
        this.clientId = clientId;
        this.availableQuantity = availableQuantity;
        this.lastUpdatedBy = lastUpdatedBy;
        this.lastUpdatedAt = Utils.getCurrentDate();
    }

    public ProductInventoryHierarchyMapping(Long id, Long inventoryId, Long hierarchyMappingId, Long clientId, Long availableQuantity, Long lastUpdatedBy){
        this(inventoryId, hierarchyMappingId, clientId, availableQuantity, lastUpdatedBy);
        this.id = id;
    }
}
