package com.everwell.dispensationservice.models.http.requests;

import com.everwell.dispensationservice.exceptions.ValidationException;
import com.everwell.dispensationservice.models.http.responses.ProductResponse;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.ToString;
import org.apache.commons.lang3.StringUtils;
import org.springframework.util.CollectionUtils;

import java.util.List;

@Getter
@ToString
@AllArgsConstructor
@NoArgsConstructor
public class ProductBulkRequest {
    private List<ProductRequest> productDetails;

    public void validate() {
        if(CollectionUtils.isEmpty(productDetails)) {
            throw new ValidationException("product details is required");
        }
    }
}
