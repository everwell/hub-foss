package com.everwell.dispensationservice.models.http.responses;

import com.everwell.dispensationservice.models.db.Dispensation;
import com.everwell.dispensationservice.models.dto.DispensationData;
import com.everwell.dispensationservice.models.dto.DispensationProductDto;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.*;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@ToString
public class EntityDispensationResponse implements Serializable {

    private Long entityId;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy HH:mm:ss")
    private Date refillDate;

    private Long totalDispensedDrugs;

    private List<DispensationData> dispensations;
}
