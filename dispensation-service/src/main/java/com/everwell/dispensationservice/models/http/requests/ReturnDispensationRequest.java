package com.everwell.dispensationservice.models.http.requests;

import com.everwell.dispensationservice.Utils.Utils;
import com.everwell.dispensationservice.constants.Constants;
import com.everwell.dispensationservice.exceptions.ValidationException;
import lombok.*;
import org.apache.commons.lang3.StringUtils;

import java.text.ParseException;
import java.util.Date;
import java.util.List;

@Getter
@Setter
@ToString
@AllArgsConstructor
@NoArgsConstructor
public class ReturnDispensationRequest {
    private Long dispensationId;
    private List<ReturnDispensationData> returnDispensationProductDetails;

    @Data
    @AllArgsConstructor
    @NoArgsConstructor
    public static class ReturnDispensationData {

        Long transactionId;
        Long returnQuantity;
        String reasonForReturn;
        Long updatedBy;
        String batchNumber;
        String expiryDate;
        Long pInventoryId;

        public ReturnDispensationData(Long transactionId, Long returnQuantity,
                                      String reasonForReturn, Long updatedBy) {
            this.transactionId = transactionId;
            this.returnQuantity = returnQuantity;
            this.reasonForReturn = reasonForReturn;
            this.updatedBy = updatedBy;
        }
    }

    public void validate() {

        if(null == getDispensationId()) {
            throw new ValidationException("dispensation id is required");
        }

        if(null == getReturnDispensationProductDetails() || getReturnDispensationProductDetails().isEmpty()) {
            throw new ValidationException("return dispensation data is required");
        } else {
            for (ReturnDispensationData returnDispensationData : getReturnDispensationProductDetails()) {

                if(null == returnDispensationData.getTransactionId()) {
                    throw new ValidationException("transaction id is required");
                } else if(null == returnDispensationData.getReturnQuantity()) {
                    throw new ValidationException("number of units is required");
                } else if(null == returnDispensationData.getReasonForReturn()) {
                    throw new ValidationException("reason for return is required");
                } else if(!StringUtils.isBlank(returnDispensationData.getExpiryDate())) {
                    try {
                        Utils.convertStringToDate(returnDispensationData.getExpiryDate());
                    } catch (ParseException ex){
                        throw  new ValidationException("expiry date format is not valid");
                    }
                }
            }
        }
    }
}
