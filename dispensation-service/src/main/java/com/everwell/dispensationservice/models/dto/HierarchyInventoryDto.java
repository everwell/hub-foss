package com.everwell.dispensationservice.models.dto;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class HierarchyInventoryDto {
    private Long inventoryId;
    private Long hierarchyId;
    private Long availableQuantity;
}
