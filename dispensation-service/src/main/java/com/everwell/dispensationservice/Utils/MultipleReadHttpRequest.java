package com.everwell.dispensationservice.Utils;

import lombok.SneakyThrows;
import org.apache.commons.io.IOUtils;

import javax.servlet.ReadListener;
import javax.servlet.ServletInputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletRequestWrapper;
import java.io.*;

public class MultipleReadHttpRequest extends HttpServletRequestWrapper {

    private byte[] cacheData;

    public MultipleReadHttpRequest(HttpServletRequest request) throws IOException {
        super(request);
        InputStream inputStream = request.getInputStream();
        cacheData = IOUtils.toByteArray(inputStream);
    }


    @Override
    public ServletInputStream getInputStream() throws IOException {
        // Create input stream from cachedContent and return it
        return new CachedBodyServletInputStream(this.cacheData);

    }

    @Override
    public BufferedReader getReader() throws IOException {
        // Create a reader from cachedContent and return it
        ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(this.cacheData);
        return new BufferedReader(new InputStreamReader(byteArrayInputStream));
    }

    private class CachedBodyServletInputStream extends ServletInputStream {

        private final InputStream cachedBodyInputStream;

        public CachedBodyServletInputStream(byte[] cachedBody) {
            this.cachedBodyInputStream = new ByteArrayInputStream(cachedBody);
        }

        @SneakyThrows
        @Override
        public boolean isFinished() {
            return cachedBodyInputStream.available()==0;
        }

        @Override
        public boolean isReady() {
            return true;
        }

        @Override
        public void setReadListener(ReadListener readListener) {

        }

        @Override
        public int read() throws IOException {
            return cachedBodyInputStream.read();
        }
    }
}
