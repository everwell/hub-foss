package com.everwell.dispensationservice.enums.event;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum EventCategoryEnum {
    DISPENSATION("Dispensation"),
    PRODUCT("Product"),
    CLIENT("Client"),
    STOCK("Stock"),
    ENTITY("Entity");

    private String name;
}