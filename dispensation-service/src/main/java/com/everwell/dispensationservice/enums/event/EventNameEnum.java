package com.everwell.dispensationservice.enums.event;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum EventNameEnum {
    ADD_DISPENSATION("Add dispensation"),
    GET_DISPENSATION("Get dispensation"),
    GET_ENTITY_DISPENSATIONS("Get dispensations for entity"),
    DELETE_DISPENSATION("Delete dispensation"),
    ADD_PRODUCT("Add product"),
    ADD_PRODUCT_BULK("Add product - Bulk"),
    GET_PRODUCT("Get product details"),
    GET_PRODUCT_BULK("Get product details - Bulk"),
    GET_ALL_PRODUCT("Get all product details"),
    SEARCH_ENTITY_DISPENSATIONS("Search Multiple Entity Dispensation"),
    ADD_CLIENT("Add client"),
    GET_CLIENT("Get client"),
    RETURN_ENTITY_DISPENSATIONS("Return dispensation for entity"),
    GET_DISPENSATION_BULK("Get dispensation details - Bulk"),
    STOCK_CREDIT("Stock Credit"),
    STOCK_DEBIT("Stock Debit"),
    STOCK_SEARCH("Stock Search"),
    STOCK_UPDATE("Stock Update"),
    SHARE_DISPENSATION("Share Dispensation"),
    GET_PRODUCT_BULK_SEARCH_FROM_ELASTIC("Get product details - Bulk from Elastic Search");
    private String name;
}