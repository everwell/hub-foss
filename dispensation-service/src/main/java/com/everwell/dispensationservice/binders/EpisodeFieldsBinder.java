package com.everwell.dispensationservice.binders;

import org.springframework.cloud.stream.annotation.Output;
import org.springframework.messaging.MessageChannel;

public interface EpisodeFieldsBinder {
    String EPISODE_FIELD_TRACKER_EXCHANGE = "ex.episode.update_tracker";

    @Output(EPISODE_FIELD_TRACKER_EXCHANGE)
    MessageChannel episodeTrackerPublisher();
}
