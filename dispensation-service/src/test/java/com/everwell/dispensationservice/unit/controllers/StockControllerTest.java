package com.everwell.dispensationservice.unit.controllers;

import com.everwell.dispensationservice.Utils.Utils;
import com.everwell.dispensationservice.controllers.StockController;
import com.everwell.dispensationservice.enums.CommentType;
import com.everwell.dispensationservice.enums.TransactionIdType;
import com.everwell.dispensationservice.enums.TransactionType;
import com.everwell.dispensationservice.exceptions.CustomExceptionHandler;
import com.everwell.dispensationservice.models.db.Product;
import com.everwell.dispensationservice.models.db.ProductInventoryLog;
import com.everwell.dispensationservice.models.dto.HierarchyData;
import com.everwell.dispensationservice.models.dto.ProductStockData;
import com.everwell.dispensationservice.models.http.requests.ProductStockSearchRequest;
import com.everwell.dispensationservice.models.http.requests.StockBulkRequest;
import com.everwell.dispensationservice.models.http.requests.StockRequest;
import com.everwell.dispensationservice.repositories.ProductInventoryLogRepository;
import com.everwell.dispensationservice.services.ProductService;
import com.everwell.dispensationservice.services.StockService;
import com.everwell.dispensationservice.unit.BaseTestNoSpring;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static com.everwell.dispensationservice.Utils.Utils.asJsonString;
import static com.everwell.dispensationservice.constants.Constants.X_DS_CLIENT_ID;
import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

public class StockControllerTest extends BaseTestNoSpring {

    private MockMvc mockMvc;

    @Spy
    @InjectMocks
    private StockController stockController;

    @Mock
    private StockService stockService;

    @Mock
    private ProductService productService;

    @MockBean
    ProductInventoryLogRepository productInventoryLogRepository;

    @Mock
    private ApplicationEventPublisher applicationEventPublisher;

    private final String CLIENT_ID = "1";

    @BeforeEach
    public void setUp() {
        mockMvc = MockMvcBuilders
                .standaloneSetup(stockController)
                .setControllerAdvice(new CustomExceptionHandler())
                .build();
    }

    private ProductInventoryLog product_inventory_log_credit_entity(){
        return new ProductInventoryLog(1L, TransactionType.CREDIT, 12345L, 100L, CommentType.REASON_FOR_CREDIT, "Test", 1L, 1L, 1L, TransactionIdType.EXTERNAL);
    }

    private ProductInventoryLog product_inventory_log_debit_entity(){
        return new ProductInventoryLog(1L, TransactionType.DEBIT, 12345L, 100L, CommentType.REASON_FOR_DEBIT, "Test", 1L, 1L, 1L, TransactionIdType.EXTERNAL);
    }


    @Test
    public  void test_credit_stock() throws  Exception {
        long clientId = 1L;
        StockRequest stockRequest = new StockRequest( 111L, 1L, "ABC123", "10-11-2025 00:00:00", 100L,12345L, 1L, "Test" );
        Product product = new Product(1L, "dc1", "Product1", "dose1", "df1", "manufacturer1", "composition1", "category");
        when(stockService.setStockData(any(), any(), any())).thenReturn(product_inventory_log_credit_entity());
        when(productService.getProduct(any())).thenReturn(product);
        mockMvc
                .perform(post("/v1/stock")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(asJsonString(stockRequest))
                        .header(X_DS_CLIENT_ID, Long.toString(clientId)))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.message").value("stock credited successfully"));
    }

    @Test
    public  void test_debit_stock() throws  Exception {
        long clientId = 1L;
        StockRequest stockRequest = new StockRequest( 111L, 1L, "ABC123", "10-11-2025 00:00:00", 100L,12345L, 1L, "Test"  );
        when(stockService.setStockData(any(), any(), any())).thenReturn(product_inventory_log_debit_entity());
        mockMvc
                .perform(put("/v1/stock")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(asJsonString(stockRequest))
                        .header(X_DS_CLIENT_ID, Long.toString(clientId)))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.message").value("stock debited successfully"));
    }

    @Test
    public void test_product_stock_search() throws Exception {
        long clientId = 1L;
        List<Long> productIds = new ArrayList<>();
        productIds.add(1L);
        List<Long> hierarchyMappingIds = new ArrayList<>();
        hierarchyMappingIds.add(1L);
        ProductStockSearchRequest productStockSearchRequest = new ProductStockSearchRequest(productIds, hierarchyMappingIds);
        List<ProductStockData> productStockSearchResponse = new ArrayList<>();
        List<HierarchyData> hierarchyData= new ArrayList<>();
        hierarchyData.add(new HierarchyData(1L, 1000L));
        productStockSearchResponse.add(new ProductStockData(1L, "ABC", Utils.convertStringToDateOrDefault("10-11-2025 00:00:00"), hierarchyData));
        when(stockService.getProductStockData(any())).thenReturn(productStockSearchResponse);

        mockMvc
                .perform(post("/v1/product/stocks/search")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(asJsonString(productStockSearchRequest))
                        .header(X_DS_CLIENT_ID, Long.toString(clientId)))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.message").value("product stock details fetched successfully"));
    }

    @Test
    public  void test_credit_stock_with_null_hierarchy_id_throws_validation_exception() throws  Exception {
        long clientId = 1L;
        StockRequest stockRequest = new StockRequest( null, 1L, "ABC123", "10-11-2025 00:00:00", 100L,12345L, 1L , "Test" );
        when(stockService.setStockData(any(), any(), any())).thenReturn(product_inventory_log_credit_entity());
        mockMvc
                .perform(post("/v1/stock")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(asJsonString(stockRequest))
                        .header(X_DS_CLIENT_ID, Long.toString(clientId)))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.message").value("hierarchy mapping Id is required"));
    }

    @Test
    public  void test_credit_stock_with_null_product_id_throws_validation_exception() throws  Exception {
        long clientId = 1L;
        StockRequest stockRequest = new StockRequest( 111L, null, "ABC123", "10-11-2025 00:00:00", 100L,12345L, 1L , "Test" );
        when(stockService.setStockData(any(), any(), any())).thenReturn(product_inventory_log_credit_entity());
        mockMvc
                .perform(post("/v1/stock")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(asJsonString(stockRequest))
                        .header(X_DS_CLIENT_ID, Long.toString(clientId)))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.message").value("product Id is required"));
    }

    @Test
    public  void test_credit_stock_with_null_transaction_id_throws_validation_exception() throws  Exception {
        long clientId = 1L;
        StockRequest stockRequest = new StockRequest( 111L, 1L, "ABC123", "10-11-2025 00:00:00", 100L,null, 1L , "Test" );
        when(stockService.setStockData(any(), any(), any())).thenReturn(product_inventory_log_credit_entity());
        mockMvc
                .perform(post("/v1/stock")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(asJsonString(stockRequest))
                        .header(X_DS_CLIENT_ID, Long.toString(clientId)))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.message").value("transaction Id is required"));
    }

    @Test
    public  void test_credit_stock_with_null_batch_number_throws_validation_exception() throws  Exception {
        long clientId = 1L;
        StockRequest stockRequest = new StockRequest(111L, 1L, null, "10-11-2025 00:00:00", 100L,12345L, 1L , "Test" );
        when(stockService.setStockData(any(), any(), any())).thenReturn(product_inventory_log_credit_entity());
        mockMvc
                .perform(post("/v1/stock")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(asJsonString(stockRequest))
                        .header(X_DS_CLIENT_ID, Long.toString(clientId)))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.message").value("batch number is required"));
    }
    
    @Test
    public  void test_credit_stock_with_null_expiry_date_throws_validation_exception() throws  Exception {
        long clientId = 1L;
        StockRequest stockRequest = new StockRequest( 111L, 1L, "ABC123", null, 100L,12345L, 1L , "Test" );
        when(stockService.setStockData(any(), any(), any())).thenReturn(product_inventory_log_credit_entity());
        mockMvc
                .perform(post("/v1/stock")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(asJsonString(stockRequest))
                        .header(X_DS_CLIENT_ID, Long.toString(clientId)))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.message").value("expiry date  is required"));
    }

    @Test
    public  void test_credit_stock_with_null_quantity_throws_validation_exception() throws  Exception {
        long clientId = 1L;
        StockRequest stockRequest = new StockRequest(111L, 1L, "ABC123", "10-11-2025 00:00:00", null,12345L, 1L , "Test" );
        when(stockService.setStockData(any(), any(), any())).thenReturn(product_inventory_log_credit_entity());
        mockMvc
                .perform(post("/v1/stock")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(asJsonString(stockRequest))
                        .header(X_DS_CLIENT_ID, Long.toString(clientId)))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.message").value("quantity is required"));
    }

    @Test
    public  void test_credit_stock_with_negative_quantity_throws_validation_exception() throws  Exception {
        long clientId = 1L;
        StockRequest stockRequest = new StockRequest(111L, 1L, "ABC123", "10-11-2025 00:00:00", -100L,12345L, 1L , "Test" );
        when(stockService.setStockData(any(), any(), any())).thenReturn(product_inventory_log_credit_entity());
        mockMvc
                .perform(post("/v1/stock")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(asJsonString(stockRequest))
                        .header(X_DS_CLIENT_ID, Long.toString(clientId)))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.message").value("quantity can not be less than zero"));
    }

    @Test
    public  void test_credit_stock_with_invalid_expiry_date_format_throws_validation_exception() throws  Exception {
        long clientId = 1L;
        StockRequest stockRequest = new StockRequest( 111L, 1L, "ABC123", "10-11-2025 00.00.00", 100L,12345L, 1L , "Test" );
        when(stockService.setStockData(any(), any(), any())).thenReturn(product_inventory_log_credit_entity());
        mockMvc
                .perform(post("/v1/stock")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(asJsonString(stockRequest))
                        .header(X_DS_CLIENT_ID, Long.toString(clientId)))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.message").value("expiry date format is not valid"));
    }

    @Test
    public  void test_credit_stock_with_null_last_updated_by_format_throws_validation_exception() throws  Exception {
        long clientId = 1L;
        StockRequest stockRequest = new StockRequest( 111L, 1L, "ABC123", "10-11-2025 00.00.00", 100L,12345L, null , "Test" );
        when(stockService.setStockData(any(), any(), any())).thenReturn(product_inventory_log_credit_entity());
        mockMvc
                .perform(post("/v1/stock")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(asJsonString(stockRequest))
                        .header(X_DS_CLIENT_ID, Long.toString(clientId)))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.message").value("last updated by is required"));
    }

    @Test
    public void test_product_stock_search_null_product_id() throws Exception {
        long clientId = 1L;
        List<Long> hierarchyMappingIds = new ArrayList<>();
        hierarchyMappingIds.add(1L);
        ProductStockSearchRequest productStockSearchRequest = new ProductStockSearchRequest(null, hierarchyMappingIds);
        List<ProductStockData> productStockSearchResponse = new ArrayList<>();
        List<HierarchyData> hierarchyData= new ArrayList<>();
        hierarchyData.add(new HierarchyData(1L, 1000L));
        productStockSearchResponse.add(new ProductStockData(1L, "ABC", Utils.convertStringToDateOrDefault("10-11-2025 00:00:00"), hierarchyData));
        when(stockService.getProductStockData(any())).thenReturn(productStockSearchResponse);

        mockMvc
                .perform(post("/v1/product/stocks/search")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(asJsonString(productStockSearchRequest))
                        .header(X_DS_CLIENT_ID, Long.toString(clientId)))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.message").value("product id is required"));
    }

    @Test
    public void test_product_stock_search_null_hierarchy_id() throws Exception {
        long clientId = 1L;
        List<Long> productIds = new ArrayList<>();
        productIds.add(1L);
        ProductStockSearchRequest productStockSearchRequest = new ProductStockSearchRequest(productIds, null);
        List<ProductStockData> productStockSearchResponse = new ArrayList<>();
        List<HierarchyData> hierarchyData= new ArrayList<>();
        hierarchyData.add(new HierarchyData(1L, 1000L));
        productStockSearchResponse.add(new ProductStockData(1L, "ABC", Utils.convertStringToDateOrDefault("10-11-2025 00:00:00"), hierarchyData));
        when(stockService.getProductStockData(any())).thenReturn(productStockSearchResponse);

        mockMvc
                .perform(post("/v1/product/stocks/search")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(asJsonString(productStockSearchRequest))
                        .header(X_DS_CLIENT_ID, Long.toString(clientId)))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.message").value("hierarchy mapping id is required"));
    }

    @Test
    public void test_product_stock_search_one_null_product_id() throws Exception {
        long clientId = 1L;
        List<Long> productIds = new ArrayList<>();
        productIds.add(1L);
        productIds.add(null);
        List<Long> hierarchyMappingIds = new ArrayList<>();
        hierarchyMappingIds.add(1L);
        ProductStockSearchRequest productStockSearchRequest = new ProductStockSearchRequest(productIds, hierarchyMappingIds);
        List<ProductStockData> productStockSearchResponse = new ArrayList<>();
        List<HierarchyData> hierarchyData= new ArrayList<>();
        hierarchyData.add(new HierarchyData(1L, 1000L));
        productStockSearchResponse.add(new ProductStockData(1L, "ABC", Utils.convertStringToDateOrDefault("10-11-2025 00:00:00"), hierarchyData));
        when(stockService.getProductStockData(any())).thenReturn(productStockSearchResponse);

        mockMvc
                .perform(post("/v1/product/stocks/search")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(asJsonString(productStockSearchRequest))
                        .header(X_DS_CLIENT_ID, Long.toString(clientId)))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.message").value("product id cannot be null"));
    }

    @Test
    public void test_product_stock_search_one_null_hierarchy_id() throws Exception {
        long clientId = 1L;
        List<Long> productIds = new ArrayList<>();
        productIds.add(1L);
        List<Long> hierarchyMappingIds = new ArrayList<>();
        hierarchyMappingIds.add(1L);
        hierarchyMappingIds.add(null);
        ProductStockSearchRequest productStockSearchRequest = new ProductStockSearchRequest(productIds, hierarchyMappingIds);
        List<ProductStockData> productStockSearchResponse = new ArrayList<>();
        List<HierarchyData> hierarchyData= new ArrayList<>();
        hierarchyData.add(new HierarchyData(1L, 1000L));
        productStockSearchResponse.add(new ProductStockData(1L, "ABC", Utils.convertStringToDateOrDefault("10-11-2025 00:00:00"), hierarchyData));
        when(stockService.getProductStockData(any())).thenReturn(productStockSearchResponse);

        mockMvc
                .perform(post("/v1/product/stocks/search")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(asJsonString(productStockSearchRequest))
                        .header(X_DS_CLIENT_ID, Long.toString(clientId)))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.message").value("hierarchy mapping id cannot be null"));
    }

    @Test
    public  void test_update_stock() throws  Exception {
        long clientId = 1L;
        StockRequest stockRequest = new StockRequest( 111L, 1L, "ABC123", "10-11-2025 00:00:00", 100L,12345L, 1L, "Test" );
        StockBulkRequest stockBulkRequest = new StockBulkRequest(Collections.singletonList(stockRequest));
        Product product = new Product(1L, "dc1", "Product1", "dose1", "df1", "manufacturer1", "composition1", "category");
        when(stockService.setStockData(any(), any(), any())).thenReturn(product_inventory_log_credit_entity());
        when(productService.getProduct(any())).thenReturn(product);
        mockMvc
                .perform(put("/v1/stocks")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(asJsonString(stockBulkRequest))
                        .header(X_DS_CLIENT_ID, Long.toString(clientId)))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.message").value("stock updated successfully"));
    }

}
