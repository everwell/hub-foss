package com.everwell.dispensationservice.unit.services;

import com.everwell.dispensationservice.Utils.Utils;
import com.everwell.dispensationservice.constants.Constants;
import com.everwell.dispensationservice.enums.CommentType;
import com.everwell.dispensationservice.enums.TransactionIdType;
import com.everwell.dispensationservice.enums.TransactionType;
import com.everwell.dispensationservice.exceptions.ValidationException;
import com.everwell.dispensationservice.models.db.Product;
import com.everwell.dispensationservice.models.db.ProductInventory;
import com.everwell.dispensationservice.models.db.ProductInventoryHierarchyMapping;
import com.everwell.dispensationservice.models.db.ProductInventoryLog;
import com.everwell.dispensationservice.models.dto.*;
import com.everwell.dispensationservice.models.http.requests.ProductStockSearchRequest;
import com.everwell.dispensationservice.models.http.requests.StockRequest;
import com.everwell.dispensationservice.repositories.ProductInventoryHierarchyMappingRepository;
import com.everwell.dispensationservice.repositories.ProductInventoryLogRepository;
import com.everwell.dispensationservice.repositories.ProductInventoryRepository;
import com.everwell.dispensationservice.services.ProductService;
import com.everwell.dispensationservice.services.impl.StockServiceImpl;
import com.everwell.dispensationservice.unit.BaseTestNoSpring;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;
import org.springframework.boot.test.mock.mockito.MockBean;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.stream.Collectors;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.*;


public class StockServiceTest extends BaseTestNoSpring {

    @Spy
    @InjectMocks
    StockServiceImpl stockService;

    @MockBean
    ProductService productService;

    @MockBean
    ProductInventoryRepository productInventoryRepository;

    @MockBean
    ProductInventoryHierarchyMappingRepository productInventoryHierarchyMappingRepository;

    @MockBean
    ProductInventoryLogRepository productInventoryLogRepository;

    @BeforeEach
    public void initMocks() {
        MockitoAnnotations.initMocks(this);
    }


    private ProductInventory product_inventory_entity(){
        return new ProductInventory(1L, 1L, "ABC123", Utils.convertStringToDateOrDefault("10-11-2025 00:00:00"));
    }

    private List<ProductInventory> product_inventory_list(){
        List<ProductInventory> productInventoryList = new ArrayList<>();
        productInventoryList.add(new ProductInventory(1L, 1L, "ABC123", Utils.convertStringToDateOrDefault("10-11-2025 00:00:00")));
        productInventoryList.add(new ProductInventory(2L, 2L, "ABC456", Utils.convertStringToDateOrDefault("10-11-2025 00:00:00")));
        return productInventoryList;
    }

    private ProductInventoryHierarchyMapping product_inventory_hierarchy_mapping_entity(){
        return new ProductInventoryHierarchyMapping(1L, 1L, 111L, 1L, 1000L, 1L);
    }

    private List<ProductInventoryHierarchyMapping> product_inventory_hierarchy_mapping_list(){
        List<ProductInventoryHierarchyMapping> productInventoryHierarchyMappingList = new ArrayList<>();
        productInventoryHierarchyMappingList.add(new ProductInventoryHierarchyMapping(1L,1L, 111L, 1L, 1000L, 1L));
        productInventoryHierarchyMappingList.add(new ProductInventoryHierarchyMapping(2L,1L, 112L, 1L, 2000L, 1L));
        return productInventoryHierarchyMappingList;
    }

    private ProductInventoryLog product_inventory_log_credit_entity(){
        return new ProductInventoryLog(1L, TransactionType.CREDIT, 12345L, 100L, CommentType.REASON_FOR_CREDIT, "Test", 1L, 1L, 1L, TransactionIdType.EXTERNAL);
    }

    private ProductInventoryLog product_inventory_log_debit_entity(){
        return new ProductInventoryLog(1L, TransactionType.DEBIT, 12345L, 100L, CommentType.REASON_FOR_DEBIT, "Test", 1L, 1L, 1L, TransactionIdType.EXTERNAL);
    }

    private ProductInventoryLog product_inventory_log_issued_entity(){
        return new ProductInventoryLog(1L, TransactionType.ISSUED, 12345L, 100L, CommentType.REASON_FOR_ADD, "Test", 1L, 1L, 1L, TransactionIdType.INTERNAL);
    }

    private ProductInventoryLog product_inventory_log_return_entity(){
        return new ProductInventoryLog(1L, TransactionType.RETURNED, 12345L, 100L, CommentType.REASON_FOR_RETURN, "Test", 1L, 1L, 1L, TransactionIdType.INTERNAL);
    }

    @Test
    public void test_get_inventory_data() {
        when(productInventoryRepository.findIdByProductIdAndBatchNumber(any(), any())).thenReturn(1L);
        ProductInventoryDto productInventoryDto = new ProductInventoryDto(1L, "ABC123", Utils.convertStringToDateOrDefault("10-11-2025 00:00:00"));
        Long id = stockService.getInventoryData(productInventoryDto);
        assertEquals(id,1L);
        verify(productInventoryRepository, Mockito.times(1)).findIdByProductIdAndBatchNumber(any(), any());
    }

    @Test
    public void test_add_inventory_data() {
        ProductInventoryDto productInventoryDto = new ProductInventoryDto(1L, "ABC123", Utils.convertStringToDateOrDefault("10-11-2025 00:00:00"));
        when(productInventoryRepository.save(any())).thenReturn(product_inventory_entity());
        stockService.addInventoryData(productInventoryDto);
        verify(productInventoryRepository, Mockito.times(1)).save(any());
    }

    @Test
    public void test_get_inventory_hierarchy_data() {
        when(productInventoryHierarchyMappingRepository.findProductInventoryHierarchyMappingByInventoryIdAndHierarchyMappingIdAndClientId(any(), any(), any())).thenReturn(product_inventory_hierarchy_mapping_entity());
        ProductInventoryHierarchyMappingDto productInventoryHierarchyMappingDto = new ProductInventoryHierarchyMappingDto(1L, 111L, 1L, 1000L, 1L);
        ProductInventoryHierarchyMapping productInventoryHierarchyMapping = stockService.getInventoryHierarchyData(productInventoryHierarchyMappingDto);
        assertEquals(productInventoryHierarchyMapping.getInventoryId(), productInventoryHierarchyMappingDto.getInventoryId());
        assertEquals(productInventoryHierarchyMapping.getHierarchyMappingId(), productInventoryHierarchyMappingDto.getHierarchyMappingId());
        assertEquals(productInventoryHierarchyMapping.getClientId(), productInventoryHierarchyMappingDto.getClientId());
        verify(productInventoryHierarchyMappingRepository, Mockito.times(1)).findProductInventoryHierarchyMappingByInventoryIdAndHierarchyMappingIdAndClientId(any(), any(), any());
    }

    @Test
    public void test_add_inventory_hierarchy_data() {
        ProductInventoryHierarchyMappingDto productInventoryHierarchyMappingDto = new ProductInventoryHierarchyMappingDto(1L, 111L, 1L, 1000L, 1L);
        when(productInventoryHierarchyMappingRepository.save(any())).thenReturn(product_inventory_hierarchy_mapping_entity());
        stockService.addInventoryHierarchyData(productInventoryHierarchyMappingDto);
        verify(productInventoryHierarchyMappingRepository, Mockito.times(1)).save(any());
    }

    @Test
    public void test_update_inventory_hierarchy_data() {
        ProductInventoryHierarchyMapping productInventoryHierarchyMapping = product_inventory_hierarchy_mapping_entity();
        Long updatedQuantity = productInventoryHierarchyMapping.getAvailableQuantity() - 10L;
        when(productInventoryHierarchyMappingRepository.save(any())).thenReturn(productInventoryHierarchyMapping);
        stockService.updateInventoryHierarchyData(productInventoryHierarchyMapping, updatedQuantity, 1L);
        assertEquals(updatedQuantity, productInventoryHierarchyMapping.getAvailableQuantity());
        assertEquals(1L, productInventoryHierarchyMapping.getLastUpdatedBy());
        assertEquals(Utils.getCurrentDate(), productInventoryHierarchyMapping.getLastUpdatedAt());
        verify(productInventoryHierarchyMappingRepository, Mockito.times(1)).save(any());
    }

    @Test
    public void test_add_inventory_log() {
        ProductInventoryLogDto productInventoryLogDto =  new ProductInventoryLogDto(TransactionType.CREDIT, 1L, 2L, CommentType.REASON_FOR_CREDIT, Constants.CREDIT_COMMENT, 1L, 1L,1L, TransactionIdType.EXTERNAL);
        when(productInventoryLogRepository.save(any())).thenReturn(product_inventory_log_credit_entity());
        stockService.addInventoryLog(productInventoryLogDto);
        verify(productInventoryLogRepository, Mockito.times(1)).save(any());
    }

    @Test
    public void test_set_inventory_log_data_credit() {
        StockRequest stockRequest = new StockRequest( 111L, 1L, "ABC123", "10-11-2025 00:00:00", 100L,12345L, 1L, "Test" );
        when(productInventoryLogRepository.save(any())).thenReturn(product_inventory_log_credit_entity());
        stockService.setInventoryLogData(stockRequest, TransactionType.CREDIT, 1L );
        verify(productInventoryLogRepository, Mockito.times(1)).save(any());
    }

    @Test
    public void test_set_inventory_log_data_debit() {
        StockRequest stockRequest = new StockRequest( 111L, 1L, "ABC123", "10-11-2025 00:00:00", 100L,12345L, 1L, "Test" );
        when(productInventoryLogRepository.save(any())).thenReturn(product_inventory_log_debit_entity());
        stockService.setInventoryLogData(stockRequest, TransactionType.DEBIT, 1L );
        verify(productInventoryLogRepository, Mockito.times(1)).save(any());
    }

    @Test
    public void test_set_inventory_log_data_issued() {
        StockRequest stockRequest = new StockRequest( 111L, 1L, "ABC123", "10-11-2025 00:00:00", 100L,12345L, 1L, "Test" );
        when(productInventoryLogRepository.save(any())).thenReturn(product_inventory_log_issued_entity());
        stockService.setInventoryLogData(stockRequest, TransactionType.ISSUED, 1L );
        verify(productInventoryLogRepository, Mockito.times(1)).save(any());
    }

    @Test
    public void test_set_inventory_log_data_returned() {
        StockRequest stockRequest = new StockRequest( 111L, 1L, "ABC123", "10-11-2025 00:00:00", 100L,12345L, 1L, "Test" );
        when(productInventoryLogRepository.save(any())).thenReturn(product_inventory_log_return_entity());
        stockService.setInventoryLogData(stockRequest, TransactionType.RETURNED, 1L );
        verify(productInventoryLogRepository, Mockito.times(1)).save(any());
    }

    @Test
    public void test_set_inventory_log_data_empty() {
        StockRequest stockRequest = new StockRequest( 111L, 1L, "ABC123", "10-11-2025 00:00:00", 100L,12345L, 1L, "Test" );
        when(productInventoryLogRepository.save(any())).thenReturn(product_inventory_log_return_entity());
        stockService.setInventoryLogData(stockRequest, null, 1L );
        verify(productInventoryLogRepository, Mockito.times(0)).save(any());
    }

    @Test
    public void test_set_stock_data_debit_with_null_product_inventory_data() {
        StockRequest stockRequest = new StockRequest( 111L, 1L, "ABC123", "10-11-2025 00:00:00", 100L,12345L, 1L, "Test");
        when(productInventoryRepository.findIdByProductIdAndBatchNumber(any(),any())).thenReturn(null);
        Exception ex = assertThrows(ValidationException.class, () -> stockService.setStockData(stockRequest, TransactionType.DEBIT, 1L));
        assertEquals("product Id, batch number and expiry date does not exist in the inventory", ex.getMessage());
        verify(productInventoryRepository, Mockito.times(1)).findIdByProductIdAndBatchNumber(any(), any());
        verify(productInventoryRepository, Mockito.times(0)).save(any());
    }

    @Test
    public void test_set_stock_data_debit_with_null_product_inventory_hierarchy_data() {
        StockRequest stockRequest = new StockRequest(111L, 1L, "ABC123", "10-11-2025 00:00:00", 100L,12345L, 1L, "Test" );
        when(productInventoryRepository.findIdByProductIdAndBatchNumber(any(),any())).thenReturn(1L);
        when(productInventoryHierarchyMappingRepository.findProductInventoryHierarchyMappingByInventoryIdAndHierarchyMappingIdAndClientId(any(),any(),any())).thenReturn(null);
        Exception ex = assertThrows(ValidationException.class, () -> stockService.setStockData(stockRequest, TransactionType.DEBIT, 1L));
        assertEquals("hierarchy Id does not exist in the inventory", ex.getMessage());
        verify(productInventoryRepository, Mockito.times(1)).findIdByProductIdAndBatchNumber(any(), any());
        verify(productInventoryHierarchyMappingRepository, Mockito.times(1)).findProductInventoryHierarchyMappingByInventoryIdAndHierarchyMappingIdAndClientId(any(), any(), any());
        verify(productInventoryHierarchyMappingRepository, Mockito.times(0)).save(any());
    }

    @Test
    public void test_set_stock_data_debit_with_invalid_quantity() {
        StockRequest stockRequest = new StockRequest( 111L, 1L, "ABC123", "10-11-2025 00:00:00", 10000L,12345L, 1L, "Test" );
        Product product = new Product(1L, "dc1", "ProductA", "dose1", "df1", "manufacturer1", "composition1", "category");
        when(productInventoryRepository.findIdByProductIdAndBatchNumber(any(),any())).thenReturn(1L);
        when(productInventoryHierarchyMappingRepository.findProductInventoryHierarchyMappingByInventoryIdAndHierarchyMappingIdAndClientId(any(),any(),any())).thenReturn(product_inventory_hierarchy_mapping_entity());
        when(productService.getProduct(any())).thenReturn(product);
        Exception ex = assertThrows(ValidationException.class, () -> stockService.setStockData(stockRequest, TransactionType.DEBIT, 1L));
        assertEquals("Stock is currently not available for ProductA and batch number ABC123", ex.getMessage());
        verify(productInventoryRepository, Mockito.times(1)).findIdByProductIdAndBatchNumber(any(), any());
        verify(productInventoryHierarchyMappingRepository, Mockito.times(1)).findProductInventoryHierarchyMappingByInventoryIdAndHierarchyMappingIdAndClientId(any(), any(), any());
        verify(productInventoryHierarchyMappingRepository, Mockito.times(0)).save(any());
    }

    @Test
    public void test_set_stock_data_debit() {
        StockRequest stockRequest = new StockRequest( 111L, 1L, "ABC123", "10-11-2025 00:00:00", 100L,12345L, 1L, "Test" );
        when(productInventoryRepository.findIdByProductIdAndBatchNumber(any(),any())).thenReturn(1L);
        when(productInventoryHierarchyMappingRepository.findProductInventoryHierarchyMappingByInventoryIdAndHierarchyMappingIdAndClientId(any(),any(),any())).thenReturn(product_inventory_hierarchy_mapping_entity());
        when(productInventoryLogRepository.save(any())).thenReturn(product_inventory_log_debit_entity());
        ProductInventoryLog productInventoryLog = stockService.setStockData(stockRequest, TransactionType.DEBIT, 1L);
        assertEquals(stockRequest.getQuantity(), productInventoryLog.getTransactionQuantity());
        assertEquals(stockRequest.getProductId(), productInventoryLog.getProductId());
        assertEquals(stockRequest.getLastUpdatedBy(), productInventoryLog.getUpdatedBy());
        assertEquals(TransactionType.DEBIT, productInventoryLog.getTransactionType());
        verify(productInventoryRepository, Mockito.times(1)).findIdByProductIdAndBatchNumber(any(), any());
        verify(productInventoryHierarchyMappingRepository, Mockito.times(1)).findProductInventoryHierarchyMappingByInventoryIdAndHierarchyMappingIdAndClientId(any(), any(), any());
        verify(productInventoryHierarchyMappingRepository, Mockito.times(1)).save(any());
        verify(productInventoryLogRepository, Mockito.times(1)).save(any());
    }

    @Test
    public void test_set_stock_data_credit() {
        StockRequest stockRequest = new StockRequest(111L, 1L, "ABC123", "10-11-2025 00:00:00", 100L,12345L, 1L, "Test" );
        when(productInventoryRepository.findIdByProductIdAndBatchNumber(any(),any())).thenReturn(1L);
        when(productInventoryHierarchyMappingRepository.findProductInventoryHierarchyMappingByInventoryIdAndHierarchyMappingIdAndClientId(any(),any(),any())).thenReturn(product_inventory_hierarchy_mapping_entity());
        when(productInventoryLogRepository.save(any())).thenReturn(product_inventory_log_credit_entity());
        ProductInventoryLog productInventoryLog = stockService.setStockData(stockRequest, TransactionType.CREDIT, 1L);
        assertEquals(stockRequest.getQuantity(), productInventoryLog.getTransactionQuantity());
        assertEquals(stockRequest.getProductId(), productInventoryLog.getProductId());
        assertEquals(stockRequest.getLastUpdatedBy(), productInventoryLog.getUpdatedBy());
        assertEquals(TransactionType.CREDIT, productInventoryLog.getTransactionType());
        verify(productInventoryRepository, Mockito.times(1)).findIdByProductIdAndBatchNumber(any(), any());
        verify(productInventoryHierarchyMappingRepository, Mockito.times(1)).findProductInventoryHierarchyMappingByInventoryIdAndHierarchyMappingIdAndClientId(any(), any(), any());
        verify(productInventoryHierarchyMappingRepository, Mockito.times(1)).save(any());
        verify(productInventoryLogRepository, Mockito.times(1)).save(any());
    }

    @Test
    public void test_set_stock_data_credit_with_null_product_inventory_data_and_null_product_inventory_hierarchy_data() {
        StockRequest stockRequest = new StockRequest( 111L, 1L, "ABC123", "10-11-2025 00:00:00", 100L,12345L, 1L, "Test" );
        when(productInventoryRepository.findIdByProductIdAndBatchNumber(any(),any())).thenReturn(null);
        when(productInventoryRepository.save(any())).thenReturn(product_inventory_entity());
        when(productInventoryHierarchyMappingRepository.findProductInventoryHierarchyMappingByInventoryIdAndHierarchyMappingIdAndClientId(any(),any(),any())).thenReturn(null);
        when(productInventoryHierarchyMappingRepository.save(any())).thenReturn(product_inventory_hierarchy_mapping_entity());
        when(productInventoryLogRepository.save(any())).thenReturn(product_inventory_log_credit_entity());
        ProductInventoryLog productInventoryLog = stockService.setStockData(stockRequest, TransactionType.CREDIT, 1L);
        assertEquals(stockRequest.getQuantity(), productInventoryLog.getTransactionQuantity());
        assertEquals(stockRequest.getProductId(), productInventoryLog.getProductId());
        assertEquals(stockRequest.getLastUpdatedBy(), productInventoryLog.getUpdatedBy());
        assertEquals(TransactionType.CREDIT, productInventoryLog.getTransactionType());
        verify(productInventoryRepository, Mockito.times(1)).findIdByProductIdAndBatchNumber(any(), any());
        verify(productInventoryRepository, Mockito.times(1)).save(any());
        verify(productInventoryHierarchyMappingRepository, Mockito.times(1)).findProductInventoryHierarchyMappingByInventoryIdAndHierarchyMappingIdAndClientId(any(), any(), any());
        verify(productInventoryHierarchyMappingRepository, Mockito.times(2)).save(any());
        verify(productInventoryLogRepository, Mockito.times(1)).save(any());
    }

    @Test
    public void test_set_stock_data_credit_with_with_null_product_inventory_hierarchy_data() {
        StockRequest stockRequest = new StockRequest( 111L, 1L, "ABC123", "10-11-2025 00:00:00", 100L,12345L, 1L, "Test" );
        when(productInventoryRepository.findIdByProductIdAndBatchNumber(any(),any())).thenReturn(1L);
        when(productInventoryHierarchyMappingRepository.findProductInventoryHierarchyMappingByInventoryIdAndHierarchyMappingIdAndClientId(any(),any(),any())).thenReturn(null);
        when(productInventoryHierarchyMappingRepository.save(any())).thenReturn(product_inventory_hierarchy_mapping_entity());
        ProductInventoryLog productInventoryLog = stockService.setStockData(stockRequest, TransactionType.CREDIT, 1L);
        assertEquals(stockRequest.getQuantity(), productInventoryLog.getTransactionQuantity());
        assertEquals(stockRequest.getProductId(), productInventoryLog.getProductId());
        assertEquals(stockRequest.getLastUpdatedBy(), productInventoryLog.getUpdatedBy());
        assertEquals(TransactionType.CREDIT, productInventoryLog.getTransactionType());
        verify(productInventoryRepository, Mockito.times(1)).findIdByProductIdAndBatchNumber(any(), any());
        verify(productInventoryHierarchyMappingRepository, Mockito.times(1)).findProductInventoryHierarchyMappingByInventoryIdAndHierarchyMappingIdAndClientId(any(), any(), any());
        verify(productInventoryHierarchyMappingRepository, Mockito.times(2)).save(any());
        verify(productInventoryLogRepository, Mockito.times(1)).save(any());
    }
    @Test
    public void test_get_product_stock_data() {
        List<Long> productIds = new ArrayList<>();
        productIds.add(1L);
        productIds.add(2L);
        List<Long> hierarchyMappingIds = new ArrayList<>();
        hierarchyMappingIds.add(111L);
        hierarchyMappingIds.add(113L);
        ProductStockSearchRequest productStockSearchRequest = new ProductStockSearchRequest(productIds, hierarchyMappingIds);
        List<ProductStockData> productStockSearchResponse = new ArrayList<>();
        List<HierarchyData> hierarchyData= new ArrayList<>();
        hierarchyData.add(new HierarchyData(111L, 1000L));
        productStockSearchResponse.add(new ProductStockData(1L, "ABC", Utils.convertStringToDateOrDefault("10-11-2025 00:00:00"), hierarchyData));
        List<ProductStockHierarchyDto> productStockHierarchyDtoList = new ArrayList<>();
        productStockHierarchyDtoList.add(new ProductStockHierarchyDto(1L, 1L, "ABC", Utils.convertStringToDateOrDefault("10-11-2025 00:00:00"), 111L, 1000L));
        when(productInventoryHierarchyMappingRepository.getProductStockHierarchyDto(any(), any())).thenReturn(productStockHierarchyDtoList);
        List<ProductStockData> productStockDataList = stockService.getProductStockData(productStockSearchRequest);
        List<Long> actualProductIds = productStockHierarchyDtoList.stream().map(ProductStockHierarchyDto::getProductId).collect(Collectors.toList());
        List<Long> responseProductIds = productStockDataList.stream().map(ProductStockData::getProductId).collect(Collectors.toList());
        assertEquals(productStockSearchResponse.size(), productStockDataList.size());
        assertEquals(actualProductIds, responseProductIds);
       // assertEquals(new HashSet<>(actualProductIds), new HashSet<>(productIds));
        verify(productInventoryHierarchyMappingRepository,Mockito.times(1)).getProductStockHierarchyDto(any(), any());
    }

    @Test
    public void test_get_product_stock_non_zero_quantity_data() {
        List<Long> productIds = new ArrayList<>();
        productIds.add(1L);
        productIds.add(2L);
        List<Long> hierarchyMappingIds = new ArrayList<>();
        hierarchyMappingIds.add(111L);
        hierarchyMappingIds.add(113L);
        ProductStockSearchRequest productStockSearchRequest = new ProductStockSearchRequest(productIds, hierarchyMappingIds);
        List<ProductStockData> productStockSearchResponse = new ArrayList<>();
        List<HierarchyData> hierarchyData= new ArrayList<>();
        hierarchyData.add(new HierarchyData(111L, 1000L));
        productStockSearchResponse.add(new ProductStockData(1L, "ABC", Utils.convertStringToDateOrDefault("10-11-2025 00:00:00"), hierarchyData));
        List<ProductStockHierarchyDto> productStockHierarchyDtoList = new ArrayList<>();
        productStockHierarchyDtoList.add(new ProductStockHierarchyDto(1L, 1L, "ABC", Utils.convertStringToDateOrDefault("10-11-2025 00:00:00"), 111L, 1000L));
        productStockHierarchyDtoList.add(new ProductStockHierarchyDto(1L, 1L, "XYZ", Utils.convertStringToDateOrDefault("10-11-2025 00:00:00"), 111L, 0L));

        when(productInventoryHierarchyMappingRepository.getProductStockHierarchyDto(any(), any())).thenReturn(productStockHierarchyDtoList);
        List<ProductStockData> productStockDataList = stockService.getProductStockData(productStockSearchRequest);
        List<Long> actualProductIds = productStockHierarchyDtoList.stream().map(ProductStockHierarchyDto::getProductId).collect(Collectors.toList());
        List<Long> responseProductIds = productStockDataList.stream().map(ProductStockData::getProductId).collect(Collectors.toList());
        assertEquals(productStockSearchResponse.size(), productStockDataList.size());
        assertEquals(actualProductIds.size()-1, responseProductIds.size());
        verify(productInventoryHierarchyMappingRepository,Mockito.times(1)).getProductStockHierarchyDto(any(), any());
    }


    @Test
    public void get_product_inventory_log() {
        List<ProductInventoryLog> productInventoryLogList = new ArrayList<>();
        productInventoryLogList.add(new ProductInventoryLog(TransactionType.ISSUED, 1L, 5L, CommentType.REASON_FOR_ADD, "issued 1 5 units", 1L, 1L,1L,TransactionIdType.INTERNAL));
        when(productInventoryHierarchyMappingRepository.findAllByIdIn(any())).thenReturn(Collections.singletonList(new ProductInventoryHierarchyMappingMinDto(1L,1L)));
        when(productInventoryRepository.findAllByIdIn(any())).thenReturn(product_inventory_list());
        List<ProductInventoryLogDto> productInventoryLogDtoList = stockService.getProductInventoryLogDto(productInventoryLogList);
        assertEquals(productInventoryLogDtoList.size(), productInventoryLogDtoList.size());
        assertNotNull(productInventoryLogDtoList.get(0).getBatchNumber());
        assertNotNull(productInventoryLogDtoList.get(0).getExpiryDate());
        verify(productInventoryHierarchyMappingRepository, Mockito.times(1)).findAllByIdIn(any());
        verify(productInventoryRepository, Mockito.times(1)).findAllByIdIn(any());
    }

    @Test
    public void get_product_inventory_log_without_inventory() {
        List<ProductInventoryLog> productInventoryLogList = new ArrayList<>();
        productInventoryLogList.add(new ProductInventoryLog(TransactionType.ISSUED, 1L, 5L, CommentType.REASON_FOR_ADD, "issued 1 5 units", null, 1L,1L,TransactionIdType.INTERNAL));
        when(productInventoryHierarchyMappingRepository.findAllByIdIn(any())).thenReturn(Collections.singletonList(new ProductInventoryHierarchyMappingMinDto(1L,1L)));
        when(productInventoryRepository.findAllByIdIn(any())).thenReturn(product_inventory_list());
        List<ProductInventoryLogDto> productInventoryLogDtoList = stockService.getProductInventoryLogDto(productInventoryLogList);
        assertEquals(productInventoryLogDtoList.size(), productInventoryLogDtoList.size());
        assertNull(productInventoryLogDtoList.get(0).getBatchNumber());
        assertNull(productInventoryLogDtoList.get(0).getExpiryDate());
        verify(productInventoryHierarchyMappingRepository, Mockito.times(1)).findAllByIdIn(any());
        verify(productInventoryRepository, Mockito.times(1)).findAllByIdIn(any());
    }

    @Test
    public void test_set_stock_data_update() {
        StockRequest stockRequest = new StockRequest( 111L, 1L, "ABC123", "10-11-2025 00:00:00", 100L,12345L, 1L, "Test" );
        when(productInventoryRepository.findIdByProductIdAndBatchNumber(any(),any())).thenReturn(1L);
        when(productInventoryHierarchyMappingRepository.findProductInventoryHierarchyMappingByInventoryIdAndHierarchyMappingIdAndClientId(any(),any(),any())).thenReturn(product_inventory_hierarchy_mapping_entity());
        when(productInventoryLogRepository.save(any())).thenReturn(product_inventory_log_debit_entity());
        ProductInventoryLog productInventoryLog = stockService.setStockData(stockRequest, TransactionType.UPDATE, 1L);
        assertEquals(stockRequest.getQuantity(), productInventoryLog.getTransactionQuantity());
        assertEquals(stockRequest.getProductId(), productInventoryLog.getProductId());
        assertEquals(stockRequest.getLastUpdatedBy(), productInventoryLog.getUpdatedBy());
        assertEquals(TransactionType.UPDATE, productInventoryLog.getTransactionType());
        verify(productInventoryRepository, Mockito.times(1)).findIdByProductIdAndBatchNumber(any(), any());
        verify(productInventoryHierarchyMappingRepository, Mockito.times(1)).findProductInventoryHierarchyMappingByInventoryIdAndHierarchyMappingIdAndClientId(any(), any(), any());
        verify(productInventoryHierarchyMappingRepository, Mockito.times(1)).save(any());
        verify(productInventoryLogRepository, Mockito.times(1)).save(any());
    }

}
