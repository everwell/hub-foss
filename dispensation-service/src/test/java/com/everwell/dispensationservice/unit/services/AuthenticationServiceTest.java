package com.everwell.dispensationservice.unit.services;

import com.everwell.dispensationservice.Utils.Utils;
import com.everwell.dispensationservice.auth.JwtToken;
import com.everwell.dispensationservice.models.db.Client;
import com.everwell.dispensationservice.services.impl.AuthenticationServiceImpl;
import com.everwell.dispensationservice.unit.BaseTestNoSpring;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Spy;

import static org.junit.jupiter.api.Assertions.assertTrue;

public class AuthenticationServiceTest extends BaseTestNoSpring {

    @Spy
    @InjectMocks
    AuthenticationServiceImpl authenticationService;

    @Spy
    JwtToken jwtTokenUtil;

    @Test
    public void test_generate_token_and_validate() {
        Client client = new Client(1L, "Test_1", "Test_1", Utils.getCurrentDate());
        String token = authenticationService.generateToken(client);
        Boolean valid = authenticationService.isValidToken(token, client);

        assertTrue(valid);
    }
}
