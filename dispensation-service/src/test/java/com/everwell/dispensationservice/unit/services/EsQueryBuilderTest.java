package com.everwell.dispensationservice.unit.services;

import com.everwell.dispensationservice.Utils.Utils;
import com.everwell.dispensationservice.elasticsearch.dto.BaseSearchRequest;
import com.everwell.dispensationservice.elasticsearch.dto.MatchSearchDto;
import com.everwell.dispensationservice.elasticsearch.dto.QueryDto;
import com.everwell.dispensationservice.elasticsearch.dto.SupportedSearchMethods;
import com.everwell.dispensationservice.elasticsearch.enums.ElasticSearchValidation;
import com.everwell.dispensationservice.elasticsearch.service.ESQueryBuilder;
import com.everwell.dispensationservice.exceptions.ValidationException;
import com.everwell.dispensationservice.unit.BaseTestNoSpring;
import com.fasterxml.jackson.core.type.TypeReference;
import org.elasticsearch.index.query.BoolQueryBuilder;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Spy;

import java.io.IOException;
import java.util.Collections;
import java.util.List;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.*;

public class EsQueryBuilderTest extends BaseTestNoSpring {

    @Spy
    @InjectMocks
    ESQueryBuilder esQueryBuilder;

    @Test
    public void buildQueryWithNoSearchOptionTest() throws IOException {
        BaseSearchRequest baseSearchRequest = new BaseSearchRequest(null, 0, 25);

        BoolQueryBuilder boolQueryBuilder = esQueryBuilder.build(baseSearchRequest);

        Map<String, Object> queryMap = Utils.jsonToObject(boolQueryBuilder.toString(), new TypeReference<Map<String, Object>>() {});
        Map<String, Object> boolMap = (Map<String, Object>) queryMap.get("bool");
        assertFalse(boolMap.containsKey("must"));
        assertEquals(2L, boolMap.size());
    }

    @Test
    public void buildQueryWithNoValidSearchOptionTest() throws IOException {
        BaseSearchRequest baseSearchRequest = new BaseSearchRequest(new SupportedSearchMethods(null), 0, 25);

        BoolQueryBuilder boolQueryBuilder = esQueryBuilder.build(baseSearchRequest);

        Map<String, Object> queryMap = Utils.jsonToObject(boolQueryBuilder.toString(), new TypeReference<Map<String, Object>>() {});
        Map<String, Object> boolMap = (Map<String, Object>) queryMap.get("bool");
        assertFalse(boolMap.containsKey("must"));
        assertEquals(2L, boolMap.size());
    }

    @Test
    public void buildMatchQueryWithNoSearchKeyOptionTest() {
        MatchSearchDto matchSearchDto = new MatchSearchDto(null, "test");
        QueryDto queryDto = new QueryDto(Collections.singletonList(matchSearchDto));
        SupportedSearchMethods supportedSearchMethods = new SupportedSearchMethods(queryDto);
        BaseSearchRequest baseSearchRequest = new BaseSearchRequest(supportedSearchMethods, 0, 25);

        Exception ex = assertThrows(ValidationException.class, () ->  esQueryBuilder.build(baseSearchRequest));
        assertEquals(ElasticSearchValidation.INVALID_SEARCH_KEY_AND_SEARCH_TERM_VALUE.getMessage(), ex.getMessage());
    }

    @Test
    public void buildMatchQueryWithNoSearchTermOptionTest() {
        MatchSearchDto matchSearchDto = new MatchSearchDto("productName", null);
        QueryDto queryDto = new QueryDto(Collections.singletonList(matchSearchDto));
        SupportedSearchMethods supportedSearchMethods = new SupportedSearchMethods(queryDto);
        BaseSearchRequest baseSearchRequest = new BaseSearchRequest(supportedSearchMethods, 0, 25);

        Exception ex = assertThrows(ValidationException.class, () ->  esQueryBuilder.build(baseSearchRequest));
        assertEquals(ElasticSearchValidation.INVALID_SEARCH_KEY_AND_SEARCH_TERM_VALUE.getMessage(), ex.getMessage());
    }

    @Test
    public void buildMatchQueryWithFuzzinessValueGreaterThanTwoTest() {
        MatchSearchDto matchSearchDto = new MatchSearchDto("productName", "test", true, 5L, "or", 0);
        QueryDto queryDto = new QueryDto(Collections.singletonList(matchSearchDto));
        SupportedSearchMethods supportedSearchMethods = new SupportedSearchMethods(queryDto);
        BaseSearchRequest baseSearchRequest = new BaseSearchRequest(supportedSearchMethods, 0, 25);

        Exception ex = assertThrows(ValidationException.class, () ->  esQueryBuilder.build(baseSearchRequest));
        assertEquals(ElasticSearchValidation.INVALID_FUZZINESS_VALUE.getMessage(), ex.getMessage());
    }

    @Test
    public void buildMatchQueryWithWithFuzzinessValueLessThanZeroTest() {
        MatchSearchDto matchSearchDto = new MatchSearchDto("productName", "test", true, -1L, "or", 0);
        QueryDto queryDto = new QueryDto(Collections.singletonList(matchSearchDto));
        SupportedSearchMethods supportedSearchMethods = new SupportedSearchMethods(queryDto);
        BaseSearchRequest baseSearchRequest = new BaseSearchRequest(supportedSearchMethods, 0, 25);

        Exception ex = assertThrows(ValidationException.class, () ->  esQueryBuilder.build(baseSearchRequest));
        assertEquals(ElasticSearchValidation.INVALID_FUZZINESS_VALUE.getMessage(), ex.getMessage());
    }

    @Test
    public void buildMatchQueryWithInvalidOperatorOptionTest() {
        MatchSearchDto matchSearchDto = new MatchSearchDto("productName", "test", true, 1L, "test", 0);
        QueryDto queryDto = new QueryDto(Collections.singletonList(matchSearchDto));
        SupportedSearchMethods supportedSearchMethods = new SupportedSearchMethods(queryDto);
        BaseSearchRequest baseSearchRequest = new BaseSearchRequest(supportedSearchMethods, 0, 25);

        Exception ex = assertThrows(ValidationException.class, () ->  esQueryBuilder.build(baseSearchRequest));
        assertEquals(ElasticSearchValidation.INVALID_SEARCH_OPERATOR_VALUE.getMessage(), ex.getMessage());
    }

    @Test
    public void buildMatchQueryWithInvalidPrefixLengthTest() {
        MatchSearchDto matchSearchDto = new MatchSearchDto("productName", "test", true, 1L, "or", -1);
        QueryDto queryDto = new QueryDto(Collections.singletonList(matchSearchDto));
        SupportedSearchMethods supportedSearchMethods = new SupportedSearchMethods(queryDto);
        BaseSearchRequest baseSearchRequest = new BaseSearchRequest(supportedSearchMethods, 0, 25);

        Exception ex = assertThrows(ValidationException.class, () ->  esQueryBuilder.build(baseSearchRequest));
        assertEquals(ElasticSearchValidation.INVALID_PREFIX_LENGTH_VALUE.getMessage(), ex.getMessage());
    }

    @Test
    public void buildMatchQueryWithFuzzinessAndTranspositionsValueTest() throws IOException {
        MatchSearchDto matchSearchDto = new MatchSearchDto("productName", "test", true, 1L, "or", 0);
        QueryDto queryDto = new QueryDto(Collections.singletonList(matchSearchDto));
        SupportedSearchMethods supportedSearchMethods = new SupportedSearchMethods(queryDto);
        BaseSearchRequest baseSearchRequest = new BaseSearchRequest(supportedSearchMethods, 0, 25);

        BoolQueryBuilder boolQueryBuilder = esQueryBuilder.build(baseSearchRequest);

        Map<String, Object> queryMap = Utils.jsonToObject(boolQueryBuilder.toString(), new TypeReference<Map<String, Object>>() {});
        Map<String, Object> boolMap = (Map<String, Object>) queryMap.get("bool");
        assertTrue(boolMap.containsKey("must"));
        List<Map<String, Object>> mustMapList = ((List<Map<String, Object>>) boolMap.get("must"));
        assertTrue(mustMapList.get(0).containsKey("match"));
        Map<String, Object> clauseValueMap = (Map<String, Object>) mustMapList.get(0).get("match");
        Map<String, Object> fuzzySearchValues = (Map<String, Object>) clauseValueMap.get("productName");
        assertEquals(matchSearchDto.getSearchTerm(), fuzzySearchValues.get("query"));
        assertEquals(matchSearchDto.getFuzziness().toString(), fuzzySearchValues.get("fuzziness"));
        assertEquals(matchSearchDto.isFuzzyTranspositions(), fuzzySearchValues.get("fuzzy_transpositions"));
    }

    @Test
    public void buildFuzzyQueryWithDefaultValueTest() throws IOException {
        MatchSearchDto matchSearchDto = new MatchSearchDto("productName", "test");
        QueryDto queryDto = new QueryDto(Collections.singletonList(matchSearchDto));
        SupportedSearchMethods supportedSearchMethods = new SupportedSearchMethods(queryDto);
        BaseSearchRequest baseSearchRequest = new BaseSearchRequest(supportedSearchMethods, 0, 25);

        BoolQueryBuilder boolQueryBuilder = esQueryBuilder.build(baseSearchRequest);

        Map<String, Object> queryMap = Utils.jsonToObject(boolQueryBuilder.toString(), new TypeReference<Map<String, Object>>() {});
        Map<String, Object> boolMap = (Map<String, Object>) queryMap.get("bool");
        assertTrue(boolMap.containsKey("must"));
        List<Map<String, Object>> mustMapList = ((List<Map<String, Object>>) boolMap.get("must"));
        assertTrue(mustMapList.get(0).containsKey("match"));
        Map<String, Object> clauseValueMap = (Map<String, Object>) mustMapList.get(0).get("match");
        Map<String, Object> fuzzySearchValues = (Map<String, Object>) clauseValueMap.get("productName");
        assertEquals(matchSearchDto.getSearchTerm(), fuzzySearchValues.get("query"));
        assertEquals("AUTO", fuzzySearchValues.get("fuzziness"));
        assertEquals(matchSearchDto.isFuzzyTranspositions(), fuzzySearchValues.get("fuzzy_transpositions"));
    }

}
