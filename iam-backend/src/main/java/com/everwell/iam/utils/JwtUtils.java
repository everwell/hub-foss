package com.everwell.iam.utils;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import org.apache.commons.lang3.time.DateUtils;

import java.util.Base64;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.function.Function;

public class JwtUtils {

    public static String getSubject(String token, String secret) {
        return getClaimFromToken(token, secret, Claims::getSubject);
    }

    public static Date getExpirationDateFromToken(String token, String secret) {
        return getClaimFromToken(token, secret, Claims::getExpiration);
    }

    private static <T> T getClaimFromToken(String token, String secret, Function<Claims, T> claimsResolver) {
        final Claims claims = getAllClaimsFromToken(token, secret);
        return claimsResolver.apply(claims);
    }

    private static Claims getAllClaimsFromToken(String token, String secret) {
        return Jwts
                .parser()
                .setSigningKey(Base64.getEncoder().encodeToString(secret.getBytes()))
                .parseClaimsJws(token)
                .getBody();
    }

    private static Boolean isTokenExpired(String token, String secret) {
        final Date expiration = getExpirationDateFromToken(token, secret);
        return expiration.before(Utils.getCurrentDate());
    }

    private static String generateToken(Map<String, Object> claims, String subject, String secret, Date expiry) {
        return Jwts.builder()
                .setClaims(claims)
                .setSubject(subject)
                .setIssuedAt(Utils.getCurrentDate())
                .setExpiration(expiry)
                .signWith(SignatureAlgorithm.HS256, Base64.getEncoder().encodeToString(secret.getBytes())) //weak secret, create secure secret, encrypt them and store in db
                .compact();
    }

    public static String generateToken(String subject, String secret) {
        Map<String, Object> claims = new HashMap<>();
        return generateToken(claims, subject, secret, DateUtils.addDays(Utils.getCurrentDate(), 1));
    }

    public static Boolean isValidToken(String token, String secret) {
        return !isTokenExpired(token, secret);
    }
}
