package com.everwell.iam.handlers.impl;

import com.everwell.iam.enums.AdherenceCodeEnum;
import com.everwell.iam.enums.AdherenceTech;
import com.everwell.iam.handlers.ScheduleHandler;
import com.everwell.iam.models.db.*;
import com.everwell.iam.models.dto.TFNRepeatDto;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.math.BigInteger;
import java.util.*;

@Component
public class NNDLiteHandler extends NNDCommonHandler {

    private static final Logger LOGGER = LoggerFactory.getLogger(NNDLiteHandler.class);

    @Override
    public AdherenceTech getAdhTechEnumerator() {
        return AdherenceTech.NNDLITE;
    }

    @Override
    public Character getTFNRepeatAdherenceCode(TFNRepeatDto tfnRepeatDto) {
        return null;
    }

    @Override
    public void applyTfnFlagging(TreeMap<Integer, List<CallLogs>> calledDateToNumberDialedMap, Registration registration, Map<Date, AdherenceStringLog> logs) {
    }


    @Override
    public List<Character> getAdherenceCodesToRegenerate(){
        return Arrays.asList(
                AdherenceCodeEnum.RECEIVED.getCode(),
                AdherenceCodeEnum.RECEIVED_UNSCHEDULED.getCode(),
                AdherenceCodeEnum.RECEIVED_UNSURE.getCode()
        );
    }

    @Override
    public boolean eligibleForUpdate(char before, char after) {
        if (before == AdherenceCodeEnum.RECEIVED_UNSCHEDULED.getCode() && after == AdherenceCodeEnum.RECEIVED.getCode()) return true;
        if (before != AdherenceCodeEnum.RECEIVED.getCode() && after == AdherenceCodeEnum.RECEIVED_UNSCHEDULED.getCode()) return true;
        if (before == AdherenceCodeEnum.RECEIVED_UNSCHEDULED.getCode()) return false;
        if (before == AdherenceCodeEnum.NO_INFO.getCode()) return true;
        if (before == AdherenceCodeEnum.RECEIVED_UNSURE.getCode() && (after == AdherenceCodeEnum.RECEIVED.getCode())) return true;
        if ((before == AdherenceCodeEnum.MANUAL.getCode() || before == AdherenceCodeEnum.MISSED.getCode()) && (after == AdherenceCodeEnum.NO_INFO.getCode() || after == AdherenceCodeEnum.RECEIVED.getCode() || after == AdherenceCodeEnum.RECEIVED_UNSURE.getCode())) return true;
        if ((before != AdherenceCodeEnum.MANUAL.getCode() && before != AdherenceCodeEnum.MISSED.getCode() && before != AdherenceCodeEnum.RECEIVED.getCode()) && (after == AdherenceCodeEnum.PATIENT_MANUAL.getCode() || after == AdherenceCodeEnum.PATIENT_MISSED.getCode())) return true;
        if (before == AdherenceCodeEnum.PATIENT_MISSED.getCode() || before == AdherenceCodeEnum.PATIENT_MANUAL.getCode()) return true;
        return false;
    }
}
