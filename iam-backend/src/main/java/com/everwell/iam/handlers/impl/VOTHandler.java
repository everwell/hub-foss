package com.everwell.iam.handlers.impl;

import com.everwell.iam.enums.AdherenceCodeEnum;
import com.everwell.iam.enums.AdherenceTech;
import com.everwell.iam.exceptions.NotFoundException;
import com.everwell.iam.exceptions.ValidationException;
import com.everwell.iam.handlers.AdherenceTechHandler;
import com.everwell.iam.handlers.ScheduleHandler;
import com.everwell.iam.models.db.AdherenceStringLog;
import com.everwell.iam.models.db.Registration;
import com.everwell.iam.models.dto.AdhTechLogsData;
import com.everwell.iam.models.http.requests.EntityRequest;
import com.everwell.iam.models.http.requests.RecordAdherenceRequest;
import com.everwell.iam.repositories.AccessMappingRepository;
import com.everwell.iam.repositories.RegistrationRepository;
import com.everwell.iam.utils.Utils;
import com.everwell.iam.vendors.enums.VideoReviewStatus;
import com.everwell.iam.vendors.models.db.VideoEntityMapping;
import com.everwell.iam.vendors.repositories.VideoEntityMappingRepository;
import org.apache.commons.lang3.NotImplementedException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Set;
import java.util.concurrent.CompletableFuture;

@Component
public class VOTHandler extends AdherenceTechHandler {

    private static Logger LOGGER = LoggerFactory.getLogger(VOTHandler.class);

    @Autowired
    private VideoEntityMappingRepository videoRepository;

    @Autowired
    private AccessMappingRepository mappingRepository;

    @Autowired
    private RegistrationRepository registrationRepository;

    @Override
    public void postRegistration(Registration registration, EntityRequest entityRequest, boolean isNew) {
        super.postRegistration(registration, entityRequest, isNew);
    }

    @Override
    public void preDeletion(Registration registration) {

    }

    @Override
    protected boolean eligibleForUpdate(char before, char after) {
        boolean toUpdate = false;
        switch (AdherenceCodeEnum.getByCode(before)) {
            case UNVALIDATED:
                switch (AdherenceCodeEnum.getByCode(after)) {
                    case RECEIVED:
                    case RECEIVED_UNSURE:
                    case NO_INFO:
                    case PATIENT_MANUAL:
                    case PATIENT_MISSED:
                        toUpdate = true;
                        break;
                }
                break;
            case RECEIVED:
                switch (AdherenceCodeEnum.getByCode(after)) {
                    case MISSED:
                        toUpdate = true;
                        break;
                }
                break;
            case NO_INFO:
                toUpdate = true;
                break;
            case MANUAL:
                switch (AdherenceCodeEnum.getByCode(after)) {
                    case MISSED:
                    case RECEIVED:
                    case NO_INFO:
                    case UNVALIDATED:
                        toUpdate = true;
                        break;
                }
                break;
            case MISSED:
                switch (AdherenceCodeEnum.getByCode(after)) {
                    case NO_INFO:
                    case RECEIVED:
                    case MANUAL:
                    case UNVALIDATED:
                        toUpdate = true;
                        break;
                }
                break;
            case PATIENT_MANUAL:
            case PATIENT_MISSED:
                switch (AdherenceCodeEnum.getByCode(after)) {
                    case NO_INFO:
                    case RECEIVED:
                    case MANUAL:
                    case UNVALIDATED:
                    case MISSED:
                    case PATIENT_MANUAL:
                    case PATIENT_MISSED:
                        toUpdate = true;
                        break;
                }
                break;
        }
        return toUpdate;
    }

    @Override
    public AdherenceTech getAdhTechEnumerator() {
        return AdherenceTech.VOT;
    }

    @Override
    public CompletableFuture<List<Long>> regenerateAdherence(Set<Long> iamIds) {
        LOGGER.info("Regenerate Adherence started for entities: " + iamIds.size());
        List<Long> completeLogs = new ArrayList<>();
        List<Integer> videoLogStatusList = Arrays.asList(
                VideoReviewStatus.UNREVIEWED.getCode(),
                VideoReviewStatus.TAKEN.getCode()
        );
        List<Character> eventLogGeneratedValueList = Arrays.asList(
                getAdherenceValue(VideoReviewStatus.UNREVIEWED),
                getAdherenceValue(VideoReviewStatus.TAKEN)
        );
        for (Long id : iamIds) {
            /**
             * delete all previous event adherence data
             * DO NOT DELETE MANUALLY MARKED DOSES (values = 2 or 9)
             **/
            deleteAdherenceLogByIamIdAndAdherenceCodes(eventLogGeneratedValueList, id);
            List<VideoEntityMapping> videoLogsForTakenAndUnreviewed = videoRepository.findByIamIdForStatus(id,videoLogStatusList);
            List<AdherenceStringLog> logs = computeCompleteAdherenceString(videoLogsForTakenAndUnreviewed, id);
            completeLogs.addAll(updateAdherenceLogBulk(logs));
        }
        LOGGER.info("Regenerated Successfully for entities: " + iamIds.size());
        return CompletableFuture.completedFuture(completeLogs);
    }

    @Async
    public List<AdherenceStringLog> computeCompleteAdherenceString(List<VideoEntityMapping> videoLogs, Long id) {
        LOGGER.info("Regenerating Adherence for IAM id: " + id);
        List<AdherenceStringLog> logs = new ArrayList<>();
        Registration registration = registrationRepository.findById(id).get();
        ScheduleHandler handler = scheduleTypeHandlerMap.getHandler(registration.getScheduleTypeId());
        for (VideoEntityMapping v : videoLogs) {
            char adherenceCode = getAdherenceValue(VideoReviewStatus.getByCode(v.getStatus()));
            if (handler.shouldTakeDose(id, v.getUploadDate())) {
                logs.add(new AdherenceStringLog(id, adherenceCode, Utils.formatDate(v.getUploadDate())));
            }
        }
        updateRegistrationAfterRegen(registration.getId(), logs);
        LOGGER.info("Regenerating Adherence finished for IAM id: " + id);
        return logs;
    }

  @Override
  public List<AdhTechLogsData> getTechLogs(Long iamId) {
      List<AdhTechLogsData> adhTechLogs = new ArrayList<>();
      videoRepository.findByIamIdIn(Arrays.asList(iamId))
      .forEach(f -> {
          adhTechLogs.add(new AdhTechLogsData(f.getUploadDate(), null, null, null, f.getStatus(), null, f.getVideoId()));
      });
      return adhTechLogs;
  }


  public VideoEntityMapping updateVideoMapping(String videoId, Long iamId, Integer value) {
        VideoEntityMapping mapping = videoRepository.findByVideoIdAndIamId(videoId, iamId);
        if(null == mapping) {
            throw new NotFoundException("video mapping not found");
        }
        mapping.setStatus(value);
        videoRepository.save(mapping);
        return mapping;
    }

    public Character getAdherenceValue(VideoReviewStatus reviewStatus) {
        AdherenceCodeEnum codeEnum = AdherenceCodeEnum.NO_INFO;
        switch (reviewStatus) {
            case MISSED:
                codeEnum = AdherenceCodeEnum.NO_INFO;
                break;
            case TAKEN:
                codeEnum = AdherenceCodeEnum.RECEIVED;
                break;
            case UNREVIEWED:
                codeEnum = AdherenceCodeEnum.UNVALIDATED;
                break;
        }
        return codeEnum.getCode();
    }

    public Character getUploadedValue() {
        return AdherenceCodeEnum.UNVALIDATED.getCode();
    }

    public VideoEntityMapping recordVideoMapping(String videoId, RecordAdherenceRequest request, Long iamId) {
        VideoEntityMapping newMapping = new VideoEntityMapping(videoId, iamId, VideoReviewStatus.UNREVIEWED.getCode());
        if(null != request.getDate()) {
            try {
                newMapping.setUploadDate(Utils.convertStringToDate(request.getDate()));
            } catch (ParseException pE) {
                LOGGER.warn("[recordVideoMapping] couldn't parse date");
            }
        }
        videoRepository.save(newMapping);
        return newMapping;
    }

    public void validateVideoId(String videoId) {
        VideoEntityMapping mapping = videoRepository.findByVideoId(videoId);
        if(null != mapping) {
            throw new ValidationException("video mapping already exists");
        }
    }

    @Override
    public void deleteRegistration(Set<Long> iamId) {
        super.deleteRegistration(iamId);
        videoRepository.deleteAllByIamIdIn(iamId);
    }
}
