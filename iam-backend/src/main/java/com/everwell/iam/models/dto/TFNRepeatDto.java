package com.everwell.iam.models.dto;

import com.everwell.iam.handlers.ScheduleHandler;
import com.everwell.iam.models.db.AdherenceStringLog;
import com.everwell.iam.models.db.Registration;
import com.everwell.iam.models.db.Schedule;
import com.everwell.iam.models.db.ScheduleMap;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Date;
import java.util.List;
import java.util.Map;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class TFNRepeatDto {

    public List<AdherenceStringLog> logs;
    public Registration registrationForIamId;
    public boolean shared;
    public Date calledDate;
    public ScheduleHandler scheduleHandler;
    public List<ScheduleMap> allScheduleMaps;
    public Map<Long, Schedule> scheduleIdScheduleMap;
}
