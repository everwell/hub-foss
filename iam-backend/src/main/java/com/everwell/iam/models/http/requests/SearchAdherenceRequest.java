package com.everwell.iam.models.http.requests;

import com.everwell.iam.enums.RangeFilterType;
import com.everwell.iam.exceptions.NotFoundException;
import com.everwell.iam.exceptions.ValidationException;
import com.everwell.iam.models.dto.RangeFilters;
import com.everwell.iam.utils.Utils;
import lombok.*;
import org.springframework.util.CollectionUtils;

import java.text.ParseException;
import java.util.Arrays;
import java.util.List;
import com.everwell.iam.constants.Constants;

@Getter
@Setter
@ToString
@AllArgsConstructor
@NoArgsConstructor
public class SearchAdherenceRequest {

    private List<String> entityIdList;
    private List<RangeFilters> filters;

    public boolean validate() throws ValidationException{
        if(CollectionUtils.isEmpty(entityIdList)) {
            throw new ValidationException("invalid entity list");
        }
        if(entityIdList.size()>=Constants.MAX_BULK_ADHERENCE_SIZE){
            throw new ValidationException("entity list size exceeds maximum allowed size of "+Constants.MAX_BULK_ADHERENCE_SIZE);
        }
        if(filters == null)
            return true;

        for (RangeFilters filter : filters){
            if (filter.getType() == RangeFilterType.CONSECUTIVE_MISSED_DOSES){
                try{
                    if(filter.getFrom() != null)
                        Integer.parseInt(filter.getFrom());
                    if(filter.getTo() != null)
                        Integer.parseInt(filter.getTo());
                }
                catch (NumberFormatException e) {
                    throw new ValidationException("Filter numbers are invalid");
                }
            }
            if (filter.getType() == RangeFilterType.LAST_DOSAGE || filter.getType() == RangeFilterType.ADHERENCE_STATISTICS){
                try{
                    if(filter.getFrom() != null)
                        Utils.convertStringToDate(filter.getFrom());
                    if(filter.getTo() != null)
                        Utils.convertStringToDate(filter.getTo());
                }
                catch (NumberFormatException | ParseException e) {
                    throw new ValidationException("Filter date format is wrong");
                }
            }
        }
        return true;
    }
}
