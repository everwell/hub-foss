package com.everwell.iam.models.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.*;

import java.util.Date;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class AdhTechLogsData {
  @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy HH:mm:ss")
  private Date eventTime;
  private String imeiNumber;
  private String phoneNumber;
  private String numberDialled;
  private Integer videoStatus;
  @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy HH:mm:ss")
  private Date createdDate;
  private String videoId;
  public AdhTechLogsData(Date eventTime) {
    this.eventTime = eventTime;
  }
}
