package com.everwell.iam.models.http.responses;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class PositiveCountResponse {
  Long manualCount;
  Long digitalCount;
  Long missedCount;
}
