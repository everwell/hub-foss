package com.everwell.iam.models.http.responses;

import com.everwell.iam.models.dto.AdherenceCodeExtra;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class AdherenceCodeConfigResponse {
    char code;
    String codeName;
    AdherenceCodeExtra design;
    String legendText;
}
