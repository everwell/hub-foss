package com.everwell.iam.models.db;

import com.everwell.iam.utils.Utils;
import lombok.*;

import javax.persistence.*;
import java.util.Date;

@Getter
@Entity
@Table(name = "IAM_Registration")
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class Registration{

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Setter
    @Column(name = "adtech_id")
    private Long adTechId;

    @Setter
    @Column(name ="start_date")
    @Temporal(TemporalType.TIMESTAMP)
    private Date startDate;

    @Setter
    @Column(name = "end_date")
    @Temporal(TemporalType.TIMESTAMP)
    private Date endDate;

    @Setter
    @Column(name = "unique_identifier")
    private String uniqueIdentifier;

    @Setter
    @Column(name = "adh_string", columnDefinition = "TEXT")
    private String adherenceString;

    @Column(name = "created_date")
    private Date createdDate;

    @Setter
    @Column(name = "updated_date")
    private Date updatedDate;

    @Setter
    @Column(name = "last_dosage")
    private Date lastDosage;

    @Setter
    @Column(name = "scheduletype_id")
    private Long scheduleTypeId;

    @Setter
    @Column(name = "last_missed_dosage")
    private Date lastMissedDosage;

    public Registration(Long adherenceId, Long scheduleTypeId, String uniqueIdentifier) {
        this.adTechId = adherenceId;
        this.uniqueIdentifier = uniqueIdentifier;
        this.startDate = Utils.getCurrentDate();
        this.endDate = null;
        this.createdDate = Utils.getCurrentDate();
        this.updatedDate = Utils.getCurrentDate();
        this.scheduleTypeId = scheduleTypeId;
    }

    public Registration(Long id, Long scheduleTypeId) {
        this.id = id;
        this.scheduleTypeId = scheduleTypeId;
    }

    public Registration(Long id, Long adhTechId, Date startDate, Date endDate, String uniqueId, String adhString, Date createdDate, Date updatedDate) {
        this.id = id;
        this.adTechId = adhTechId;
        this.startDate = startDate;
        this.endDate = endDate;
        this.adherenceString = adhString;
        this.uniqueIdentifier = uniqueId;
        this.createdDate = createdDate;
        this.updatedDate = updatedDate;
        this.scheduleTypeId = (long) 1;
    }

    public Registration(Long id, Long adhTechId, Date startDate, Date endDate, String uniqueId, String adhString, Date createdDate, Date updatedDate, Date lastDosage) {
        this(id, adhTechId, startDate, endDate, uniqueId, adhString, createdDate, updatedDate);
        this.lastDosage = lastDosage;
    }

    public Registration(Long id, Long adhTechId, Date startDate, Date endDate, String uniqueId, String adhString, Date createdDate, Date updatedDate, Date lastDosage, Long scheduleTypeId) {
        this(id,adhTechId,startDate,endDate,uniqueId,adhString,createdDate,updatedDate,lastDosage);
        this.scheduleTypeId = scheduleTypeId;
    }
}
