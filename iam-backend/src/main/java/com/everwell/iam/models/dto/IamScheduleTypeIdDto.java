package com.everwell.iam.models.dto;

import lombok.Getter;

import java.math.BigInteger;

@Getter
public class IamScheduleTypeIdDto {
    private BigInteger iamId;
    private BigInteger scheduleTypeId;

    public IamScheduleTypeIdDto(BigInteger iamId, BigInteger scheduleTypeId) {
        this.iamId = iamId;
        this.scheduleTypeId = scheduleTypeId;
    }
}
