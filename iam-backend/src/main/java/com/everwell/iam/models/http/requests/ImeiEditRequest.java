package com.everwell.iam.models.http.requests;

import com.everwell.iam.exceptions.ValidationException;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.ToString;
import org.apache.commons.lang3.StringUtils;

@Getter
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class ImeiEditRequest {
  String entityId;
  String imei;

  public void validate(ImeiEditRequest imeiEditRequest) {
    if (StringUtils.isEmpty(imeiEditRequest.getEntityId())) {
      throw new ValidationException("entity mapping is required");
    }
    if(StringUtils.isEmpty(imeiEditRequest.getImei())){
      throw new ValidationException("imei is required");
    }
  }
}
