package com.everwell.iam.models.db;

import lombok.*;

import javax.persistence.*;

@Entity
@Table(name = "IAM_Schedule")
@Setter
@NoArgsConstructor
public class Schedule {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Getter
    private Long id;

    @Getter
    @Column(name = "value")
    private String value;

    @Column(name = "scheduletype_id")
    private Long scheduleTypeId;

    public Schedule(Long id, String value, Long scheduleTypeId) {
        this.id = id;
        this.value = value;
        this.scheduleTypeId = scheduleTypeId;
    }
}
