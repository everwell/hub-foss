package com.everwell.iam.models.http.requests;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * DTO request class for RegisteredPhoneNumber event handling
 * @author Vaibhav Sharma
 */
@Data
@NoArgsConstructor
public class ProcessPhoneNumberRequest {
    // Number from which call is received
    private String callerNumber;
    // Number to which call was targeted
    private String numberDialled;
    private String iamId;

    private Long entityId;

    public ProcessPhoneNumberRequest(String callerNumber, String numberDialled, String iamId) {
        this.callerNumber = callerNumber;
        this.numberDialled = numberDialled;
        this.iamId = iamId;
    }
}
