package com.everwell.iam.models.http.responses;

import com.everwell.iam.models.dto.AdhTechLogsData;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.Date;
import java.util.List;

@Data
@AllArgsConstructor
public class AdherenceTechLogsResponse {
  Long adherenceTechId;
  @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy HH:mm:ss")
  Date startDate;
  @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy HH:mm:ss")
  Date endDate;
  List<AdhTechLogsData> adhTechLogs;
  String adherenceString;
}
