package com.everwell.iam.models.dto;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.util.Date;
import java.util.List;

@Getter
@Setter
@ToString
public class PhoneDeleteDto {
  List<Long> iamIds;
  List<String> entityIds;
}

