package com.everwell.iam.models.http.requests;

import com.everwell.iam.constants.Constants;
import com.everwell.iam.enums.RangeFilterType;
import com.everwell.iam.exceptions.ValidationException;
import com.everwell.iam.models.dto.RangeFilters;
import com.everwell.iam.utils.Utils;
import lombok.*;
import org.springframework.util.CollectionUtils;

import java.text.ParseException;
import java.util.List;

@Getter
@Setter
@ToString
@AllArgsConstructor
@NoArgsConstructor
public class GetBulkImeiRequest {
    @Getter
    private List<String> entityIdList;

    @Getter
    private boolean active;

    public boolean validate() throws ValidationException {
        if(CollectionUtils.isEmpty(entityIdList)) {
            throw new ValidationException("invalid entity list");
        }
        if(entityIdList.size()>= Constants.MAX_BULK_IMEI_SIZE){
            throw new ValidationException("entity list size exceeds maximum allowed size of "+Constants.MAX_BULK_IMEI_SIZE);
        }
        return true;
    }
}
