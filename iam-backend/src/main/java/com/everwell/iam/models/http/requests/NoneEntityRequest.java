package com.everwell.iam.models.http.requests;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class NoneEntityRequest extends EntityRequest {

  private NoneEntityRequest() {
    super(5);
  }

}
