package com.everwell.iam.models.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class StopScheduleExtraInfoDto {
    Character adherenceCode;
    Date endDate;
    boolean scheduleDeleted;
}
