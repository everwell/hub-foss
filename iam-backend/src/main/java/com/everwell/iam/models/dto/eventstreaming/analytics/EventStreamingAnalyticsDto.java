package com.everwell.iam.models.dto.eventstreaming.analytics;

import com.everwell.iam.utils.Utils;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.Date;

@Data
@AllArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class EventStreamingAnalyticsDto {
    private String eventName;

    private String eventAction;

    private String eventCategory;

    private Integer eventValue;

    private Long eventTime;


    public EventStreamingAnalyticsDto(String eventName, String eventAction, String eventCategory) {
        this.eventName = eventName;
        this.eventAction = eventAction;
        this.eventCategory = eventCategory;
        this.eventTime = Utils.getCurrentTime();
    }
}
