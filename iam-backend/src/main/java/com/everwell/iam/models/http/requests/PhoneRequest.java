package com.everwell.iam.models.http.requests;

import com.everwell.iam.constants.Constants;
import com.everwell.iam.models.db.PhoneMap;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.*;

import java.util.Date;
import java.util.List;

@Getter
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class PhoneRequest {
  String entityId;
  List<PhoneNumbers> phones;

  @Data
  @AllArgsConstructor
  @NoArgsConstructor
  public static class PhoneNumbers {
    String phoneNumber;
    String stopDate;
  }

}
