package com.everwell.iam.models.http.requests;

import com.everwell.iam.constants.Constants;
import com.everwell.iam.enums.AverageAdherenceIntervals;
import com.everwell.iam.enums.RangeFilterType;
import com.everwell.iam.exceptions.ValidationException;
import com.everwell.iam.models.dto.RangeFilters;
import com.everwell.iam.utils.Utils;
import lombok.*;
import org.springframework.util.CollectionUtils;

import java.text.ParseException;
import java.util.Arrays;
import java.util.EnumSet;
import java.util.List;
import java.util.stream.Collectors;

@Getter
@Setter
@ToString
@AllArgsConstructor
@NoArgsConstructor
public class AverageAdherenceRequest {

    private List<String> entityIdList;
    private boolean patientDetailsRequired;
    private List<AverageAdherenceIntervals> averageAdherenceIntervals;

    public boolean validate() throws ValidationException {
        if(entityIdList == null || CollectionUtils.isEmpty(entityIdList)) {
            throw new ValidationException("invalid entity list");
        }
        if(patientDetailsRequired && entityIdList.size() > Constants.MAX_ENTITY_AVG_ADHERENCE_SIZE) {
            throw new ValidationException("entity list size exceeds maximum allowed size of "+Constants.MAX_ENTITY_AVG_ADHERENCE_SIZE);
        }

        if(averageAdherenceIntervals != null){
            List<AverageAdherenceIntervals> allowedIntervals = Arrays.stream(AverageAdherenceIntervals.values()).collect(Collectors.toList());
            if (!allowedIntervals.containsAll(averageAdherenceIntervals)) {
                throw new ValidationException("Invalid Interval Values");
            }

        }
        return true;
    }
}


