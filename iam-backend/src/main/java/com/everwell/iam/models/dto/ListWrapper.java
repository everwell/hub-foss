package com.everwell.iam.models.dto;

import com.everwell.iam.exceptions.ValidationException;
import lombok.*;
import org.springframework.util.CollectionUtils;

import java.util.Collection;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ListWrapper<T> {
  private List<T> request;

    public void validate() {
      if (CollectionUtils.isEmpty(request))
        throw new ValidationException("request list cannot be empty!");
    }
}
