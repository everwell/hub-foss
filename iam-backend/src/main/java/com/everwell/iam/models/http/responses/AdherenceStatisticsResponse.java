package com.everwell.iam.models.http.responses;

import com.everwell.iam.enums.RangeFilterType;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

@AllArgsConstructor
@Getter
@Setter
@JsonInclude(JsonInclude.Include.NON_NULL)
public class AdherenceStatisticsResponse implements Serializable {
  private String from;
  private String to;
  private int technologyDoses;
  private int manualDoses;
  private int totalDoses;
}
