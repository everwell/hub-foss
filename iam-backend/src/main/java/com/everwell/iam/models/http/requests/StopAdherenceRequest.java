package com.everwell.iam.models.http.requests;

import com.everwell.iam.exceptions.ValidationException;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.*;
import org.apache.commons.lang3.StringUtils;

@ToString
@AllArgsConstructor
@NoArgsConstructor
public class StopAdherenceRequest {

    @Getter
    @Setter
    private String entityId;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss")
    @Getter
    private String endDate;

    public void validate() {
        if (StringUtils.isEmpty(entityId))
            throw new ValidationException("entity id is required");
        if (StringUtils.isEmpty(endDate))
            throw new ValidationException("end date is required");
    }
}
