package com.everwell.iam.models.dto;

import com.everwell.iam.models.http.requests.EntityRequest;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ValidateScheduleDto {
    String scheduleString;
    ScheduleSensitivity sensitivity;
    List<EntityRequest.DoseTimeInfo> doseDetails;
}
