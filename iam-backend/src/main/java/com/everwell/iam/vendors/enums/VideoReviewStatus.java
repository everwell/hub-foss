package com.everwell.iam.vendors.enums;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.ToString;

@RequiredArgsConstructor
@ToString
public enum VideoReviewStatus {

    UNREVIEWED(0),
    TAKEN(1),
    MISSED(2),
    UNKNOWN(3),
    CHALLENGE(4);

    @Getter
    private final Integer code;

    public static VideoReviewStatus[] values;

    public static VideoReviewStatus getByCode(int code) {
        if (values == null) {
            values = VideoReviewStatus.values();
        }
        for (VideoReviewStatus aCode : values) {
            if (aCode.getCode() == code) {
                return aCode;
            }
        }
        return null;
    }

}
