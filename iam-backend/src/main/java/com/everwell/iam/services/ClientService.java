package com.everwell.iam.services;

import com.everwell.iam.models.db.CAccess;
import com.everwell.iam.models.http.requests.RegisterClientRequest;
import com.everwell.iam.models.http.responses.ClientResponse;

public interface ClientService {

    /**
     * Add a new client if it does not exist with the same name
     * @param clientRequest
     * @return object of type ClientResponse
     */
    ClientResponse registerClient(RegisterClientRequest clientRequest);

    boolean deleteClient(Long id);

    ClientResponse getClient(Long id);

    CAccess getClientById(Long id);
}
