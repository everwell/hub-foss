package com.everwell.iam.services;

import com.everwell.iam.models.dto.eventstreaming.EventStreamingDto;

import java.util.Map;

public interface RabbitMQPublisherService {
   public <T> void send(EventStreamingDto<T> message, String exchange) ;

   void send(String message, String routingKey, String exchange, Map<String, Object> map);

   void send(String message, String routingKey, String exchange, String expirationTime, Map<String, Object> map);
}
