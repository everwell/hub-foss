package com.everwell.iam.services.impl;

import com.everwell.iam.config.RmqDlxHelper;
import com.everwell.iam.constants.Constants;
import com.everwell.iam.constants.RabbitMQConstants;
import com.everwell.iam.exceptions.ConflictException;
import com.everwell.iam.exceptions.NotFoundException;
import com.everwell.iam.exceptions.ValidationException;
import com.everwell.iam.handlers.impl.MERMHandler;
import com.everwell.iam.handlers.impl.NNDHandler;
import com.everwell.iam.handlers.impl.NNDLiteHandler;
import com.everwell.iam.models.db.CAccess;
import com.everwell.iam.models.db.CallLogs;
import com.everwell.iam.models.dto.eventstreaming.EventStreamingDto;
import com.everwell.iam.models.http.requests.*;
import com.everwell.iam.models.http.responses.ClientResponse;
import com.everwell.iam.models.http.responses.EntityResponse;
import com.everwell.iam.services.AdherenceService;
import com.everwell.iam.services.ClientService;
import com.everwell.iam.services.EntityService;
import com.everwell.iam.services.EventServiceConsumerService;
import com.everwell.iam.utils.Utils;
import com.fasterxml.jackson.core.type.TypeReference;
import com.rabbitmq.client.Channel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

@Service
public class EventServiceConsumerServiceImpl implements EventServiceConsumerService {

    @Autowired
    NNDHandler nndHandler;

    @Autowired
    NNDLiteHandler nndLiteHandler;

    @Autowired
    MERMHandler mermHandler;

    @Autowired
    private EntityService entityService;

    @Autowired
    private AdherenceService adherenceService;

    @Autowired
    private ClientService clientService;

    @Value("${iam.regen.batch_size}")
    private long REGEN_LIST_MAX_BATCH;

    private static Logger LOGGER = LoggerFactory.getLogger(EventServiceConsumerServiceImpl.class);

    public final ArrayList<Long> iamIdListToRegenerate = new ArrayList<>();

    @Override
    @RabbitListener(queues = "${q.iam.process_call}", containerFactory = "rmqPrefetchCount20", concurrency = "2")
    public void processCallConsumer(Message message) throws IOException {
        String body = new String(message.getBody());
        LOGGER.info("[processCallConsumer] message received with body: " + body);
        EventStreamingDto<IncomingCallRequest> eventStreamingDto = Utils.jsonToObject(body, new TypeReference<EventStreamingDto<IncomingCallRequest>>(){});
        IncomingCallRequest incomingCallRequest = eventStreamingDto.getField();
        LOGGER.info("[processCallConsumer] Call received with caller=" + incomingCallRequest.getCaller() + ", TFN=" + incomingCallRequest.getNumberDialed() + ", utc time=" + incomingCallRequest.getUtcDateTime());
        incomingCallRequest.validateRequest();
        // Invoking saveCallLog only through nndHandler (and not nndLiteHandler) to avoid doubling of callLog entries
        CallLogs callLog = nndHandler.saveCallLog(incomingCallRequest);
        nndHandler.processIncomingCall(callLog);
        nndLiteHandler.processIncomingCall(callLog);
    }

    @Override
    @RabbitListener(queues = "${q.iam.regenerate_entity}", containerFactory = "rmqPrefetchCount20", concurrency = "2")
    public void processRegenerateEntity(Message message) throws IOException {
        String body = new String(message.getBody());
        LOGGER.info("[processRegenerateEntity] message received with body: " + body);
        EventStreamingDto<RegenerateEventRequest> eventStreamingDto = Utils.jsonToObject(body, new TypeReference<EventStreamingDto<RegenerateEventRequest>>(){});
        RegenerateEventRequest regenerateRequest = eventStreamingDto.getField();
        if (regenerateRequest.getIamId() == null){
            throw new ValidationException("[processRegenerateEntity] IAM id is required");
        }
        synchronized(iamIdListToRegenerate) {
            iamIdListToRegenerate.add(regenerateRequest.getIamId());
        }
        if(iamIdListToRegenerate.size() > REGEN_LIST_MAX_BATCH) {
            processRegenList();
        }
    }

    public synchronized void processRegenList() {
        if (iamIdListToRegenerate.size() > 0) {
            List<Long> tempIamIdList;
            synchronized(iamIdListToRegenerate) {
                tempIamIdList = new ArrayList<>(iamIdListToRegenerate);
                iamIdListToRegenerate.clear();
            }
            adherenceService.regenerateAdherenceString(tempIamIdList);
        }
    }

    @Scheduled(fixedDelayString = "${iam.regen.flush_gap_in_seconds}000")
    public void clearRegenerationList() {
        if (iamIdListToRegenerate.size() > 0){
            processRegenList();
        }
    }

    @Override
    @RabbitListener(queues = "${q.iam.register_entity}", containerFactory = "rmqPrefetchCount20", concurrency = "2")
    public void registerEntityConsumer(Message message) throws IOException {
        String body = new String(message.getBody());
        LOGGER.info("[registerEntityConsumer] message received with body: " + body);
        EventStreamingDto<EntityRequest> entityRequestEventStreamingDto = Utils.jsonToObject(body, new TypeReference<EventStreamingDto<EntityRequest>>(){});
        EntityRequest entityRequest = entityRequestEventStreamingDto.getField();
        entityRequest.validate(entityRequest);
        Long clientId = entityRequest.getClientId();
        if (clientId == null)
            throw new NotFoundException("Client Id does not exists");

        CAccess client = clientService.getClientById(clientId);
        if(null != client) {
            if (entityService.validateActiveIamMappingByEntityId(clientId, entityRequest.getEntityId())) {
                LOGGER.error("[registerEntityConsumer] Active entity already exists: " + entityRequest.getEntityId());
                throw new ConflictException("Active entity already exists");
            }
            if(adherenceService.hasScheduleMappings(entityRequest.getScheduleTypeId()) && adherenceService.activeScheduleMappingExistsForEntityIdAndClientId(clientId, entityRequest.getEntityId(), entityRequest.getScheduleTypeId())) {
                throw new ConflictException("[registerEntityConsumer] Active schedule exits");
            }
            Long iamId = adherenceService.createIam(entityRequest);
            entityService.registerEntity(entityRequest.getEntityId(), iamId, entityRequest.getClientId());
        }
    }

    @Override
    @RabbitListener(queues = "${q.iam.process_merm_event}", containerFactory = "rmqPrefetchCount20", concurrency = "2")
    public void processMermEventConsumer(Message message) throws IOException {
        String body = new String(message.getBody());
        LOGGER.info("[processMermEventConsumer] message received with body: " + body);
        EventStreamingDto<MermEventRequest> entityRequestEventStreamingDto = Utils.jsonToObject(body, new TypeReference<EventStreamingDto<MermEventRequest>>(){});
        MermEventRequest mermEventRequest = entityRequestEventStreamingDto.getField();
        mermEventRequest.validate();
        mermHandler.processIncomingEvent(mermEventRequest);
    }
}
