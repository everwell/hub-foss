package com.everwell.iam.exceptions.rmq;

import com.everwell.iam.config.RmqDlxHelper;
import com.everwell.iam.constants.RabbitMQConstants;
import com.fasterxml.jackson.databind.exc.UnrecognizedPropertyException;
import org.springframework.amqp.AmqpRejectAndDontRequeueException;
import org.springframework.amqp.ImmediateAcknowledgeAmqpException;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.listener.exception.ListenerExecutionFailedException;
import org.springframework.util.ErrorHandler;

public class RmqExceptionStrategy implements ErrorHandler {

    @Override
    public void handleError(Throwable throwable) {
        if (throwable instanceof ListenerExecutionFailedException) {
            Message message = ((ListenerExecutionFailedException) throwable).getFailedMessage();
            String consumerQueue = message.getMessageProperties().getConsumerQueue();
            RmqDlxHelper.triggerRetryMechanism(message, RabbitMQConstants.DLX_EXCHANGE, consumerQueue, RabbitMQConstants.WAIT_EXCHANGE);
            throw new ImmediateAcknowledgeAmqpException("[RMQ handleError] - Handled Error, Triggering Retry Mechanism");
        }
        throw new AmqpRejectAndDontRequeueException("Exception at consumer", throwable);
    }

}
