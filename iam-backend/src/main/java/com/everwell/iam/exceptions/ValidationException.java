package com.everwell.iam.exceptions;

public class ValidationException extends RuntimeException {

    public ValidationException(String exception) {
        super(exception);
    }
}
