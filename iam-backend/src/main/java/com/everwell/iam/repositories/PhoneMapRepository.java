package com.everwell.iam.repositories;

import com.everwell.iam.models.db.PhoneMap;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigInteger;
import java.util.Date;
import java.util.List;
import java.util.Set;

@Repository
public interface PhoneMapRepository extends JpaRepository<PhoneMap, Long> {

  @Query(value = "SELECT m.iam_id FROM iam_phone_map m inner join iam_registration r ON m.iam_id = r.id where phone_number IN :phones and r.end_date is null", nativeQuery = true)
  List<BigInteger> getActiveIamIdsByPhoneNumber(List<String> phones);

  List<PhoneMap> findAllByIamId(Long iamId);

  List<PhoneMap> findAllByIamIdIn(Set<Long> iamIds);

  // for phone record to be active:
  // 1. caller's record should have been active at that time
  // 2. the phone numbers associated, should also be active at that time i.e incoming call date <= phone end date
  @Query(value = "SELECT m.iam_id FROM iam_phone_map m inner join iam_registration r ON m.iam_id = r.id where phone_number = :phoneNumber and r.start_date <= :calledDate AND (r.end_date is null OR :calledDate <= r.end_date) and (:calledDate <= m.stop_date OR m.stop_date is null)", nativeQuery = true)
  List<BigInteger> findAllActiveIamIdsByPhoneNumbers(String phoneNumber, Date calledDate);

  @Query("SELECT DISTINCT(p.iamId) from PhoneMap p where p.phoneNumber IN :numbers and p.stopDate is null")
  List<Long> findAllActiveIamIdsByPhoneNumbers(List<String> numbers);

  @Modifying
  @Transactional
  @Query("UPDATE PhoneMap p SET p.stopDate = :date where p.iamId IN :ids and p.stopDate is null")
  void deactivatePhonesByIamIds(Set<Long> ids, Date date);

  @Modifying
  @Transactional
  @Query("UPDATE PhoneMap p SET p.stopDate = :date where p.phoneNumber IN :phone and p.stopDate is null and p.iamId = :iamId")
  void deactivateOldPhones(List<String> phone, Date date, Long iamId);

  @Modifying
  @Transactional
  @Query("UPDATE PhoneMap p SET p.stopDate = :date where p.phoneNumber IN :phone and p.iamId = :iamId")
  void updateEndDate(List<String> phone, Long iamId, Date date);

  @Modifying
  @Transactional
  @Query("DELETE from PhoneMap where iamId in :iamIds")
  void deleteAllByIamIdIn(Set<Long> iamIds);

}
