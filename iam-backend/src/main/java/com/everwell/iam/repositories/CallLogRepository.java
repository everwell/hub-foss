package com.everwell.iam.repositories;

import com.everwell.iam.models.db.CallLogs;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;

@Repository
public interface CallLogRepository extends JpaRepository<CallLogs, Long> {
  @Query(value = "select cl.* from iam_call_logs cl inner join iam_phone_map p ON cl.caller = p.phone_number where p.iam_id = :iamId and (p.stop_date is null or cl.client_time <= p.stop_date) and cl.client_time >= :startDate and cl.client_time <= :endDate ORDER BY cl.client_time", nativeQuery = true)
  List<CallLogs> getAllByIamIdOrderByClientTimeCustom(Long iamId, Date startDate, Date endDate);

  @Query(value = "select cl.* from iam_call_logs cl where cl.caller = :phoneNumber and cl.client_time >= :registrationStartDate and cl.client_time <= :phoneEndDate and cl.client_time <= :registrationEndDate ORDER BY cl.client_time", nativeQuery = true)
  List<CallLogs> getCallLogsByPhone(String phoneNumber, Date phoneEndDate, Date registrationStartDate, Date registrationEndDate);

  @Query(value = "select cl.* from iam_call_logs cl inner join iam_phone_map p ON cl.caller = p.phone_number where p.iam_id = :iamId AND cl.client_time <= :date ORDER BY cl.client_time", nativeQuery = true)
  List<CallLogs> getAllByIamIdOrderByClientTimeLimitByDateCustom(Long iamId, Date date);

}