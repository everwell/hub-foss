package com.everwell.iam.repositories;

import com.everwell.iam.models.db.AdherenceTechnology;
import org.springframework.data.jpa.repository.JpaRepository;

public interface AdhTechRepository extends JpaRepository<AdherenceTechnology, Long> {
}
