package com.everwell.iam.enums;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public enum DosageTypeEnum {
    LAST_MISSED_DOSAGE("last_missed_dosage"),
    LAST_DOSAGE("last_dosage");

    @Getter
    private final String name;
}
