create index iam_schedule_map_iamId_idx
    on iam_schedule_map (iam_id);


create index iam_schedule_map_active_iamId_idx
    on iam_schedule_map (active, iam_id);


create index iam_schedule_map_iamId_startDt_endDt_idx
    on iam_schedule_map (iam_id, start_date, end_date);

