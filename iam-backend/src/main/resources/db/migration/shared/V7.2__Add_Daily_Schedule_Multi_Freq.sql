INSERT into iam_schedule_type(id,created_date, name) values ((select max(id) from iam_schedule_type) + 1, current_timestamp, 'Multi Frequency');

INSERT into iam_schedule(id,value,scheduletype_id) values ((select max(id) from iam_schedule) + 1,'1111111', (select id from iam_schedule_type where name = 'Multi Frequency')) ON CONFLICT DO NOTHING;