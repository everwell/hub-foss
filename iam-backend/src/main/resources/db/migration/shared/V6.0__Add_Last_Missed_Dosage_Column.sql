ALTER TABLE iam_registration
  ADD COLUMN IF NOT EXISTS last_missed_dosage timestamp;