package tests.unit.controllers;

import com.everwell.iam.constants.Constants;
import com.everwell.iam.controllers.AdherenceController;
import com.everwell.iam.enums.AverageAdherenceIntervals;
import com.everwell.iam.enums.RangeFilterType;
import com.everwell.iam.exceptions.CustomExceptionHandler;
import com.everwell.iam.models.db.AccessMapping;
import com.everwell.iam.models.dto.*;
import com.everwell.iam.models.http.requests.*;
import com.everwell.iam.models.http.responses.*;
import com.everwell.iam.services.AdherenceService;
import com.everwell.iam.services.EntityService;
import com.everwell.iam.utils.CacheUtils;
import com.everwell.iam.utils.Utils;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.ValueOperations;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import tests.BaseTestNoSpring;

import java.util.*;
import java.util.stream.Collectors;

import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

public class AdherenceControllerTest extends BaseTestNoSpring {

    private MockMvc mockMvc;

    @Mock
    private EntityService entityService;

    @Mock
    private AdherenceService adherenceService;

    @InjectMocks
    private AdherenceController adherenceController;

    CacheUtils cacheUtils;

    @Mock
    RedisTemplate<String, Object> redisTemplate;

    @Mock
    private ValueOperations<String, Object> valueOperations;

    @Mock
    ApplicationEventPublisher applicationEventPublisher;

    private static final String DATA_FETCH_SUCCESS = "data fetched successfully";
    private static final String REGENERATE_ADHERENCE_SUCCESS = "Adherence Regenerated Successfully!";
    private static final String ADHERENCE_FAILED_TO_STOP = "entity adherence failed to stop";
    private static final String ADHERENCE_STOP_SUCCESS = "entity adherence successfully stopped";
    private static final String ACTIVE_ENTITY_NOT_FOUND = "active entity was not found";
    private static final String NO_ENTITY_FOUND = "No entity found!";
    private static final String ADHERENCE_TYPE_MISSING = "Adherence Type is missing";
    private static final String EXCEED_SIZE_LIMIT = "Size should be less than 10,000!";
    private static final String ENTITY_IDS_NOT_FOUND = "Entity ids not found!";
    private static final String ENTITY_REQUEST_ACCEPTED = "entity request accepted";
    private static final String INVALID_ENTITY_LIST = "invalid entity list";
    private static final String DATE_PARSE_FAILED = "failed to parse date";
    private static final String ENTITY_IDENTIFICATION_MISSING = "entity identification required";
    private static final String ENTITY_MAPPING_MISSING = "entity mapping does not exist";
    private static final String NO_ENTITY_MAPPING_FOUND = "entity mapping not found for any entity id";
    private static final String AVERAGE_ADHERENCE_SUCCESS = "average adherence calculated successfully";
    private static final String INVALID_DATE_FORMAT = "Filter date format is wrong";
    private static final String INVALID_NUMBER = "Filter numbers are invalid";
    private static final String ENTITY_ID_LIST_SIZE_EXCEEDED = "entity list size exceeds maximum allowed size of "+ Constants.MAX_BULK_ADHERENCE_SIZE;
    private static final String AVG_ADHERENCE_ENTITY_ID_LIST_SIZE_EXCEEDED = "entity list size exceeds maximum allowed size of "+ Constants.MAX_ENTITY_AVG_ADHERENCE_SIZE;

    @Before
    public void setup() {
        mockMvc = MockMvcBuilders.standaloneSetup(adherenceController)
                .setControllerAdvice(new CustomExceptionHandler())
                .build();
        cacheUtils = new CacheUtils(redisTemplate);
        when(redisTemplate.opsForValue()).thenReturn(valueOperations);
        when(redisTemplate.delete(anyString())).thenReturn(true);
        doNothing().when(valueOperations).set(anyString(), any(), anyLong(), any());
    }

    @Test
    public void testStopAdherenceActiveEntityNotFound() throws Exception {
        String uri = "/v1/adherence/stop";
        String entityId = "123";
        String stopDate = "2019-08-31 18:30:00";
        Long clientId = 4L;
        StopAdherenceRequest adherenceRequest = new StopAdherenceRequest(entityId,stopDate);

        when(entityService.getActiveIamMappingByEntityId(clientId, entityId)).thenReturn(null);

        mockMvc
            .perform(
                MockMvcRequestBuilders
                    .post(uri)
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(Utils.asJsonString(adherenceRequest))
                    .header("X-IAM-Client-Id", clientId.toString())
                    .accept(MediaType.APPLICATION_JSON_VALUE)
            )
            .andExpect(status().isNotFound())
            .andExpect(MockMvcResultMatchers.jsonPath("$.message").value(ACTIVE_ENTITY_NOT_FOUND));
    }

    @Test
    public void testStopAdherenceInvalidDate() throws Exception {
        String uri = "/v1/adherence/stop";
        String entityId = "123";
        String stopDate = "2019-08-31 18:30";
        Long clientId = 4L;
        Long iamId = 123L;
        StopAdherenceRequest adherenceRequest = new StopAdherenceRequest(entityId,stopDate);
        AccessMapping accessMapping = new AccessMapping(clientId, iamId, entityId);

        when(entityService.getActiveIamMappingByEntityId(clientId, entityId)).thenReturn(accessMapping);

        mockMvc
                .perform(
                        MockMvcRequestBuilders
                                .post(uri)
                                .contentType(MediaType.APPLICATION_JSON)
                                .content(Utils.asJsonString(adherenceRequest))
                                .header("X-IAM-Client-Id", clientId.toString())
                                .accept(MediaType.APPLICATION_JSON_VALUE)
                )
                .andExpect(status().isBadRequest())
                .andExpect(MockMvcResultMatchers.jsonPath("$.message").value(DATE_PARSE_FAILED));
    }

    @Test
    public void testStopAdherenceSuccess() throws Exception {
        String uri = "/v1/adherence/stop";
        String entityId = "123";
        String stopDate = "2019-08-31 18:30:00";
        Long clientId = 4L;
        Long iamId = 123L;
        StopAdherenceRequest adherenceRequest = new StopAdherenceRequest(entityId,stopDate);
        AccessMapping accessMapping = new AccessMapping(clientId, iamId, entityId);

        when(entityService.getActiveIamMappingByEntityId(clientId, entityId)).thenReturn(accessMapping);
        when(adherenceService.stopAdherence(accessMapping.getIamId(),Utils.convertStringToDate(stopDate))).thenReturn(true);
        when(entityService.toggleActiveStatus(accessMapping.getIamId(), clientId,false)).thenReturn(true);

        mockMvc
                .perform(
                        MockMvcRequestBuilders
                                .post(uri)
                                .contentType(MediaType.APPLICATION_JSON)
                                .content(Utils.asJsonString(adherenceRequest))
                                .header("X-IAM-Client-Id", clientId.toString())
                                .accept(MediaType.APPLICATION_JSON_VALUE)
                )
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.message").value(ADHERENCE_STOP_SUCCESS));
    }

    @Test
    public void testStopAdherenceSuccessWithoutEndDate() throws Exception {
        String uri = "/v1/adherence/stop";
        String entityId = "123";
        String stopDate = "";
        Long clientId = 4L;
        Long iamId = 123L;
        StopAdherenceRequest adherenceRequest = new StopAdherenceRequest(entityId,stopDate);
        AccessMapping accessMapping = new AccessMapping(clientId, iamId, entityId);

        when(entityService.getActiveIamMappingByEntityId(clientId, entityId)).thenReturn(accessMapping);
        when(adherenceService.stopAdherence(accessMapping.getIamId(),null)).thenReturn(true);
        when(entityService.toggleActiveStatus(accessMapping.getIamId(), clientId,false)).thenReturn(true);

        mockMvc
                .perform(
                        MockMvcRequestBuilders
                                .post(uri)
                                .contentType(MediaType.APPLICATION_JSON)
                                .content(Utils.asJsonString(adherenceRequest))
                                .header("X-IAM-Client-Id", clientId.toString())
                                .accept(MediaType.APPLICATION_JSON_VALUE)
                )
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.message").value(ADHERENCE_STOP_SUCCESS));
    }

    @Test
    public void testStopAdherenceFailedInAdherenceService() throws Exception {
        String uri = "/v1/adherence/stop";
        String entityId = "123";
        String stopDate = "2019-08-31 18:30:00";
        Long clientId = 4L;
        Long iamId = 123L;
        StopAdherenceRequest adherenceRequest = new StopAdherenceRequest(entityId,stopDate);
        AccessMapping accessMapping = new AccessMapping(clientId, iamId, entityId);

        when(entityService.getActiveIamMappingByEntityId(clientId, entityId)).thenReturn(accessMapping);
        when(adherenceService.stopAdherence(accessMapping.getIamId(),Utils.convertStringToDate(stopDate))).thenReturn(false);
        when(entityService.toggleActiveStatus(accessMapping.getIamId(), clientId,false)).thenReturn(true);

        mockMvc
                .perform(
                        MockMvcRequestBuilders
                                .post(uri)
                                .contentType(MediaType.APPLICATION_JSON)
                                .content(Utils.asJsonString(adherenceRequest))
                                .header("X-IAM-Client-Id", clientId.toString())
                                .accept(MediaType.APPLICATION_JSON_VALUE)
                )
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.message").value(ADHERENCE_FAILED_TO_STOP));
    }

    @Test
    public void testStopAdherenceFailedInEntityService() throws Exception {
        String uri = "/v1/adherence/stop";
        String entityId = "123";
        String stopDate = "2019-08-31 18:30:00";
        Long clientId = 4L;
        Long iamId = 123L;
        StopAdherenceRequest adherenceRequest = new StopAdherenceRequest(entityId,stopDate);
        AccessMapping accessMapping = new AccessMapping(clientId, iamId, entityId);

        when(entityService.getActiveIamMappingByEntityId(clientId, entityId)).thenReturn(accessMapping);
        when(adherenceService.stopAdherence(accessMapping.getIamId(),Utils.convertStringToDate(stopDate))).thenReturn(true);
        when(entityService.toggleActiveStatus(accessMapping.getIamId(), clientId,false)).thenReturn(false);

        mockMvc
                .perform(
                        MockMvcRequestBuilders
                                .post(uri)
                                .contentType(MediaType.APPLICATION_JSON)
                                .content(Utils.asJsonString(adherenceRequest))
                                .header("X-IAM-Client-Id", clientId.toString())
                                .accept(MediaType.APPLICATION_JSON_VALUE)
                )
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.message").value(ADHERENCE_FAILED_TO_STOP));
    }

    @Test
    public void testGetAdherenceEntityIdentificationMissing() throws Exception {
        String uri = "/v1/adherence";
        String iamId = null;
        String entityId = null;
        Boolean logsRequired = false;
        Long clientId = 4L;

        mockMvc
                .perform(
                        MockMvcRequestBuilders
                                .get(uri)
                                .param("iamId", iamId)
                                .param("entityId", entityId)
                                .param("logsRequired", logsRequired.toString())
                                .header("X-IAM-Client-Id", clientId.toString())
                                .accept(MediaType.APPLICATION_JSON_VALUE)
                )
                .andExpect(status().isBadRequest())
                .andExpect(MockMvcResultMatchers.jsonPath("$.message").value(ENTITY_IDENTIFICATION_MISSING));
    }

    @Test
    public void testGetAdherenceSuccess() throws Exception {
        String uri = "/v1/adherence";
        Long iamId = 123L;
        String entityId = "123";
        Boolean logsRequired = false;
        Long clientId = 4L;
        List<Long> iamIdList = new ArrayList<>();
        iamIdList.add(iamId);

        List<AdherenceData> adherenceDataList = new ArrayList<>();
        AdherenceData adherenceData = new AdherenceData();
        adherenceData.setStartDate(Utils.convertStringToDate("2019-01-18 18:30:00"));
        adherenceDataList.add(adherenceData);
        AdherenceResponse adherenceResponse = new AdherenceResponse(entityId, adherenceDataList, "666666", Utils.convertStringToDate("2019-01-01 01:00:00"), 0, 0, 0, 0, 0, null,null,null, 0,0, new ArrayList<>(), "NONE", Utils.convertStringToDate("2019-01-01 01:00:00"));
        List<AccessMapping> accessMapping = new ArrayList<>();
        accessMapping.add(new AccessMapping(clientId, iamId, entityId));

        when(entityService.getIamMapping(clientId, Arrays.asList(iamId))).thenReturn(accessMapping);
        when(adherenceService.getAllAdherence(iamIdList, logsRequired)).thenReturn(adherenceResponse);
        when(adherenceService.getAdherenceFromCache(eq(Collections.singletonList(entityId)), anyString(), any())).thenReturn(new AdherenceCacheDataDto<>());

        mockMvc
                .perform(
                        MockMvcRequestBuilders
                                .get(uri)
                                .param("iamId", iamId.toString())
                                .param("entityId", entityId)
                                .param("logsRequired", logsRequired.toString())
                                .header("X-IAM-Client-Id", clientId.toString())
                                .accept(MediaType.APPLICATION_JSON_VALUE)
                )
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.data.entityId").value(entityId))
                .andExpect(MockMvcResultMatchers.jsonPath("$.message").value(DATA_FETCH_SUCCESS));
    }

    @Test
    public void testGetAdherenceSuccessWithoutPassingEntityId() throws Exception {
        String uri = "/v1/adherence";
        Long iamId = 123L;
        String entityId = null;
        Boolean logsRequired = false;
        Long clientId = 4L;
        List<Long> iamIdList = new ArrayList<>();
        iamIdList.add(iamId);

        List<AdherenceData> adherenceDataList = new ArrayList<>();
        AdherenceData adherenceData = new AdherenceData();
        adherenceData.setStartDate(Utils.convertStringToDate("2019-01-18 18:30:00"));
        adherenceDataList.add(adherenceData);
        AdherenceResponse adherenceResponse = new AdherenceResponse(entityId, null, "666666", Utils.convertStringToDate("2019-01-01 01:00:00"), 0, 0, 0, 0, 0, null,null,null, 0,0, new ArrayList<>(), "NONE", Utils.convertStringToDate("2019-01-01 01:00:00"));
        List<AccessMapping> accessMapping = new ArrayList<>();
        accessMapping.add(new AccessMapping(clientId, iamId, entityId));

        when(entityService.getIamMapping(clientId, Arrays.asList(iamId))).thenReturn(accessMapping);
        when(adherenceService.getAllAdherence(iamIdList, logsRequired)).thenReturn(adherenceResponse);

        mockMvc
                .perform(
                        MockMvcRequestBuilders
                                .get(uri)
                                .param("iamId", iamId.toString())
                                .param("entityId", entityId)
                                .param("logsRequired", logsRequired.toString())
                                .header("X-IAM-Client-Id", clientId.toString())
                                .accept(MediaType.APPLICATION_JSON_VALUE)
                )
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.message").value(DATA_FETCH_SUCCESS));
    }

    @Test
    public void testGetAdherenceEntityMappingDoesNotExist() throws Exception {
        String uri = "/v1/adherence";
        String iamId = null;
        String entityId = "123";
        Boolean logsRequired = false;
        Long clientId = 4L;

        when(entityService.getAllIamMapping(clientId, entityId)).thenReturn(null);
        when(adherenceService.getAdherenceFromCache(eq(Collections.singletonList(entityId)), anyString(), any())).thenReturn(new AdherenceCacheDataDto<>());

        mockMvc
                .perform(
                        MockMvcRequestBuilders
                                .get(uri)
                                .param("iamId", iamId)
                                .param("entityId", entityId)
                                .param("logsRequired", logsRequired.toString())
                                .header("X-IAM-Client-Id", clientId.toString())
                                .accept(MediaType.APPLICATION_JSON_VALUE)
                )
                .andExpect(status().isNotFound())
                .andExpect(MockMvcResultMatchers.jsonPath("$.message").value(ENTITY_MAPPING_MISSING));
    }

    @Test
    public void testGetAdherenceSuccessWithoutPassingIamId() throws Exception {
        String uri = "/v1/adherence";
        String initialIamId = null;
        Long iamId = 123L;
        String entityId = "123";
        Boolean logsRequired = false;
        Long clientId = 4L;
        List<AccessMapping> accessMapping = new ArrayList<>();
        accessMapping.add(new AccessMapping(clientId, iamId, entityId));
        List<Long> iamIdList = new ArrayList<>();
        iamIdList.add(iamId);

        List<AdherenceData> adherenceDataList = new ArrayList<>();
        AdherenceData adherenceData = new AdherenceData();
        adherenceData.setStartDate(Utils.convertStringToDate("2019-01-18 18:30:00"));
        adherenceDataList.add(adherenceData);

        AdherenceResponse adherenceResponse = new AdherenceResponse(entityId, adherenceDataList, "666666", Utils.convertStringToDate("2019-01-01 01:00:00"), 0, 0, 0, 0, 0, null,null,null, 0,0, new ArrayList<>(), "NONE", Utils.convertStringToDate("2019-01-01 01:00:00"));

        when(entityService.getAllIamMapping(clientId, entityId)).thenReturn(accessMapping);
        when(adherenceService.getAllAdherence(iamIdList, logsRequired)).thenReturn(adherenceResponse);
        when(adherenceService.getAdherenceFromCache(eq(Collections.singletonList(entityId)), anyString(), any())).thenReturn(new AdherenceCacheDataDto<>());

        mockMvc
                .perform(
                        MockMvcRequestBuilders
                                .get(uri)
                                .param("iamId", initialIamId)
                                .param("entityId", entityId)
                                .param("logsRequired", logsRequired.toString())
                                .header("X-IAM-Client-Id", clientId.toString())
                                .accept(MediaType.APPLICATION_JSON_VALUE)
                )
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.data.entityId").value(entityId))
                .andExpect(MockMvcResultMatchers.jsonPath("$.message").value(DATA_FETCH_SUCCESS));
    }

    @Test
    public void testSearchAdherenceInvalidSearchRequest() throws Exception {
        String uri = "/v1/adherence/search";
        List<String> entityIdList = new ArrayList<>();
        Long clientId = 4L;
        SearchAdherenceRequest adherenceRequest = new SearchAdherenceRequest(entityIdList,null);

        mockMvc
                .perform(
                        MockMvcRequestBuilders
                                .post(uri)
                                .contentType(MediaType.APPLICATION_JSON)
                                .content(Utils.asJsonString(adherenceRequest))
                                .header("X-IAM-Client-Id", clientId.toString())
                                .accept(MediaType.APPLICATION_JSON_VALUE)
                )
                .andExpect(status().isBadRequest())
                .andExpect(MockMvcResultMatchers.jsonPath("$.message").value(INVALID_ENTITY_LIST));
    }

    @Test
    public void testSearchAdherenceInvalidSizeSearchRequest() throws Exception {
        String uri = "/v1/adherence/search";
        List<String> entityIdList = new ArrayList<>(Collections.nCopies(Constants.MAX_BULK_ADHERENCE_SIZE+1,"1"));
        Long clientId = 4L;
        SearchAdherenceRequest adherenceRequest = new SearchAdherenceRequest(entityIdList,null);

        mockMvc
                .perform(
                        MockMvcRequestBuilders
                                .post(uri)
                                .contentType(MediaType.APPLICATION_JSON)
                                .content(Utils.asJsonString(adherenceRequest))
                                .header("X-IAM-Client-Id", clientId.toString())
                                .accept(MediaType.APPLICATION_JSON_VALUE)
                )
                .andExpect(status().isBadRequest())
                .andExpect(MockMvcResultMatchers.jsonPath("$.message").value(ENTITY_ID_LIST_SIZE_EXCEEDED));
    }

    @Test
    public void testSearchAdherenceSuccessWithoutEntityId() throws Exception {
        String uri = "/v1/adherence/search";
        List<String> entityIdList = new ArrayList<>();
        String entityId = "123";
        entityIdList.add(entityId);
        Long clientId = 4L;
        Long iamId = 123L;
        SearchAdherenceRequest adherenceRequest = new SearchAdherenceRequest(entityIdList,null);
        List<AccessMapping> accessMapping = new ArrayList<>();
        accessMapping.add(new AccessMapping(clientId, iamId, entityId));
        Map<String,List<AccessMapping>> accessMappingToEntityMap = new HashMap<>();
        accessMappingToEntityMap.put(null,accessMapping);
        List<Long> mappedIamList = new ArrayList<>();
        mappedIamList.add(iamId);
        AdherenceResponse adherenceResponse = new AdherenceResponse(entityId, null, "666666", Utils.convertStringToDate("2019-01-01 01:00:00"), 0, 0, 0, 0, 0, null,null,null, 0,0, new ArrayList<>(), "NONE", Utils.convertStringToDate("2019-01-01 01:00:00"));

        when(entityService.getMultiEntityMapping(adherenceRequest.getEntityIdList())).thenReturn(accessMappingToEntityMap);
        when(adherenceService.getAdherenceBulk(accessMappingToEntityMap, null)).thenReturn(Arrays.asList(adherenceResponse));

        mockMvc
                .perform(
                        MockMvcRequestBuilders
                                .post(uri)
                                .contentType(MediaType.APPLICATION_JSON)
                                .content(Utils.asJsonString(adherenceRequest))
                                .header("X-IAM-Client-Id", clientId.toString())
                                .accept(MediaType.APPLICATION_JSON_VALUE)
                )
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.message").value(DATA_FETCH_SUCCESS));
    }

    @Test
    public void testSearchAdherenceSuccess() throws Exception {
        String uri = "/v1/adherence/search";
        List<String> entityIdList = new ArrayList<>();
        String entityId = "123";
        entityIdList.add(entityId);
        Long clientId = 4L;
        Long iamId = 123L;
        SearchAdherenceRequest adherenceRequest = new SearchAdherenceRequest(entityIdList,null);
        List<AccessMapping> accessMapping = new ArrayList<>();
        accessMapping.add(new AccessMapping(clientId, iamId, entityId));
        Map<String,List<AccessMapping>> accessMappingToEntityMap = new HashMap<>();
        accessMappingToEntityMap.put(entityId,accessMapping);
        List<Long> mappedIamList = new ArrayList<>();
        mappedIamList.add(iamId);

        List<AdherenceData> adherenceDataList = new ArrayList<>();
        AdherenceData adherenceData = new AdherenceData();
        adherenceData.setStartDate(Utils.convertStringToDate("2019-01-18 18:30:00"));
        adherenceDataList.add(adherenceData);

        AdherenceResponse adherenceResponse = new AdherenceResponse(entityId, adherenceDataList, "666666", Utils.convertStringToDate("2019-01-01 01:00:00"), 0, 0, 0, 0, 0, null,null,null, 0,0, new ArrayList<>(), "NONE", Utils.convertStringToDate("2019-01-01 01:00:00"));

        when(entityService.getMultiEntityMapping(adherenceRequest.getEntityIdList())).thenReturn(accessMappingToEntityMap);
        when(adherenceService.getAdherenceBulk(accessMappingToEntityMap, null)).thenReturn(Arrays.asList(adherenceResponse));

        mockMvc
                .perform(
                        MockMvcRequestBuilders
                                .post(uri)
                                .contentType(MediaType.APPLICATION_JSON)
                                .content(Utils.asJsonString(adherenceRequest))
                                .header("X-IAM-Client-Id", clientId.toString())
                                .accept(MediaType.APPLICATION_JSON_VALUE)
                )
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.data.adherenceResponseList[0].entityId").value(entityId))
                .andExpect(MockMvcResultMatchers.jsonPath("$.message").value(DATA_FETCH_SUCCESS));
    }

    @Test
    public void testSearchAdherenceWithFiltersSuccess() throws Exception {
        String uri = "/v1/adherence/search";
        List<String> entityIdList = new ArrayList<>();
        String entityId = "123";
        entityIdList.add(entityId);
        String from = "2019-08-31 18:30:00";
        String to = "2019-10-31 18:30:00";
        Long clientId = 4L;
        Long iamId = 123L;
        RangeFilters rangeFilter = new RangeFilters(RangeFilterType.LAST_DOSAGE,from,to);
        List<RangeFilters> filters = new ArrayList<>();
        filters.add(rangeFilter);
        SearchAdherenceRequest adherenceRequest = new SearchAdherenceRequest(entityIdList,filters);
        List<AccessMapping> accessMapping = new ArrayList<>();
        accessMapping.add(new AccessMapping(clientId, iamId, entityId));
        Map<String,List<AccessMapping>> accessMappingToEntityMap = new HashMap<>();
        accessMappingToEntityMap.put(entityId,accessMapping);
        List<Long> mappedIamList = new ArrayList<>();
        mappedIamList.add(iamId);

        List<AdherenceData> adherenceDataList = new ArrayList<>();
        AdherenceData adherenceData = new AdherenceData();
        adherenceData.setStartDate(Utils.convertStringToDate("2019-01-18 18:30:00"));
        adherenceDataList.add(adherenceData);

        AdherenceResponse adherenceResponse = new AdherenceResponse(entityId, adherenceDataList, "666666", Utils.convertStringToDate("2019-01-01 01:00:00"), 0, 0, 0, 0, 0, null,null,null, 0,0, new ArrayList<>(), "NONE", Utils.convertStringToDate("2019-01-01 01:00:00"));

        List<AdherenceResponse> allEntityResponseList = new ArrayList<>();
        allEntityResponseList.add(adherenceResponse);

        when(entityService.getMultiEntityMapping(adherenceRequest.getEntityIdList())).thenReturn(accessMappingToEntityMap);

        mockMvc
                .perform(
                        MockMvcRequestBuilders
                                .post(uri)
                                .contentType(MediaType.APPLICATION_JSON)
                                .content(Utils.asJsonString(adherenceRequest))
                                .header("X-IAM-Client-Id", clientId.toString())
                                .accept(MediaType.APPLICATION_JSON_VALUE)
                )
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.message").value(DATA_FETCH_SUCCESS));
    }

    @Test
    public void testSearchAdherenceFiltersLastDosageWrongFormatException() throws Exception {
        String uri = "/v1/adherence/search";
        List<String> entityIdList = new ArrayList<>();
        String entityId = "123";
        entityIdList.add(entityId);
        String from = "2019-08-31 18:30:00";
        String to = "2019-17-31 18:30:00";
        Long clientId = 4L;
        RangeFilters rangeFilter = new RangeFilters(RangeFilterType.LAST_DOSAGE,from,to);
        List<RangeFilters> filters = new ArrayList<>();
        filters.add(rangeFilter);
        SearchAdherenceRequest adherenceRequest = new SearchAdherenceRequest(entityIdList,filters);

        mockMvc
                .perform(
                        MockMvcRequestBuilders
                                .post(uri)
                                .contentType(MediaType.APPLICATION_JSON)
                                .content(Utils.asJsonString(adherenceRequest))
                                .header("X-IAM-Client-Id", clientId.toString())
                                .accept(MediaType.APPLICATION_JSON_VALUE)
                )
                .andExpect(status().is4xxClientError())
                .andExpect(MockMvcResultMatchers.jsonPath("$.message").value(INVALID_DATE_FORMAT));
    }

    @Test
    public void testSearchAdherenceFiltersAdherenceStatisticsWrongFormatException() throws Exception {
        String uri = "/v1/adherence/search";
        List<String> entityIdList = new ArrayList<>();
        String entityId = "123";
        entityIdList.add(entityId);
        String from = "2019-08-31 18:30:00";
        String to = "2019-17-31 18:30:00";
        Long clientId = 4L;
        RangeFilters rangeFilter = new RangeFilters(RangeFilterType.ADHERENCE_STATISTICS,from,to);
        List<RangeFilters> filters = new ArrayList<>();
        filters.add(rangeFilter);
        SearchAdherenceRequest adherenceRequest = new SearchAdherenceRequest(entityIdList,filters);

        mockMvc
                .perform(
                        MockMvcRequestBuilders
                                .post(uri)
                                .contentType(MediaType.APPLICATION_JSON)
                                .content(Utils.asJsonString(adherenceRequest))
                                .header("X-IAM-Client-Id", clientId.toString())
                                .accept(MediaType.APPLICATION_JSON_VALUE)
                )
                .andExpect(status().is4xxClientError())
                .andExpect(MockMvcResultMatchers.jsonPath("$.message").value(INVALID_DATE_FORMAT));
    }

    @Test
    public void testSearchAdherenceFiltersMissedDosesWrongFormatException() throws Exception {
        String uri = "/v1/adherence/search";
        List<String> entityIdList = new ArrayList<>();
        String entityId = "123";
        entityIdList.add(entityId);
        Long clientId = 4L;
        String fromInt = "1x";
        String toInt = "12";
        RangeFilters rangeFilter = new RangeFilters(RangeFilterType.CONSECUTIVE_MISSED_DOSES,fromInt,toInt);
        List<RangeFilters> filters = new ArrayList<>();
        filters.add(rangeFilter);
        SearchAdherenceRequest adherenceRequest = new SearchAdherenceRequest(entityIdList,filters);

        mockMvc
                .perform(
                        MockMvcRequestBuilders
                                .post(uri)
                                .contentType(MediaType.APPLICATION_JSON)
                                .content(Utils.asJsonString(adherenceRequest))
                                .header("X-IAM-Client-Id", clientId.toString())
                                .accept(MediaType.APPLICATION_JSON_VALUE)
                )
                .andExpect(status().is4xxClientError())
                .andExpect(MockMvcResultMatchers.jsonPath("$.message").value(INVALID_NUMBER));
    }

    @Test
    public void testRecordAdherenceEntityNotFound() throws Exception {
        String uri = "/v1/adherence/record";
        String entityId = "123";
        String date = "2019-08-31 18:30:00";
        Long clientId = 4L;
        Long iamId = 123L;
        Map<String, Character> dmvMap = new HashMap<>();
        dmvMap.put("2019-01-02 18:30:00.000000", '4');
        RecordAdherenceRequest adherenceRequest = new RecordAdherenceRequest(iamId,'4',entityId,date,4L,dmvMap);
        List<AccessMapping> accessMapping = new ArrayList<>();

        when(entityService.getAllIamMapping(clientId, entityId)).thenReturn(accessMapping);

        mockMvc
                .perform(
                        MockMvcRequestBuilders
                                .post(uri)
                                .contentType(MediaType.APPLICATION_JSON)
                                .content(Utils.asJsonString(adherenceRequest))
                                .header("X-IAM-Client-Id", clientId.toString())
                                .accept(MediaType.APPLICATION_JSON_VALUE)
                )
                .andExpect(status().isNotFound())
                .andExpect(MockMvcResultMatchers.jsonPath("$.message").value("entity with id " + adherenceRequest.getEntityId() + " was not found"));
    }

    @Test
    public void testRecordAdherenceSuccess() throws Exception {
        String uri = "/v1/adherence/record";
        String entityId = "123";
        String date = "2019-08-31 18:30:00";
        Long clientId = 4L;
        Long iamId = 123L;
        Map<String, Character> dmvMap = new HashMap<>();
        dmvMap.put("2019-01-02 18:30:00.000000", '4');
        RecordAdherenceRequest adherenceRequest = new RecordAdherenceRequest(iamId,'4',entityId,date,4L,dmvMap);
        List<AccessMapping> accessMapping = new ArrayList<>();
        accessMapping.add(new AccessMapping(clientId, iamId, entityId));
        List<Long> iamIdList = new ArrayList<>();
        iamIdList.add(iamId);
        RecordAdherenceResponse adherenceResponse = new RecordAdherenceResponse(iamIdList);

        when(entityService.getAllIamMapping(clientId, entityId)).thenReturn(accessMapping);

        mockMvc
                .perform(
                        MockMvcRequestBuilders
                                .post(uri)
                                .contentType(MediaType.APPLICATION_JSON)
                                .content(Utils.asJsonString(adherenceRequest))
                                .header("X-IAM-Client-Id", clientId.toString())
                                .accept(MediaType.APPLICATION_JSON_VALUE)
                )
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.message").value(ENTITY_REQUEST_ACCEPTED));
    }

    @Test
    public void testRecordAdherenceEntityBulkNotFound() throws Exception {
        String uri = "/v1/adherence/record/bulk";
        String entityId = "123";
        String date = "2019-08-31 18:30:00";
        Long clientId = 4L;
        Long iamId = 123L;
        Map<String, Character> dmvMap = new HashMap<>();
        dmvMap.put("2019-01-02 18:30:00.000000", '4');
        RecordAdherenceRequest adherenceRequest = new RecordAdherenceRequest(iamId,'4',entityId,date,4L,dmvMap);
        ListWrapper<RecordAdherenceRequest> request = new ListWrapper<>(Arrays.asList(adherenceRequest));

        List<AccessMapping> accessMapping = new ArrayList<>();

        mockMvc
                .perform(
                        MockMvcRequestBuilders
                                .post(uri)
                                .contentType(MediaType.APPLICATION_JSON)
                                .content(Utils.asJsonString(request))
                                .header("X-IAM-Client-Id", clientId.toString())
                                .accept(MediaType.APPLICATION_JSON_VALUE)
                )
                .andExpect(status().isNotFound())
                .andExpect(MockMvcResultMatchers.jsonPath("$.message").value("entity ids not found"));
    }

    @Test
    public void testRecordAdherenceEntityBulkEmptyRequest() throws Exception {
        String uri = "/v1/adherence/record/bulk";
        ListWrapper<RecordAdherenceRequest> request = new ListWrapper<>();

        mockMvc
                .perform(
                        MockMvcRequestBuilders
                                .post(uri)
                                .contentType(MediaType.APPLICATION_JSON)
                                .content(Utils.asJsonString(request))
                                .header("X-IAM-Client-Id", "29")
                                .accept(MediaType.APPLICATION_JSON_VALUE)
                )
                .andExpect(status().isBadRequest())
                .andExpect(MockMvcResultMatchers.jsonPath("$.message").value("request list cannot be empty!"));
    }


    @Test
    public void testRecordAdherenceEntityBulkSuccess() throws Exception {
        String uri = "/v1/adherence/record/bulk";
        String entityId = "123";
        String date = "2019-08-31 18:30:00";
        Long clientId = 4L;
        Long iamId = 123L;
        Map<String, Character> dmvMap = new HashMap<>();
        dmvMap.put("2019-01-02 18:30:00.000000", '4');
        RecordAdherenceRequest adherenceRequest = new RecordAdherenceRequest(iamId,'4',entityId,date,4L,dmvMap);
        ListWrapper<RecordAdherenceRequest> request = new ListWrapper<>(Arrays.asList(adherenceRequest));

        List<AccessMapping> accessMapping = new ArrayList<>();
        accessMapping.add(new AccessMapping(clientId, iamId, entityId));

        List<Long> iamIdList = new ArrayList<>();
        iamIdList.add(iamId);

        RecordAdherenceResponse adherenceResponse = new RecordAdherenceResponse(iamIdList);

        when(entityService.getIamMappingByEntityId(any(), anyList())).thenReturn(accessMapping);
        when(adherenceService.recordAdherence(any(), anyList())).thenReturn(adherenceResponse);

        mockMvc
                .perform(
                        MockMvcRequestBuilders
                                .post(uri)
                                .contentType(MediaType.APPLICATION_JSON)
                                .content(Utils.asJsonString(request))
                                .header("X-IAM-Client-Id", clientId.toString())
                                .accept(MediaType.APPLICATION_JSON_VALUE)
                )
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.message").value(ENTITY_REQUEST_ACCEPTED));
    }

    @Test
    public void testRegenerateAdherenceSuccessWithoutIamIds() throws Exception {
        String uri = "/v1/adherence/regenerate";
        String entityId = "123";
        List<String> entityIdList = new ArrayList<>();
        entityIdList.add(entityId);
        Long clientId = 4L;
        Long iamId = 123L;
        List<Long> iamIdList = new ArrayList<>();
        //iamIdList.add(iamId);
        RegenerateAdherenceRequest adherenceRequest = new RegenerateAdherenceRequest(entityIdList,iamIdList);
        List<AccessMapping> accessMapping = new ArrayList<>();
        accessMapping.add(new AccessMapping(clientId, iamId, entityId));

        when(entityService.getIamMappingByEntityId(clientId, adherenceRequest.getEntityIdList())).thenReturn(accessMapping);

        mockMvc
                .perform(
                        MockMvcRequestBuilders
                                .post(uri)
                                .contentType(MediaType.APPLICATION_JSON)
                                .content(Utils.asJsonString(adherenceRequest))
                                .header("X-IAM-Client-Id", clientId.toString())
                                .accept(MediaType.APPLICATION_JSON_VALUE)
                )
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.message").value(REGENERATE_ADHERENCE_SUCCESS));
    }

    @Test
    public void testRegenerateAdherenceSuccess() throws Exception {
        String uri = "/v1/adherence/regenerate";
        String entityId = "123";
        List<String> entityIdList = new ArrayList<>();
        entityIdList.add(entityId);
        Long clientId = 4L;
        Long iamId = 123L;
        List<Long> iamIdList = new ArrayList<>();
        iamIdList.add(iamId);
        RegenerateAdherenceRequest adherenceRequest = new RegenerateAdherenceRequest(entityIdList,iamIdList);
        List<AccessMapping> accessMapping = new ArrayList<>();
        accessMapping.add(new AccessMapping(clientId, iamId, entityId));

        doNothing().when(applicationEventPublisher).publishEvent(any(RegenerateAdherenceRequest.class));

        mockMvc
                .perform(
                        MockMvcRequestBuilders
                                .post(uri)
                                .contentType(MediaType.APPLICATION_JSON)
                                .content(Utils.asJsonString(adherenceRequest))
                                .header("X-IAM-Client-Id", clientId.toString())
                                .accept(MediaType.APPLICATION_JSON_VALUE)
                )
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.message").value(REGENERATE_ADHERENCE_SUCCESS));
    }

    @Test
    public void testRegenerateAdherenceEntityIdNotFound() throws Exception {
        String uri = "/v1/adherence/regenerate";
        String entityId = "123";
        List<String> entityIdList = new ArrayList<>();
        Long clientId = 4L;
        Long iamId = 123L;
        List<Long> iamIdList = new ArrayList<>();
        RegenerateAdherenceRequest adherenceRequest = new RegenerateAdherenceRequest(entityIdList,iamIdList);
        List<AccessMapping> accessMapping = new ArrayList<>();
        accessMapping.add(new AccessMapping(clientId, iamId, entityId));

        when(entityService.getIamMappingByEntityId(clientId, adherenceRequest.getEntityIdList())).thenReturn(accessMapping);

        mockMvc
                .perform(
                        MockMvcRequestBuilders
                                .post(uri)
                                .contentType(MediaType.APPLICATION_JSON)
                                .content(Utils.asJsonString(adherenceRequest))
                                .header("X-IAM-Client-Id", clientId.toString())
                                .accept(MediaType.APPLICATION_JSON_VALUE)
                )
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.success").value(false))
                .andExpect(MockMvcResultMatchers.jsonPath("$.message").value(ENTITY_IDS_NOT_FOUND));
    }

    @Test
    public void testRegenerateAdherenceForAllAdherenceTypeNotFound() throws Exception {
        String uri = "/v1/adherence/regenerate/all";
        Long adherenceType = null;
        Long size = 100L;
        Long clientId = 4L;
        RegenerateAdherenceForAllRequest adherenceRequest = new RegenerateAdherenceForAllRequest(adherenceType,size);

        mockMvc
                .perform(
                        MockMvcRequestBuilders
                                .post(uri)
                                .contentType(MediaType.APPLICATION_JSON)
                                .content(Utils.asJsonString(adherenceRequest))
                                .header("X-IAM-Client-Id", clientId.toString())
                                .accept(MediaType.APPLICATION_JSON_VALUE)
                )
                .andExpect(status().isBadRequest())
                .andExpect(MockMvcResultMatchers.jsonPath("$.message").value(ADHERENCE_TYPE_MISSING));
    }

    @Test
    public void testRegenerateAdherenceForAllExceedSize() throws Exception {
        String uri = "/v1/adherence/regenerate/all";
        Long adherenceType = 3L;
        Long size = 10001L;
        Long clientId = 4L;
        RegenerateAdherenceForAllRequest adherenceRequest = new RegenerateAdherenceForAllRequest(adherenceType,size);

        mockMvc
                .perform(
                        MockMvcRequestBuilders
                                .post(uri)
                                .contentType(MediaType.APPLICATION_JSON)
                                .content(Utils.asJsonString(adherenceRequest))
                                .header("X-IAM-Client-Id", clientId.toString())
                                .accept(MediaType.APPLICATION_JSON_VALUE)
                )
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.success").value(false))
                .andExpect(MockMvcResultMatchers.jsonPath("$.data").value(EXCEED_SIZE_LIMIT));
    }

    @Test
    public void testRegenerateAdherenceForAllEntityIdNotFound() throws Exception {
        String uri = "/v1/adherence/regenerate/all";
        Long adherenceType = 3L;
        Long size = null;
        List<Long> iamIdList = new ArrayList<>();
        Long clientId = 4L;
        RegenerateAdherenceForAllRequest adherenceRequest = new RegenerateAdherenceForAllRequest(adherenceType,size);

        mockMvc
                .perform(
                        MockMvcRequestBuilders
                                .post(uri)
                                .contentType(MediaType.APPLICATION_JSON)
                                .content(Utils.asJsonString(adherenceRequest))
                                .header("X-IAM-Client-Id", clientId.toString())
                                .accept(MediaType.APPLICATION_JSON_VALUE)
                )
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.success").value(false))
                .andExpect(MockMvcResultMatchers.jsonPath("$.data").value(NO_ENTITY_FOUND));
    }

    @Test
    public void testRegenerateAdherenceForAllSuccess() throws Exception {
        String uri = "/v1/adherence/regenerate/all";
        Long adherenceType = 3L;
        Long size = 100L;
        Long iamId = 123L;
        List<Long> iamIdList = new ArrayList<>();
        iamIdList.add(iamId);
        Long clientId = 4L;
        RegenerateAdherenceForAllRequest adherenceRequest = new RegenerateAdherenceForAllRequest(adherenceType,size);
        
        doNothing().when(applicationEventPublisher).publishEvent(any(RegenerateAdherenceRequest.class));
        when(adherenceService.getIamIdsByAdherenceTech(adherenceRequest.getAdherenceType(), adherenceRequest.getSize())).thenReturn(iamIdList);

        mockMvc
                .perform(
                        MockMvcRequestBuilders
                                .post(uri)
                                .contentType(MediaType.APPLICATION_JSON)
                                .content(Utils.asJsonString(adherenceRequest))
                                .header("X-IAM-Client-Id", clientId.toString())
                                .accept(MediaType.APPLICATION_JSON_VALUE)
                )
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.success").value(true))
                .andExpect(MockMvcResultMatchers.jsonPath("$.data").value(REGENERATE_ADHERENCE_SUCCESS));
    }

    @Test
    public void testRegenerateAdherenceForAllSuccessWithoutSize() throws Exception {
        String uri = "/v1/adherence/regenerate/all";
        Long adherenceType = 3L;
        Long size = null;
        Long iamId = 123L;
        List<Long> iamIdList = new ArrayList<>();
        iamIdList.add(iamId);
        Long clientId = 4L;
        Long finalSize = 1000L;
        RegenerateAdherenceForAllRequest adherenceRequest = new RegenerateAdherenceForAllRequest(adherenceType,size);

        doNothing().when(applicationEventPublisher).publishEvent(any(RegenerateAdherenceRequest.class));
        when(adherenceService.getIamIdsByAdherenceTech(adherenceRequest.getAdherenceType(), finalSize)).thenReturn(iamIdList);

        mockMvc
                .perform(
                        MockMvcRequestBuilders
                                .post(uri)
                                .contentType(MediaType.APPLICATION_JSON)
                                .content(Utils.asJsonString(adherenceRequest))
                                .header("X-IAM-Client-Id", clientId.toString())
                                .accept(MediaType.APPLICATION_JSON_VALUE)
                )
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.success").value(true))
                .andExpect(MockMvcResultMatchers.jsonPath("$.data").value(REGENERATE_ADHERENCE_SUCCESS));
    }

    @Test
    public void getPositiveEventsCountTestWithoutFallback() throws Exception {
        PositiveAdherenceRequest positiveAdherenceRequest = new PositiveAdherenceRequest();
        positiveAdherenceRequest.setEntityIdList(Arrays.asList("1"));
        when(adherenceService.countPositiveCallsForToday(anyList(), eq(false))).thenReturn(new PositiveAdherenceDto(1L, 5L, 1L, new ArrayList<>()));
        mockMvc
            .perform(
                MockMvcRequestBuilders
                    .post("/v1/adherence/positive-events")
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(Utils.asJsonString(positiveAdherenceRequest))
                    .header("X-IAM-Client-Id", "29")
                    .accept(MediaType.APPLICATION_JSON_VALUE)
            )
            .andExpect(status().isOk())
            .andExpect(MockMvcResultMatchers.jsonPath("$.success").value(true))
            .andExpect(MockMvcResultMatchers.jsonPath("$.data.manualCount").value("1"))
            .andExpect(MockMvcResultMatchers.jsonPath("$.data.digitalCount").value("5"))
            .andExpect(MockMvcResultMatchers.jsonPath("$.data.missedCount").value("1"));
    }

    @Test
    public void getPositiveEventsCountTestWithFallback() throws Exception {
        adherenceController.setFallbackForCache(true);
        PositiveAdherenceRequest positiveAdherenceRequest = new PositiveAdherenceRequest();
        positiveAdherenceRequest.setEntityIdList(Arrays.asList("1"));
        List<String> entityIdList = new ArrayList<>();
        String entityId = "123";
        entityIdList.add(entityId);
        Long clientId = 4L;
        Long iamId = 123L;
        List<AccessMapping> accessMapping = new ArrayList<>();
        accessMapping.add(new AccessMapping(clientId, iamId, entityId));
        Map<String,List<AccessMapping>> accessMappingToEntityMap = new HashMap<>();
        accessMappingToEntityMap.put(entityId,accessMapping);
        List<AdherenceData> adherenceDataList = new ArrayList<>();
        AdherenceData adherenceData = new AdherenceData();
        adherenceData.setStartDate(Utils.convertStringToDate("2019-01-18 18:30:00"));
        adherenceDataList.add(adherenceData);
        AdherenceResponse adherenceResponse = new AdherenceResponse(entityId, adherenceDataList, "666664", Utils.convertStringToDate("2019-01-01 01:00:00"), 0, 0, 0, 0, 0, null,null,null, 0,0, new ArrayList<>(), "NONE", Utils.convertStringToDate("2019-01-01 01:00:00"));
        AdherenceResponse adherenceResponse2 = new AdherenceResponse(entityId, adherenceDataList, "666664", Utils.convertStringToDate("2019-01-01 01:00:00"), 0, 0, 0, 0, 0, null,null,null, 0,0, new ArrayList<>(), "NONE", Utils.convertStringToDate("2019-01-01 01:00:00"));
        AdherenceResponse adherenceResponse3 = new AdherenceResponse(entityId, adherenceDataList, "666669", Utils.convertStringToDate("2019-01-01 01:00:00"), 0, 0, 0, 0, 0, null,null,null, 0,0, new ArrayList<>(), "NONE", Utils.convertStringToDate("2019-01-01 01:00:00"));
        AdherenceResponse adherenceResponse4 = new AdherenceResponse(entityId, adherenceDataList, "666666", Utils.convertStringToDate("2019-01-01 01:00:00"), 0, 0, 0, 0, 0, null,null,null, 0,0, new ArrayList<>(), "NONE", Utils.convertStringToDate("2019-01-01 01:00:00"));
        AdherenceResponse adherenceResponse5 = new AdherenceResponse(entityId, adherenceDataList, "666662", Utils.convertStringToDate("2019-01-01 01:00:00"), 0, 0, 0, 0, 0, null,null,null, 0,0, new ArrayList<>(), "NONE", Utils.convertStringToDate("2019-01-01 01:00:00"));

        when(adherenceService.countPositiveCallsForToday(anyList(), eq(true))).thenReturn(new PositiveAdherenceDto(1L, 5L, 1L, Arrays.asList("1")));
        when(entityService.getMultiEntityMapping(anyList())).thenReturn(accessMappingToEntityMap);
        when(adherenceService.getAdherenceBulk(accessMappingToEntityMap, null)).thenReturn(Arrays.asList(adherenceResponse, adherenceResponse2, adherenceResponse3, adherenceResponse4, adherenceResponse5));

        mockMvc
            .perform(
                MockMvcRequestBuilders
                    .post("/v1/adherence/positive-events")
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(Utils.asJsonString(positiveAdherenceRequest))
                    .header("X-IAM-Client-Id", "29")
                    .accept(MediaType.APPLICATION_JSON_VALUE)
            )
            .andExpect(status().isOk())
            .andExpect(MockMvcResultMatchers.jsonPath("$.success").value(true))
            .andExpect(MockMvcResultMatchers.jsonPath("$.data.manualCount").value("2"))
            .andExpect(MockMvcResultMatchers.jsonPath("$.data.digitalCount").value("7"))
            .andExpect(MockMvcResultMatchers.jsonPath("$.data.missedCount").value("2"));
    }

    @Test
    public void testAverageAdherenceInvalidSearchRequest() throws Exception {
        String uri = "/v1/adherence/average";
        List<String> entityIdList = new ArrayList<>();
        Long clientId = 4L;
        List<AverageAdherenceIntervals> allIntervals = Arrays.stream(AverageAdherenceIntervals.values()).collect(Collectors.toList());
        AverageAdherenceRequest avgAdherenceRequest = new AverageAdherenceRequest(entityIdList,false, allIntervals);

        mockMvc
                .perform(
                        MockMvcRequestBuilders
                                .post(uri)
                                .contentType(MediaType.APPLICATION_JSON)
                                .content(Utils.asJsonString(avgAdherenceRequest))
                                .header("X-IAM-Client-Id", clientId.toString())
                                .accept(MediaType.APPLICATION_JSON_VALUE)
                )
                .andExpect(status().isBadRequest())
                .andExpect(MockMvcResultMatchers.jsonPath("$.message").value(INVALID_ENTITY_LIST));
    }

    private Map<AverageAdherenceIntervals, IntervalAdherencePercentDto> getDummyAverageAdherenceIntervals () {
        HashMap<AverageAdherenceIntervals, IntervalAdherencePercentDto> dummyAverageAdherenceIntervals = new HashMap<>();
        dummyAverageAdherenceIntervals.put(AverageAdherenceIntervals.ALL_TIME, new IntervalAdherencePercentDto(5,15));
        dummyAverageAdherenceIntervals.put(AverageAdherenceIntervals.LAST_7_DAYS, new IntervalAdherencePercentDto(6,16));
        dummyAverageAdherenceIntervals.put(AverageAdherenceIntervals.LAST_30_DAYS, new IntervalAdherencePercentDto(7, 17));
        dummyAverageAdherenceIntervals.put(AverageAdherenceIntervals.LAST_90_DAYS, new IntervalAdherencePercentDto(8,18));
        dummyAverageAdherenceIntervals.put(AverageAdherenceIntervals.LAST_365_DAYS, new IntervalAdherencePercentDto(9,19));
        return dummyAverageAdherenceIntervals;
    }

    @Test
    public void testAverageAdherenceSuccess() throws Exception {
        String uri = "/v1/adherence/average";
        List<String> entityIdList = new ArrayList<>();
        String entityId = "123";
        entityIdList.add(entityId);
        Long clientId = 4L;
        Long iamId = 123L;
        List<AverageAdherenceIntervals> allIntervals = Arrays.stream(AverageAdherenceIntervals.values()).collect(Collectors.toList());
        AverageAdherenceRequest averageAdherenceRequest = new AverageAdherenceRequest(entityIdList,false, allIntervals);
        List<AccessMapping> accessMapping = new ArrayList<>();
        accessMapping.add(new AccessMapping(clientId, iamId, entityId));
        Map<String,List<AccessMapping>> accessMappingToEntityMap = new HashMap<>();
        accessMappingToEntityMap.put(entityId,accessMapping);

        double digitalAdherence=10,totalAdherence=20;
        Map<AverageAdherenceIntervals, IntervalAdherencePercentDto> dummyAverageAdherenceIntervals = getDummyAverageAdherenceIntervals();
        AllAvgAdherenceResponse avgAdherenceResponse = new AllAvgAdherenceResponse(digitalAdherence, totalAdherence, dummyAverageAdherenceIntervals ,null);

        when(entityService.getMultiEntityMapping(averageAdherenceRequest.getEntityIdList())).thenReturn(accessMappingToEntityMap);
        when(adherenceService.getAverageAdherence(accessMappingToEntityMap, false, allIntervals)).thenReturn(avgAdherenceResponse);

        mockMvc
                .perform(
                        MockMvcRequestBuilders
                                .post(uri)
                                .contentType(MediaType.APPLICATION_JSON)
                                .content(Utils.asJsonString(averageAdherenceRequest))
                                .header("X-IAM-Client-Id", clientId.toString())
                                .accept(MediaType.APPLICATION_JSON_VALUE)
                )
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.data.digitalAdherence").value(digitalAdherence))
                .andExpect(MockMvcResultMatchers.jsonPath("$.data.totalAdherence").value(totalAdherence))
                .andExpect(MockMvcResultMatchers
                        .jsonPath("$.data.averageAdherenceIntervals.LAST_7_DAYS.digitalAdherence")
                        .value(dummyAverageAdherenceIntervals.get(AverageAdherenceIntervals.LAST_7_DAYS).getDigitalAdherence()))
                .andExpect(MockMvcResultMatchers
                        .jsonPath("$.data.averageAdherenceIntervals.LAST_7_DAYS.totalAdherence")
                        .value(dummyAverageAdherenceIntervals.get(AverageAdherenceIntervals.LAST_7_DAYS).getTotalAdherence()))
                .andExpect(MockMvcResultMatchers
                        .jsonPath("$.data.averageAdherenceIntervals.LAST_30_DAYS.digitalAdherence")
                        .value(dummyAverageAdherenceIntervals.get(AverageAdherenceIntervals.LAST_30_DAYS).getDigitalAdherence()))
                .andExpect(MockMvcResultMatchers
                        .jsonPath("$.data.averageAdherenceIntervals.LAST_30_DAYS.totalAdherence")
                        .value(dummyAverageAdherenceIntervals.get(AverageAdherenceIntervals.LAST_30_DAYS).getTotalAdherence()))
                .andExpect(MockMvcResultMatchers
                        .jsonPath("$.data.averageAdherenceIntervals.LAST_90_DAYS.digitalAdherence")
                        .value(dummyAverageAdherenceIntervals.get(AverageAdherenceIntervals.LAST_90_DAYS).getDigitalAdherence()))
                .andExpect(MockMvcResultMatchers
                        .jsonPath("$.data.averageAdherenceIntervals.LAST_90_DAYS.totalAdherence")
                        .value(dummyAverageAdherenceIntervals.get(AverageAdherenceIntervals.LAST_90_DAYS).getTotalAdherence()))
                .andExpect(MockMvcResultMatchers
                        .jsonPath("$.data.averageAdherenceIntervals.LAST_365_DAYS.digitalAdherence")
                        .value(dummyAverageAdherenceIntervals.get(AverageAdherenceIntervals.LAST_365_DAYS).getDigitalAdherence()))
                .andExpect(MockMvcResultMatchers
                        .jsonPath("$.data.averageAdherenceIntervals.LAST_365_DAYS.totalAdherence")
                        .value(dummyAverageAdherenceIntervals.get(AverageAdherenceIntervals.LAST_365_DAYS).getTotalAdherence()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.message").value(AVERAGE_ADHERENCE_SUCCESS));
    }

    @Test
    public void testAverageAdherenceNoEntityFound() throws Exception {
        String uri = "/v1/adherence/average";
        List<String> entityIdList = new ArrayList<>();
        String entityId1 = "123";
        String entityId2 = "234";
        entityIdList.add(entityId1);
        entityIdList.add(entityId2);
        Long clientId = 4L;
        List<AverageAdherenceIntervals> allIntervals = Arrays.stream(AverageAdherenceIntervals.values()).collect(Collectors.toList());
        AverageAdherenceRequest averageAdherenceRequest = new AverageAdherenceRequest(entityIdList,false, allIntervals);
        Map<String,List<AccessMapping>> accessMappingToEntityMap = new HashMap<>();
        when(entityService.getMultiEntityMapping(averageAdherenceRequest.getEntityIdList())).thenReturn(accessMappingToEntityMap);

        mockMvc
                .perform(
                        MockMvcRequestBuilders
                                .post(uri)
                                .contentType(MediaType.APPLICATION_JSON)
                                .content(Utils.asJsonString(averageAdherenceRequest))
                                .header("X-IAM-Client-Id", clientId.toString())
                                .accept(MediaType.APPLICATION_JSON_VALUE)
                )
                .andExpect(status().isNotFound())
                .andExpect(MockMvcResultMatchers.jsonPath("$.message").value(NO_ENTITY_MAPPING_FOUND));
    }

    @Test
    public void testAverageAdherenceSizeExceededForPatientDetails() throws Exception {
        String uri = "/v1/adherence/average";
        boolean patientDetails = true;
        List<String> entityIdList = new ArrayList<>(Collections.nCopies(Constants.MAX_ENTITY_AVG_ADHERENCE_SIZE+1,"1"));
        Long clientId = 4L;
        List<AverageAdherenceIntervals> allIntervals = Arrays.stream(AverageAdherenceIntervals.values()).collect(Collectors.toList());
        AverageAdherenceRequest averageAdherenceRequest = new AverageAdherenceRequest(entityIdList, patientDetails, allIntervals);
        Map<String,List<AccessMapping>> accessMappingToEntityMap = new HashMap<>();

        mockMvc
                .perform(
                        MockMvcRequestBuilders
                                .post(uri)
                                .contentType(MediaType.APPLICATION_JSON)
                                .content(Utils.asJsonString(averageAdherenceRequest))
                                .header("X-IAM-Client-Id", clientId.toString())
                                .accept(MediaType.APPLICATION_JSON_VALUE)
                )
                .andExpect(status().isBadRequest())
                .andExpect(MockMvcResultMatchers.jsonPath("$.message").value(AVG_ADHERENCE_ENTITY_ID_LIST_SIZE_EXCEEDED));
    }

    @Test
    public void testAverageAdherenceSizeNotExceededWithoutPatientDetails() throws Exception {
        String uri = "/v1/adherence/average";
        boolean patientDetails = false;
        List<String> entityIdList = new ArrayList<>(Collections.nCopies(Constants.MAX_ENTITY_AVG_ADHERENCE_SIZE+1,"1"));
        Long clientId = 4L;
        List<AverageAdherenceIntervals> allIntervals = Arrays.stream(AverageAdherenceIntervals.values()).collect(Collectors.toList());
        AverageAdherenceRequest averageAdherenceRequest = new AverageAdherenceRequest(entityIdList, patientDetails, allIntervals);
        List<AccessMapping> accessMapping = new ArrayList<>();
        accessMapping.add(new AccessMapping(clientId, 1L, "1"));
        Map<String,List<AccessMapping>> accessMappingToEntityMap = new HashMap<>();
        accessMappingToEntityMap.put("1",accessMapping);

        double digitalAdherence=10,totalAdherence=20;
        AllAvgAdherenceResponse avgAdherenceResponse = new AllAvgAdherenceResponse(digitalAdherence, totalAdherence, null ,null);

        when(entityService.getMultiEntityMapping(averageAdherenceRequest.getEntityIdList())).thenReturn(accessMappingToEntityMap);
        when(adherenceService.getAverageAdherence(accessMappingToEntityMap, patientDetails, allIntervals)).thenReturn(avgAdherenceResponse);

        mockMvc
                .perform(
                        MockMvcRequestBuilders
                                .post(uri)
                                .contentType(MediaType.APPLICATION_JSON)
                                .content(Utils.asJsonString(averageAdherenceRequest))
                                .header("X-IAM-Client-Id", clientId.toString())
                                .accept(MediaType.APPLICATION_JSON_VALUE)
                )
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.message").value(AVERAGE_ADHERENCE_SUCCESS))
                .andExpect(MockMvcResultMatchers.jsonPath("$.data.digitalAdherence").value(digitalAdherence))
                .andExpect(MockMvcResultMatchers.jsonPath("$.data.totalAdherence").value(totalAdherence));
    }

    @Test
    public void getAdherenceCodeConfigTest() throws Exception {
        String uri = "/v1/adherence-config";
        List<AdherenceCodeConfigResponse> configResponses = new ArrayList<>();
        configResponses.add(new AdherenceCodeConfigResponse('2', "MISSED", new AdherenceCodeExtra("#990000"), "manually_reported_missed_dose" ));
        when(adherenceService.getAdherenceColorCodeConfig()).thenReturn(configResponses);

        mockMvc
            .perform(
                MockMvcRequestBuilders
                    .get(uri)
                    .contentType(MediaType.APPLICATION_JSON)
                    .header("X-IAM-Client-Id", "29")
                    .accept(MediaType.APPLICATION_JSON_VALUE)
            )
            .andExpect(status().isOk())
            .andExpect(MockMvcResultMatchers.jsonPath("$.message").value("data fetched successfully"))
            .andExpect(MockMvcResultMatchers.jsonPath("$.data[0].code").value(2))
            .andExpect(MockMvcResultMatchers.jsonPath("$.data[0].codeName").value("MISSED"))
            .andExpect(MockMvcResultMatchers.jsonPath("$.data[0].design.color").value("#990000"));

    }

    @Test
    public void getAdherenceConfigNamesTest() throws Exception {
        String uri = "/v1/adherence-config-names";
        List<String> configNameList = new ArrayList<>();
        configNameList.add("ADHERENCE_TECH");
        configNameList.add("COLOR_CODE");
        configNameList.add("SCHEDULE_TYPE");

        when(adherenceService.getAllConfigNames()).thenReturn(configNameList);

        mockMvc
                .perform(
                        MockMvcRequestBuilders
                                .get(uri)
                                .contentType(MediaType.APPLICATION_JSON)
                                .accept(MediaType.APPLICATION_JSON_VALUE)
                )
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.message").value("Adherence config names fetched successfully"))
                .andExpect(MockMvcResultMatchers.jsonPath("$.data").value(configNameList));

    }


    @Test
    public void getAdherenceGlobalConfigTest() throws Exception {
        String uri = "/v1/adherence-global-config";
        List<String> configNameList = new ArrayList<>();
        configNameList.add("ADHERENCE_TECH");
        AdherenceGlobalConfigRequest adherenceGlobalConfigRequest = new AdherenceGlobalConfigRequest(configNameList);

        List<GenericConfigResponse> adherenceTechResponseList = new ArrayList<>();
        adherenceTechResponseList.add(new GenericConfigResponse("MERM", 1));
        AdherenceGlobalConfigResponse adherenceGlobalConfigResponse = new AdherenceGlobalConfigResponse();
        adherenceGlobalConfigResponse.setAdherenceTechConfigResponseList(adherenceTechResponseList);

        when(adherenceService.getAdherenceGlobalConfig(any())).thenReturn(adherenceGlobalConfigResponse);

        mockMvc
                .perform(
                        MockMvcRequestBuilders
                                .post(uri)
                                .contentType(MediaType.APPLICATION_JSON)
                                .content(Utils.asJsonString(adherenceGlobalConfigRequest))
                                .accept(MediaType.APPLICATION_JSON_VALUE)
                )
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.message").value("Adherence global configs fetched successfully"))
                .andExpect(MockMvcResultMatchers.jsonPath("$.data.adherenceTechConfigResponseList.size()").value(adherenceGlobalConfigResponse.getAdherenceTechConfigResponseList().size()));
    }

}
