package tests.unit.controllers;

import com.everwell.iam.controllers.ClientController;
import com.everwell.iam.exceptions.CustomExceptionHandler;
import com.everwell.iam.exceptions.ValidationException;
import com.everwell.iam.models.http.responses.ClientResponse;
import com.everwell.iam.services.ClientService;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import tests.BaseTest;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

public class ClientControllerTest extends BaseTest {

    private MockMvc mockMvc;

    @Mock
    private ClientService clientService;

    @InjectMocks
    private ClientController clientController;

    @Before
    public void setup() {
        mockMvc = MockMvcBuilders.standaloneSetup(clientController)
                .setControllerAdvice(new CustomExceptionHandler())
                .build();
    }

    @Test
    public void testGetClientSuccess() throws Exception {
        String uri = "/v1/client";
        ClientResponse dummyClientResponse = new ClientResponse();
        dummyClientResponse.setId(24L);
        dummyClientResponse.setName("Dummy Client");

        when(clientService.getClient(any())).thenReturn(dummyClientResponse);
        mockMvc.perform(MockMvcRequestBuilders.get(uri)
                    .header("X-IAM-Client-Id", "24")
                    .accept(MediaType.APPLICATION_JSON_VALUE))
                    .andExpect(status().isOk());
    }

}
