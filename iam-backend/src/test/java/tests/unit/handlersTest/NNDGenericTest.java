package tests.unit.handlersTest;

import com.everwell.iam.enums.AdherenceCodeEnum;
import com.everwell.iam.enums.CacheAdherenceStatusToday;
import com.everwell.iam.handlers.impl.NNDHandler;
import com.everwell.iam.models.db.AdherenceStringLog;
import com.everwell.iam.models.db.CallLogs;
import com.everwell.iam.models.db.PhoneMap;
import com.everwell.iam.models.db.Registration;
import com.everwell.iam.models.dto.AdhTechLogsData;
import com.everwell.iam.models.http.requests.EntityRequest;
import com.everwell.iam.models.http.requests.NNDEntityRequest;
import com.everwell.iam.repositories.CallLogRepository;
import com.everwell.iam.repositories.PhoneMapRepository;
import com.everwell.iam.utils.Utils;
import org.apache.commons.lang3.time.DateUtils;
import org.junit.Assert;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.when;

public class NNDGenericTest extends AdherenceTechHandlerTest {

  @InjectMocks
  NNDHandler nndHandler;

  @Mock
  CallLogRepository callLogRepository;

  @Mock
  PhoneMapRepository phoneMapRepository;

  private static final Long NND_ID = 1L;

  @Override
  public void getTechLogsTest() throws Exception {
    String dummyPhoneToCheck = "1";
    when(callLogRepository.getCallLogsByPhone(eq(dummyPhoneToCheck), any(), any(), any())).thenReturn(dummyCallLogs());
    when(phoneMapRepository.findAllByIamId(any())).thenReturn(getDummyPhoneMaps());
    Registration registration = new Registration(1L, 1L, Utils.convertStringToDate("2019-08-31 18:30:00"), Utils.convertStringToDate("2019-09-06 18:30:00"), "91929399495", "666666", Utils.convertStringToDate("2019-01-06 12:23:26"), Utils.convertStringToDate("2019-01-06 12:23:26"));
    when(registrationRepository.getOne(any())).thenReturn(registration);
    List<AdhTechLogsData> adhTechLogsData = nndHandler.getTechLogs(1L);
    assertEquals(6, adhTechLogsData.size());
  }

  private List<CallLogs> dummyCallLogs() throws ParseException {
    List<CallLogs> callLogs = new ArrayList<>();
    callLogs.add(new CallLogs("1", "?", Utils.convertStringToDate("2019-07-02 02:00:26"), Utils.convertStringToDate("2019-08-31 21:30:26"), "1"));
    callLogs.add(new CallLogs("2", "?", Utils.convertStringToDate("2019-07-03 21:30:26"), Utils.convertStringToDate("2019-09-02 16:00:26"), "1"));
    callLogs.add(new CallLogs("3", "?", Utils.convertStringToDate("2019-07-04 21:23:26"), Utils.convertStringToDate("2019-09-03 16:00:26"), "1"));
    callLogs.add(new CallLogs("4", "?", Utils.convertStringToDate("2019-07-05 03:00:26"), Utils.convertStringToDate("2019-09-03 22:30:26"), "1"));
    callLogs.add(new CallLogs("5", "?", Utils.convertStringToDate("2019-07-06 21:23:26"), Utils.convertStringToDate("2019-09-05 16:00:26"), "1"));
    callLogs.add(new CallLogs("6", "?", Utils.convertStringToDate("2019-07-07 17:23:26"), Utils.convertStringToDate("2019-09-06 12:00:26"), "1"));
    return callLogs;
  }

  private List<PhoneMap> getDummyPhoneMaps() {
    List<PhoneMap> phoneMapList = new ArrayList<>();
    phoneMapList.add(new PhoneMap("1", 1L));
    phoneMapList.add(new PhoneMap("2", 1L));
    phoneMapList.add(new PhoneMap("3", 1L));
    return phoneMapList;
  }

  @Test
  public void computeAdherenceStatusTodayTest_DAT() throws ParseException {
    Registration dummyRegistration = new Registration(1L, 5L, Utils.convertStringToDate("2019-09-09 18:30:00"), null, "1", "66", Utils.convertStringToDate("2019-01-01 12:23:26"), Utils.convertStringToDate("2019-01-10 12:23:26"));
    List<AdherenceStringLog> adherenceStringLogList = new ArrayList<>();
    adherenceStringLogList.add(new AdherenceStringLog(1L, AdherenceCodeEnum.TFN_REPEAT_RECEIVED.getCode(), Utils.getCurrentDate()));
    testHandler.computeAdherenceStatusToday(dummyRegistration, adherenceStringLogList);
    Assert.assertEquals(
        CacheAdherenceStatusToday.DIGITAL,
        testHandler.computeAdherenceStatusToday(dummyRegistration, adherenceStringLogList)
    );
  }

  @Test
  public void computeAdherenceStatusTodayTest_DOT() throws ParseException {
    Registration dummyRegistration = new Registration(1L, 5L, Utils.convertStringToDate("2019-09-09 18:30:00"), null, "1", "66", Utils.convertStringToDate("2019-01-01 12:23:26"), Utils.convertStringToDate("2019-01-10 12:23:26"));
    List<AdherenceStringLog> adherenceStringLogList = new ArrayList<>();
    adherenceStringLogList.add(new AdherenceStringLog(1L, AdherenceCodeEnum.MANUAL.getCode(), Utils.getCurrentDate()));
    testHandler.computeAdherenceStatusToday(dummyRegistration, adherenceStringLogList);
    Assert.assertEquals(
        CacheAdherenceStatusToday.MANUAL,
        testHandler.computeAdherenceStatusToday(dummyRegistration, adherenceStringLogList)
    );
  }

  @Test
  public void computeAdherenceStatusTodayTest_NOINFO() throws ParseException {
    Registration dummyRegistration = new Registration(1L, 5L, Utils.convertStringToDate("2019-09-09 18:30:00"), null, "1", "66", Utils.convertStringToDate("2019-01-01 12:23:26"), Utils.convertStringToDate("2019-01-10 12:23:26"));
    List<AdherenceStringLog> adherenceStringLogList = new ArrayList<>();
    adherenceStringLogList.add(new AdherenceStringLog(1L, AdherenceCodeEnum.NO_INFO.getCode(), Utils.getCurrentDate()));
    testHandler.computeAdherenceStatusToday(dummyRegistration, adherenceStringLogList);
    Assert.assertEquals(
            CacheAdherenceStatusToday.NO_INFO,
            testHandler.computeAdherenceStatusToday(dummyRegistration, adherenceStringLogList)
    );
  }

  @Test
  public void computeAdherenceStatusTodayTest_MISSED() throws ParseException {
    Registration dummyRegistration = new Registration(1L, 5L, Utils.convertStringToDate("2019-09-09 18:30:00"), null, "1", "66", Utils.convertStringToDate("2019-01-01 12:23:26"), Utils.convertStringToDate("2019-01-10 12:23:26"));
    List<AdherenceStringLog> adherenceStringLogList = new ArrayList<>();
    adherenceStringLogList.add(new AdherenceStringLog(1L, AdherenceCodeEnum.MISSED.getCode(), Utils.getCurrentDate()));
    testHandler.computeAdherenceStatusToday(dummyRegistration, adherenceStringLogList);
    Assert.assertEquals(
            CacheAdherenceStatusToday.MISSED,
            testHandler.computeAdherenceStatusToday(dummyRegistration, adherenceStringLogList)
    );
  }

  @Test
  public void computeAdherenceStatusTodayTest_NULL() throws ParseException {
    Registration dummyRegistration = new Registration(1L, 5L, Utils.convertStringToDate("2019-09-09 18:30:00"), null, "1", "66", Utils.convertStringToDate("2019-01-01 12:23:26"), Utils.convertStringToDate("2019-01-10 12:23:26"));
    List<AdherenceStringLog> adherenceStringLogList = new ArrayList<>();
    adherenceStringLogList.add(new AdherenceStringLog(1L, AdherenceCodeEnum.NO_INFO.getCode(), DateUtils.addDays(Utils.getCurrentDate(), -2)));
    testHandler.computeAdherenceStatusToday(dummyRegistration, adherenceStringLogList);
    Assert.assertNull(testHandler.computeAdherenceStatusToday(dummyRegistration, adherenceStringLogList));
  }

  @Override
  public void deleteRegistration() {
    nndHandler.deleteRegistration(anySet());
    Mockito.verify(registrationRepository, Mockito.times(1)).deleteAllByIdIn(anySet());
    Mockito.verify(adhStringLogRepository, Mockito.times(1)).deleteAllByIamIds(anySet());
    Mockito.verify(phoneMapRepository, Mockito.times(1)).deleteAllByIamIdIn(anySet());
  }

  @Test
  public void saveRegistration() {
    EntityRequest entityRequest = new NNDEntityRequest();
    entityRequest.setAdherenceType(1);
    entityRequest.setScheduleTypeId(1L);
    entityRequest.setUniqueIdentifier("1");
    entityRequest.setStartDate("20-12-2018 10:10:10");
    when(registrationRepository.save(any())).thenReturn(null);
    nndHandler.save(entityRequest);
    Mockito.verify(registrationRepository, Mockito.times(1)).save(any());
  }

  @Test
  public void lastMissedDosageTest() throws ParseException {
    Registration dummyRegistration = new Registration(1L, 1L, Utils.convertStringToDate("2019-09-01 13:00:00"), Utils.convertStringToDate("2019-09-05 13:00:00"), "1", "64662", Utils.convertStringToDate("2019-01-02 12:23:26"), Utils.convertStringToDate("2019-01-10 12:23:26"), null, 1L,Utils.convertStringToDate("2019-09-05 13:00:00"));
    Date lastMissedDosage = nndHandler.computeLastDosage(dummyRegistration.getAdherenceString(),dummyRegistration.getEndDate(),true);
    Assert.assertEquals(lastMissedDosage,Utils.convertStringToDate("2019-09-05 13:00:00"));
  }

}
