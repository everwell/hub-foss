package tests.unit.handlersTest;

import com.everwell.iam.enums.AdherenceCodeEnum;
import com.everwell.iam.enums.AdherenceTech;
import com.everwell.iam.exceptions.NotFoundException;
import com.everwell.iam.exceptions.ValidationException;
import com.everwell.iam.handlers.ScheduleHandler;
import com.everwell.iam.handlers.ScheduleTypeHandlerMap;
import com.everwell.iam.handlers.impl.DailyScheduleHandler;
import com.everwell.iam.handlers.impl.VOTHandler;
import com.everwell.iam.handlers.impl.WeeklyScheduleHandler;
import com.everwell.iam.models.db.*;
import com.everwell.iam.models.dto.AdhTechLogsData;
import com.everwell.iam.models.dto.AdherenceData;
import com.everwell.iam.models.dto.ScheduleSensitivity;
import com.everwell.iam.models.http.requests.RecordAdherenceRequest;
import com.everwell.iam.repositories.AdhStringLogRepository;
import com.everwell.iam.repositories.ScheduleMapRepository;
import com.everwell.iam.repositories.ScheduleRepository;
import com.everwell.iam.utils.Utils;
import com.everwell.iam.vendors.enums.VideoReviewStatus;
import com.everwell.iam.vendors.models.db.VideoEntityMapping;
import com.everwell.iam.vendors.repositories.VideoEntityMappingRepository;
import org.apache.commons.lang3.NotImplementedException;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.Spy;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;

import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.*;
import java.util.concurrent.CompletableFuture;

import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.*;

public class VotHandlerTest extends AdherenceTechHandlerTest {

  @Spy
  @InjectMocks
  VOTHandler votHandler;

  @Spy
  @InjectMocks
  WeeklyScheduleHandler weeklyHandler;

  @Spy
  DailyScheduleHandler dailyScheduleHandler;

  @Mock
  VideoEntityMappingRepository mappingRepository;

  @Mock
  ScheduleMapRepository scheduleMapRepository;

  @Mock
  ScheduleTypeHandlerMap scheduleTypeHandlerMap;

  @Mock
  ScheduleRepository scheduleRepository;

  @Mock
  ScheduleHandler scheduleHandler;


  private static final Long VOT_ID = 2L;
  private static Long iamId = 1L;

  @Before
  public void init() {
    weeklyHandler.setScheduleTypeId(2L);
    dailyScheduleHandler.setScheduleTypeId(1L);
    when(scheduleTypeHandlerMap.getHandler(1L)).thenReturn(dailyScheduleHandler);
    when(scheduleTypeHandlerMap.getHandler(2L)).thenReturn(weeklyHandler);
  }

  @Override
  public void getTechLogsTest() throws Exception {
    when(mappingRepository.findByIamIdIn(any())).thenReturn(dummyVideoMappings());
    List<AdhTechLogsData> adhTechLogsData = votHandler.getTechLogs(1L);
    assertEquals(5, adhTechLogsData.size());
  }

  @Test
  public void getMissedReviewNoInfoAdherenceCode(){
    VideoReviewStatus videoReviewStatus = VideoReviewStatus.MISSED;
    Assert.assertEquals(votHandler.getAdherenceValue(videoReviewStatus).charValue(), AdherenceCodeEnum.NO_INFO.getCode());
  }

  @Test
  public void getTakenReviewReceivedAdherenceCode(){
    VideoReviewStatus videoReviewStatus = VideoReviewStatus.TAKEN;
    Assert.assertEquals(votHandler.getAdherenceValue(videoReviewStatus).charValue(), AdherenceCodeEnum.RECEIVED.getCode());
  }

  @Test
  public void getUnreviewedUnvalidatedAdherenceCode(){
    VideoReviewStatus videoReviewStatus = VideoReviewStatus.UNREVIEWED;
    Assert.assertEquals(votHandler.getAdherenceValue(videoReviewStatus).charValue(), AdherenceCodeEnum.UNVALIDATED.getCode());
  }

  @Test(expected = ValidationException.class)
  public void testValidateVideoIdThrowsExceptionOnNull() {
    VideoEntityMapping videoEntityMapping = new VideoEntityMapping();
    when(mappingRepository.findByVideoId(any())).thenReturn(videoEntityMapping);
    votHandler.validateVideoId("5");
  }

  @Test
  public void testCorrectRecordVideoMappingWithoutPassingDate() {
    String videoId = "5";
    RecordAdherenceRequest recordAdherenceRequest = new RecordAdherenceRequest();
    Long iamId = 1L;

    VideoEntityMapping mapping = votHandler.recordVideoMapping(videoId, recordAdherenceRequest, iamId);
    Assert.assertEquals(mapping.getIamId(), iamId);
    Assert.assertEquals(mapping.getVideoId(), videoId);
    Assert.assertNotNull(mapping.getUploadDate());
  }

  @Test
  public void testCorrectRecordVideoMappingWithPassingDate() throws Exception{
    String videoId = "5";
    RecordAdherenceRequest recordAdherenceRequest = new RecordAdherenceRequest();
    recordAdherenceRequest.setDate("2019-03-22 00:00:00");
    Long iamId = 1L;

    VideoEntityMapping mapping = votHandler.recordVideoMapping(videoId, recordAdherenceRequest, iamId);
    Assert.assertEquals(mapping.getIamId(), iamId);
    Assert.assertEquals(mapping.getVideoId(), videoId);
    Assert.assertEquals(mapping.getUploadDate(), Utils.convertStringToDate(recordAdherenceRequest.getDate()));
  }

  @Test(expected = NotFoundException.class)
  public void testUpdateVideoMappingNoVideoFound() {
    when(mappingRepository.findByVideoIdAndIamId(any(), any())).thenReturn(null);
    votHandler.updateVideoMapping("5",1L,1);
  }

  @Test
  public void testUpdateVideoMapping() {
    String videoId = "5";
    int value= 5;
    Long iamId = 1L;
    VideoEntityMapping dummyMapping = new VideoEntityMapping();
    when(mappingRepository.findByVideoIdAndIamId(any(), any())).thenReturn(dummyMapping);
    VideoEntityMapping testMapping = votHandler.updateVideoMapping(videoId, iamId, value);
    Assert.assertEquals(value, testMapping.getStatus().intValue());
  }

  public List<VideoEntityMapping> dummyVideoMappings () {
    List<VideoEntityMapping> mappings = new ArrayList<>();
    mappings.add(new VideoEntityMapping("1", 1L, 0));
    mappings.add(new VideoEntityMapping("2", 2L, 2));
    mappings.add(new VideoEntityMapping("3", 3L, 1));
    mappings.add(new VideoEntityMapping("4", 4L, 0));
    mappings.add(new VideoEntityMapping("5", 5L, 1));
    return mappings;
  }

  @Test
  public void getConsecutiveMissedDosesDaily() {
    LocalDate todayLocal = LocalDate.now();
    LocalDate startLocal = todayLocal.minusDays(10);
    LocalDate lastLocal = todayLocal.minusDays(4);
    Date start = Date.from(startLocal.atStartOfDay(ZoneId.of("Asia/Kolkata")).toInstant());
    Date lastDose = Date.from(lastLocal.atStartOfDay(ZoneId.of("Asia/Kolkata")).toInstant());

    AdherenceData dummyRegistration1 = new AdherenceData("9966696666", AdherenceTech.VOT.getName());
    dummyRegistration1.setScheduleType(1L);
    dummyRegistration1.setStartDate(start);

    Long missedDoses = votHandler.getEntityConsecutiveMissedDoses("9966696666", start, lastDose, dummyRegistration1,true);
    Assert.assertTrue(3L == missedDoses);
  }
  @Test
  public void getConsecutiveMissedDosesFromYesterday() {
    LocalDate todayLocal = LocalDate.now();
    LocalDate startLocal = todayLocal.minusDays(10);
    LocalDate lastLocal = todayLocal.minusDays(0);
    Date start = Date.from(startLocal.atStartOfDay(ZoneId.of("Asia/Kolkata")).toInstant());
    Date lastDose = Date.from(lastLocal.atStartOfDay(ZoneId.of("Asia/Kolkata")).toInstant());

    AdherenceData dummyRegistration1 = new AdherenceData("9966696669", AdherenceTech.VOT.getName());
    dummyRegistration1.setScheduleType(1L);
    dummyRegistration1.setStartDate(start);

    Long missedDoses = votHandler.getEntityConsecutiveMissedDoses("9966696669", start, lastDose, dummyRegistration1,false);
    Assert.assertTrue(3L == missedDoses);
  }

  @Test
  public void getConsecutiveMissedDosesWeeklyNotInBuffer() {
    LocalDate todayLocal = LocalDate.now();
    LocalDate startLocal = todayLocal.minusDays(10);
    LocalDate lastTaken = todayLocal.minusDays(10);
    LocalDate lastLocal = todayLocal.minusDays(3);
    int doseInd = DayOfWeek.from(lastLocal).getValue();
    Date start = Date.from(startLocal.atStartOfDay(ZoneId.of("Asia/Kolkata")).toInstant());
    Date lastDose = Date.from(lastTaken.atStartOfDay(ZoneId.of("Asia/Kolkata")).toInstant());

    AdherenceData dummyRegistration1 = new AdherenceData("9777776777", AdherenceTech.VOT.getName());
    dummyRegistration1.setScheduleType(2L);
    dummyRegistration1.setStartDate(start);

    ScheduleMap schedMap = new ScheduleMap(1L, Long.valueOf(doseInd), start, 0L, new ScheduleSensitivity(72L, 72L));
    when(scheduleMapRepository.findByActiveTrueAndIamId(any())).thenReturn(schedMap);

    Long missedDoses = votHandler.getEntityConsecutiveMissedDoses("9777776777", start, lastDose, dummyRegistration1, true);
    Assert.assertTrue(1L == missedDoses);
  }

  @Test
  public void testRegenerateAdherence() throws Exception
  {
    List<VideoEntityMapping> videoLogs = new ArrayList<>();
    VideoEntityMapping videoLog = new VideoEntityMapping("1",1L,0,Utils.convertStringToDate("2019-09-01 12:00:00"));
    videoLogs.add(videoLog);

    doNothing().when(votHandler).deleteAdherenceLogByIamIdAndAdherenceCodes(anyList(), anyLong());
    when(mappingRepository.findByIamIdForStatus(anyLong(),anyList())).thenReturn(videoLogs);

    List<AdherenceStringLog> logList = new ArrayList<AdherenceStringLog>();
    logList.add(new AdherenceStringLog(1L,'4'));
    doReturn(logList).when(votHandler).computeCompleteAdherenceString(anyList(), anyLong());
    doReturn(Collections.singletonList(1L)).when(votHandler).updateAdherenceLogBulk(anyList());

    List<Long> logs = votHandler.regenerateAdherence(Collections.singleton(iamId)).get();
    Assert.assertEquals(logs.size(),1);
    Assert.assertEquals(logs.get(0).longValue(), 1L);
  }

  @Test
  public void testComputeCompleteAdherenceString() throws Exception
  {
    Registration registration = new Registration(1L, 1L, Utils.convertStringToDate("2019-08-31 18:30:00"), Utils.convertStringToDate("2019-09-06 12:00:00"), "1", "666666", Utils.convertStringToDate("2019-01-06 12:23:26"), Utils.convertStringToDate("2019-01-06 12:23:26"));
    when(registrationRepository.findById(any())).thenReturn(Optional.of(registration));

    List<VideoEntityMapping> videoLogs = new ArrayList<>();
    VideoEntityMapping videoLog = new VideoEntityMapping("1",1L,0,Utils.convertStringToDate("2019-09-01 12:00:00"));
    videoLogs.add(videoLog);
    VideoEntityMapping videoLog2 = new VideoEntityMapping("2",1L,1,Utils.convertStringToDate("2019-09-02 12:00:00"));
    videoLogs.add(videoLog2);

    when(scheduleTypeHandlerMap.getHandler(anyLong())).thenReturn(scheduleHandler);
    when(scheduleHandler.shouldTakeDose(anyLong(), any())).thenReturn(true);

    doNothing().when(votHandler).updateRegistrationAfterRegen(anyLong(), anyList());

    List<AdherenceStringLog> adherenceStringIds1 = votHandler.computeCompleteAdherenceString(Arrays.asList(videoLog), iamId);
    Assert.assertEquals(adherenceStringIds1.get(0).getValue(), new Character('3'));

    List<AdherenceStringLog> adherenceStringIds2 = votHandler.computeCompleteAdherenceString(Arrays.asList(videoLog2), iamId);
    Assert.assertEquals(adherenceStringIds2.get(0).getValue(), new Character('4'));

    List<AdherenceStringLog> adherenceStringIdsMultiple = votHandler.computeCompleteAdherenceString(videoLogs, iamId);
    Assert.assertEquals(adherenceStringIdsMultiple.size(), 2);
  }

  @Override
  public void deleteRegistration() {
    votHandler.deleteRegistration(anySet());
    Mockito.verify(registrationRepository, Mockito.times(1)).deleteAllByIdIn(anySet());
    Mockito.verify(adhStringLogRepository, Mockito.times(1)).deleteAllByIamIds(anySet());
    Mockito.verify(mappingRepository, Mockito.times(1)).deleteAllByIamIdIn(anySet());
  }

}
