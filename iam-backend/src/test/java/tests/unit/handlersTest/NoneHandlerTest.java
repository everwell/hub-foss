package tests.unit.handlersTest;

import com.everwell.iam.handlers.impl.NoneHandler;
import com.everwell.iam.models.db.AdherenceStringLog;
import com.everwell.iam.models.db.Registration;
import com.everwell.iam.models.dto.AdhTechLogsData;
import com.everwell.iam.utils.Utils;
import org.mockito.InjectMocks;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;

import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.when;

public class NoneHandlerTest extends AdherenceTechHandlerTest {

  @InjectMocks
  NoneHandler noneHandler;

  private static final Long NONE_ID = 5L;

  @Override
  public void getTechLogsTest() throws Exception {
    Registration registration = new Registration();
    registration.setStartDate(Utils.getCurrentDate());
    AdherenceStringLog adherenceStringLog = new AdherenceStringLog(1L, 'F', Utils.getCurrentDate());
    when(registrationRepository.getOne(eq(1L))).thenReturn(registration);
    when(adhStringLogRepository.findAllByIamAndStartDate(eq(registration.getStartDate()), eq(1L))).thenReturn(Collections.singletonList(adherenceStringLog));
    List<AdhTechLogsData> adhTechLogsData = noneHandler.getTechLogs(1L);
    assertEquals(1, adhTechLogsData.size());
  }

  @Override
  public void deleteRegistration() {
    noneHandler.deleteRegistration(anySet());
    Mockito.verify(registrationRepository, Mockito.times(1)).deleteAllByIdIn(anySet());
    Mockito.verify(adhStringLogRepository, Mockito.times(1)).deleteAllByIamIds(anySet());
  }

}
