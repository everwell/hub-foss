package tests.unit.handlersTest;

import com.everwell.iam.enums.AdherenceCodeEnum;
import com.everwell.iam.exceptions.ValidationException;
import com.everwell.iam.handlers.impl.DailyScheduleHandler;
import com.everwell.iam.handlers.impl.MultiFreqWeeklyScheduleHandler;
import com.everwell.iam.handlers.impl.WeeklyScheduleHandler;
import com.everwell.iam.models.db.*;
import com.everwell.iam.models.dto.DoseTimeData;
import com.everwell.iam.models.dto.ScheduleSensitivity;
import com.everwell.iam.models.dto.StopScheduleExtraInfoDto;
import com.everwell.iam.models.dto.ValidateScheduleDto;
import com.everwell.iam.models.http.requests.EntityRequest;
import com.everwell.iam.repositories.AdhStringLogRepository;
import com.everwell.iam.repositories.ScheduleMapRepository;
import com.everwell.iam.repositories.ScheduleRepository;
import com.everwell.iam.repositories.ScheduleTimeMapRepository;
import com.everwell.iam.utils.Utils;
import org.apache.commons.lang3.time.DateUtils;
import org.apache.commons.lang3.tuple.Pair;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.*;
import tests.BaseTestNoSpring;

import java.security.InvalidParameterException;
import java.sql.Time;
import java.text.ParseException;
import java.util.*;

import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.*;

public class MultiFrequencyWeeklyScheduleHandlerTest extends BaseTestNoSpring {

    @Spy
    @InjectMocks
    MultiFreqWeeklyScheduleHandler multiFreqWeeklyScheduleHandler;

    @Mock
    ScheduleMapRepository scheduleMapRepository;

    @Mock
    ScheduleRepository scheduleRepository;

    @Mock
    private ScheduleTimeMapRepository scheduleTimeMapRepository;

    @Mock
    private AdhStringLogRepository adhStringLogRepository;

    //to test the hasScheduleMapping() checks
    @Spy
    DailyScheduleHandler dailyScheduleHandler;

    @Spy
    WeeklyScheduleHandler weeklyScheduleHandler;

    @Before
    public void init() {
        weeklyScheduleHandler.setScheduleTypeId(2L);
        dailyScheduleHandler.setScheduleTypeId(1L);
        multiFreqWeeklyScheduleHandler.setScheduleTypeId(3L);
    }

    @Test
    public void validate_schedule_sensitivity_no_overlap() {
        ScheduleSensitivity scheduleSensitivity = new ScheduleSensitivity(0L, 0L);
        Schedule schedule = new Schedule(2L, "1111111", 3L);
        doReturn(schedule).when(multiFreqWeeklyScheduleHandler).findByScheduleValue(anyString());
        //no exceptions, successfully validated
        multiFreqWeeklyScheduleHandler.validateScheduleAndSensitivityOverLap(new ValidateScheduleDto("MTWTFSU", scheduleSensitivity, Arrays.asList(new EntityRequest.DoseTimeInfo("03:00:00", "something", 2))));
    }

    @Test(expected = ValidationException.class)
    public void validate_schedule_sensitivity_unsupported_schedule() {
        ScheduleSensitivity scheduleSensitivity = new ScheduleSensitivity(0L, 0L);
        Schedule schedule = new Schedule(2L, "0100000", 3L);
        doReturn(schedule).when(multiFreqWeeklyScheduleHandler).findByScheduleValue(anyString());
        //no exceptions, successfully validated
        multiFreqWeeklyScheduleHandler.validateScheduleAndSensitivityOverLap(new ValidateScheduleDto("MTWTFSU", scheduleSensitivity, Arrays.asList(new EntityRequest.DoseTimeInfo("03:00:00", "something", 2))));
    }

    @Test (expected = InvalidParameterException.class)
    public void validate_schedule_sensitivity_incorrect_schedule() {
        ScheduleSensitivity scheduleSensitivity = new ScheduleSensitivity(0L, 0L);
        //no exceptions, successfully validated
        multiFreqWeeklyScheduleHandler.validateScheduleAndSensitivityOverLap(new ValidateScheduleDto("M", scheduleSensitivity, Arrays.asList(new EntityRequest.DoseTimeInfo("03:00:00", "something", 2))));
    }

    @Test(expected = InvalidParameterException.class)
    public void validate_schedule_sensitivity_overlap() {
        ScheduleSensitivity scheduleSensitivity = new ScheduleSensitivity(0L, 0L);
        multiFreqWeeklyScheduleHandler.validateScheduleAndSensitivityOverLap(new ValidateScheduleDto("MTWHFSU", scheduleSensitivity, Arrays.asList(
                new EntityRequest.DoseTimeInfo("03:00:00", "something", 2),
                new EntityRequest.DoseTimeInfo("04:00:00", "something", 2),
                new EntityRequest.DoseTimeInfo("03:00:00", "something", 2)
        )));
    }

    @Test
    public void saveScheduleAndCreateTimeMap() {
        ScheduleMap scheduleMap = new ScheduleMap();
        scheduleMap.setScheduleId(1L);
        List<EntityRequest.DoseTimeInfo> doseTimeInfo = Arrays.asList(new EntityRequest.DoseTimeInfo("03:30:00", "exc", 4), new EntityRequest.DoseTimeInfo("04:00:00", "exc", 4));

        when(scheduleMapRepository.save(any())).thenReturn(scheduleMap);

        multiFreqWeeklyScheduleHandler.save(1L, Utils.getCurrentDate(), null, null, new ScheduleSensitivity(0L, 0L), doseTimeInfo);

        Mockito.verify(scheduleTimeMapRepository, Mockito.times(1)).saveAll(any());
    }

    @Test(expected = ValidationException.class)
    public void shouldTakeDoseForDatesDoseExists() throws ParseException {
        AdherenceStringLog log = new AdherenceStringLog(1L, '4', Utils.convertStringToDate("2022-11-13 03:30:00"));
        Set<Date> dates = new HashSet<>(Collections.singletonList(Utils.convertStringToDate("2022-11-13 03:30:00")));

        multiFreqWeeklyScheduleHandler.shouldTakeDoseForDates(dates, 1L, Collections.singletonList(log));
    }

    @Test(expected = ValidationException.class)
    public void shouldTakeDoseForEmptySchedule() throws ParseException {
        AdherenceStringLog log = new AdherenceStringLog(1L, '4', Utils.convertStringToDate("2022-11-13 03:30:00"));
        Set<Date> dates = new HashSet<>(Collections.singletonList(Utils.convertStringToDate("2022-11-13 11:30:00")));

        List<ScheduleMap> scheduleMapList = new ArrayList<>();
        scheduleMapList.add(new ScheduleMap(1L,true, 1L, Utils.convertStringToDate("2020-01-01 18:30:00"), null, Utils.convertStringToDate("2020-01-01 18:30:00"), Utils.convertStringToDate("2023-01-25 18:29:00"), 0L, new ScheduleSensitivity(0L, 0L)));
        when(scheduleMapRepository.findAllByIamId(eq(1L))).thenReturn(scheduleMapList);
        when(scheduleTimeMapRepository.findAllByScheduleMapIdIn(eq(Collections.singletonList(1L)))).thenReturn(Collections.emptyList());

        multiFreqWeeklyScheduleHandler.shouldTakeDoseForDates(dates, 1L, Collections.singletonList(log));
    }

    @Test(expected = ValidationException.class)
    public void shouldTakeDoseWithNoSchedules() throws ParseException {
        AdherenceStringLog log = new AdherenceStringLog(1L, '4', Utils.convertStringToDate("2022-11-13 03:30:00"));
        Set<Date> dates = new HashSet<>(Collections.singletonList(Utils.convertStringToDate("2022-11-13 11:30:00")));

        when(scheduleMapRepository.findAllByIamId(eq(1L))).thenReturn(new ArrayList<>());
        when(scheduleTimeMapRepository.findAllByScheduleMapIdIn(any())).thenReturn(Collections.emptyList());

        multiFreqWeeklyScheduleHandler.shouldTakeDoseForDates(dates, 1L, Collections.singletonList(log));
    }

    @Test(expected = ValidationException.class)
    public void shouldTakeDoseForDatesTimeDoesNotExist() throws ParseException {
        Set<Date> dates = new HashSet<>(Collections.singletonList(Utils.convertStringToDate("2022-11-13 11:30:00")));

        List<ScheduleMap> scheduleMapList = new ArrayList<>();
        scheduleMapList.add(new ScheduleMap(1L,true, 1L, Utils.convertStringToDate("2020-01-01 18:30:00"), null, Utils.convertStringToDate("2020-01-01 18:30:00"), Utils.convertStringToDate("2023-01-25 18:29:00"), 0L, new ScheduleSensitivity(0L, 0L)));
        when(scheduleMapRepository.findAllByIamId(eq(1L))).thenReturn(scheduleMapList);
        when(scheduleTimeMapRepository.findAllByScheduleMapIdIn(any())).thenReturn(Collections.singletonList(new ScheduleTimeMap(1L, true, Utils.getCurrentDate(), 1L, Time.valueOf("03:30:00"))));

        multiFreqWeeklyScheduleHandler.shouldTakeDoseForDates(dates, 1L, Collections.emptyList());
    }

    @Test
    public void shouldTakeDoseForDatesNoException() throws ParseException {
        Set<Date> dates = new HashSet<>(Collections.singletonList(Utils.convertStringToDate("2022-11-13 03:30:00")));

        List<ScheduleMap> scheduleMapList = new ArrayList<>();
        scheduleMapList.add(new ScheduleMap(1L,true, 1L, Utils.convertStringToDate("2020-01-01 18:30:00"), null, Utils.convertStringToDate("2020-01-01 18:30:00"), Utils.convertStringToDate("2023-01-25 18:29:00"), 0L, new ScheduleSensitivity(0L, 0L)));
        List<ScheduleTimeMap> scheduleTimeMapList = new ArrayList<>();
        scheduleTimeMapList.add(new ScheduleTimeMap(1L, true, null, 1L, Time.valueOf("03:30:00")));
        when(scheduleMapRepository.findAllByIamId(eq(1L))).thenReturn(scheduleMapList);
        when(scheduleTimeMapRepository.findAllByScheduleMapIdIn(eq(Collections.singletonList(1L)))).thenReturn(scheduleTimeMapList);

        //no exceptions, successfully validated
        multiFreqWeeklyScheduleHandler.shouldTakeDoseForDates(dates, 1L, Collections.emptyList());

        Mockito.verify(scheduleMapRepository, Mockito.times(1)).findAllByIamId(any());
        Mockito.verify(scheduleTimeMapRepository, Mockito.times(1)).findAllByScheduleMapIdIn(any());
    }

    @Test
    public void getTransformedValueReceived() throws ParseException {
        AdherenceStringLog log = new AdherenceStringLog(1L, '4', Utils.convertStringToDate("2022-11-13 03:30:00"));
        List<ScheduleMap> scheduleMapList = new ArrayList<>();
        scheduleMapList.add(new ScheduleMap(1L,true, 1L, Utils.convertStringToDate("2020-01-01 18:30:00"), null, Utils.convertStringToDate("2020-01-01 18:30:00"), Utils.convertStringToDate("2023-01-01 18:30:00"), 0L, new ScheduleSensitivity(0L, 0L)));
        List<ScheduleTimeMap> scheduleTimeMapList = new ArrayList<>();
        scheduleTimeMapList.add(new ScheduleTimeMap(1L, true, null, 1L, Time.valueOf("03:30:00")));
        when(scheduleMapRepository.findAllByIamId(eq(1L))).thenReturn(scheduleMapList);

        Character value = multiFreqWeeklyScheduleHandler.getTransformedValue('9', 0L, 1L, Collections.singletonList(log), Utils.convertStringToDate("2022-11-12 18:30:00"), Utils.convertStringToDate("2022-11-13 18:30:00"));

        Assert.assertEquals('D', value.charValue());
        Mockito.verify(scheduleMapRepository, Mockito.times(1)).findAllByIamId(any());
        Mockito.verify(scheduleTimeMapRepository, Mockito.times(1)).findAllByScheduleMapIdIn(any());
    }

    @Test
    public void getTransformedValuePartial() throws ParseException {
        AdherenceStringLog log = new AdherenceStringLog(1L, '9', Utils.convertStringToDate("2022-11-12 03:30:00"));
        List<ScheduleMap> scheduleMapList = new ArrayList<>();
        scheduleMapList.add(new ScheduleMap(1L,true, 1L, Utils.convertStringToDate("2020-01-01 18:30:00"), null, Utils.convertStringToDate("2020-01-01 18:30:00"), Utils.convertStringToDate("2023-01-01 18:30:00"), 0L, new ScheduleSensitivity(0L, 0L)));
        List<ScheduleTimeMap> scheduleTimeMapList = new ArrayList<>();
        scheduleTimeMapList.add(new ScheduleTimeMap(1L, true, null, 1L, Time.valueOf("03:30:00"), Utils.convertStringToDate("2022-11-10 18:30:00"), Utils.convertStringToDate("2020-01-01 18:30:00"), "666"));
        scheduleTimeMapList.add(new ScheduleTimeMap(2L, true, null, 1L, Time.valueOf("07:30:00"), Utils.convertStringToDate("2022-11-10 18:30:00"), Utils.convertStringToDate("2020-01-01 18:30:00"), "666"));
        when(scheduleMapRepository.findAllByIamId(eq(1L))).thenReturn(scheduleMapList);
        when(scheduleTimeMapRepository.findAllByScheduleMapIdIn(anyList())).thenReturn(scheduleTimeMapList);

        Character value = multiFreqWeeklyScheduleHandler.getTransformedValue('9', 0L, 1L, Collections.singletonList(log), Utils.convertStringToDate("2022-11-12 03:30:00"), Utils.convertStringToDate("2022-11-13 03:30:00"));

        Assert.assertEquals('F', value.charValue());
        Mockito.verify(scheduleMapRepository, Mockito.times(1)).findAllByIamId(any());
        Mockito.verify(scheduleTimeMapRepository, Mockito.times(1)).findAllByScheduleMapIdIn(any());
    }

    @Test
    public void getTransformedValueNoInfo() throws ParseException {
        AdherenceStringLog log = new AdherenceStringLog(1L, '4', Utils.convertStringToDate("2022-11-13 03:30:00"));
        List<ScheduleMap> scheduleMapList = new ArrayList<>();
        scheduleMapList.add(new ScheduleMap(1L,true, 1L, Utils.convertStringToDate("2020-01-01 18:30:00"), null, Utils.convertStringToDate("2020-01-01 18:30:00"), Utils.convertStringToDate("2023-01-01 18:30:00"), 0L, new ScheduleSensitivity(0L, 0L)));
        List<ScheduleTimeMap> scheduleTimeMapList = new ArrayList<>();
        scheduleTimeMapList.add(new ScheduleTimeMap(1L, true, null, 1L, Time.valueOf("03:30:00")));
        when(scheduleMapRepository.findAllByIamId(eq(1L))).thenReturn(scheduleMapList);

        Character value = multiFreqWeeklyScheduleHandler.getTransformedValue('9', 0L, 1L, Collections.emptyList(), Utils.convertStringToDate("2022-11-12 18:30:00"), Utils.convertStringToDate("2022-11-13 18:30:00"));

        Assert.assertEquals('6', value.charValue());
        Mockito.verify(scheduleMapRepository, Mockito.times(1)).findAllByIamId(any());
        Mockito.verify(scheduleTimeMapRepository, Mockito.times(1)).findAllByScheduleMapIdIn(any());
    }

    @Test
    public void testGenerateAdherenceString() throws ParseException {
        ScheduleMap scheduleMap = new ScheduleMap(1L, null, Utils.convertStringToDate("2022-01-01 18:30:00"), Utils.convertStringToDate("2022-01-01 18:30:00"), 0L, new ScheduleSensitivity(0L, 0L));
        scheduleMap.setId(1L);
        HashMap<Integer, List<Pair<String, Character>>> dayToAllDoses = new HashMap<>();
        dayToAllDoses.put(0, Arrays.asList(Pair.of("03:00:00", '9')));
        dayToAllDoses.put(1, Arrays.asList(Pair.of("14:00:00", '9'), Pair.of("9", '9')));
        dayToAllDoses.put(2, new ArrayList<>());

        when(scheduleMapRepository.findAllByIamId(eq(1L))).thenReturn(Collections.singletonList(scheduleMap));
        when(scheduleTimeMapRepository.findAllByScheduleMapIdIn(any())).thenReturn(Arrays.asList(new ScheduleTimeMap(1L, true, Utils.convertStringToDate("2022-11-14 18:30:00"), 1L, Time.valueOf("03:30:00"), Utils.convertStringToDate("2022-01-01 18:30:00"), Utils.convertStringToDate("2022-01-05 18:30:00"), "6666")));

        String adherenceString = multiFreqWeeklyScheduleHandler.generateAdherenceString(new HashMap<>(), Utils.convertStringToDate("2022-01-02 18:30:00"), Utils.convertStringToDate("2022-01-05 18:29:59"), 1L, dayToAllDoses);
        Assert.assertEquals("DD6", adherenceString);
    }

    @Test
    public void testGenerateAdherenceStringWithEmptySchedule() throws ParseException {
        List<ScheduleMap> scheduleMapList = new ArrayList<>();
        ScheduleMap scheduleMapOld = new ScheduleMap(1L, null, Utils.convertStringToDate("2022-11-13 22:30:00"), Utils.convertStringToDate("2022-11-13 18:30:00"), 0L, new ScheduleSensitivity(0L, 0L));
        scheduleMapOld.setId(1L);
        scheduleMapOld.setEndDate(Utils.convertStringToDate("2022-11-16 00:00:00"));
        scheduleMapList.add(scheduleMapOld);
        ScheduleMap scheduleMapNew = new ScheduleMap(1L, null, Utils.convertStringToDate("2022-11-16 00:00:00"), Utils.convertStringToDate("2022-11-16 00:00:00"), 0L, new ScheduleSensitivity(0L, 0L));
        scheduleMapNew.setId(2L);
        scheduleMapList.add(scheduleMapNew);
        List<ScheduleTimeMap> scheduleTimeMapList = Arrays.asList(new ScheduleTimeMap(1L, true, Utils.convertStringToDate("2022-11-13 22:30:00"), 1L, Time.valueOf("03:30:00"), Utils.convertStringToDate("2022-11-13 22:30:00"), Utils.convertStringToDate("2022-11-16 22:30:00"), "6666"), new ScheduleTimeMap(1L, true, Utils.convertStringToDate("2022-11-13 22:30:00"), 1L, Time.valueOf("11:30:00"), Utils.convertStringToDate("2022-11-13 22:30:00"), Utils.convertStringToDate("2022-11-16 22:30:00"), "6666"));
        HashMap<Integer, List<Pair<String, Character>>> dayToAllDoses = new HashMap<>();
        dayToAllDoses.put(0, Arrays.asList(Pair.of("03:00:00", '9')));
        dayToAllDoses.put(1, Arrays.asList(Pair.of("14:00:00", '9'), Pair.of("9", '9')));
        dayToAllDoses.put(2, new ArrayList<>());
        dayToAllDoses.put(3, new ArrayList<>());
        when(scheduleMapRepository.findAllByIamId(eq(1L))).thenReturn(scheduleMapList);
        when(scheduleTimeMapRepository.findAllByScheduleMapIdIn(any())).thenReturn(scheduleTimeMapList);
        String adherenceString = multiFreqWeeklyScheduleHandler.generateAdherenceString(new HashMap<>(), Utils.convertStringToDate("2022-11-13 18:30:00"), Utils.convertStringToDate("2022-11-17 18:29:59"), 1L, dayToAllDoses);
        Assert.assertEquals("GD6G", adherenceString);
    }

    @Test
    public void testGetDoseTime() throws ParseException {
        List<ScheduleMap> scheduleMapList = new ArrayList<>();
        scheduleMapList.add(new ScheduleMap(1L,true, 1L, Utils.convertStringToDate("2020-01-01 18:30:00"), null, Utils.convertStringToDate("2020-01-01 18:30:00"), Utils.convertStringToDate("2020-01-25 18:29:00"), 0L, new ScheduleSensitivity(0L, 0L)));
        List<ScheduleTimeMap> scheduleTimeMapList = new ArrayList<>();
        scheduleTimeMapList.add(new ScheduleTimeMap(1L, true, null, 1L, Time.valueOf("03:30:00")));
        when(scheduleMapRepository.findAllByIamId(eq(1L))).thenReturn(scheduleMapList);
        when(scheduleTimeMapRepository.findAllByScheduleMapIdIn(eq(Collections.singletonList(1L)))).thenReturn(scheduleTimeMapList);

        List<DoseTimeData> doseTimeList = multiFreqWeeklyScheduleHandler.getDoseTimes(1L);

        Assert.assertEquals(scheduleMapList.size(), doseTimeList.size());
        Assert.assertEquals("03:30:00", doseTimeList.get(0).getDoseTime().toString());
        Mockito.verify(scheduleMapRepository, Mockito.times(1)).findAllByIamId(any());
        Mockito.verify(scheduleTimeMapRepository, Mockito.times(1)).findAllByScheduleMapIdIn(any());
    }

    @Test
    public void saveScheduleForFirstTime() {
        Long iamId = 1L;

        List<EntityRequest.DoseTimeInfo> doseTimeInfodoseTimeInfo = Arrays.asList(new EntityRequest.DoseTimeInfo("03:30:00", "exc", 4), new EntityRequest.DoseTimeInfo("04:00:00", "exc", 4));

        ScheduleMap scheduleMap = new ScheduleMap();
        scheduleMap.setScheduleId(1L);
        when(scheduleMapRepository.save(any())).thenReturn(scheduleMap);

        multiFreqWeeklyScheduleHandler.save(iamId, Utils.getCurrentDate(), null, null, new ScheduleSensitivity(0L, 0L), doseTimeInfodoseTimeInfo);

        Mockito.verify(scheduleMapRepository, Mockito.times(1)).save(any());
        Mockito.verify(scheduleTimeMapRepository, Mockito.times(1)).saveAll(any());
    }

    @Test
    public void saveScheduleForFirstTimeForEmptyDoseTimeList() {
        Long iamId = 1L;
        List<EntityRequest.DoseTimeInfo> doseTimeInfodoseTimeInfo = Arrays.asList(new EntityRequest.DoseTimeInfo("03:30:00", "exc", 4), new EntityRequest.DoseTimeInfo("04:00:00", "exc", 4));
        ScheduleMap scheduleMap = new ScheduleMap();
        scheduleMap.setScheduleId(1L);
        when(scheduleMapRepository.save(any())).thenReturn(scheduleMap);

        multiFreqWeeklyScheduleHandler.save(iamId, Utils.getCurrentDate(), null, null, new ScheduleSensitivity(0L, 0L), doseTimeInfodoseTimeInfo);

        Mockito.verify(scheduleMapRepository, Mockito.times(1)).save(any());
        Mockito.verify(scheduleTimeMapRepository, Mockito.times(1)).saveAll(any());
    }

    @Test
    public void stopSchedulePartiallyOrFullTest_StartDateEqualScheduleTime() throws ParseException {
        ScheduleMap scheduleMap = new ScheduleMap(
                1L,
                3L,
                Utils.getCurrentDate(),
                Utils.convertStringToDate("2022-11-13 22:30:00"),
                0L,
                new ScheduleSensitivity(1L, 1L)
        );
        scheduleMap.setId(1L);
        List<ScheduleTimeMap> scheduleTimeMaps = Arrays.asList(
                new ScheduleTimeMap(1L, true, Utils.convertStringToDate("2022-11-13 22:30:00"), 3L, Time.valueOf("03:30:00"), Utils.convertStringToDate("2022-11-13 22:30:00"), null, "6666")
        );
        when(scheduleTimeMapRepository.findAllByScheduleMapIdInAndEndDateIsNull(anyList())).thenReturn(scheduleTimeMaps);

        multiFreqWeeklyScheduleHandler.stopSchedulePartiallyOrFull(scheduleMap, Arrays.asList("03:30:00"), Utils.convertStringToDate("2022-11-13 22:30:00"));

        Mockito.verify(scheduleTimeMapRepository, Mockito.times(1)).saveAll(any());
        Mockito.verify(scheduleTimeMapRepository, Mockito.times(1)).deleteAll(any());
        Mockito.verify(adhStringLogRepository, Mockito.times(1)).deleteByIamIdAndRecordDateIn(any(), any(), any());
    }

    @Test
    public void stopSchedulePartiallyOrFullTest_ScheduleTimeAfterNow() throws ParseException {
        ScheduleMap scheduleMap = new ScheduleMap(
                1L,
                3L,
                Utils.getCurrentDate(),
                Utils.convertStringToDate("2022-11-13 22:30:00"),
                0L,
                new ScheduleSensitivity(1L, 1L)
        );
        scheduleMap.setId(1L);

        Date nowHourAgo = DateUtils.addHours(Utils.getCurrentDate(), 1);
        String dateToDelete = Utils.getFormattedDate(nowHourAgo, "HH:mm:ss");
        List<ScheduleTimeMap> scheduleTimeMaps = Arrays.asList(
                new ScheduleTimeMap(1L, true, Utils.convertStringToDate("2022-11-13 22:30:00"), 3L, Time.valueOf(dateToDelete), Utils.convertStringToDate("2022-11-13 22:30:00"), null, "6666")
        );
        when(scheduleTimeMapRepository.findAllByScheduleMapIdInAndEndDateIsNull(anyList())).thenReturn(scheduleTimeMaps);

        Date dayStartTime = DateUtils.addHours(nowHourAgo, -1);

        Character code = multiFreqWeeklyScheduleHandler.stopSchedulePartiallyOrFull(scheduleMap, Arrays.asList(dateToDelete), dayStartTime).getAdherenceCode();

        Assert.assertEquals("666", scheduleTimeMaps.get(0).getAdhString());
        Assert.assertFalse(scheduleTimeMaps.get(0).isActive());
        Assert.assertEquals(DateUtils.addSeconds(dayStartTime, -1), scheduleTimeMaps.get(0).getEndDate());
        Assert.assertNull(code);
        Mockito.verify(scheduleTimeMapRepository, Mockito.times(1)).saveAll(any());
        Mockito.verify(scheduleTimeMapRepository, Mockito.times(0)).deleteAll(any());

    }

    @Test
    public void stopSchedulePartiallyOrFullTest_ScheduleTimeBeforeNow() throws ParseException {
        ScheduleMap scheduleMap = new ScheduleMap(
                1L,
                3L,
                Utils.getCurrentDate(),
                Utils.convertStringToDate("2022-11-13 22:30:00"),
                0L,
                new ScheduleSensitivity(1L, 1L)
        );
        scheduleMap.setId(1L);

        Date nowHourAfter = DateUtils.addHours(Utils.getCurrentDate(), -1);
        String dateToDelete = Utils.getFormattedDate(nowHourAfter, "HH:mm:ss");
        List<ScheduleTimeMap> scheduleTimeMaps = Arrays.asList(
                new ScheduleTimeMap(1L, true, Utils.convertStringToDate("2022-11-13 22:30:00"), 3L, Time.valueOf(dateToDelete), Utils.convertStringToDate("2022-11-13 22:30:00"), null, "6666")
        );
        when(scheduleTimeMapRepository.findAllByScheduleMapIdInAndEndDateIsNull(anyList())).thenReturn(scheduleTimeMaps);

        Date dayStartTime = DateUtils.addHours(nowHourAfter, -1);

        Character code = multiFreqWeeklyScheduleHandler.stopSchedulePartiallyOrFull(scheduleMap, Arrays.asList(dateToDelete), dayStartTime).getAdherenceCode();

        Assert.assertEquals("6666", scheduleTimeMaps.get(0).getAdhString());
        Assert.assertFalse(scheduleTimeMaps.get(0).isActive());
        Assert.assertNotNull(scheduleTimeMaps.get(0).getEndDate());
        Assert.assertEquals(Character.valueOf('6'), code);
        Mockito.verify(scheduleTimeMapRepository, Mockito.times(1)).saveAll(any());
        Mockito.verify(scheduleTimeMapRepository, Mockito.times(0)).deleteAll(any());

    }

    @Test
    public void stopSchedulePartiallyOrFullTest_NoDeletion() throws ParseException {
        ScheduleMap scheduleMap = new ScheduleMap(
                1L,
                3L,
                Utils.getCurrentDate(),
                Utils.convertStringToDate("2022-11-13 22:30:00"),
                0L,
                new ScheduleSensitivity(1L, 1L)
        );
        scheduleMap.setId(1L);

        Date nowHourAgo = DateUtils.addHours(Utils.getCurrentDate(), -1);
        String dateToDelete = Utils.getFormattedDate(nowHourAgo, "HH:mm:ss");
        List<ScheduleTimeMap> scheduleTimeMaps = Arrays.asList(
                new ScheduleTimeMap(1L, true, Utils.convertStringToDate("2022-11-13 22:30:00"), 3L, Time.valueOf(dateToDelete), Utils.convertStringToDate("2022-11-13 22:30:00"), null, "6666")
        );
        when(scheduleTimeMapRepository.findAllByScheduleMapIdInAndEndDateIsNull(anyList())).thenReturn(scheduleTimeMaps);

        Date dayStartTime = DateUtils.addHours(nowHourAgo, -1);

        Character code = multiFreqWeeklyScheduleHandler.stopSchedulePartiallyOrFull(scheduleMap, Arrays.asList("03:01:00"), dayStartTime).getAdherenceCode();

        Assert.assertEquals("6666", scheduleTimeMaps.get(0).getAdhString());
        Assert.assertTrue(scheduleTimeMaps.get(0).isActive());
        Assert.assertNull(scheduleTimeMaps.get(0).getEndDate());
        Assert.assertEquals(Character.valueOf('6'), code);
        Mockito.verify(scheduleTimeMapRepository, Mockito.times(1)).saveAll(any());
        Mockito.verify(scheduleTimeMapRepository, Mockito.times(0)).deleteAll(any());
    }

    @Test
    public void generateAdherenceForSchedules() throws ParseException {
        ScheduleMap scheduleMap = new ScheduleMap(
                1L,
                3L,
                Utils.getCurrentDate(),
                Utils.convertStringToDate("2022-11-13 22:30:00"),
                0L,
                new ScheduleSensitivity(1L, 1L)
        );
        List<ScheduleTimeMap> scheduleTimeMaps = Arrays.asList(
                new ScheduleTimeMap(1L, true, Utils.convertStringToDate("2022-11-13 22:30:00"), 3L, Time.valueOf("03:30:00"), Utils.convertStringToDate("2023-01-01 18:30:00"), null, "6666")
        );
        HashMap<Integer, List<Pair<String, Character>>> dayToAllDoses = new HashMap<>();
        List<Pair<String, Character>> dateToCharacters = Arrays.asList(
                Pair.of("2023-01-03 03:30:00", '4')
        );
        dayToAllDoses.put(0, dateToCharacters);
        multiFreqWeeklyScheduleHandler.generateAdherenceForSchedules(
            Utils.convertStringToDate("2023-01-01 18:30:00"),
            dayToAllDoses,
            scheduleTimeMaps
        );
    }

    @Test
    public void updateMultiDoseScheduleTest_AddSchedule() throws ParseException {
        ScheduleMap scheduleMap = new ScheduleMap(
                1L,
                3L,
                Utils.getCurrentDate(),
                Utils.convertStringToDate("2022-11-13 22:30:00"),
                0L,
                new ScheduleSensitivity(1L, 1L)
        );
        scheduleMap.setId(1L);
        List<ScheduleTimeMap> scheduleTimeMaps = Arrays.asList(
                new ScheduleTimeMap(1L,
                        true,
                        Utils.convertStringToDate("2022-11-13 22:30:00"),
                        3L,
                        Time.valueOf("03:30:00"),
                        Utils.convertStringToDate("2022-11-13 22:30:00"),
                        null,
                        "6669"
                )
        );
        doReturn(scheduleMap).when(multiFreqWeeklyScheduleHandler).getActiveScheduleMapForIam(any());
        when(scheduleTimeMapRepository.findAllByScheduleMapIdInAndEndDateIsNull(anyList())).thenReturn(scheduleTimeMaps);

        List<EntityRequest.DoseTimeInfo> doseDetails = Arrays.asList(
                new EntityRequest.DoseTimeInfo("03:30:00", "workout", 4),
                new EntityRequest.DoseTimeInfo("06:30:00", "workout", 4)
        );
        Registration registration = new Registration(1L, 3L, Utils.convertStringToDate("2019-08-31 18:30:00"), Utils.convertStringToDate("2019-09-06 18:30:00"), "9988799887", "666666", Utils.convertStringToDate("2019-01-06 12:23:26"), Utils.convertStringToDate("2019-01-06 12:23:26"));
        Date dayStartTime = Utils.convertStringToDate("2022-11-13 22:30:00");

        Character newAdherenceCode = multiFreqWeeklyScheduleHandler.updateMultiDoseSchedule(doseDetails, registration, dayStartTime).getAdherenceCode();
        Assert.assertEquals(AdherenceCodeEnum.MULTI_DOSE_PARTIAL.getCode(), newAdherenceCode.charValue());
        Mockito.verify(scheduleTimeMapRepository, Mockito.times(1)).saveAll(any());
    }

    @Test
    public void updateMultiDoseSchedule_DeleteSchedule() throws ParseException {
        ScheduleMap scheduleMap = new ScheduleMap(
                1L,
                3L,
                Utils.getCurrentDate(),
                Utils.convertStringToDate("2022-11-13 22:30:00"),
                0L,
                new ScheduleSensitivity(1L, 1L)
        );
        scheduleMap.setId(1L);
        List<ScheduleTimeMap> scheduleTimeMaps = Arrays.asList(
                new ScheduleTimeMap(1L,
                        true,
                        Utils.convertStringToDate("2022-11-13 22:30:00"),
                        3L,
                        Time.valueOf("03:30:00"),
                        Utils.convertStringToDate("2022-11-13 22:30:00"),
                        null,
                        "6669"
                )
        );
        doReturn(scheduleMap).when(multiFreqWeeklyScheduleHandler).getActiveScheduleMapForIam(any());
        when(scheduleTimeMapRepository.findAllByScheduleMapIdInAndEndDateIsNull(anyList())).thenReturn(scheduleTimeMaps);
        doReturn(new StopScheduleExtraInfoDto(AdherenceCodeEnum.MULTI_DOSE_PARTIAL.getCode(), null, false)).when(multiFreqWeeklyScheduleHandler).stopSchedulePartiallyOrFull(any(), any(), any());

        List<EntityRequest.DoseTimeInfo> doseDetails = Arrays.asList(
                new EntityRequest.DoseTimeInfo("06:30:00", "workout", 4)
        );

        Registration registration = new Registration(1L, 3L, Utils.convertStringToDate("2019-08-31 18:30:00"), Utils.convertStringToDate("2019-09-06 18:30:00"), "9988799887", "666666", Utils.convertStringToDate("2019-01-06 12:23:26"), Utils.convertStringToDate("2019-01-06 12:23:26"));
        Date dayStartTime = Utils.convertStringToDate("2022-11-13 22:30:00");

        StopScheduleExtraInfoDto newAdherenceCode = multiFreqWeeklyScheduleHandler.updateMultiDoseSchedule(doseDetails, registration, dayStartTime);
        Assert.assertEquals(AdherenceCodeEnum.MULTI_DOSE_PARTIAL.getCode(), newAdherenceCode.getAdherenceCode().charValue());

        Mockito.verify(scheduleTimeMapRepository, Mockito.times(1)).saveAll(any());
        Mockito.verify(multiFreqWeeklyScheduleHandler, Mockito.times(1)).stopSchedulePartiallyOrFull(any(), any(), any());
    }
}
