# How to contribute

Thank you for choosing to contribute towards the open source patient and adherence management platform - Everwell Hub.

Here are some important resources:

  * [E-Hub for Developers](https://gitlab.com/everwell/hub-foss/-/wikis/home) tells you how the architecture, features and customizations are made possible,
  * [Our blog](https://medium.com/everwell-pulse) is the 10000 ft. view of how we are realising our product goals
  * Want to report bugs? [Gitlab](https://gitlab.com/everwell/hub-foss/-/issues) is where to report them, and
  * our website [everwell.org](https://everwell.org) talks about how the E-hub has been successfully deployed and customized in improving real-life usecases and outcomes.

## Testing

We have a handful of unit tests added to each of the repositories, which we are hopeful will summarise and validate critical interactions. Please write tests for any new code you create.

## Submitting changes

Please send a [Gitlab Pull Request to e-hub-foss](https://gitlab.com/everwell/hub-foss/-/merge_requests/new) with a clear list of what you've done. When you send a pull request, we will love you 3000 if you include issue details and wiki documentation to guide us through the 1H 5Ws of the changes. We can always use more test coverage. Please follow our coding conventions (below) and make sure all of your commits are atomic (one feature per commit).

Always write a clear log message for your commits. One-line messages are fine for small changes, but bigger changes should look like this:

    $ git commit -m "A brief summary of the commit
    > 
    > A paragraph describing what changed and its impact."

## Coding conventions

Start reading our code and you'll get the hang of it. We optimize for readability and performance:

  * We indent using four spaces (hard tabs)
  * We avoid specific logic in controllers, putting services in place to capture entity logic.
  * We ALWAYS put spaces after list items and method parameters (`[1, 2, 3]`, not `[1,2,3]`), around operators (`x += 1`, not `x+=1`), and around hash arrows.
  * Hard coding is a bad practice and we avoid it like the plague.
  * This is open source software. Consider the people who will read your code, and make it look nice for them. Since there is no direct communication, the code should be made clean and modular.
