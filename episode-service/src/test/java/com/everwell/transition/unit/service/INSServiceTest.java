package com.everwell.transition.unit.service;

import com.everwell.transition.BaseTest;
import com.everwell.transition.constants.INSConstants;
import com.everwell.transition.enums.episode.EpisodeField;
import com.everwell.transition.enums.iam.IAMEndpoint;
import com.everwell.transition.enums.ins.INSEndpoint;
import com.everwell.transition.exceptions.ValidationException;
import com.everwell.transition.model.db.Client;
import com.everwell.transition.model.db.Trigger;
import com.everwell.transition.model.dto.EngagementDto;
import com.everwell.transition.model.dto.EpisodeDto;
import com.everwell.transition.model.request.EngagementRequest;
import com.everwell.transition.model.request.ins.NotificationLogRequest;
import com.everwell.transition.model.response.AdherenceCodeConfigResponse;
import com.everwell.transition.model.response.Response;
import com.everwell.transition.model.response.ins.TypeResponse;
import com.everwell.transition.postconstruct.INSTypeMap;
import com.everwell.transition.repositories.TriggerRepository;
import com.everwell.transition.service.ClientService;
import com.everwell.transition.service.INSService;
import com.everwell.transition.service.impl.INSServiceImpl;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.ArgumentMatchers;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.util.ReflectionTestUtils;

import java.util.*;

import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.when;

public class INSServiceTest extends BaseTest {

    @Spy
    @InjectMocks
    private INSServiceImpl insService;

    @Mock
    ClientService clientService;

    @Mock
    INSTypeMap insTypeMap;

    @Mock
    TriggerRepository triggerRepository;

    @Before
    public void init(){
        initNotificationMap();
        ReflectionTestUtils.setField(insService, "dataGatewayURL", "http://test:8080");
        ReflectionTestUtils.setField(insService, "username", "test");
        ReflectionTestUtils.setField(insService, "password", "test");
        when(clientService.getClientByTokenOrDefault()).thenReturn(new Client(29L,"test", "test", null ));
        insService.init();
    }

    private void initNotificationMap() {
        Map<String, Long> typeMap = new HashMap<>();
        typeMap.put(INSConstants.TYPE_SMS, 1L);
        insTypeMap.notificationTypeMap = typeMap;
    }

    @Test
    public void testGetEngagementNull() {
        Map<String, Object> stageData = new HashMap<>();
        EpisodeDto episodeDto = new EpisodeDto();
        episodeDto.setStageData(stageData);

        List<Map<String, Object>> response = insService.getEngagement(episodeDto);
        Assert.assertNull(response);
    }

    @Test
    public void testGetEngagementSuccess() {
        Map<String, Object> stageData = new HashMap<>();
        stageData.put(EpisodeField.ENGAGEMENT_EXISTS.getFieldName(), true);
        EpisodeDto episodeDto = new EpisodeDto();
        episodeDto.setId(123L);
        episodeDto.setStageData(stageData);
        Map<String, Object> responseMap = new HashMap<>();
        responseMap.put("entityId", 123L);
        ResponseEntity<Response<List<Map<String, Object>>>> insResponse =
                new ResponseEntity<>(
                        new Response<>(
                                Arrays.asList(
                                        responseMap
                                )
                        ),
                        HttpStatus.OK
                );

        doReturn(insResponse).when(insService).getExchange(any(), any(), eq(123L), any());

        List<Map<String, Object>> actualResponse = insService.getEngagement(episodeDto);
        Assert.assertEquals(insResponse.getBody().getData().get(0).get("entityId"), actualResponse.get(0).get("entityId"));
    }

    @Test
    public void testAddEngagement() {
        Map<String, Object> stageData = new HashMap<>();
        EpisodeDto episodeDto = new EpisodeDto();
        episodeDto.setStageData(stageData);

        EngagementRequest engagementRequest = new EngagementRequest(123L, true, "18:30:00", 1L, true, false, 12345L);
        doReturn(new ResponseEntity<>(
                new Response<>(
                        true, null
                ),
                HttpStatus.OK
        )).when(insService).postExchange(eq(INSEndpoint.SAVE_ENGAGEMENT_BULK.getEndpointUrl()), any(), any());


        EngagementDto response = insService.saveEngagement(engagementRequest, episodeDto);
        Assert.assertEquals(true, response.getUpdateRequest().get(EpisodeField.ENGAGEMENT_EXISTS.getFieldName()));
    }

    @Test
    public void testAddEngagementNoCallCenterCalls() {
        Map<String, Object> stageData = new HashMap<>();
        EpisodeDto episodeDto = new EpisodeDto();
        episodeDto.setStageData(stageData);

        EngagementRequest engagementRequest = new EngagementRequest(123L, true, "18:30:00", 1L, true, null, 12345L);
        doReturn(new ResponseEntity<>(
                new Response<>(
                        true, null
                ),
                HttpStatus.OK
        )).when(insService).postExchange(eq(INSEndpoint.SAVE_ENGAGEMENT_BULK.getEndpointUrl()), any(), any());


        EngagementDto response = insService.saveEngagement(engagementRequest, episodeDto);
        Assert.assertNull(response.getUpdateRequest().get(EpisodeField.CALL_CENTER_CALLS.getFieldName()));
    }

    @Test
    public void testAddEngagementNoHouseholdVisits() {
        Map<String, Object> stageData = new HashMap<>();
        EpisodeDto episodeDto = new EpisodeDto();
        episodeDto.setStageData(stageData);

        EngagementRequest engagementRequest = new EngagementRequest(123L, true, "18:30:00", 1L, null, null, 12345L);
        doReturn(new ResponseEntity<>(
                new Response<>(
                        true, null
                ),
                HttpStatus.OK
        )).when(insService).postExchange(eq(INSEndpoint.SAVE_ENGAGEMENT_BULK.getEndpointUrl()), any(), any());


        EngagementDto response = insService.saveEngagement(engagementRequest, episodeDto);
        Assert.assertNull(response.getUpdateRequest().get(EpisodeField.HOUSEHOLD_VISITS.getFieldName()));
    }

    @Test
    public void testUpdateEngagement() {
        Map<String, Object> stageData = new HashMap<>();
        stageData.put(EpisodeField.ENGAGEMENT_EXISTS.getFieldName(), true);
        EpisodeDto episodeDto = new EpisodeDto();
        episodeDto.setStageData(stageData);

        EngagementRequest engagementRequest = new EngagementRequest(123L, true, "18:30:00", 1L, true, false, 12345L);
        doReturn(new ResponseEntity<>(
                new Response<>(
                        true, null
                ),
                HttpStatus.OK
        )).when(insService).putExchange(eq(INSEndpoint.SAVE_ENGAGEMENT_BULK.getEndpointUrl()), any(), any());


        EngagementDto response = insService.saveEngagement(engagementRequest, episodeDto);
        Assert.assertEquals(true, response.getUpdateRequest().get(EpisodeField.ENGAGEMENT_EXISTS.getFieldName()));
    }

    @Test
    public void testGetNotificationType() {
        doReturn(new ResponseEntity<>(
                new Response<>(
                        new TypeResponse()
                ),
                HttpStatus.OK
        )).when(insService).getExchange(eq(INSEndpoint.GET_NOTIFICATION_TYPES.getEndpointUrl()), any(), any(), any());

        TypeResponse response = insService.getNotificationTypes();
        Assert.assertNotNull(response);

    }

    @Test
    public void testAddNotificationLogSuccess() {
        Trigger trigger = new Trigger();
        trigger.setTriggerId(1L);
        trigger.setDefaultTemplateId(1L);
        trigger.setVendorId(1L);

        NotificationLogRequest notificationLogRequest = new NotificationLogRequest();
        notificationLogRequest.setNotifications(Collections.singletonList(new HashMap<>()));

        doReturn(new ResponseEntity<>(
                new Response<>(
                        true, "added!"
                ),
                HttpStatus.OK
        )).when(insService).postExchange(eq(INSEndpoint.ADD_NOTIFICATION_LOGS.getEndpointUrl()), any(), any());
        when(triggerRepository.getTriggerByEventNameAndClientId(anyString(), anyLong())).thenReturn(trigger);

        String response = insService.addNotificationLog(notificationLogRequest, 29L);
        Assert.assertEquals(response, "added!");
    }

    @Test(expected = ValidationException.class)
    public void testAddNotificationLogFailure() {
        Trigger trigger = new Trigger();
        trigger.setTriggerId(1L);
        trigger.setDefaultTemplateId(1L);
        trigger.setVendorId(1L);

        NotificationLogRequest notificationLogRequest = new NotificationLogRequest();
        notificationLogRequest.setNotifications(Collections.singletonList(new HashMap<>()));

        doReturn(new ResponseEntity<>(
                new Response<>(
                        false, null
                ),
                HttpStatus.BAD_REQUEST
        )).when(insService).postExchange(eq(INSEndpoint.ADD_NOTIFICATION_LOGS.getEndpointUrl()), any(), any());
        when(triggerRepository.getTriggerByEventNameAndClientId(anyString(), anyLong())).thenReturn(trigger);

        String response = insService.addNotificationLog(notificationLogRequest, 29L);
    }
}
