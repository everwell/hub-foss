package com.everwell.transition.unit.service;

import com.everwell.transition.BaseTest;
import com.everwell.transition.model.db.VendorConfig;
import com.everwell.transition.model.response.WeatherResponse;
import com.everwell.transition.repositories.VendorConfigRepository;
import com.everwell.transition.service.impl.WeatherServiceImpl;
import com.fasterxml.jackson.databind.ObjectMapper;
import net.minidev.json.JSONObject;
import org.junit.Assert;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;


import java.util.*;

import static org.junit.Assert.*;
import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.*;

public class WeatherServiceTest extends BaseTest {

    @Spy
    @InjectMocks
    private WeatherServiceImpl weatherService;

    @Mock
    private VendorConfigRepository configRepository;

    @Mock
    private RestTemplate restTemplate;

    @Mock
    private ObjectMapper mapper;

    @Test
    public void testGetWeather_SuccessfulResponse() {
        // Arrange
        Double lat = 37.7749;
        Double lon = -122.4194;
        String url = "https://api.weather.com";
        String secretKey = "your-secret-key";

        // Prepare the expected response body
        Map<String, Object> expectedBody = new HashMap<>();
        expectedBody.put("temperature", 25.0);
        ResponseEntity<Map<String, Object>> responseEntity = new ResponseEntity<>(expectedBody, HttpStatus.OK);

        // Mock the restTemplate.exchange() method
        ReflectionTestUtils.setField(weatherService, "restTemplate", restTemplate);
        when(restTemplate.exchange(
                anyString(),
                eq(HttpMethod.GET),
                eq(null),
                eq(new ParameterizedTypeReference<Map<String, Object>>() {}))
        ).thenReturn(responseEntity);

        // Act
        Map<String, Object> result = weatherService.getWeather(lat, lon, url, secretKey);

        // Assert
        assertEquals(expectedBody, result);
        verify(restTemplate, times(1)).exchange(anyString(), eq(HttpMethod.GET), eq(null), eq(new ParameterizedTypeReference<Map<String, Object>>() {}));
    }

    @Test
    public void testGetWeather_RetryOnHttpClientErrorException() {
        // Arrange
        Double lat = 37.7749;
        Double lon = -122.4194;
        String url = "https://api.weather.com";
        String secretKey = "your-secret-key";

        // Mock the restTemplate.exchange() method to throw HttpClientErrorException twice and then return a successful response
        ReflectionTestUtils.setField(weatherService, "restTemplate", restTemplate);
        when(restTemplate.exchange(anyString(), eq(HttpMethod.GET), eq(null), eq(new ParameterizedTypeReference<Map<String, Object>>() {})))
                .thenThrow(new HttpClientErrorException(HttpStatus.BAD_GATEWAY))
                .thenThrow(new HttpClientErrorException(HttpStatus.NOT_FOUND))
                .thenReturn(new ResponseEntity<>(new HashMap<>(), HttpStatus.OK));

        // Act
        Map<String, Object> result = weatherService.getWeather(lat, lon, url, secretKey);

        // Assert
        assertEquals(result.size(), 0);
        verify(restTemplate, times(3)).exchange(anyString(), eq(HttpMethod.GET), eq(null), eq(new ParameterizedTypeReference<Map<String, Object>>() {}));
    }

    @Test
    public void testGetCompleteWeather_SuccessfulResponse() {
        // Arrange
        Double lat = 37.7749;
        Double lon = -122.4194;
        Long clientId = 123L;

        // Prepare the mock vendor config
        VendorConfig vendorConfig = new VendorConfig();
        vendorConfig.setCredentials("{\"base_site_url\":\"https://api.weather.com/\",\"client_secret\":\"your-secret-key\",\"image_url\":\"https://your-image-url.com/%s\"}");
        when(configRepository.findByClientIdAndVendor(anyString(), anyString())).thenReturn(vendorConfig);

        // Prepare the mock response from getWeather()
        Map<String, Object> response = new HashMap<>();
        response.put("coord", new HashMap<String, Object>() {{
            put("lon", -122.4194);
            put("lat", 37.7749);
        }});
        response.put("main", new HashMap<String, Object>() {{
            put("temp", 25.0);
            put("feels_like", 23.5);
            put("temp_min", 22.0);
            put("temp_max", 28.0);
            put("pressure", 1015);
            put("humidity", 70);
        }});
        List<Map<String, Object>> weatherList = new ArrayList<>();
        weatherList.add(new HashMap<String, Object>() {{
            put("main", "Clear");
            put("description", "clear sky");
            put("icon", "01d");
        }});
        response.put("weather", weatherList);
        when(weatherService.getWeather(lat, lon, "https://api.weather.com/weather", "your-secret-key")).thenReturn(response);

        // Prepare the mock response from getWeather() for air pollution
        Map<String, Object> airPollutionResponse = new HashMap<>();
        List<Map<String, Object>> airPollutionList = new ArrayList<>();
        airPollutionList.add(new HashMap<String, Object>() {{
            put("main", new HashMap<String, Object>() {{
                put("aqi", "45");
            }});
            put("components", new HashMap<String, Object>() {{
                put("co", "0.3");
                put("no", "0.02");
                put("no2", "0.03");
                put("o3", "0.028");
                put("so2", "0.012");
                put("pm2_5", "10");
                put("pm10", "20");
                put("nh3", "0.015");
            }});
        }});
        airPollutionResponse.put("list", airPollutionList);
        when(weatherService.getWeather(lat, lon, "https://api.weather.com/air_pollution", "your-secret-key")).thenReturn(airPollutionResponse);

        // Act
        WeatherResponse result = weatherService.getCompleteWeather(lat, lon, clientId);

        // Assert
        assertNotNull(result);
        assertEquals("-122.4194", result.getCoord().getLon());
        assertEquals("37.7749", result.getCoord().getLat());
        assertEquals("25.0", result.getMain().getTemp());
        assertEquals("23.5", result.getMain().getFeelsLike());
        assertEquals("22.0", result.getMain().getTempMin());
        assertEquals("28.0", result.getMain().getTempMax());
        assertEquals("1015", result.getMain().getPressure());
        assertEquals("70", result.getMain().getHumidity());
        assertEquals(1, result.getWeather().size());
        assertEquals("Clear", result.getWeather().get(0).getMain());
        assertEquals("clear sky", result.getWeather().get(0).getDescription());
        assertEquals("01d", result.getWeather().get(0).getIcon());
        assertEquals("https://your-image-url.com/01d", result.getWeather().get(0).getIconUrl());
        assertEquals(1, result.getAqi().size());
        assertEquals("45", result.getAqi().get(0).getAqi());
        assertEquals("0.3", result.getAqi().get(0).getCo());
        assertEquals("0.02", result.getAqi().get(0).getNo());
        assertEquals("0.03", result.getAqi().get(0).getNo2());
        assertEquals("0.028", result.getAqi().get(0).getO3());
        assertEquals("0.012", result.getAqi().get(0).getSo2());
        assertEquals("10", result.getAqi().get(0).getPm2_5());
        assertEquals("20", result.getAqi().get(0).getPm10());
        assertEquals("0.015", result.getAqi().get(0).getNh3());

        verify(configRepository, times(1)).findByClientIdAndVendor(anyString(), anyString());
        verify(weatherService, times(1)).getWeather(lat, lon, "https://api.weather.com/weather", "your-secret-key");
        verify(weatherService, times(1)).getWeather(lat, lon, "https://api.weather.com/air_pollution", "your-secret-key");
    }

    @Test
    public void testGetCompleteWeather_ExceptionInConfigParsing() {
        // Arrange
        Double lat = 37.7749;
        Double lon = -122.4194;
        Long clientId = 123L;

        // Prepare the mock vendor config with invalid credentials
        VendorConfig vendorConfig = new VendorConfig();
        vendorConfig.setCredentials("{\"invalid\":\"credentials\"}");
        when(configRepository.findByClientIdAndVendor(anyString(), anyString())).thenReturn(null);

        // Act
        WeatherResponse result = weatherService.getCompleteWeather(lat, lon, clientId);

        // Assert
        assertNotNull(result);
        assertNull(result.getCoord());
        assertNull(result.getMain());
        assertNull(result.getWeather());
        assertNull(result.getAqi());

        verify(configRepository, times(1)).findByClientIdAndVendor(anyString(), anyString());
        verify(weatherService, never()).getWeather(anyDouble(), anyDouble(), anyString(), anyString());
    }
}
