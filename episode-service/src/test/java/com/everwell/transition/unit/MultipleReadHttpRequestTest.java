package com.everwell.transition.unit;

import com.everwell.transition.BaseTest;
import com.everwell.transition.exceptions.InternalServerErrorException;
import com.everwell.transition.utils.MultipleReadHttpRequest;
import org.apache.commons.io.IOUtils;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;

import javax.servlet.ReadListener;
import javax.servlet.ServletInputStream;
import javax.servlet.http.HttpServletRequest;
import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;

import static org.mockito.Mockito.when;

public class MultipleReadHttpRequestTest extends BaseTest {

    @Mock
    HttpServletRequest httpServletRequest;

    MultipleReadHttpRequest multipleReadHttpRequest;

    @Mock
    InputStream inputStream;

    static String inputString = "temp_body";
    static String emptyInput = "";

    @Before
    public void init() throws IOException {
        when(httpServletRequest.getInputStream()).thenReturn(getServletInputStream(inputString));
        multipleReadHttpRequest = new MultipleReadHttpRequest(httpServletRequest);
    }

    @Test
    public void constructorTest() throws IOException {
        Assert.assertEquals(new String(multipleReadHttpRequest.getCacheData()), inputString);
    }

    @Test
    public void getInputStreamTest() throws IOException {
        BufferedReader bufferedReader = multipleReadHttpRequest.getReader();
        Assert.assertEquals(bufferedReader.readLine(), inputString);
    }

    @Test
    public void CachedBodyServletInputStreamAvailableTest() throws IOException {
        ServletInputStream cachedBodyServletInputStream = multipleReadHttpRequest.getInputStream();
        cachedBodyServletInputStream.setReadListener(null);
        Assert.assertFalse(cachedBodyServletInputStream.isFinished());
        Assert.assertTrue(cachedBodyServletInputStream.isReady());
        Assert.assertEquals(inputString.charAt(0), (char)cachedBodyServletInputStream.read());
    }

    @Test
    public void CachedBodyServletInputStreamFinishedTest() throws IOException {
        multipleReadHttpRequest.setCacheData(IOUtils.toByteArray(getServletInputStream(emptyInput)));
        ServletInputStream cachedBodyServletInputStream = multipleReadHttpRequest.getInputStream();
        cachedBodyServletInputStream.setReadListener(null);
        Assert.assertTrue(cachedBodyServletInputStream.isFinished());
        Assert.assertTrue(cachedBodyServletInputStream.isReady());
        Assert.assertEquals(-1, cachedBodyServletInputStream.read());
    }

    @Test(expected = InternalServerErrorException.class)
    public void isFinishedIOException() throws IOException {
        MultipleReadHttpRequest.CachedBodyServletInputStream cachedBodyServletInputStream = new MultipleReadHttpRequest.CachedBodyServletInputStream(inputStream);
        when(inputStream.available()).thenThrow(new IOException());
        cachedBodyServletInputStream.isFinished();
    }

    public static ServletInputStream getServletInputStream(String input) {
        final ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(input.getBytes());
        return new ServletInputStream(){
            @Override
            public boolean isFinished() {
                return false;
            }

            @Override
            public boolean isReady() {
                return false;
            }

            @Override
            public void setReadListener(ReadListener readListener) {

            }

            public int read() {
                return byteArrayInputStream.read();
            }
        };
    }


}
