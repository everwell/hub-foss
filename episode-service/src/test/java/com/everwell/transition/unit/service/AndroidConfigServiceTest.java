package com.everwell.transition.unit.service;

import com.everwell.transition.BaseTest;
import com.everwell.transition.constants.LocaliseKeyConstants;
import com.everwell.transition.model.request.wix.WixPostResponse;
import com.everwell.transition.model.response.android.GenericUiResponse;
import com.everwell.transition.service.LocaliseService;
import com.everwell.transition.service.WixApiIntegrationService;
import com.everwell.transition.service.impl.AndroidConfigServiceImpl;
import org.junit.Assert;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.when;

public class AndroidConfigServiceTest extends BaseTest {

    @InjectMocks
    private AndroidConfigServiceImpl androidConfigService;

    @Mock
    WixApiIntegrationService wixApiIntegrationService;

    @Mock
    LocaliseService localiseService;

    @Test
    public void getFitFeedTest() {
        String language = "en";
        String region = "IN";

        when (localiseService.getTranslations(eq(language), eq(region))).thenReturn(LocaliseKeyConstants.defaultTranslationMap);

        List<WixPostResponse> wixPostResponses = Arrays.asList(
                new WixPostResponse("url", "title", "category", "post", "en")
        );

        when (wixApiIntegrationService.listPosts(any())).thenReturn(wixPostResponses);

        Map<String, WixPostResponse> map = new HashMap<>();
        map.put("id_1", new WixPostResponse("url", "title", "category", "post", "en"));

        when (wixApiIntegrationService.listCategoriesFilter(any())).thenReturn(map);

        List<GenericUiResponse> feed = androidConfigService.getFitFeed(language, region);

        Assert.assertEquals(3, feed.size());
        Assert.assertEquals("3x2", feed.get(1).getSectionType());
    }

    @Test
    public void getHomeFitFeedTest() {
        String language = "en";
        String region = "IN";

        when (localiseService.getTranslations(eq(language), eq(region))).thenReturn(LocaliseKeyConstants.defaultTranslationMap);

        List<WixPostResponse> wixPostResponses = Arrays.asList(
                new WixPostResponse("url", "title", "category", "post", "en")
        );

        List<GenericUiResponse> feed = androidConfigService.getHomeFeed(language, region);

        Assert.assertEquals(6, feed.size());
    }

}
