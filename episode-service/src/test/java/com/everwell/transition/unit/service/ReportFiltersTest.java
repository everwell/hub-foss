package com.everwell.transition.unit.service;

import com.everwell.transition.BaseTest;
import com.everwell.transition.constants.FieldConstants;
import com.everwell.transition.elasticsearch.service.Impl.ReportFiltersToEpisodeSearchFields;
import com.everwell.transition.exceptions.ValidationException;
import com.everwell.transition.model.dto.EpisodeReportsFilter;
import com.everwell.transition.model.dto.EpisodeReportsRequest;
import com.everwell.transition.model.request.episodesearch.EpisodeSearchRequest;
import org.junit.Assert;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Spy;

import java.util.Arrays;


public class ReportFiltersTest extends BaseTest {

    @Spy
    @InjectMocks
    ReportFiltersToEpisodeSearchFields reportFiltersToEpisodeSearchFields;

    @Test
    public void convertDateTypeToRegistration() {
        EpisodeReportsFilter episodeReportsFilter = new EpisodeReportsFilter();
        episodeReportsFilter.setDateType(FieldConstants.FIELD_REGISTRATION_DATE);
        episodeReportsFilter.setStartDate("2022-01-01 00:00:00");
        episodeReportsFilter.setEndDate("2022-01-02 00:00:00");
        EpisodeReportsRequest episodeReportsRequest = new EpisodeReportsRequest(null, episodeReportsFilter);
        EpisodeSearchRequest episodeSearchRequest = reportFiltersToEpisodeSearchFields.convert(episodeReportsRequest);
        Assert.assertNotNull(episodeSearchRequest.getRange().get(FieldConstants.EPISODE_FIELD_CREATED_DATE));
    }

    @Test
    public void convertDateTypeToTbTreatmentStartDate_withRangeNull() {
        EpisodeReportsFilter episodeReportsFilter = new EpisodeReportsFilter();
        episodeReportsFilter.setDateType(FieldConstants.TB_TREATMENT_START_DATE);
        episodeReportsFilter.setStartDate("2022-01-01 00:00:00");
        episodeReportsFilter.setEndDate("2022-01-02 00:00:00");
        EpisodeSearchRequest episodeSearchRequest = new EpisodeSearchRequest();
        episodeSearchRequest.setRange(null);
        EpisodeReportsRequest episodeReportsRequest = new EpisodeReportsRequest(episodeSearchRequest, episodeReportsFilter);
        EpisodeSearchRequest episodeSearchRequestResponse = reportFiltersToEpisodeSearchFields.convert(episodeReportsRequest);
        Assert.assertNotNull(episodeSearchRequestResponse.getRange().get(FieldConstants.EPISODE_FIELD_START_DATE));
    }

    @Test(expected = ValidationException.class)
    public void convertDateTypeThrowsException() {
        EpisodeReportsFilter episodeReportsFilter = new EpisodeReportsFilter();
        episodeReportsFilter.setDateType("Test");
        episodeReportsFilter.setStartDate("2022-01-01 00:00:00");
        episodeReportsFilter.setEndDate("2022-01-02 00:00:00");
        EpisodeReportsRequest episodeReportsRequest = new EpisodeReportsRequest(new EpisodeSearchRequest(), episodeReportsFilter);
        EpisodeSearchRequest episodeSearchRequest = reportFiltersToEpisodeSearchFields.convert(episodeReportsRequest);
    }

    @Test
    public void convertAllFieldsToEpisodeSearchRequest() {
        EpisodeReportsFilter episodeReportsFilter = new EpisodeReportsFilter("residence_all", Arrays.asList(1L, 2L), Arrays.asList(3L, 5L), Arrays.asList("IndiaTbPublic"), Arrays.asList("ON_TREATMENT"),
                null, "2022-01-01", "2022-02-02");
        EpisodeReportsRequest episodeReportsRequest = new EpisodeReportsRequest(new EpisodeSearchRequest(), episodeReportsFilter);
        EpisodeSearchRequest episodeSearchRequest = reportFiltersToEpisodeSearchFields.convert(episodeReportsRequest);
        Assert.assertEquals(episodeSearchRequest.getSearch().getMust().get(FieldConstants.EPISODE_FIELD_ID), Arrays.asList(3L, 5L));
        Assert.assertEquals(episodeSearchRequest.getSearch().getMust().get("residence_all"), Arrays.asList(1L, 2L));
        Assert.assertEquals(episodeSearchRequest.getSearch().getMust().get(FieldConstants.EPISODE_FIELD_STAGE_KEY), Arrays.asList("ON_TREATMENT"));
    }

    @Test
    public void convertAllEmptyFieldsToEpisodeSearchRequest() {
        EpisodeReportsFilter episodeReportsFilter = new EpisodeReportsFilter();
        EpisodeReportsRequest episodeReportsRequest = new EpisodeReportsRequest(new EpisodeSearchRequest(), episodeReportsFilter);
        EpisodeSearchRequest episodeSearchRequest = reportFiltersToEpisodeSearchFields.convert(episodeReportsRequest);
        Assert.assertNull(episodeSearchRequest.getSearch().getMust().get(FieldConstants.EPISODE_FIELD_ID));
        Assert.assertNull(episodeSearchRequest.getSearch().getMust().get("residence_all"));
        Assert.assertNull(episodeSearchRequest.getSearch().getMust().get(FieldConstants.EPISODE_FIELD_STAGE_KEY));
    }
}
