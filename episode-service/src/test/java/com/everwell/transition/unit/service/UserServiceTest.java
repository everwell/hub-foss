package com.everwell.transition.unit.service;

import com.everwell.transition.BaseTest;
import com.everwell.transition.enums.Event;
import com.everwell.transition.enums.user.UserServiceEndpoint;
import com.everwell.transition.enums.user.UserServiceField;
import com.everwell.transition.exceptions.ValidationException;
import com.everwell.transition.model.db.Client;
import com.everwell.transition.model.dto.EventDto;
import com.everwell.transition.model.response.Response;
import com.everwell.transition.service.ClientService;
import com.everwell.transition.service.impl.UserServiceImpl;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.jupiter.api.Assertions;
import org.mockito.*;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.util.ReflectionTestUtils;


import java.util.*;
import java.util.stream.Collectors;

import static org.mockito.Mockito.*;

public class UserServiceTest extends BaseTest {

    @Spy
    @InjectMocks
    UserServiceImpl userService;

    @Mock
    ClientService clientService;

    @Mock
    ApplicationEventPublisher applicationEventPublisher;

    @Before
    public void init() {
        ReflectionTestUtils.setField(userService, "dataGatewayURL", "http://test:8080");
        ReflectionTestUtils.setField(userService, "username", "test");
        ReflectionTestUtils.setField(userService, "password", "test");
        when(clientService.getClientByTokenOrDefault()).thenReturn( new Client(29L,"test", "test", null ));
        userService.init();
    }

    private Map<String, Object> getUserRequestTestDataOne() {
        Map<String, Object> userResponse = new HashMap<>();
        userResponse.put("primaryPhoneNumber", "9999999999");
        userResponse.put("firstName", "testName");
        return userResponse;
    }

    private Map<String, Object> getUserRequestTestDataTwo() {
        Map<String, Object> userResponse = new HashMap<>();
        Map<String, Object> mobileObject = new HashMap<String, Object>(){ { put("number", "1234567891"); put("primary", true); } };
        List<Map<String, Object>> mobileList = Collections.singletonList(mobileObject);
        Map<String, Object> emailObject = new HashMap<String, Object>(){ { put("emailId", "abc@email.con"); put("primary", true);} };
        List<Map<String, Object>> emailList = Collections.singletonList(emailObject);
        userResponse.put("primaryPhoneNumber", "9999999999");
        userResponse.put("firstName", "testName");
        userResponse.put("mobileList", mobileList);
        userResponse.put("emailList", emailList);
        return userResponse;
    }

    private Map<String, Object> getUserRequestTestDataThree() {
        Map<String, Object> userResponse = new HashMap<>();
        userResponse.put("deletedBy", "500");
        userResponse.put("deletedByType", "USER");
        return userResponse;
    }

    private Map<String, Object> getUserResponseTestDataOne(Long personId) {
        Map<String, Object> userResponse = new HashMap<>(getUserRequestTestDataOne());
        userResponse.put("id", personId);
        return userResponse;
    }

    private List<Map<String, Object>> getUserResponseListTestDataOne() {
        List<Map<String, Object>> userResponseList = new ArrayList<>();
        userResponseList.add(getUserResponseTestDataOne(1L));
        return userResponseList;
    }

    private Map<String, Object> getFetchDuplicateRequestOne() {
        Map<String, Object> fetchDuplicateMap = new HashMap<>();
        fetchDuplicateMap.put("offset", 0L);
        fetchDuplicateMap.put("count", 10L);
        fetchDuplicateMap.put("scheme", "PRIMARYPHONE_GENDER");
        fetchDuplicateMap.put("primaryPhoneNumber", "9988799887" );
        fetchDuplicateMap.put("gender", "Male");
        return fetchDuplicateMap;
    }

    @Test
    public void getPrimaryPhoneTest() {
        ResponseEntity<Response<List<Map<String, Object>>>> userResponse =
                new ResponseEntity<>(new Response<>(getUserResponseListTestDataOne()), HttpStatus.OK);

        doReturn(userResponse).when(userService).getExchange(eq(UserServiceEndpoint.GET_USER_BY_PRIMARY_PHONE.getEndpointUrl()), any(), any(), any());

        ResponseEntity<Response<List<Map<String, Object>>>> userResponseList = userService.getByPrimaryPhone("9999999999");

        Assert.assertEquals("9999999999", Objects.requireNonNull(userResponseList.getBody()).getData().get(0).get("primaryPhoneNumber"));
    }

    @Test
    public void fetchDuplicatesTest() {
        List<Map<String, Object>> userResponseList = getUserResponseListTestDataOne();
        ResponseEntity<Response<List<Map<String, Object>>>> userResponse =
                new ResponseEntity<>(new Response<>(userResponseList), HttpStatus.OK);
        List<Long> responsePersonIds = userResponseList
                .stream().map(i->Long.parseLong(String.valueOf(i.get(UserServiceField.Id.getFieldName())))).collect(Collectors.toList());

        doReturn(userResponse).when(userService).postExchange(eq(UserServiceEndpoint.FETCH_DUPLICATE_USER.getEndpointUrl()), ArgumentMatchers.any(), ArgumentMatchers.any());

        List<Long> personIds = userService.fetchDuplicates(getFetchDuplicateRequestOne());

        Assert.assertEquals(personIds.size(), Objects.requireNonNull(userResponse.getBody()).getData().size());
        Assert.assertEquals(personIds, responsePersonIds);
        Mockito.verify(userService, Mockito.times(1)).postExchange(anyString(), any(), any());
    }

    @Test
    public void getUserDetailsWithNullPersonIdTest() {
        Map<String, Object> userResponseMap = userService.getUserDetails(null);

        Assert.assertEquals(0L, userResponseMap.size());
        Mockito.verify(userService, Mockito.times(0)).getExchange(anyString(), any(), any(), any());
    }

    @Test
    public void getUserDetailsTest() {
        Long personId = 1L;
        ResponseEntity<Response<Map<String, Object>>> userResponse =
                new ResponseEntity<>(new Response<>(getUserResponseTestDataOne(personId)), HttpStatus.OK);

        doReturn(userResponse).when(userService).getExchange(eq(UserServiceEndpoint.GET_USER.getEndpointUrl()), any(), any(), any());

        Map<String, Object> userResponseMap = userService.getUserDetails(personId);

        Assert.assertEquals(3L, userResponseMap.size());
        Assert.assertTrue(userResponseMap.containsKey("firstName"));
        Mockito.verify(userService, Mockito.times(1)).getExchange(anyString(), any(), any(), any());
    }

    @Test(expected = ValidationException.class)
    public void getUserDetailsWithExceptionTest() {
        ResponseEntity<Response<Map<String, Object>>> userResponse =
                new ResponseEntity<>(new Response<>(null , false), HttpStatus.OK);

        doReturn(userResponse).when(userService).getExchange(eq(UserServiceEndpoint.GET_USER.getEndpointUrl()), any(), any(), any());

        Map<String, Object> userResponseMap = userService.getUserDetails(1L);

        Mockito.verify(userService, Mockito.times(1)).getExchange(anyString(), any(), any(), any());
    }


    @Test
    public void createUserDetailsTest() {
        Map<String, Object> createUserRequest = getUserRequestTestDataOne();
        Long actualPersonId = 1L;
        ResponseEntity<Response<Map<String, Object>>> userResponse =
                new ResponseEntity<>(new Response<>(getUserResponseTestDataOne(actualPersonId)), HttpStatus.OK);

        doReturn(userResponse).when(userService).postExchange(anyString(), any(), any());

        Long responsePersonId = userService.createUser(createUserRequest);

        Assert.assertEquals(actualPersonId, responsePersonId);
        Mockito.verify(userService, Mockito.times(1)).postExchange(eq(UserServiceEndpoint.CREATE_USER.getEndpointUrl()), any(), any());
    }

    @Test
    public <T> void updateUserDetailsWithMobileAndEmailDetailsTest() {
        Long personId = 1L;
        Map<String, Object> updateUserRequest = getUserRequestTestDataTwo();;
        EventDto<T> updateUserMobileEventDto = new EventDto(Event.UPDATE_USER_MOBILE, updateUserRequest);
        EventDto<T> updateUserEmailEventDto = new EventDto(Event.UPDATE_USER_EMAIL, updateUserRequest);
        ResponseEntity<Response<String>> userResponse =
                new ResponseEntity<>(new Response<>(true,"Update Success"), HttpStatus.OK);

        doReturn(userResponse).when(userService).putExchange(anyString(), any(), any());

        userService.updateUser(updateUserRequest, personId);

        Mockito.verify(userService, Mockito.times(1)).putExchange(eq(UserServiceEndpoint.UPDATE_USER.getEndpointUrl()), any(), any());
        verify(applicationEventPublisher, Mockito.times(1)).publishEvent(updateUserMobileEventDto);
        verify(applicationEventPublisher, Mockito.times(1)).publishEvent(updateUserEmailEventDto);
    }

    @Test
    public <T> void updateUserDetailsWithOnlyUserUpdate() {
        Long personId = 1L;
        Map<String, Object> updateUserRequest = getUserRequestTestDataOne();
        EventDto<T> updateUserMobileEventDto = new EventDto(Event.UPDATE_USER_MOBILE, updateUserRequest);
        EventDto<T> updateUserEmailEventDto = new EventDto(Event.UPDATE_USER_EMAIL, updateUserRequest);
        ResponseEntity<Response<String>> userResponse =
                new ResponseEntity<>(new Response<>(true,"Update Success"), HttpStatus.OK);

        doReturn(userResponse).when(userService).putExchange(anyString(), any(), any());

        userService.updateUser(updateUserRequest, personId);

        Mockito.verify(userService, Mockito.times(1)).putExchange(eq(UserServiceEndpoint.UPDATE_USER.getEndpointUrl()), any(), any());
        verify(applicationEventPublisher, Mockito.times(0)).publishEvent(updateUserMobileEventDto);
        verify(applicationEventPublisher, Mockito.times(0)).publishEvent(updateUserEmailEventDto);
    }

    @Test
    public <T> void updateUserDetailsNoUserUpdate() {
        Long personId = 1L;

        userService.updateUser(new HashMap<>(), personId);

        Mockito.verify(userService, Mockito.times(0)).putExchange(eq(UserServiceEndpoint.UPDATE_USER.getEndpointUrl()), any(), any());
        verify(applicationEventPublisher, Mockito.times(0)).publishEvent(any());
        verify(applicationEventPublisher, Mockito.times(0)).publishEvent(any());
    }

    @Test
    public <T> void deleteUserTest() {
        Long personId = 1L;
        Map<String, Object> deleteUserRequest = getUserRequestTestDataThree();
        EventDto<T> deleteUserEventDto = new EventDto(Event.DELETE_USER, deleteUserRequest);

        userService.deleteUser(deleteUserRequest, personId);

        verify(applicationEventPublisher, Mockito.times(1)).publishEvent(deleteUserEventDto);
    }

    @Test
    public <T> void deleteUserTestWithNoDeleteData() {
        Long personId = 1L;
        Map<String, Object> deleteUserRequest = new HashMap<>();
        EventDto<T> deleteUserEventDto = new EventDto(Event.DELETE_USER, deleteUserRequest);

        userService.deleteUser(deleteUserRequest, personId);

        verify(applicationEventPublisher, Mockito.times(0)).publishEvent(deleteUserEventDto);
    } 

    @Test
    public void testAddUserRelation() {
        doReturn(new ResponseEntity<>(new Response<>("profile added", true), HttpStatus.OK)).when(userService).postExchange(eq(UserServiceEndpoint.ADD_RELATION.getEndpointUrl()), any(), any());

        Assertions.assertDoesNotThrow(() -> userService.addUserRelation(new HashMap<>()));
    }

    @Test(expected = ValidationException.class)
    public void testAddUserRelationFailure() {
        doReturn(new ResponseEntity<>(new Response<>("error"), HttpStatus.BAD_REQUEST)).when(userService).postExchange(eq(UserServiceEndpoint.ADD_RELATION.getEndpointUrl()), any(), any());

        userService.addUserRelation(new HashMap<>());
    }

    @Test
    public void testUpdateUserRelation() {
        doReturn(new ResponseEntity<>(new Response<>("profile updated", true), HttpStatus.OK)).when(userService).putExchange(eq(UserServiceEndpoint.UPDATE_RELATION.getEndpointUrl()), any(), any());

        Assertions.assertDoesNotThrow(() -> userService.updateUserRelation(new HashMap<>()));
    }

    @Test(expected = ValidationException.class)
    public void testUpdateUserRelationFailure() {
        doReturn(new ResponseEntity<>(new Response<>("error"), HttpStatus.BAD_REQUEST)).when(userService).putExchange(eq(UserServiceEndpoint.UPDATE_RELATION.getEndpointUrl()), any(), any());

        userService.updateUserRelation(new HashMap<>());
    }

    @Test
    public void testDeleteUserRelation() {
        doReturn(new ResponseEntity<>(new Response<>("profile deleted", true), HttpStatus.OK)).when(userService).deleteExchange(eq(UserServiceEndpoint.ADD_RELATION.getEndpointUrl()), any(), any(), any(), any());

        Assertions.assertDoesNotThrow(() -> userService.deleteUserRelation(new HashMap<>()));
    }

    @Test(expected = ValidationException.class)
    public void testDeleteUserRelationFailure() {
        doReturn(new ResponseEntity<>(new Response<>("error"), HttpStatus.BAD_REQUEST)).when(userService).deleteExchange(eq(UserServiceEndpoint.ADD_RELATION.getEndpointUrl()), any(), any(), any(), any());

        userService.deleteUserRelation(new HashMap<>());
    }

    @Test
    public void testGetUserRelations() {
        doReturn(new ResponseEntity<>(new Response(true, null), HttpStatus.OK)).when(userService).getExchange(eq(UserServiceEndpoint.GET_RELATIONS.getEndpointUrl()), any(), any(), any());

        ResponseEntity<Response<List<Map<String, Object>>>> response = userService.getUserRelations(123L, "ACCEPT");
        Assert.assertTrue(response.getBody().isSuccess());
    }

    @Test
    public void testGetUserRelationsPhone() {
        doReturn(new ResponseEntity<>(new Response(true, null), HttpStatus.OK)).when(userService).getExchange(eq(UserServiceEndpoint.GET_RELATIONS_FOR_PHONE.getEndpointUrl()), any(), any(), any());

        ResponseEntity<Response<List<Map<String, Object>>>> response = userService.getUserRelationsPrimaryPhone("9876512345",123L);
        Assert.assertTrue(response.getBody().isSuccess());
    }

    @Test
    public void testGetAllVitals() {
        String endPoint = UserServiceEndpoint.GET_VITALS.getEndpointUrl();
        doReturn(new ResponseEntity<>(new Response(true, new ArrayList<>()), HttpStatus.OK)).when(userService).getExchange(eq(endPoint), any(), any(), any());

        ResponseEntity<Response<List<Map<String, Object>>>> response = userService.getAllVitals();

        Assert.assertNotNull(response.getBody());
        Assert.assertTrue(response.getBody().isSuccess());
        Mockito.verify(userService, Mockito.times(1)).getExchange(eq(endPoint), any(), any(), any());
    }

    @Test
    public void testGetVitalsById() {
        Long vitalId = 1L;
        String endPoint = UserServiceEndpoint.GET_VITALS_BY_ID.getEndpointUrl();
        doReturn( new ResponseEntity<>(new Response(true, new HashMap<>()), HttpStatus.OK)).when(userService).getExchange(eq(endPoint), any(), eq(vitalId), any());

        ResponseEntity<Response<Map<String, Object>>> response = userService.getVitalsById(vitalId);

        Assert.assertNotNull(response.getBody());
        Assert.assertTrue(response.getBody().isSuccess());
        Mockito.verify(userService, Mockito.times(1)).getExchange(eq(endPoint), any(), any(), any());
    }

    @Test
    public void testSaveVitals() {
        String endPoint = UserServiceEndpoint.ADD_VITALS.getEndpointUrl();
        doReturn(new ResponseEntity<>(new Response(true, new ArrayList<>()), HttpStatus.CREATED)).when(userService).postExchange(eq(endPoint), any(), any());

        ResponseEntity<Response<List<Map<String, Object>>>> response = userService.saveVitals(new HashMap<>());

        Assert.assertNotNull(response.getBody());
        Assert.assertTrue(response.getBody().isSuccess());
        Mockito.verify(userService, Mockito.times(1)).postExchange(eq(endPoint), any(), any());
    }

    @Test
    public void testUpdateVitalsById() {
        Long vitalId = 1L;
        String endPoint = String.format(UserServiceEndpoint.UPDATE_VITALS_BY_ID.getEndpointUrl(), vitalId);
        doReturn(new ResponseEntity<>(new Response(true, new HashMap<>()), HttpStatus.OK)).when(userService).putExchange(eq(endPoint), any(), any());

        ResponseEntity<Response<Map<String, Object>>> response = userService.updateVitalsById(vitalId, new HashMap<>());

        Assert.assertNotNull(response.getBody());
        Assert.assertTrue(response.getBody().isSuccess());
        Mockito.verify(userService, Mockito.times(1)).putExchange(eq(endPoint), any(), any());
    }

    @Test
    public void testDeleteVitalsById() {
        String endPoint = UserServiceEndpoint.DELETE_VITALS_BY_ID.getEndpointUrl();
        doReturn(new ResponseEntity<>(new Response(true, null), HttpStatus.OK)).when(userService).deleteExchange(eq(endPoint), any(), any(), any(), any());

        ResponseEntity<Response<String>> response = userService.deleteVitalsById(1L);

        Assert.assertNotNull(response.getBody());
        Assert.assertTrue(response.getBody().isSuccess());
        Mockito.verify(userService, Mockito.times(1)).deleteExchange(eq(endPoint), any(), any(), any(), any());
    }

    @Test
    public void testGetVitalsGoalByUserId() {
        Long userId = 1L;
        String endPoint = String.format(UserServiceEndpoint.GET_USER_VITALS_GOAL.getEndpointUrl(), userId);
        doReturn(new ResponseEntity<>(new Response(true, new ArrayList<>()), HttpStatus.OK)).when(userService).getExchange(eq(endPoint), any(), any(), any());

        ResponseEntity<Response<List<Map<String, Object>>>> response = userService.getVitalsGoalByUserId(1L);

        Assert.assertNotNull(response.getBody());
        Assert.assertTrue(response.getBody().isSuccess());
        Mockito.verify(userService, Mockito.times(1)).getExchange(eq(endPoint), any(), any(), any());
    }


    @Test
    public void testSaveVitalsGoal() {
        Long userId = 1L;
        String endPoint = String.format(UserServiceEndpoint.ADD_USER_VITALS_GOAL.getEndpointUrl(), userId);
        doReturn(new ResponseEntity<>(new Response(true, null), HttpStatus.CREATED)).when(userService).postExchange(eq(endPoint), any(), any());

        ResponseEntity<Response<String>> response = userService.saveUserVitalsGoal(userId, new HashMap<>());

        Assert.assertNotNull(response.getBody());
        Assert.assertTrue(response.getBody().isSuccess());
        Mockito.verify(userService, Mockito.times(1)).postExchange(eq(endPoint), any(), any());
    }

    @Test
    public void testUpdateVitalsGoal() {
        Long userId = 1L;
        String endPoint = String.format(UserServiceEndpoint.UPDATE_USER_VITALS_GOAL.getEndpointUrl(), userId);
        doReturn(new ResponseEntity<>(new Response(true, null), HttpStatus.OK)).when(userService).putExchange(eq(endPoint), any(), any());

        ResponseEntity<Response<String>> response = userService.updateUserVitalsGoal(userId, new HashMap<>());

        Assert.assertNotNull(response.getBody());
        Assert.assertTrue(response.getBody().isSuccess());
        Mockito.verify(userService, Mockito.times(1)).putExchange(eq(endPoint), any(), any());
    }

    @Test
    public void testDeleteVitalsGoal() {
        Long userId = 1L;
        String endPoint = String.format(UserServiceEndpoint.DELETE_USER_VITALS_GOAL.getEndpointUrl(), userId);
        doReturn(new ResponseEntity<>(new Response(true, null), HttpStatus.OK)).when(userService).deleteExchange(eq(endPoint), any(), any(), any(), any());

        ResponseEntity<Response<String>> response = userService.deleteUserVitalsGoal(userId);

        Assert.assertNotNull(response.getBody());
        Assert.assertTrue(response.getBody().isSuccess());
        Mockito.verify(userService, Mockito.times(1)).deleteExchange(eq(endPoint), any(), any(), any(), any());
    }

    @Test
    public void testGetUserVitalsDetails() {
        String endPoint = UserServiceEndpoint.GET_USER_VITALS_DETAILS.getEndpointUrl();
        doReturn(new ResponseEntity<>(new Response(true, new HashMap<>()), HttpStatus.CREATED)).when(userService).postExchange(eq(endPoint), any(), any());

        ResponseEntity<Response<Map<String, Object>>> response = userService.getUserVitalsDetails(new HashMap<>());

        Assert.assertNotNull(response.getBody());
        Assert.assertTrue(response.getBody().isSuccess());
        Mockito.verify(userService, Mockito.times(1)).postExchange(eq(endPoint), any(), any());
    }

    @Test
    public void testSaveUserVitalsDetails() {
        Long userId = 1L;
        String endPoint = String.format(UserServiceEndpoint.ADD_USER_VITALS_DETAILS.getEndpointUrl(), userId);
        doReturn(new ResponseEntity<>(new Response(true, null), HttpStatus.CREATED)).when(userService).postExchange(eq(endPoint), any(), any());

        ResponseEntity<Response<String>> response = userService.saveUserVitalsDetails(userId, new HashMap<>());

        Assert.assertNotNull(response.getBody());
        Assert.assertTrue(response.getBody().isSuccess());
        Mockito.verify(userService, Mockito.times(1)).postExchange(eq(endPoint), any(), any());
    }
}
