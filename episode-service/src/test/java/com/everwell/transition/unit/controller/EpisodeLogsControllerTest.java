package com.everwell.transition.unit.controller;

import com.everwell.transition.BaseTest;
import com.everwell.transition.controller.EpisodeLogController;
import com.everwell.transition.exceptions.CustomExceptionHandler;
import com.everwell.transition.model.db.EpisodeLog;
import com.everwell.transition.model.request.episodelogs.*;
import com.everwell.transition.model.response.EpisodeLogResponse;
import com.everwell.transition.model.response.LogsConfigResponse;
import com.everwell.transition.service.EpisodeLogService;
import com.everwell.transition.service.impl.EpisodeTagServiceImpl;
import com.everwell.transition.utils.Utils;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

public class EpisodeLogsControllerTest extends BaseTest {

    private MockMvc mockMvc;
    
    @InjectMocks
    private EpisodeLogController episodeLogController;
    
    @Mock
    EpisodeLogService episodeLogService;

    @Mock
    EpisodeTagServiceImpl episodeTagService;

    @Before
    public void setup() {
        mockMvc = MockMvcBuilders.standaloneSetup(episodeLogController)
            .setControllerAdvice(new CustomExceptionHandler())
            .build();
    }

    @Test
    public void getLogsTest() throws Exception {
        long episodeId = 12345L;
        String uri = "/v1/logs?episodeId=" + episodeId;
        when(episodeLogService.getEpisodeLogs(episodeId)).thenReturn(getEpisodeLogs().get(0));
        mockMvc
            .perform(
                MockMvcRequestBuilders
                    .get(uri)
                    .contentType(MediaType.APPLICATION_JSON)
                    .accept(MediaType.APPLICATION_JSON_VALUE)
            )
            .andExpect(status().isOk())
            .andExpect(MockMvcResultMatchers.jsonPath("$.data.episodeId").value(episodeId))
            .andExpect(MockMvcResultMatchers.jsonPath("$.data.logs[0].category").value("Case_Closed"))
            .andExpect(MockMvcResultMatchers.jsonPath("$.data.logs[1].category").value("Case_Reopened"));
    }

    @Test
    public void getLogsTest_ZeroEpisodeId() throws Exception {
        long episodeId = 12345L;
        String uri = "/v1/logs?episodeId=0";
        when(episodeLogService.getEpisodeLogs(episodeId)).thenReturn(getEpisodeLogs().get(0));
        mockMvc
            .perform(
                MockMvcRequestBuilders
                    .get(uri)
                    .contentType(MediaType.APPLICATION_JSON)
                    .accept(MediaType.APPLICATION_JSON_VALUE)
            )
            .andExpect(status().isBadRequest())
            .andExpect(MockMvcResultMatchers.jsonPath("$.message").value("episodeId must not be null!"));
    }

    @Test
    public void getLogsBulkTest() throws Exception {
        String uri = "/v1/logs/bulk";
        List<Long> episodeIdList = Arrays.asList(
            12345L, 2L, 3L
        );
        SearchLogsRequest searchLogsRequest = new SearchLogsRequest(episodeIdList, null, null);
        when(episodeLogService.search(searchLogsRequest)).thenReturn(getEpisodeLogs());
        mockMvc
            .perform(
                MockMvcRequestBuilders
                    .post(uri)
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(Utils.asJsonString(searchLogsRequest))
                    .accept(MediaType.APPLICATION_JSON_VALUE)
            )
            .andExpect(status().isOk())
            .andExpect(MockMvcResultMatchers.jsonPath("$.data[0].logs[0].category").value("Case_Closed"))
            .andExpect(MockMvcResultMatchers.jsonPath("$.data[0].logs[1].category").value("Case_Reopened"));
    }

    @Test
    public void getLogsBulk_EntityIdListNullTest() throws Exception {
        String uri = "/v1/logs/bulk";
        SearchLogsRequest searchLogsRequest = new SearchLogsRequest(null, null, null);
        when(episodeLogService.search(searchLogsRequest)).thenReturn(getEpisodeLogs());
        mockMvc
            .perform(
                MockMvcRequestBuilders
                    .post(uri)
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(Utils.asJsonString(searchLogsRequest))
                    .accept(MediaType.APPLICATION_JSON_VALUE)
            )
            .andExpect(status().isBadRequest())
            .andExpect(MockMvcResultMatchers.jsonPath("$.message").value("Episode Id List cannot be empty"));
    }

    @Test
    public void saveLogsTest() throws Exception {
        String uri = "/v1/logs";
        List<EpisodeLogDataRequest> list = Arrays.asList(
            new EpisodeLogDataRequest(12345L,"Case_Closed",7945L,"Action Taken","Comments")
        );
        EpisodeLogRequest episodeLogRequest = new EpisodeLogRequest(list);
        when(episodeLogService.save(any())).thenReturn(getEpisodeLogs());
        mockMvc
            .perform(
                MockMvcRequestBuilders
                    .post(uri)
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(Utils.asJsonString(episodeLogRequest))
                    .accept(MediaType.APPLICATION_JSON_VALUE)
            )
            .andExpect(status().isOk())
            .andExpect(MockMvcResultMatchers.jsonPath("$.data[0].logs[0].category").value("Case_Closed"))
            .andExpect(MockMvcResultMatchers.jsonPath("$.data[0].logs[1].category").value("Case_Reopened"));
    }

    @Test
    public void saveLogsTestLogDataNull() throws Exception {
        String uri = "/v1/logs";
        List<EpisodeLogDataRequest> list = Arrays.asList(
            new EpisodeLogDataRequest(12345L,"Case_Closed",7945L,"Action Taken","Comments")
        );
        EpisodeLogRequest episodeLogRequest = new EpisodeLogRequest(null);
        when(episodeLogService.save(any())).thenReturn(getEpisodeLogs());
        mockMvc
            .perform(
                MockMvcRequestBuilders
                    .post(uri)
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(Utils.asJsonString(episodeLogRequest))
                    .accept(MediaType.APPLICATION_JSON_VALUE)
            )
            .andExpect(status().isBadRequest())
            .andExpect(MockMvcResultMatchers.jsonPath("$.message").value("Log data cannot be null!"));
    }

    @Test
    public void saveLogsTestLogEpisodeIdNull() throws Exception {
        String uri = "/v1/logs";
        List<EpisodeLogDataRequest> list = Arrays.asList(
            new EpisodeLogDataRequest(null,"Case_Closed",7945L,"Action Taken","Comments")
        );
        EpisodeLogRequest episodeLogRequest = new EpisodeLogRequest(list);
        when(episodeLogService.save(any())).thenReturn(getEpisodeLogs());
        mockMvc
            .perform(
                MockMvcRequestBuilders
                    .post(uri)
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(Utils.asJsonString(episodeLogRequest))
                    .accept(MediaType.APPLICATION_JSON_VALUE)
            )
            .andExpect(status().isBadRequest())
            .andExpect(MockMvcResultMatchers.jsonPath("$.message").value("Episode Id cannot be empty!"));
    }

    @Test
    public void saveLogsTestLogCategoryNull() throws Exception {
        String uri = "/v1/logs";
        List<EpisodeLogDataRequest> list = Arrays.asList(
            new EpisodeLogDataRequest(12345L,null,7945L,"Action Taken","Comments")
        );
        EpisodeLogRequest episodeLogRequest = new EpisodeLogRequest(list);
        when(episodeLogService.save(any())).thenReturn(getEpisodeLogs());
        mockMvc
            .perform(
                MockMvcRequestBuilders
                    .post(uri)
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(Utils.asJsonString(episodeLogRequest))
                    .accept(MediaType.APPLICATION_JSON_VALUE)
            )
            .andExpect(status().isBadRequest())
            .andExpect(MockMvcResultMatchers.jsonPath("$.message").value("Category cannot be empty!"));
    }

    @Test
    public void saveLogsTestLogAddedByNull() throws Exception {
        String uri = "/v1/logs";
        List<EpisodeLogDataRequest> list = Arrays.asList(
            new EpisodeLogDataRequest(12345L,"Case_Closed",null,"Action Taken","Comments")
        );
        EpisodeLogRequest episodeLogRequest = new EpisodeLogRequest(list);
        when(episodeLogService.save(any())).thenReturn(getEpisodeLogs());
        mockMvc
            .perform(
                MockMvcRequestBuilders
                    .post(uri)
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(Utils.asJsonString(episodeLogRequest))
                    .accept(MediaType.APPLICATION_JSON_VALUE)
            )
            .andExpect(status().isBadRequest())
            .andExpect(MockMvcResultMatchers.jsonPath("$.message").value("Added By cannot be null"));
    }

    @Test
    public void deleteLogsTest() throws Exception {
        String uri = "/v1/logs/delete";
        List<String> categories = Arrays.asList(
            "A", "B", "C"
        );
        String deleteAllAfter = "11-10-2021 18:30:00";
        EpisodeLogDeletionRequest episodeLogDeletionRequest = new EpisodeLogDeletionRequest(1L, categories, deleteAllAfter);
        when(episodeLogService.save(any())).thenReturn(getEpisodeLogs());
        mockMvc
            .perform(
                MockMvcRequestBuilders
                    .post(uri)
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(Utils.asJsonString(episodeLogDeletionRequest))
                    .accept(MediaType.APPLICATION_JSON_VALUE)
            )
            .andExpect(status().isOk())
            .andExpect(MockMvcResultMatchers.jsonPath("$.data").value("Deleted Successfully!"));
    }

    @Test
    public void deleteLogsTestEpisodeIdNull() throws Exception {
        String uri = "/v1/logs/delete";
        List<String> categories = Arrays.asList(
            "A", "B", "C"
        );
        String deleteAllAfter = "11-10-2021 18:30:00";
        EpisodeLogDeletionRequest episodeLogDeletionRequest = new EpisodeLogDeletionRequest(null, categories, deleteAllAfter);
        when(episodeLogService.save(any())).thenReturn(getEpisodeLogs());
        mockMvc
            .perform(
                MockMvcRequestBuilders
                    .post(uri)
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(Utils.asJsonString(episodeLogDeletionRequest))
                    .accept(MediaType.APPLICATION_JSON_VALUE)
            )
            .andExpect(status().isBadRequest())
            .andExpect(MockMvcResultMatchers.jsonPath("$.message").value("Episode Id cannot be null!"));
    }

    @Test
    public void deleteLogsTestCategoriesNull() throws Exception {
        String uri = "/v1/logs/delete";
        List<String> categories = Arrays.asList(
            "A", "B", "C"
        );
        String deleteAllAfter = "11-10-2021 18:30:00";
        EpisodeLogDeletionRequest episodeLogDeletionRequest = new EpisodeLogDeletionRequest(1L, null, deleteAllAfter);
        when(episodeLogService.save(any())).thenReturn(getEpisodeLogs());
        mockMvc
            .perform(
                MockMvcRequestBuilders
                    .post(uri)
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(Utils.asJsonString(episodeLogDeletionRequest))
                    .accept(MediaType.APPLICATION_JSON_VALUE)
            )
            .andExpect(status().isBadRequest())
            .andExpect(MockMvcResultMatchers.jsonPath("$.message").value("Categories cannot be empty!"));
    }

    @Test
    public void getLogsConfig() throws Exception {
        String uri = "/v1/logs/config";
        when(episodeLogService.getEpisodeLogsConfig()).thenReturn(getConfig());
        mockMvc
            .perform(
                MockMvcRequestBuilders
                    .get(uri)
                    .contentType(MediaType.APPLICATION_JSON)
                    .accept(MediaType.APPLICATION_JSON_VALUE)
            )
            .andExpect(status().isOk())
            .andExpect(MockMvcResultMatchers.jsonPath("$.data[0].category").value("Case_Closed"))
            .andExpect(MockMvcResultMatchers.jsonPath("$.data[0].categoryGroup").value("Patient Management"));
    }

    List<LogsConfigResponse> getConfig() {
        List<LogsConfigResponse> config = Arrays.asList(
            new LogsConfigResponse("Case_Closed","Patient Management")
        );
        return config;
    }

    List<EpisodeLogResponse> getEpisodeLogs() {
        long episodeId = 12345;
        List<EpisodeLog> logs = Arrays.asList(
            new EpisodeLog(1L, episodeId, LocalDateTime.now(), 7945L,"Case_Closed", null, null, null, null),
            new EpisodeLog(1L, episodeId, LocalDateTime.now(), 7946L,"Case_Reopened", null, null, null, null)
        );

        List<EpisodeLogResponse> list = new ArrayList<>();
        list.add(new EpisodeLogResponse(12345L, logs));
        return list;
    }

    @Test
    public void syncLogsTest() throws Exception {
        long episodeId = 12345L;
        String endDate = "10-10-2021 18:30:00";
        SyncLogsRequest syncLogsRequest = new SyncLogsRequest(episodeId, endDate);
        String uri = "/v1/logs/sync";
        mockMvc
            .perform(
                MockMvcRequestBuilders
                    .post(uri)
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(Utils.asJsonString(syncLogsRequest))
                    .accept(MediaType.APPLICATION_JSON_VALUE)
            )
            .andExpect(status().isOk())
            .andExpect(MockMvcResultMatchers.jsonPath("$.data").value("Logs Synced Successfully!"));
    }

    @Test
    public void syncLogsTestNullEpisodeId() throws Exception {
        String endDate = "10-10-2021 18:30:00";
        String uri = "/v1/logs/sync";
        SyncLogsRequest syncLogsRequest = new SyncLogsRequest(null, endDate);
        mockMvc
            .perform(
                MockMvcRequestBuilders
                    .post(uri)
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(Utils.asJsonString(syncLogsRequest))
                    .accept(MediaType.APPLICATION_JSON_VALUE)
            )
            .andExpect(status().isBadRequest())
            .andExpect(MockMvcResultMatchers.jsonPath("$.message").value("episodeId must not be null!"));
    }

    @Test
    public void syncLogsTestZeroEpisodeId() throws Exception {
        String endDate = "10-10-2021 18:30:00";
        String uri = "/v1/logs/sync";
        SyncLogsRequest syncLogsRequest = new SyncLogsRequest(0L, endDate);
        mockMvc
            .perform(
                MockMvcRequestBuilders
                    .post(uri)
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(Utils.asJsonString(syncLogsRequest))
                    .accept(MediaType.APPLICATION_JSON_VALUE)
            )
            .andExpect(status().isBadRequest())
            .andExpect(MockMvcResultMatchers.jsonPath("$.message").value("episodeId must not be null!"));
    }

    @Test
    public void syncLogsTestEmptyEndDate() throws Exception {
        String uri = "/v1/logs/sync";
        SyncLogsRequest syncLogsRequest = new SyncLogsRequest(1L, null);
        mockMvc
            .perform(
                MockMvcRequestBuilders
                    .post(uri)
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(Utils.asJsonString(syncLogsRequest))
                    .accept(MediaType.APPLICATION_JSON_VALUE)
            )
            .andExpect(status().isBadRequest())
            .andExpect(MockMvcResultMatchers.jsonPath("$.message").value("End Date cannot be empty!"));
    }

    @Test
    public void updateSupportActionsTest() throws Exception
    {
        String uri = "/v1/support-actions";
        UpdateSupportActionsRequest request = new UpdateSupportActionsRequest(1L, Collections.singletonList(new UpdateSupportActionsDataRequest(1L, 1L, "category", "subCategory", 1L, "action", "comments", LocalDateTime.now())));

        doNothing().when(episodeLogService).updateSupportActions(any());

        mockMvc
                .perform(
                        MockMvcRequestBuilders
                                .put(uri)
                                .contentType(MediaType.APPLICATION_JSON)
                                .content(Utils.asJsonString(request))
                                .accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.success").value("true"))
                .andExpect(MockMvcResultMatchers.jsonPath("$.data").value("Updated successfully"));
    }
}
