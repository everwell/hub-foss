package com.everwell.transition.unit.service;

import com.everwell.transition.BaseTest;
import com.everwell.transition.constants.IAMConstants;
import com.everwell.transition.exceptions.IllegalArgumentException;
import com.everwell.transition.exceptions.NotFoundException;
import com.everwell.transition.exceptions.ValidationException;
import com.everwell.transition.model.db.*;
import com.everwell.transition.model.dto.treatmentPlan.TreatmentPlanConfig;
import com.everwell.transition.model.dto.treatmentPlan.TreatmentPlanDoseTimeDto;
import com.everwell.transition.model.dto.treatmentPlan.TreatmentPlanDto;
import com.everwell.transition.model.dto.treatmentPlan.TreatmentPlanProductMapDto;
import com.everwell.transition.model.request.treatmentPlan.*;
import com.everwell.transition.model.response.AdherenceResponse;
import com.everwell.transition.model.response.AllAdherenceResponse;
import com.everwell.transition.repositories.RegimenRepository;
import com.everwell.transition.repositories.RegimenTreatmentPlanMapRepository;
import com.everwell.transition.repositories.TreatmentPlanProductMapRepository;
import com.everwell.transition.repositories.TreatmentPlanRepository;
import com.everwell.transition.service.AdherenceService;
import com.everwell.transition.service.impl.TreatmentPlanServiceImpl;
import com.everwell.transition.utils.Utils;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.Spy;

import java.text.ParseException;
import java.time.LocalDateTime;
import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.*;

public class TreatmentPlanServiceTest extends BaseTest {

    @Spy
    @InjectMocks
    private TreatmentPlanServiceImpl treatmentPlanService;

    @Mock
    private TreatmentPlanRepository treatmentPlanRepository;

    @Mock
    private RegimenTreatmentPlanMapRepository regimenTreatmentPlanMapRepository;

    @Mock
    private TreatmentPlanProductMapRepository treatmentPlanProductMapRepository;

    @Mock
    private RegimenRepository regimenRepository;

    @Mock
    private AdherenceService adherenceService;

    @Before
    public void init() {
        Map<String, Integer> map = new HashMap<>();
        map.put(IAMConstants.DAILY.toLowerCase(), 1);
        map.put(IAMConstants.MULTI_FREQUENCY.toLowerCase(), 3);
        when(adherenceService.getScheduleTypeConfig()).thenReturn(map);
    }

    @Test
    public void test_getPlanDetails_success_hasNoRegimenAndProductMappings() {
        Long id = 1L;
        Long clientId = 1L;
        when(treatmentPlanRepository.findByIdAndClientId(id, clientId)).thenReturn(getTreatmentPlan());
        when(regimenTreatmentPlanMapRepository.findAllByTreatmentPlanId(id)).thenReturn(new ArrayList<>());
        when(treatmentPlanProductMapRepository.findAllByTreatmentPlanId(id)).thenReturn(new ArrayList<>());
        TreatmentPlanDto actualResponse = treatmentPlanService.getPlanDetails(id, clientId);
        assertEquals(getTreatmentPlan().getId(), actualResponse.getId());
        assertEquals(getTreatmentPlan().getName(), actualResponse.getName());
        assertEquals(0, actualResponse.getMappedProducts().size());
        assertEquals(0, actualResponse.getMappedToRegimens().size());
    }

    @Test(expected = NotFoundException.class)
    public void test_getPlanDetails_throws_notFoundException() {
        Long id = 1L;
        Long clientId = 1L;
        when(treatmentPlanRepository.findByIdAndClientId(id, clientId)).thenReturn(null);
        treatmentPlanService.getPlanDetails(id, clientId);
    }

    private TreatmentPlan getTreatmentPlan() {
        TreatmentPlan treatmentPlan = new TreatmentPlan(1L, 1L, "treatmentPlan1", Utils.asJsonString(new TreatmentPlanConfig()));
        return treatmentPlan;
    }

    private TreatmentPlan getTreatmentPlanWithRegimenAndProductMappings() {
        TreatmentPlanConfig config = new TreatmentPlanConfig(true, true);
        TreatmentPlan treatmentPlan = new TreatmentPlan(1L, 1L, "treatmentPlan1", Utils.asJsonString(config));
        return treatmentPlan;
    }

    @Test
    public void test_getPlanDetails_success_hasRegimenAndProductMappings() throws ParseException {
        Long id = 1L;
        Long clientId = 1L;
        AllAdherenceResponse allAdherenceResponse = new AllAdherenceResponse(
                Arrays.asList(
                        new AdherenceResponse("med_1", null, "666666", Utils.convertStringToDateNew("2019-01-01 01:00:00"), 0, 0, 0, null,null,null, 0,0, new ArrayList<>(), "NONE", null)
                )
        );
        when(treatmentPlanRepository.findByIdAndClientId(id, clientId)).thenReturn(getTreatmentPlanWithRegimenAndProductMappings());
        when(regimenTreatmentPlanMapRepository.findAllByTreatmentPlanId(id)).thenReturn(getRegimenTreatmentPlanMapList());
        when(treatmentPlanProductMapRepository.findAllByTreatmentPlanId(id)).thenReturn(getTreatmentPlanProductMapList());
        when(adherenceService.getAdherenceBulk(any(), any())).thenReturn(allAdherenceResponse);
        TreatmentPlanDto actualResponse = treatmentPlanService.getPlanDetails(id, clientId);
        assertEquals(getTreatmentPlan().getId(), actualResponse.getId());
        assertEquals(getTreatmentPlan().getName(), actualResponse.getName());
        assertEquals(1, actualResponse.getMappedProducts().size());
        assertEquals(1, actualResponse.getMappedToRegimens().size());
        Mockito.verify(adherenceService, Mockito.times(1)).getAdherenceBulk(any(), any());
    }

    private List<RegimenTreatmentPlanMap> getRegimenTreatmentPlanMapList() {
        List<RegimenTreatmentPlanMap> list = new ArrayList<>();
        list.add(new RegimenTreatmentPlanMap(1L, Utils.getCurrentDateNew(), 1L, 1L));
        return list;
    }

    private List<TreatmentPlanProductMap> getTreatmentPlanProductMapList() {
        List<TreatmentPlanProductMap> list = new ArrayList<>();
        list.add(new TreatmentPlanProductMap(1L, Utils.getCurrentDateNew(), Utils.getCurrentDateNew(), null, 1L, 1L, "1111111 1**1", LocalDateTime.now(), null, "Capsule", false));
        return list;
    }

    @Test(expected = IllegalArgumentException.class)
    public void test_createPlan_with_invalidStartDate() {
        AddTreatmentPlanRequest addTreatmentPlanRequest = new AddTreatmentPlanRequest("treatmentPlan1", null,"2022----12-18 00:00:00", Arrays.asList(new TreatmentPlanProductMapRequest(1L,"product name", "1111111 1**1", 2, "other")));
        Long clientId = 1L;
        treatmentPlanService.createPlan(addTreatmentPlanRequest, clientId);
    }

    @Test
    public void test_createPlan_success() {
        AddTreatmentPlanRequest addTreatmentPlanRequest = new AddTreatmentPlanRequest("treatmentPlan1", 1L,null, Arrays.asList(new TreatmentPlanProductMapRequest(1L, "product name", "1111111 1**1", 1, "other")));
        Long clientId = 1L;
        AtomicInteger productMapCount = new AtomicInteger();
        when(treatmentPlanProductMapRepository.saveAll(any())).thenAnswer(i -> {
            List<EpisodeAssociation> argument = i.getArgument(0);
            productMapCount.set(argument.size());
            return argument;
        });
        when(regimenRepository.findById(any())).thenReturn(Optional.of(new Regimen()));
        TreatmentPlan actualResponse = treatmentPlanService.createPlan(addTreatmentPlanRequest, clientId);
        assertEquals(1, productMapCount.get());
        assertEquals(actualResponse.getName(), addTreatmentPlanRequest.getName());
        Mockito.verify(treatmentPlanRepository, Mockito.times(2)).save(any());
        Mockito.verify(regimenTreatmentPlanMapRepository, Mockito.times(1)).save(any());
        Mockito.verify(treatmentPlanProductMapRepository, Mockito.times(1)).saveAll(any());
    }

    @Test
    public void test_createPlan_success_noRegimenAndProductMaps() {
        AddTreatmentPlanRequest addTreatmentPlanRequest = new AddTreatmentPlanRequest("treatmentPlan1", null,"2022-12-18 00:00:00", Collections.EMPTY_LIST);
        Long clientId = 1L;
        TreatmentPlan actualResponse = treatmentPlanService.createPlan(addTreatmentPlanRequest, clientId);
        assertEquals(actualResponse.getName(), addTreatmentPlanRequest.getName());
        Mockito.verify(treatmentPlanRepository, Mockito.times(2)).save(any());
        Mockito.verify(regimenTreatmentPlanMapRepository, Mockito.times(0)).save(any());
        Mockito.verify(treatmentPlanProductMapRepository, Mockito.times(0)).saveAll(any());
    }

    @Test
    public void test_createPlan_success_hasRegimenMapAndNoProductMap() {
        AddTreatmentPlanRequest addTreatmentPlanRequest = new AddTreatmentPlanRequest("treatmentPlan1", 1L, "2022-12-18 00:00:00", Collections.EMPTY_LIST);
        Long clientId = 1L;
        when(regimenRepository.findById(any())).thenReturn(Optional.of(new Regimen()));
        TreatmentPlan actualResponse = treatmentPlanService.createPlan(addTreatmentPlanRequest, clientId);
        assertEquals(actualResponse.getName(), addTreatmentPlanRequest.getName());
        Mockito.verify(treatmentPlanRepository, Mockito.times(2)).save(any());
        Mockito.verify(regimenTreatmentPlanMapRepository, Mockito.times(1)).save(any());
        Mockito.verify(treatmentPlanProductMapRepository, Mockito.times(0)).saveAll(any());
    }

    @Test
    public void test_createPlan_success_hasProductMapAndNoRegimenMap() {
        AddTreatmentPlanRequest addTreatmentPlanRequest = new AddTreatmentPlanRequest("treatmentPlan1", null, "2022-12-18 00:00:00", Arrays.asList(new TreatmentPlanProductMapRequest(1L,"product name", "1111111 1**1", 3, "other")));
        Long clientId = 1L;
        AtomicInteger productMapCount = new AtomicInteger();
        when(treatmentPlanProductMapRepository.saveAll(any())).thenAnswer(i -> {
            List<EpisodeAssociation> argument = i.getArgument(0);
            productMapCount.set(argument.size());
            return argument;
        });
        TreatmentPlan actualResponse = treatmentPlanService.createPlan(addTreatmentPlanRequest, clientId);
        assertEquals(1, productMapCount.get());
        assertEquals(actualResponse.getName(), addTreatmentPlanRequest.getName());
        Mockito.verify(treatmentPlanRepository, Mockito.times(2)).save(any());
        Mockito.verify(regimenTreatmentPlanMapRepository, Mockito.times(0)).save(any());
        Mockito.verify(treatmentPlanProductMapRepository, Mockito.times(1)).saveAll(any());
    }

    @Test(expected = NotFoundException.class)
    public void test_createPlan_throws_invalidRegimen_exception() {
        AddTreatmentPlanRequest addTreatmentPlanRequest = new AddTreatmentPlanRequest("treatmentPlan1", 1L,"2022-12-18 00:00:00", Arrays.asList(new TreatmentPlanProductMapRequest(1L,"product name", "1111111 1**1", 1, "other")));
        Long clientId = 1L;
        AtomicInteger productMapCount = new AtomicInteger();
        when(regimenRepository.findById(any())).thenReturn(Optional.empty());
        TreatmentPlan actualResponse = treatmentPlanService.createPlan(addTreatmentPlanRequest, clientId);
    }

    @Test
    public void get_treatmentPlan_aggregated_schedulestring_map() {
        List<Long> treatmentPlanIds = Arrays.asList(1L, 2L, 3L);
        when(treatmentPlanProductMapRepository.findAllByTreatmentPlanIdIn(any())).thenReturn(getTreatmentPlanProductMapList_MDR());
        Map<Long, String> expectedResult = new HashMap<>();
        expectedResult.put(1L, "111*");
        expectedResult.put(2L, "111*");
        expectedResult.put(3L, "1111");

        Map<Long,String> actualResult = treatmentPlanService.getTreatmentPlanAggregatedScheduleStringMapForTreatmentPlanIds(treatmentPlanIds, Utils.convertStringToDateNew("2023-01-02 18:30:00").toLocalDate());
        assertEquals(expectedResult.size(), actualResult.size());
        for(Map.Entry<Long, String> entry : actualResult.entrySet()) {
            Assert.assertTrue(expectedResult.containsKey(entry.getKey()));
            assertEquals(expectedResult.get(entry.getKey()), entry.getValue());
        }
    }

    @Test
    public void get_treatmentPlan_aggregated_schedulestring_map_without_date_filter() {
        List<Long> treatmentPlanIds = Arrays.asList(1L, 2L, 3L);
        when(treatmentPlanProductMapRepository.findAllByTreatmentPlanIdIn(any())).thenReturn(getTreatmentPlanProductMapList_MDR());
        Map<Long, String> expectedResult = new HashMap<>();
        expectedResult.put(1L, "1111");
        expectedResult.put(2L, "1111");
        expectedResult.put(3L, "1111");

        Map<Long,String> actualResult = treatmentPlanService.getTreatmentPlanAggregatedScheduleStringMapForTreatmentPlanIds(treatmentPlanIds, null);
        assertEquals(expectedResult.size(), actualResult.size());
        for(Map.Entry<Long, String> entry : actualResult.entrySet()) {
            Assert.assertTrue(expectedResult.containsKey(entry.getKey()));
            assertEquals(expectedResult.get(entry.getKey()), entry.getValue());
        }
    }

    @Test(expected = IllegalArgumentException.class)
    public void processAddProductMappingsForTreatmentPlanWithInvalidStartDateTest() {
        Long treatmentPlanId = 1L;
        Long clientId = 1L;
        Long episodeId = 1L;
        EditTreatmentPlanRequest editTreatmentPlanRequest = new EditTreatmentPlanRequest("2022----12-20 00:00:00", new ArrayList<>(), "2022-12-20 00:00:00");
        treatmentPlanService.processAddProductMappingsForTreatmentPlan(treatmentPlanId, episodeId, clientId, editTreatmentPlanRequest);
    }

    @Test
    public void processAddProductMappingsForTreatmentPlanWithoutProductMappingsTest() {
        Long treatmentPlanId = 1L;
        Long clientId = 1L;
        Long episodeId = 1L;
        when(treatmentPlanRepository.findByIdAndClientId(treatmentPlanId, clientId)).thenReturn(getTreatmentPlan());

        List<TreatmentPlanProductMapDto> treatmentPlanProductMapDtoList = treatmentPlanService.processAddProductMappingsForTreatmentPlan(treatmentPlanId, episodeId, clientId, new EditTreatmentPlanRequest());

        Assert.assertTrue(treatmentPlanProductMapDtoList.isEmpty());
        Mockito.verify(treatmentPlanRepository, Mockito.times(1)).findByIdAndClientId(treatmentPlanId, clientId);
        Mockito.verify(treatmentPlanProductMapRepository, Mockito.times(0)).findAllByTreatmentPlanId(any());
        Mockito.verify(adherenceService, Mockito.times(0)).updateIamSchedule(anyList(), any(), any(), any());
        Mockito.verify(treatmentPlanProductMapRepository, Mockito.times(0)).saveAll(any());
    }

    @Test
    public void processAddProductMappingsForTreatmentPlanWithNewDoseTimeMappingsTest() {
        Long treatmentPlanId = 1L;
        Long clientId = 1L;
        Long episodeId = 1L;
        List<TreatmentPlanProductMapRequest> productMapList = new ArrayList<>();
        productMapList.add(new TreatmentPlanProductMapRequest(1L,"product name", "1111 1111111", 2, "other"));
        productMapList.add(new TreatmentPlanProductMapRequest(2L, "product name","1111 1111111", 2, "other"));
        EditTreatmentPlanRequest editTreatmentPlanRequest = new EditTreatmentPlanRequest("2022-12-20 00:00:00", productMapList, "2022-12-20 00:00:00");
        List<TreatmentPlanProductMap> existingTreatmentPlanProductMapList = getTreatmentPlanProductMapList_MDR().subList(0, 1);
        when(treatmentPlanRepository.findByIdAndClientId(treatmentPlanId, clientId)).thenReturn(getTreatmentPlanWithRegimenAndProductMappings());
        when(treatmentPlanProductMapRepository.findAllByTreatmentPlanId(treatmentPlanId)).thenReturn(existingTreatmentPlanProductMapList);

        List<TreatmentPlanProductMapDto> treatmentPlanProductMapDtoList = treatmentPlanService.processAddProductMappingsForTreatmentPlan(treatmentPlanId, episodeId, clientId, editTreatmentPlanRequest);

        assertEquals(2L, treatmentPlanProductMapDtoList.size());
        Mockito.verify(treatmentPlanRepository, Mockito.times(1)).findByIdAndClientId(treatmentPlanId, clientId);
        Mockito.verify(treatmentPlanProductMapRepository, Mockito.times(1)).findAllByTreatmentPlanId(treatmentPlanId);
        Mockito.verify(treatmentPlanProductMapRepository, Mockito.times(1)).saveAll(any());
    }

    @Test
    public void processAddProductMappingsForTreatmentPlanWithNoUpdateInDoseTimeMappingsTest() {
        Long treatmentPlanId = 1L;
        Long clientId = 1L;
        Long episodeId = 1L;
        List<TreatmentPlanProductMapRequest> productMapList = new ArrayList<>();
        productMapList.add(new TreatmentPlanProductMapRequest(2L, "product name", "1*** 1111111", 2, "other"));
        EditTreatmentPlanRequest editTreatmentPlanRequest = new EditTreatmentPlanRequest("2022-12-20 00:00:00", productMapList, "2022-12-20 00:00:00");
        List<TreatmentPlanProductMap> existingTreatmentPlanProductMapList = getTreatmentPlanProductMapList_MDR().subList(0, 1);
        when(treatmentPlanRepository.findByIdAndClientId(treatmentPlanId, clientId)).thenReturn(getTreatmentPlanWithRegimenAndProductMappings());
        when(treatmentPlanProductMapRepository.findAllByTreatmentPlanId(treatmentPlanId)).thenReturn(existingTreatmentPlanProductMapList);

        List<TreatmentPlanProductMapDto> treatmentPlanProductMapDtoList = treatmentPlanService.processAddProductMappingsForTreatmentPlan(treatmentPlanId, episodeId, clientId, editTreatmentPlanRequest);

        assertEquals(1L, treatmentPlanProductMapDtoList.size());
        Mockito.verify(treatmentPlanRepository, Mockito.times(1)).findByIdAndClientId(any(), any());
        Mockito.verify(treatmentPlanProductMapRepository, Mockito.times(1)).findAllByTreatmentPlanId(treatmentPlanId);
        Mockito.verify(adherenceService, Mockito.times(0)).updateIamSchedule(any(), any(), any(), any());
        Mockito.verify(treatmentPlanProductMapRepository, Mockito.times(1)).saveAll(any());
    }

    @Test(expected = IllegalArgumentException.class)
    public void processDeleteProductMappingsForInvalidEndDateTest() {
        Long treatmentPlanId = 1L;
        Long clientId = 1L;
        Long episodeId = 1L;

        treatmentPlanService.processDeleteProductMappingsForTreatmentPlan(treatmentPlanId, episodeId, clientId, new DeleteProductsMappingRequest(new ArrayList<>(), "2022-----12-20 00:00:00", "2022-12-20 00:00:00"));
    }

    @Test
    public void processDeleteProductMappingsForTreatmentPlanWithoutProductMappingsTest() {
        Long treatmentPlanId = 1L;
        Long clientId = 1L;
        Long episodeId = 1L;
        when(treatmentPlanRepository.findByIdAndClientId(treatmentPlanId, clientId)).thenReturn(getTreatmentPlan());

        treatmentPlanService.processDeleteProductMappingsForTreatmentPlan(treatmentPlanId, episodeId, clientId, new DeleteProductsMappingRequest(new ArrayList<>(), "2022-12-20 00:00:00", "2022-12-20 00:00:00"));

        Mockito.verify(treatmentPlanRepository, Mockito.times(1)).findByIdAndClientId(any(), any());
        Mockito.verify(treatmentPlanProductMapRepository, Mockito.times(0)).findAllByTreatmentPlanId(any());
        Mockito.verify(adherenceService, Mockito.times(0)).updateIamSchedule(any(), any(), any(), any());
        Mockito.verify(treatmentPlanProductMapRepository, Mockito.times(0)).saveAll(any());
    }

    @Test
    public void processDeleteProductMappingsForTreatmentPlanWithNoUpdateInDoseTimeMappingsTest() {
        Long treatmentPlanId = 1L;
        Long clientId = 1L;
        Long episodeId = 1L;
        List<TreatmentPlanProductMapRequest> productMapList = new ArrayList<>();
        productMapList.add(new TreatmentPlanProductMapRequest(1L,3L,"product name", null, 2, null, "Capsule", false));
        productMapList.add(new TreatmentPlanProductMapRequest(1L,3L,"product name duplicate", null, 2, null, "Capsule", false));
        DeleteProductsMappingRequest deleteProductsMappingRequest = new DeleteProductsMappingRequest(productMapList, null, "2022-12-20 00:00:00");
        List<TreatmentPlanProductMap> existingTreatmentPlanProductMapList = new ArrayList<>();
        existingTreatmentPlanProductMapList.add(new TreatmentPlanProductMap(1L, Utils.getCurrentDateNew(), Utils.getCurrentDateNew(), null, 1L, 1L, "111* 1111111", LocalDateTime.now(), null, "Capsule", false));
        existingTreatmentPlanProductMapList.add(new TreatmentPlanProductMap(2L, Utils.getCurrentDateNew(), Utils.getCurrentDateNew(),null, 1L, 2L, "111* 1111111", LocalDateTime.now(), null, "Capsule", false));
        existingTreatmentPlanProductMapList.add(new TreatmentPlanProductMap(3L, Utils.getCurrentDateNew(), Utils.getCurrentDateNew(),null, 1L, 3L, "11** 1111111", LocalDateTime.now(), null, "Capsule", false));

        when(treatmentPlanRepository.findByIdAndClientId(treatmentPlanId, clientId)).thenReturn(getTreatmentPlanWithRegimenAndProductMappings());
        when(treatmentPlanProductMapRepository.findAllByTreatmentPlanId(treatmentPlanId)).thenReturn(existingTreatmentPlanProductMapList);
        doNothing().when(adherenceService).updateIamSchedule(any(), any(), any(), any());

        List<TreatmentPlanProductMapDto> treatmentPlanProductMapDtoList = treatmentPlanService.processDeleteProductMappingsForTreatmentPlan(treatmentPlanId, episodeId, clientId, deleteProductsMappingRequest);

        assertEquals(1L, treatmentPlanProductMapDtoList.size());
        Mockito.verify(treatmentPlanRepository, Mockito.times(1)).findByIdAndClientId(treatmentPlanId, clientId);
        Mockito.verify(treatmentPlanProductMapRepository, Mockito.times(1)).findAllByTreatmentPlanId(treatmentPlanId);
        Mockito.verify(adherenceService, Mockito.times(0)).updateIamSchedule(any(), any(), any(), any());
        Mockito.verify(treatmentPlanProductMapRepository, Mockito.times(1)).saveAll(any());
    }

    @Test
    public void processDeleteProductMappingsForTreatmentPlanTest() {
        Long treatmentPlanId = 1L;
        Long clientId = 1L;
        Long episodeId = 1L;
        List<TreatmentPlanProductMapRequest> productMapList = new ArrayList<>();
        productMapList.add(new TreatmentPlanProductMapRequest(1L,1L,"product name one", null, 2, null, "Capsule", false));
        productMapList.add(new TreatmentPlanProductMapRequest(2L,2L,"product name two", null, 2, null, "Capsule", false));
        DeleteProductsMappingRequest deleteProductsMappingRequest = new DeleteProductsMappingRequest(productMapList, "2022-12-20 00:00:00", "2022-12-20 00:00:00");
        List<TreatmentPlanProductMap> existingTreatmentPlanProductMapList = getTreatmentPlanProductMapList_MDR()
                .stream()
                .filter (i -> i.getTreatmentPlanId() == 1)
                .collect(Collectors.toList());
        when(treatmentPlanRepository.findByIdAndClientId(treatmentPlanId, clientId)).thenReturn(getTreatmentPlanWithRegimenAndProductMappings());
        when(treatmentPlanProductMapRepository.findAllByTreatmentPlanId(treatmentPlanId)).thenReturn(existingTreatmentPlanProductMapList);
//        doNothing().when(adherenceService).updateIamSchedule(anyList(), eq(deleteProductsMappingRequest.getEndDate()), eq(episodeId), eq(clientId));

        List<TreatmentPlanProductMapDto> treatmentPlanProductMapDtoList = treatmentPlanService.processDeleteProductMappingsForTreatmentPlan(treatmentPlanId, episodeId, clientId, deleteProductsMappingRequest);

        assertEquals(2L, treatmentPlanProductMapDtoList.size());
        Mockito.verify(treatmentPlanRepository, Mockito.times(1)).findByIdAndClientId(treatmentPlanId, clientId);
        Mockito.verify(treatmentPlanProductMapRepository, Mockito.times(1)).findAllByTreatmentPlanId(treatmentPlanId);
//        Mockito.verify(adherenceService, Mockito.times(1)).updateIamSchedule(anyList(), eq(deleteProductsMappingRequest.getEndDate()), eq(episodeId), eq(clientId));
        Mockito.verify(treatmentPlanProductMapRepository, Mockito.times(1)).saveAll(any());
    }

    @Test
    public void processDeleteProductMappingsForTreatmentPlanWithOtherDrugTest() {
        Long treatmentPlanId = 1L;
        Long clientId = 1L;
        Long episodeId = 1L;
        List<TreatmentPlanProductMapRequest> productMapList = new ArrayList<>();
        productMapList.add(new TreatmentPlanProductMapRequest(1L,1L,"other", null, 2, "product name one", "Capsule", false));
        DeleteProductsMappingRequest deleteProductsMappingRequest = new DeleteProductsMappingRequest(productMapList, "2022-12-20 00:00:00", "2022-12-20 00:00:00");
        List<TreatmentPlanProductMap> existingTreatmentPlanProductMapList = new ArrayList<>();
        existingTreatmentPlanProductMapList.add(new TreatmentPlanProductMap(1L, Utils.convertStringToDateNew( "2022-12-20 18:30:00"), Utils.convertStringToDateNew( "2022-12-20 18:30:00"), null, 1L, 1L, "**1* 1111111", LocalDateTime.now(), "product name one", "Capsule", false));
        existingTreatmentPlanProductMapList.add(new TreatmentPlanProductMap(2L, Utils.convertStringToDateNew( "2022-12-20 18:30:00"), Utils.convertStringToDateNew( "2022-12-20 18:30:00"), null, 1L, 1L, "1*** 1111111", LocalDateTime.now(), "product name two", "Capsule", false));
        when(treatmentPlanRepository.findByIdAndClientId(treatmentPlanId, clientId)).thenReturn(getTreatmentPlanWithRegimenAndProductMappings());
        when(treatmentPlanProductMapRepository.findAllByTreatmentPlanId(treatmentPlanId)).thenReturn(existingTreatmentPlanProductMapList);
//        doNothing().when(adherenceService).updateIamSchedule(anyList(), eq(deleteProductsMappingRequest.getEndDate()), eq(episodeId), eq(clientId));

        List<TreatmentPlanProductMapDto> treatmentPlanProductMapDtoList = treatmentPlanService.processDeleteProductMappingsForTreatmentPlan(treatmentPlanId, episodeId, clientId, deleteProductsMappingRequest);

        assertEquals(1, treatmentPlanProductMapDtoList.size());
        Mockito.verify(treatmentPlanRepository, Mockito.times(1)).findByIdAndClientId(treatmentPlanId, clientId);
        Mockito.verify(treatmentPlanProductMapRepository, Mockito.times(1)).findAllByTreatmentPlanId(treatmentPlanId);
//        Mockito.verify(adherenceService, Mockito.times(1)).updateIamSchedule(anyList(), eq(deleteProductsMappingRequest.getEndDate()), eq(episodeId), eq(clientId));
        Mockito.verify(treatmentPlanProductMapRepository, Mockito.times(1)).saveAll(any());
    }

    @Test(expected = ValidationException.class)
    public void processDeleteProductMappingsForTreatmentPlanWithInvalidProductIdTest() {
        Long treatmentPlanId = 1L;
        Long clientId = 1L;
        Long episodeId = 1L;
        List<TreatmentPlanProductMapRequest> productMapList = new ArrayList<>();
        productMapList.add(new TreatmentPlanProductMapRequest(4L,3L,"product name", null, 2, null, "Capsule", false));
        productMapList.add(new TreatmentPlanProductMapRequest(4L,3L,"product name duplicate", null, 2, null, "Capsule", false));
        DeleteProductsMappingRequest deleteProductsMappingRequest = new DeleteProductsMappingRequest(productMapList, "2022-12-20 00:00:00", "2022-12-20 00:00:00");
        List<TreatmentPlanProductMap> existingTreatmentPlanProductMapList = getTreatmentPlanProductMapList_MDR()
                .stream()
                .filter (i -> i.getTreatmentPlanId() == 1)
                .collect(Collectors.toList());
        when(treatmentPlanRepository.findByIdAndClientId(treatmentPlanId, clientId)).thenReturn(getTreatmentPlanWithRegimenAndProductMappings());
        when(treatmentPlanProductMapRepository.findAllByTreatmentPlanId(treatmentPlanId)).thenReturn(existingTreatmentPlanProductMapList);

        treatmentPlanService.processDeleteProductMappingsForTreatmentPlan(treatmentPlanId, episodeId, clientId, deleteProductsMappingRequest);
    }

    @Test
    public void getDoseTimesForTreatmentPlanTest() {
        Long treatmentPlanId = 1L;
        Map<Long, String> treatmentPlanIdScheduleMap = new HashMap<>();
        treatmentPlanIdScheduleMap.put(1L, "1*11 1111111");
        doReturn(treatmentPlanIdScheduleMap).when(treatmentPlanService).getTreatmentPlanAggregatedScheduleStringMapForTreatmentPlanIds(any(), any());

        List<String> doseTimeList = treatmentPlanService.getDoseTimesForTreatmentPlan(treatmentPlanId);

        assertEquals(3L, doseTimeList.size());
        assertEquals(Arrays.asList("03:30:00", "11:30:00", "15:30:00"), doseTimeList);
        Mockito.verify(treatmentPlanService, Mockito.times(1)).getTreatmentPlanAggregatedScheduleStringMapForTreatmentPlanIds(any(), any());
    }

    @Test
    public void getDoseTimesForTreatmentPlanWithNullScheduleStringTest() {
        Long treatmentPlanId = 1L;
        Map<Long, String> treatmentPlanIdScheduleMap = new HashMap<>();
        treatmentPlanIdScheduleMap.put(1L, null);
        doReturn(treatmentPlanIdScheduleMap).when(treatmentPlanService).getTreatmentPlanAggregatedScheduleStringMapForTreatmentPlanIds(any(), any());

        List<String> doseTimeList = treatmentPlanService.getDoseTimesForTreatmentPlan(treatmentPlanId);

        Assert.assertTrue(doseTimeList.isEmpty());
        Mockito.verify(treatmentPlanService, Mockito.times(1)).getTreatmentPlanAggregatedScheduleStringMapForTreatmentPlanIds(any(), any());
    }

    @Test
    public void validateMedicine() {
        List<TreatmentPlanProductMap> allTreatmentPlans = new ArrayList<>();
        allTreatmentPlans.add(new TreatmentPlanProductMap(1L, 2L, "111 1111", LocalDateTime.now(), LocalDateTime.now(), LocalDateTime.now().plusDays(1), null, "Capsule", false));
        allTreatmentPlans.get(0).setId(1L);

        when (treatmentPlanProductMapRepository.findAllById(anyList())).thenReturn(allTreatmentPlans);
        treatmentPlanService.validateMedicine(1L, Arrays.asList(1L, 2L));
    }

    @Test(expected = ValidationException.class)
    public void validateMedicine_TreatmentPlanIdMismatch() {
        List<TreatmentPlanProductMap> allTreatmentPlans = new ArrayList<>();
        allTreatmentPlans.add(new TreatmentPlanProductMap(2L, 2L, "111 1111", LocalDateTime.now(), LocalDateTime.now(), LocalDateTime.now().plusDays(1), null, "Capsule", false));
        allTreatmentPlans.get(0).setId(1L);

        when (treatmentPlanProductMapRepository.findAllById(anyList())).thenReturn(allTreatmentPlans);
        treatmentPlanService.validateMedicine(1L, Arrays.asList(1L, 2L));
    }

    private List<TreatmentPlanProductMap> getTreatmentPlanProductMapList_MDR() {
        List<TreatmentPlanProductMap> list = new ArrayList<>();
        list.add(new TreatmentPlanProductMap(1L, Utils.convertStringToDateNew( "2022-12-20 18:30:00"), Utils.convertStringToDateNew( "2022-12-20 18:30:00"), null, 1L, 1L, "1*** 1111111", LocalDateTime.now(), null, "Capsule", false));
        list.add(new TreatmentPlanProductMap(2L, Utils.convertStringToDateNew( "2022-12-20 18:30:00"), Utils.convertStringToDateNew( "2022-12-20 18:30:00"),null, 1L, 2L, "*1** 1111111", LocalDateTime.now(), null, "Capsule", false));
        list.add(new TreatmentPlanProductMap(3L, Utils.convertStringToDateNew( "2022-12-20 18:30:00"), Utils.convertStringToDateNew( "2022-12-20 18:30:00"), null, 1L, 3L, "**1* 1111111", LocalDateTime.now(), null, "Capsule", false));
        list.add(new TreatmentPlanProductMap(4L, Utils.convertStringToDateNew( "2022-12-20 18:30:00"), Utils.convertStringToDateNew( "2022-12-20 18:30:00"), Utils.convertStringToDateNew( "2022-12-20 18:30:00"), 1L, 4L, "***1 1111111", LocalDateTime.now(), null, "Capsule", false));
        list.add(new TreatmentPlanProductMap(5L, Utils.convertStringToDateNew( "2022-12-20 18:30:00"), Utils.convertStringToDateNew( "2022-12-20 18:30:00"),null,2L, 1L, "11** 1111111", LocalDateTime.now(), null, "Capsule", false));
        list.add(new TreatmentPlanProductMap(6L, Utils.convertStringToDateNew( "2022-12-20 18:30:00"), Utils.convertStringToDateNew( "2022-12-20 18:30:00"), Utils.convertStringToDateNew( "2022-12-20 18:30:00"), 2L, 2L, "***1 1111111", LocalDateTime.now(), null, "Capsule", false));
        list.add(new TreatmentPlanProductMap(7L, Utils.convertStringToDateNew( "2022-12-20 18:30:00"), Utils.convertStringToDateNew( "2022-12-20 18:30:00"), Utils.convertStringToDateNew( "2023-01-02 18:30:00"), 2L, 3L, "**1* 1111111", LocalDateTime.now(), null, "Capsule", false));
        list.add(new TreatmentPlanProductMap(8L, Utils.convertStringToDateNew( "2022-12-20 18:30:00"), Utils.convertStringToDateNew( "2022-12-20 18:30:00"), null, 2L, 4L, "**1* 1111111", LocalDateTime.now(), null, "Capsule", false));
        list.add(new TreatmentPlanProductMap(9L, Utils.convertStringToDateNew( "2022-12-20 18:30:00"), Utils.convertStringToDateNew( "2022-12-20 18:30:00"),null,3L, 1L, "11** 1111111", LocalDateTime.now(), null, "Capsule", false));
        list.add(new TreatmentPlanProductMap(10L, Utils.convertStringToDateNew( "2022-12-20 18:30:00"), Utils.convertStringToDateNew( "2022-12-20 18:30:00"),null, 3L, 2L, "*11* 1111111", LocalDateTime.now(), null, "Capsule", false));
        list.add(new TreatmentPlanProductMap(11L, Utils.convertStringToDateNew( "2022-12-20 18:30:00"), Utils.convertStringToDateNew( "2022-12-20 18:30:00"),null, 3L, 3L, "**11 1111111", LocalDateTime.now(), null, "Capsule", false));
        return list;
    }

    @Test
    public void testGetDoseTimeCountMapForTreatmentPlanProductMaps() {
        // create some TreatmentPlanProductMap instances with different schedules
        TreatmentPlanProductMap map1 = new TreatmentPlanProductMap(1L, 1L, "1001", LocalDateTime.now(), LocalDateTime.now(), LocalDateTime.now(), "", "Capsule", false);
        TreatmentPlanProductMap map2 = new TreatmentPlanProductMap(2L, 2L, "0110", LocalDateTime.now(), LocalDateTime.now(), LocalDateTime.now(), "", "Capsule", false);
        TreatmentPlanProductMap map3 = new TreatmentPlanProductMap(3L, 3L, "1111", LocalDateTime.now(), LocalDateTime.now(), LocalDateTime.now(), "", "Capsule", false);
        List<TreatmentPlanProductMap> list = new ArrayList<>(Arrays.asList(map1, map2, map3));

        // call the method under test
        TreatmentPlanDoseTimeDto dto = treatmentPlanService.getDoseTimeCountMapForTreatmentPlanProductMaps(list);

        // verify the expected doseTimeSet
        Set<String> expectedSet = new HashSet<>(Arrays.asList("03:30:00", "07:30:00", "11:30:00", "15:30:00"));
        assertEquals(expectedSet, dto.getDoseTimeSet());

        // verify the expected doseTimeToDoseCountMap
        Map<String, Integer> expectedMap = Stream.of(
                        new AbstractMap.SimpleEntry<>("03:30:00", 2),
                        new AbstractMap.SimpleEntry<>("15:30:00", 2),
                        new AbstractMap.SimpleEntry<>("07:30:00", 2),
                        new AbstractMap.SimpleEntry<>("11:30:00", 2))
                .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue));
        assertEquals(expectedMap, dto.getDoseTimeToDoseCountMap());
    }

    @Test
    public void testGetDoseTimeCountMapForTreatmentPlanProductMaps_emptyInput() {
        // call the method under test with an empty list
        List<TreatmentPlanProductMap> list = new ArrayList<>();
        TreatmentPlanDoseTimeDto dto = treatmentPlanService.getDoseTimeCountMapForTreatmentPlanProductMaps(list);

        // verify that the doseTimeSet and doseTimeToDoseCountMap are both empty
        Assert.assertTrue(dto.getDoseTimeSet().isEmpty());
        Assert.assertTrue(dto.getDoseTimeToDoseCountMap().isEmpty());
    }

    @Test
    public void testGetDoseTimeCountMapForTreatmentPlanProductMaps_nullInput() {
        // call the method under test with a null input
        List<TreatmentPlanProductMap> list = new ArrayList<>();
        TreatmentPlanDoseTimeDto dto = treatmentPlanService.getDoseTimeCountMapForTreatmentPlanProductMaps(list);

        // verify that the doseTimeSet and doseTimeToDoseCountMap are both empty
        Assert.assertTrue(dto.getDoseTimeSet().isEmpty());
        Assert.assertTrue(dto.getDoseTimeToDoseCountMap().isEmpty());
    }

    @Test
    public void testGetMedicinesForSchedule_WithValidScheduleAndTreatmentPlanId_ReturnsCorrectMeds() {
        // Arrange
        Long treatmentPlanId = 1L;
        List<String> scheduleTime = Arrays.asList("03:30:00", "07:30:00");
        TreatmentPlanProductMap map1 = new TreatmentPlanProductMap(1L, 1L, "1001", LocalDateTime.now(), LocalDateTime.now(), LocalDateTime.now(), "", "Capsule", false);
        TreatmentPlanProductMap map2 = new TreatmentPlanProductMap(2L, 2L, "0110", LocalDateTime.now(), LocalDateTime.now(), LocalDateTime.now(), "", "Capsule", false);
        TreatmentPlanProductMap map3 = new TreatmentPlanProductMap(3L, 3L, "1111", LocalDateTime.now(), LocalDateTime.now(), LocalDateTime.now(), "", "Capsule", false);
        List<TreatmentPlanProductMap> activeTreatmentPlanList = new ArrayList<>(Arrays.asList(map1, map2, map3));

        doReturn(activeTreatmentPlanList).when(treatmentPlanProductMapRepository).findAllByTreatmentPlanId(treatmentPlanId);

        // Act
        List<TreatmentPlanProductMap> result = treatmentPlanService.getMedicinesForSchedule(scheduleTime, treatmentPlanId);

        // Assert
        assertEquals(4, result.size());
    }

}
