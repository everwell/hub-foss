package com.everwell.transition.unit.controller;

import com.everwell.transition.BaseTest;
import com.everwell.transition.constants.NotificationConstants;
import com.everwell.transition.controller.NotificationController;
import com.everwell.transition.exceptions.CustomExceptionHandler;
import com.everwell.transition.model.db.Client;
import com.everwell.transition.model.dto.EpisodeDto;
import com.everwell.transition.model.response.ClientResponse;
import com.everwell.transition.model.response.Response;
import com.everwell.transition.service.ClientService;
import com.everwell.transition.service.EpisodeService;
import com.everwell.transition.service.NotificationService;
import com.everwell.transition.utils.Utils;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.springframework.http.HttpStatus;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.util.Collections;
import java.util.List;
import java.util.Map;

import static org.junit.Assert.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

public class NotificationControllerTest extends BaseTest {
    private MockMvc mockMvc;

    @Spy
    @InjectMocks
    private NotificationController notificationController;

    @Mock
    private ClientService clientService;

    @Mock
    private NotificationService notificationService;

    @Mock
    private EpisodeService episodeService;

    private static int id = 29;


    @Before
    public void setup() {
        mockMvc = MockMvcBuilders.standaloneSetup(notificationController)
                .setControllerAdvice(new CustomExceptionHandler())
                .build();
    }

    @Test
    public void sendOtpSuccess() throws Exception {
        String uri = "/v1/notification/send-otp/9876543210?source=aaro";
        String dummyString = "Otp Sent";
        when(clientService.getClientByTokenOrDefault()).thenReturn( new Client(29L,"test", "test", null ));
        doNothing().when(notificationService).sendRegistrationOtp(eq(29L), eq("9876543210"), any(), eq("aaro"));
        mockMvc.
                perform(
                        MockMvcRequestBuilders
                                .get(uri)
                                .accept(MediaType.APPLICATION_JSON_VALUE)
                )
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.data").value(dummyString));
    }

    @Test
    public void validateOtpSuccess() throws Exception {
        String uri = "/v1/notification/validate-otp?phoneNumber=9876543210&otp=9876";
        String dummyString = "OTP Validated";
        when(notificationService.validateOtp(eq("9876543210"),eq("9876"))).thenReturn(true);
        mockMvc.
                perform(
                        MockMvcRequestBuilders
                                .get(uri)
                                .accept(MediaType.APPLICATION_JSON_VALUE)
                )
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.data").value(dummyString));
    }

    @Test
    public void validateOtpFailure() throws Exception {
        String uri = "/v1/notification/validate-otp?phoneNumber=9876543210&otp=9876";
        String dummyString = "OTP INVALID";
        String dummyMessage = "Wrong OTP / OTP has expired";
        when(notificationService.validateOtp(eq("9876543210"),eq("9876"))).thenReturn(false);
        mockMvc.
                perform(
                        MockMvcRequestBuilders
                                .get(uri)
                                .accept(MediaType.APPLICATION_JSON_VALUE)
                )
                .andExpect(status().isBadRequest())
                .andExpect(MockMvcResultMatchers.jsonPath("$.data").value(dummyString))
                .andExpect(MockMvcResultMatchers.jsonPath("$.message").value(dummyMessage));
    }

    @Test
    public void testGetPersonNotifications() throws Exception {
        String uri = "/v1/notification/123";
        when(clientService.getClientByTokenOrDefault()).thenReturn( new Client(29L,"test", "test", null ));
        EpisodeDto episodeDto =  new EpisodeDto(1L, 1L, 1L, true, "DSTB", 1L, null, Utils.getCurrentDateNew(), Utils.getCurrentDateNew(), null, Utils.getCurrentDateNew(), null, null, 1L,"PRESUMPTIVE_OPEN", "Presumptive Open", "IndiaTbPublic", 1L, null, null, null, null, null);
        List<EpisodeDto> episodeDtos = Collections.singletonList(episodeDto);
        doReturn(episodeDtos).when(episodeService).getEpisodesForPersonList(any(), any(), anyBoolean());
        ResponseEntity<Response<List<Map<String, Object>>>> response = new ResponseEntity<>(new Response(true), HttpStatus.OK);
        when(notificationService.getEpisodeNotifications(any())).thenReturn(response);

        mockMvc.
                perform(
                        MockMvcRequestBuilders
                                .get(uri)
                                .accept(MediaType.APPLICATION_JSON_VALUE)
                )
                .andExpect(status().isOk());
    }
    @Test
    public void testUpdateNotificationBulk() throws Exception {
        String uri = "/v1/notification/123";
        when(clientService.getClientByTokenOrDefault()).thenReturn( new Client(29L,"test", "test", null ));
        EpisodeDto episodeDto =  new EpisodeDto(1L, 1L, 1L, true, "DSTB", 1L, null, Utils.getCurrentDateNew(), Utils.getCurrentDateNew(), null, Utils.getCurrentDateNew(), null, null, 1L,"PRESUMPTIVE_OPEN", "Presumptive Open", "IndiaTbPublic", 1L, null, null, null, null, null);
        List<EpisodeDto> episodeDtos = Collections.singletonList(episodeDto);
        doReturn(episodeDtos).when(episodeService).getEpisodesForPersonList(any(), any(), anyBoolean());
        doReturn(new ResponseEntity<>(new Response(true), HttpStatus.OK)).when(notificationService).updateNotificationBulk(any());
        mockMvc.
                perform(
                        MockMvcRequestBuilders
                                .put(uri)
                                .contentType(MediaType.APPLICATION_JSON)
                                .content(Utils.asJsonString(1L))
                                .accept(MediaType.APPLICATION_JSON_VALUE)
                )
                .andExpect(status().isOk());
    }
    @Test
    public void testGetUnreadPersonNotifications() throws Exception {
        String uri = "/v1/notification/unread/123/count/false";
        when(clientService.getClientByTokenOrDefault()).thenReturn( new Client(29L,"test", "test", null ));
        EpisodeDto episodeDto =  new EpisodeDto(1L, 1L, 1L, true, "DSTB", 1L, null, Utils.getCurrentDateNew(), Utils.getCurrentDateNew(), null, Utils.getCurrentDateNew(), null, null, 1L,"PRESUMPTIVE_OPEN", "Presumptive Open", "IndiaTbPublic", 1L, null, null, null, null, null);
        List<EpisodeDto> episodeDtos = Collections.singletonList(episodeDto);
        doReturn(episodeDtos).when(episodeService).getEpisodesForPersonList(any(), any(), anyBoolean());
        ResponseEntity<Response<List<Map<String, Object>>>> response = new ResponseEntity<>(new Response(true), HttpStatus.OK);
        when(notificationService.getEpisodeNotifications(any())).thenReturn(response);

        mockMvc.
                perform(
                        MockMvcRequestBuilders
                                .get(uri)
                                .accept(MediaType.APPLICATION_JSON_VALUE)
                )
                .andExpect(status().isOk());
    }
}