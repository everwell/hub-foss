package com.everwell.transition.unit.controller;

import com.everwell.transition.BaseTest;
import com.everwell.transition.constants.Constants;
import com.everwell.transition.constants.FieldConstants;
import com.everwell.transition.controller.TreatmentPlanController;
import com.everwell.transition.enums.treamentPlan.TreatmentPlanValidation;
import com.everwell.transition.exceptions.CustomExceptionHandler;
import com.everwell.transition.exceptions.NotFoundException;
import com.everwell.transition.model.db.Client;
import com.everwell.transition.model.db.Episode;
import com.everwell.transition.model.db.TreatmentPlan;
import com.everwell.transition.model.dto.EpisodeDto;
import com.everwell.transition.model.dto.treatmentPlan.TreatmentPlanConfig;
import com.everwell.transition.model.dto.treatmentPlan.TreatmentPlanDto;
import com.everwell.transition.model.dto.treatmentPlan.TreatmentPlanProductMapDto;
import com.everwell.transition.model.request.treatmentPlan.*;
import com.everwell.transition.service.ClientService;
import com.everwell.transition.service.EpisodeLogService;
import com.everwell.transition.model.response.ProductResponseWithScheduleDto;
import com.everwell.transition.service.DispensationService;
import com.everwell.transition.service.EpisodeService;
import com.everwell.transition.service.TreatmentPlanService;
import com.everwell.transition.utils.Utils;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.*;

import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

public class TreatmentPlanControllerTest extends BaseTest {

    private MockMvc mockMvc;

    @InjectMocks
    private TreatmentPlanController treatmentPlanController;

    @Mock
    private TreatmentPlanService treatmentPlanService;

    @Mock
    private ClientService clientService;

    @Mock
    private EpisodeService episodeService;

    @Mock
    private EpisodeLogService episodeLogService;

    @Mock
    private DispensationService dispensationService;

    @Before
    public void setup() {
        mockMvc = MockMvcBuilders.standaloneSetup(treatmentPlanController)
                .setControllerAdvice(new CustomExceptionHandler())
                .build();
    }

    @Test
    public void getTreatmentPlan_success() throws Exception {
        long treatmentPlanId = 1L;
        long clientId = 1L;
        String uri = "/v1/treatment-plan/" + treatmentPlanId;
        when(treatmentPlanService.getPlanDetails(treatmentPlanId, clientId)).thenReturn(getTreatmentPlanDetails());
        mockMvc
                .perform(
                        MockMvcRequestBuilders
                                .get(uri)
                                .header(Constants.X_CLIENT_ID, clientId)
                                .contentType(MediaType.APPLICATION_JSON)
                                .accept(MediaType.APPLICATION_JSON_VALUE)
                )
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.data.id").value(treatmentPlanId))
                .andExpect(MockMvcResultMatchers.jsonPath("$.data.name").value(getTreatmentPlanDetails().getName()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.data.mappedProducts").isEmpty())
                .andExpect(MockMvcResultMatchers.jsonPath("$.data.mappedToRegimens").isEmpty());
    }

    @Test
    public void getTreatmentPlan_throws_notFoundException() throws Exception {
        Long treatmentPlanId = 1L;
        Long clientId = 1L;
        String uri = "/v1/treatment-plan/" + treatmentPlanId;
        when(treatmentPlanService.getPlanDetails(eq(treatmentPlanId), eq(clientId) )).thenThrow(new NotFoundException("no treatment plan found with id : " + treatmentPlanId));

        mockMvc
                .perform(MockMvcRequestBuilders
                        .get(uri)
                        .header(Constants.X_CLIENT_ID, clientId)
                        .accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isNotFound());
    }

    @Test
    public void createTreatmentPlan_success() throws Exception {
        long clientId = 1L;
        String uri = "/v1/treatment-plan";
        when(treatmentPlanService.createPlan(any(), any())).thenReturn(getTreatmentPlan());
        when(clientService.getClientByTokenOrDefault()).thenReturn(getClient());

        mockMvc
                .perform(MockMvcRequestBuilders
                        .post(uri)
                        .content(Utils.asJsonString(addTreatmentPlanRequestObject()))
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.success").value("true"))
                .andExpect(MockMvcResultMatchers.jsonPath("$.data.id").value(getTreatmentPlan().getId()));
    }

    @Test
    public void createTreatmentPlan_throws_validationExceptionForName() throws Exception {
        long clientId = 1L;
        String uri = "/v1/treatment-plan";
        when(treatmentPlanService.createPlan(any(), any())).thenReturn(getTreatmentPlan());
        when(clientService.getClientByTokenOrDefault()).thenReturn(getClient());

        mockMvc
                .perform(MockMvcRequestBuilders
                        .post(uri)
                        .content(Utils.asJsonString(addTreatmentPlanRequestObjectWithNullName()))
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isBadRequest())
                .andExpect(MockMvcResultMatchers.jsonPath("$.success").value("false"))
                .andExpect(MockMvcResultMatchers.jsonPath("$.message").value("name cannot be null or empty"));
    }

    @Test
    public void createTreatmentPlan_throws_validationExceptionForProductId() throws Exception {
        long clientId = 1L;
        String uri = "/v1/treatment-plan";
        List<TreatmentPlanProductMapRequest> productMapList = new ArrayList<>();
        productMapList.add(new TreatmentPlanProductMapRequest(null, "name","1111 111111", 2, "other"));
        AddTreatmentPlanRequest addTreatmentPlanRequest =  new AddTreatmentPlanRequest("name", 1L, "2022-12-18 00:00:00", productMapList);
        when(treatmentPlanService.createPlan(any(), any())).thenReturn(getTreatmentPlan());
        when(clientService.getClientByTokenOrDefault()).thenReturn(getClient());

        mockMvc
                .perform(MockMvcRequestBuilders
                        .post(uri)
                        .content(Utils.asJsonString(addTreatmentPlanRequest))
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isBadRequest())
                .andExpect(MockMvcResultMatchers.jsonPath("$.success").value("false"))
                .andExpect(MockMvcResultMatchers.jsonPath("$.message").value(TreatmentPlanValidation.PRODUCT_ID_INVALID.getMessage()));
    }

    @Test
    public void createTreatmentPlan_throws_validationExceptionForScheduleString() throws Exception {
        long clientId = 1L;
        String uri = "/v1/treatment-plan";
        List<TreatmentPlanProductMapRequest> productMapList = new ArrayList<>();
        productMapList.add(new TreatmentPlanProductMapRequest(1L, "name","1121 111111", 2, "other"));
        AddTreatmentPlanRequest addTreatmentPlanRequest =  new AddTreatmentPlanRequest("name", 1L, "2022-12-18 00:00:00", productMapList);
        when(treatmentPlanService.createPlan(any(), any())).thenReturn(getTreatmentPlan());
        when(clientService.getClientByTokenOrDefault()).thenReturn(getClient());

        mockMvc
                .perform(MockMvcRequestBuilders
                        .post(uri)
                        .content(Utils.asJsonString(addTreatmentPlanRequest))
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isBadRequest())
                .andExpect(MockMvcResultMatchers.jsonPath("$.success").value("false"))
                .andExpect(MockMvcResultMatchers.jsonPath("$.message").value(TreatmentPlanValidation.SCHEDULE_STRING_INVALID.getMessage()));
    }

    @Test
    public void createTreatmentPlan_withEpisodeId() throws Exception {
        long clientId = 1L;
        String uri = "/v1/treatment-plan";

        List<TreatmentPlanProductMapRequest> productMapList = new ArrayList<>();
        productMapList.add(new TreatmentPlanProductMapRequest(1L, "name","1111 1111111", 2, "other"));
        AddTreatmentPlanRequest addTreatmentPlanRequest =  new AddTreatmentPlanRequest("name", 1L, "2022-12-18 00:00:00", productMapList, 1L);

        when(treatmentPlanService.createPlan(any(), any())).thenReturn(getTreatmentPlan());
        when(clientService.getClientByTokenOrDefault()).thenReturn(getClient());
        when(episodeService.processUpdateEpisode(anyLong(), any())).thenReturn(new Episode());

        mockMvc
                .perform(MockMvcRequestBuilders
                        .post(uri)
                        .content(Utils.asJsonString(addTreatmentPlanRequest))
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.success").value("true"))
                .andExpect(MockMvcResultMatchers.jsonPath("$.data.id").value(getTreatmentPlan().getId()));
    }

    @Test
    public void updateProductMappingsForTreatmentPlanTest() throws Exception {
        String uri = "/v1/episode/1/treatment-plan/1/products";
        List<TreatmentPlanProductMapRequest> productMapList = new ArrayList<>();
        productMapList.add(new TreatmentPlanProductMapRequest(1L, "product name", "1111 1111111", 2, "other"));
        EditTreatmentPlanRequest editTreatmentPlanRequest = new EditTreatmentPlanRequest("2022-12-20 00:00:00", productMapList, "2022-12-20 00:00:00");

        when(clientService.getClientByTokenOrDefault()).thenReturn(getClient());
        when(episodeService.getEpisode(1L, 1L)).thenReturn(new Episode());
        when(treatmentPlanService.processAddProductMappingsForTreatmentPlan(any(), any(), any(), any())).thenReturn(getTreatmentPlanProductMapDto());
        when(episodeLogService.save(any())).thenReturn(new ArrayList<>());

        mockMvc
                .perform(MockMvcRequestBuilders
                        .put(uri)
                        .content(Utils.asJsonString(editTreatmentPlanRequest))
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.success").value("true"))
                .andExpect(MockMvcResultMatchers.jsonPath("$.data.size()").value(getTreatmentPlanProductMapDto().size()));
    }

    @Test
    public void updateProductMappingsForTreatmentPlanWithNoNewEntriesAddedTest() throws Exception {
        String uri = "/v1/episode/1/treatment-plan/1/products";
        List<TreatmentPlanProductMapRequest> productMapList = new ArrayList<>();
        productMapList.add(new TreatmentPlanProductMapRequest(1L, "product name", "1111 1111111", 2, "other"));
        EditTreatmentPlanRequest editTreatmentPlanRequest = new EditTreatmentPlanRequest("2022-12-20 00:00:00", productMapList, "2022-12-20 00:00:00");

        when(clientService.getClientByTokenOrDefault()).thenReturn(getClient());
        when(episodeService.getEpisode(1L, 1L)).thenReturn(new Episode());
        when(treatmentPlanService.processAddProductMappingsForTreatmentPlan(any(), any(), any(), any())).thenReturn(new ArrayList<>());
        when(episodeLogService.save(any())).thenReturn(new ArrayList<>());

        mockMvc
                .perform(MockMvcRequestBuilders
                        .put(uri)
                        .content(Utils.asJsonString(editTreatmentPlanRequest))
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.success").value("true"))
                .andExpect(MockMvcResultMatchers.jsonPath("$.data.size()").value(0));
    }

    @Test
    public void updateProductMappingsForTreatmentPlanWithEmptyProductMapTest() throws Exception {
        String uri = "/v1/episode/1/treatment-plan/1/products";
        EditTreatmentPlanRequest editTreatmentPlanRequest = new EditTreatmentPlanRequest("2022-12-20 00:00:00", new ArrayList<>(), "2022-12-20 00:00:00");
        when(clientService.getClientByTokenOrDefault()).thenReturn(getClient());
        when(episodeService.getEpisode(1L, 1L)).thenReturn(new Episode());
        when(treatmentPlanService.processAddProductMappingsForTreatmentPlan(any(), any(), any(), any())).thenReturn(new ArrayList<>());
        when(episodeLogService.save(any())).thenReturn(new ArrayList<>());

        mockMvc
                .perform(MockMvcRequestBuilders
                        .put(uri)
                        .content(Utils.asJsonString(editTreatmentPlanRequest))
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isBadRequest())
                .andExpect(MockMvcResultMatchers.jsonPath("$.success").value("false"))
                .andExpect(MockMvcResultMatchers.jsonPath("$.message").value(TreatmentPlanValidation.PRODUCT_MAP_INVALID.getMessage()));
    }

    @Test
    public void updateProductMappingsForTreatmentPlanWithNullProductIdTest() throws Exception {
        String uri = "/v1/episode/1/treatment-plan/1/products";
        List<TreatmentPlanProductMapRequest> productMapList = new ArrayList<>();
        productMapList.add(new TreatmentPlanProductMapRequest(null,"product name","1111 1111111", 2, "other"));
        EditTreatmentPlanRequest editTreatmentPlanRequest = new EditTreatmentPlanRequest("2022-12-20 00:00:00", productMapList, "2022-12-20 00:00:00");

        when(clientService.getClientByTokenOrDefault()).thenReturn(getClient());
        when(episodeService.getEpisode(1L, 1L)).thenReturn(new Episode());
        when(treatmentPlanService.processAddProductMappingsForTreatmentPlan(any(), any(), any(), any())).thenReturn(new ArrayList<>());
        when(episodeLogService.save(any())).thenReturn(new ArrayList<>());

        mockMvc
                .perform(MockMvcRequestBuilders
                        .put(uri)
                        .content(Utils.asJsonString(editTreatmentPlanRequest))
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isBadRequest())
                .andExpect(MockMvcResultMatchers.jsonPath("$.success").value("false"))
                .andExpect(MockMvcResultMatchers.jsonPath("$.message").value(TreatmentPlanValidation.PRODUCT_ID_INVALID.getMessage()));
    }

    @Test
    public void updateProductMappingsForTreatmentPlanWithNullProductNameTest() throws Exception {
        String uri = "/v1/episode/1/treatment-plan/1/products";
        List<TreatmentPlanProductMapRequest> productMapList = new ArrayList<>();
        productMapList.add(new TreatmentPlanProductMapRequest(1L,null,"1111 1111111", 2, "other"));
        EditTreatmentPlanRequest editTreatmentPlanRequest = new EditTreatmentPlanRequest("2022-12-20 00:00:00", productMapList, "2022-12-20 00:00:00");

        when(clientService.getClientByTokenOrDefault()).thenReturn(getClient());
        when(episodeService.getEpisode(1L, 1L)).thenReturn(new Episode());
        when(treatmentPlanService.processAddProductMappingsForTreatmentPlan(any(), any(), any(), any())).thenReturn(new ArrayList<>());
        when(episodeLogService.save(any())).thenReturn(new ArrayList<>());

        mockMvc
                .perform(MockMvcRequestBuilders
                        .put(uri)
                        .content(Utils.asJsonString(editTreatmentPlanRequest))
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isBadRequest())
                .andExpect(MockMvcResultMatchers.jsonPath("$.success").value("false"))
                .andExpect(MockMvcResultMatchers.jsonPath("$.message").value(TreatmentPlanValidation.PRODUCT_NAME_INVALID.getMessage()));
    }

    @Test
    public void updateProductMappingsForTreatmentPlanWithNullScheduleStringTest() throws Exception {
        String uri = "/v1/episode/1/treatment-plan/1/products";
        List<TreatmentPlanProductMapRequest> productMapList = new ArrayList<>();
        productMapList.add(new TreatmentPlanProductMapRequest(1L,"product name",null, 2, "other"));
        EditTreatmentPlanRequest editTreatmentPlanRequest = new EditTreatmentPlanRequest("2022-12-20 00:00:00", productMapList, "2022-12-20 00:00:00");

        when(clientService.getClientByTokenOrDefault()).thenReturn(getClient());
        when(episodeService.getEpisode(1L, 1L)).thenReturn(new Episode());
        when(treatmentPlanService.processAddProductMappingsForTreatmentPlan(any(), any(), any(), any())).thenReturn(new ArrayList<>());
        when(episodeLogService.save(any())).thenReturn(new ArrayList<>());

        mockMvc
                .perform(MockMvcRequestBuilders
                        .put(uri)
                        .content(Utils.asJsonString(editTreatmentPlanRequest))
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isBadRequest())
                .andExpect(MockMvcResultMatchers.jsonPath("$.success").value("false"))
                .andExpect(MockMvcResultMatchers.jsonPath("$.message").value(TreatmentPlanValidation.SCHEDULE_STRING_INVALID.getMessage()));
    }

    @Test
    public void updateProductMappingsForTreatmentPlanWithEmptyScheduleStringTest() throws Exception {
        String uri = "/v1/episode/1/treatment-plan/1/products";
        List<TreatmentPlanProductMapRequest> productMapList = new ArrayList<>();
        productMapList.add(new TreatmentPlanProductMapRequest(1L,"product name", "", 2, "other"));
        EditTreatmentPlanRequest editTreatmentPlanRequest = new EditTreatmentPlanRequest("2022-12-20 00:00:00", productMapList, "2022-12-20 00:00:00");

        when(clientService.getClientByTokenOrDefault()).thenReturn(getClient());
        when(episodeService.getEpisode(1L, 1L)).thenReturn(new Episode());
        when(treatmentPlanService.processAddProductMappingsForTreatmentPlan(any(), any(), any(), any())).thenReturn(new ArrayList<>());
        when(episodeLogService.save(any())).thenReturn(new ArrayList<>());

        mockMvc
                .perform(MockMvcRequestBuilders
                        .put(uri)
                        .content(Utils.asJsonString(editTreatmentPlanRequest))
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isBadRequest())
                .andExpect(MockMvcResultMatchers.jsonPath("$.success").value("false"))
                .andExpect(MockMvcResultMatchers.jsonPath("$.message").value(TreatmentPlanValidation.SCHEDULE_STRING_INVALID.getMessage()));
    }

    @Test
    public void updateProductMappingsForTreatmentPlanWithInvalidScheduleStringTest() throws Exception {
        String uri = "/v1/episode/1/treatment-plan/1/products";
        List<TreatmentPlanProductMapRequest> productMapList = new ArrayList<>();
        productMapList.add(new TreatmentPlanProductMapRequest(1L,"product name", "11", 2, "other"));
        EditTreatmentPlanRequest editTreatmentPlanRequest = new EditTreatmentPlanRequest("2022-12-20 00:00:00", productMapList, "2022-12-20 00:00:00");

        when(clientService.getClientByTokenOrDefault()).thenReturn(getClient());
        when(episodeService.getEpisode(1L, 1L)).thenReturn(new Episode());
        when(treatmentPlanService.processAddProductMappingsForTreatmentPlan(any(), any(), any(), any())).thenReturn(new ArrayList<>());
        when(episodeLogService.save(any())).thenReturn(new ArrayList<>());

        mockMvc
                .perform(MockMvcRequestBuilders
                        .put(uri)
                        .content(Utils.asJsonString(editTreatmentPlanRequest))
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isBadRequest())
                .andExpect(MockMvcResultMatchers.jsonPath("$.success").value("false"))
                .andExpect(MockMvcResultMatchers.jsonPath("$.message").value(TreatmentPlanValidation.SCHEDULE_STRING_INVALID.getMessage()));
    }

    @Test
    public void updateProductMappingsForTreatmentPlanWithInvalidDailyScheduleStringTest() throws Exception {
        String uri = "/v1/episode/1/treatment-plan/1/products";
        List<TreatmentPlanProductMapRequest> productMapList = new ArrayList<>();
        productMapList.add(new TreatmentPlanProductMapRequest(1L,"product name", "1 1111111", 2, "other"));
        EditTreatmentPlanRequest editTreatmentPlanRequest = new EditTreatmentPlanRequest("2022-12-20 00:00:00", productMapList, "2022-12-20 00:00:00");

        when(clientService.getClientByTokenOrDefault()).thenReturn(getClient());
        when(episodeService.getEpisode(1L, 1L)).thenReturn(new Episode());
        when(treatmentPlanService.processAddProductMappingsForTreatmentPlan(any(), any(), any(), any())).thenReturn(new ArrayList<>());
        when(episodeLogService.save(any())).thenReturn(new ArrayList<>());

        mockMvc
                .perform(MockMvcRequestBuilders
                        .put(uri)
                        .content(Utils.asJsonString(editTreatmentPlanRequest))
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isBadRequest())
                .andExpect(MockMvcResultMatchers.jsonPath("$.success").value("false"))
                .andExpect(MockMvcResultMatchers.jsonPath("$.message").value(TreatmentPlanValidation.SCHEDULE_STRING_INVALID.getMessage()));
    }

    @Test
    public void updateProductMappingsForTreatmentPlanWithInvalidWeeklyScheduleStringTest() throws Exception {
        String uri = "/v1/episode/1/treatment-plan/1/products";
        List<TreatmentPlanProductMapRequest> productMapList = new ArrayList<>();
        productMapList.add(new TreatmentPlanProductMapRequest(1L,"product name", "1111 111", 2, "other"));
        EditTreatmentPlanRequest editTreatmentPlanRequest = new EditTreatmentPlanRequest("2022-12-20 00:00:00", productMapList, "2022-12-20 00:00:00");

        when(clientService.getClientByTokenOrDefault()).thenReturn(getClient());
        when(episodeService.getEpisode(1L, 1L)).thenReturn(new Episode());
        when(treatmentPlanService.processAddProductMappingsForTreatmentPlan(any(), any(), any(), any())).thenReturn(new ArrayList<>());
        when(episodeLogService.save(any())).thenReturn(new ArrayList<>());

        mockMvc
                .perform(MockMvcRequestBuilders
                        .put(uri)
                        .content(Utils.asJsonString(editTreatmentPlanRequest))
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isBadRequest())
                .andExpect(MockMvcResultMatchers.jsonPath("$.success").value("false"))
                .andExpect(MockMvcResultMatchers.jsonPath("$.message").value(TreatmentPlanValidation.SCHEDULE_STRING_INVALID.getMessage()));
    }

    @Test
    public void updateProductMappingsForTreatmentPlanWithInvalidCharactersScheduleStringTest() throws Exception {
        String uri = "/v1/episode/1/treatment-plan/1/products";
        List<TreatmentPlanProductMapRequest> productMapList = new ArrayList<>();
        productMapList.add(new TreatmentPlanProductMapRequest(1L,"product name", "1121 1111111", 2, "other"));
        EditTreatmentPlanRequest editTreatmentPlanRequest = new EditTreatmentPlanRequest("2022-12-20 00:00:00", productMapList, "2022-12-20 00:00:00");

        when(clientService.getClientByTokenOrDefault()).thenReturn(getClient());
        when(episodeService.getEpisode(1L, 1L)).thenReturn(new Episode());
        when(treatmentPlanService.processAddProductMappingsForTreatmentPlan(any(), any(), any(), any())).thenReturn(new ArrayList<>());
        when(episodeLogService.save(any())).thenReturn(new ArrayList<>());

        mockMvc
                .perform(MockMvcRequestBuilders
                        .put(uri)
                        .content(Utils.asJsonString(editTreatmentPlanRequest))
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isBadRequest())
                .andExpect(MockMvcResultMatchers.jsonPath("$.success").value("false"))
                .andExpect(MockMvcResultMatchers.jsonPath("$.message").value(TreatmentPlanValidation.SCHEDULE_STRING_INVALID.getMessage()));
    }

    @Test
    public void deleteProductMappingsForTreatmentPlanTest() throws Exception {
        String uri = "/v1/episode/1/treatment-plan/1/products";
        List<TreatmentPlanProductMapRequest> productMapList = new ArrayList<>();
        productMapList.add(new TreatmentPlanProductMapRequest(1L,"product name", null, 2, "other"));
        DeleteProductsMappingRequest deleteProductsMappingRequest = new DeleteProductsMappingRequest(productMapList, "2022-12-20 00:00:00", "2022-12-20 00:00:00");
        when(clientService.getClientByTokenOrDefault()).thenReturn(getClient());
        when(episodeService.getEpisode(1L, 1L)).thenReturn(new Episode());
        when(treatmentPlanService.processDeleteProductMappingsForTreatmentPlan(any(), any(), any(), any())).thenReturn(getTreatmentPlanProductMapWithEnDateDto());
        when(episodeLogService.save(any())).thenReturn(new ArrayList<>());

        mockMvc
                .perform(MockMvcRequestBuilders
                        .delete(uri)
                        .content(Utils.asJsonString(deleteProductsMappingRequest))
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.success").value("true"))
                .andExpect(MockMvcResultMatchers.jsonPath("$.data.size()").value(getTreatmentPlanProductMapDto().size()));
    }

    @Test
    public void deleteProductMappingsForTreatmentPlanWithNoDeletedEntiesTest() throws Exception {
        String uri = "/v1/episode/1/treatment-plan/1/products";
        List<TreatmentPlanProductMapRequest> productMapList = new ArrayList<>();
        productMapList.add(new TreatmentPlanProductMapRequest(1L,"product name", null, 2, "other"));
        DeleteProductsMappingRequest deleteProductsMappingRequest = new DeleteProductsMappingRequest(productMapList, "2022-12-20 00:00:00", "2022-12-20 00:00:00");
        when(clientService.getClientByTokenOrDefault()).thenReturn(getClient());
        when(episodeService.getEpisode(1L, 1L)).thenReturn(new Episode());
        when(treatmentPlanService.processDeleteProductMappingsForTreatmentPlan(any(), any(), any(), any())).thenReturn(new ArrayList<>());
        when(episodeLogService.save(any())).thenReturn(new ArrayList<>());

        mockMvc
                .perform(MockMvcRequestBuilders
                        .delete(uri)
                        .content(Utils.asJsonString(deleteProductsMappingRequest))
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.success").value("true"))
                .andExpect(MockMvcResultMatchers.jsonPath("$.data.size()").value(0));
    }

    @Test
    public void deleteProductMappingsForTreatmentPlanWithEmptyProductListTest() throws Exception {
        String uri = "/v1/episode/1/treatment-plan/1/products";
        DeleteProductsMappingRequest deleteProductsMappingRequest = new DeleteProductsMappingRequest(new ArrayList<>(), "2022-12-20 00:00:00", "2022-12-20 00:00:00");
        when(clientService.getClientByTokenOrDefault()).thenReturn(getClient());
        when(episodeService.getEpisode(1L, 1L)).thenReturn(new Episode());
        when(treatmentPlanService.processDeleteProductMappingsForTreatmentPlan(any(), any(), any(), any())).thenReturn(new ArrayList<>());

        mockMvc
                .perform(MockMvcRequestBuilders
                        .delete(uri)
                        .content(Utils.asJsonString(deleteProductsMappingRequest))
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isBadRequest())
                .andExpect(MockMvcResultMatchers.jsonPath("$.success").value("false"))
                .andExpect(MockMvcResultMatchers.jsonPath("$.message").value(TreatmentPlanValidation.PRODUCT_ID_LIST_INVALID.getMessage()));
    }

    @Test
    public void deleteProductMappingsForTreatmentPlanWithNullProductIdTest() throws Exception {
        String uri = "/v1/episode/1/treatment-plan/1/products";
        List<TreatmentPlanProductMapRequest> productMapList = new ArrayList<>();
        productMapList.add(new TreatmentPlanProductMapRequest(null,"product name", null, 2, "other"));
        DeleteProductsMappingRequest deleteProductsMappingRequest = new DeleteProductsMappingRequest(productMapList, "2022-12-20 00:00:00", "2022-12-20 00:00:00");
        when(clientService.getClientByTokenOrDefault()).thenReturn(getClient());
        when(episodeService.getEpisode(1L, 1L)).thenReturn(new Episode());
        when(treatmentPlanService.processDeleteProductMappingsForTreatmentPlan(any(), any(), any(), any())).thenReturn(new ArrayList<>());
        when(episodeLogService.save(any())).thenReturn(new ArrayList<>());

        mockMvc
                .perform(MockMvcRequestBuilders
                        .delete(uri)
                        .content(Utils.asJsonString(deleteProductsMappingRequest))
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isBadRequest())
                .andExpect(MockMvcResultMatchers.jsonPath("$.success").value("false"))
                .andExpect(MockMvcResultMatchers.jsonPath("$.message").value(TreatmentPlanValidation.PRODUCT_ID_INVALID.getMessage()));
    }

    @Test
    public void deleteProductMappingsForTreatmentPlanWithNullProductNameTest() throws Exception {
        String uri = "/v1/episode/1/treatment-plan/1/products";
        List<TreatmentPlanProductMapRequest> productMapList = new ArrayList<>();
        productMapList.add(new TreatmentPlanProductMapRequest(1L,null, null, 2, "other"));
        DeleteProductsMappingRequest deleteProductsMappingRequest = new DeleteProductsMappingRequest(productMapList, "2022-12-20 00:00:00", "2022-12-20 00:00:00");
        when(clientService.getClientByTokenOrDefault()).thenReturn(getClient());
        when(episodeService.getEpisode(1L, 1L)).thenReturn(new Episode());
        when(treatmentPlanService.processDeleteProductMappingsForTreatmentPlan(any(), any(), any(), any())).thenReturn(new ArrayList<>());
        when(episodeLogService.save(any())).thenReturn(new ArrayList<>());

        mockMvc
                .perform(MockMvcRequestBuilders
                        .delete(uri)
                        .content(Utils.asJsonString(deleteProductsMappingRequest))
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isBadRequest())
                .andExpect(MockMvcResultMatchers.jsonPath("$.success").value("false"))
                .andExpect(MockMvcResultMatchers.jsonPath("$.message").value(TreatmentPlanValidation.PRODUCT_NAME_INVALID.getMessage()));
    }

    @Test
    public void getTreatmentPlanProduct_throws_NotFoundForTreatmentPlanId() throws Exception {
        Long treatmentPlanId = 1L;
        Long clientId = 1L;
        String uri = "/v1/treatment-plan/" + treatmentPlanId + "/product";
        when(clientService.getClientByTokenOrDefault()).thenReturn(getClient());
        when(treatmentPlanService.getPlanDetails(eq(treatmentPlanId), eq(clientId) )).thenThrow(new NotFoundException("no treatment plan found with id : " + treatmentPlanId));
        mockMvc
                .perform(MockMvcRequestBuilders
                        .get(uri + "?treatmentPlanId=" + treatmentPlanId)
                        .accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isNotFound())
                .andExpect(MockMvcResultMatchers.jsonPath("$.success").value("false"));
    }

    @Test
    public void getTreatmentPlanProduct_success_forTreatmentPlanId() throws Exception {
        Long treatmentPlanId = 1L;
        Long clientId = 1L;
        String uri = "/v1/treatment-plan/" + treatmentPlanId + "/product";
        TreatmentPlanDto planDto = getTreatmentPlanDetailsWithMappedProducts();
        when(clientService.getClientByTokenOrDefault()).thenReturn(getClient());
        when(treatmentPlanService.getPlanDetails(eq(treatmentPlanId), eq(clientId))).thenReturn(planDto);
        when(dispensationService.getProductWithSchedulesFromTreatmentPlan(eq(planDto))).thenReturn(getFilledProductScheduleList());

        mockMvc
                .perform(MockMvcRequestBuilders
                        .get(uri + "?treatmentPlanId=" + treatmentPlanId)
                        .accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.success").value("true"))
                .andExpect(MockMvcResultMatchers.jsonPath("$.data.[0].productId").value(1L));
    }

    @Test
    public void getTreatmentPlanProduct_throws_NotFoundForEpisodeId() throws Exception {
        Long episodeId = 1L;
        Long clientId = 1L;
        String uri = "/v1/treatment-plan/product/episode/" + episodeId;
        when(clientService.getClientByTokenOrDefault()).thenReturn(getClient());
        when(episodeService.getEpisodeDetails(eq(episodeId), eq(clientId))).thenThrow(new NotFoundException("no episode found for Id : " + episodeId));
        mockMvc
                .perform(MockMvcRequestBuilders
                        .get(uri + "?episodeId=" + episodeId)
                        .accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isNotFound())
                .andExpect(MockMvcResultMatchers.jsonPath("$.success").value("false"));
    }

    @Test
    public void getTreatmentPlanProduct_throws_NotFoundForEpisodeWithoutTreatmentPlan() throws Exception {
        Long episodeId = 1L;
        Long clientId = 1L;
        String uri = "/v1/treatment-plan/product/episode/" + episodeId;
        when(clientService.getClientByTokenOrDefault()).thenReturn(getClient());
        when(episodeService.getEpisodeDetails(eq(episodeId), eq(clientId))).thenReturn(getEpisodeDtoWithoutTreatmentPlan());
        mockMvc
                .perform(MockMvcRequestBuilders
                        .get(uri + "?episodeId=" + episodeId)
                        .accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isNotFound())
                .andExpect(MockMvcResultMatchers.jsonPath("$.success").value("false"))
                .andExpect(MockMvcResultMatchers.jsonPath("$.message").value("Treatment Plan is not found"));
    }

    @Test
    public void getTreatmentPlanProduct_success_ForEpisodeWithTreatmentPlan() throws Exception {
        Long episodeId = 1L;
        Long clientId = 1L;
        Long treatmentPlanId = 12L;
        String uri = "/v1/treatment-plan/product/episode/" + episodeId;
        TreatmentPlanDto planDto = getTreatmentPlanDetailsWithMappedProducts();
        when(clientService.getClientByTokenOrDefault()).thenReturn(getClient());
        when(episodeService.getEpisodeDetails(eq(episodeId), eq(clientId))).thenReturn(getEpisodeDtoWithTreatmentPlan());
        when(treatmentPlanService.getPlanDetails(eq(treatmentPlanId), eq(clientId), anyBoolean())).thenReturn(planDto);
        when(dispensationService.getProductWithSchedulesFromTreatmentPlan(eq(planDto))).thenReturn(getFilledProductScheduleList());
        mockMvc
                .perform(MockMvcRequestBuilders
                        .get(uri + "?episodeId=" + episodeId)
                        .accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.success").value("true"))
                .andExpect(MockMvcResultMatchers.jsonPath("$.data.[0].productId").value(1L));
    }

    private TreatmentPlan getTreatmentPlan() {
        TreatmentPlan treatmentPlan = new TreatmentPlan(1L, 1L, "treatmentPlan1", Utils.asJsonString(new TreatmentPlanConfig()));
        return treatmentPlan;
    }

    private TreatmentPlanDto getTreatmentPlanDetails() {
        TreatmentPlanDto treatmentPlanDto = new TreatmentPlanDto(1L, "treatmentPlan1", new ArrayList<>(), new ArrayList<>(), null);
        return treatmentPlanDto;
    }

    private TreatmentPlanDto getTreatmentPlanDetailsWithMappedProducts() {
        TreatmentPlanDto treatmentPlanDto = new TreatmentPlanDto(1L, "treatmentPlan1", getFilledMappedProducts(), new ArrayList<>(), null);
        return treatmentPlanDto;
    }

    private List<TreatmentPlanProductMapDto> getFilledMappedProducts() {
        TreatmentPlanProductMapDto treatmentPlanProduct1MapDto = new TreatmentPlanProductMapDto(Utils.getCurrentDateNew(), Utils.getCurrentDateNew(), null, 1L, "11** 1111111", LocalDateTime.now(), null, null, null, false);
        TreatmentPlanProductMapDto treatmentPlanProduct2MapDto = new TreatmentPlanProductMapDto(Utils.getCurrentDateNew(), Utils.getCurrentDateNew(), null, 2L, "1**1 1111111", LocalDateTime.now(), null, null, null, false);
        return Arrays.asList(treatmentPlanProduct1MapDto, treatmentPlanProduct2MapDto);
    }

    private List<ProductResponseWithScheduleDto> getFilledProductScheduleList() {
        ProductResponseWithScheduleDto product1ResponseWithScheduleDto = new ProductResponseWithScheduleDto();
        product1ResponseWithScheduleDto.setProductId(1L);
        ProductResponseWithScheduleDto product2ResponseWithScheduleDto = new ProductResponseWithScheduleDto();
        product2ResponseWithScheduleDto.setProductId(2L);
        return Arrays.asList(product1ResponseWithScheduleDto, product2ResponseWithScheduleDto);
    }

    private EpisodeDto getEpisodeDtoWithoutTreatmentPlan() {
        EpisodeDto episodeDto = new EpisodeDto();
        episodeDto.setId(1L);
        return episodeDto;
    }

    private EpisodeDto getEpisodeDtoWithTreatmentPlan() {
        EpisodeDto episodeDto = new EpisodeDto();
        episodeDto.setId(1L);
        Map<String, Object> stageData = new HashMap<>();
        stageData.put(FieldConstants.ESD_TREATMENT_PLAN_ID, 12L);
        episodeDto.setStageData(stageData);
        return episodeDto;
    }

    private Client getClient() {
        Client client = new Client(1L, "Client1", null, Utils.getCurrentDate());
        return client;
    }

    private AddTreatmentPlanRequest addTreatmentPlanRequestObject() {
        AddTreatmentPlanRequest addTreatmentPlanRequest = new AddTreatmentPlanRequest("treatmentPlan1", 1L, "2022-12-18 00:00:00",  new ArrayList<>());
        return addTreatmentPlanRequest;
    }

    private AddTreatmentPlanRequest addTreatmentPlanRequestObjectWithNullName() {
        AddTreatmentPlanRequest addTreatmentPlanRequest = new AddTreatmentPlanRequest(null, 1L, "2022-12-18 00:00:00", new ArrayList<>());
        return addTreatmentPlanRequest;
    }

    private List<TreatmentPlanProductMapDto> getTreatmentPlanProductMapDto() {
        List<TreatmentPlanProductMapDto> treatmentPlanProductMapDtoList = new ArrayList<>();
        treatmentPlanProductMapDtoList.add(new TreatmentPlanProductMapDto(Utils.getCurrentDateNew(), Utils.getCurrentDateNew(), null, 1L, "1111 111111", LocalDateTime.now(), null, null, null, false));
        treatmentPlanProductMapDtoList.add(new TreatmentPlanProductMapDto(Utils.getCurrentDateNew(), Utils.getCurrentDateNew(), null, 2L, "1111 111111", LocalDateTime.now(), null, null, null, false));
        return treatmentPlanProductMapDtoList;
    }

    private List<TreatmentPlanProductMapDto> getTreatmentPlanProductMapWithEnDateDto() {
        List<TreatmentPlanProductMapDto> treatmentPlanProductMapDtoList = new ArrayList<>();
        treatmentPlanProductMapDtoList.add(new TreatmentPlanProductMapDto(Utils.getCurrentDateNew(), Utils.getCurrentDateNew(), Utils.getCurrentDateNew(), 1L, "1111 111111", LocalDateTime.now(), null, null, null, false));
        treatmentPlanProductMapDtoList.add(new TreatmentPlanProductMapDto(Utils.getCurrentDateNew(), Utils.getCurrentDateNew(), Utils.getCurrentDateNew(), 2L, "1111 111111", LocalDateTime.now(), null, null, null, false));
        return treatmentPlanProductMapDtoList;
    }

}
