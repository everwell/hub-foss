package com.everwell.transition.unit.service;

import com.everwell.transition.BaseTest;
import com.everwell.transition.constants.FieldConstants;
import com.everwell.transition.exceptions.NotFoundException;
import com.everwell.transition.model.db.*;
import com.everwell.transition.model.dto.DiseaseStageFieldMapDto;
import com.everwell.transition.model.dto.TabPermissionDto;
import com.everwell.transition.model.response.DiseaseTemplateResponse;
import com.everwell.transition.postconstruct.EpisodeStaticTableCacheConstruction;
import com.everwell.transition.repositories.*;
import com.everwell.transition.service.impl.DiseaseTemplateServiceImpl;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.Spy;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.*;
import java.util.stream.Collectors;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

public class DiseaseTemplateServiceTest extends BaseTest {

    @Mock
    private RegimenRepository regimenRepository;

    @Spy
    @InjectMocks
    private DiseaseTemplateServiceImpl diseaseTemplateService;

    @Mock
    EpisodeStaticTableCacheConstruction episodeStaticTableCacheConstruction;

    @Before
    public void init() {
        Map<Long, Set<DiseaseStageMapping>> diseaseStageMappingMap = new HashMap<>();
        for (DiseaseStageMapping diseaseStageMapping : getDiseaseStageMapping_testCase1()) {
            diseaseStageMappingMap.putIfAbsent(diseaseStageMapping.getDiseaseTemplateId(), new HashSet<>());
            diseaseStageMappingMap.get(diseaseStageMapping.getDiseaseTemplateId()).add(diseaseStageMapping);
        }
        when(episodeStaticTableCacheConstruction.getDiseaseStageMappingMap()).thenReturn(diseaseStageMappingMap);
        when(episodeStaticTableCacheConstruction.getSupportedTabList()).thenReturn(getAllSupportedTab());
        when(episodeStaticTableCacheConstruction.getTabPermissionList()).thenReturn(getTabPermissions_testCase1());
        when(episodeStaticTableCacheConstruction.getDiseaseTemplateList()).thenReturn(getDiseaseTemplateList_testCase1());
    }

    @Test
    public void testGetDefinedStageIdKeyMappingsListMap_SuccessForStageIdAndKeySize() {
        when(episodeStaticTableCacheConstruction.getDiseaseStageKeyMappingList()).thenReturn(getDiseaseStageKeyMapping_testCase1());
        when(episodeStaticTableCacheConstruction.getFieldList()).thenReturn(getFieldList_testCase1());

        Map<Long, List<DiseaseStageFieldMapDto>> map = diseaseTemplateService.getDefinedStageIdKeyMappingsListMap(diseaseId_Set(), Arrays.asList(1L, 2L, 3L));

        Long stageIdExpected = 1L;
        int countExpected = 4;
        assertTrue(map.containsKey(stageIdExpected));
        assertEquals(countExpected, map.get(stageIdExpected).size());
    }

    @Test
    public void testGetDefinedStageIdKeyMappingsListMap_SuccessForFieldsMappedToMultipleStages() {
        when(episodeStaticTableCacheConstruction.getDiseaseStageKeyMappingList()).thenReturn(getDiseaseStageKeyMapping_testCase2());
        when(episodeStaticTableCacheConstruction.getFieldList()).thenReturn(getFieldList_testCase1());

        Map<Long, List<DiseaseStageFieldMapDto>> map = diseaseTemplateService.getDefinedStageIdKeyMappingsListMap(diseaseId_Set(), Arrays.asList(1L, 2L, 3L));

        List<Long> stageIdsExpected = Arrays.asList(1L, 2L, 3L);

        Map<Long, List<Long> > stageIdToFieldIdsMap = new HashMap<>();
        stageIdToFieldIdsMap.put(1L, Arrays.asList(1L, 2L, 3L, 4L));
        stageIdToFieldIdsMap.put(2L, Arrays.asList(1L, 2L));
        stageIdToFieldIdsMap.put(3L, Arrays.asList(3L, 4L));

        for(Long l : stageIdsExpected) {
            assertTrue(map.containsKey(l));
            assertEquals(stageIdToFieldIdsMap.get(l).size(), map.get(l).size());
            for(DiseaseStageFieldMapDto dto : map.get(l)) {
                assertTrue(stageIdToFieldIdsMap.get(l).contains(dto.getFieldId()));
            }
        }
    }

    @Test
    public void testGetDefinedStageIdKeyMappingsListMap_DeletedFieldsTestCase() {
        when(episodeStaticTableCacheConstruction.getDiseaseStageKeyMappingList()).thenReturn(getDiseaseStageKeyMapping_includeDeletedFields());
        when(episodeStaticTableCacheConstruction.getFieldList()).thenReturn(getFieldList_testCase1());

        Map<Long, List<DiseaseStageFieldMapDto>> map = diseaseTemplateService.getDefinedStageIdKeyMappingsListMap(diseaseId_Set(), Arrays.asList(1L, 2L, 3L));

        Long stageIdExpected = 1L;
        int countExpected = 2;
        assertTrue(map.containsKey(stageIdExpected));
        assertEquals(countExpected, map.get(stageIdExpected).size());
    }

    @Test
    public void testGetStageIdToTabPermissionsListMap_success_equateStageToTabsSize() {
        List<Long> stageIds = Arrays.asList(1L, 2L, 3L);
        Map<String, Object> episodeStageData = new HashMap<>();
        episodeStageData.put(FieldConstants.MONITORING_METHOD, "MERM");

        Map<Long, List<TabPermissionDto>> actualMap = diseaseTemplateService.getStageToTabPermissionsListMapForStageIds(diseaseId_Set(), stageIds, episodeStageData);

        assertEquals(2, actualMap.get(1L).size());
        assertEquals(1, actualMap.get(2L).size());
        assertEquals(1, actualMap.get(3L).size());
        assertTrue(actualMap.get(1L).get(0).isAdd());
        Mockito.verify(episodeStaticTableCacheConstruction, Mockito.times(1)).getSupportedTabList();
        Mockito.verify(episodeStaticTableCacheConstruction, Mockito.times(1)).getDiseaseStageMappingMap();
        Mockito.verify(episodeStaticTableCacheConstruction, Mockito.times(1)).getTabPermissionList();
    }

    @Test
    public void testGetStageIdToTabPermissionsListMap_success_forEmptyTabRepositoryAndTabPermissionRepository() {
        List<Long> stageIds = Arrays.asList(1L, 2L, 3L);
        when(episodeStaticTableCacheConstruction.getTabPermissionList()).thenReturn(new ArrayList<>());
        Map<Long, List<TabPermissionDto>> actualMap = diseaseTemplateService.getStageToTabPermissionsListMapForStageIds(diseaseId_Set(), stageIds, new HashMap<>());

        assertEquals(0, actualMap.size());
        Mockito.verify(episodeStaticTableCacheConstruction, Mockito.times(1)).getSupportedTabList();
        Mockito.verify(episodeStaticTableCacheConstruction, Mockito.times(1)).getDiseaseStageMappingMap();
        Mockito.verify(episodeStaticTableCacheConstruction, Mockito.times(1)).getTabPermissionList();
    }

    @Test
    public void testGetDiseaseIdTemplateMap_success_equateDiseaseIdsExist() {
        List<Long> diseaseIds = Arrays.asList(1L, 2L);
        Map<Long, DiseaseTemplate> map = diseaseTemplateService.getDiseaseIdTemplateMap(diseaseIds);
        assertEquals(2, map.size());
        for(Long diseaseId : diseaseIds) {
            assertTrue(map.containsKey(diseaseId));
            assertEquals(map.get(diseaseId).getId(), diseaseId);
        }
    }

    private Set<Long> diseaseId_Set() {
        Set<Long> diseaseIds = new HashSet<>();
        diseaseIds.add(1L);
        return diseaseIds;
    }

    private List<DiseaseStageMapping> getDiseaseStageMapping_testCase1() {
        List<DiseaseStageMapping> list = new ArrayList<>();
        list.add(new DiseaseStageMapping(1L, 1L, 1L));
        list.add(new DiseaseStageMapping(2L, 1L, 2L));
        list.add(new DiseaseStageMapping(3L, 1L, 3L));
        return list;
    }

    private List<DiseaseStageKeyMapping> getDiseaseStageKeyMapping_testCase1() {
        List<DiseaseStageKeyMapping> list = new ArrayList<>();
        list.add(new DiseaseStageKeyMapping(1L, 1L, 1L, true, false, "DefaultValue1"));
        list.add(new DiseaseStageKeyMapping(2L, 1L, 2L, true, false, "DefaultValue2"));
        list.add(new DiseaseStageKeyMapping(3L, 1L, 3L, true, false, "DefaultValue3"));
        list.add(new DiseaseStageKeyMapping(4L, 1L, 4L, true, false, "DefaultValue4"));
        return list;
    }

    private List<DiseaseStageKeyMapping> getDiseaseStageKeyMapping_testCase2() {
        List<DiseaseStageKeyMapping> list = new ArrayList<>();
        list.add(new DiseaseStageKeyMapping(1L, 1L, 1L, true, false, "DefaultValue1"));
        list.add(new DiseaseStageKeyMapping(2L, 1L, 2L, true, false, "DefaultValue2"));
        list.add(new DiseaseStageKeyMapping(3L, 1L, 3L, true, false, "DefaultValue3"));
        list.add(new DiseaseStageKeyMapping(4L, 1L, 4L, true, false, "DefaultValue4"));
        list.add(new DiseaseStageKeyMapping(5L, 2L, 1L, true, false, "DefaultValue1"));
        list.add(new DiseaseStageKeyMapping(6L, 2L, 2L, true, false, "DefaultValue2"));
        list.add(new DiseaseStageKeyMapping(7L, 3L, 3L, true, false, "DefaultValue3"));
        list.add(new DiseaseStageKeyMapping(8L, 3L, 4L, true, false, "DefaultValue4"));
        return list;
    }

    private List<DiseaseStageKeyMapping> getDiseaseStageKeyMapping_includeDeletedFields() {
        List<DiseaseStageKeyMapping> list = new ArrayList<>();
        list.add(new DiseaseStageKeyMapping(1L, 1L, 1L, true, false, "DefaultValue1"));
        list.add(new DiseaseStageKeyMapping(2L, 1L, 2L, true, true, "DefaultValue2"));
        list.add(new DiseaseStageKeyMapping(3L, 1L, 3L, true, false, "DefaultValue3"));
        list.add(new DiseaseStageKeyMapping(4L, 1L, 4L, true, true, "DefaultValue4"));
        return list;
    }

    private List<Field> getFieldList_testCase1() {
        List<Field> list = new ArrayList<>();
        list.add(new Field(1L, "Field1", "Module1", "Entity1"));
        list.add(new Field(2L, "Field2", "Module2", "Entity1"));
        list.add(new Field(3L, "Field3", "Module3", "Entity1"));
        list.add(new Field(4L, "Field4", "Module4", "Entity1"));
        return list;
    }

    private List<SupportedTab> getAllSupportedTab() {
        List<SupportedTab> supportedTabs = new ArrayList<>();
        supportedTabs.add(new SupportedTab(1L, "MERM", "Tab1Desc", "MERM"));
        supportedTabs.add(new SupportedTab(2L, "VOT_REVIEW", "Tab2Desc", "VOT_REVIEW"));
        supportedTabs.add(new SupportedTab(3L, "Tab3", "Tab3Desc", "_tab3"));
        supportedTabs.add(new SupportedTab(4L, "Tab4", "Tab4Desc", "_tab4"));
        supportedTabs.add(new SupportedTab(5L, "Tab5", "Tab5Desc", "_tab5"));
        supportedTabs.add(new SupportedTab(6L, "Tab6", "Tab6Desc", "_tab6"));
        supportedTabs.add(new SupportedTab(7L, "Tab7", "Tab7Desc", "_tab7"));
        return supportedTabs;
    }

    private List<DiseaseStageMapping> getDiseaseStageMappingList_testcase1() {
        List<DiseaseStageMapping> list = new ArrayList<>();
        list.add(new DiseaseStageMapping(1L, 1L, 1L));
        list.add(new DiseaseStageMapping(2L, 1L, 2L));
        list.add(new DiseaseStageMapping(3L, 1L, 3L));
        list.add(new DiseaseStageMapping(4L, 1L, 4L));
        return list;
    }

    private List<TabPermission> getTabPermissions_testCase1() {
        List<TabPermission> permissions = new ArrayList<>();
        permissions.add(new TabPermission(1L, 1L, 1L, true, true, true, true, 1L));
        permissions.add(new TabPermission(2L, 1L, 2L, true, true, true, true, 2L));
        permissions.add(new TabPermission(3L, 1L, 3L, true, true, true, true, 3L));
        permissions.add(new TabPermission(4L, 1L, 1L, true, true, true, true, 4L));
        permissions.add(new TabPermission(5L, 1L, 1L, false, false, false, false, 5L));
        permissions.add(new TabPermission(4L, 2L, 1L, true, true, true, true, 6L));
        permissions.add(new TabPermission(5L, 2L, 2L, true, true, true, true, 7L));
        permissions.add(new TabPermission(6L, 3L, 1L, true, true, true, true, 8L));
        return permissions;
    }

    private List<DiseaseTemplate> getDiseaseTemplateList_testCase1() {
        List<DiseaseTemplate> list = new ArrayList<>();
        list.add(new DiseaseTemplate(1L, "Disease1", 29L, false, "Disease1"));
        list.add(new DiseaseTemplate(2L, "Disease1", 29L, false, "Disease1"));
        return list;
    }

    @Test(expected = Exception.class)
    public void getRegimensForDisease_throwsValidationException_testCase1() {
        Long clientId = 1L;
        diseaseTemplateService.getRegimensForDisease(1L, null, clientId);
    }

    @Test(expected = Exception.class)
    public void getRegimensForDisease_throwsValidationException_testCase2() {
        Long clientId = 1L;
        diseaseTemplateService.getRegimensForDisease(null, "dstb", clientId);
    }

    @Test
    public void getRegimensForDisease_success_testCase1() {
        when(regimenRepository.findAllByDiseaseId(any())).thenReturn(getRegimens());
        List<Regimen> regimens = diseaseTemplateService.getRegimensForDisease(1L, null, 29L);
        assertEquals(getRegimens().size(), regimens.size());
    }

    @Test
    public void getRegimensForDisease_success_testCase2() {
        when(regimenRepository.findAllByDiseaseId(any())).thenReturn(getRegimens());
        List<Regimen> regimens = diseaseTemplateService.getRegimensForDisease(null, "Disease1", 29L);
        assertEquals(getRegimens().size(), regimens.size());
    }

    @Test
    public void getAllFieldsByNameInTest() {
        diseaseTemplateService.getAllFieldsByNameIn(new ArrayList<>());
        Mockito.verify(episodeStaticTableCacheConstruction, Mockito.times(1)).getFieldList();
    }

    @Test
    public void getAllFieldsTest() {
        diseaseTemplateService.getAllFields();
        Mockito.verify(episodeStaticTableCacheConstruction, Mockito.times(1)).getFieldList();
    }

    private List<Regimen> getRegimens() {
        List<Regimen> regimens = new ArrayList<>();
        regimens.add(new Regimen(1L, "Regimen1", 180L, 1L));
        regimens.add(new Regimen(2L, "Regimen2", 170L, 1L));
        regimens.add(new Regimen(3L, "Regimen3", 160L, 1L));
        regimens.add(new Regimen(4L, "Regimen4", 150L, 1L));
        return regimens;
    }

    @Test
    public void getDiseasesForClientTest() {
        when(episodeStaticTableCacheConstruction.getDiseaseTemplateList()).thenReturn(getDiseases(29));
        DiseaseTemplateResponse diseaseTemplateResponse = diseaseTemplateService.getDiseasesForClient(29L);
        Assert.assertEquals(diseaseTemplateResponse.getDiseases().get(0).getDiseaseName(),getDiseases(29L).get(0).getDiseaseName());

        when(episodeStaticTableCacheConstruction.getDiseaseTemplateList()).thenReturn(getDiseases(9));
        diseaseTemplateResponse = diseaseTemplateService.getDiseasesForClient(9L);
        Assert.assertEquals(diseaseTemplateResponse.getDiseases().get(0).getDiseaseName(),getDiseases(9L).get(0).getDiseaseName());
    }

    List<DiseaseTemplate> getDiseases() {
        return Collections.singletonList(new DiseaseTemplate(1L, "TB", 29L, false, "TB"));
    }

    List<DiseaseTemplate> getDiseases(long clientId) {
        List<DiseaseTemplate> diseaseTemplates = new ArrayList<>();
        diseaseTemplates.add(new DiseaseTemplate(1L, "TB", 29L, false, "TB"));
        diseaseTemplates.add(new DiseaseTemplate(2L, "Diabetes", 9L, false, "Diabetes"));
        diseaseTemplates.removeIf(dt -> dt.getClientId() != clientId);
        return diseaseTemplates;
    }

    @Test
    public void getDefaultDiseaseForClientIdTest() {
        DiseaseTemplate diseaseTemplate = new DiseaseTemplate(1L,"disease", 1L, true, "disease");
        when(episodeStaticTableCacheConstruction.getDiseaseTemplateList()).thenReturn(Arrays.asList(diseaseTemplate));
        diseaseTemplateService.getDefaultDiseaseForClientId(1L);
    }

    @Test(expected = NotFoundException.class)
    public void getDefaultDiseaseForClientIdTest_Exception() {
        DiseaseTemplate diseaseTemplate = new DiseaseTemplate(1L,"disease", 1L, true, "disease");
        when(episodeStaticTableCacheConstruction.getDiseaseTemplateList()).thenReturn(Arrays.asList(diseaseTemplate));
        diseaseTemplateService.getDefaultDiseaseForClientId(29L);
    }
}
