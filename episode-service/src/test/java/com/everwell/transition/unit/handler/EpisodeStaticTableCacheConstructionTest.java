package com.everwell.transition.unit.handler;

import com.everwell.transition.BaseTest;
import com.everwell.transition.model.db.*;
import com.everwell.transition.postconstruct.EpisodeStaticTableCacheConstruction;
import com.everwell.transition.repositories.*;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.Spy;

import java.util.*;

import static org.mockito.Mockito.when;

public class EpisodeStaticTableCacheConstructionTest extends BaseTest {
    
    @InjectMocks
    @Spy
    EpisodeStaticTableCacheConstruction episodeStaticTableCacheConstruction;

    @Mock
    private DiseaseStageKeyMappingRepository diseaseStageKeyMappingRepository;

    @Mock
    private DiseaseStageMappingRepository diseaseStageMappingRepository;

    @Mock
    private TabPermissionRepository tabPermissionRepository;

    @Mock
    private SupportedTabRepository supportedTabRepository;

    @Mock
    private DiseaseTemplateRepository diseaseTemplateRepository;

    @Mock
    private FieldRepository fieldRepository;

    @Test
    public void verifyPostConstruction() {
        when (fieldRepository.findAll()).thenReturn(getFieldList_testCase1());
        when (tabPermissionRepository.findAll()).thenReturn(getTabPermissions_testCase1());
        when (supportedTabRepository.findAll()).thenReturn(getAllSupportedTab());
        when (diseaseStageKeyMappingRepository.findAll()).thenReturn(getDiseaseStageKeyMapping_testCase1());
        when (diseaseStageMappingRepository.findAll()).thenReturn(getDiseaseStageMapping_testCase1());
        when (diseaseTemplateRepository.findAll()).thenReturn(getDiseaseTemplateList_testCase1());
        episodeStaticTableCacheConstruction.init();
        Mockito.verify(fieldRepository, Mockito.times(1)).findAll();
        Mockito.verify(tabPermissionRepository, Mockito.times(1)).findAll();
        Mockito.verify(supportedTabRepository, Mockito.times(1)).findAll();
        Mockito.verify(diseaseStageKeyMappingRepository, Mockito.times(1)).findAll();
        Mockito.verify(diseaseStageMappingRepository, Mockito.times(1)).findAll();
        Mockito.verify(diseaseTemplateRepository, Mockito.times(1)).findAll();
    }

    private List<DiseaseStageMapping> getDiseaseStageMapping_testCase1() {
        List<DiseaseStageMapping> list = new ArrayList<>();
        list.add(new DiseaseStageMapping(1L, 1L, 1L));
        return list;
    }

    private List<DiseaseStageKeyMapping> getDiseaseStageKeyMapping_testCase1() {
        List<DiseaseStageKeyMapping> list = new ArrayList<>();
        list.add(new DiseaseStageKeyMapping(1L, 1L, 1L, true, false, "DefaultValue1"));
        return list;
    }

    private List<DiseaseTemplate> getDiseaseTemplateList_testCase1() {
        List<DiseaseTemplate> list = new ArrayList<>();
        list.add(new DiseaseTemplate(1L, "Disease1", 29L, false, "Disease1"));
        list.add(new DiseaseTemplate(2L, "Disease1", 29L, false, "Disease1"));
        return list;
    }

    private List<Field> getFieldList_testCase1() {
        List<Field> list = new ArrayList<>();
        list.add(new Field(1L, "Field1", "Module1", "Entity1"));
        return list;
    }

    private List<SupportedTab> getAllSupportedTab() {
        List<SupportedTab> supportedTabs = new ArrayList<>();
        supportedTabs.add(new SupportedTab(1L, "MERM", "Tab1Desc", "MERM"));
        return supportedTabs;
    }

    private List<TabPermission> getTabPermissions_testCase1() {
        List<TabPermission> permissions = new ArrayList<>();
        permissions.add(new TabPermission(1L, 1L, 1L, true, true, true, true, 1L));
        return permissions;
    }
}
