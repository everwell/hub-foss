package com.everwell.transition.unit.service;

import com.everwell.transition.BaseTest;
import com.everwell.transition.model.db.TBChampionActivity;
import com.everwell.transition.model.dto.ActivityDto;
import com.everwell.transition.model.request.AddActivityRequest;
import com.everwell.transition.model.response.TBChampionActivityResponse;
import com.everwell.transition.repositories.CourseCompletionRepository;
import com.everwell.transition.repositories.TBChampionActivityRepository;
import com.everwell.transition.service.impl.CourseServiceImpl;
import com.everwell.transition.service.impl.TBChampionActivityServiceImpl;
import com.everwell.transition.utils.Utils;
import org.junit.Test;
import org.junit.jupiter.api.Assertions;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

public class TBChampionActivityServiceTest extends BaseTest {
    @Spy
    @InjectMocks
    TBChampionActivityServiceImpl tbChampionActivityService;

    @Mock
    TBChampionActivityRepository tbChampionActivityRepository;

    @Test
    public void getActivitiesTest() {
        long episodeId = 12345L;
        String uri = "/v1/tb-champ/activity/" + episodeId;
        when(tbChampionActivityRepository.getAllByEpisodeIdOrderByCreatedOnDesc(episodeId)).thenReturn(getActivities().get(0).getActivities());
        TBChampionActivityResponse tbChampionActivityResponse = tbChampionActivityService.getActivities(episodeId);
        Assertions.assertEquals(tbChampionActivityResponse.getActivities().get(0).getFileUploaded(),getActivities().get(0).getActivities().get(0).getFileUploaded());
    }

    @Test
    public void saveActivityTest() throws Exception {
        long episodeId = 12345L;
        String uri = "/v1/tb-champ/activity";
        AddActivityRequest addActivityRequest = new AddActivityRequest(episodeId,new ActivityDto("22/03/2022",2L,3L,"",""));
        when(tbChampionActivityRepository.save(getActivities().get(0).getActivities().get(0))).thenReturn(getActivities().get(0).getActivities().get(0));
        tbChampionActivityService.saveActivity(addActivityRequest);
    }

    List<TBChampionActivityResponse> getActivities() {
        long episodeId = 12345;
        List<TBChampionActivity> activities = Arrays.asList(
                new TBChampionActivity("22/03/2022", episodeId, 3L, 4L, "", ""),
                new TBChampionActivity("22/03/2022", episodeId, 2L, 5L,"", "")
        );
        List<TBChampionActivityResponse> list = new ArrayList<>();
        list.add(new TBChampionActivityResponse(12345L, activities));
        return list;
    }
}
