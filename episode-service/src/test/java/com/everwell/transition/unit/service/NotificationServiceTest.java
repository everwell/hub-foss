package com.everwell.transition.unit.service;

import com.everwell.transition.BaseTest;
import com.everwell.transition.constants.FieldConstants;
import com.everwell.transition.constants.NotificationConstants;
import com.everwell.transition.enums.notification.NotificationEndpoint;
import com.everwell.transition.handlers.INSHandler;
import com.everwell.transition.model.db.Client;
import com.everwell.transition.model.db.Trigger;
import com.everwell.transition.model.dto.notification.*;
import com.everwell.transition.model.request.episodesearch.EpisodeSearchRequest;
import com.everwell.transition.model.dto.EpisodeDto;
import com.everwell.transition.model.dto.notification.NotificationTriggerDto;
import com.everwell.transition.model.dto.notification.PNDtoWithNotificationData;
import com.everwell.transition.model.dto.notification.PushNotificationTemplateDto;
import com.everwell.transition.model.dto.notification.UserSmsDetails;
import com.everwell.transition.model.response.EpisodeUnreadNotificationResponse;
import com.everwell.transition.model.response.Response;
import com.everwell.transition.repositories.TriggerRepository;
import com.everwell.transition.service.ClientService;
import com.everwell.transition.service.EpisodeService;
import com.everwell.transition.service.impl.NotificationServiceImpl;
import com.everwell.transition.utils.CacheUtils;
import com.everwell.transition.utils.Utils;
import org.hibernate.Cache;
import org.junit.Assert;
import org.junit.Test;
import org.junit.jupiter.api.BeforeEach;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.Spy;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.ValueOperations;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.List;
import java.time.LocalDateTime;
import java.util.*;
import java.util.concurrent.TimeUnit;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.*;

public class NotificationServiceTest extends BaseTest {

    @InjectMocks
    CacheUtils cacheUtils;

    @Spy
    @InjectMocks
    NotificationServiceImpl notificationService;
    
    @Mock
    EpisodeService episodeService;

    @Mock
    TriggerRepository triggerRepository;

    @Mock
    INSHandler insHandler;

    @Mock
    ClientService clientService;

    @Mock
    RedisTemplate<String, Object> redisTemplate;

    @Mock
    private ValueOperations<String, Object> valueOperations;

    @Test
    public void sendPushNotificationGenericTest()  {
        NotificationTriggerDto notificationTriggerDto = new NotificationTriggerDto();
        notificationTriggerDto.setFunctionName("getDoseReminderPNDtosForClientId");
        List<PushNotificationTemplateDto> pushNotificationTemplateDtoList = new ArrayList<>();
        pushNotificationTemplateDtoList.add(new PushNotificationTemplateDto("1", "1", null, null));
        Boolean sendNotificationData = false;
        PNDtoWithNotificationData pnDtoWithNotificationData = new PNDtoWithNotificationData(pushNotificationTemplateDtoList, sendNotificationData, null, true);
        Trigger trigger = new Trigger();
        Client client = new Client();
        trigger.setFunctionName("getDoseReminderPNDtosForClientId");
        Mockito.when(clientService.getClient(171L)).thenReturn(client);
        Mockito.when(triggerRepository.getTriggerByFunctionName(eq("getDoseReminderPNDtosForClientId"))).thenReturn(trigger);
        Mockito.when(episodeService.getDoseReminderPNDtosForClientId(any(), any())).thenReturn(pnDtoWithNotificationData);
        notificationService.sendPushNotificationGeneric(notificationTriggerDto, 171L, "test");
        verify(notificationService, Mockito.times(1)).sendToIns(any(), any(), any(), any());
    }

    @Test
    public void sendPushNotificationGenericTestFailure()  {
        NotificationTriggerDto notificationTriggerDto = new NotificationTriggerDto();
        notificationTriggerDto.setFunctionName("getDoseReminderPNDtosForClientId");
        List<PushNotificationTemplateDto> pushNotificationTemplateDtoList = new ArrayList<>();
        pushNotificationTemplateDtoList.add(new PushNotificationTemplateDto("1", "1", null, null));
        Boolean sendNotificationData = false;
        PNDtoWithNotificationData pnDtoWithNotificationData = new PNDtoWithNotificationData(pushNotificationTemplateDtoList, sendNotificationData, null, true);
        Trigger trigger = new Trigger();
        trigger.setFunctionName("test");
        Client client = new Client();
        Mockito.when(clientService.getClient(171L)).thenReturn(client);
        Mockito.when(triggerRepository.getTriggerByFunctionName(eq("getDoseReminderPNDtosForClientId"))).thenReturn(trigger);
        Mockito.when(episodeService.getDoseReminderPNDtosForClientId(any(), any())).thenReturn(pnDtoWithNotificationData);
        notificationService.sendPushNotificationGeneric(notificationTriggerDto, 171L, "test");
    }

    @Test
    public void sendRegistrationOtpTest(){
        when(redisTemplate.opsForValue()).thenReturn(valueOperations);
        doNothing().when(valueOperations).set(anyString(), any(), anyLong(), any());
        Trigger trigger = new Trigger();
        trigger.setFunctionName(NotificationConstants.REGISTRATION_EVENT);
        trigger.setEventName(NotificationConstants.REGISTRATION_EVENT);
        Mockito.when(triggerRepository.getTriggerByFunctionName(eq(NotificationConstants.REGISTRATION_EVENT))).thenReturn(trigger);
        notificationService.sendRegistrationOtp(29L,"9876543210","aaro","source");
        verify(notificationService, Mockito.times(1)).sendSmsToIns(any(), any(), eq(trigger.getEventName()), eq(29L));
    }

    @Test
    public void validateOtpTestSuccess(){
        when(redisTemplate.opsForValue()).thenReturn(valueOperations);
        when(valueOperations.get(any())).thenReturn("9876");
        Boolean validated = notificationService.validateOtp("9876543210","9876");
        assertEquals(validated, true);
    }

    @Test
    public void validateOtpTestFailure(){
        when(redisTemplate.opsForValue()).thenReturn(valueOperations);
        when(valueOperations.get(any())).thenReturn(null);
        Boolean validated = notificationService.validateOtp("9876543210","9876");
        assertEquals(validated, false);
    }

    @Test
    public void sendNotificationToInsTest() {
        List<PushNotificationTemplateDto> pushNotificationTemplateDto = new ArrayList<>();
        List<PushNotificationTemplateDto> pushNotificationTemplateDtoList = new ArrayList<>();
        pushNotificationTemplateDtoList.add(new PushNotificationTemplateDto("1", "1", null, null));
        Boolean sendNotificationData = false;
        PNDtoWithNotificationData pnDtoWithNotificationData = new PNDtoWithNotificationData(pushNotificationTemplateDtoList, sendNotificationData, null, true);
        NotificationTriggerDto notificationTriggerDto = new NotificationTriggerDto();
        notificationTriggerDto.setFunctionName("getDoseReminderPNDtosForClientId ");
        notificationTriggerDto.setVendorId(1L);
        doNothing().when(insHandler).sendNotificationToINS(any(), any());
        notificationService.sendToIns(pnDtoWithNotificationData, notificationTriggerDto, "test", 29L);
        verify(insHandler, times(1)).sendNotificationToINS(any(), any());
    }

    @Test
    public void testSendSmsSuccess()  {
        NotificationTriggerDto notificationTriggerDto = new NotificationTriggerDto();
        notificationTriggerDto.setFunctionName("getEpisodesForTreatmentInitiation");
        NotificationTriggerEvent notificationTriggerEvent = new NotificationTriggerEvent(notificationTriggerDto, new EpisodeSearchRequest());

        List<UserSmsDetails> userSmsDetailsList = new ArrayList<>();
        userSmsDetailsList.add(new UserSmsDetails(1L, null, "2"));
        Trigger triggerStore = new Trigger();
        triggerStore.setFunctionName("getEpisodesForTreatmentInitiation");

        when(triggerRepository.getTriggerByFunctionName(eq("getEpisodesForTreatmentInitiation"))).thenReturn(triggerStore);
        doNothing().when(insHandler).sendSmsToINS(any(), eq(29L));

        notificationService.sendSms(notificationTriggerEvent, "29", "test");
        verify(insHandler, times(1)).sendSmsToINS(any(), eq(29L));
    }

    @Test
    public void testSendSmsFailure() {
        NotificationTriggerDto notificationTriggerDto = new NotificationTriggerDto();
        notificationTriggerDto.setFunctionName("getEpisodesForTreatmentInitiation");
        NotificationTriggerEvent notificationTriggerEvent = new NotificationTriggerEvent(notificationTriggerDto, new EpisodeSearchRequest());
        List<UserSmsDetails> userSmsDetailsList = new ArrayList<>();
        userSmsDetailsList.add(new UserSmsDetails(1L, null, "2"));
        Trigger triggerStore = new Trigger();
        triggerStore.setFunctionName("test");

        when(triggerRepository.getTriggerByFunctionName(eq("getEpisodesForTreatmentInitiation"))).thenReturn(triggerStore);

        notificationService.sendSms(notificationTriggerEvent, "29", "test");
        verify(insHandler, times(0)).sendSmsToINS(any(), eq(29L));
    }

    @Test
    public void sendSmsToInsTest() {
        List<UserSmsDetails> userSmsDetails = new ArrayList<>();
        Map<String, String> userParameters = new HashMap<>();
        userParameters.put(NotificationConstants.TEMPLATE_OTP, "9876");
        userParameters.put(NotificationConstants.TEMPLATE_USER, "purposeText");
        userParameters.put(NotificationConstants.NAME_OF_APP, "source");
        userSmsDetails.add(new UserSmsDetails(0L, userParameters, "9876543210"));
        NotificationTriggerDto notificationTriggerDto = new NotificationTriggerDto();
        notificationTriggerDto.setFunctionName(NotificationConstants.REGISTRATION_EVENT);
        notificationTriggerDto.setVendorId(1L);
        doNothing().when(insHandler).sendSmsToINS(any(), any());
        notificationService.sendSmsToIns(userSmsDetails, notificationTriggerDto, "test", 29L);
        verify(insHandler, times(1)).sendSmsToINS(any(), any());
    }

    @Test
    public void sendAppointmentConfirmationSmsTest() {
        Trigger trigger = new Trigger();
        trigger.setFunctionName("test");
        trigger.setTimeZone("Asia/Kolkata");
        Map<String, Object> stageData = new HashMap<>();
        stageData.put(FieldConstants.PERSON_FIELD_FIRST_NAME, "test");
        Map<String, String> mobileListMap = new HashMap<>();
        mobileListMap.put("number", "1111111111");
        List<Map<String, String>> mobileList = new ArrayList<>();
        mobileList.add(mobileListMap);
        stageData.put(FieldConstants.PERSON_MOBILE_LIST, mobileList);
        doNothing().when(notificationService).sendSmsToIns(any(), any(), any(), any());
        EpisodeDto episodeDto = new EpisodeDto(1L, 1L, 1L, true, "DSTB", 1L, null, Utils.getCurrentDateNew(), Utils.getCurrentDateNew(), null, Utils.getCurrentDateNew(), null, null, 1L,"PRESUMPTIVE_OPEN", "Presumptive Open", "IndiaTbPublic", null, null, null, null, null, stageData);
        when(triggerRepository.getTriggerByFunctionName(any())).thenReturn(trigger);
        notificationService.sendAppointmentConfirmationSms(episodeDto, "test", LocalDateTime.now());
        verify(notificationService, times(1)).sendSmsToIns(any(), any(), any(), any());
    }


    @Test
    public void sendPushNotificationWithExtraDataTest() {
        NotificationTriggerDto notificationTriggerDto = new NotificationTriggerDto();
        notificationTriggerDto.setFunctionName("getAddUserRelationPnDtos");
        List<PushNotificationTemplateDto> pushNotificationTemplateDtoList = new ArrayList<>();
        pushNotificationTemplateDtoList.add(new PushNotificationTemplateDto("1", "1", null, null));
        Boolean sendNotificationData = false;
        PNDtoWithNotificationData pnDtoWithNotificationData = new PNDtoWithNotificationData(pushNotificationTemplateDtoList, sendNotificationData, null, true);
        Trigger trigger = new Trigger();
        trigger.setClientId(171L);
        trigger.setFunctionName("getAddUserRelationPnDtos");
        Mockito.when(triggerRepository.getTriggerByEventName(eq("addUserRelation"))).thenReturn(trigger);
        Mockito.when(episodeService.getAddUserRelationPnDtos(any(), any(), any())).thenReturn(pnDtoWithNotificationData);
        notificationService.sendPushNotificationForEpisodeWithExtraData(new EpisodeDto(), new HashMap<>(), "addUserRelation");
        verify(notificationService, Mockito.times(1)).sendToIns(any(), any(), any(), any());
    }

    @Test
    public void sendPushNotificationWithExtraDataTestFailure() {
        NotificationTriggerDto notificationTriggerDto = new NotificationTriggerDto();
        notificationTriggerDto.setFunctionName("getAddUserRelationPnDtos");
        Trigger trigger = new Trigger();
        trigger.setClientId(171L);
        trigger.setFunctionName("test");
        Mockito.when(triggerRepository.getTriggerByEventName(eq("addUserRelation"))).thenReturn(trigger);
        notificationService.sendPushNotificationForEpisodeWithExtraData(new EpisodeDto(), new HashMap<>(), "addUserRelation");
        verify(notificationService, Mockito.times(0)).sendToIns(any(), any(), any(), any());
    }

    @Test
    public void updateNotification() {
        ResponseEntity<Response<String>> response = new ResponseEntity<>(new Response<>(true, "success"), HttpStatus.OK);
        doReturn(response).when(notificationService).putExchange(eq(NotificationEndpoint.UPDATE_NOTIFICATION.getEndpointUrl()), any(), any());

        ResponseEntity<Response<String>> actualResponse = notificationService.updateNotification(1L, true, "accept");
        Assert.assertTrue(actualResponse.getBody().isSuccess());
    }

    @Test
    public void updateNotificationFailure() {
        ResponseEntity<Response<String>> response = new ResponseEntity<>(new Response<>(false, "success"), HttpStatus.BAD_REQUEST);
        doReturn(response).when(notificationService).putExchange(eq(NotificationEndpoint.UPDATE_NOTIFICATION.getEndpointUrl()), any(), any());

        ResponseEntity<Response<String>> actualResponse = notificationService.updateNotification(1L, true, "accept");
        Assert.assertFalse(actualResponse.getBody().isSuccess());
    }

    @Test
    public void getEpisodeNotification() {
        ResponseEntity<Response<List<Map<String, Object>>>> response = new ResponseEntity<>(new Response<>(true, Collections.emptyList()), HttpStatus.OK);
        doReturn(response).when(notificationService).postExchange(eq(NotificationEndpoint.GET_NOTIFICATION.getEndpointUrl()), any(), any());

        ResponseEntity<Response<List<Map<String, Object>>>> actualResponse = notificationService.getEpisodeNotifications(Collections.singletonList(1L));
        Assert.assertTrue(actualResponse.getBody().isSuccess());
    }
    @Test
    public void deleteNotification() {
        ResponseEntity<Response<String>> response = new ResponseEntity<>(new Response<>(true, "success"), HttpStatus.OK);
        doReturn(response).when(notificationService).deleteExchange(eq(NotificationEndpoint.DELETE_NOTIFICATION.getEndpointUrl()), any(), any(),any(),any());

        ResponseEntity<Response<String>> actualResponse = notificationService.deleteNotification(new HashMap<>());
        Assert.assertTrue(actualResponse.getBody().isSuccess());
    }

    @Test
    public void deleteNotificationFailure() {
        ResponseEntity<Response<String>> response = new ResponseEntity<>(new Response<>(false, "failure"), HttpStatus.BAD_REQUEST);
        doReturn(response).when(notificationService).deleteExchange(eq(NotificationEndpoint.DELETE_NOTIFICATION.getEndpointUrl()), any(), any(),any(),any());

        ResponseEntity<Response<String>> actualResponse = notificationService.deleteNotification(new HashMap<>());
        Assert.assertFalse(actualResponse.getBody().isSuccess());
    }
    @Test
    public void updateNotificationBulk() {
        ResponseEntity<Response<String>> response = new ResponseEntity<>(new Response<>(true, "success"), HttpStatus.OK);
        doReturn(response).when(notificationService).putExchange(eq(NotificationEndpoint.UPDATE_NOTIFICATION_BULK.getEndpointUrl()), any(), any());

        ResponseEntity<Response<String>> actualResponse = notificationService.updateNotificationBulk(Collections.singletonList(1L));
        Assert.assertTrue(actualResponse.getBody().isSuccess());
    }
    @Test
    public void updateNotificationBulkFailure() {
        ResponseEntity<Response<String>> response = new ResponseEntity<>(new Response<>(false, "failure"), HttpStatus.BAD_REQUEST);
        doReturn(response).when(notificationService).putExchange(eq(NotificationEndpoint.UPDATE_NOTIFICATION_BULK.getEndpointUrl()), any(), any());

        ResponseEntity<Response<String>> actualResponse = notificationService.updateNotificationBulk(Collections.singletonList(1L));
        Assert.assertFalse(actualResponse.getBody().isSuccess());
    }
    @Test
    public void getUnreadEpisodeNotification() {
        ResponseEntity<Response<List<Map<String, Object>>>> response = new ResponseEntity<>(new Response<>(true, Collections.emptyList()), HttpStatus.OK);
        doReturn(response).when(notificationService).postExchange(eq(NotificationEndpoint.GET_UNREAD_NOTIFICATION.getEndpointUrl()), any(), any());

        ResponseEntity<Response<EpisodeUnreadNotificationResponse>> actualResponse = notificationService.getUnreadEpisodeNotifications(Collections.singletonList(1L), true);
        Assert.assertTrue(actualResponse.getBody().isSuccess());
    }
    @Test
    public void getUnreadEpisodeNotificationFailure() {
        ResponseEntity<Response<List<Map<String, Object>>>> response = new ResponseEntity<>(new Response<>(false, Collections.emptyList()), HttpStatus.BAD_REQUEST);
        doReturn(response).when(notificationService).postExchange(eq(NotificationEndpoint.GET_UNREAD_NOTIFICATION.getEndpointUrl()), any(), any());

        ResponseEntity<Response<EpisodeUnreadNotificationResponse>> actualResponse = notificationService.getUnreadEpisodeNotifications(Collections.singletonList(1L), false);
        Assert.assertFalse(actualResponse.getBody().isSuccess());
    }
}
