package com.everwell.transition.unit.controller;

import com.everwell.transition.BaseTest;
import com.everwell.transition.controller.EpisodeTagController;
import com.everwell.transition.exceptions.CustomExceptionHandler;
import com.everwell.transition.model.db.EpisodeTag;
import com.everwell.transition.model.db.EpisodeTagStore;
import com.everwell.transition.model.request.episodetags.*;
import com.everwell.transition.model.response.EpisodeTagResponse;
import com.everwell.transition.model.response.TagsConfigResponse;
import com.everwell.transition.service.EpisodeTagService;
import com.everwell.transition.utils.Utils;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

public class EpisodeTagsControllerTest extends BaseTest {

    private MockMvc mockMvc;

    @InjectMocks
    private EpisodeTagController episodeTagController;

    @Mock
    EpisodeTagService episodeTagService;

    @Mock
    ApplicationEventPublisher applicationEventPublisher;

    @Before
    public void setup() {
        mockMvc = MockMvcBuilders.standaloneSetup(episodeTagController)
            .setControllerAdvice(new CustomExceptionHandler())
            .build();
    }

    @Test
    public void getTagsTest() throws Exception {
        long episodeId = 12345L;
        String uri = "/v1/tags?episodeId=" + episodeId;
        when(episodeTagService.getEpisodeTags(episodeId)).thenReturn(getEpisodeTags().get(0));
        mockMvc
            .perform(
                MockMvcRequestBuilders
                    .get(uri)
                    .contentType(MediaType.APPLICATION_JSON)
                    .accept(MediaType.APPLICATION_JSON_VALUE)
            )
            .andExpect(status().isOk())
            .andExpect(MockMvcResultMatchers.jsonPath("$.data.episodeId").value(episodeId))
            .andExpect(MockMvcResultMatchers.jsonPath("$.data.tags[0].tagName").value("PHONE_UNREACHABLE"))
            .andExpect(MockMvcResultMatchers.jsonPath("$.data.currentTags[0]").value("PHONE_SWITCHED_OFF"));
    }

    @Test
    public void getTagsTest_ZeroEpisodeId() throws Exception {
        String uri = "/v1/tags?episodeId=0";
        mockMvc
            .perform(
                MockMvcRequestBuilders
                    .get(uri)
                    .contentType(MediaType.APPLICATION_JSON)
                    .accept(MediaType.APPLICATION_JSON_VALUE)
            )
            .andExpect(status().isBadRequest())
            .andExpect(MockMvcResultMatchers.jsonPath("$.message").value("episodeId must not be null!"));
    }

    @Test
    public void getTagsBulkTest() throws Exception {
        String uri = "/v1/tags/bulk";
        List<Long> episodeIdList = Arrays.asList(
            12345L, 2L, 3L
        );
        SearchTagsRequest searchTagsRequest = new SearchTagsRequest(episodeIdList, null);
        when(episodeTagService.search(searchTagsRequest)).thenReturn(getEpisodeTags());
        mockMvc
            .perform(
                MockMvcRequestBuilders
                    .post(uri)
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(Utils.asJsonString(searchTagsRequest))
                    .accept(MediaType.APPLICATION_JSON_VALUE)
            )
            .andExpect(status().isOk())
            .andExpect(MockMvcResultMatchers.jsonPath("$.data[0].tags[0].tagName").value("PHONE_UNREACHABLE"))
            .andExpect(MockMvcResultMatchers.jsonPath("$.data[0].currentTags[0]").value("PHONE_SWITCHED_OFF"));
    }

    @Test
    public void getTagsBulk_EntityIdListNullTest() throws Exception {
        String uri = "/v1/tags/bulk";
        SearchTagsRequest searchTagsRequest = new SearchTagsRequest(null, null);
        when(episodeTagService.search(searchTagsRequest)).thenReturn(getEpisodeTags());
        mockMvc
                .perform(
                        MockMvcRequestBuilders
                                .post(uri)
                                .contentType(MediaType.APPLICATION_JSON)
                                .content(Utils.asJsonString(searchTagsRequest))
                                .accept(MediaType.APPLICATION_JSON_VALUE)
                )
                .andExpect(status().isBadRequest())
                .andExpect(MockMvcResultMatchers.jsonPath("$.message").value("Episode Id List cannot be empty!"));
    }

    @Test
    public void getTagsBulk_EntityIdListEmptyTest() throws Exception {
        String uri = "/v1/tags/bulk";
        SearchTagsRequest searchTagsRequest = new SearchTagsRequest(new ArrayList<>(), null);
        when(episodeTagService.search(searchTagsRequest)).thenReturn(getEpisodeTags());
        mockMvc
                .perform(
                        MockMvcRequestBuilders
                                .post(uri)
                                .contentType(MediaType.APPLICATION_JSON)
                                .content(Utils.asJsonString(searchTagsRequest))
                                .accept(MediaType.APPLICATION_JSON_VALUE)
                )
                .andExpect(status().isBadRequest())
                .andExpect(MockMvcResultMatchers.jsonPath("$.message").value("Episode Id List cannot be empty!"));
    }

    @Test
    public void getTagsBulk_NoTagsFound_Test() throws Exception {
        String uri = "/v1/tags/bulk";
        List<Long> episodeIdList = Arrays.asList(
            12345L, 2L, 3L
        );
        SearchTagsRequest searchTagsRequest = new SearchTagsRequest(episodeIdList, null);
        when(episodeTagService.search(searchTagsRequest)).thenReturn(new ArrayList<>());
        mockMvc
            .perform(
                MockMvcRequestBuilders
                    .post(uri)
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(Utils.asJsonString(searchTagsRequest))
                    .accept(MediaType.APPLICATION_JSON_VALUE)
            )
            .andExpect(status().isOk())
            .andExpect(MockMvcResultMatchers.jsonPath("$.data").value(new ArrayList<>()));
    }

    @Test
    public void saveTagsTest() throws Exception {
        String uri = "/v1/tags";
        List<EpisodeTagDataRequest> list = Arrays.asList(
            new EpisodeTagDataRequest("TAG", LocalDateTime.now(), 12345L, true)
        );
        EpisodeTagRequest episodeTagRequest = new EpisodeTagRequest(list);
        when(episodeTagService.save(any())).thenReturn(getEpisodeTags());
        doNothing().when(applicationEventPublisher).publishEvent(any());
        mockMvc
            .perform(
                MockMvcRequestBuilders
                    .post(uri)
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(Utils.asJsonString(episodeTagRequest))
                    .accept(MediaType.APPLICATION_JSON_VALUE)
            )
            .andExpect(status().isOk())
            .andExpect(MockMvcResultMatchers.jsonPath("$.data[0].tags[0].tagName").value("PHONE_UNREACHABLE"))
            .andExpect(MockMvcResultMatchers.jsonPath("$.data[0].currentTags[0]").value("PHONE_SWITCHED_OFF"))
                .andExpect(MockMvcResultMatchers.jsonPath("$.data[0].tags[0].sticky").value(false));
    }

    @Test
    public void saveTags_DataNullTest() throws Exception {
        String uri = "/v1/tags";
        EpisodeTagRequest episodeTagRequest = new EpisodeTagRequest(null);
        when(episodeTagService.save(any())).thenReturn(getEpisodeTags());
        mockMvc
                .perform(
                        MockMvcRequestBuilders
                                .post(uri)
                                .contentType(MediaType.APPLICATION_JSON)
                                .content(Utils.asJsonString(episodeTagRequest))
                                .accept(MediaType.APPLICATION_JSON_VALUE)
                )
                .andExpect(status().isBadRequest())
                .andExpect(MockMvcResultMatchers.jsonPath("$.message").value("Tag data is null!"));
    }

    @Test
    public void saveTagsTest_ListDataEpisodeIdNull() throws Exception {
        String uri = "/v1/tags";
        List<EpisodeTagDataRequest> list = Arrays.asList(
                new EpisodeTagDataRequest("TAG", LocalDateTime.now(), null, false)
        );
        EpisodeTagRequest episodeTagRequest = new EpisodeTagRequest(list);
        when(episodeTagService.save(any())).thenReturn(getEpisodeTags());
        mockMvc
                .perform(
                        MockMvcRequestBuilders
                                .post(uri)
                                .contentType(MediaType.APPLICATION_JSON)
                                .content(Utils.asJsonString(episodeTagRequest))
                                .accept(MediaType.APPLICATION_JSON_VALUE)
                )
                .andExpect(status().isBadRequest())
                .andExpect(MockMvcResultMatchers.jsonPath("$.message").value("Episode Id cannot be empty!"));
    }

    @Test
    public void saveTagsTest_ListDataTagNameNull() throws Exception {
        String uri = "/v1/tags";
        List<EpisodeTagDataRequest> list = Arrays.asList(
                new EpisodeTagDataRequest(null, LocalDateTime.now(), null, false)
        );
        EpisodeTagRequest episodeTagRequest = new EpisodeTagRequest(list);
        when(episodeTagService.save(any())).thenReturn(getEpisodeTags());
        mockMvc
                .perform(
                        MockMvcRequestBuilders
                                .post(uri)
                                .contentType(MediaType.APPLICATION_JSON)
                                .content(Utils.asJsonString(episodeTagRequest))
                                .accept(MediaType.APPLICATION_JSON_VALUE)
                )
                .andExpect(status().isBadRequest())
                .andExpect(MockMvcResultMatchers.jsonPath("$.message").value("Tag Name cannot be empty!"));
    }

    @Test
    public void deleteTagsBulkTest() throws Exception {
        String uri = "/v1/tags/delete/bulk";
        List<String> tagList = Arrays.asList(
                "A", "B", "C"
        );
        List<String> tagDate = Arrays.asList(
                "12-12-2021", "12-12-2021", "12-12-2021"
        );
        EpisodeTagBulkDeletionRequest episodeTagBulkDeletionRequest = new EpisodeTagBulkDeletionRequest(Collections.singletonList(1L), tagList, tagDate, true);
        when(episodeTagService.save(any())).thenReturn(getEpisodeTags());
        doNothing().when(applicationEventPublisher).publishEvent(any());
        mockMvc
                .perform(
                        MockMvcRequestBuilders
                                .post(uri)
                                .contentType(MediaType.APPLICATION_JSON)
                                .content(Utils.asJsonString(episodeTagBulkDeletionRequest))
                                .accept(MediaType.APPLICATION_JSON_VALUE)
                )
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.data").value("Deleted Successfully!"));
    }

    @Test
    public void deleteTagsBulkTestNonSticky() throws Exception {
        String uri = "/v1/tags/delete/bulk";
        List<String> tagList = Arrays.asList(
                "A", "B", "C"
        );
        List<String> tagDate = Arrays.asList(
                "12-12-2021", "12-12-2021", "12-12-2021"
        );
        EpisodeTagBulkDeletionRequest episodeTagBulkDeletionRequest = new EpisodeTagBulkDeletionRequest(Collections.singletonList(1L), tagList, tagDate, false);
        when(episodeTagService.save(any())).thenReturn(getEpisodeTags());
        doNothing().when(applicationEventPublisher).publishEvent(any());
        mockMvc
                .perform(
                        MockMvcRequestBuilders
                                .post(uri)
                                .contentType(MediaType.APPLICATION_JSON)
                                .content(Utils.asJsonString(episodeTagBulkDeletionRequest))
                                .accept(MediaType.APPLICATION_JSON_VALUE)
                )
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.data").value("Deleted Successfully!"));
    }

    @Test
    public void deleteTagsBulkTest_EpisodeIdAbsent() throws Exception {
        String uri = "/v1/tags/delete/bulk";
        List<String> tagList = Arrays.asList(
                "A", "B", "C"
        );
        List<String> tagDate = Arrays.asList(
                "12-12-2021", "12-12-2021", "12-12-2021"
        );
        EpisodeTagBulkDeletionRequest episodeTagBulkDeletionRequest = new EpisodeTagBulkDeletionRequest(null, tagList, tagDate, false);
        when(episodeTagService.save(any())).thenReturn(getEpisodeTags());
        mockMvc
                .perform(
                        MockMvcRequestBuilders
                                .post(uri)
                                .contentType(MediaType.APPLICATION_JSON)
                                .content(Utils.asJsonString(episodeTagBulkDeletionRequest))
                                .accept(MediaType.APPLICATION_JSON_VALUE)
                )
                .andExpect(status().isBadRequest())
                .andExpect(MockMvcResultMatchers.jsonPath("$.message").value("EpisodeId must be present"));
    }

    @Test
    public void deleteTagsBulkTest_TagListAbsent() throws Exception {
        String uri = "/v1/tags/delete/bulk";
        List<String> tagList = Arrays.asList(
                "A", "B", "C"
        );
        List<String> tagDate = Arrays.asList(
                "12-12-2021", "12-12-2021", "12-12-2021"
        );
        EpisodeTagBulkDeletionRequest episodeTagBulkDeletionRequest = new EpisodeTagBulkDeletionRequest(Collections.singletonList(1L), null, tagDate, false);
        when(episodeTagService.save(any())).thenReturn(getEpisodeTags());
        mockMvc
                .perform(
                        MockMvcRequestBuilders
                                .post(uri)
                                .contentType(MediaType.APPLICATION_JSON)
                                .content(Utils.asJsonString(episodeTagBulkDeletionRequest))
                                .accept(MediaType.APPLICATION_JSON_VALUE)
                )
                .andExpect(status().isBadRequest())
                .andExpect(MockMvcResultMatchers.jsonPath("$.message").value("TagList must be present"));
    }

    @Test
    public void deleteTagsBulkTest_TagDatesListAbsent() throws Exception {
        String uri = "/v1/tags/delete/bulk";
        List<String> tagList = Arrays.asList(
                "A", "B", "C"
        );
        List<String> tagDate = Arrays.asList(
                "12-12-2021", "12-12-2021", "12-12-2021"
        );
        EpisodeTagBulkDeletionRequest episodeTagBulkDeletionRequest = new EpisodeTagBulkDeletionRequest(Collections.singletonList(1L), tagList, null, false);
        when(episodeTagService.save(any())).thenReturn(getEpisodeTags());
        mockMvc
                .perform(
                        MockMvcRequestBuilders
                                .post(uri)
                                .contentType(MediaType.APPLICATION_JSON)
                                .content(Utils.asJsonString(episodeTagBulkDeletionRequest))
                                .accept(MediaType.APPLICATION_JSON_VALUE)
                )
                .andExpect(status().isBadRequest())
                .andExpect(MockMvcResultMatchers.jsonPath("$.message").value("TagDates must be present"));
    }

    @Test
    public void getTagsConfig() throws Exception {
        String uri = "/v1/tags/config";
        when(episodeTagService.getEpisodeConfig()).thenReturn(getConfig());
        mockMvc
                .perform(
                        MockMvcRequestBuilders
                                .get(uri)
                                .contentType(MediaType.APPLICATION_JSON)
                                .accept(MediaType.APPLICATION_JSON_VALUE)
                )
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.data[0].tagName").value("Invalid_Phone_Number"))
                .andExpect(MockMvcResultMatchers.jsonPath("$.data[0].tagGroup").value("Phone_issues"));
    }

    @Test
    public void deleteTagsTest() throws Exception {
        String uri = "/v1/tags/delete";
        List<String> tagList = Arrays.asList(
                "A", "B", "C"
        );
        List<String> tagDate = Arrays.asList(
                "12-12-2021", "12-12-2021", "12-12-2021"
        );
        EpisodeTagDeletionRequest episodeTagDeletionRequest = new EpisodeTagDeletionRequest(1L, tagList, tagDate);
        when(episodeTagService.save(any())).thenReturn(getEpisodeTags());
        doNothing().when(applicationEventPublisher).publishEvent(any());
        mockMvc
                .perform(
                        MockMvcRequestBuilders
                                .post(uri)
                                .contentType(MediaType.APPLICATION_JSON)
                                .content(Utils.asJsonString(episodeTagDeletionRequest))
                                .accept(MediaType.APPLICATION_JSON_VALUE)
                )
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.data").value("Deleted Successfully!"));
    }

    @Test
    public void deleteTagsTest_EpisodeIdAbsent() throws Exception {
        String uri = "/v1/tags/delete";
        List<String> tagList = Arrays.asList(
                "A", "B", "C"
        );
        List<String> tagDate = Arrays.asList(
                "12-12-2021", "12-12-2021", "12-12-2021"
        );
        EpisodeTagDeletionRequest episodeTagDeletionRequest = new EpisodeTagDeletionRequest(null, tagList, tagDate);
        when(episodeTagService.save(any())).thenReturn(getEpisodeTags());
        mockMvc
                .perform(
                        MockMvcRequestBuilders
                                .post(uri)
                                .contentType(MediaType.APPLICATION_JSON)
                                .content(Utils.asJsonString(episodeTagDeletionRequest))
                                .accept(MediaType.APPLICATION_JSON_VALUE)
                )
                .andExpect(status().isBadRequest())
                .andExpect(MockMvcResultMatchers.jsonPath("$.message").value("EpisodeId must be present"));
    }

    @Test
    public void deleteTagsTest_TagListAbsent() throws Exception {
        String uri = "/v1/tags/delete";
        List<String> tagList = Arrays.asList(
                "A", "B", "C"
        );
        List<String> tagDate = Arrays.asList(
                "12-12-2021", "12-12-2021", "12-12-2021"
        );
        EpisodeTagDeletionRequest episodeTagDeletionRequest = new EpisodeTagDeletionRequest(1L, null, tagDate);
        when(episodeTagService.save(any())).thenReturn(getEpisodeTags());
        mockMvc
                .perform(
                        MockMvcRequestBuilders
                                .post(uri)
                                .contentType(MediaType.APPLICATION_JSON)
                                .content(Utils.asJsonString(episodeTagDeletionRequest))
                                .accept(MediaType.APPLICATION_JSON_VALUE)
                )
                .andExpect(status().isBadRequest())
                .andExpect(MockMvcResultMatchers.jsonPath("$.message").value("TagList must be present"));
    }

    @Test
    public void deleteTagsTest_TagListEmptyTest() throws Exception {
        String uri = "/v1/tags/delete";
        List<String> tagList = new ArrayList<>();
        List<String> tagDate = Arrays.asList(
                "12-12-2021", "12-12-2021", "12-12-2021"
        );
        EpisodeTagDeletionRequest episodeTagDeletionRequest = new EpisodeTagDeletionRequest(1L, tagList, tagDate);
        when(episodeTagService.save(any())).thenReturn(getEpisodeTags());
        mockMvc
                .perform(
                        MockMvcRequestBuilders
                                .post(uri)
                                .contentType(MediaType.APPLICATION_JSON)
                                .content(Utils.asJsonString(episodeTagDeletionRequest))
                                .accept(MediaType.APPLICATION_JSON_VALUE)
                )
                .andExpect(status().isBadRequest())
                .andExpect(MockMvcResultMatchers.jsonPath("$.message").value("TagList must be present"));
    }

    @Test
    public void deleteTagsTest_TagDatesListAbsent() throws Exception {
        String uri = "/v1/tags/delete";
        List<String> tagList = Arrays.asList(
                "A", "B", "C"
        );
        List<String> tagDate = Arrays.asList(
                "12-12-2021", "12-12-2021", "12-12-2021"
        );
        EpisodeTagDeletionRequest episodeTagDeletionRequest = new EpisodeTagDeletionRequest(1L, tagList, null);
        when(episodeTagService.save(any())).thenReturn(getEpisodeTags());
        mockMvc
                .perform(
                        MockMvcRequestBuilders
                                .post(uri)
                                .contentType(MediaType.APPLICATION_JSON)
                                .content(Utils.asJsonString(episodeTagDeletionRequest))
                                .accept(MediaType.APPLICATION_JSON_VALUE)
                )
                .andExpect(status().isBadRequest())
                .andExpect(MockMvcResultMatchers.jsonPath("$.message").value("TagDates must be present"));
    }

    @Test
    public void deleteTagsTest_TagDatesListEmpty() throws Exception {
        String uri = "/v1/tags/delete";
        List<String> tagList = Arrays.asList(
                "A", "B", "C"
        );
        List<String> tagDate = new ArrayList<>();
        EpisodeTagDeletionRequest episodeTagDeletionRequest = new EpisodeTagDeletionRequest(1L, tagList, tagDate);
        when(episodeTagService.save(any())).thenReturn(getEpisodeTags());
        mockMvc
                .perform(
                        MockMvcRequestBuilders
                                .post(uri)
                                .contentType(MediaType.APPLICATION_JSON)
                                .content(Utils.asJsonString(episodeTagDeletionRequest))
                                .accept(MediaType.APPLICATION_JSON_VALUE)
                )
                .andExpect(status().isBadRequest())
                .andExpect(MockMvcResultMatchers.jsonPath("$.message").value("TagDates must be present"));
    }



    List<TagsConfigResponse> getConfig() {
        List<TagsConfigResponse> config = Arrays.asList(
            new TagsConfigResponse("Invalid_Phone_Number", "displayName", "icon", "desc", "Phone_issues")
        );
        return config;
    }

    public static List<EpisodeTagResponse> getEpisodeTags() {
        long episodeId = 12345;
        List<EpisodeTag> tags = Arrays.asList(
            new EpisodeTag(1L, episodeId, "PHONE_UNREACHABLE", LocalDateTime.now(), LocalDateTime.now(), false),
            new EpisodeTag(1L, episodeId, "PHONE_SWITCHED_OFF", LocalDateTime.now(), LocalDateTime.now(), true)
        );
        List<String> currentTags = tags.stream().filter(EpisodeTag::getSticky).map(EpisodeTag::getTagName).collect(Collectors.toList());
        List<EpisodeTag> nonStickyTags = tags.stream().filter(t -> !t.getSticky()).collect(Collectors.toList());
        List<EpisodeTagResponse> list = new ArrayList<>();
        list.add(new EpisodeTagResponse(12345L, nonStickyTags, currentTags));
        return list;
    }

}
