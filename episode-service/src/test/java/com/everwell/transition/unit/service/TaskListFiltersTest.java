package com.everwell.transition.unit.service;

import com.everwell.transition.BaseTest;
import com.everwell.transition.constants.FieldConstants;
import com.everwell.transition.constants.TaskListFields;
import com.everwell.transition.elasticsearch.ElasticConstants;
import com.everwell.transition.elasticsearch.service.Impl.TaskListFiltersToEpisodeSearchFields;
import com.everwell.transition.exceptions.ValidationException;
import com.everwell.transition.model.request.episodesearch.EpisodeSearchRequest;
import com.everwell.transition.model.request.tasklistfilters.EpisodeTasklistRequest;
import com.everwell.transition.model.request.tasklistfilters.TaskListFilterRequest;
import org.junit.Assert;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mockito;
import org.mockito.Spy;
import org.springframework.data.domain.Sort;

import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Map;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.doReturn;

public class TaskListFiltersTest extends BaseTest {

    @Spy
    @InjectMocks
    TaskListFiltersToEpisodeSearchFields taskListFiltersToEpisodeSearchFields;

    @Test
    public void transformTest() {
        EpisodeSearchRequest episodeSearchRequest = new EpisodeSearchRequest();
        episodeSearchRequest.setSortKey("id");
        episodeSearchRequest.setSortDirection(Sort.Direction.ASC);
        EpisodeTasklistRequest episodeTasklistRequest = new EpisodeTasklistRequest(episodeSearchRequest, Collections.emptyList());

        doReturn("id").when(taskListFiltersToEpisodeSearchFields).taskListFiltersSortKeyToElasticSortKey(eq("id"));
        doReturn(new EpisodeSearchRequest()).when(taskListFiltersToEpisodeSearchFields).taskListFiltersToElasticSearchRequest(any());

        EpisodeSearchRequest searchRequest = taskListFiltersToEpisodeSearchFields.transform(episodeTasklistRequest);
        Assert.assertEquals("id", searchRequest.getSortKey());
    }

    @Test
    public void testSortKeyId() {
        String episodeSortKey = taskListFiltersToEpisodeSearchFields.taskListFiltersSortKeyToElasticSortKey("id");
        Assert.assertEquals("id", episodeSortKey);
    }

    @Test
    public void testSortKeyName() {
        String episodeSortKey = taskListFiltersToEpisodeSearchFields.taskListFiltersSortKeyToElasticSortKey("name");
        Assert.assertEquals(FieldConstants.PERSON_FIELD_NAME, episodeSortKey);
    }

    @Test
    public void testSortKeyPhone() {
        String episodeSortKey = taskListFiltersToEpisodeSearchFields.taskListFiltersSortKeyToElasticSortKey(TaskListFields.TASK_LIST_FIELD_CONTACT_NUMBER);
        Assert.assertEquals(FieldConstants.PERSON_PRIMARY_PHONE, episodeSortKey);
    }

    @Test
    public void testSortKeyLastDosage() {
        String episodeSortKey = taskListFiltersToEpisodeSearchFields.taskListFiltersSortKeyToElasticSortKey(TaskListFields.TASK_LIST_FIELD_LAST_CALLED_ON);
        Assert.assertEquals(FieldConstants.EPISODE_FIELD_LAST_DOSAGE, episodeSortKey);
    }

    @Test
    public void testSortKeyDiagnosisDate() {
        String episodeSortKey = taskListFiltersToEpisodeSearchFields.taskListFiltersSortKeyToElasticSortKey(TaskListFields.TASK_LIST_FIELD_DIAGNOSIS_DATE);
        Assert.assertEquals(FieldConstants.EPISODE_FIELD_DIAGNOSIS_DATE ,episodeSortKey);
    }

    @Test
    public void testSortKeyStartDate() {
        String episodeSortKey = taskListFiltersToEpisodeSearchFields.taskListFiltersSortKeyToElasticSortKey(FieldConstants.TB_TREATMENT_START_DATE);
        Assert.assertEquals(FieldConstants.EPISODE_FIELD_START_DATE, episodeSortKey);
    }

    @Test
    public void testSortKeyMonitoringMethod() {
        String episodeSortKey = taskListFiltersToEpisodeSearchFields.taskListFiltersSortKeyToElasticSortKey(TaskListFields.TASK_LIST_FIELD_ADHERENCE_TECH);
        Assert.assertEquals(FieldConstants.MONITORING_METHOD, episodeSortKey);
    }

    @Test
    public void testSortKeyTypeOfEpisode() {
        String episodeSortKey = taskListFiltersToEpisodeSearchFields.taskListFiltersSortKeyToElasticSortKey(TaskListFields.TASK_LIST_FIELD_PATIENT_TYPE);
        Assert.assertEquals(FieldConstants.EPISODE_FIELD_TYPE_OF_EPISODE, episodeSortKey);
    }

    @Test
    public void testToElasticSearchRequestEndDate() {
        List<TaskListFilterRequest> taskListFilterRequestList = Collections.singletonList(new TaskListFilterRequest("EndDatePassed", "True", 4L));
        EpisodeTasklistRequest episodeTasklistRequest = new EpisodeTasklistRequest(new EpisodeSearchRequest(), taskListFilterRequestList);

        EpisodeSearchRequest searchRequest = taskListFiltersToEpisodeSearchFields.taskListFiltersToElasticSearchRequest(episodeTasklistRequest);
        Assert.assertNotNull(searchRequest.getRange().get(FieldConstants.EPISODE_FIELD_END_DATE));
    }

    @Test
    public void testToElasticSearchRequestMonitoringMethod() {
        List<TaskListFilterRequest> taskListFilterRequestList = Collections.singletonList(new TaskListFilterRequest(TaskListFields.TASK_LIST_MONITORING_METHOD, "MERM", 43L));
        EpisodeTasklistRequest episodeTasklistRequest = new EpisodeTasklistRequest(new EpisodeSearchRequest(), taskListFilterRequestList);

        EpisodeSearchRequest searchRequest = taskListFiltersToEpisodeSearchFields.taskListFiltersToElasticSearchRequest(episodeTasklistRequest);
        Assert.assertNotNull(searchRequest.getSearch().getMust().get(FieldConstants.MONITORING_METHOD));
    }

    @Test
    public void testToElasticSearchRequestRegistrationDateRange() {
        List<TaskListFilterRequest> taskListFilterRequestList = Collections.singletonList(new TaskListFilterRequest(TaskListFields.TASK_LIST_REGISTRATION_DATE_RANGE, "{\"LowerBound\":14,\"UpperBound\":20}", 36L));
        EpisodeTasklistRequest episodeTasklistRequest = new EpisodeTasklistRequest(new EpisodeSearchRequest(), taskListFilterRequestList);

        EpisodeSearchRequest searchRequest = taskListFiltersToEpisodeSearchFields.taskListFiltersToElasticSearchRequest(episodeTasklistRequest);
        Assert.assertNotNull(searchRequest.getRange().get(FieldConstants.EPISODE_FIELD_CREATED_DATE));
    }

    @Test(expected = ValidationException.class)
    public void testToElasticSearchRequestRegistrationDateRangeException() {
        List<TaskListFilterRequest> taskListFilterRequestList = Collections.singletonList(new TaskListFilterRequest(TaskListFields.TASK_LIST_REGISTRATION_DATE_RANGE, "{", 36L));
        EpisodeTasklistRequest episodeTasklistRequest = new EpisodeTasklistRequest(new EpisodeSearchRequest(), taskListFilterRequestList);

        EpisodeSearchRequest searchRequest = taskListFiltersToEpisodeSearchFields.taskListFiltersToElasticSearchRequest(episodeTasklistRequest);
        Assert.assertNotNull(searchRequest.getRange().get(FieldConstants.EPISODE_FIELD_CREATED_DATE));
    }

    @Test
    public void testToElasticSearchRequestRefillDue() {
        List<TaskListFilterRequest> taskListFilterRequestList = Collections.singletonList(new TaskListFilterRequest(TaskListFields.TASK_LIST_REFILL_DUE, "True", 36L));
        EpisodeTasklistRequest episodeTasklistRequest = new EpisodeTasklistRequest(new EpisodeSearchRequest(), taskListFilterRequestList);

        EpisodeSearchRequest searchRequest = taskListFiltersToEpisodeSearchFields.taskListFiltersToElasticSearchRequest(episodeTasklistRequest);
        Assert.assertNotNull(searchRequest.getRange().get(FieldConstants.EPISODE_FIELD_START_DATE));
    }

    @Test
    public void testToElasticSearchRequestDoseNotReported() {
        List<TaskListFilterRequest> taskListFilterRequestList = Collections.singletonList(new TaskListFilterRequest(TaskListFields.TASK_LIST_DOSE_NOT_REPORTED_TODAY, "True", 36L));
        EpisodeTasklistRequest episodeTasklistRequest = new EpisodeTasklistRequest(new EpisodeSearchRequest(), taskListFilterRequestList);

        EpisodeSearchRequest searchRequest = taskListFiltersToEpisodeSearchFields.taskListFiltersToElasticSearchRequest(episodeTasklistRequest);
        Assert.assertNotNull(searchRequest.getRange().get(FieldConstants.EPISODE_FIELD_LAST_DOSAGE));
    }

    @Test
    public void testToElasticSearchRequestTreatmentOutcome() {
        List<TaskListFilterRequest> taskListFilterRequestList = Collections.singletonList(new TaskListFilterRequest(TaskListFields.TASK_LIST_TREATMENT_OUTCOME, "Cured", 1L));
        EpisodeTasklistRequest episodeTasklistRequest = new EpisodeTasklistRequest(new EpisodeSearchRequest(), taskListFilterRequestList);

        EpisodeSearchRequest searchRequest = taskListFiltersToEpisodeSearchFields.taskListFiltersToElasticSearchRequest(episodeTasklistRequest);
        Assert.assertTrue(searchRequest.getSearch().getMust().containsKey(FieldConstants.FIELD_TREATMENT_OUTCOME));
        Assert.assertEquals("Cured", searchRequest.getSearch().getMust().get(FieldConstants.FIELD_TREATMENT_OUTCOME));
    }

    @Test
    public void testToElasticSearchMaxDaysSinceTreatmentStart() {
        List<TaskListFilterRequest> taskListFilterRequestList = Collections.singletonList(new TaskListFilterRequest(TaskListFields.TASK_LIST_MAX_DAYS_SINCE_TREATMENT_START, "5", 1L));
        EpisodeTasklistRequest episodeTasklistRequest = new EpisodeTasklistRequest(new EpisodeSearchRequest(), taskListFilterRequestList);

        EpisodeSearchRequest searchRequest = taskListFiltersToEpisodeSearchFields.taskListFiltersToElasticSearchRequest(episodeTasklistRequest);
        Assert.assertTrue(searchRequest.getRange().containsKey(FieldConstants.EPISODE_FIELD_START_DATE));
        Assert.assertTrue(((Map<String, String>) searchRequest.getRange().get(FieldConstants.EPISODE_FIELD_START_DATE)).containsKey(ElasticConstants.ELASTIC_SEARCH_RANGE_GREATER_THAN_EQUAL));
    }

    @Test
    public void testToElasticSearchMinDaysSinceTreatmentStart() {
        List<TaskListFilterRequest> taskListFilterRequestList = Collections.singletonList(new TaskListFilterRequest(TaskListFields.TASK_LIST_MIN_DAYS_SINCE_TREATMENT_START, "5", 1L));
        EpisodeTasklistRequest episodeTasklistRequest = new EpisodeTasklistRequest(new EpisodeSearchRequest(), taskListFilterRequestList);

        EpisodeSearchRequest searchRequest = taskListFiltersToEpisodeSearchFields.taskListFiltersToElasticSearchRequest(episodeTasklistRequest);
        Assert.assertTrue(searchRequest.getRange().containsKey(FieldConstants.EPISODE_FIELD_START_DATE));
        Assert.assertTrue(((Map<String, String>) searchRequest.getRange().get(FieldConstants.EPISODE_FIELD_START_DATE)).containsKey(ElasticConstants.ELASTIC_SEARCH_RANGE_LOWER_THAN_EQUAL));
    }

    @Test
    public void testToElasticSearchMaxDaysSinceTreatmentStart_MaxThenMin() {
        List<TaskListFilterRequest> taskListFilterRequestList = Arrays.asList(
            new TaskListFilterRequest(TaskListFields.TASK_LIST_MAX_DAYS_SINCE_TREATMENT_START, "5", 1L),
            new TaskListFilterRequest(TaskListFields.TASK_LIST_MIN_DAYS_SINCE_TREATMENT_START, "5", 2L)
        );
        EpisodeTasklistRequest episodeTasklistRequest = new EpisodeTasklistRequest(new EpisodeSearchRequest(), taskListFilterRequestList);

        EpisodeSearchRequest searchRequest = taskListFiltersToEpisodeSearchFields.taskListFiltersToElasticSearchRequest(episodeTasklistRequest);
        Assert.assertTrue(searchRequest.getRange().containsKey(FieldConstants.EPISODE_FIELD_START_DATE));
        Assert.assertTrue(((Map<String, String>) searchRequest.getRange().get(FieldConstants.EPISODE_FIELD_START_DATE)).containsKey(ElasticConstants.ELASTIC_SEARCH_RANGE_GREATER_THAN_EQUAL));
        Assert.assertTrue(((Map<String, String>) searchRequest.getRange().get(FieldConstants.EPISODE_FIELD_START_DATE)).containsKey(ElasticConstants.ELASTIC_SEARCH_RANGE_LOWER_THAN_EQUAL));
    }

    @Test
    public void testToElasticSearchMaxDaysSinceTreatmentStart_MinThenMax() {
        List<TaskListFilterRequest> taskListFilterRequestList = Arrays.asList(
                new TaskListFilterRequest(TaskListFields.TASK_LIST_MIN_DAYS_SINCE_TREATMENT_START, "5", 2L),
        new TaskListFilterRequest(TaskListFields.TASK_LIST_MAX_DAYS_SINCE_TREATMENT_START, "5", 1L)
        );
        EpisodeTasklistRequest episodeTasklistRequest = new EpisodeTasklistRequest(new EpisodeSearchRequest(), taskListFilterRequestList);

        EpisodeSearchRequest searchRequest = taskListFiltersToEpisodeSearchFields.taskListFiltersToElasticSearchRequest(episodeTasklistRequest);
        Assert.assertTrue(searchRequest.getRange().containsKey(FieldConstants.EPISODE_FIELD_START_DATE));
        Assert.assertTrue(((Map<String, String>) searchRequest.getRange().get(FieldConstants.EPISODE_FIELD_START_DATE)).containsKey(ElasticConstants.ELASTIC_SEARCH_RANGE_GREATER_THAN_EQUAL));
        Assert.assertTrue(((Map<String, String>) searchRequest.getRange().get(FieldConstants.EPISODE_FIELD_START_DATE)).containsKey(ElasticConstants.ELASTIC_SEARCH_RANGE_LOWER_THAN_EQUAL));
    }

    @Test
    public void testToElasticSearchMinDaysSinceTreatmentStart_PositiveDosingDaysFilter() {
        List<TaskListFilterRequest> taskListFilterRequestList = Arrays.asList(
                new TaskListFilterRequest(TaskListFields.TASK_LIST_MIN_DAYS_SINCE_TREATMENT_START, "5", 1L),
                new TaskListFilterRequest(TaskListFields.TASK_LIST_POSITIVE_DOSING_DAYS, "5", 2L)
        );
        EpisodeTasklistRequest episodeTasklistRequest = new EpisodeTasklistRequest(new EpisodeSearchRequest(), taskListFilterRequestList);

        EpisodeSearchRequest searchRequest = taskListFiltersToEpisodeSearchFields.taskListFiltersToElasticSearchRequest(episodeTasklistRequest);
        Assert.assertFalse(searchRequest.getRange().containsKey(FieldConstants.EPISODE_FIELD_START_DATE));
    }

}
