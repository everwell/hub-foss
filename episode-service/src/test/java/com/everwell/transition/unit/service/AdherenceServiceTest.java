package com.everwell.transition.unit.service;

import com.everwell.transition.BaseTest;
import com.everwell.transition.constants.RuleConstants;
import com.everwell.transition.enums.AdherenceCodeEnum;
import com.everwell.transition.enums.AdherenceTypeMappingEnum;
import com.everwell.transition.enums.iam.IAMField;
import com.everwell.transition.exceptions.ValidationException;
import com.everwell.transition.model.db.EpisodeTag;
import com.everwell.transition.model.dto.EpisodeDto;
import com.everwell.transition.model.dto.dispensation.DispensationData;
import com.everwell.transition.model.dto.dispensation.DispensationProduct;
import com.everwell.transition.model.dto.episode.EpisodeTransitionDetails;
import com.everwell.transition.model.request.adherence.EditDoseBulkRequest;
import com.everwell.transition.model.request.adherence.EditDosesRequest;
import com.everwell.transition.model.db.EpisodeTagStore;
import com.everwell.transition.model.dto.AdherenceDataForCalendar;
import com.everwell.transition.model.dto.AdherenceMonthWiseData;
import com.everwell.transition.model.request.adherence.RecordAdherenceBulkRequest;
import com.everwell.transition.model.request.treatmentPlan.DoseDetails;
import com.everwell.transition.model.response.AdherenceCodeConfigResponse;
import com.everwell.transition.model.response.AdherenceResponse;
import com.everwell.transition.model.response.dispensation.EntityDispensationResponse;
import com.everwell.transition.model.response.Response;
import com.everwell.transition.model.response.EpisodeTagResponse;
import com.everwell.transition.postconstruct.AdherenceGlobalConfigMap;
import com.everwell.transition.service.DispensationService;
import com.everwell.transition.service.EpisodeLogService;
import com.everwell.transition.service.EpisodeTagService;
import com.everwell.transition.service.IAMService;
import com.everwell.transition.postconstruct.EpisodeTagStoreCreation;
import com.everwell.transition.service.impl.AdherenceServiceImpl;
import com.everwell.transition.utils.Utils;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.Spy;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import static org.mockito.Mockito.*;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.*;

public class AdherenceServiceTest extends BaseTest {

    @InjectMocks
    @Spy
    AdherenceServiceImpl adherenceService;

    @Mock
    AdherenceGlobalConfigMap adherenceGlobalConfigMap;

    @Mock
    IAMService iamService;

    @Mock
    DispensationService dispensationService;

    @Mock
    EpisodeTagStoreCreation episodeTagStoreCreation;

    @Mock
    EpisodeLogService episodeLogService;

    @Mock
    EpisodeTagService episodeTagService;

    @Before
    public void init() {
        initAdherenceCodeMap();
    }

    private void initAdherenceCodeMap() {
        Map<String, EpisodeTagStore> episodeTagStoreMap = new HashMap<>();
        episodeTagStoreMap.put("No_Coverage", new EpisodeTagStore(1L, "No_Coverage", "No Coverage", "", "", ""));
        Mockito.when(episodeTagStoreCreation.getEpisodeTagStoreMap()).thenReturn(episodeTagStoreMap);
    }

    private Map<String, AdherenceCodeConfigResponse> getAdherenceCodeConfigMap() {
        Map<String, AdherenceCodeConfigResponse> codeToConfigMap = new HashMap<>();
        codeToConfigMap.put("BLANK", new AdherenceCodeConfigResponse('7', "BLANK", null, "No adherence status"));
        return codeToConfigMap;
    }

    private Map<String, Integer> getAdherenceMonitoringMethodConfig() {
        Map<String, Integer> monitoringMethodToIdMap =  new HashMap<String, Integer>() {{
            put("none", 1);
            put("merm", 2);
            put("99dots", 3);
        }};
        return monitoringMethodToIdMap;
    }

    private Map<String, Integer> getAdherenceScheduleTypeConfig() {
        Map<String, Integer> scheduleTypeToIdMap =  new HashMap<String, Integer>() {{
            put("daily", 1);
            put("weekly", 2);
        }};
        return scheduleTypeToIdMap;
    }

    private Map<String, Object> getStageTransitionResultData(String action) {
        Map<String, Object> stageTransitionResult =  new HashMap<String, Object>() {{
            put(RuleConstants.ACTION_ON_IAM, action);
        }};
        return stageTransitionResult;
    }

    @Test
    public void formatAdherenceStringMonthWiseAndAttachTagsTest() {
        AdherenceResponse adherenceResponse = new AdherenceResponse(
            Utils.toLocalDateTimeWithNull("02-01-2022 18:30:00"),
            Utils.toLocalDateTimeWithNull("06-01-2022 18:30:00"),
            "654A9"
        );
        Map<LocalDateTime, List<EpisodeTag>> dateToTagListMap = new HashMap<>();
        dateToTagListMap.put(Utils.toLocalDateTimeWithNull("04-01-2022 00:00:00"), Arrays.asList(
                new EpisodeTag(123L, "No_Coverage", Utils.toLocalDateTimeWithNull("04-01-2022 00:30:00"), false)
        ));
        ZoneId zoneId = ZoneId.of("Asia/Calcutta");
        Mockito.when(adherenceGlobalConfigMap.getCodeToConfigMap()).thenReturn(getAdherenceCodeConfigMap());
        List<AdherenceMonthWiseData> adherenceMonthWiseData =
                adherenceService.formatAdherenceStringMonthWiseAndAttachTags(adherenceResponse, dateToTagListMap, zoneId);
        //month data (day-time) validations
        Assert.assertEquals(adherenceMonthWiseData.size(), 1);
        Assert.assertEquals(adherenceMonthWiseData.get(0).getMonthNumber(), 1);
        Assert.assertEquals(adherenceMonthWiseData.get(0).getDaysInMonth(), 31);
        Assert.assertEquals(adherenceMonthWiseData.get(0).getMonthName(), "JANUARY");
        Assert.assertEquals(adherenceMonthWiseData.get(0).getYear(), 2022);
        //adherence data validations
        List<AdherenceDataForCalendar> adherenceDataForCalendars = adherenceMonthWiseData.get(0).getAdherenceDataForCalendar();
        Assert.assertEquals(adherenceDataForCalendars.size(), 7);
        Assert.assertEquals(adherenceDataForCalendars.get(0).getCode(), '7');
        Assert.assertEquals(adherenceDataForCalendars.get(1).getCode(), '7');
        Assert.assertEquals(adherenceDataForCalendars.get(2).getCode(), '6');
        Assert.assertEquals(adherenceDataForCalendars.get(3).getCode(), '5');
        Assert.assertEquals(adherenceDataForCalendars.get(4).getCode(), '4');
        Assert.assertEquals(adherenceDataForCalendars.get(5).getCode(), 'A');
        Assert.assertEquals(adherenceDataForCalendars.get(6).getCode(), '9');
        //adherence day tag validations
        List<String> tags = adherenceMonthWiseData.get(0).getAdherenceDataForCalendar().get(3).getTagsForDay();
        Assert.assertEquals(tags.size(), 1);
        Assert.assertEquals(tags.get(0), "No Coverage");
    }

    @Test
    public void formatAdherenceStringMonthWiseAndAttachTagsTestMultipleMonths() {
        AdherenceResponse adherenceResponse = new AdherenceResponse(
                Utils.toLocalDateTimeWithNull("29-01-2022 18:30:00"),
                Utils.toLocalDateTimeWithNull("03-02-2022 18:30:00"),
                "654A92"
        );
        Map<LocalDateTime, List<EpisodeTag>> dateToTagListMap = new HashMap<>();
        ZoneId zoneId = ZoneId.of("Asia/Calcutta");
        Mockito.when(adherenceGlobalConfigMap.getCodeToConfigMap()).thenReturn(getAdherenceCodeConfigMap());
        List<AdherenceMonthWiseData> adherenceMonthWiseData =
                adherenceService.formatAdherenceStringMonthWiseAndAttachTags(adherenceResponse, dateToTagListMap, zoneId);
        //month data (day-time) validations
        Assert.assertEquals(adherenceMonthWiseData.size(), 2);
    }

    @Test
    public void getAdherenceConfigTest() {
        Mockito.when(adherenceGlobalConfigMap.getCodeToConfigMap()).thenReturn(getAdherenceCodeConfigMap());

        List<AdherenceCodeConfigResponse> responses = adherenceService.getAdherenceConfig();

        Assert.assertEquals(responses.size(), 1);
        Assert.assertEquals(responses.get(0).getCode(), '7');
        Assert.assertEquals(responses.get(0).getCodeName(), "BLANK");
        Mockito.verify(adherenceGlobalConfigMap, Mockito.times(1)).getCodeToConfigMap();
    }

    @Test
    public void getAdherenceConfigForEmptyExistingConfigTest() {
        Mockito.when(adherenceGlobalConfigMap.getCodeToConfigMap()).
                thenReturn(new HashMap<>())
                .thenReturn(getAdherenceCodeConfigMap());

        List<AdherenceCodeConfigResponse> responses = adherenceService.getAdherenceConfig();

        Assert.assertEquals(responses.size(), 1);
        Assert.assertEquals(responses.get(0).getCode(), '7');
        Assert.assertEquals(responses.get(0).getCodeName(), "BLANK");
        Mockito.verify(adherenceGlobalConfigMap, Mockito.times(2)).getCodeToConfigMap();
    }

    @Test
    public void getMonitoringMethodConfigTest() {
        Map<String, Integer> monitoringMethodConfig = getAdherenceMonitoringMethodConfig();
        Mockito.when(adherenceGlobalConfigMap.getMonitoringMethodToIdMap()).thenReturn(monitoringMethodConfig);

        Map<String, Integer> response = adherenceService.getMonitoringMethodConfig();

        Assert.assertEquals(monitoringMethodConfig.size(), response.size());
        Assert.assertEquals(Integer.valueOf(1), response.get("none"));
        Assert.assertEquals(Integer.valueOf(2), response.get("merm"));
        Assert.assertEquals(Integer.valueOf(3), response.get("99dots"));
        Mockito.verify(adherenceGlobalConfigMap, Mockito.times(1)).getMonitoringMethodToIdMap();
    }

    @Test
    public void getMonitoringMethodConfigForEmptyExistingConfigTest() {
        Map<String, Integer> monitoringMethodConfig = getAdherenceMonitoringMethodConfig();
        Mockito.when(adherenceGlobalConfigMap.getMonitoringMethodToIdMap())
                .thenReturn(new HashMap<>())
                .thenReturn(monitoringMethodConfig);

        Map<String, Integer> response = adherenceService.getMonitoringMethodConfig();

        Assert.assertEquals(monitoringMethodConfig.size(), response.size());
        Assert.assertEquals(Integer.valueOf(1), response.get("none"));
        Assert.assertEquals(Integer.valueOf(2), response.get("merm"));
        Assert.assertEquals(Integer.valueOf(3), response.get("99dots"));
        Mockito.verify(adherenceGlobalConfigMap, Mockito.times(2)).getMonitoringMethodToIdMap();
    }

    @Test
    public void getScheduleTypeConfigTest() {
        Map<String, Integer> scheduleTypeConfig = getAdherenceScheduleTypeConfig();
        Mockito.when(adherenceGlobalConfigMap.getScheduleTypeToIdMap()).thenReturn(scheduleTypeConfig);

        Map<String, Integer> response = adherenceService.getScheduleTypeConfig();

        Assert.assertEquals(scheduleTypeConfig.size(), response.size());
        Assert.assertEquals(Integer.valueOf(1), response.get("daily"));
        Assert.assertEquals(Integer.valueOf(2), response.get("weekly"));
        Mockito.verify(adherenceGlobalConfigMap, Mockito.times(1)).getScheduleTypeToIdMap();
    }

    @Test
    public void getScheduleTypeConfigForEmptyExistingConfigTest() {
        Map<String, Integer> scheduleTypeConfig = getAdherenceScheduleTypeConfig();
        Mockito.when(adherenceGlobalConfigMap.getScheduleTypeToIdMap())
                .thenReturn(new HashMap<>())
                .thenReturn(scheduleTypeConfig);

        Map<String, Integer> response = adherenceService.getScheduleTypeConfig();

        Assert.assertEquals(scheduleTypeConfig.size(), response.size());
        Assert.assertEquals(Integer.valueOf(1), response.get("daily"));
        Assert.assertEquals(Integer.valueOf(2), response.get("weekly"));
        Mockito.verify(adherenceGlobalConfigMap, Mockito.times(2)).getScheduleTypeToIdMap();
    }

    @Test
    public void getTagsByDateTest() {
        EpisodeTagResponse episodeTagResponse = new EpisodeTagResponse(
            1L,
            Arrays.asList(
                    new EpisodeTag(1L, "No_Coverage", Utils.toLocalDateTimeWithNull("03-01-2022 18:30:00"), false)
            ),
            Arrays.asList("Coverage")
        );
        ZoneId zoneId = ZoneId.of("Asia/Calcutta");
        Map<LocalDateTime, List<EpisodeTag>> responses =
                adherenceService.getTagsByDate(episodeTagResponse, zoneId);
        Assert.assertEquals(responses.size(), 1);
        Assert.assertEquals(responses.get(Utils.toLocalDateTimeWithNull("04-01-2022 00:00:00")).get(0).getTagName(), "No_Coverage");
    }

    @Test
    public void formatAdherenceStringMonthWiseTest() {
        AdherenceResponse adherenceResponse = new AdherenceResponse(
                Utils.toLocalDateTimeWithNull("02-01-2022 18:30:00"),
                Utils.toLocalDateTimeWithNull("06-01-2022 18:30:00"),
                "654A9"
        );
        EpisodeTagResponse episodeTagResponse = new EpisodeTagResponse(
                1L,
                Arrays.asList(
                        new EpisodeTag(1L, "No_Coverage", Utils.toLocalDateTimeWithNull("03-01-2022 18:30:00"), false)
                ),
                Arrays.asList("Coverage")
        );
        ZoneId zoneId = ZoneId.of("Asia/Calcutta");
        Mockito.when(adherenceGlobalConfigMap.getCodeToConfigMap()).thenReturn(getAdherenceCodeConfigMap());
        List<AdherenceMonthWiseData> monthWiseData = adherenceService.formatAdherenceStringMonthWise(adherenceResponse, episodeTagResponse, zoneId);
        Assert.assertEquals(monthWiseData.size(), 1);
    }

    @Test
    public void validateDispensationTest() {
        List<String> utcDates = Collections.singletonList("05-03-2022 18:30:00");
        Long episodeId = 12345L;
        List<DispensationProduct> productList = Collections.singletonList(new DispensationProduct(1L, "1"));
        DispensationData dispensationData = new DispensationData(1L, 12345L, "04-03-2022 18:30:00", "06-03-2022 18:30:00", productList);
        List<DispensationData> dispensationDataList = Collections.singletonList(dispensationData);
        EntityDispensationResponse entityDispensationResponse = new EntityDispensationResponse(12345L, dispensationDataList);

        List<LocalDateTime> response = adherenceService.validateDispensation(utcDates, episodeId, entityDispensationResponse);

        Assert.assertEquals(1, response.size());
    }

    @Test
    public void validateDispensationEventBeforeStartDateTest() {
        List<String> utcDates = Collections.singletonList("05-03-2022 18:30:00");
        Long episodeId = 12345L;
        List<DispensationProduct> productList = Collections.singletonList(new DispensationProduct(1L, "1"));
        DispensationData dispensationData = new DispensationData(1L, 12345L, "06-03-2022 18:30:00", "06-03-2022 18:30:00", productList);
        List<DispensationData> dispensationDataList = Collections.singletonList(dispensationData);
        EntityDispensationResponse entityDispensationResponse = new EntityDispensationResponse(12345L, dispensationDataList);

        List<LocalDateTime> response = adherenceService.validateDispensation(utcDates, episodeId, entityDispensationResponse);

        Assert.assertEquals(0, response.size());

    }

    @Test
    public void validateDispensationEventAfterEndDateTest() {
        List<String> utcDates = Collections.singletonList("05-03-2022 18:30:00");
        Long episodeId = 12345L;
        List<DispensationProduct> productList = Collections.singletonList(new DispensationProduct(1L, "1"));
        DispensationData dispensationData = new DispensationData(1L, 12345L, "02-03-2022 18:30:00", "04-03-2022 18:30:00", productList);
        List<DispensationData> dispensationDataList = Collections.singletonList(dispensationData);
        EntityDispensationResponse entityDispensationResponse = new EntityDispensationResponse(12345L, dispensationDataList);

        List<LocalDateTime> response = adherenceService.validateDispensation(utcDates, episodeId, entityDispensationResponse);

        Assert.assertEquals(0, response.size());

    }

    @Test(expected = ValidationException.class)
    public void editDosesNoDispensationFailure() {
        EditDosesRequest editDosesRequest = new EditDosesRequest();
        editDosesRequest.setAddDoses(false);
        editDosesRequest.setAdherenceDispensationValidation(false);
        List<String> utcDates = Collections.nCopies(2, "10-10-2021 18:30:00");
        editDosesRequest.setUtcDates(utcDates);
        editDosesRequest.setEpisodeId(1L);

        ResponseEntity<Response<Void>> response = new ResponseEntity<>(new Response<>(false, null), HttpStatus.BAD_REQUEST);

        when(iamService.recordAdherenceForMultipleDates(any())).thenReturn(response);

        adherenceService.editDoses(editDosesRequest);
    }

    @Test
    public void editDosesNoDispensationSuccess() {
        EditDosesRequest editDosesRequest = new EditDosesRequest();
        editDosesRequest.setAddDoses(true);
        editDosesRequest.setAdherenceDispensationValidation(false);
        editDosesRequest.setCode(AdherenceCodeEnum.PATIENT_MANUAL.name());
        editDosesRequest.setMonitoringMethod(AdherenceTypeMappingEnum.NNDLITE.getName());
        List<String> utcDates = Collections.singletonList("10-10-2021 18:30:00");
        editDosesRequest.setUtcDates(utcDates);
        editDosesRequest.setEpisodeId(1L);

        ResponseEntity<Response<Void>> iamResponse = new ResponseEntity<>(new Response<>(true, null), HttpStatus.OK);

        when(iamService.recordAdherenceForMultipleDates(any())).thenReturn(iamResponse);


        List<LocalDateTime> response = adherenceService.editDoses(editDosesRequest);

        Assert.assertEquals("10-10-2021 18:30:00", response.get(0).format(DateTimeFormatter.ofPattern("dd-MM-yyyy HH:mm:ss")));
    }

    @Test
    public void editDosesDispensationSuccess() {
        EditDosesRequest editDosesRequest = new EditDosesRequest();
        editDosesRequest.setAddDoses(true);
        editDosesRequest.setAdherenceDispensationValidation(true);
        editDosesRequest.setCode(AdherenceCodeEnum.PATIENT_MANUAL.name());
        editDosesRequest.setMonitoringMethod(AdherenceTypeMappingEnum.NNDLITE.getName());
        List<String> utcDates = Collections.singletonList("05-03-2022 18:30:00");
        editDosesRequest.setUtcDates(utcDates);
        editDosesRequest.setEpisodeId(12345L);
        List<DispensationProduct> productList = new ArrayList<>();
        productList.add(new DispensationProduct(1L, "1"));
        productList.add(new DispensationProduct(2L, "0"));
        DispensationData dispensationData = new DispensationData(1L, 12345L, "04-03-2022 18:30:00", "06-03-2022 18:30:00", productList);
        List<DispensationData> dispensationDataList = Collections.singletonList(dispensationData);
        EntityDispensationResponse entityDispensationResponse = new EntityDispensationResponse(12345L, dispensationDataList);
        ResponseEntity<Response<EntityDispensationResponse>> response
                = new ResponseEntity<>(
                new Response<>(entityDispensationResponse),
                HttpStatus.OK
        );

        when(dispensationService.getAllDispensationDataForEntity(any(), anyBoolean())).thenReturn(response);
        ResponseEntity<Response<Void>> iamResponse = new ResponseEntity<>(new Response<>(true, null), HttpStatus.OK);

        when(iamService.recordAdherenceForMultipleDates(any())).thenReturn(iamResponse);


        List<LocalDateTime> finalResponse = adherenceService.editDoses(editDosesRequest);

        Assert.assertEquals("05-03-2022 18:30:00", finalResponse.get(0).format(DateTimeFormatter.ofPattern("dd-MM-yyyy HH:mm:ss")));
    }

    @Test(expected = ValidationException.class)
    public void editDosesDispensationEmptyDate() {
        EditDosesRequest editDosesRequest = new EditDosesRequest();
        editDosesRequest.setAdherenceDispensationValidation(true);
        List<String> utcDates = Collections.singletonList("03-03-2022 18:30:00");
        editDosesRequest.setUtcDates(utcDates);
        editDosesRequest.setEpisodeId(12345L);
        List<DispensationProduct> productList = Collections.singletonList(new DispensationProduct(1L, "1"));
        DispensationData dispensationData = new DispensationData(1L, 12345L, "04-03-2022 18:30:00", "06-03-2022 18:30:00", productList);
        List<DispensationData> dispensationDataList = Collections.singletonList(dispensationData);
        EntityDispensationResponse entityDispensationResponse = new EntityDispensationResponse(12345L, dispensationDataList);
        ResponseEntity<Response<EntityDispensationResponse>> response
                = new ResponseEntity<>(
                new Response<>(entityDispensationResponse),
                HttpStatus.OK
        );

        when(dispensationService.getAllDispensationDataForEntity(any(), anyBoolean())).thenReturn(response);

        adherenceService.editDoses(editDosesRequest);
    }

    @Test
    public void updateIamScheduleTest() {
        ResponseEntity<Response<Map<String, Object>>> response = new ResponseEntity<>(
                new Response<>(new HashMap<>()),
                HttpStatus.OK
        );
        when(iamService.updateEntity(any())).thenReturn(response);
        adherenceService.updateIamSchedule(Arrays.asList(
                new DoseDetails(), new DoseDetails()
        ), "2022-12-20 00:00:00", "1", 1L);
        Mockito.verify(iamService, Mockito.times(1)).updateEntity(any());
    }

    @Test(expected = ValidationException.class)
    public void updateIamScheduleWithFailureTest() {
        ResponseEntity<Response<Map<String, Object>>> response = new ResponseEntity<>(
                new Response<>(new HashMap<>(), false),
                HttpStatus.BAD_REQUEST
        );
        when(iamService.updateEntity(any())).thenReturn(response);
        adherenceService.updateIamSchedule(Arrays.asList(
                new DoseDetails(),
                new DoseDetails()
        ), "2022-12-20 00:00:00", "1", 1L);
        Mockito.verify(iamService, Mockito.times(1)).updateEntity(any());
    }

    @Test
    public void registerEntityWithoutStageTransitionData() {
        Long episodeId = 1L;
        adherenceService.registerEntity(episodeId, new HashMap<>(), new HashMap<>(), new HashMap<>());
        Mockito.verify(iamService, Mockito.times(0)).registerEntity(any());
    }

    @Test(expected = ValidationException.class)
    public void registerEntityWithoutStartDate() {
        Long episodeId = 1L;
        Map<String, Object> processedRequestInput =  new HashMap<String, Object>() {{
            put("monitoringMethod", "None");
            put("scheduleType", "Daily");
        }};
        Map<String, Object> stageTransitionResult = getStageTransitionResultData(RuleConstants.IAM_REGISTER_OR_UPDATE);
        ResponseEntity<Response<Map<String, Object>>> response = new ResponseEntity<>(
                new Response<>(new HashMap<>()),
                HttpStatus.OK
        );
        when(iamService.registerEntity(any())).thenReturn(response);
        when(adherenceGlobalConfigMap.getMonitoringMethodToIdMap()).thenReturn(getAdherenceMonitoringMethodConfig());
        when(adherenceGlobalConfigMap.getScheduleTypeToIdMap()).thenReturn(getAdherenceScheduleTypeConfig());
        adherenceService.registerEntity(episodeId, new HashMap<>(), processedRequestInput, stageTransitionResult);
    }

    @Test
    public void registerEntityForMonitoringMethodNone() {
        Long episodeId = 1L;
        Map<String, Object> processedRequestInput =  new HashMap<String, Object>() {{
            put("iamStartDate", "2022-12-20 00:00:00");
            put("monitoringMethod", "None");
            put("scheduleType", "Daily");
        }};
        Map<String, Object> stageTransitionResult = getStageTransitionResultData(RuleConstants.IAM_REGISTER_OR_UPDATE);
        ResponseEntity<Response<Map<String, Object>>> response = new ResponseEntity<>(
                new Response<>(new HashMap<>()),
                HttpStatus.OK
        );
        when(iamService.registerEntity(any())).thenReturn(response);
        when(adherenceGlobalConfigMap.getMonitoringMethodToIdMap()).thenReturn(getAdherenceMonitoringMethodConfig());
        when(adherenceGlobalConfigMap.getScheduleTypeToIdMap()).thenReturn(getAdherenceScheduleTypeConfig());
        adherenceService.registerEntity(episodeId, new HashMap<>(), processedRequestInput, stageTransitionResult);
        Mockito.verify(iamService, Mockito.times(1)).registerEntity(any());
    }

    @Test
    public void registerEntityForMonitoringMethodNNDOTS_withNoMobileData() {
        Long episodeId = 1L;
        Map<String, Object> requestInput =  new HashMap<>();
        Map<String, Object> processedRequestInput =  new HashMap<String, Object>() {{
            put("iamStartDate", "2022-12-20 00:00:00");
            put("monitoringMethod", "99DOTS");
            put("scheduleType", "Daily");
        }};
        Map<String, Object> stageTransitionResult = getStageTransitionResultData(RuleConstants.IAM_REGISTER_OR_UPDATE);
        ResponseEntity<Response<Map<String, Object>>> response = new ResponseEntity<>(
                new Response<>(new HashMap<>()),
                HttpStatus.OK
        );
        when(iamService.registerEntity(any())).thenReturn(response);
        when(adherenceGlobalConfigMap.getMonitoringMethodToIdMap()).thenReturn(getAdherenceMonitoringMethodConfig());
        when(adherenceGlobalConfigMap.getScheduleTypeToIdMap()).thenReturn(getAdherenceScheduleTypeConfig());
        adherenceService.registerEntity(episodeId, requestInput, processedRequestInput, stageTransitionResult);
        Mockito.verify(iamService, Mockito.times(1)).registerEntity(any());
    }

    @Test
    public void registerEntityForMonitoringMethodNNDOTS() {
        Long episodeId = 1L;
        List<Map<String, Object>> mobileList = new ArrayList<>();
        mobileList.add(new HashMap<String, Object>(){{put("number", "1234567891");put("primary", true);}});
        mobileList.add(new HashMap<String, Object>(){{put("number", "");put("primary", false);}});
        mobileList.add(new HashMap<String, Object>(){{put("number", "1234567892");put("primary", false); put("stoppedAt", "1679411320115");}});
        Map<String, Object> requestInput =  new HashMap<String, Object>() {{
            put("countryPrefix" , "91");
            put("mobileList", mobileList);
        }};
        Map<String, Object> processedRequestInput =  new HashMap<String, Object>() {{
            put("iamStartDate", "2022-12-20 00:00:00");
            put("monitoringMethod", "99DOTS");
            put("scheduleType", "Daily");
        }};
        Map<String, Object> stageTransitionResult = getStageTransitionResultData(RuleConstants.IAM_REGISTER_OR_UPDATE);
        ResponseEntity<Response<Map<String, Object>>> response = new ResponseEntity<>(
                new Response<>(new HashMap<>()),
                HttpStatus.OK
        );
        when(iamService.registerEntity(any())).thenReturn(response);
        when(adherenceGlobalConfigMap.getMonitoringMethodToIdMap()).thenReturn(getAdherenceMonitoringMethodConfig());
        when(adherenceGlobalConfigMap.getScheduleTypeToIdMap()).thenReturn(getAdherenceScheduleTypeConfig());
        adherenceService.registerEntity(episodeId, requestInput, processedRequestInput, stageTransitionResult);
        Mockito.verify(iamService, Mockito.times(1)).registerEntity(any());
    }

    @Test
    public void registerEntityForMonitoringMethodMerm() {
        Long episodeId = 1L;
        Map<String, Object> requestInput =  new HashMap<>();
        Map<String, Object> processedRequestInput =  new HashMap<String, Object>() {{
            put("iamStartDate", "2022-12-20 00:00:00");
            put("monitoringMethod", "MERM");
            put("scheduleType", "Daily");
        }};
        Map<String, Object> stageTransitionResult = getStageTransitionResultData(RuleConstants.IAM_REGISTER_OR_UPDATE);
        ResponseEntity<Response<Map<String, Object>>> response = new ResponseEntity<>(
                new Response<>(new HashMap<>()),
                HttpStatus.OK
        );
        when(iamService.registerEntity(any())).thenReturn(response);
        when(adherenceGlobalConfigMap.getMonitoringMethodToIdMap()).thenReturn(getAdherenceMonitoringMethodConfig());
        when(adherenceGlobalConfigMap.getScheduleTypeToIdMap()).thenReturn(getAdherenceScheduleTypeConfig());
        adherenceService.registerEntity(episodeId, requestInput, processedRequestInput, stageTransitionResult);
        Mockito.verify(iamService, Mockito.times(1)).registerEntity(any());
    }

    private EpisodeDto getEpisodeDto(String monitoringMethod) {
        EpisodeDto episodeDto = new EpisodeDto();
        Map<String, Object> stageData = new HashMap<>();
        stageData.put(IAMField.MONITORING_METHOD.getFieldName(), monitoringMethod);
        episodeDto.setStageData(stageData);
        return episodeDto;
    }

    @Test
    public void updateEntityWithNoStageTransition_noChangeInMonitoringMethod_caseOneTest() {
        EpisodeTransitionDetails episodeTransitionDetails = new EpisodeTransitionDetails(getEpisodeDto("MERM"), getEpisodeDto("MERM"));
        Map<String, Object> requestInput =  new HashMap<>();
        Map<String, Object> processedRequestInput =  new HashMap<String, Object>() {{
            put("monitoringMethod", "MERM");
        }};
        Map<String, Object> stageTransitionResult = new HashMap<>();

        adherenceService.updateEntity(episodeTransitionDetails, requestInput, processedRequestInput, stageTransitionResult);

        Mockito.verify(iamService, Mockito.times(0)).closeCase(any(), any(), any(), any());
        Mockito.verify(iamService, Mockito.times(0)).registerEntity(any());
        Mockito.verify(episodeLogService, Mockito.times(0)).save(any());
    }

    @Test
    public void updateEntityWithNoStageTransition_noChangeInMonitoringMethod_caseTwoTest() {
        EpisodeTransitionDetails episodeTransitionDetails = new EpisodeTransitionDetails(getEpisodeDto("MERM"), getEpisodeDto("MERM"));
        Map<String, Object> requestInput =  new HashMap<>();
        Map<String, Object> processedRequestInput =  new HashMap<String, Object>() {{
            put("monitoringMethod", null);
        }};
        Map<String, Object> stageTransitionResult = new HashMap<>();

        adherenceService.updateEntity(episodeTransitionDetails, requestInput, processedRequestInput, stageTransitionResult);

        Mockito.verify(iamService, Mockito.times(0)).closeCase(any(), any(), any(), any());
        Mockito.verify(iamService, Mockito.times(0)).registerEntity(any());
        Mockito.verify(episodeLogService, Mockito.times(0)).save(any());
    }

    @Test
    public void updateEntityWithNoStageTransition_changeInMonitoringMethod_previousNullTest() {
        EpisodeTransitionDetails episodeTransitionDetails = new EpisodeTransitionDetails(getEpisodeDto(null), getEpisodeDto("99DOTS"));
        Map<String, Object> requestInput =  new HashMap<>();
        Map<String, Object> processedRequestInput =  new HashMap<String, Object>() {{
            put("monitoringMethod", "99DOTS");
            put("iamStartDate", "2022-12-20 00:00:00");
            put("scheduleType", "Daily");
        }};
        Map<String, Object> stageTransitionResult = new HashMap<>();
        when(iamService.registerEntity(any())).thenReturn(new ResponseEntity<>(new Response<>(new HashMap<>()), HttpStatus.OK));
        when(adherenceGlobalConfigMap.getMonitoringMethodToIdMap()).thenReturn(getAdherenceMonitoringMethodConfig());
        when(adherenceGlobalConfigMap.getScheduleTypeToIdMap()).thenReturn(getAdherenceScheduleTypeConfig());

        adherenceService.updateEntity(episodeTransitionDetails, requestInput, processedRequestInput, stageTransitionResult);

        Mockito.verify(iamService, Mockito.times(1)).registerEntity(any());
    }


    @Test
    public void updateEntityWithNoStageTransition_changeInMonitoringMethod_previousNotNullTest() {
        EpisodeTransitionDetails episodeTransitionDetails = new EpisodeTransitionDetails(getEpisodeDto("MERM"), getEpisodeDto("99DOTS"));
        Map<String, Object> requestInput =  new HashMap<>();
        Map<String, Object> processedRequestInput =  new HashMap<String, Object>() {{
            put("monitoringMethod", "99DOTS");
            put("iamStartDate", "2022-12-20 00:00:00");
            put("scheduleType", "Daily");

        }};
        Map<String, Object> stageTransitionResult = new HashMap<>();

        when(iamService.registerEntity(any())).thenReturn(new ResponseEntity<>(new Response<>(new HashMap<>()), HttpStatus.OK));
        when(iamService.closeCase(any(), any(), any(), any())).thenReturn(new ResponseEntity<>(new Response<>("stop success", true), HttpStatus.OK));
        when(episodeLogService.save(any())).thenReturn(new ArrayList<>());
        when(adherenceGlobalConfigMap.getMonitoringMethodToIdMap()).thenReturn(getAdherenceMonitoringMethodConfig());
        when(adherenceGlobalConfigMap.getScheduleTypeToIdMap()).thenReturn(getAdherenceScheduleTypeConfig());

        adherenceService.updateEntity(episodeTransitionDetails, requestInput, processedRequestInput, stageTransitionResult);

        Mockito.verify(iamService, Mockito.times(1)).closeCase(any(), any(), any(), any());
        Mockito.verify(iamService, Mockito.times(1)).registerEntity(any());
        Mockito.verify(episodeLogService, Mockito.times(1)).save(any());
    }

    @Test
    public void updateEntityWithStageTransitionToClose_withDeleteTest() {
        EpisodeTransitionDetails episodeTransitionDetails = new EpisodeTransitionDetails(getEpisodeDto("MERM"), getEpisodeDto("MERM"));
        Map<String, Object> requestInput =  new HashMap<>();
        Map<String, Object> processedRequestInput =  new HashMap<String, Object>() {{
            put(IAMField.END_DATE.getFieldName(), "2022-12-20 00:00:00");
        }};
        Map<String, Object> stageTransitionResult =   new HashMap<String, Object>() {{
            put(RuleConstants.ACTION_ON_IAM, "close");
        }};
        when(iamService.closeCase(any(), any(), any(), any())).thenReturn(new ResponseEntity<>(new Response<>("stop success", true), HttpStatus.OK));
        when(adherenceGlobalConfigMap.getMonitoringMethodToIdMap()).thenReturn(getAdherenceMonitoringMethodConfig());
        when(adherenceGlobalConfigMap.getScheduleTypeToIdMap()).thenReturn(getAdherenceScheduleTypeConfig());

        adherenceService.updateEntity(episodeTransitionDetails, requestInput, processedRequestInput, stageTransitionResult);

        Mockito.verify(iamService, Mockito.times(1)).closeCase(any(), any(), any(), any());
    }

    @Test
    public void updateEntityWithStageTransitionToClose_withoutDeleteTest() {
        EpisodeTransitionDetails episodeTransitionDetails = new EpisodeTransitionDetails(getEpisodeDto("MERM"), getEpisodeDto("MERM"));
        Map<String, Object> requestInput = new HashMap<String, Object>() {{
            put(IAMField.DELETE_IF_REQUIRED.getFieldName(), true);
        }};
        Map<String, Object> processedRequestInput =  new HashMap<String, Object>() {{
            put(IAMField.END_DATE.getFieldName(), "2022-12-20 00:00:00");
        }};
        Map<String, Object> stageTransitionResult =   new HashMap<String, Object>() {{
            put(RuleConstants.ACTION_ON_IAM, "close");
        }};
        when(iamService.closeCase(any(), any(), any(), any())).thenReturn(new ResponseEntity<>(new Response<>("stop success", true), HttpStatus.OK));
        doNothing().when(episodeLogService).syncLogs(any(), any());
        doNothing().when(episodeTagService).deleteAllTagsAfterEndDate(any(), any());
        when(adherenceGlobalConfigMap.getMonitoringMethodToIdMap()).thenReturn(getAdherenceMonitoringMethodConfig());
        when(adherenceGlobalConfigMap.getScheduleTypeToIdMap()).thenReturn(getAdherenceScheduleTypeConfig());

        adherenceService.updateEntity(episodeTransitionDetails, requestInput, processedRequestInput, stageTransitionResult);

        Mockito.verify(iamService, Mockito.times(1)).closeCase(any(), any(), any(), any());
        Mockito.verify(episodeLogService, Mockito.times(1)).syncLogs(any(), any());
        Mockito.verify(episodeTagService, Mockito.times(1)).deleteAllTagsAfterEndDate(any(), any());
    }

    @Test
    public void updateEntityWithStageTransitionToReopenTest() {
        EpisodeTransitionDetails episodeTransitionDetails = new EpisodeTransitionDetails(getEpisodeDto("99DOTS"), getEpisodeDto("99DOTS"));
        Map<String, Object> requestInput =  new HashMap<>();
        Map<String, Object> processedRequestInput =  new HashMap<String, Object>() {{
            put(IAMField.END_DATE.getFieldName(), "2022-12-20 00:00:00");
            put("iamStartDate", "2022-12-20 00:00:00");
            put("scheduleType", "Daily");
        }};
        Map<String, Object> stageTransitionResult =   new HashMap<String, Object>() {{
            put(RuleConstants.ACTION_ON_IAM, "reopen");
        }};
        when(iamService.reopenEntity(any())).thenReturn(new ResponseEntity<>(new Response<>(new HashMap<>(), true), HttpStatus.OK));
        when(adherenceGlobalConfigMap.getMonitoringMethodToIdMap()).thenReturn(getAdherenceMonitoringMethodConfig());
        when(adherenceGlobalConfigMap.getScheduleTypeToIdMap()).thenReturn(getAdherenceScheduleTypeConfig());

        adherenceService.updateEntity(episodeTransitionDetails, requestInput, processedRequestInput, stageTransitionResult);

        Mockito.verify(iamService, Mockito.times(1)).reopenEntity(any());
    }

    @Test
    public void getAdherenceDetailsWithNullEpisodeIdTest() {
        Map<String, Object> adherenceResponse = adherenceService.getAdherenceDetails(null);

        Assert.assertTrue(adherenceResponse.isEmpty());
        Mockito.verify(iamService, Mockito.times(0)).getAdherence(anyLong(), any());
    }

    @Test
    public void getAdherenceDetailsTest() {
        Long episodeId = 1L;
        ResponseEntity<Response<AdherenceResponse>> response = new ResponseEntity<>(
                new Response<>(new AdherenceResponse()),
                HttpStatus.OK
        );
        when(iamService.getAdherence(eq(episodeId), any())).thenReturn(response);

        Map<String, Object> adherenceResponse = adherenceService.getAdherenceDetails(episodeId);

        Assert.assertFalse(adherenceResponse.isEmpty());
        Mockito.verify(iamService, Mockito.times(1)).getAdherence(eq(episodeId), any());
    }

    @Test
    public void getAdherenceDetailsWithInvalidResponseTest() {
        Long episodeId = 1L;
        ResponseEntity<Response<AdherenceResponse>> response = new ResponseEntity<>(
                new Response<>(null, false),
                HttpStatus.OK
        );
        when(iamService.getAdherence(eq(episodeId), any())).thenReturn(response);

        Map<String, Object> adherenceResponse = adherenceService.getAdherenceDetails(episodeId);

        Assert.assertTrue(adherenceResponse.isEmpty());
        Mockito.verify(iamService, Mockito.times(1)).getAdherence(eq(episodeId), any());
    }

    @Test
    public void testEditDosesBulkWithAdherenceDispensationValidation() {
        // arrange
        EditDoseBulkRequest request = new EditDoseBulkRequest();
        List<EditDosesRequest> editDosesRequests = new ArrayList<>();
        EditDosesRequest editDosesRequest = new EditDosesRequest();
        editDosesRequest.setAdherenceDispensationValidation(true);
        editDosesRequest.setEpisodeId(1L);
        editDosesRequest.setUtcDates(Collections.singletonList("2023-04-11T12:00:00"));
        editDosesRequest.setCode("MISSED");
        editDosesRequest.setMonitoringMethod("99DOTS");
        editDosesRequest.setAddDoses(false);
        editDosesRequest.setIamEntityId("entity1");
        editDosesRequests.add(editDosesRequest);
        request.setEditDosesRequestList(editDosesRequests);

        EntityDispensationResponse entityDispensationResponse = new EntityDispensationResponse(1L, new ArrayList<>());
        ResponseEntity<Response<EntityDispensationResponse>> dispensationServiceResponse = new ResponseEntity<>(new Response<>(true, entityDispensationResponse), HttpStatus.OK);
        Mockito.when(dispensationService.getAllDispensationDataForEntity(eq(1L), eq(false))).thenReturn(dispensationServiceResponse);

        ResponseEntity<Response<Void>> iamServiceResponse = new ResponseEntity<>(new Response<>(true, null, ""), HttpStatus.OK);
        Mockito.when(iamService.recordAdherenceBulk(Mockito.any(RecordAdherenceBulkRequest.class))).thenReturn(iamServiceResponse);

        doReturn(Arrays.asList(Utils.convertStringToDateNew("2023-04-11 12:00:00"))).when(adherenceService).validateDispensation(any(), any(), any());
        // act
        Map<String, List<LocalDateTime>> result = adherenceService.editDosesBulk(request);

        // assert
        Assert.assertNotNull(result);
        Assert.assertEquals(1, result.size());
        Assert.assertTrue(result.containsKey("entity1"));
        Assert.assertEquals(1, result.get("entity1").size());
        Assert.assertEquals(LocalDateTime.of(2023, 4, 11, 12, 0), result.get("entity1").get(0));
    }

}
