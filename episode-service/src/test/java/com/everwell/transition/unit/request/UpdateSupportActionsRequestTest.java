package com.everwell.transition.unit.request;

import com.everwell.transition.BaseTest;
import com.everwell.transition.model.db.EpisodeLog;
import com.everwell.transition.model.request.episodelogs.UpdateSupportActionsDataRequest;
import org.junit.Assert;
import org.junit.Test;

import java.time.LocalDateTime;

public class UpdateSupportActionsRequestTest extends BaseTest {

    @Test
    public void testEqual() {
        UpdateSupportActionsDataRequest request1 = new UpdateSupportActionsDataRequest(1L, 1L, null, null, null, null, null, null);
        EpisodeLog log1 = new EpisodeLog(1L, 1L, null, null, null, null, null, null, null);

        Assert.assertTrue(request1.equal(log1));

        LocalDateTime now = LocalDateTime.now();

        UpdateSupportActionsDataRequest request2 = new UpdateSupportActionsDataRequest(1L, 1L, "category", "subCategory", 1L, "action", "comments", now);
        EpisodeLog log2 = new EpisodeLog(1L, 1L, null, 1L, "category", "subCategory", "action", "comments", now);

        Assert.assertTrue(request2.equal(log2));

        UpdateSupportActionsDataRequest request3 = new UpdateSupportActionsDataRequest(1L, 1L, null, null, null, null, null, null);
        EpisodeLog log3 = new EpisodeLog(1L, 1L, null, 1L, "category", "subCategory", "action", "comments", now);

        Assert.assertFalse(request3.equal(log3));

        UpdateSupportActionsDataRequest request4 = new UpdateSupportActionsDataRequest(1L, 1L, "category", "subCategory", 1L, "action", "comments", now);
        EpisodeLog log4 = new EpisodeLog(1L, 1L, null, null, null, null, null, null, null);

        Assert.assertFalse(request4.equal(log4));
    }
}
