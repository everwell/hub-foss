package com.everwell.transition.unit.consumers;

import com.everwell.transition.BaseTest;
import com.everwell.transition.constants.Constants;
import com.everwell.transition.constants.FieldConstants;
import com.everwell.transition.consumers.EpisodeDoseReminderConsumer;
import com.everwell.transition.elasticsearch.ElasticConstants;
import com.everwell.transition.elasticsearch.indices.EpisodeIndex;
import com.everwell.transition.exceptions.ValidationException;
import com.everwell.transition.model.dto.AdherenceData;
import com.everwell.transition.model.dto.EpisodeSearchResult;
import com.everwell.transition.model.request.episodesearch.EpisodeSearchRequest;
import com.everwell.transition.model.response.AdherenceResponse;
import com.everwell.transition.model.response.AllAdherenceResponse;
import com.everwell.transition.model.response.Response;
import com.everwell.transition.service.EpisodeService;
import com.everwell.transition.service.IAMService;
import com.everwell.transition.service.NotificationService;
import com.everwell.transition.utils.Utils;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.core.MessageProperties;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.io.IOException;
import java.time.LocalDate;
import java.time.ZoneOffset;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

public class EpisodeDoseReminderConsumerTest extends BaseTest {

    @Spy
    @InjectMocks
    EpisodeDoseReminderConsumer episodeDoseReminderConsumer;

    @Mock
    IAMService iamService;

    @Mock
    EpisodeService episodeService;

    @Mock
    NotificationService notificationService;

    @Test
    public void consumerTest() throws IOException {
        String request = getRequestData();
        EpisodeIndex episodeIndex = new EpisodeIndex();
        episodeIndex.setId("1");
        HashMap<String, Object> stageData = new HashMap<String, Object>() {{
            put(FieldConstants.EPISODE_FIELD_PHONE_NUMBER, "9999999999");
            put(FieldConstants.EPISODE_FIELD_LAST_DOSAGE, "2023-04-01 00:00:00");
        }};
        episodeIndex.setStageData(stageData);
        EpisodeSearchResult episodeSearchResult = new EpisodeSearchResult(1, Collections.singletonList(episodeIndex), new HashMap<>());
        doReturn(episodeSearchResult).when(episodeService).elasticSearch(any(EpisodeSearchRequest.class));
        episodeDoseReminderConsumer.consume(new Message(request.getBytes(), new MessageProperties()));
        verify(notificationService, times(1)).sendSmsToIns(any(), any(), any(), any());
    }

    @Test
    public void consumerTestElasticBatch() throws IOException {
        String request = getRequestData();
        EpisodeIndex episodeIndex = new EpisodeIndex();
        episodeIndex.setId("1");
        HashMap<String, Object> stageData = new HashMap<String, Object>() {{
            put(FieldConstants.EPISODE_FIELD_PHONE_NUMBER, "9999999999");
            put(FieldConstants.EPISODE_FIELD_LAST_DOSAGE, "2023-04-01 00:00:00");
        }};
        episodeIndex.setStageData(stageData);
        List<EpisodeIndex> episodeIndexList = new ArrayList<>(Collections.nCopies(ElasticConstants.ELASTIC_BATCH_SIZE + 1, episodeIndex));
        EpisodeSearchResult episodeSearchResult = new EpisodeSearchResult(1, episodeIndexList, new HashMap<>());
        EpisodeSearchResult episodeSearchResult1 = new EpisodeSearchResult(1, Collections.singletonList(episodeIndex), new HashMap<>());
        when(episodeService.elasticSearch(any(EpisodeSearchRequest.class))).thenReturn(episodeSearchResult).thenReturn(episodeSearchResult1);
        episodeDoseReminderConsumer.consume(new Message(request.getBytes(), new MessageProperties()));
        verify(notificationService, times(1)).sendSmsToIns(any(), any(), any(), any());
    }

    @Test
    public void consumerTestEpisodeTookDose() throws IOException {
        String request = getRequestData();
        EpisodeIndex episodeIndex = new EpisodeIndex();
        episodeIndex.setId("1");
        String lastDosage = Utils.getFormattedDateNew(LocalDate.now(ZoneOffset.UTC).atStartOfDay().minusHours(1), Constants.DATE_FORMAT);
        HashMap<String, Object> stageData = new HashMap<String, Object>() {{
            put(FieldConstants.EPISODE_FIELD_PHONE_NUMBER, "9999999999");
            put(FieldConstants.EPISODE_FIELD_LAST_DOSAGE, lastDosage);
        }};
        episodeIndex.setStageData(stageData);
        EpisodeSearchResult episodeSearchResult = new EpisodeSearchResult(1, Collections.singletonList(episodeIndex), new HashMap<>());
        doReturn(episodeSearchResult).when(episodeService).elasticSearch(any(EpisodeSearchRequest.class));
        episodeDoseReminderConsumer.consume(new Message(request.getBytes(), new MessageProperties()));
        verify(notificationService, times(0)).sendSmsToIns(any(), any(), any(), any());
    }

    @Test
    public void consumerTestInvalidPhoneNumber() throws IOException {
        String request = getRequestData();
        EpisodeIndex episodeIndex = new EpisodeIndex();
        episodeIndex.setId("1");
        HashMap<String, Object> stageData = new HashMap<String, Object>() {{
            put(FieldConstants.EPISODE_FIELD_PHONE_NUMBER, "19999999999");
            put(FieldConstants.EPISODE_FIELD_LAST_DOSAGE, "2023-04-01 00:00:00");
        }};
        episodeIndex.setStageData(stageData);
        EpisodeSearchResult episodeSearchResult = new EpisodeSearchResult(1, Collections.singletonList(episodeIndex), new HashMap<>());
        doReturn(episodeSearchResult).when(episodeService).elasticSearch(any(EpisodeSearchRequest.class));
        episodeDoseReminderConsumer.consume(new Message(request.getBytes(), new MessageProperties()));
        verify(notificationService, times(0)).sendSmsToIns(any(), any(), any(), any());
    }

    @Test
    public void consumerTestNullEpisodeResult() throws IOException {
        String request = getRequestData();
        EpisodeIndex episodeIndex = new EpisodeIndex();
        episodeIndex.setId("1");
        HashMap<String, Object> stageData = new HashMap<String, Object>() {{
            put(FieldConstants.EPISODE_FIELD_PHONE_NUMBER, "9999999999");
            put(FieldConstants.EPISODE_FIELD_LAST_DOSAGE, "2023-04-01 00:00:00");
        }};
        episodeIndex.setStageData(stageData);
        EpisodeSearchResult episodeSearchResult = new EpisodeSearchResult(1, Collections.singletonList(episodeIndex), new HashMap<>());
        when(episodeService.elasticSearch(any(EpisodeSearchRequest.class))).thenReturn(episodeSearchResult);
        when(episodeService.elasticSearch(any(EpisodeSearchRequest.class))).thenReturn(new EpisodeSearchResult());
        episodeDoseReminderConsumer.consume(new Message(request.getBytes(), new MessageProperties()));
        verify(notificationService, times(0)).sendSmsToIns(any(), any(), any(), any());
    }

    @Test
    public void consumerTestFilterEpisodeForXDays() throws IOException {
        String request = getRequestDataExcludingTodayDose();
        EpisodeIndex episodeIndex = new EpisodeIndex();
        episodeIndex.setId("1");
        HashMap<String, Object> stageData = new HashMap<String, Object>() {{
            put(FieldConstants.EPISODE_FIELD_PHONE_NUMBER, "9999999999");
            put(FieldConstants.EPISODE_FIELD_LAST_DOSAGE, "2023-04-01 00:00:00");
        }};
        episodeIndex.setStageData(stageData);
        EpisodeSearchResult episodeSearchResult = new EpisodeSearchResult(1, Collections.singletonList(episodeIndex), new HashMap<>());
        doReturn(episodeSearchResult).when(episodeService).elasticSearch(any(EpisodeSearchRequest.class));

        ResponseEntity<Response<AllAdherenceResponse>> response
                = new ResponseEntity<>(
                new Response<>(getBulkAdherenceResponse()),
                HttpStatus.OK
        );
        when(iamService.getAdherenceBulk(any())).thenReturn(response);

        episodeDoseReminderConsumer.consume(new Message(request.getBytes(), new MessageProperties()));
        verify(notificationService, times(0)).sendSmsToIns(any(), any(), any(), any());
    }

    @Test
    public void consumerTestFilterEpisodeForXDaysOneConsecutiveMissedDose() throws IOException {
        String request = getRequestDataExcludingTodayDose();
        EpisodeIndex episodeIndex = new EpisodeIndex();
        episodeIndex.setId("1");
        HashMap<String, Object> stageData = new HashMap<String, Object>() {{
            put(FieldConstants.EPISODE_FIELD_PHONE_NUMBER, "9999999999");
            put(FieldConstants.EPISODE_FIELD_LAST_DOSAGE, "2023-04-01 00:00:00");
        }};
        episodeIndex.setStageData(stageData);
        EpisodeSearchResult episodeSearchResult = new EpisodeSearchResult(1, Collections.singletonList(episodeIndex), new HashMap<>());
        doReturn(episodeSearchResult).when(episodeService).elasticSearch(any(EpisodeSearchRequest.class));

        AllAdherenceResponse allAdherenceResponse = getBulkAdherenceResponse();
        allAdherenceResponse.getAdherenceResponseList().get(0).setConsecutiveMissedDosesFromYesterday(1);
        ResponseEntity<Response<AllAdherenceResponse>> response
                = new ResponseEntity<>(
                new Response<>(allAdherenceResponse),
                HttpStatus.OK
        );
        when(iamService.getAdherenceBulk(any())).thenReturn(response);

        episodeDoseReminderConsumer.consume(new Message(request.getBytes(), new MessageProperties()));
        verify(notificationService, times(1)).sendSmsToIns(any(), any(), any(), any());
    }

    @Test
    public void consumerTestFilterEpisodeForXDaysBulkAdherenceNull() throws IOException {
        String request = getRequestDataExcludingTodayDose();
        EpisodeIndex episodeIndex = new EpisodeIndex();
        episodeIndex.setId("1");
        HashMap<String, Object> stageData = new HashMap<String, Object>() {{
            put(FieldConstants.EPISODE_FIELD_PHONE_NUMBER, "9999999999");
            put(FieldConstants.EPISODE_FIELD_LAST_DOSAGE, "2023-04-01 00:00:00");
        }};
        episodeIndex.setStageData(stageData);
        EpisodeSearchResult episodeSearchResult = new EpisodeSearchResult(1, Collections.singletonList(episodeIndex), new HashMap<>());
        doReturn(episodeSearchResult).when(episodeService).elasticSearch(any(EpisodeSearchRequest.class));

        ResponseEntity<Response<AllAdherenceResponse>> response
                = new ResponseEntity<>(
                new Response<>(null),
                HttpStatus.OK
        );
        when(iamService.getAdherenceBulk(any())).thenReturn(response);

        episodeDoseReminderConsumer.consume(new Message(request.getBytes(), new MessageProperties()));
        verify(notificationService, times(0)).sendSmsToIns(any(), any(), any(), any());
    }

    private String getRequestData() {
        return "{\"episodeSearchRequest\":{\"size\":1,\"page\":0,\"fieldsToShow\":[\"id\",\"primaryPhoneNumber\",\"lastDosage\"],\"typeOfEpisode\":[\"IndiaTbPublic\",\"IndiaTbPrivate\",\"PublicPrivatePartnership\"],\"sortKey\":\"id\",\"sortDirection\":\"ASC\",\"agg\":null,\"search\":{\"must\":{\"clientId\":1,\"deploymentCode\":\"ASCETH\"},\"mustNot\":null},\"requestIdentifier\":null,\"count\":false},\"deploymentCode\":\"ASCETH\",\"phonePrefix\":\"+123\",\"clientId\":1,\"phoneNumberValidationRegex\":\"^[1-9][0-9]{9}$\",\"days\":0,\"excludingTodayDose\":false,\"notificationTriggerDto\":{\"hierarchyId\":2,\"vendorId\":1,\"templateId\":1,\"triggerId\":1,\"functionName\":\"episodeLastDoseReminder\",\"notificationType\":\"\",\"mandatory\":false,\"defaultConsent\":true,\"isDefaultTime\":true,\"timeOfSms\":\"10:00:00\",\"templateIds\":[]},\"eventName\":\"asceth-episode-morning-reminder\"}";
    }

    private String getRequestDataExcludingTodayDose() {
        return "{\"episodeSearchRequest\":{\"size\":1,\"page\":0,\"fieldsToShow\":[\"id\",\"primaryPhoneNumber\",\"lastDosage\"],\"typeOfEpisode\":[\"IndiaTbPublic\",\"IndiaTbPrivate\",\"PublicPrivatePartnership\"],\"sortKey\":\"id\",\"sortDirection\":\"ASC\",\"agg\":null,\"search\":{\"must\":{\"clientId\":1,\"deploymentCode\":\"ASCETH\"},\"mustNot\":null},\"requestIdentifier\":null,\"count\":false},\"deploymentCode\":\"ASCETH\",\"phonePrefix\":\"+123\",\"clientId\":1,\"phoneNumberValidationRegex\":\"^[1-9][0-9]{9}$\",\"days\":1,\"excludingTodayDose\":true,\"notificationTriggerDto\":{\"hierarchyId\":2,\"vendorId\":1,\"templateId\":1,\"triggerId\":1,\"functionName\":\"episodeLastDoseReminder\",\"notificationType\":\"\",\"mandatory\":false,\"defaultConsent\":true,\"isDefaultTime\":true,\"timeOfSms\":\"10:00:00\",\"templateIds\":[]},\"eventName\":\"asceth-episode-morning-reminder\"}";
    }

    private AllAdherenceResponse getBulkAdherenceResponse() {
        List<AdherenceData> adherenceDataList = new ArrayList<>();
        AdherenceData adherenceData = new AdherenceData();
        adherenceData.setStartDate(Utils.toLocalDateTimeWithNull("18-01-2019 18:30:00"));
        adherenceDataList.add(adherenceData);
        AdherenceResponse adherenceResponse = new AdherenceResponse("1", adherenceDataList, "666666", Utils.toLocalDateTimeWithNull("01-01-2022 01:00:00"), 0, 0, 0, null, null, null, 0, 0, new ArrayList<>(), "MERM", Utils.toLocalDateTimeWithNull("01-01-2022 01:00:00"));
        List<AdherenceResponse> allEntityResponseList = new ArrayList<>();
        allEntityResponseList.add(adherenceResponse);
        return new AllAdherenceResponse(allEntityResponseList);
    }
}
