package com.everwell.transition.unit.controller;

import com.everwell.transition.BaseTest;
import com.everwell.transition.constants.Constants;
import com.everwell.transition.controller.SSOController;
import com.everwell.transition.exceptions.CustomExceptionHandler;
import com.everwell.transition.model.response.Response;
import com.everwell.transition.service.SSOService;
import com.everwell.transition.utils.CacheUtils;
import com.everwell.transition.utils.Utils;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.ValueOperations;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.util.HashMap;
import java.util.Map;

import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

public class SSOControllerTest extends BaseTest {

    private MockMvc mockMvc;

    @InjectMocks
    CacheUtils cacheUtils;

    @Spy
    @InjectMocks
    private SSOController ssoController;

    @Mock
    SSOService ssoService;

    @Mock
    RedisTemplate<String, Object> redisTemplate;

    @Mock
    private ValueOperations<String, Object> valueOperations;

    @Before
    public void setup() {
        mockMvc = MockMvcBuilders.standaloneSetup(ssoController)
                .setControllerAdvice(new CustomExceptionHandler())
                .build();
        when(redisTemplate.opsForValue()).thenReturn(valueOperations);
        doNothing().when(valueOperations).set(anyString(), anyString(), anyLong(), any());
    }

    @Test
    public void loginTest() throws Exception {
        String uri = "/v1/sso/login";
        Long platformId=2L;
        Map<String, Object> loginRequest = new HashMap<>();
        Map<String, Object> loginResponse = new HashMap<>();
        loginResponse.put("sessionId", "1");
        ResponseEntity<Response<Map<String, Object>>> response
                = new ResponseEntity<>(
                new Response<>(loginResponse),
                HttpStatus.OK
        );

        when(ssoService.login(any(),any())).thenReturn(response);

        mockMvc
                .perform(
                        MockMvcRequestBuilders
                                .post(uri)
                                .content(Utils.asJsonString(loginRequest))
                                .contentType(MediaType.APPLICATION_JSON)
                                .accept(MediaType.APPLICATION_JSON_VALUE)
                                .header(Constants.X_PLATFORM_ID,platformId)
                )
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.data.sessionId").value("1"));
    }

    @Test
    public void logoutTest() throws Exception {
        String uri = "/v1/sso/logout";
        Map<String, Object>  logoutRequest = new HashMap<>();

        ResponseEntity<Response<Void>> response
                = new ResponseEntity<>(
                  new Response<>(),
                  HttpStatus.OK
        );
        when (ssoService.logout(any())).thenReturn(response);

        mockMvc
                .perform(
                        MockMvcRequestBuilders
                                .post(uri)
                                .content(Utils.asJsonString(logoutRequest))
                                .contentType(MediaType.APPLICATION_JSON)
                                .accept(MediaType.APPLICATION_JSON_VALUE)
                                .header(Constants.AUTHORIZATION, "Bearer Asadasdas")
                )
                .andExpect(status().isOk());
    }

    @Test
    public void refreshDeviceIdAndLastActivityDateTest() throws Exception {
        String uri = "/v1/sso/device-id";
        Map<String, Object> updateDeviceIdRequest = new HashMap<>();
        ResponseEntity<Response<Void>> response
                = new ResponseEntity<>(
                new Response<>(),
                HttpStatus.OK
        );
        when (ssoService.refreshDeviceIdAndLastActivityDate(any())).thenReturn(response);

        mockMvc
                .perform(
                        MockMvcRequestBuilders
                                .put(uri)
                                .content(Utils.asJsonString(updateDeviceIdRequest))
                                .contentType(MediaType.APPLICATION_JSON)
                                .accept(MediaType.APPLICATION_JSON_VALUE)
                )
                .andExpect(status().isOk());
    }

}


