package com.everwell.transition.unit;

import com.everwell.transition.BaseTest;
import com.everwell.transition.model.Rule;
import com.everwell.transition.model.RuleNamespace;
import com.everwell.transition.model.db.Rules;
import com.everwell.transition.repositories.RulesRepository;
import com.everwell.transition.service.impl.WeightBandInferenceEngine;
import org.junit.Assert;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static org.mockito.Mockito.when;

public class WeightBandInferenceEngineTest extends BaseTest {

    @Spy
    @InjectMocks
    private WeightBandInferenceEngine weightBandInferenceEngine;

    @Mock
    private RulesRepository rulesRepository;

    @Test
    public void getRuleNameSpace_correctName() {
        Assert.assertEquals(
                weightBandInferenceEngine.getRuleNamespace(),
                RuleNamespace.WEIGHT_BAND.name()
        );
    }

    @Test
    public void getRuleNameSpace() {
        Long clientId = 29L;
        List<Rules> rules = Collections.singletonList(
                new Rules(1L, RuleNamespace.WEIGHT_BAND.name(), "test", "testAction", 0, "Test Weight Band Rule", null, null, clientId));
        when(rulesRepository.findAllByRuleNamespaceAndClientId(RuleNamespace.WEIGHT_BAND.name(), clientId)).thenReturn(rules);

        List<Rule> response = weightBandInferenceEngine.getRules(null, true, clientId);

        rules.forEach(r -> Assert.assertEquals(r.getRuleNamespace(), RuleNamespace.WEIGHT_BAND.name()));

    }
}
