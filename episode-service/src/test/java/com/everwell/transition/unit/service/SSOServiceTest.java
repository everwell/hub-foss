package com.everwell.transition.unit.service;

import com.everwell.transition.BaseTest;
import com.everwell.transition.model.db.Client;
import com.everwell.transition.model.response.Response;
import com.everwell.transition.model.response.SSO.DeviceIdsMapResponse;
import com.everwell.transition.service.ClientService;
import com.everwell.transition.service.impl.SSOServiceImpl;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.ArgumentMatchers;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.util.ReflectionTestUtils;

import java.util.*;

import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.when;

public class SSOServiceTest extends BaseTest {

    @Spy
    @InjectMocks
    SSOServiceImpl ssoHelperService;

    @Mock
    ClientService clientService;

    @Before
    public void init() {
        ReflectionTestUtils.setField(ssoHelperService, "dataGatewayURL", "http://test:8080");
        ReflectionTestUtils.setField(ssoHelperService, "username", "test");
        ReflectionTestUtils.setField(ssoHelperService, "password", "test");
        when(clientService.getClientByTokenOrDefault()).thenReturn(new Client(29L, "test", "test", null));
        ssoHelperService.init();
    }

    @Test
    public void loginTest() {
        Map<String, Object> loginRequest = new HashMap<>();
        Map<String, Object> loginResponse = new HashMap<>();
        loginResponse.put("sessionId", "1");
        ResponseEntity<Response<Map<String, Object>>> response
                = new ResponseEntity<>(
                new Response<>((loginResponse)),
                HttpStatus.OK
        );
        Map<String,String> headers = new HashMap<>();
        headers.put("X-Platform-id", "0");
        Long platformId = 0L;
        doReturn(response).when(ssoHelperService).postExchange(ArgumentMatchers.anyString(), ArgumentMatchers.any(), ArgumentMatchers.any(), ArgumentMatchers.any());
        ResponseEntity<Response<Map<String, Object>>> responseEntity = ssoHelperService.login(loginRequest, platformId);
        Assert.assertEquals("1", responseEntity.getBody().getData().get("sessionId"));
    }

    @Test
    public void logoutTest() {
        Map<String, Object> logoutRequest = new HashMap<>();
        ResponseEntity<Response<Void>> response
                =new ResponseEntity<>(
                        new Response<>(true,null),
                HttpStatus.OK
        );
        doReturn(response).when(ssoHelperService).postExchange(ArgumentMatchers.anyString(), ArgumentMatchers.any(), ArgumentMatchers.any());
        ResponseEntity<Response<Void>> responseEntity = ssoHelperService.logout(logoutRequest);
        Assert.assertEquals(true, responseEntity.getBody().isSuccess());
    }

    @Test
    public void updateDeviceIdAndLastActivityDate() {
        Map<String, Object>  updateDeviceIdRequest = new HashMap<>();
        ResponseEntity<Response<Void>> response
                =new ResponseEntity<>(
                new Response<>(true,null),
                HttpStatus.OK
        );
        doReturn(response).when(ssoHelperService).putExchange(ArgumentMatchers.anyString(), ArgumentMatchers.any(), ArgumentMatchers.any());
        ResponseEntity<Response<Void>> responseEntity = ssoHelperService.refreshDeviceIdAndLastActivityDate(updateDeviceIdRequest);
        Assert.assertEquals(true, responseEntity.getBody().isSuccess());
    }

    @Test
    public void getDeviceIdUserMap() {
        ResponseEntity<Response<Void>> response
                =new ResponseEntity<>(
                new Response<>(true,null),
                HttpStatus.OK
        );
        doReturn(response).when(ssoHelperService).getExchange(ArgumentMatchers.anyString(), ArgumentMatchers.any(), ArgumentMatchers.any(), ArgumentMatchers.any());
        List<String> usernames = Arrays.asList("test");
        ResponseEntity<Response<DeviceIdsMapResponse>> responseEntity = ssoHelperService.fetchDeviceIdUserMap(usernames);
        Assert.assertEquals(true, responseEntity.getBody().isSuccess());
    }

    @Test
    public void registration() {
        ResponseEntity<Response<Void>> response
                =new ResponseEntity<>(
                new Response<>(true,null),
                HttpStatus.OK
        );
        doReturn(response).when(ssoHelperService).postExchange(ArgumentMatchers.anyString(), ArgumentMatchers.any(), ArgumentMatchers.any());
        Boolean responseEntity = ssoHelperService.registration(ArgumentMatchers.any());
        Assert.assertEquals(true, responseEntity);
    }

}
