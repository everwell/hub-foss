package com.everwell.transition.unit.handler;

import com.everwell.transition.BaseTest;
import com.everwell.transition.binders.ABDMCommunicationBinder;
import com.everwell.transition.constants.Constants;
import com.everwell.transition.handlers.ABDMCommunicationHandler;
import com.everwell.transition.model.db.Episode;
import com.everwell.transition.model.dto.ABDMNotifyInputDto;
import com.everwell.transition.model.dto.EpisodeDto;
import com.everwell.transition.model.dto.EventStreamingDto;
import com.everwell.transition.model.dto.ExternalIdResponse;
import com.everwell.transition.model.dto.dispensation.DispensationData;
import com.everwell.transition.service.EpisodeService;
import com.everwell.transition.service.impl.EpisodeServiceImpl;
import com.everwell.transition.utils.Utils;
import org.junit.Before;
import org.junit.Test;
import org.mockito.*;
import org.springframework.messaging.Message;
import org.springframework.messaging.MessageChannel;
import org.springframework.messaging.support.MessageBuilder;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import static com.everwell.transition.constants.Constants.HEALTH_ADDRESS;
import static com.everwell.transition.constants.Constants.X_CLIENT_ID;
import static com.everwell.transition.constants.FieldConstants.*;
import static org.mockito.Mockito.*;

public class ABDMCommunicationHandlerTest extends BaseTest {

    @Spy
    @InjectMocks
    ABDMCommunicationHandler abdmCommunicationHandler;

    @Mock
    ABDMCommunicationBinder abdmCommunicationBinder;

    @Mock
    EpisodeService episodeService;

    @Mock
    EpisodeServiceImpl episodeServiceImpl;

    @Mock
    MessageChannel abdmLinkCareContextOutput;

    @Mock
    MessageChannel abdmCareContextNotifyOutput;

    @Mock
    MessageChannel abdmNotifyViaSMSOutput;

    private long nikshayClientId = 29;

    @Before
    public void init() throws Exception
    {
        MockitoAnnotations.initMocks(this);
        when(abdmCommunicationBinder.abdmCareContextNotifyOutput()).thenReturn(abdmCareContextNotifyOutput);
        when(abdmCommunicationBinder.abdmLinkCareContextOutput()).thenReturn(abdmLinkCareContextOutput);
        when(abdmCommunicationBinder.abdmNotifyViaSMSOutput()).thenReturn(abdmNotifyViaSMSOutput);
        abdmCommunicationHandler.setClientId(nikshayClientId);
    }

    @Test
    public void diagnosticsEventHandler_nikshayClientId_notifyCareContext() {
        ABDMNotifyInputDto input = new ABDMNotifyInputDto(nikshayClientId, 1111L);
        Message<ABDMNotifyInputDto> msg = MessageBuilder.withPayload(input).setHeader(X_CLIENT_ID,Long.toString(nikshayClientId)).build();
        when(episodeService.getEpisodeDetails(any(), any())).thenReturn(getEpisode(Boolean.TRUE.toString()));
        abdmCommunicationHandler.diagnosticsEventHandler(msg);
        Mockito.verify(abdmCommunicationBinder.abdmCareContextNotifyOutput(), Mockito.times(1)).send(any());
    }

    @Test
    public void diagnosticsEventHandler_nikshayClientId_addCareContext() {
        ABDMNotifyInputDto input = new ABDMNotifyInputDto(nikshayClientId, 1111L);
        Message<ABDMNotifyInputDto> msg = MessageBuilder.withPayload(input).setHeader(X_CLIENT_ID,Long.toString(nikshayClientId)).build();
        when(episodeService.getEpisodeDetails(any(), any())).thenReturn(getEpisode(Boolean.FALSE.toString()));
        when(episodeServiceImpl.processUpdateEpisode(any(),any())).thenReturn(new Episode());
        abdmCommunicationHandler.diagnosticsEventHandler(msg);
        Mockito.verify(abdmCommunicationBinder.abdmLinkCareContextOutput(), Mockito.times(1)).send(any());
    }

    @Test
    public void diagnosticsEventHandler_invalidClientId() {
        ABDMNotifyInputDto input = new ABDMNotifyInputDto(11L, 1111L);
        Message<ABDMNotifyInputDto> msg = MessageBuilder.withPayload(input).setHeader(X_CLIENT_ID,"11").build();
        abdmCommunicationHandler.diagnosticsEventHandler(msg);
        Mockito.verify(abdmCommunicationBinder.abdmLinkCareContextOutput(), Mockito.times(0)).send(any());
    }

    @Test
    public void dispensationEventHandler_nikshayClientId_notifyCareContext() {
        EventStreamingDto<DispensationData> input = new EventStreamingDto<>();
        input.setField(new DispensationData());
        input.getField().setEntityId(1111L);
        Message<EventStreamingDto<DispensationData>> msg = MessageBuilder.withPayload(input).setHeader(X_CLIENT_ID,Long.toString(nikshayClientId)).build();
        when(episodeService.getEpisodeDetails(any(), any())).thenReturn(getEpisode(Boolean.TRUE.toString()));
        abdmCommunicationHandler.dispensationEventHandler(msg);
        Mockito.verify(abdmCommunicationBinder.abdmCareContextNotifyOutput(), Mockito.times(1)).send(any());
    }

    @Test
    public void dispensationEventHandler_nikshayClientId_addCareContext() {
        EventStreamingDto<DispensationData> input = new EventStreamingDto<>();
        input.setField(new DispensationData());
        input.getField().setEntityId(1111L);
        Message<EventStreamingDto<DispensationData>> msg = MessageBuilder.withPayload(input).setHeader(X_CLIENT_ID,Long.toString(nikshayClientId)).build();
        when(episodeService.getEpisodeDetails(any(), any())).thenReturn(getEpisode(Boolean.FALSE.toString()));
        when(episodeServiceImpl.processUpdateEpisode(any(),any())).thenReturn(new Episode());
        abdmCommunicationHandler.dispensationEventHandler(msg);
        Mockito.verify(abdmCommunicationBinder.abdmLinkCareContextOutput(), Mockito.times(1)).send(any());
    }

    @Test
    public void dispensationEventHandler_invalidClientId() {
        EventStreamingDto<DispensationData> input = new EventStreamingDto<>();
        input.setField(new DispensationData());
        input.getField().setEntityId(1111L);
        Message<EventStreamingDto<DispensationData>> msg = MessageBuilder.withPayload(input).setHeader(X_CLIENT_ID,Long.toString(11222L)).build();
        abdmCommunicationHandler.dispensationEventHandler(msg);
        ABDMNotifyInputDto result =  new ABDMNotifyInputDto(11222L,input.getField().entityId);
        Mockito.verify(abdmCommunicationBinder.abdmLinkCareContextOutput(), Mockito.times(0)).send(any());
    }

    @Test
    public void abdmNotifySMSTest_ValidPrimaryPhone() {
        EventStreamingDto<DispensationData> input = new EventStreamingDto<>();
        input.setField(new DispensationData());
        input.getField().setEntityId(1111L);
        Message<EventStreamingDto<DispensationData>> msg = MessageBuilder.withPayload(input).setHeader(X_CLIENT_ID,Long.toString(nikshayClientId)).build();
        EpisodeDto episodeDto = getEpisode(Boolean.FALSE.toString());
        episodeDto.getStageData().remove("externalIds");
        episodeDto.getStageData().put("externalIds",new ArrayList<>());
        when(episodeService.getEpisodeDetails(any(), any())).thenReturn(episodeDto);
        when(episodeServiceImpl.processUpdateEpisode(any(),any())).thenReturn(new Episode());
        abdmCommunicationHandler.dispensationEventHandler(msg);
        Mockito.verify(abdmCommunicationBinder.abdmNotifyViaSMSOutput(), Mockito.times(1)).send(any());
    }

    @Test
    public void abdmNotifySMSTest_InvalidPrimaryPhone() {
        EventStreamingDto<DispensationData> input = new EventStreamingDto<>();
        input.setField(new DispensationData());
        input.getField().setEntityId(1111L);
        Message<EventStreamingDto<DispensationData>> msg = MessageBuilder.withPayload(input).setHeader(X_CLIENT_ID,Long.toString(nikshayClientId)).build();
        EpisodeDto episodeDto = getEpisode(Boolean.FALSE.toString());
        episodeDto.getStageData().remove(PERSON_PRIMARY_PHONE);
        episodeDto.getStageData().remove("externalIds");
        episodeDto.getStageData().put("externalIds",new ArrayList<>());
        when(episodeService.getEpisodeDetails(any(), any())).thenReturn(episodeDto);
        when(episodeServiceImpl.processUpdateEpisode(any(),any())).thenReturn(new Episode());
        abdmCommunicationHandler.dispensationEventHandler(msg);
        Mockito.verify(abdmCommunicationBinder.abdmNotifyViaSMSOutput(), Mockito.times(0)).send(any());
    }

    public EpisodeDto getEpisode(String isAddCareContext) {
        Map<String,Object> associationsMap = new HashMap<>();
        associationsMap.put("CareContextCreated",isAddCareContext);

        return new EpisodeDto(1L, 1L, 1L, true, "DSTB", 1L, null, Utils.getCurrentDateNew(), Utils.getCurrentDateNew(), null, Utils.getCurrentDateNew(), null, null, 1L,"PRESUMPTIVE_OPEN", "Presumptive Open", "IndiaTbPublic", 1L, associationsMap, null, null, null, getStageData());
    }

    public Map<String,Object> getStageData() {
        Map<String,Object> stageData = new HashMap<>();
        stageData.put("externalIds", Collections.singleton(new ExternalIdResponse(HEALTH_ADDRESS,"test@sbx")));
        stageData.put(PERSON_FIELD_FIRST_NAME,"test");
        stageData.put(PERSON_FIELD_LAST_NAME,"test");
        stageData.put(PERSON_DATE_OF_BIRTH,915148800000L);
        stageData.put(PERSON_GENDER,"Male");
        stageData.put(PERSON_PRIMARY_PHONE,"9848064996");
        return stageData;
    }
}