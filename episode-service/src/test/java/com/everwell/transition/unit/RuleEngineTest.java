package com.everwell.transition.unit;

import com.everwell.transition.BaseTest;
import com.everwell.transition.model.RuleNamespace;
import com.everwell.transition.model.db.Episode;
import com.everwell.transition.model.db.Rules;
import com.everwell.transition.model.db.Stage;
import com.everwell.transition.model.dto.EpisodeDto;
import com.everwell.transition.model.response.dispensation.DispensationWeightBandResponse;
import com.everwell.transition.parser.MVELParser;
import com.everwell.transition.parser.RuleParser;
import com.everwell.transition.repositories.RulesRepository;
import com.everwell.transition.service.AggregationService;
import com.everwell.transition.service.RuleEngine;
import com.everwell.transition.service.impl.StageTransitionKeyValueInferenceEngine;
import com.everwell.transition.service.impl.WeightBandInferenceEngine;
import com.everwell.transition.utils.Utils;
import com.fasterxml.jackson.core.type.TypeReference;
import org.junit.Assert;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;

import java.io.IOException;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;


public class RuleEngineTest extends BaseTest {

    @InjectMocks
    RuleEngine ruleEngine;

    @InjectMocks
    StageTransitionKeyValueInferenceEngine stageTransitionKeyValueInferenceEngine;

    @InjectMocks
    WeightBandInferenceEngine weightBandInferenceEngine;

    @Spy
    @InjectMocks
    RuleParser ruleParser;

    @Spy
    MVELParser mvelParser;

    @Mock
    RulesRepository rulesRepository;

    @Mock
    AggregationService aggregationService;

    private final Long clientId = 29L;
    @Test
    public void ruleEngineRunTest_presumptiveOpenRuleTest() throws IOException {
        when(rulesRepository.findAllByRuleNamespaceAndClientId(any(), any())).thenReturn(getDbRules());
        when(aggregationService.resolveFields(any(), any())).thenReturn(new HashMap<>());

        Map<String, Object> result = ruleEngine.run(stageTransitionKeyValueInferenceEngine, (Map<String, Object>) getInputMap_noStageInput().get("Field"), null, true, clientId);

        Assert.assertEquals("PRESUMPTIVE_OPEN", result.get("ToStage"));
    }

    @Test
    public void ruleEngineRunTest_closeTreatmentTest() throws IOException {
        when(rulesRepository.findAllByRuleNamespaceAndClientId(any(), any())).thenReturn(getDbRules());
        when(aggregationService.resolveFields(any(), any())).thenReturn(new HashMap<>());

        Map<String, Object> result = ruleEngine.run(stageTransitionKeyValueInferenceEngine, (Map<String, Object>) getInputMap_stageAndTreatmentOutComeInput().get("Field"), null, true, clientId);

        Assert.assertEquals("DIAGNOSED_OUTCOME_ASSIGNED", result.get("ToStage"));
    }

    @Test
    public void ruleEngineRunTest_noMatchingRule() throws IOException {
        when(rulesRepository.findAllByRuleNamespaceAndClientId(any(), any())).thenReturn(getDbRules());
        when(aggregationService.resolveFields(any(), any())).thenReturn(new HashMap<>());

        Map<String, Object> result = ruleEngine.run(stageTransitionKeyValueInferenceEngine, (Map<String, Object>) getInputMap_noRuleMatchingInput().get("Field"), null, true, clientId);

        Assert.assertNull(result);
    }

    @Test
    public void ruleEngineRunTest_noRules() throws IOException {
        when(rulesRepository.findAllByRuleNamespaceAndClientId(any(), any())).thenReturn(new ArrayList<>());
        when(aggregationService.resolveFields(any(), any())).thenReturn(new HashMap<>());

        Map<String, Object> result = ruleEngine.run(stageTransitionKeyValueInferenceEngine, (Map<String, Object>) getInputMap_noRuleMatchingInput().get("Field"), null, true, clientId);

        Assert.assertNull(result);
    }

    @Test
    public void ruleEngineRunTest_diagnosedNotOnTreatmentTest_valid() throws IOException {
        when(rulesRepository.findAllByRuleNamespaceAndClientId(any(), any())).thenReturn(getDbRules());

        Map<String, Object> input = (Map<String, Object>) getInputMap_testResultInput().get(("Field"));
        input.put("Stage", "PRESUMPTIVE_OPEN");
        Map<String, Object> result = ruleEngine.run(stageTransitionKeyValueInferenceEngine, input, null, true, clientId);
        Assert.assertEquals("DIAGNOSED_NOT_ON_TREATMENT", result.get("ToStage"));
    }

    Map<String, Object> getResolvedFields_diagnosedNotOnTreatment() {
        Map<String, Object> map = new HashMap<>();
        map.put("Stage", "PRESUMPTIVE_OPEN");
        return map;
    }

    Map<String, Object> getInputMap_testResultInput() throws IOException {
        Map<String, Object> map = new HashMap<>();
        String jsonData = "{\"EventName\":\"test_update\",\"Field\":{\"TestRequestId\":26116,\"EntityId\":531,\"Reason\":\"Diagnosis\",\"SubReason\":\"{\\\"typeOfPresumptiveDRTB\\\":\\\"\\\"}\",\"Status\":\"Results Available\",\"PreviousATT\":false,\"RequestDate\":1531765800000,\"TypeMap\":{\"CBNAAT\":{\"Result\":{\"TestResultId\":25454,\"DateTested\":1525717800000,\"DateReported\":1525717800000,\"FinalInterpretation\":\"M.Tb. detected, Rif resistance not detected\",\"Positive\":true,\"Remarks\":\"\",\"LabSerialNumber\":\"151\",\"SampleIndex\":25717,\"UpdatedAt\":1531765800000},\"SampleList\":[{\"SampleId\":25717,\"SpecimenType\":\"Sputum\"}]}}}}";
        return Utils.jsonToObject(jsonData, new TypeReference<Map<String, Object>>() {});
    }

    public void ruleEngineRunTest_publicPatientOnTreatment() throws IOException {
        when(rulesRepository.findAllByRuleNamespaceAndClientId(any(), any())).thenReturn(getDbRules());

        Map<String, Object> result = ruleEngine.run(stageTransitionKeyValueInferenceEngine, (Map<String, Object>) getInputMap_noOutcomeInput().get("Field"), null, true, clientId);

        Assert.assertEquals("DIAGNOSED_ON_TREATMENT", result.get("ToStage"));
    }

    @Test
    public void ruleEngineRunTest_privatePatientOnTreatment() throws IOException {
        when(rulesRepository.findAllByRuleNamespaceAndClientId(any(), any())).thenReturn(getDbRules());

        Map<String, Object> result = ruleEngine.run(stageTransitionKeyValueInferenceEngine, (Map<String, Object>) getInputMap_diagnosisDateInput().get("Field"), null, true, clientId);

        Assert.assertEquals("DIAGNOSED_ON_TREATMENT", result.get("ToStage"));
    }


    Map<String, Object> getInputMap_noStageInput() throws IOException {
        Map<String, Object> map = new HashMap<>();
        String jsonData = "{\"EventName\":\"patient_enrollment\",\"Field\":{\"Id\":10712725,\"UserId\":123223,\"Deleted\":false,\"AddedTimestamp\":\"2021-02-03T20:36:02.9471744\",\"FirstName\":\"Testing\",\"LastName\":\"Test\",\"Gender\":\"Female\",\"Language\":null,\"Age\":12,\"Occupation\":\"Unknown\",\"Address\":\"test\",\"PrimaryPhone\":\"1413241234\",\"PreARTNumber\":null,\"ARTNumber\":null,\"EnrollmentDate\":null,\"EndDate\":null,\"HierarchyMapping_Initiation\":null,\"HierarchyMapping_Residence\":324,\"TypeOfPatient\":\"IndiaTbPublic\",\"CurrentSchedule\":null,\"CurrentTags\":\"New_Enrollment\",\"TreatmentOutcome\":null,\"AttentionRequired\":null,\"AdherenceStatus\":null,\"LastDosage\":null,\"WeightBand\":null,\"NewOrPreviouslyTreated\":null,\"SiteOfDisease\":null,\"EPSite\":null,\"TypeOfCase\":null,\"HIVTestStatus\":\"Unknown\",\"NikshayId\":null,\"TBNumber\":null,\"TBCategory\":null,\"MermId\":null,\"DrugRegimen\":null,\"TBTreatmentStartDate\":null,\"AddedBy\":7945,\"DeploymentCode\":\"IND\",\"AdherenceString\":null,\"RegistrationDate\":\"2021-02-03T20:36:02.9315483\",\"FathersName\":null,\"Pincode\":\"123412\",\"Taluka\":null,\"Town\":null,\"Ward\":null,\"Landmark\":null,\"Stage\":\"PRESUMPTIVE_OPEN\",\"DiagnosisDate\":null,\"DiagnosisBasis\":null,\"MonitoringMethod\":null,\"TreatmentType\":null,\"DrugSusceptibility\":null,\"RefillMonitoring\":null,\"Area\":\"Unknown\",\"MaritalStatus\":\"Unknown\",\"SocioeconomicStatus\":\"Unknown\",\"KeyPopulation\":\"NotApplicable\",\"ContactPersonName\":null,\"ContactPersonAddress\":null,\"ContactPersonPhone\":null,\"MonthsOfTreatment\":null,\"MonthsSinceEpisode\":null,\"TreatmentSource\":null,\"Height\":null,\"CPInitiationDate\":null,\"RegimenType\":null,\"DrugResistance\":null,\"HierarchyMapping_Enrollment\":285624,\"HierarchyMapping_Diagnosed\":null,\"HierarchyMapping_Current\":285624,\"HierarchyMapping_ART\":null,\"HierarchyMapping_DRTB\":null,\"RemarksOutcome\":null,\"MedicalOfficerOutcome\":null,\"NotificationTestId\":null,\"BeneficiaryId\":45036,\"MigrationStatus\":null,\"Latitude\":null,\"Longitude\":null,\"HierarchyMapping_PVTHUB\":null,\"ParentEpisodeId\":null,\"IsDuplicate\":false,\"DuplicateStatus\":null,\"DuplicateOf\":null,\"IsIAMEnabled\":false,\"HierarchyMapping_Outcome\":null,\"LastActivityDate\":\"2021-02-03T20:36:02.9471744\",\"TransferId\":null,\"ForegoBenefits\":false}}";
        return Utils.jsonToObject(jsonData, new TypeReference<Map<String, Object>>() {});
    }

    Map<String, Object> getInputMap_stageAndTreatmentOutComeInput() throws IOException {
        Map<String, Object> map = new HashMap<>();
        String jsonData = "{\"EventName\":\"patient_enrollment\",\"Field\":{\"Id\":10712725,\"UserId\":123223,\"Deleted\":false,\"AddedTimestamp\":\"2021-02-03T20:36:02.9471744\",\"FirstName\":\"Testing\",\"LastName\":\"Test\",\"Gender\":\"Female\",\"Language\":null,\"Age\":12,\"Occupation\":\"Unknown\",\"Address\":\"test\",\"PrimaryPhone\":\"1413241234\",\"PreARTNumber\":null,\"ARTNumber\":null,\"EnrollmentDate\":null,\"EndDate\":null,\"HierarchyMapping_Initiation\":null,\"HierarchyMapping_Residence\":324,\"TypeOfPatient\":\"IndiaTbPublic\",\"CurrentSchedule\":null,\"CurrentTags\":\"New_Enrollment\",\"TreatmentOutcome\":null,\"AttentionRequired\":null,\"AdherenceStatus\":null,\"LastDosage\":null,\"WeightBand\":null,\"NewOrPreviouslyTreated\":null,\"SiteOfDisease\":null,\"EPSite\":null,\"TypeOfCase\":null,\"HIVTestStatus\":\"Unknown\",\"NikshayId\":null,\"TBNumber\":null,\"TBCategory\":null,\"MermId\":null,\"DrugRegimen\":null,\"TBTreatmentStartDate\":null,\"AddedBy\":7945,\"DeploymentCode\":\"IND\",\"AdherenceString\":null,\"RegistrationDate\":\"2021-02-03T20:36:02.9315483\",\"FathersName\":null,\"Pincode\":\"123412\",\"Taluka\":null,\"Town\":null,\"Ward\":null,\"Landmark\":null,\"Stage\":\"DIAGNOSED_ON_TREATMENT\",\"DiagnosisDate\":null,\"DiagnosisBasis\":null,\"MonitoringMethod\":null,\"TreatmentType\":null,\"DrugSusceptibility\":null,\"RefillMonitoring\":null,\"Area\":\"Unknown\",\"MaritalStatus\":\"Unknown\",\"SocioeconomicStatus\":\"Unknown\",\"KeyPopulation\":\"NotApplicable\",\"ContactPersonName\":null,\"ContactPersonAddress\":null,\"ContactPersonPhone\":null,\"MonthsOfTreatment\":null,\"MonthsSinceEpisode\":null,\"TreatmentSource\":null,\"Height\":null,\"CPInitiationDate\":null,\"RegimenType\":null,\"DrugResistance\":null,\"HierarchyMapping_Enrollment\":285624,\"HierarchyMapping_Diagnosed\":null,\"HierarchyMapping_Current\":285624,\"HierarchyMapping_ART\":null,\"HierarchyMapping_DRTB\":null,\"RemarksOutcome\":null,\"MedicalOfficerOutcome\":null,\"NotificationTestId\":null,\"BeneficiaryId\":45036,\"MigrationStatus\":null,\"Latitude\":null,\"Longitude\":null,\"HierarchyMapping_PVTHUB\":null,\"ParentEpisodeId\":null,\"IsDuplicate\":false,\"DuplicateStatus\":null,\"DuplicateOf\":null,\"IsIAMEnabled\":false,\"HierarchyMapping_Outcome\":null,\"LastActivityDate\":\"2021-02-03T20:36:02.9471744\",\"TreatmentOutcome\":\"Treatment Completed\",\"TransferId\":null,\"ForegoBenefits\":false}}";
        return Utils.jsonToObject(jsonData, new TypeReference<Map<String, Object>>() {});
    }

    Map<String, Object> getInputMap_noRuleMatchingInput() throws IOException {
        Map<String, Object> map = new HashMap<>();
        String jsonData = "{\"EventName\":\"patient_enrollment\",\"Field\":{\"Id\":10712725,\"UserId\":123223,\"Deleted\":false,\"AddedTimestamp\":\"2021-02-03T20:36:02.9471744\",\"FirstName\":\"Testing\",\"LastName\":\"Test\",\"Gender\":\"Female\",\"Language\":null,\"Age\":12,\"Occupation\":\"Unknown\",\"Address\":\"test\",\"PrimaryPhone\":\"1413241234\",\"PreARTNumber\":null,\"ARTNumber\":null,\"EnrollmentDate\":null,\"EndDate\":null,\"HierarchyMapping_Initiation\":null,\"HierarchyMapping_Residence\":324,\"TypeOfPatient\":\"IndiaTbPublic\",\"CurrentSchedule\":null,\"CurrentTags\":\"New_Enrollment\",\"TreatmentOutcome\":null,\"AttentionRequired\":null,\"AdherenceStatus\":null,\"LastDosage\":null,\"WeightBand\":null,\"NewOrPreviouslyTreated\":null,\"SiteOfDisease\":null,\"EPSite\":null,\"TypeOfCase\":null,\"HIVTestStatus\":\"Unknown\",\"NikshayId\":null,\"TBNumber\":null,\"TBCategory\":null,\"MermId\":null,\"DrugRegimen\":null,\"TBTreatmentStartDate\":null,\"AddedBy\":7945,\"DeploymentCode\":\"IND\",\"AdherenceString\":null,\"RegistrationDate\":\"2021-02-03T20:36:02.9315483\",\"FathersName\":null,\"Pincode\":\"123412\",\"Taluka\":null,\"Town\":null,\"Ward\":null,\"Landmark\":null,\"Stage\":\"DIAGNOSED_TREATMENT\",\"DiagnosisDate\":null,\"DiagnosisBasis\":null,\"MonitoringMethod\":null,\"TreatmentType\":null,\"DrugSusceptibility\":null,\"RefillMonitoring\":null,\"Area\":\"Unknown\",\"MaritalStatus\":\"Unknown\",\"SocioeconomicStatus\":\"Unknown\",\"KeyPopulation\":\"NotApplicable\",\"ContactPersonName\":null,\"ContactPersonAddress\":null,\"ContactPersonPhone\":null,\"MonthsOfTreatment\":null,\"MonthsSinceEpisode\":null,\"TreatmentSource\":null,\"Height\":null,\"CPInitiationDate\":null,\"RegimenType\":null,\"DrugResistance\":null,\"HierarchyMapping_Enrollment\":285624,\"HierarchyMapping_Diagnosed\":null,\"HierarchyMapping_Current\":285624,\"HierarchyMapping_ART\":null,\"HierarchyMapping_DRTB\":null,\"RemarksOutcome\":null,\"MedicalOfficerOutcome\":null,\"NotificationTestId\":null,\"BeneficiaryId\":45036,\"MigrationStatus\":null,\"Latitude\":null,\"Longitude\":null,\"HierarchyMapping_PVTHUB\":null,\"ParentEpisodeId\":null,\"IsDuplicate\":false,\"DuplicateStatus\":null,\"DuplicateOf\":null,\"IsIAMEnabled\":false,\"HierarchyMapping_Outcome\":null,\"LastActivityDate\":\"2021-02-03T20:36:02.9471744\",\"TransferId\":null,\"ForegoBenefits\":false}}";
        return Utils.jsonToObject(jsonData, new TypeReference<Map<String, Object>>() {});
    }

    Map<String, Object> getInputMap_noOutcomeInput() throws IOException {
        Map<String, Object> map = new HashMap<>();
        String jsonData = "{\"EventName\":\"patient_enrollment\",\"Field\":{\"Id\":10712725,\"UserId\":123223,\"Deleted\":false,\"AddedTimestamp\":\"2021-02-03T20:36:02.9471744\",\"FirstName\":\"Testing\",\"LastName\":\"Test\",\"Gender\":\"Female\",\"Language\":null,\"Age\":12,\"Occupation\":\"Unknown\",\"Address\":\"test\",\"PrimaryPhone\":\"1413241234\",\"PreARTNumber\":null,\"ARTNumber\":null,\"EnrollmentDate\":null,\"EndDate\":null,\"HierarchyMapping_Initiation\":null,\"HierarchyMapping_Residence\":324,\"TypeOfPatient\":\"IndiaTbPublic\",\"CurrentSchedule\":null,\"CurrentTags\":\"New_Enrollment\",\"TreatmentOutcome\":null,\"AttentionRequired\":null,\"AdherenceStatus\":null,\"LastDosage\":null,\"WeightBand\":null,\"NewOrPreviouslyTreated\":null,\"SiteOfDisease\":null,\"EPSite\":null,\"TypeOfCase\":null,\"HIVTestStatus\":\"Unknown\",\"NikshayId\":null,\"TBNumber\":null,\"TBCategory\":null,\"MermId\":null,\"DrugRegimen\":null,\"TBTreatmentStartDate\":null,\"AddedBy\":7945,\"DeploymentCode\":\"IND\",\"AdherenceString\":null,\"RegistrationDate\":\"2021-02-03T20:36:02.9315483\",\"FathersName\":null,\"Pincode\":\"123412\",\"Taluka\":null,\"Town\":null,\"Ward\":null,\"Landmark\":null,\"Stage\":\"DIAGNOSED_NOT_ON_TREATMENT\",\"DiagnosisDate\":null,\"DiagnosisBasis\":null,\"MonitoringMethod\":null,\"TreatmentType\":null,\"DrugSusceptibility\":null,\"RefillMonitoring\":null,\"Area\":\"Unknown\",\"MaritalStatus\":\"Unknown\",\"SocioeconomicStatus\":\"Unknown\",\"KeyPopulation\":\"NotApplicable\",\"ContactPersonName\":null,\"ContactPersonAddress\":null,\"ContactPersonPhone\":null,\"MonthsOfTreatment\":null,\"MonthsSinceEpisode\":null,\"TreatmentSource\":null,\"Height\":null,\"CPInitiationDate\":null,\"RegimenType\":null,\"DrugResistance\":null,\"HierarchyMapping_Enrollment\":285624,\"HierarchyMapping_Diagnosed\":null,\"HierarchyMapping_Current\":285624,\"HierarchyMapping_ART\":null,\"HierarchyMapping_DRTB\":null,\"RemarksOutcome\":null,\"MedicalOfficerOutcome\":null,\"NotificationTestId\":null,\"BeneficiaryId\":45036,\"MigrationStatus\":null,\"Latitude\":null,\"Longitude\":null,\"HierarchyMapping_PVTHUB\":null,\"ParentEpisodeId\":null,\"IsDuplicate\":false,\"DuplicateStatus\":null,\"DuplicateOf\":null,\"IsIAMEnabled\":false,\"HierarchyMapping_Outcome\":null,\"LastActivityDate\":\"2021-02-03T20:36:02.9471744\",\"TransferId\":null,\"ForegoBenefits\":false}}";
        return Utils.jsonToObject(jsonData, new TypeReference<Map<String, Object>>() {});
    }

    Map<String, Object> getInputMap_diagnosisDateInput() throws IOException {
        Map<String, Object> map = new HashMap<>();
        String jsonData = "{\"EventName\":\"patient_enrollment\",\"Field\":{\"Id\":10712725,\"UserId\":123223,\"Deleted\":false,\"AddedTimestamp\":\"2021-02-03T20:36:02.9471744\",\"FirstName\":\"Testing\",\"LastName\":\"Test\",\"Gender\":\"Female\",\"Language\":null,\"Age\":12,\"Occupation\":\"Unknown\",\"Address\":\"test\",\"PrimaryPhone\":\"1413241234\",\"PreARTNumber\":null,\"ARTNumber\":null,\"EnrollmentDate\":null,\"EndDate\":null,\"HierarchyMapping_Initiation\":null,\"HierarchyMapping_Residence\":324,\"TypeOfPatient\":\"IndiaTbPublic\",\"CurrentSchedule\":null,\"CurrentTags\":\"New_Enrollment\",\"TreatmentOutcome\":null,\"AttentionRequired\":null,\"AdherenceStatus\":null,\"LastDosage\":null,\"WeightBand\":null,\"NewOrPreviouslyTreated\":null,\"SiteOfDisease\":null,\"EPSite\":null,\"TypeOfCase\":null,\"HIVTestStatus\":\"Unknown\",\"NikshayId\":null,\"TBNumber\":null,\"TBCategory\":null,\"MermId\":null,\"DrugRegimen\":null,\"TBTreatmentStartDate\":null,\"AddedBy\":7945,\"DeploymentCode\":\"IND\",\"AdherenceString\":null,\"RegistrationDate\":\"2021-02-03T20:36:02.9315483\",\"FathersName\":null,\"Pincode\":\"123412\",\"Taluka\":null,\"Town\":null,\"Ward\":null,\"Landmark\":null,\"Stage\":\"PRESUMPTIVE_OPEN\",\"DiagnosisDate\":\"2021-09-02T00:00:00\",\"DiagnosisBasis\":null,\"MonitoringMethod\":null,\"TreatmentType\":null,\"DrugSusceptibility\":null,\"RefillMonitoring\":null,\"Area\":\"Unknown\",\"MaritalStatus\":\"Unknown\",\"SocioeconomicStatus\":\"Unknown\",\"KeyPopulation\":\"NotApplicable\",\"ContactPersonName\":null,\"ContactPersonAddress\":null,\"ContactPersonPhone\":null,\"MonthsOfTreatment\":null,\"MonthsSinceEpisode\":null,\"TreatmentSource\":null,\"Height\":null,\"CPInitiationDate\":null,\"RegimenType\":null,\"DrugResistance\":null,\"HierarchyMapping_Enrollment\":285624,\"HierarchyMapping_Diagnosed\":null,\"HierarchyMapping_Current\":285624,\"HierarchyMapping_ART\":null,\"HierarchyMapping_DRTB\":null,\"RemarksOutcome\":null,\"MedicalOfficerOutcome\":null,\"NotificationTestId\":null,\"BeneficiaryId\":45036,\"MigrationStatus\":null,\"Latitude\":null,\"Longitude\":null,\"HierarchyMapping_PVTHUB\":null,\"ParentEpisodeId\":null,\"IsDuplicate\":false,\"DuplicateStatus\":null,\"DuplicateOf\":null,\"IsIAMEnabled\":false,\"HierarchyMapping_Outcome\":null,\"LastActivityDate\":\"2021-02-03T20:36:02.9471744\",\"TransferId\":null,\"ForegoBenefits\":false}}";
        return Utils.jsonToObject(jsonData, new TypeReference<Map<String, Object>>() {});
    }

    List<Rules> getDbRules () {
        List<Rules> rules = Arrays.asList(
            new Rules(1L, RuleNamespace.STAGE_TRANSITION.name(), "var stage = input.get('Stage');var outcome = input.get('TreatmentOutcome');return (stage == null || stage.equals('') || stage.equals('PRESUMPTIVE_OPEN')) && (null == outcome || outcome.equals(''));", "output.put('ToStage', 'PRESUMPTIVE_OPEN');", 2, "TB Stage Transitions to PRESUMPTIVE_OPEN",null, null, clientId),
            new Rules(1L, RuleNamespace.STAGE_TRANSITION.name(), "var stage = input.get('Stage');var outcome = input.get('TreatmentOutcome');return stage != null && !stage.equals('PRESUMPTIVE_OPEN') && !stage.equals('DIAGNOSED_NOT_ON_TREATMENT') && outcome != null && !outcome.equals('');", "output.put('ToStage', 'DIAGNOSED_OUTCOME_ASSIGNED');", 1, "TB Stage Transitions to DIAGNOSED_OUTCOME_ASSIGNED", null, null, clientId),
            new Rules(11L, RuleNamespace.STAGE_TRANSITION.name(),
                    "var testReason = input.get('Reason'); var status = input.get('Status'); var isPositive = false; var stage = input.get('Stage'); var typeMap = input.get('TypeMap'); if(null != typeMap){foreach(testType: typeMap.entrySet()){var testTypeData = testType.value;var testResult = testTypeData.Result;if(testResult.Positive == true){isPositive = true;}}} return testReason == 'Diagnosis' && status == 'Results Available' && isPositive == true && stage == 'PRESUMPTIVE_OPEN';",
                    "output.put('ToStage', 'DIAGNOSED_NOT_ON_TREATMENT');", 1, "TB Stage Transitions from PRESUMPTIVE_OPEN to DIAGNOSED_NOT_ON_TREATMENT",null, null, clientId),
            new Rules(12L,RuleNamespace.STAGE_TRANSITION.name(),"return null != input.get('Stage') && input.get('Stage').equals('DIAGNOSED_NOT_ON_TREATMENT')&& (null == input.get('TreatmentOutcome') || input.get('TreatmentOutcome').equals(''));","output.put('ToStage', 'DIAGNOSED_ON_TREATMENT');",1,"TB Stage Transitions from DIAGNOSED_NOT_ON_TREATMENT to DIAGNOSED_ON_TREATMENT",null, null, clientId),
            new Rules(13L,RuleNamespace.STAGE_TRANSITION.name(),"return null != input.get('Stage') && input.get('Stage').equals('PRESUMPTIVE_OPEN')&& (null != input.get('DiagnosisDate'));","output.put('ToStage', 'DIAGNOSED_ON_TREATMENT');",0,"TB Stage Transitions from PRESUMPTIVE_OPEN to DIAGNOSED_ON_TREATMENT",null, null, clientId)
        );
        return rules;
    }

    @Test
    public void test_ruleEngine_nullToPresumptiveOpenPublicStage() {
        Map<String, Object> requestInput = new HashMap<>();
        when(rulesRepository.findAllByRuleNamespaceAndClientId(any(), any())).thenReturn(getDbRulesV2());
        requestInput.put("Id", 1L);
        requestInput.put("typeOfEpisode", "IndiaTbPublic");
        Map<String, Object> result = ruleEngine.run(stageTransitionKeyValueInferenceEngine, requestInput, (Long) requestInput.get("currentStageId"), false, clientId);
        Assert.assertNotNull(result);
        Assert.assertEquals("PRESUMPTIVE_OPEN_PUBLIC", result.get("ToStage"));
    }

    @Test
    public void test_ruleEngine_nullToPresumptiveOpenPrivateStage() {
        Map<String, Object> requestInput = new HashMap<>();
        when(rulesRepository.findAllByRuleNamespaceAndClientId(any(), any())).thenReturn(getDbRulesV2());
        requestInput.put("Id", 1L);
        requestInput.put("typeOfEpisode", "IndiaTbPrivate");
        Map<String, Object> result = ruleEngine.run(stageTransitionKeyValueInferenceEngine, requestInput, (Long) requestInput.get("currentStageId"), false, clientId);
        Assert.assertNotNull(result);
        Assert.assertEquals("PRESUMPTIVE_OPEN_PRIVATE", result.get("ToStage"));
    }

    @Test
    public void test_ruleEngine_presumptiveOpenPublicToClosedStage() {
        Episode episode = new Episode(1L, 1L, 1L, false, null, "1,2,3", Utils.getCurrentDateNew(), Utils.getCurrentDateNew(), null, Utils.getCurrentDateNew(), null, 1L, "IndiaTbPublic", 1L);
        Stage stage = new Stage(1L, "PRESUMPTIVE_OPEN_PUBLIC", null, "Presumptive Open");
        Map<String, Object> stageData = new HashMap<>();
        stageData.put("TreatmentOutcome", "TreatmentComplete");
        EpisodeDto episodeDto = new EpisodeDto(episode, new ArrayList<>(), stage, null, new ArrayList<>(), new HashMap<>(), new HashMap<>(), new ArrayList<>(), stageData);
        when(rulesRepository.findAllByRuleNamespaceAndClientId(any(), any())).thenReturn(getDbRulesV2());
        Map<String, Object> result = ruleEngine.run(stageTransitionKeyValueInferenceEngine, Utils.objectToMap(episodeDto), episodeDto.getCurrentStageId(), false, clientId);
        Assert.assertNotNull(result);
        Assert.assertEquals("PRESUMPTIVE_CLOSED", result.get("ToStage"));
    }

    @Test
    public void test_ruleEngine_presumptiveOpenPrivateToClosedStage() {
        Episode episode = new Episode(1L, 1L, 1L, false, null, "1,2,3", Utils.getCurrentDateNew(), Utils.getCurrentDateNew(), null, Utils.getCurrentDateNew(), null, 2L, "IndiaTbPrivate", 1L);
        Stage stage = new Stage(2L, "PRESUMPTIVE_OPEN_PRIVATE", null, "Presumptive Open");
        Map<String, Object> stageData = new HashMap<>();
        stageData.put("TreatmentOutcome", "TreatmentComplete");
        EpisodeDto episodeDto = new EpisodeDto(episode, new ArrayList<>(), stage, null, new ArrayList<>(), new HashMap<>(), new HashMap<>(), new ArrayList<>(), stageData);
        when(rulesRepository.findAllByRuleNamespaceAndClientId(any(), any())).thenReturn(getDbRulesV2());
        Map<String, Object> result = ruleEngine.run(stageTransitionKeyValueInferenceEngine, Utils.objectToMap(episodeDto), episodeDto.getCurrentStageId(), false, clientId);
        Assert.assertNotNull(result);
        Assert.assertEquals("PRESUMPTIVE_CLOSED", result.get("ToStage"));
    }

    @Test
    public void test_ruleEngine_presumptiveClosedToPresumptiveOpenPublicStage() {
        Episode episode = new Episode(1L, 1L, 1L, false, null, "1,2,3", Utils.getCurrentDateNew(), Utils.getCurrentDateNew(), null, Utils.getCurrentDateNew(), null, 3L, "IndiaTbPublic", 1L);
        Stage stage = new Stage(3L, "PRESUMPTIVE_CLOSED", null, "Presumptive Closed");
        Map<String, Object> stageData = new HashMap<>();
        stageData.put("TreatmentOutcome", null);
        EpisodeDto episodeDto = new EpisodeDto(episode, new ArrayList<>(), stage, null, new ArrayList<>(), new HashMap<>(), new HashMap<>(), new ArrayList<>(), stageData);
        when(rulesRepository.findAllByRuleNamespaceAndClientId(any(), any())).thenReturn(getDbRulesV2());
        Map<String, Object> result = ruleEngine.run(stageTransitionKeyValueInferenceEngine, Utils.objectToMap(episodeDto), episodeDto.getCurrentStageId(), false, clientId);
        Assert.assertNotNull(result);
        Assert.assertEquals("PRESUMPTIVE_OPEN_PUBLIC", result.get("ToStage"));
    }

    @Test
    public void test_ruleEngine_presumptiveClosedToPresumptiveOpenPrivateStage() {
        Episode episode = new Episode(1L, 1L, 1L, false, null, "1,2,3", Utils.getCurrentDateNew(), Utils.getCurrentDateNew(), null, Utils.getCurrentDateNew(), null, 3L, "IndiaTbPrivate", 1L);
        Stage stage = new Stage(3L, "PRESUMPTIVE_CLOSED", null, "Presumptive Closed");
        Map<String, Object> stageData = new HashMap<>();
        stageData.put("TreatmentOutcome", null);
        EpisodeDto episodeDto = new EpisodeDto(episode, new ArrayList<>(), stage, null, new ArrayList<>(), new HashMap<>(), new HashMap<>(), new ArrayList<>(), stageData);
        when(rulesRepository.findAllByRuleNamespaceAndClientId(any(), any())).thenReturn(getDbRulesV2());
        Map<String, Object> result = ruleEngine.run(stageTransitionKeyValueInferenceEngine, Utils.objectToMap(episodeDto), episodeDto.getCurrentStageId(), false, clientId);
        Assert.assertNotNull(result);
        Assert.assertEquals("PRESUMPTIVE_OPEN_PRIVATE", result.get("ToStage"));
    }

    @Test
    public void test_ruleEngine_diagnosedNotOnTreatmentToDiagnosedNotOnTreatmentClosedStage() {
        Episode episode = new Episode(1L, 1L, 1L, false, null, "1,2,3", Utils.getCurrentDateNew(), Utils.getCurrentDateNew(), null, Utils.getCurrentDateNew(), null, 4L, "IndiaTbPrivate", 1L);
        Stage stage = new Stage(4L, "DIAGNOSED_NOT_ON_TREATMENT", null, "Diagnosed Not On Treatment");
        Map<String, Object> stageData = new HashMap<>();
        stageData.put("TreatmentOutcome", "Not Opted for treatment");
        EpisodeDto episodeDto = new EpisodeDto(episode, new ArrayList<>(), stage, null, new ArrayList<>(), new HashMap<>(), new HashMap<>(), new ArrayList<>(), stageData);
        when(rulesRepository.findAllByRuleNamespaceAndClientId(any(), any())).thenReturn(getDbRulesV2());
        Map<String, Object> result = ruleEngine.run(stageTransitionKeyValueInferenceEngine, Utils.objectToMap(episodeDto), episodeDto.getCurrentStageId(), false, clientId);
        Assert.assertNotNull(result);
        Assert.assertEquals("DIAGNOSED_NOT_ON_TREATMENT_CLOSED", result.get("ToStage"));
    }

    @Test
    public void test_ruleEngine_diagnosedOnTreatmentPublicToDiagnosedOutcomeAssignedStage() {
        Episode episode = new Episode(1L, 1L, 1L, false, null, "1,2,3", Utils.getCurrentDateNew(), Utils.getCurrentDateNew(), null, Utils.getCurrentDateNew(),null, 6L, "IndiaTbPublic", 1L);
        Stage stage = new Stage(6L, "DIAGNOSED_ON_TREATMENT_PUBLIC", null, "Diagnosed On Treatment");
        Map<String, Object> stageData = new HashMap<>();
        stageData.put("TreatmentOutcome", "Treatment complete");
        EpisodeDto episodeDto = new EpisodeDto(episode, new ArrayList<>(), stage, null, new ArrayList<>(), new HashMap<>(), new HashMap<>(), new ArrayList<>(), stageData);
        when(rulesRepository.findAllByRuleNamespaceAndClientId(any(), any())).thenReturn(getDbRulesV2());
        Map<String, Object> result = ruleEngine.run(stageTransitionKeyValueInferenceEngine, Utils.objectToMap(episodeDto), episodeDto.getCurrentStageId(), false, clientId);
        Assert.assertNotNull(result);
        Assert.assertEquals("DIAGNOSED_OUTCOME_ASSIGNED", result.get("ToStage"));
    }

    @Test
    public void test_ruleEngine_diagnosedOnTreatmentPrivateToDiagnosedOutcomeAssignedStage() {
        Episode episode = new Episode(1L, 1L, 1L, false, null, "1,2,3", Utils.getCurrentDateNew(), Utils.getCurrentDateNew(), null, Utils.getCurrentDateNew(),null, 5L, "IndiaTbPrivate", 1L);
        Stage stage = new Stage(5L, "DIAGNOSED_ON_TREATMENT_PRIVATE", null, "Diagnosed On Treatment");
        Map<String, Object> stageData = new HashMap<>();
        stageData.put("TreatmentOutcome", "Treatment complete");
        EpisodeDto episodeDto = new EpisodeDto(episode, new ArrayList<>(), stage, null, new ArrayList<>(), new HashMap<>(), new HashMap<>(), new ArrayList<>(), stageData);
        when(rulesRepository.findAllByRuleNamespaceAndClientId(any(), any())).thenReturn(getDbRulesV2());
        Map<String, Object> result = ruleEngine.run(stageTransitionKeyValueInferenceEngine, Utils.objectToMap(episodeDto), episodeDto.getCurrentStageId(), false, clientId);
        Assert.assertNotNull(result);
        Assert.assertEquals("DIAGNOSED_OUTCOME_ASSIGNED", result.get("ToStage"));
    }

    @Test
    public void test_ruleEngine_diagnosedNotOnTreatmentClosedToDiagnosedNotOnTreatmentStage() {
        Episode episode = new Episode(1L, 1L, 1L, false, null, "1,2,3", Utils.getCurrentDateNew(), Utils.getCurrentDateNew(), null, Utils.getCurrentDateNew(), null, 8L, "IndiaTbPrivate", 1L);
        Stage stage = new Stage(8L, "DIAGNOSED_NOT_ON_TREATMENT_CLOSED", null, "Diagnosed Not On Treatment Closed");
        Map<String, Object> stageData = new HashMap<>();
        stageData.put("TreatmentOutcome", null);
        EpisodeDto episodeDto = new EpisodeDto(episode, new ArrayList<>(), stage, null, new ArrayList<>(), new HashMap<>(), new HashMap<>(), new ArrayList<>(), stageData);
        when(rulesRepository.findAllByRuleNamespaceAndClientId(any(), any())).thenReturn(getDbRulesV2());
        Map<String, Object> result = ruleEngine.run(stageTransitionKeyValueInferenceEngine, Utils.objectToMap(episodeDto), episodeDto.getCurrentStageId(), false, clientId);
        Assert.assertNotNull(result);
        Assert.assertEquals("DIAGNOSED_NOT_ON_TREATMENT", result.get("ToStage"));
    }

    @Test
    public void test_ruleEngine_diagnosedOutcomeAssignedToDiagnosedOnTreatmentPublicStage() {
        Episode episode = new Episode(1L, 1L, 1L, false, null, "1,2,3", Utils.getCurrentDateNew(), Utils.getCurrentDateNew(), null, Utils.getCurrentDateNew(), null, 7L, "IndiaTbPublic", 1L);
        Stage stage = new Stage(7L, "DIAGNOSED_OUTCOME_ASSIGNED", null, "Diagnosed Outcome Assigned");
        Map<String, Object> stageData = new HashMap<>();
        stageData.put("TreatmentOutcome", null);
        EpisodeDto episodeDto = new EpisodeDto(episode, new ArrayList<>(), stage, null, new ArrayList<>(), new HashMap<>(), new HashMap<>(), new ArrayList<>(), stageData);
        when(rulesRepository.findAllByRuleNamespaceAndClientId(any(), any())).thenReturn(getDbRulesV2());
        Map<String, Object> result = ruleEngine.run(stageTransitionKeyValueInferenceEngine, Utils.objectToMap(episodeDto), episodeDto.getCurrentStageId(), false, clientId);
        Assert.assertNotNull(result);
        Assert.assertEquals("DIAGNOSED_ON_TREATMENT_PUBLIC", result.get("ToStage"));
    }

    @Test
    public void test_ruleEngine_diagnosedOutcomeAssignedToDiagnosedOnTreatmentPrivateStage() {
        Episode episode = new Episode(1L, 1L, 1L, false, null, "1,2,3", Utils.getCurrentDateNew(), Utils.getCurrentDateNew(), null, Utils.getCurrentDateNew(),null, 7L, "IndiaTbPrivate", 1L);
        Stage stage = new Stage(7L, "DIAGNOSED_OUTCOME_ASSIGNED", null, "Diagnosed Outcome Assigned");
        Map<String, Object> stageData = new HashMap<>();
        stageData.put("TreatmentOutcome", null);
        EpisodeDto episodeDto = new EpisodeDto(episode, new ArrayList<>(), stage, null, new ArrayList<>(), new HashMap<>(), new HashMap<>(), new ArrayList<>(), stageData);
        when(rulesRepository.findAllByRuleNamespaceAndClientId(any(), any())).thenReturn(getDbRulesV2());
        Map<String, Object> result = ruleEngine.run(stageTransitionKeyValueInferenceEngine, Utils.objectToMap(episodeDto), episodeDto.getCurrentStageId(), false, clientId);
        Assert.assertNotNull(result);
        Assert.assertEquals("DIAGNOSED_ON_TREATMENT_PRIVATE", result.get("ToStage"));
    }

    @Test
    public void test_ruleEngine_diagnosedNotOnTreatmentToDiagnosedOnTreatmentPublicStage() {
        Episode episode = new Episode(1L, 1L, 1L, false, null, "1,2,3", Utils.getCurrentDateNew(), Utils.getCurrentDateNew(), null, Utils.getCurrentDateNew(), null, 4L, "IndiaTbPublic", 1L);
        Stage stage = new Stage(4L, "DIAGNOSED_NOT_ON_TREATMENT", null, "Diagnosed Not On Treatment");
        Map<String, Object> stageData = new HashMap<>();
        stageData.put("TreatmentOutcome", null);
        stageData.put("TreatmentStartTimeStamp",Utils.getFormattedDateNew(Utils.getCurrentDateNew()));
        EpisodeDto episodeDto = new EpisodeDto(episode, new ArrayList<>(), stage, null, new ArrayList<>(), new HashMap<>(), new HashMap<>(), new ArrayList<>(), stageData);
        when(rulesRepository.findAllByRuleNamespaceAndClientId(any(), any())).thenReturn(getDbRulesV2());
        Map<String, Object> result = ruleEngine.run(stageTransitionKeyValueInferenceEngine, Utils.objectToMap(episodeDto), episodeDto.getCurrentStageId(), false, clientId);
        Assert.assertNotNull(result);
        Assert.assertEquals("DIAGNOSED_ON_TREATMENT_PUBLIC", result.get("ToStage"));
    }

    @Test
    public void test_ruleEngine_diagnosedNotOnTreatmentToDiagnosedOnTreatmentPrivateStage() {
        Episode episode = new Episode(1L, 1L, 1L, false, null, "1,2,3", Utils.getCurrentDateNew(), Utils.getCurrentDateNew(), null, Utils.getCurrentDateNew(), null, 4L, "IndiaTbPrivate", 1L);
        Stage stage = new Stage(4L, "DIAGNOSED_NOT_ON_TREATMENT", null, "Diagnosed Not On Treatment");
        Map<String, Object> stageData = new HashMap<>();
        stageData.put("TreatmentOutcome", null);
        stageData.put("TreatmentStartTimeStamp",Utils.getFormattedDateNew(Utils.getCurrentDateNew()));
        EpisodeDto episodeDto = new EpisodeDto(episode, new ArrayList<>(), stage, null, new ArrayList<>(), new HashMap<>(), new HashMap<>(), new ArrayList<>(), stageData);
        when(rulesRepository.findAllByRuleNamespaceAndClientId(any(), any())).thenReturn(getDbRulesV2());
        Map<String, Object> result = ruleEngine.run(stageTransitionKeyValueInferenceEngine, Utils.objectToMap(episodeDto), episodeDto.getCurrentStageId(), false, clientId);
        Assert.assertNotNull(result);
        Assert.assertEquals("DIAGNOSED_ON_TREATMENT_PRIVATE", result.get("ToStage"));
    }

    @Test
    public void test_ruleEngine_presumptiveOpenPrivateToDiagnosedOnTreatmentPrivateStage() {
        Episode episode = new Episode(1L, 1L, 1L, false, null, "1,2,3", Utils.getCurrentDateNew(), Utils.getCurrentDateNew(), null, Utils.getCurrentDateNew(), null, 2L, "IndiaTbPrivate", 1L);
        Stage stage = new Stage(2L, "PRESUMPTIVE_OPEN_PRIVATE", null, "Presumptive Open");
        Map<String, Object> stageData = new HashMap<>();
        stageData.put("TreatmentOutcome", null);
        stageData.put("TreatmentStartTimeStamp",Utils.getFormattedDateNew(Utils.getCurrentDateNew()));
        EpisodeDto episodeDto = new EpisodeDto(episode, new ArrayList<>(), stage, null, new ArrayList<>(), new HashMap<>(), new HashMap<>(), new ArrayList<>(), stageData);
        when(rulesRepository.findAllByRuleNamespaceAndClientId(any(), any())).thenReturn(getDbRulesV2());
        Map<String, Object> result = ruleEngine.run(stageTransitionKeyValueInferenceEngine, Utils.objectToMap(episodeDto), episodeDto.getCurrentStageId(), false, clientId);
        Assert.assertNotNull(result);
        Assert.assertEquals("DIAGNOSED_ON_TREATMENT_PRIVATE", result.get("ToStage"));
    }

    @Test
    public void test_ruleEngine_noTransition_presumptiveOpenPrivateToDiagnosedOnTreatmentPrivateStage() {
        Episode episode = new Episode(1L, 1L, 1L, false, null, "1,2,3", Utils.getCurrentDateNew(), Utils.getCurrentDateNew(), null, Utils.getCurrentDateNew(), null, 1L, "IndiaTbPublic", 1L);
        Stage stage = new Stage(1L, "PRESUMPTIVE_OPEN_PUBLIC", null, "Presumptive Open");
        Map<String, Object> stageData = new HashMap<>();
        stageData.put("TreatmentOutcome", null);
        stageData.put("TreatmentStartTimeStamp",Utils.getFormattedDateNew(Utils.getCurrentDateNew()));
        EpisodeDto episodeDto = new EpisodeDto(episode, new ArrayList<>(), stage, null, new ArrayList<>(), new HashMap<>(), new HashMap<>(), new ArrayList<>(), stageData);
        when(rulesRepository.findAllByRuleNamespaceAndClientId(any(), any())).thenReturn(getDbRulesV2());
        Map<String, Object> result = ruleEngine.run(stageTransitionKeyValueInferenceEngine, Utils.objectToMap(episodeDto), episodeDto.getCurrentStageId(), false, clientId);
        Assert.assertNull(result);
    }

    @Test
    public void test_ruleEngine_presumptiveOpenPublicToDiagnosedNotOnTreatmentStage() throws IOException {
        Episode episode = new Episode(1L, 1L, 1L, false, null, "1,2,3", Utils.getCurrentDateNew(), Utils.getCurrentDateNew(), null, Utils.getCurrentDateNew(), null, 1L, "IndiaTbPublic", 1L);
        Stage stage = new Stage(1L, "PRESUMPTIVE_OPEN_PUBLIC", null, "Presumptive Open");
        EpisodeDto episodeDto = new EpisodeDto(episode, new ArrayList<>(), stage, null, new ArrayList<>(), new HashMap<>(), new HashMap<>(), new ArrayList<>(), new HashMap<>());
        Map<String, Object> input = Utils.objectToMap(episodeDto);
        input.putAll((Map<? extends String, ?>) getInputMap_testResultInput().get("Field"));
        when(rulesRepository.findAllByRuleNamespaceAndClientId(any(), any())).thenReturn(getDbRulesV2());
        Map<String, Object> result = ruleEngine.run(stageTransitionKeyValueInferenceEngine, input, episodeDto.getCurrentStageId(), false, clientId);
        Assert.assertNotNull(result);
        Assert.assertEquals("DIAGNOSED_NOT_ON_TREATMENT", result.get("ToStage"));
    }

    @Test
    public void test_ruleEngine_presumptiveOpenPrivateToDiagnosedNotOnTreatmentStage() throws IOException {
        Episode episode = new Episode(1L, 1L, 1L, false, null, "1,2,3", Utils.getCurrentDateNew(), Utils.getCurrentDateNew(), null, Utils.getCurrentDateNew(), null, 2L, "IndiaTbPrivate", 1L);
        Stage stage = new Stage(2L, "PRESUMPTIVE_OPEN_PRIVATE", null, "Presumptive Open");
        EpisodeDto episodeDto = new EpisodeDto(episode, new ArrayList<>(), stage, null, new ArrayList<>(), new HashMap<>(), new HashMap<>(), new ArrayList<>(), new HashMap<>());
        Map<String, Object> input = Utils.objectToMap(episodeDto);
        input.putAll((Map<? extends String, ?>) getInputMap_testResultInput().get("Field"));
        when(rulesRepository.findAllByRuleNamespaceAndClientId(any(), any())).thenReturn(getDbRulesV2());
        Map<String, Object> result = ruleEngine.run(stageTransitionKeyValueInferenceEngine, input, episodeDto.getCurrentStageId(), false, clientId);
        Assert.assertNotNull(result);
        Assert.assertEquals("DIAGNOSED_NOT_ON_TREATMENT", result.get("ToStage"));
    }

    @Test
    public void testRulesDRTB_Pediatric_Seven_to_Nine() {
        EpisodeDto episodeDto = new EpisodeDto();
        episodeDto.setDiseaseId(1L);
        Map<String, Object> stageData = new HashMap<>();
        stageData.put("Age", 17);
        stageData.put("Weight", 8);
        episodeDto.setStageData(stageData);

        when(rulesRepository.findAllByRuleNamespaceAndClientId(eq(RuleNamespace.WEIGHT_BAND.name()), eq(clientId))).thenReturn(getDbRulesV3());

        DispensationWeightBandResponse response = ruleEngine.run(weightBandInferenceEngine, episodeDto.getDiseaseId(),null, true, episodeDto, clientId);
        Assert.assertEquals("DRTB: Pediatric: 7-9 Kg", response.getWeightBand());
    }

    @Test
    public void testRulesDRTB_Adult_FortySix_to_Seventy() {
        EpisodeDto episodeDto = new EpisodeDto();
        episodeDto.setDiseaseId(1L);
        Map<String, Object> stageData = new HashMap<>();
        stageData.put("Age", 21);
        stageData.put("Weight", 46);
        episodeDto.setStageData(stageData);

        when(rulesRepository.findAllByRuleNamespaceAndClientId(eq(RuleNamespace.WEIGHT_BAND.name()), eq(clientId))).thenReturn(getDbRulesV3());

        DispensationWeightBandResponse response = ruleEngine.run(weightBandInferenceEngine, episodeDto.getDiseaseId(),null, true, episodeDto, clientId);
        Assert.assertEquals("DRTB: Adult: 46-70 Kg", response.getWeightBand());
    }

    @Test
    public void testRulesDRTB_Null() {
        EpisodeDto episodeDto = new EpisodeDto();
        episodeDto.setDiseaseId(1L);
        Map<String, Object> stageData = new HashMap<>();
        stageData.put("Weight", 46);
        episodeDto.setStageData(stageData);

        when(rulesRepository.findAllByRuleNamespaceAndClientId(eq(RuleNamespace.WEIGHT_BAND.name()), eq(clientId))).thenReturn(getDbRulesV3());

        DispensationWeightBandResponse response = ruleEngine.run(weightBandInferenceEngine, episodeDto.getDiseaseId(),null, true, episodeDto, clientId);
        Assert.assertNull(response.getWeightBand());
    }

    @Test
    public void testRulesDSTB_Adult_SixtyFive_to_SeventyFive() {
        EpisodeDto episodeDto = new EpisodeDto();
        episodeDto.setDiseaseId(2L);
        Map<String, Object> stageData = new HashMap<>();
        stageData.put("Age", 21);
        stageData.put("Weight", 66);
        episodeDto.setStageData(stageData);

        when(rulesRepository.findAllByRuleNamespaceAndClientId(eq(RuleNamespace.WEIGHT_BAND.name()), eq(clientId))).thenReturn(getDbRulesV3());

        DispensationWeightBandResponse response = ruleEngine.run(weightBandInferenceEngine, episodeDto.getDiseaseId(),null, true, episodeDto, clientId);
        Assert.assertEquals("DSTB: Adult: 65-75 Kg", response.getWeightBand());
    }

    @Test
    public void testRulesDSTB_Pediatric_Thirty_to_ThirtyNine() {
        EpisodeDto episodeDto = new EpisodeDto();
        episodeDto.setDiseaseId(2L);
        Map<String, Object> stageData = new HashMap<>();
        stageData.put("Age", 17);
        stageData.put("Weight", 33);
        episodeDto.setStageData(stageData);

        when(rulesRepository.findAllByRuleNamespaceAndClientId(eq(RuleNamespace.WEIGHT_BAND.name()), eq(clientId))).thenReturn(getDbRulesV3());

        DispensationWeightBandResponse response = ruleEngine.run(weightBandInferenceEngine, episodeDto.getDiseaseId(),null, true, episodeDto, clientId);
        Assert.assertEquals("DSTB: Pediatric: 30-39 Kg", response.getWeightBand());
    }

    @Test
    public void testRulesDSTB_Null() {
        EpisodeDto episodeDto = new EpisodeDto();
        episodeDto.setDiseaseId(2L);
        Map<String, Object> stageData = new HashMap<>();
        stageData.put("Weight", 33);
        episodeDto.setStageData(stageData);

        when(rulesRepository.findAllByRuleNamespaceAndClientId(eq(RuleNamespace.WEIGHT_BAND.name()), eq(clientId))).thenReturn(getDbRulesV3());

        DispensationWeightBandResponse response = ruleEngine.run(weightBandInferenceEngine, episodeDto.getDiseaseId(),null, true, episodeDto, clientId);
        Assert.assertNull(response.getWeightBand());
    }

    @Test
    public void testRulesTPT_Null() {
        EpisodeDto episodeDto = new EpisodeDto();
        episodeDto.setDiseaseId(3L);
        Map<String, Object> stageData = new HashMap<>();
        stageData.put("Age", 21);
        stageData.put("Weight", 33);
        episodeDto.setStageData(stageData);

        when(rulesRepository.findAllByRuleNamespaceAndClientId(eq(RuleNamespace.WEIGHT_BAND.name()), eq(clientId))).thenReturn(getDbRulesV3());

        DispensationWeightBandResponse response = ruleEngine.run(weightBandInferenceEngine, episodeDto.getDiseaseId(),null, true, episodeDto, clientId);
        Assert.assertNull(response.getWeightBand());
    }

    @Test
    public void testRulesTPT_6H_LESS10_LESS7() {
        EpisodeDto episodeDto = new EpisodeDto();
        episodeDto.setDiseaseId(3L);
        Map<String, Object> stageData = new HashMap<>();
        stageData.put("Age", 9);
        stageData.put("Weight", 7);
        stageData.put("RegimenType", "6H");
        episodeDto.setStageData(stageData);

        when(rulesRepository.findAllByRuleNamespaceAndClientId(eq(RuleNamespace.WEIGHT_BAND.name()), eq(clientId))).thenReturn(getDbRulesV3());

        DispensationWeightBandResponse response = ruleEngine.run(weightBandInferenceEngine, episodeDto.getDiseaseId(),null, true, episodeDto, clientId);
        Assert.assertEquals("TPT 6H (<10years): <=7 kgs", response.getWeightBand());
    }

    @Test
    public void testRulesTPT_3HP_LESS15_SixteenToTwentyThree() {
        EpisodeDto episodeDto = new EpisodeDto();
        episodeDto.setDiseaseId(3L);
        Map<String, Object> stageData = new HashMap<>();
        stageData.put("Age", 3);
        stageData.put("Weight", 16);
        stageData.put("RegimenType", "3HP");
        episodeDto.setStageData(stageData);

        when(rulesRepository.findAllByRuleNamespaceAndClientId(eq(RuleNamespace.WEIGHT_BAND.name()), eq(clientId))).thenReturn(getDbRulesV3());

        DispensationWeightBandResponse response = ruleEngine.run(weightBandInferenceEngine, episodeDto.getDiseaseId(),null, true, episodeDto, clientId);
        Assert.assertEquals("TPT 3HP (2-14years): 16-23 kgs", response.getWeightBand());
    }

    @Test
    public void testRules_NoDiseaseIdMatch() {
        EpisodeDto episodeDto = new EpisodeDto();
        episodeDto.setDiseaseId(4L);
        when(rulesRepository.findAllByRuleNamespaceAndClientId(eq(RuleNamespace.WEIGHT_BAND.name()), eq(clientId))).thenReturn(getDbRulesV3());

        DispensationWeightBandResponse response = ruleEngine.run(weightBandInferenceEngine, episodeDto.getDiseaseId(),null, true, episodeDto, clientId);
        Assert.assertNull(response);
    }

    List<Rules> getDbRulesV2 () {
        List<Rules> rules = new ArrayList<>();
        rules.add(new Rules( 1L, RuleNamespace.STAGE_TRANSITION.name(), "var typeOfEpisode = input.get(\"typeOfEpisode\");var outcome = input.get(\"TreatmentOutcome\");return (null == outcome || outcome.equals(\"\")) && (typeOfEpisode.equals(\"IndiaTbPublic\"));", "output.put(\"ToStage\", \"PRESUMPTIVE_OPEN_PUBLIC\");", 2, "TB Stage Transitions to PRESUMPTIVE_OPEN_PUBLIC", 1L, null, clientId));
        rules.add(new Rules( 2L, RuleNamespace.STAGE_TRANSITION.name(), "var outcome = input.get(\"stageData\").get(\"TreatmentOutcome\");return null != outcome && !outcome.equals(\"\");", "output.put(\"ToStage\", \"PRESUMPTIVE_CLOSED\");", 1, "TB Stage Transitions from PRESUMPTIVE_OPEN_PUBLIC to PRESUMPTIVE_CLOSED", 3L, 1L, clientId));
        rules.add(new Rules( 3L, RuleNamespace.STAGE_TRANSITION.name(), "var outcome = input.get(\"stageData\").get(\"TreatmentOutcome\");var typeOfEpisode = input.get(\"typeOfEpisode\");return (null == outcome || outcome.equals(\"\")) && typeOfEpisode.equals(\"IndiaTbPublic\");", "output.put(\"ToStage\", \"PRESUMPTIVE_OPEN_PUBLIC\");", 1, "TB Stage Transitions from PRESUMPTIVE_CLOSED to PRESUMPTIVE_OPEN_PUBLIC", 1L, 3L, clientId));
        rules.add(new Rules( 4L, RuleNamespace.STAGE_TRANSITION.name(), "var outcome = input.get(\"stageData\").get(\"TreatmentOutcome\");return outcome != null && !outcome.equals(\"\");", "output.put(\"ToStage\", \"DIAGNOSED_NOT_ON_TREATMENT_CLOSED\");", 1, "TB Stage Transitions from DIAGNOSED_NOT_ON_TREATMENT to DIAGNOSED_NOT_ON_TREATMENT_CLOSED", 8L, 4L, clientId));
        rules.add(new Rules( 5L, RuleNamespace.STAGE_TRANSITION.name(), "var outcome = input.get(\"stageData\").get(\"TreatmentOutcome\");var typeOfEpisode = input.get(\"typeOfEpisode\");return outcome != null && !outcome.equals(\"\") && typeOfEpisode.equals(\"IndiaTbPublic\");", "output.put(\"ToStage\", \"DIAGNOSED_OUTCOME_ASSIGNED\");", 1, "TB Stage Transitions from DIAGNOSED_ON_TREATMENT_PUBLIC to DIAGNOSED_OUTCOME_ASSIGNED", 7L, 6L, clientId));
        rules.add(new Rules( 9L, RuleNamespace.STAGE_TRANSITION.name(), "var outcome = input.get(\"stageData\").get(\"TreatmentOutcome\");return null == outcome || outcome.equals(\"\");", "output.put(\"ToStage\", \"DIAGNOSED_NOT_ON_TREATMENT\");", 1, "TB Stage Transitions from DIAGNOSED_NOT_ON_TREATMENT_CLOSED to DIAGNOSED_NOT_ON_TREATMENT", 4L, 8L, clientId));
        rules.add(new Rules(10L, RuleNamespace.STAGE_TRANSITION.name(), "var outcome = input.get(\"stageData\").get(\"TreatmentOutcome\");var typeOfEpisode = input.get(\"typeOfEpisode\");return (null == outcome || outcome.equals(\"\")) && typeOfEpisode.equals(\"IndiaTbPublic\");", "output.put(\"ToStage\", \"DIAGNOSED_ON_TREATMENT_PUBLIC\");", 1, "TB Stage Transitions from DIAGNOSED_OUTCOME_ASSIGNED to DIAGNOSED_ON_TREATMENT_PUBLIC", 6L, 7L, clientId));
        rules.add(new Rules(11L, RuleNamespace.STAGE_TRANSITION.name(), "var testReason = input.get(\"Reason\"); var status = input.get(\"Status\"); var isPositive = false; var typeMap = input.get(\"TypeMap\"); if(null != typeMap){foreach(testType: typeMap.entrySet()){var testTypeData = testType.value;var testResult = testTypeData.Result;if(testResult.Positive == true){isPositive = true;}}} return testReason == \"Diagnosis\" && status == \"Results Available\" && isPositive == true;", "output.put(\"ToStage\", \"DIAGNOSED_NOT_ON_TREATMENT\");", 1, "TB Stage Transitions from PRESUMPTIVE_OPEN_PUBLIC to DIAGNOSED_NOT_ON_TREATMENT", 4L, 1L, clientId));
        rules.add(new Rules(12L, RuleNamespace.STAGE_TRANSITION.name(), "var treatmentStartTimeStamp = input.get(\"stageData\").get(\"TreatmentStartTimeStamp\");var outcome = input.get(\"stageData\").get(\"TreatmentOutcome\");var typeOfEpisode = input.get(\"typeOfEpisode\");return null != treatmentStartTimeStamp && (null == outcome || outcome.equals(\"\"))&& typeOfEpisode.equals(\"IndiaTbPublic\");", "output.put(\"ToStage\", \"DIAGNOSED_ON_TREATMENT_PUBLIC\");", 1, "TB Stage Transitions from DIAGNOSED_NOT_ON_TREATMENT to DIAGNOSED_ON_TREATMENT_PUBLIC", 6L, 4L, clientId));
        rules.add(new Rules(13L, RuleNamespace.STAGE_TRANSITION.name(), "var treatmentStartDate = input.get(\"stageData\").get(\"TreatmentStartTimeStamp\");var outcome = input.get(\"stageData\").get(\"TreatmentOutcome\");var typeOfEpisode = input.get(\"typeOfEpisode\");return null != treatmentStartDate && typeOfEpisode.equals(\"IndiaTbPrivate\") && (null == outcome ||outcome.equals(\"\"));", "output.put(\"ToStage\", \"DIAGNOSED_ON_TREATMENT_PRIVATE\");", 0, "TB Stage Transitions from PRESUMPTIVE_OPEN_PRIVATE to DIAGNOSED_ON_TREATMENT_PRIVATE", 5L, 2L, clientId));
        rules.add(new Rules(14L, RuleNamespace.STAGE_TRANSITION.name(), "var typeOfEpisode = input.get(\"typeOfEpisode\");var outcome = input.get(\"TreatmentOutcome\");return (null == outcome || outcome.equals(\"\")) && (typeOfEpisode.equals(\"IndiaTbPrivate\"));", "output.put(\"ToStage\", \"PRESUMPTIVE_OPEN_PRIVATE\");", 2, "TB Stage Transitions to PRESUMPTIVE_OPEN_PRIVATE", 2L, null, clientId));
        rules.add(new Rules(15L, RuleNamespace.STAGE_TRANSITION.name(), "var outcome = input.get(\"stageData\").get(\"TreatmentOutcome\");return null != outcome && !outcome.equals(\"\");", "output.put(\"ToStage\", \"PRESUMPTIVE_CLOSED\");", 1, "TB Stage Transitions from PRESUMPTIVE_OPEN_PRIVATE to PRESUMPTIVE_CLOSED", 3L, 2L, clientId));
        rules.add(new Rules(16L, RuleNamespace.STAGE_TRANSITION.name(), "var outcome = input.get(\"stageData\").get(\"TreatmentOutcome\");var typeOfEpisode = input.get(\"typeOfEpisode\");return (null == outcome || outcome.equals(\"\")) && typeOfEpisode.equals(\"IndiaTbPrivate\");", "output.put(\"ToStage\", \"PRESUMPTIVE_OPEN_PRIVATE\");", 1, "TB Stage Transitions from PRESUMPTIVE_CLOSED to PRESUMPTIVE_OPEN_PRIVATE", 2L, 3L, clientId));
        rules.add(new Rules(17L, RuleNamespace.STAGE_TRANSITION.name(), "var outcome = input.get(\"stageData\").get(\"TreatmentOutcome\");var typeOfEpisode = input.get(\"typeOfEpisode\");return outcome != null && !outcome.equals(\"\") && typeOfEpisode.equals(\"IndiaTbPrivate\");", "output.put(\"ToStage\", \"DIAGNOSED_OUTCOME_ASSIGNED\");", 1, "TB Stage Transitions from DIAGNOSED_ON_TREATMENT_PRIVATE to DIAGNOSED_OUTCOME_ASSIGNED", 7L, 5L, clientId));
        rules.add(new Rules(18L, RuleNamespace.STAGE_TRANSITION.name(), "var outcome = input.get(\"stageData\").get(\"TreatmentOutcome\");var typeOfEpisode = input.get(\"typeOfEpisode\");return (null == outcome || outcome.equals(\"\")) && typeOfEpisode.equals(\"IndiaTbPrivate\");", "output.put(\"ToStage\", \"DIAGNOSED_ON_TREATMENT_PRIVATE\");", 1, "TB Stage Transitions from DIAGNOSED_OUTCOME_ASSIGNED to DIAGNOSED_ON_TREATMENT_PRIVATE", 5L, 7L, clientId));
        rules.add(new Rules(19L, RuleNamespace.STAGE_TRANSITION.name(), "var testReason = input.get(\"Reason\"); var status = input.get(\"Status\"); var isPositive = false; var typeMap = input.get(\"TypeMap\"); if(null != typeMap){foreach(testType: typeMap.entrySet()){var testTypeData = testType.value;var testResult = testTypeData.Result;if(testResult.Positive){isPositive = true;}}} return testReason == \"Diagnosis\" && status == \"Results Available\" && isPositive;", "output.put(\"ToStage\", \"DIAGNOSED_NOT_ON_TREATMENT\");", 1, "TB Stage Transitions from PRESUMPTIVE_OPEN_PRIVATE to DIAGNOSED_NOT_ON_TREATMENT", 4L, 2L, clientId));
        rules.add(new Rules(20L, RuleNamespace.STAGE_TRANSITION.name(), "var treatmentStartTimeStamp = input.get(\"stageData\").get(\"TreatmentStartTimeStamp\");var outcome = input.get(\"stageData\").get(\"TreatmentOutcome\");var typeOfEpisode = input.get(\"typeOfEpisode\");return null != treatmentStartTimeStamp && typeOfEpisode.equals(\"IndiaTbPrivate\") && (null == outcome ||outcome.equals(\"\"));", "output.put(\"ToStage\", \"DIAGNOSED_ON_TREATMENT_PRIVATE\");", 1, "TB Stage Transitions from DIAGNOSED_NOT_ON_TREATMENT to DIAGNOSED_ON_TREATMENT_PRIVATE", 5L, 4L, clientId));
        return rules;
    }

    List<Rules> getDbRulesV3 () {
        List<Rules> rules = new ArrayList<>();
        rules.add(new Rules(21L, RuleNamespace.WEIGHT_BAND.name(), "return input.equals(1L);", "var fieldAge = \"Age\";\n" +
                "var fieldWeight = \"Weight\";\n" +
                "Long age = input.getStageData().get(fieldAge) == null ? null : (Long) input.getStageData().get(fieldAge);\n" +
                "Long weight = input.getStageData().get(fieldWeight) == null ? null : Long.parseLong((String) input.getStageData().get(fieldWeight));\n" +
                "var weightBand = null;\n" +
                "var DRTB_Pediatric_FiveToSix = \"DRTB: Pediatric: 5-6 Kg\";\n" +
                "var DRTB_Pediatric_SevenToNine = \"DRTB: Pediatric: 7-9 Kg\";\n" +
                "var DRTB_Pediatric_TenToFifteen = \"DRTB: Pediatric: 10-15 Kg\";\n" +
                "var DRTB_Pediatric_SixtennToTwentyThree = \"DRTB: Pediatric: 16-23 Kg\";\n" +
                "var DRTB_Pediatric_TwentyFourToThirty = \"DRTB: Pediatric: 24-30 Kg\";\n" +
                "var DRTB_Pediatric_ThirtyOneToThirtyFour = \"DRTB: Pediatric: 31-34 Kg\";\n" +
                "var DRTB_Pediatric_GreaterThanThirtyFour = \"DRTB: Pediatric: >34 Kg\";\n" +
                "var DRTB_Adult_LesserThanSixteen = \"DRTB: Adult: <16 Kg\";\n" +
                "var DRTB_Adult_SixteenToTwentyNine = \"DRTB: Adult: 16-29 Kg\";\n" +
                "var DRTB_Adult_ThirtyToFortyFive = \"DRTB: Adult: 30-45 Kg\";\n" +
                "var DRTB_Adult_FortySixToSeventy = \"DRTB: Adult: 46-70 Kg\";\n" +
                "var DRTB_Adult_GreaterThanSeventy = \"DRTB: Adult: >70 Kg\";\n" +
                "if (age != null && weight != null) {\n" +
                "\tif (age <= 18) {\n" +
                "\t\tif (weight >= 5 && weight <= 6)\n" +
                "\t\t\tweightBand = DRTB_Pediatric_FiveToSix;\n" +
                "\t\telse if (weight >= 7 && weight <= 9)\n" +
                "\t\t\tweightBand = DRTB_Pediatric_SevenToNine;\n" +
                "\t\telse if (weight >= 10 && weight <= 15)\n" +
                "\t\t\tweightBand = DRTB_Pediatric_TenToFifteen;\n" +
                "\t\telse if (weight >= 16 && weight <= 23)\n" +
                "\t\t\tweightBand = DRTB_Pediatric_SixtennToTwentyThree;\n" +
                "\t\telse if (weight >= 24 && weight <= 30)\n" +
                "\t\t\tweightBand = DRTB_Pediatric_TwentyFourToThirty;\n" +
                "\t\telse if (weight >= 31 && weight <= 34)\n" +
                "\t\t\tweightBand = DRTB_Pediatric_ThirtyOneToThirtyFour;\n" +
                "\t\telse\n" +
                "\t\t\tweightBand = DRTB_Pediatric_GreaterThanThirtyFour;\n" +
                "\t} else {\n" +
                "\n" +
                "\t\tif (weight < 16)\n" +
                "\t\t\tweightBand = DRTB_Adult_LesserThanSixteen;\n" +
                "\n" +
                "\t\telse if (weight >= 16 && weight <= 29)\n" +
                "\t\t\tweightBand = DRTB_Adult_SixteenToTwentyNine;\n" +
                "\n" +
                "\t\telse if (weight >= 30 && weight <= 45)\n" +
                "\t\t\tweightBand = DRTB_Adult_ThirtyToFortyFive;\n" +
                "\n" +
                "\t\telse if (weight >= 46 && weight <= 70)\n" +
                "\t\t\tweightBand = DRTB_Adult_FortySixToSeventy;\n" +
                "\n" +
                "\t\telse if (weight > 70)\n" +
                "\t\t\tweightBand = DRTB_Adult_GreaterThanSeventy;\n" +
                "\t}\n" +
                "}\n" +
                "output.setWeightBand(weightBand);", 0, "DRTB", null, null, clientId));
        rules.add(new Rules(22L, RuleNamespace.WEIGHT_BAND.name(), "return input.equals(2L);", "var fieldAge = \"Age\";\n" +
                "var fieldWeight = \"Weight\";\n" +
                "Long age = input.getStageData().get(fieldAge) == null ? null : (Long) input.getStageData().get(fieldAge);\n" +
                "Long weight = input.getStageData().get(fieldWeight) == null ? null : Long.parseLong((String) input.getStageData().get(fieldWeight));\n" +
                "var weightBand = null;\n" +
                "var DSTB_Pediatric_FourToSeven = \"DSTB: Pediatric: 4-7 Kg\";\n" +
                "var DSTB_Pediatric_EightToEleven = \"DSTB: Pediatric: 8-11 Kg\";\n" +
                "var DSTB_Pediatric_TwelveToFifteen = \"DSTB: Pediatric: 12-15 Kg\";\n" +
                "var DSTB_Pediatric_SixteenToTwentyFour = \"DSTB: Pediatric: 16-24 Kg\";\n" +
                "var DSTB_Pediatric_TwentyFiveToTwentyNine = \"DSTB: Pediatric: 25-29 Kg\";\n" +
                "var DSTB_Pediatric_ThirtyToThirtyNine = \"DSTB: Pediatric: 30-39 Kg\";\n" +
                "var DSTB_Adult_TwentyFiveToThirtyFour = \"DSTB: Adult: 25-34 Kg\";\n" +
                "var DSTB_Adult_ThirtyFiveToFortyNine = \"DSTB: Adult: 35-49 Kg\";\n" +
                "var DSTB_Adult_FiftyToSixtyFour = \"DSTB: Adult: 50-64 Kg\";\n" +
                "var DSTB_Adult_SixtyFiveToSeventyFive = \"DSTB: Adult: 65-75 Kg\";\n" +
                "var DSTB_Adult_GreaterThanSeventyFive = \"DSTB: Adult: >75 Kg\";\n" +
                "if (age != null && weight != null) {\n" +
                "    if (age <= 18) {\n" +
                "        if (weight >= 4 && weight <= 7)\n" +
                "            weightBand = DSTB_Pediatric_FourToSeven;\n" +
                "\n" +
                "        else if (weight >= 8 && weight <= 11)\n" +
                "            weightBand = DSTB_Pediatric_EightToEleven;\n" +
                "\n" +
                "        else if (weight >= 12 && weight <= 15)\n" +
                "            weightBand = DSTB_Pediatric_TwelveToFifteen;\n" +
                "\n" +
                "        else if (weight >= 16 && weight <= 24)\n" +
                "            weightBand = DSTB_Pediatric_SixteenToTwentyFour;\n" +
                "\n" +
                "        else if (weight >= 25 && weight <= 29)\n" +
                "            weightBand = DSTB_Pediatric_TwentyFiveToTwentyNine;\n" +
                "\n" +
                "        else if (weight >= 30 && weight <= 39)\n" +
                "            weightBand = DSTB_Pediatric_ThirtyToThirtyNine;\n" +
                "    } else {\n" +
                "\n" +
                "            if (weight >= 25 && weight <= 34)\n" +
                "                weightBand = DSTB_Adult_TwentyFiveToThirtyFour;\n" +
                "\n" +
                "            else if (weight >= 35 && weight <= 49)\n" +
                "                weightBand = DSTB_Adult_ThirtyFiveToFortyNine;\n" +
                "\n" +
                "            else if (weight >= 50 && weight <= 64)\n" +
                "                weightBand = DSTB_Adult_FiftyToSixtyFour;\n" +
                "\n" +
                "            else if (weight >= 65 && weight <= 75)\n" +
                "                weightBand = DSTB_Adult_SixtyFiveToSeventyFive;\n" +
                "\n" +
                "            else if (weight > 75)\n" +
                "                weightBand = DSTB_Adult_GreaterThanSeventyFive;\n" +
                "    }\n" +
                "}\n" +
                "output.setWeightBand(weightBand);", 1, "DSTB", null, null, clientId));
        rules.add(new Rules(23L, RuleNamespace.WEIGHT_BAND.name(), "return input.equals(3L);", "var fieldAge = \"Age\";\n" +
                "var fieldWeight = \"Weight\";\n" +
                "var fieldRegimen = \"RegimenType\";\n" +
                "Long age = input.getStageData().get(fieldAge) == null ? null : (Long) input.getStageData().get(fieldAge);\n" +
                "Long weight = input.getStageData().get(fieldWeight) == null ? null : Long.parseLong((String) input.getStageData().get(fieldWeight));\n" +
                "String typeOfRegimen = input.getStageData().get(fieldRegimen) != null ? (String) input.getStageData().get(fieldRegimen) : null;\n" +
                "var weightBand = null;\n" +
                "\n" +
                "var TPT_6H_LESS10_LESS7 = \"TPT 6H (<10years): <=7 kgs\";\n" +
                "var TPT_6H_LESS10_EightToEleven = \"TPT 6H (<10years): 8-11 kgs\";\n" +
                "var TPT_6H_LESS10_TwelveToFifteen = \"TPT 6H (<10years): 12-15 kgs\";\n" +
                "var TPT_6H_LESS10_SixteenToTwentyFive = \"TPT 6H (<10years): 16-24 kgs\";\n" +
                "var TPT_6H_LESS10_GreaterThan25 = \"TPT 6H (<10years): >=25kgs\";\n" +
                "var TPT_6H_GREATER10_LESS25 = \"TPT 6H (≥10years): <25 kgs\";\n" +
                "var TPT_6H_GREATER10_TwentyFiveToThirtyFour = \"TPT 6H (≥10years): 25-34 kgs\";\n" +
                "var TPT_6H_GREATER10_ThirtyFiveToFortyNine = \"TPT 6H (≥10years): 35-49 kgs\";\n" +
                "var TPT_6H_GREATER10_GreaterThan50 = \"TPT 6H (≥10years): >=50 kgs\";\n" +
                "var TPT_3HP_LESS15_TenToFifteen = \"TPT 3HP (2-14years): 10-15 kgs\";\n" +
                "var TPT_3HP_LESS15_SixteenToTwentyThree = \"TPT 3HP (2-14years): 16-23 kgs\";\n" +
                "var TPT_3HP_LESS15_TwentyFourToThirty = \"TPT 3HP (2-14years): 24-30 kgs\";\n" +
                "var TPT_3HP_LESS15_ThirtyOneToThirtyFour = \"TPT 3HP (2-14years): 31-34 kgs\";\n" +
                "var TPT_3HP_LESS15_GreaterThan34 = \"TPT 3HP (2-14years): >34 kgs\";\n" +
                "var TPT_3HP_GREATER14_ThirtyToThirtyFive = \"TPT 3HP (>14years): 30-35 kgs\";\n" +
                "var TPT_3HP_GREATER14_ThirtySixToFortyFive = \"TPT 3HP (>14years): 36-45 kgs\";\n" +
                "var TPT_3HP_GREATER14_FortySixToFiftyFive = \"TPT 3HP (>14years): 46-55 kgs\";\n" +
                "var TPT_3HP_GREATER14_FiftySixToSeventy = \"TPT 3HP (>14years): 56-70 kgs\";\n" +
                "var TPT_3HP_GREATER14_GreaterSeventy = \"TPT 3HP (>14years): >70 kgs\";\n" +
                "var TPT_6Lfx_Less15_LessFive = \"TPT 6Lfx (<15years): <5 kgs\";\n" +
                "var TPT_6Lfx_Less15_FiveToNine = \"TPT 6Lfx (<15years): 5-9 kgs\";\n" +
                "var TPT_6Lfx_Less15_TenToFifteen = \"TPT 6Lfx (<15years): 10-15 kgs\";\n" +
                "var TPT_6Lfx_Less15_SixteenToTwentyThree = \"TPT 6Lfx (<15years): 16-23 kgs\";\n" +
                "var TPT_6Lfx_Less15_TwentyFourToThirtyFour = \"TPT 6Lfx (<15years): 24-34 kgs\";\n" +
                "var TPT_6Lfx_Greater14_LessFortyFive = \"TPT 6Lfx (>14years): <45 kgs\";\n" +
                "var TPT_6Lfx_Greater14_GreaterFortyFour = \"TPT 6Lfx (>14years): ≥45 kgs\";\n" +
                "var TPT_4R_Greater9 = \"TPT 4R (≥10years)\";\n" +
                "var TPT_4R_Less10 = \"TPT 4R (<10years)\";\n" +
                "var TPT_3RH_Less16_LessEight = \"TPT 3RH (0-15years): <=7 kgs\";\n" +
                "var TPT_3RH_Less16_EightToEleven = \"TPT 3RH (0-15years): 8-11 kgs\";\n" +
                "var TPT_3RH_Less16_TwelveToFifteen = \"TPT 3RH (0-15years): 12-15 kgs\";\n" +
                "var TPT_3RH_Less16_SixteenToTwentyFive = \"TPT 3RH (0-15years): 16-24 kgs\";\n" +
                "var TPT_3RH_Less16_GreaterTwentyFour = \"TPT 3RH (0-15years): >=25kgs\";\n" +
                "\n" +
                "var REGIMEN_6H = \"6H\";\n" +
                "var REGIMEN_3HP = \"3HP\";\n" +
                "var REGIMEN_3RH = \"3RH\";\n" +
                "var REGIMEN_6_LEVO = \"6Lfx\";\n" +
                "var REGIMEN_4R = \"4R\";\n" +
                "\n" +
                "if (age != null && weight != null && typeOfRegimen != null) {\n" +
                "if (REGIMEN_6H.equals(typeOfRegimen))\n" +
                "{\n" +
                "\tif (age < 10)\n" +
                "\t{\n" +
                "\n" +
                "\t\tif (weight <= 7)\n" +
                "\t\t\tweightBand = TPT_6H_LESS10_LESS7;\n" +
                "\n" +
                "\t\telse if (weight >= 8 && weight <= 11)\n" +
                "\t\t\tweightBand = TPT_6H_LESS10_EightToEleven;\n" +
                "\n" +
                "\t\telse if (weight >= 12 && weight <= 15)\n" +
                "\t\t\tweightBand = TPT_6H_LESS10_TwelveToFifteen;\n" +
                "\n" +
                "\t\telse if (weight >= 16 && weight <= 24)\n" +
                "\t\t\tweightBand = TPT_6H_LESS10_SixteenToTwentyFive;\n" +
                "\n" +
                "\t\telse\n" +
                "\t\t\tweightBand = TPT_6H_LESS10_GreaterThan25;\n" +
                "\t}\n" +
                "\telse\n" +
                "\t{\n" +
                "\t\tif (weight < 25)\n" +
                "\t\t\tweightBand = TPT_6H_GREATER10_LESS25;\n" +
                "\n" +
                "\t\telse if (weight >= 25 && weight <= 34)\n" +
                "\t\t\tweightBand = TPT_6H_GREATER10_TwentyFiveToThirtyFour;\n" +
                "\n" +
                "\t\telse if (weight >= 35 && weight <= 49)\n" +
                "\t\t\tweightBand = TPT_6H_GREATER10_ThirtyFiveToFourtyNine;\n" +
                "\n" +
                "\t\telse\n" +
                "\t\t\tweightBand = TPT_6H_GREATER10_GreaterThan50;\n" +
                "\t}\n" +
                "}\n" +
                "\n" +
                "else if (REGIMEN_3HP.equals(typeOfRegimen))\n" +
                "{\n" +
                "\tif (age >= 2 && age <= 14)\n" +
                "\t{\n" +
                "\t\tif (weight >= 10 && weight <= 15)\n" +
                "\t\t\tweightBand = TPT_3HP_LESS15_TenToFifteen;\n" +
                "\n" +
                "\t\telse if (weight >= 16 && weight <= 23)\n" +
                "\t\t\tweightBand = TPT_3HP_LESS15_SixteenToTwentyThree;\n" +
                "\n" +
                "\t\telse if (weight >= 24 && weight <= 30)\n" +
                "\t\t\tweightBand = TPT_3HP_LESS15_TwentyFourToThirty;\n" +
                "\n" +
                "\t\telse if (weight >= 31 && weight <= 34)\n" +
                "\t\t\tweightBand = TPT_3HP_LESS15_ThirtyOneToThirtyFour;\n" +
                "\n" +
                "\t\telse\n" +
                "\t\t\tweightBand = TPT_3HP_LESS15_GreaterThan34;\n" +
                "\t}\n" +
                "\n" +
                "\telse\n" +
                "\t{\n" +
                "\t\tif (weight >= 30 && weight <= 35)\n" +
                "\t\t\tweightBand = TPT_3HP_GREATER14_ThirtyToThirtyFive;\n" +
                "\n" +
                "\t\telse if (weight >= 36 && weight <= 45)\n" +
                "\t\t\tweightBand = TPT_3HP_GREATER14_ThirtySixToFortyFive;\n" +
                "\n" +
                "\t\telse if (weight >= 46 && weight <= 55)\n" +
                "\t\t\tweightBand = TPT_3HP_GREATER14_FortySixToFiftyFive;\n" +
                "\n" +
                "\t\telse if (weight >= 56 && weight <= 70)\n" +
                "\t\t\tweightBand = TPT_3HP_GREATER14_FiftySixToSeventy;\n" +
                "\n" +
                "\t\telse if (weight > 70)\n" +
                "\t\t\tweightBand = TPT_3HP_GREATER14_GreaterSeventy;\n" +
                "\t}\n" +
                "}\n" + "}" +
                "output.setWeightBand(weightBand);", 2, "TPT", null, null, clientId));
        return rules;
    }

    @Test
    public void test_ruleEngine_presumptiveOpenPublicToDiagnosedOnTreatmentPublic() throws IOException {
        Episode episode = new Episode(1L, 1L, 1L, false, null, "1,2,3", Utils.getCurrentDateNew(), Utils.getCurrentDateNew(), null, Utils.getCurrentDateNew(), null, 1L, "IndiaTbPublic", 1L);
        Stage stage = new Stage(1L, "PRESUMPTIVE_OPEN_PUBLIC", null, "Presumptive Open");
        Map<String, Object> inputData = new HashMap<>();
        EpisodeDto episodeDto = new EpisodeDto(episode, new ArrayList<>(), stage, null, new ArrayList<>(), new HashMap<>(), new HashMap<>(), new ArrayList<>(), new HashMap<>());
        inputData.putAll(Utils.objectToMap(episodeDto));
        inputData.put("TreatmentOutcome", null);
        inputData.put("TreatmentStartTimeStamp",Utils.getFormattedDateNew(Utils.getCurrentDateNew()));
        inputData.put("KeyPopulation", "Contact of Known TB Patients,Patient on immunosuppressants");
        when(rulesRepository.findAllByRuleNamespaceAndClientId(any(), any())).thenReturn(getDbRulesV4());
        Map<String, Object> result = ruleEngine.run(stageTransitionKeyValueInferenceEngine, inputData, episodeDto.getCurrentStageId(), false, clientId);
        Assert.assertNotNull(result);
        Assert.assertEquals("DIAGNOSED_ON_TREATMENT_PUBLIC", result.get("ToStage"));
    }

    @Test
    public void test_ruleEngine_noTransition_presumptiveOpenPublicToDiagnosedOnTreatmentPublic() throws IOException {
        Episode episode = new Episode(1L, 1L, 1L, false, null, "1,2,3", Utils.getCurrentDateNew(), Utils.getCurrentDateNew(), null, Utils.getCurrentDateNew(), null, 1L, "IndiaTbPublic", 1L);
        Stage stage = new Stage(1L, "PRESUMPTIVE_OPEN_PUBLIC", null, "Presumptive Open");
        Map<String, Object> inputData = new HashMap<>();
        EpisodeDto episodeDto = new EpisodeDto(episode, new ArrayList<>(), stage, null, new ArrayList<>(), new HashMap<>(), new HashMap<>(), new ArrayList<>(), new HashMap<>());
        inputData.putAll(Utils.objectToMap(episodeDto));
        inputData.put("TreatmentOutcome", null);
        inputData.put("TreatmentStartTimeStamp",Utils.getFormattedDateNew(Utils.getCurrentDateNew()));
        inputData.put("KeyPopulation", "xyz");
        when(rulesRepository.findAllByRuleNamespaceAndClientId(any(), any())).thenReturn(getDbRulesV4());
        Map<String, Object> result = ruleEngine.run(stageTransitionKeyValueInferenceEngine, inputData, episodeDto.getCurrentStageId(), false, clientId);
        Assert.assertNull(result);
    }

    List<Rules> getDbRulesV4 () {
        List<Rules> rules = new ArrayList<>();
        rules.add(new Rules(21L, RuleNamespace.STAGE_TRANSITION.name(),
                "var treatmentStartDate = input.get(\"TreatmentStartTimeStamp\");var outcome = input.get(\"TreatmentOutcome\");var typeOfEpisode = input.get(\"typeOfEpisode\");var validKeyPopulationForTreatment = {\"Contact of Known TB Patients\", \"Patient on immunosuppressants\", \"Silicosis\", \"Anti-TNF treatment\", \"Dialysis\", \"Transplantation\"};var keyPopulation = input.get(\"KeyPopulation\");boolean valid = false;if(null != keyPopulation && !keyPopulation.equals(\"\")) {  foreach(keyPopulationIterator : validKeyPopulationForTreatment) {\n" +
                        "        if(keyPopulation.contains(keyPopulationIterator)) {\n" +
                        "            valid = true;\n" +
                        "        }\n" +
                        "    }}return null != treatmentStartDate && typeOfEpisode.equals(\"IndiaTbPublic\") && (null == outcome ||outcome.equals(\"\")) && valid;","output.put(\"ToStage\", \"DIAGNOSED_ON_TREATMENT_PUBLIC\");", 0, "TB Stage Transitions from PRESUMPTIVE_OPEN_PUBLIC to DIAGNOSED_ON_TREATMENT_PUBLIC", 6L, 1L, clientId ));
        return rules;
    }

}
