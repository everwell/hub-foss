package com.everwell.transition.unit.service;

import com.everwell.transition.BaseTest;
import com.everwell.transition.constants.LocaliseKeyConstants;
import com.everwell.transition.exceptions.InternalServerErrorException;
import com.everwell.transition.model.response.android.TranslationCacheObject;
import com.everwell.transition.service.LocaliseApiService;
import com.everwell.transition.service.LocaliseService;
import com.everwell.transition.service.impl.LocaliseServiceImpl;
import com.everwell.transition.utils.CacheUtils;
import com.everwell.transition.utils.Utils;
import org.junit.Assert;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.ValueOperations;

import java.util.*;

import static com.everwell.transition.constants.LocaliseKeyConstants.explore_categories;
import static com.everwell.transition.constants.LocaliseKeyConstants.trending;
import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.when;

public class LocaliseServiceTest extends BaseTest {

    @InjectMocks
    LocaliseServiceImpl localiseService;

    @Mock
    LocaliseApiService localiseApiService;

    CacheUtils cacheUtils;

    @Mock
    RedisTemplate<String, Object> redisTemplate;

    @Mock
    private ValueOperations valueOperations;

    @Test
    public void getTranslationsTest() {
        String language = "en";
        String region = "IN";

        cacheUtils = new CacheUtils(redisTemplate);

        when(redisTemplate.opsForValue()).thenReturn(valueOperations);

        doNothing().when(valueOperations).set(anyString(), anyString(), anyLong(), any());

        Map<String, Object> defaultTranslationMap = new HashMap<String, Object>() {{
            put(trending, "");
            put(explore_categories, "Explore Categories");
        }};


        when (localiseApiService.getPatientAppTranslationsFromLocalise(eq(language), eq(region))).thenReturn(
            defaultTranslationMap
        );

        Map<String, String> map = localiseService.getTranslations(language, region);

        Assert.assertEquals("Trending", map.get("trending"));
        Assert.assertEquals("Explore Categories", map.get("explore_categories"));

    }

    @Test
    public void getTranslations_FromCacheTest() {
        String language = "en";
        String region = "IN";

        List<String> cacheList = new ArrayList<>();
        LocaliseKeyConstants.defaultTranslationMap.forEach((k, v) ->
                cacheList.add(Utils.asJsonString(new TranslationCacheObject(k, v)))
        );

        cacheUtils = new CacheUtils(redisTemplate);

        when(redisTemplate.opsForValue()).thenReturn(valueOperations);
        when(valueOperations.multiGet(anyList())).thenReturn(cacheList);

        Map<String, String> map = localiseService.getTranslations(language, region);

        Assert.assertEquals("Trending", map.get("trending"));
        Assert.assertEquals("Explore Categories", map.get("explore_categories"));

    }

    @Test(expected= InternalServerErrorException.class)
    public void getTranslations_FromCache_ErrorTest() {
        String language = "en";
        String region = "IN";

        List<String> cacheList = new ArrayList<>(LocaliseKeyConstants.defaultTranslationMap.keySet());

        cacheUtils = new CacheUtils(redisTemplate);

        when(redisTemplate.opsForValue()).thenReturn(valueOperations);
        when(valueOperations.multiGet(anyList())).thenReturn(cacheList);

        localiseService.getTranslations(language, region);

    }
}
