package com.everwell.transition.unit.controller;

import com.everwell.transition.BaseTest;
import com.everwell.transition.controller.ClientController;
import com.everwell.transition.exceptions.CustomExceptionHandler;
import com.everwell.transition.model.request.RegisterClientRequest;
import com.everwell.transition.model.response.ClientResponse;
import com.everwell.transition.service.ClientService;
import com.everwell.transition.utils.Utils;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

public class ClientControllerTest extends BaseTest {

    private MockMvc mockMvc;

    @Mock
    private ClientService clientService;

    @InjectMocks
    private ClientController clientController;

    private static Long id = 1L;
    private static String name = "Dummy Client";
    private static String password = "Dummy Password";

    private static final String MESSAGE_CLIENT_REGISTERED_SUCCESSFULLY = "client registered successfully";
    private static final String MESSAGE_NAME_REQUIRED = "name is required";
    private static final String MESSAGE_PASSWORD_REQUIRED = "password is required";
    private static final String MESSAGE_PASSWORD_LEN_CHECK = "password length should be less than 255 characters";

    @Before
    public void setup() {
        mockMvc = MockMvcBuilders.standaloneSetup(clientController)
                .setControllerAdvice(new CustomExceptionHandler())
                .build();
    }

    @Test
    public void testGetClientSuccess() throws Exception {
        String uri = "/v1/client";
        ClientResponse dummyClientResponse = new ClientResponse();
        dummyClientResponse.setId(id);
        dummyClientResponse.setName(name);

        when(clientService.getClientWithToken(any())).thenReturn(null);

        mockMvc.
                perform(
                        MockMvcRequestBuilders
                                .get(uri)
                                .header("x-client-id", id)
                                .accept(MediaType.APPLICATION_JSON_VALUE)
                )
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.success").value("true"));
    }

    @Test
    public void testPostClientSuccess() throws Exception {
        String uri = "/v1/client";
        RegisterClientRequest clientRequest = new RegisterClientRequest(name, password);

        when(clientService.registerClient(any())).thenReturn(null);

        mockMvc.
                perform(
                        MockMvcRequestBuilders
                                .post(uri)
                                .contentType(MediaType.APPLICATION_JSON)
                                .content(Utils.asJsonString(clientRequest))
                                .accept(MediaType.APPLICATION_JSON_VALUE)
                )
                .andExpect(status().isCreated())
                .andExpect(MockMvcResultMatchers.jsonPath("$.success").value("true"));
        ;
    }

    @Test
    public void testPostClientWithoutName() throws Exception {
        String uri = "/v1/client";
        RegisterClientRequest clientRequest = new RegisterClientRequest(null, password);

        mockMvc.
                perform(
                        MockMvcRequestBuilders
                                .post(uri)
                                .contentType(MediaType.APPLICATION_JSON)
                                .content(Utils.asJsonString(clientRequest))
                                .accept(MediaType.APPLICATION_JSON_VALUE)
                )
                .andExpect(status().isBadRequest())
                .andExpect(MockMvcResultMatchers.jsonPath("$.message").value(MESSAGE_NAME_REQUIRED));
        ;

    }

    @Test
    public void testPostClientWithoutPassword() throws Exception {
        String uri = "/v1/client";
        RegisterClientRequest clientRequest = new RegisterClientRequest(name, null);

        mockMvc.
                perform(
                        MockMvcRequestBuilders
                                .post(uri)
                                .contentType(MediaType.APPLICATION_JSON)
                                .content(Utils.asJsonString(clientRequest))
                                .accept(MediaType.APPLICATION_JSON_VALUE)
                )
                .andExpect(status().isBadRequest())
                .andExpect(MockMvcResultMatchers.jsonPath("$.message").value(MESSAGE_PASSWORD_REQUIRED));
        ;
    }

    @Test
    public void testPostClient_PasswordGreaterThan255Chars() throws Exception {
        String uri = "/v1/client";
        RegisterClientRequest clientRequest = new RegisterClientRequest(name,
                new String(new char[256]).replace("\0", "a"));

        mockMvc.
                perform(
                        MockMvcRequestBuilders
                                .post(uri)
                                .contentType(MediaType.APPLICATION_JSON)
                                .content(Utils.asJsonString(clientRequest))
                                .accept(MediaType.APPLICATION_JSON_VALUE)
                )
                .andExpect(status().isBadRequest())
                .andExpect(MockMvcResultMatchers.jsonPath("$.message").value(MESSAGE_PASSWORD_LEN_CHECK));
        ;
    }

}
