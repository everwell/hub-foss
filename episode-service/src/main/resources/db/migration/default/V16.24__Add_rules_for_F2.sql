INSERT INTO public.rules(
	id,action, condition, description, priority, rule_namespace, to_id, client_id)
	VALUES ((select COALESCE(MAX(id), 0) + 1 from rules),'output.put("ToStage","PRESUMPTIVE_OPEN");', 'var typeOfEpisode = input.get("typeOfEpisode");return (typeOfEpisode.equals("F2Public"));', 'Stage Transitions to ON_TREATMENT while registration for F2', 1, 'STAGE_TRANSITION', (select id from stages where stage_name = 'PRESUMPTIVE_OPEN'), 63);

UPDATE rules
set priority = 2
where description in ('Stage Transitions to ON_TREATMENT from OFF_TREATMENT for clientId 63', 'Stage Transitions to OFF_TREATMENT from ON_TREATMENT for clientId 63');

