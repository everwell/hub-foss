INSERT INTO public.rules (id, action, condition, description, priority, rule_namespace)
VALUES (3, 'output.put("ToStage", "PRESUMPTIVE_OPEN");',
        'return null != input.get("Stage") && input.get("Stage").equals("PRESUMPTIVE_CLOSED") ' ||
        '&& (null == input.get("TreatmentOutcome") || input.get("TreatmentOutcome").equals(""));',
        'TB Stage Transitions from PRESUMPTIVE_CLOSED to PRESUMPTIVE_OPEN', 1,
        'STAGE_TRANSITION')
ON CONFLICT DO NOTHING;