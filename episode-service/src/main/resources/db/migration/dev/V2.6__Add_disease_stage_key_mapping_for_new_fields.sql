
-- AddedBy and AddedByType fields to be added only when stage ids are
--- 1 and 2 i.e presumptive open for public and private and
-- disease stage ids are 1,2,11,12, 21, 22
-- disease template 1 (stage id 1, 2) -> 1, 2
-- disease template 2 (stage is 1,2) -> 11, 12
-- disease template 3 (stage is 1,2) -> 21, 22
-- Required status -> Both the fields are required

DO
$do$
    declare
        disease_stage_ids int[] := array[1, 2, 11, 12, 21, 22];
        var int;
    BEGIN
        foreach var in array disease_stage_ids loop

          IF NOT EXISTS (SELECT * from disease_stage_key_mapping where disease_stage_id = var and field_id = (select id from field where key = 'addedBy'and module = 'Person' limit 1) ) THEN
          insert into disease_stage_key_mapping (id, default_value, deleted, disease_stage_id, required, field_id) VALUES ((select max(id) + 1 from disease_stage_key_mapping), null, false, var, true, (select id from field where key = 'addedBy'and module = 'Person' limit 1));
          end if;

          IF NOT EXISTS (SELECT * from disease_stage_key_mapping where disease_stage_id = var and field_id = (select id from field where key = 'addedByType'and module = 'Person' limit 1) ) THEN
          insert into disease_stage_key_mapping (id, default_value, deleted, disease_stage_id, required, field_id) VALUES ((select max(id) + 1 from disease_stage_key_mapping), null, false, var, true, (select id from field where key = 'addedByType'and module = 'Person' limit 1));
          end if;
        end loop ;
end;
$do$;


--- dataConsent, dateOfBirth, mobileList, emailList to be added only when
-- stage ids are -- 1, 2 4, 5, 6 -- presumptive open (private/public), on treatment (public/private), diagnosed not on treatment
-- disease stage ids are 1,2,4,5,6,11,12,14,15,16, 21, 22, 24, 25, 26
-- disease template 1 (Stage id 1, 2, 4, 5, 6) ->  1, 2, 4, 5,6
-- disease template 2 (Stage id 1, 2, 4, 5, 6) -> 11, 12, 14, 15, 16
-- disease template 3 (Stage id 1, 2, 4, 5, 6) -> 21, 22, 24, 25, 26
-- required fields -> mobileList


DO
$do$
    declare
        disease_stage_ids int[] := array[1,2,4,5,6,11,12,14,15,16, 21, 22, 24, 25, 26];
        var int;
    BEGIN
        foreach var in array disease_stage_ids loop
        IF NOT EXISTS (SELECT * from disease_stage_key_mapping where disease_stage_id = var and field_id = (select id from field where key = 'dataConsent'and module = 'Person' limit 1) ) THEN
          insert into disease_stage_key_mapping (id, default_value, deleted, disease_stage_id, required, field_id) VALUES ((select max(id) + 1 from disease_stage_key_mapping), null, false, var, false, (select id from field where key = 'dataConsent'and module = 'Person' limit 1));
        end if;

        IF NOT EXISTS (SELECT * from disease_stage_key_mapping where disease_stage_id = var and field_id = (select id from field where key = 'dateOfBirth'and module = 'Person' limit 1) ) THEN
          insert into disease_stage_key_mapping (id, default_value, deleted, disease_stage_id, required, field_id) VALUES ((select max(id) + 1 from disease_stage_key_mapping), null, false, var, false, (select id from field where key = 'dateOfBirth'and module = 'Person' limit 1));
        end if;


        IF NOT EXISTS (SELECT * from disease_stage_key_mapping where disease_stage_id = var and field_id = (select id from field where key = 'mobileList'and module = 'Person' limit 1) ) THEN
          insert into disease_stage_key_mapping (id, default_value, deleted, disease_stage_id, required, field_id) VALUES ((select max(id) + 1 from disease_stage_key_mapping), null, false, var, true, (select id from field where key = 'mobileList'and module = 'Person' limit 1));
        end if;

        IF NOT EXISTS (SELECT * from disease_stage_key_mapping where disease_stage_id = var and field_id = (select id from field where key = 'emailList'and module = 'Person' limit 1) ) THEN
          insert into disease_stage_key_mapping (id, default_value, deleted, disease_stage_id, required, field_id) VALUES ((select max(id) + 1 from disease_stage_key_mapping), null, false, var, false, (select id from field where key = 'emailList'and module = 'Person' limit 1));
        end if;

    end loop ;
end;
$do$;
