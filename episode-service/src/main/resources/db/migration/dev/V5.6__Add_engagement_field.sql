DO
$do$
    BEGIN

    IF NOT EXISTS (SELECT * from field where key = 'householdVisits' and module = 'Episode') THEN
      insert into field (id, key, module) values((select max(id)+1 from field), 'householdVisits', 'Episode');
    end if;

    IF NOT EXISTS (SELECT * from field where key = 'callCenterCalls' and module = 'Episode') THEN
      insert into field (id, key, module) values((select max(id)+1 from field), 'callCenterCalls', 'Episode');
    end if;

    IF NOT EXISTS (SELECT * from field where key = 'engagementExists' and module = 'Episode') THEN
          insert into field (id, key, module) values((select max(id)+1 from field), 'engagementExists', 'Episode');
        end if;


end;
$do$