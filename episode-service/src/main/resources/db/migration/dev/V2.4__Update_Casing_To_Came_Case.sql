update field
set key = lower(substring(key from 1 for 1)) || substring(key from 2 for length(key));

update supported_relation
set name = lower(substring(name from 1 for 1 )) || substring(name from 2 for length(name));

DO
$do$
BEGIN
IF EXISTS (SELECT * from field where key = 'ePSite') THEN
  update field set key = 'epSite' where key = 'ePSite';
end if;

IF EXISTS (SELECT * from field where key = 'hIVTestStatus') THEN
  update field set key = 'hivTestStatus' where key = 'hIVTestStatus';
end if;

IF EXISTS (SELECT * from field where key = 'tBCategory') THEN
  update field set key = 'tbCategory' where key = 'tBCategory';
end if;

end;
$do$;


DO
$do$
BEGIN
IF EXISTS (SELECT * from supported_relation where name = 'aRT') THEN
  update supported_relation set name = 'art' where name = 'aRT';
end if;

IF EXISTS (SELECT * from supported_relation where name = 'dRTB') THEN
  update supported_relation set name = 'drtb' where name = 'dRTB';
end if;

IF EXISTS (SELECT * from supported_relation where name = 'pVTHUB') THEN
  update supported_relation set name = 'pvtHub' where name = 'pVTHUB';
end if;

IF EXISTS (SELECT * from supported_relation where name = 'aRTNumber') THEN
  update supported_relation set name = 'artNumber' where name = 'aRTNumber';
end if;

IF EXISTS (SELECT * from supported_relation where name = 'tBNumber') THEN
  update supported_relation set name = 'tbNumber' where name = 'tBNumber';
end if;

end;
$do$;

update rules
set condition =  Replace(condition, 'TreatmentOutcome', 'treatmentOutcome');

update rules
set condition =  Replace(condition, 'Reason', 'reason') ;

update rules
set condition = Replace(condition, 'Status', 'status');

update rules
set condition =  Replace(condition, 'TreatmentStartTimeStamp', 'treatmentStartTimeStamp');

