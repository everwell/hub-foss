DO
$do$
    declare
        disease_stage_ids int[] := ARRAY(SELECT id FROM disease_stage_mapping
                                     where stage_id in (SELECT id FROM stages where stage_name in ('PRESUMPTIVE_OPEN_PUBLIC', 'PRESUMPTIVE_OPEN_PRIVATE', 'DIAGNOSED_NOT_ON_TREATMENT', 'DIAGNOSED_ON_TREATMENT_PRIVATE', 'DIAGNOSED_ON_TREATMENT_PUBLIC'  ))
                                     and disease_template_id in (select id from disease_template dt where disease_name in ('Drug Resistant Tuberculosis', 'Drug Sensitive Tuberculosis', 'Latent Tuberculosis Infection')));

        var int;
    BEGIN
        IF NOT EXISTS (SELECT * from field where key = 'deletedBy' and module = 'Person') THEN
            insert into field (id, key, module) values((select max(id)+1 from field), 'deletedBy', 'Person');
        end if;

        IF NOT EXISTS (SELECT * from field where key = 'deletedByType' and module = 'Person') THEN
          insert into field (id, key, module) values((select max(id)+1 from field), 'deletedByType', 'Person');
        end if;

        foreach var in array disease_stage_ids loop
            IF NOT EXISTS (SELECT * from disease_stage_key_mapping where disease_stage_id = var and field_id = (select id from field where key = 'isDuplicate' and module = 'Episode' limit 1) ) THEN
              insert into disease_stage_key_mapping (id, default_value, deleted, disease_stage_id, required, field_id) VALUES ((select max(id) + 1 from disease_stage_key_mapping), null, false, var, true, (select id from field where key = 'isDuplicate' and module = 'Episode' limit 1));
            end if;

            IF NOT EXISTS (SELECT * from disease_stage_key_mapping where disease_stage_id = var and field_id = (select id from field where key = 'duplicateStatus' and module = 'Episode' limit 1) ) THEN
              insert into disease_stage_key_mapping (id, default_value, deleted, disease_stage_id, required, field_id) VALUES ((select max(id) + 1 from disease_stage_key_mapping), null, false, var, true, (select id from field where key = 'duplicateStatus' and module = 'Episode' limit 1));
            end if;

            IF NOT EXISTS (SELECT * from disease_stage_key_mapping where disease_stage_id = var and field_id = (select id from field where key = 'duplicateOf' and module = 'Episode' limit 1) ) THEN
              insert into disease_stage_key_mapping (id, default_value, deleted, disease_stage_id, required, field_id) VALUES ((select max(id) + 1 from disease_stage_key_mapping), null, false, var, true, (select id from field where key = 'duplicateOf' and module = 'Episode' limit 1));
            end if;

            IF NOT EXISTS (SELECT * from disease_stage_key_mapping where disease_stage_id = var and field_id = (select id from field where key = 'deletedBy' and module = 'Person' limit 1) ) THEN
                insert into disease_stage_key_mapping (id, default_value, deleted, disease_stage_id, required, field_id) VALUES ((select max(id) + 1 from disease_stage_key_mapping), null, false, var, false, (select id from field where key = 'deletedBy' and module = 'Person' limit 1));
            end if;

            IF NOT EXISTS (SELECT * from disease_stage_key_mapping where disease_stage_id = var and field_id = (select id from field where key = 'deletedByType' and module = 'Person' limit 1) ) THEN
              insert into disease_stage_key_mapping (id, default_value, deleted, disease_stage_id, required, field_id) VALUES ((select max(id) + 1 from disease_stage_key_mapping), null, false, var, false, (select id from field where key = 'deletedByType' and module = 'Person' limit 1));
            end if;
    end loop ;
end;
$do$;