-- Sequence has to be created and added as default value for 'id' column in disease_template, stages, fields, disease_stage_mapping, disease_stage_key_mapping and rules

DO $$

DECLARE
	v_d2c_client_id INTEGER := 0;
	v_new_disease_ids INTEGER[];
	v_new_stage_id INTEGER;
	v_new_DS_mapping_ids INTEGER[];
	v_new_field_ids INTEGER[];
	v_existing_field_ids INTEGER[];
	DS_id INTEGER;
	new_field_id INTEGER;
	existing_field_id INTEGER;

BEGIN

    -- Add client_id column to disease_template
    ALTER TABLE IF EXISTS disease_template
    ADD COLUMN IF NOT EXISTS client_id bigint DEFAULT 29;

	-- D2C client search
  	SELECT id INTO v_d2c_client_id FROM client WHERE name = 'HHCPlus';
	IF v_d2c_client_id > 0 THEN

		-- New diseases
		WITH new_diseases AS (
			INSERT INTO disease_template (disease_name, client_id)
			VALUES
				('TB', v_d2c_client_id),
				('Hypertension', v_d2c_client_id),
				('HIV', v_d2c_client_id),
				('Diabetes', v_d2c_client_id)
			RETURNING id)
		SELECT array_agg(id) INTO v_new_disease_ids FROM new_diseases;

		-- New stage
		INSERT INTO stages (stage_name, stage_display_name) VALUES ('ON_TREATMENT', 'Treatment') RETURNING id INTO v_new_stage_id;

		-- Add disease_stage_mapping with newly created diseases and stage
		WITH new_DS_mappings AS (
			INSERT INTO disease_stage_mapping (disease_template_id, stage_id)
			VALUES
				(v_new_disease_ids[1], v_new_stage_id),
				(v_new_disease_ids[2], v_new_stage_id),
				(v_new_disease_ids[3], v_new_stage_id),
				(v_new_disease_ids[4], v_new_stage_id)
			RETURNING id)
		SELECT array_agg(id) INTO v_new_DS_mapping_ids FROM new_DS_mappings;

		-- New fields
		WITH new_fields AS (
			INSERT INTO field (key, module)
			VALUES
                ('LastRefillDate', 'Dispensation'),
                ('NumberOfDaysOfMedication', 'Dispensation'),
                ('TypeOfDosing', 'Adherence')
			RETURNING id)
		SELECT array_agg(id) INTO v_new_field_ids FROM new_fields;

		-- Get IDs of existing fields
		SELECT array_agg(id) INTO v_existing_field_ids FROM field where key in ('FirstName', 'LastName', 'Age', 'PrimaryPhone', 'MonitoringMethod', 'RegimenType');

		-- Add disease_stage_key_mapping for existing and newly created fields and disease_stage_mappings
		FOREACH DS_id IN ARRAY v_new_DS_mapping_ids
	   	LOOP

			-- For new fields
			FOREACH new_field_id IN ARRAY v_new_field_ids
			LOOP
				INSERT INTO disease_stage_key_mapping (disease_stage_id, required, deleted, default_value, field_id)
				VALUES (DS_id, true, false, null, new_field_id);
			END LOOP;

			-- For existing fields
			FOREACH existing_field_id IN ARRAY v_existing_field_ids
			LOOP
				INSERT INTO disease_stage_key_mapping (disease_stage_id, required, deleted, default_value, field_id)
				VALUES (DS_id, true, false, null, existing_field_id);
			END LOOP;

	   	END LOOP;

        -- RULES
	   	INSERT INTO rules(action, condition, description, priority, rule_namespace, to_id)
        VALUES(
            'output.put("ToStage","ON_TREATMENT");',
            'var clientId = input.get("clientId");return clientId==' || v_d2c_client_id || ';',
            'TB stage Transitions to ON_TREATMENT for clientId ' || v_d2c_client_id,
            1,
            'STAGE_TRANSITION',
            v_new_stage_id);

	END IF;
END $$;
