DO
$do$
BEGIN

IF NOT EXISTS (SELECT * from field where key = 'addedBy' and module = 'Person') THEN
  insert into field (id, key, module) values((select max(id)+1 from field), 'addedBy', 'Person');
end if;

IF NOT EXISTS (SELECT * from field where key = 'addedByType' and module = 'Person') THEN
  insert into field (id, key, module) values((select max(id)+1 from field), 'addedByType', 'Person');
end if;

IF NOT EXISTS (SELECT * from field where key = 'dataConsent' and module = 'Person') THEN
  insert into field (id, key, module) values((select max(id)+1 from field), 'dataConsent', 'Person');
end if;

IF NOT EXISTS (SELECT * from field where key = 'dateOfBirth' and module = 'Person') THEN
  insert into field (id, key, module) values((select max(id)+1 from field), 'dateOfBirth', 'Person');
end if;

IF NOT EXISTS (SELECT * from field where key = 'mobileList' and module = 'Person') THEN
  insert into field (id, key, module) values((select max(id)+1 from field), 'mobileList', 'Person');
end if;

IF NOT EXISTS (SELECT * from field where key = 'emailList' and module = 'Person') THEN
  insert into field (id, key, module) values((select max(id)+1 from field), 'emailList', 'Person');
end if;

IF EXISTS (SELECT * from field where key = 'fathersName' and module = 'Person') THEN
  update field set key = 'fatherName' where key = 'fathersName' and module = 'Person';
end if;

IF EXISTS (SELECT * from field where key = 'socioeconomicStatus' and module = 'Person') THEN
  update field set key = 'socioEconomicStatus' where key = 'socioeconomicStatus' and module = 'Person';
end if;

IF EXISTS (SELECT * from field where key = 'primaryPhone' and module = 'Person') THEN
  update field set key = 'primaryPhoneNumber' where key = 'primaryPhone' and module = 'Person';
end if;
end;
$do$