DO
$do$
    declare
        _client_id_list int[] := array[171];
        _client_id int;
        _disease_template_name_list varchar[] := array['Other', 'TB'];
        _disease_template_id int;
        _presumptive_open_stage_id int;
        _presumptive_closed_stage_id int;
        _on_treatment_stage_id int;
        _basic_details_tab_id varchar;
        _adherence_tab_id varchar;
        _medication_tab_id varchar;
        _consultation_tab_id varchar;
        _disease_stage_id int;
    begin
        update supported_tab set type = 'Basic Details' where type = 'basic_details';
        if not exists(select id from supported_tab where type = 'basic_details' or type = 'Basic Details') then
            if not exists(select id from supported_tab) then
                insert into supported_tab(id, type, description) values (1, 'Basic Details', null);
            else
                insert into supported_tab(id, type, description) values ((select max(id) +1 from supported_tab), 'Basic Details', null);
            end if;
        end if;
        if not exists(select id from supported_tab where type = 'Adherence') then
            insert into supported_tab(id, type, description) values ((select max(id) +1 from supported_tab), 'Adherence', null);
        end if;
        if not exists(select id from supported_tab where type = 'Medication') then
            insert into supported_tab(id, type, description) values ((select max(id) +1 from supported_tab), 'Medication', null);
        end if;
        if not exists(select id from supported_tab where type = 'Consultations') then
            insert into supported_tab(id, type, description) values ((select max(id) +1 from supported_tab), 'Consultations', null);
        end if;
        select id into _presumptive_open_stage_id from stages where stage_name = 'PRESUMPTIVE_OPEN';
        select id into _presumptive_closed_stage_id from stages where stage_name = 'PRESUMPTIVE_CLOSED';
 	   	select id into _on_treatment_stage_id from stages where stage_name = 'ON_TREATMENT';
        select cast(id as varchar) into _basic_details_tab_id  from supported_tab where type = 'basic_details' or type = 'Basic Details';
        select cast(id as varchar) into _adherence_tab_id from supported_tab where type = 'Adherence';
        select cast(id as varchar) into _medication_tab_id from supported_tab where type = 'Medication';
        select cast(id as varchar) into _consultation_tab_id from supported_tab where type = 'Consultations';
        foreach _client_id  in array _client_id_list loop
              for i in 1 .. array_upper(_disease_template_name_list, 1) loop
               select id into _disease_template_id from disease_template where disease_name = _disease_template_name_list[i] AND client_id = _client_id;
                    -- Enable Basic details for all 3 stages
                    select id into _disease_stage_id from disease_stage_mapping where stage_id = _presumptive_open_stage_id and disease_template_id = _disease_template_id;
                    if not exists(select id from tab_permission where disease_stage_mapping_id = _disease_stage_id and tab_id = _basic_details_tab_id) then
                        insert into tab_permission (id, disease_stage_mapping_id, tab_id, add, edit, view, delete)
                        values ((select max(id)+1 from tab_permission), _disease_stage_id, _basic_details_tab_id, false, true, true, false);
                    end if;
                    select id into _disease_stage_id from disease_stage_mapping where stage_id = _presumptive_closed_stage_id and disease_template_id = _disease_template_id;
                    if not exists(select id from tab_permission where disease_stage_mapping_id = _disease_stage_id and tab_id = _basic_details_tab_id) then
                        insert into tab_permission (id, disease_stage_mapping_id, tab_id, add, edit, view, delete)
                        values ((select max(id)+1 from tab_permission), _disease_stage_id, _basic_details_tab_id, false, true, true, false);
                    end if;
                    select id into _disease_stage_id from disease_stage_mapping where stage_id = _on_treatment_stage_id and disease_template_id = _disease_template_id;
                    if not exists(select id from tab_permission where disease_stage_mapping_id = _disease_stage_id and tab_id = _basic_details_tab_id) then
                        insert into tab_permission (id, disease_stage_mapping_id, tab_id, add, edit, view, delete)
                        values ((select max(id)+1 from tab_permission), _disease_stage_id, _basic_details_tab_id, false, true, true, false);
                    end if;
                    -- Enable Consultation for all 3 stages
                    select id into _disease_stage_id from disease_stage_mapping where stage_id = _presumptive_open_stage_id and disease_template_id = _disease_template_id;
                    if not exists(select id from tab_permission where disease_stage_mapping_id = _disease_stage_id and tab_id = _consultation_tab_id) then
                        insert into tab_permission (id, disease_stage_mapping_id, tab_id, add, edit, view, delete)
                        values ((select max(id)+1 from tab_permission), _disease_stage_id, _consultation_tab_id, false, true, true, false);
                    end if;
                    select id into _disease_stage_id from disease_stage_mapping where stage_id = _presumptive_closed_stage_id and disease_template_id = _disease_template_id;
                    if not exists(select id from tab_permission where disease_stage_mapping_id = _disease_stage_id and tab_id = _consultation_tab_id) then
                        insert into tab_permission (id, disease_stage_mapping_id, tab_id, add, edit, view, delete)
                        values ((select max(id)+1 from tab_permission), _disease_stage_id, _consultation_tab_id, false, true, true, false);
                    end if;
                    select id into _disease_stage_id from disease_stage_mapping where stage_id = _on_treatment_stage_id and disease_template_id = _disease_template_id;
                    if not exists(select id from tab_permission where disease_stage_mapping_id = _disease_stage_id and tab_id = _consultation_tab_id) then
                        insert into tab_permission (id, disease_stage_mapping_id, tab_id, add, edit, view, delete)
                        values ((select max(id)+1 from tab_permission), _disease_stage_id, _consultation_tab_id, false, true, true, false);
                    end if;
                    -- Enable Medication for all 3 stages
                    select id into _disease_stage_id from disease_stage_mapping where stage_id = _presumptive_open_stage_id and disease_template_id = _disease_template_id;
                    if not exists(select id from tab_permission where disease_stage_mapping_id = _disease_stage_id and tab_id = _medication_tab_id) then
                        insert into tab_permission (id, disease_stage_mapping_id, tab_id, add, edit, view, delete)
                        values ((select max(id)+1 from tab_permission), _disease_stage_id, _medication_tab_id, false, true, true, false);
                    end if;
                    select id into _disease_stage_id from disease_stage_mapping where stage_id = _presumptive_closed_stage_id and disease_template_id = _disease_template_id;
                    if not exists(select id from tab_permission where disease_stage_mapping_id = _disease_stage_id and tab_id = _medication_tab_id) then
                        insert into tab_permission (id, disease_stage_mapping_id, tab_id, add, edit, view, delete)
                        values ((select max(id)+1 from tab_permission), _disease_stage_id, _medication_tab_id, false, true, true, false);
                    end if;
                    select id into _disease_stage_id from disease_stage_mapping where stage_id = _on_treatment_stage_id and disease_template_id = _disease_template_id;
                    if not exists(select id from tab_permission where disease_stage_mapping_id = _disease_stage_id and tab_id = _medication_tab_id) then
                        insert into tab_permission (id, disease_stage_mapping_id, tab_id, add, edit, view, delete)
                        values ((select max(id)+1 from tab_permission), _disease_stage_id, _medication_tab_id, false, true, true, false);
                    end if;
                    -- Enable Adherence for on treatment
                    select id into _disease_stage_id from disease_stage_mapping where stage_id = _on_treatment_stage_id and disease_template_id = _disease_template_id;
                    if not exists(select id from tab_permission where disease_stage_mapping_id = _disease_stage_id and tab_id = _adherence_tab_id) then
                        insert into tab_permission (id, disease_stage_mapping_id, tab_id, add, edit, view, delete)
                        values ((select max(id)+1 from tab_permission), _disease_stage_id, _adherence_tab_id, true, true, true, false);
                    end if;
              end loop ;
        end loop ;
    END

$do$;
