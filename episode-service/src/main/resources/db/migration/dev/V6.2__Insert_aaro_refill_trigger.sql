insert into trigger (trigger_id, client_id, default_template_id, template_ids,
    event_name, function_name, cron_time, notification_type,
    vendor_id, time_zone)
    values((select max(id) + 1 from trigger), 171, 27, '27', 'weeklyRefillReminderAaro', 'getAaroRefillReminderPNDTOsForClientId', '* 16 * * *', 'Push Notification', 21, 'Asia/Kolkata');
