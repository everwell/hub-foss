DO
$do$
    DEClARE
        _client_id_list int[] := array[171];
        _client_id int;
        _disease_template_name_list varchar[] := array['Abdominal Pain','Alcohol Abuse and Alcoholism','Alopecia (hair loss)','Anxiety','Appendicitis','Autism','Acquired Immuno Deficiency Syndrome','Alzheimer''s Disease','Anaemia','Arthritis','Asthma','Airbag Eye Injury','Anthrax','Amoebiasis','Atherosclerosis','Adult Inclusion Conjunctivitis','Atopic Keratoconjunctivitis','Avian influenza in humans','Astigmatism','Aphakia','Anisometropia','Aniseikonia','Asbestos-related diseases','Amblyopia','Ankyloblepharon','Abnormal uterine bleeding','Acquired Capillary Haemangioma of Eyelid','Anaemia during pregnancy (Maternal anemia)','Antepartum hemorrhage (Bleeding in late pregnancy)','Acute encephalitis syndrome','Amaurosis Fugax','Abducens nerve Palsy','Attention-Deficit Hyperactivity Disorder','Blindness','Breast Cancer / Carcinoma','Bronchitis','Brucellosis','Bedsores','Back Pain','Bell''s Palsy','Brain Tumour','Benign Essential Blepharospasm','Blepharitis','Band Shaped Keratopathy','Burns','Blepharochalasis','Bad Breath (Halitosis)','Bleeding Gums','Bruxism (Teeth Grinding)','Cancer','Carpal Tunnel Syndrome','Chickenpox','Chikungunya Fever','Cholera','Coronary Heart Disease','Cough','Childhood Exotropia','Chalazion','Crimean-Congo haemorrhagic fever (CCHF)','Chronic obstructive pulmonary disease (COPD)','Coronavirus disease 2019 (COVID-19)','Conjunctivochalasis','Conjunctival Concretions','Congenital anomalies (birth defects)','Cervical cancer','Colorectal Cancer','Crimean Congo haemorrhagic fever (CCHF)','Chemical Injuries to the Eyes','Coronavirus disease 2019 (COVID-19)','Computer Vision Syndrome','Child maltreatment (child abuse)','Congenital Capillary Haemangioma of Eyelid','Cavernous Haemangioma of Eyelid','Corneal Abrasion','Chalcosis','Commotio Retinae','Cavities','Cleft Lip and Cleft Palate','Diarrhea','Deafness','Dementia','Dengue Fever','Diabetes Mellitus','Down''s Syndrome','Dacryoadenitis','Diabetic Retinopathy','Duane Retraction Syndrome','Dacryocystitis','Diphtheria','Dracunculiasis (guinea-worm disease)','Disabilities','Dermatochalasis','Distichiasis','Double Elevator Palsy','Digit-Sucking (Thumb/ Finger-Sucking)','Dyslexia and Ocular Features','Ebola Virus Disease (EVD)','Eczema','Endometriosis','Epilepsy','Ectropion','Entropion','Epidemic Keratoconjunctivitis','Epibulbar Dermoids','Epidemic dropsy','Epiblepharon','Epicanthus','Euryblepharon','Ectopic pregnancy','Exposure Keratopathy','Eyelid Varix','Early pregnancy loss','Eclampsia','Fracture (Bone fracture)','Fibroids','Filariasis','Food Poisoning','Frey''s Syndrome','Frost Bite','Filamentary Keratitis','Fluorosis','Floppy Eyelid Syndrome','Factitious Keratoconjunctivitis','Fibromyalgia','Female genital mutilation','Gangrene','Gastro-Esophageal Reflux Disease (GERD)','Glaucoma','Goitre','Gonorrhea','Guillain-Barré syndrome','Giant Papillary Conjunctivitis','Gaming disorder','Gum disease','Hepatitis','Herpes Simplex','Haemophilia','Hemangioma','Horner Syndrome','Hypertensive Retinopathy','Helminthiasis (Soil-transmitted helminth infections)','Human papillomavirus (HPV) infection and cervical cancer','Heat-Related Illnesses and Heat waves','Hand, Foot and Mouth Disease','Hypermetropia','Heterophoria','Hypervitaminosis A','High risk pregnancy','Ichthyosis','Inflammatory Bowel Disease','Insomnia','Iron Deficiency Anemia','Infertility','Intrauterine growth restriction (IUGR)','Japanese Encephalitis','Jaundice','Kala-azar/ Leishmaniasis','Kaposi’s Sarcoma','Keratosis Pilaris','Keratoconjunctivitis Sicca (Dry eye syndrome)','Kyasanur forest disease','Keratoconus','Keratoglobus','Leishmaniasis/ Kala-azar','Lichen Planus','Leprosy','Leptospirosis','Leukemia','Lymphoedema','Ligneous Conjunctivitis','Low Vision and Visual Aids','Lid Imbrication Syndrome','Lagophthalmos','Lead poisoning','lactose intolerance','Long COVID/Post-COVID conditions','Measles(Khasra)','Malaria','Meningitis','Migraine','Myocardial Infarction (Heart Attack)','Microcephaly','Mooren''s Ulcer','Middle East respiratory syndrome coronavirus (MERS‐CoV)','Myopia','Mumps','Madarosis','Mastitis','Mouth Ulcers','Malocclusion','Mucormycosis','Mouth Breathing','Monkeypox','Narcolepsy','Nasal Polyps','Neonatal Respiratory Disease Syndrome(NRDS)','Neuralgia','Nipah virus infection','Neonatal Conjunctivitis','Neurotrophic Keratopathy','Nausea and Vomiting of Pregnancy and  Hyperemesis gravidarum','Nonalcoholic fatty liver disease(NAFLD)','Obsessive Compulsive Disorder','Oral Cancer','Osteomyelitis','Osteoporosis','Otitis Media','Orbital Dermoid','Ocular Graft-versus-host Disease','Obesity','Osteoarthritis','Orbital Varix','Orbital Fat Prolapse','Orbital Cavernous Haemangioma','Orbital Solitary Fibrous Tumour','Orbital Haemangiopericytoma','Orbital Lymphangioma','Psoriasis','Post Menopausal Bleeding','Parkinson''s Disease','Pericarditis','Pneumonia','Poliomyelitis','Post Polio Syndrome','papilloedema','Pterygium','Pharyngoconjunctival Fever','Polycystic ovary syndrome (PCOS)','Perennial Allergic Conjunctivitis','Phlyctenular Keratoconjunctivitis','Plague','Pinguecula','Preterm birth','Preeclampsia','Pellucid Marginal Degeneration','Presbyopia','Premenstrual syndrome','Pseudophakia','Pseudostrabismus','Puerperal sepsis','Postpartum haemorrhage','Photophthalmia','Post-herpetic neuralgia','Postpartum depression/ Perinatal depression','Q Fever','Quinsy','Rabies','Raynaud''s Phenomenon','Rubella','Retinopathy of Prematurity','Recurrent Corneal Erosion Syndrome','Rheumatic fever and rheumatic heart disease','Ramsay-Hunt Syndrome','Rare diseases','Reiter’s Syndrome','Sarcoidosis','Sarcoma','Schizophrenia','Scurvy','Severe Acute Respiratory Syndrome (SARS)','Swine Flu','Syphilis','Superior Limbic Keratoconjunctivitis','Stroke','Scrub Typhus','Sexually transmitted infections (STIs)','Shaken Baby Syndrome','Seasonal Allergic Conjunctivitis','Spheroidal Degeneration','Salzmann''s Nodular Degeneration','Spinal cord Injury','Substance abuse','Scabies','Solar Retinopathy','Steatoblepharon','Silicosis','Sub-conjunctival Haemorrhage','Siderosis Bulbi','Tetanus','Thalassaemia','Tuberculosis','Turners Syndrome','Trichiasis','Trachoma','Toxic Keratoconjunctivitis','Thermal Injuries to the Eye','Terrien''s Marginal Degeneration','Taeniasis/cysticercosis','Typhoid / Enteric Fever','Traumatic Hyphaema','Trochlear Nerve Palsy','Tooth Sensitivity','Tomato Fever','Tics and Eye Manifestations','Ulcerative Colitis','Urticaria','Varicose Veins','Vitamin B12 Deficiency','Vitiligo','Vernal Keratoconjunctivitis','Whooping Cough/Pertussis','West Nile fever','Welding-arc Maculopathy','Xanthelasma','Xerophthalmia','Yellow Fever','Yaws','Zika virus disease'];
        _disease_template_id int;
        _stage_name_list varchar[] := array['PRESUMPTIVE_OPEN', 'PRESUMPTIVE_CLOSED','ON_TREATMENT'];
        _stage_display_name_list varchar[] := array['Presumptive (Open)', 'Presumptive (Closed)','Treatment'];
        _stage_id int;
        _stage_id_list int[] DEFAULT '{}';
        _disease_stage_id_list int[] DEFAULT '{}';
        _disease_stage_id int;
        _required_fields varchar[] := array['firstName', 'mobileList'];
        _not_required_fields varchar[] := array['lastName', 'age', 'monitoringMethod', 'regimenType', 'lastRefillDate', 'numberOfDaysOfMedication', 'typeOfDosing', 'dateOfBirth', 'gender', 'treatmentPlanId'];
        _required_field_ids int[];
        _not_required_field_ids int[];
        _field_id int;
    BEGIN
        FOR i IN 1 .. array_upper(_stage_name_list, 1)
            LOOP
                 SELECT id into _stage_id from stages where stage_name = _stage_name_list[i] and stage_display_name = _stage_display_name_list[i];
                    IF _stage_id  is NULL THEN
                        WITH new_stage AS (
                            INSERT INTO stages (id, stage_name, stage_display_name) VALUES
                            ((select max(id) + 1 from stages),_stage_name_list[i], _stage_display_name_list[i])
                            RETURNING id)
                        SELECT id INTO _stage_id from new_stage;
                    END IF;
                  _stage_id_list := _stage_id_list || _stage_id;
            END LOOP;
         foreach _client_id  in array _client_id_list loop
              FOR i IN 1 .. array_upper(_disease_template_name_list, 1) loop
                SELECT id INTO _disease_template_id FROM disease_template WHERE disease_name = _disease_template_name_list[i] AND client_id = _client_id;
                IF _disease_template_id is NULL THEN
                        With new_disease_template As (
                            INSERT  INTO disease_template (id, client_id, disease_name) values ((select max(id) +1 from disease_template), _client_id, _disease_template_name_list[i])
                            RETURNING  id)
                    SELECT id INTO _disease_template_id FROM new_disease_template;
                END IF;
               FOREACH  _stage_id in array _stage_id_list LOOP
                   SELECT id INTO _disease_stage_id FROM disease_stage_mapping WHERE stage_id = _stage_id AND disease_template_id = _disease_template_id;
                   IF _disease_stage_id is NULL THEN
                       With new_disease_stage As (
                           INSERT INTO disease_stage_mapping(id, disease_template_id, stage_id) VALUES
                               ((select max(id) +1 from disease_stage_mapping), _disease_template_id, _stage_id)
                           RETURNING id
                       )
                       select id INTO _disease_stage_id FROM new_disease_stage;
                   end if;
                   _disease_stage_id_list := _disease_stage_id_list || _disease_stage_id;
              END LOOP ;

              SELECT array_agg(id) INTO _not_required_field_ids FROM field where key = ANY(_not_required_fields);

              SELECT array_agg(id) INTO _required_field_ids FROM field where key = ANY(_required_fields);

              FOREACH  _disease_stage_id IN ARRAY _disease_stage_id_list LOOP
                  FOREACH _field_id IN ARRAY _not_required_field_ids LOOP
                    IF NOT EXISTS(SELECT id from disease_stage_key_mapping where disease_stage_id = _disease_stage_id and field_id = _field_id) THEN
                        INSERT INTO disease_stage_key_mapping (id, disease_stage_id, required, deleted, default_value, field_id)
                        VALUES ((select max(id)+1 from disease_stage_key_mapping), _disease_stage_id, false, false, null, _field_id);
                    END IF;
                  END LOOP;
                  FOREACH _field_id IN ARRAY _required_field_ids LOOP
                    IF NOT EXISTS(SELECT id from disease_stage_key_mapping where disease_stage_id = _disease_stage_id and field_id = _field_id) THEN
                        INSERT INTO disease_stage_key_mapping (id, disease_stage_id, required, deleted, default_value, field_id)
                        VALUES ((select max(id)+1 from disease_stage_key_mapping), _disease_stage_id, true, false, null, _field_id);
                    END IF;
                  END LOOP;
              END LOOP;
              end loop;
        end loop;
    END

$do$;