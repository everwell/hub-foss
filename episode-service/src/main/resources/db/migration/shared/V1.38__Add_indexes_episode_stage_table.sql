create index if not exists es_startDate_index
    on episode_stage(start_date);

create index if not exists es_stageId_index
    on episode_stage(stage_id);
