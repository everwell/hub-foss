ALTER TABLE IF EXISTS episode_log
    ADD COLUMN IF NOT EXISTS sub_category text;

ALTER TABLE IF EXISTS episode_log
    ADD COLUMN IF NOT EXISTS date_of_action timestamp;