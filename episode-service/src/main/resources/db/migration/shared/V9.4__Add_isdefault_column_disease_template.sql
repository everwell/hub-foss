ALTER TABLE disease_template
ADD COLUMN IF NOT EXISTS is_default boolean;

UPDATE disease_template
set is_default = true
where disease_name = 'Other';