create table if not exists tb_champion_activity
(
    id         bigserial
        constraint tb_champion_activity_pkey
            primary key,
    created_on   timestamp,
    episode_id bigint,
    patient_visits  bigint,
    meeting_count  bigint,
    other_acitivity varchar(255),
    file_uploaded varchar(255)
);

create index if not exists tb_champion_activity_episode_id_index
    on tb_champion_activity (episode_id);