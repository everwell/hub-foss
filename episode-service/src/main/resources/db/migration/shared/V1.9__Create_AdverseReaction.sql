create table if not exists adverse_reaction
(
    id                       bigserial not null
        constraint adverse_reaction_pkey
            primary key,
    adr_seriousness          text,
    alcohol_use              text,
    autopsy_performed        text,
    bdq_exposure             text,
    breastfeeding_infant     text,
    cause_of_death           text,
    current_weight           text,
    daids_grading            text,
    date_of_death            timestamp,
    drtb_type                text,
    hiv_reactive             text,
    hospital_admission_date  timestamp,
    hospital_discharge_date  timestamp,
    injection_drug_use       text,
    linezolid_exposure       text,
    onset_date               timestamp ,
    predominant_condition    text,
    pregnancy_status         text,
    previous_exposure        text,
    type_of_case             text,
    report_type              text,
    reporter_narrative       text,
    severity                 text,
    tb_type                  text,
    tobacco_use              text
);
