create table if not exists adverse_event
(
    id                      bigserial not null
        constraint adverse_event_pkey
            primary key,
    adverse_reaction_id     bigint,
    causality_management_id bigint,
    episode_id              bigint,
    outcome_id              bigint,
    created_on              timestamp,
    onset_date              timestamp ,
    outcome_date            timestamp
);

create index if not exists advserse_event_episode_id_index
  on adverse_event (episode_id);