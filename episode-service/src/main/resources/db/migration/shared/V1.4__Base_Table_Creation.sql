create table if not exists episode
(
    id                 integer not null
    constraint episode_pkey
    primary key,
    client_id          integer,
    person_id          integer,
    deleted            boolean,
    disease_id_options text,
    created_date       timestamp,
    start_date         timestamp,
    end_date           timestamp,
    last_activity_date timestamp,
    current_tags       text,
    risk_status        text,
    current_stage_id   integer,
    added_by           bigint,
    type_of_episode    bigint
);

create table if not exists episode_hierarchy_linkages
(
    id           integer not null
    constraint episode_hierarchy_linkages_pkey
    primary key,
    episode_id   integer,
    hierarchy_id integer,
    relation     integer
);

create table if not exists supported_relations
(
    id          integer not null
    constraint supported_relations_pkey
    primary key,
    type        varchar(255),
    description text
);

create table if not exists supported_tabs
(
    id          integer not null
    constraint supported_tabs_pkey
    primary key,
    type        varchar(255),
    description text
);

create table if not exists episode_associations
(
    id         integer not null
    constraint episode_associations_pkey
    primary key,
    episode_id integer,
    id_name    varchar(255),
    id_value   varchar(255)
);

create table if not exists disease_template
(
    id           integer not null
    constraint disease_template_pkey
    primary key,
    disease_name varchar(255)
);

create table if not exists stages
(
    id                 integer not null
    constraint stages_pkey
    primary key,
    stage_name         varchar(255),
    metadata           text,
    meta               text,
    stage_display_name varchar(255)
);

create table if not exists tab_permissions
(
    id                       integer not null
    constraint tab_permissions_pkey
    primary key,
    disease_stage_mapping_id integer,
    tab_id                   varchar(255),
    add                      boolean,
    edit                     boolean,
    view                     boolean,
    delete                   boolean
);

create table if not exists episode_stages
(
    id         integer not null
    constraint episode_stages_pkey
    primary key,
    episode_id integer,
    stage_id   integer,
    start_date timestamp,
    end_date   timestamp
);

create table if not exists episode_stage_data
(
    id               integer not null
    constraint episode_stage_data_pkey
    primary key,
    episode_stage_id integer,
    key              varchar(255),
    value            varchar(255)
);

create table if not exists disease_stage_mapping
(
    id                  integer not null
    constraint disease_stage_mapping_pkey
    primary key,
    disease_template_id integer,
    stage_id            integer
);

create table if not exists disease_stage_key_mapping
(
    id               integer not null
    constraint disease_stage_key_mapping_pkey
    primary key,
    disease_stage_id integer,
    key              varchar(255),
    module           varchar(255),
    required         boolean,
    deleted          boolean,
    default_value    varchar(255)
);