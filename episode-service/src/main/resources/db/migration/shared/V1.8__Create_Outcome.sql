create table if not exists aers_outcome
(
    id                       bigserial not null
        constraint outcome_pkey
            primary key,
    autopsy_performed        text,
    cause_of_death           text,
    date_of_death            timestamp ,
    date_of_resolution       timestamp ,
    hospital_admission_date  timestamp ,
    hospital_discharge_data  timestamp ,
    outcome                  text
);
