package com.everwell.transition.controller;

import com.everwell.transition.exceptions.ValidationException;
import com.everwell.transition.model.response.EpisodeAppConfigResponse;
import com.everwell.transition.model.response.Response;
import com.everwell.transition.service.EpisodeAppConfigService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import static com.everwell.transition.constants.Constants.DEFAULT;

@RestController
public class EpisodeAppConfigController {

    private static Logger LOGGER = LoggerFactory.getLogger(EpisodeAppConfigController.class);

    @Autowired
    private EpisodeAppConfigService episodeAppConfigService;

    @GetMapping("/v1/patient-app/config")
    public ResponseEntity<Response<EpisodeAppConfigResponse>> getConfig(@RequestParam(value = "episodeId") Long episodeId,@RequestParam(value = "technology") String technology,@RequestParam(value = "deployment") String deployment) {
        LOGGER.info("[getConfig] received request for episodeId : " + episodeId + " technology : " + technology + " and deployment :" + deployment) ;
        if (null == episodeId ) {
            throw new ValidationException("episode id is required");
        }
        if (!StringUtils.hasText(technology) || !StringUtils.hasText(deployment)) {
            technology = DEFAULT;
            deployment = DEFAULT;
        }
        EpisodeAppConfigResponse episodeAppConfigResponse = episodeAppConfigService.getEpisodeAppConfig(episodeId, technology, deployment);
        return new ResponseEntity<>(new Response<>(episodeAppConfigResponse), HttpStatus.OK);
    }
}
