package com.everwell.transition.controller;

import com.everwell.transition.constants.AppointmentConstants;
import com.everwell.transition.model.db.Appointment;
import com.everwell.transition.model.dto.EpisodeDto;
import com.everwell.transition.model.dto.appointment.AppointmentData;
import com.everwell.transition.model.dto.appointment.AppointmentDetails;
import com.everwell.transition.model.dto.notification.NotificationEventDto;
import com.everwell.transition.model.request.appointment.AppointmentListRequest;
import com.everwell.transition.model.request.appointment.DeleteAppointmentRequest;
import com.everwell.transition.model.request.appointment.FollowUpRequest;
import com.everwell.transition.model.request.appointment.RescheduleRequest;
import com.everwell.transition.model.response.Response;
import com.everwell.transition.model.response.appointment.GetAppointmentResponse;
import com.everwell.transition.service.AppointmentService;
import com.everwell.transition.service.ClientService;
import com.everwell.transition.service.EpisodeService;
import com.everwell.transition.service.NotificationService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
public class AppointmentController {

    private static final Logger LOGGER = LoggerFactory.getLogger(AppointmentController.class);

    @Autowired
    AppointmentService appointmentService;

    @Autowired
    EpisodeService episodeService;

    @Autowired
    NotificationService notificationService;

    @Autowired
    ClientService clientService;

    @Autowired
    ApplicationEventPublisher applicationEventPublisher;

    @RequestMapping(value = "/v1/appointment", method = RequestMethod.POST)
    public ResponseEntity<Response<AppointmentData>> addAppointment(@RequestBody AppointmentData appointment) {
        LOGGER.info("[/v1/appointment] POST request received " + appointment);
        appointment.isValid();
        AppointmentData response = appointmentService.addAppointment(appointment);
        return new ResponseEntity<>(new Response<>(response), HttpStatus.OK);
    }

    @RequestMapping(value = "/v1/appointment", method = RequestMethod.GET)
    public ResponseEntity<Response<GetAppointmentResponse>> getAppointment(@RequestParam Integer appointmentId) {
        LOGGER.info("[/v1/appointment] GET request received " + appointmentId);
        GetAppointmentResponse response = appointmentService.getAppointmentResponse(appointmentId);
        return new ResponseEntity<>(new Response<>(response), HttpStatus.OK);
    }

    @RequestMapping(value = "/v1/appointment/list", method = RequestMethod.POST)
    public ResponseEntity<Response<List<AppointmentDetails>>> getAppointmentList(@RequestBody AppointmentListRequest appointmentListRequest) {
        LOGGER.info("[/v1/appointment/list] POST request received " + appointmentListRequest);
        List<AppointmentDetails> response = appointmentService.getAppointments(appointmentListRequest);
        return new ResponseEntity<>(new Response<>(response), HttpStatus.OK);
    }

    @RequestMapping(value = "v1/appointment/end", method = RequestMethod.PUT)
    public ResponseEntity<Response<String>> endAppointment(@RequestParam Integer appointmentId) {
        LOGGER.info("[v1/appointment/end] PUT request received " + appointmentId);
        String response = appointmentService.endAppointment(appointmentId);
        return new ResponseEntity<>(new Response<>(true, response), HttpStatus.OK);
    }

    @RequestMapping(value = "v1/appointment/confirm", method = RequestMethod.PUT)
    public ResponseEntity<Response<String>> confirmAppointment(@RequestParam Integer appointmentId, @RequestParam String staffName) throws IllegalAccessException {
        LOGGER.info("[v1/appointment/confirm] PUT request received " + appointmentId);
        Appointment appointment = appointmentService.confirmAppointment(appointmentId);
        Map<String, Object> extraData = new HashMap<>();
        extraData.put(AppointmentConstants.APPOINTMENT_DATE, appointment.getDate());
        extraData.put(AppointmentConstants.DOCTOR_NAME, staffName);
        EpisodeDto episodeDto = episodeService.getEpisodeDetails(appointment.getEpisodeId(), clientService.getClientByTokenOrDefault().getId());
        applicationEventPublisher.publishEvent(new NotificationEventDto(episodeDto, staffName, appointment.getDate(), extraData, AppointmentConstants.SEND_APPOINTMENT_CONFIRMATION_PN));
        return new ResponseEntity<>(new Response<>(true, AppointmentConstants.CONFIRM_SUCCESS), HttpStatus.OK);
    }

    @PostMapping(value = "v1/appointment/follow-up")
    public ResponseEntity<Response<AppointmentDetails>> createFollowUpAppointment(@RequestBody FollowUpRequest followUpRequest) {
        LOGGER.info("[v1/appointment/follow-up] POST request received " + followUpRequest);
        Appointment savedFollowUpAppointment = appointmentService.createFollowUpAppointment(followUpRequest);
        EpisodeDto patientDetails = episodeService.getEpisodeDetails(savedFollowUpAppointment.getEpisodeId(),
                clientService.getClientByTokenOrDefault().getId());
        AppointmentDetails response =  appointmentService.createAppointmentDetail(savedFollowUpAppointment, patientDetails);
        return new ResponseEntity<>(new Response<>(true, response), HttpStatus.OK);
    }

    @PostMapping(value = "v1/appointment/re-schedule")
    public ResponseEntity<Response<AppointmentDetails>> rescheduleAppointment(@RequestBody RescheduleRequest rescheduleRequest) {
        LOGGER.info("[v1/appointment/re-schedule] POST request received " + rescheduleRequest);
        Appointment updatedAppointment = appointmentService.rescheduleAppointment(rescheduleRequest);
        EpisodeDto patientDetails = episodeService.getEpisodeDetails(updatedAppointment.getEpisodeId(),
                clientService.getClientByTokenOrDefault().getId());
        AppointmentDetails response = appointmentService.createAppointmentDetail(updatedAppointment, patientDetails);
        return new ResponseEntity<>(new Response<>(true, response), HttpStatus.OK);
    }

    @RequestMapping(value = "v1/appointment", method = RequestMethod.DELETE)
    public ResponseEntity<Response<String>> deleteAppointment(@RequestBody DeleteAppointmentRequest appointment) {
        LOGGER.info("[v1/appointment] DELETE request received " + appointment);
        appointmentService.deleteAppointment(appointment);
        return new ResponseEntity<>(new Response<>(true, AppointmentConstants.DELETE_SUCCESS), HttpStatus.OK);
    }
}
