package com.everwell.transition.controller;

import com.everwell.transition.constants.Constants;
import com.everwell.transition.constants.NotificationConstants;
import com.everwell.transition.enums.notification.NotificationField;
import com.everwell.transition.enums.user.UserServiceField;
import com.everwell.transition.model.dto.EpisodeDto;
import com.everwell.transition.model.response.Response;
import com.everwell.transition.service.ClientService;
import com.everwell.transition.service.EpisodeService;
import com.everwell.transition.service.NotificationService;
import com.everwell.transition.service.UserService;
import com.everwell.transition.utils.Utils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Collections;
import java.util.List;
import java.util.Map;

@RestController
public class UserController {

    private static Logger LOGGER = LoggerFactory.getLogger(UserController.class);

    @Autowired
    private UserService userService;

    @Autowired
    private EpisodeService episodeService;

    @Autowired
    private ClientService clientService;

    @Autowired
    private NotificationService notificationService;

    @GetMapping(value = "/v1/user/relation")
    public ResponseEntity<Response<List<Map<String, Object>>>> getUserRelations(@RequestParam Long userId, @RequestParam String status) {
        LOGGER.info("[getUserRelations] request received for " + userId);
        return userService.getUserRelations(userId, status);
    }

    @PostMapping(value = "/v1/user/relation")
    public ResponseEntity<Response<String>> addRelation(@RequestBody Map<String, Object> userRelationRequest) {
        LOGGER.info("[addRelation] request received for " + userRelationRequest);
        userService.addUserRelation(userRelationRequest);
        Long secondaryUserId = Long.parseLong(String.valueOf(userRelationRequest.get(UserServiceField.SECONDARY_USER_ID.getFieldName())));
        Long clientId = clientService.getClientByTokenOrDefault().getId();
        List<EpisodeDto> ep = episodeService.getEpisodesForPersonList(Collections.singletonList(secondaryUserId), clientId, true);
        notificationService.sendPushNotificationForEpisodeWithExtraData(ep.get(0), userRelationRequest, NotificationConstants.EVENT_ADD_RELATION);
        return new ResponseEntity<>(new Response<>(true, "profile added"), HttpStatus.OK);
    }

    @DeleteMapping(value = "/v1/user/relation")
    public ResponseEntity<Response<String>> deleteRelation(@RequestBody Map<String, Object> userRelationRequest) {
        LOGGER.info("[deleteRelation] request received for " + userRelationRequest);
        userService.deleteUserRelation(userRelationRequest);
        notificationService.deleteNotification(userRelationRequest);
        return new ResponseEntity<>(new Response<>(true, "Relation deleted"), HttpStatus.OK);
    }

    @PutMapping(value = "/v1/user/relation")
    public ResponseEntity<Response<String>> updateRelation(@RequestBody Map<String, Object> userRelationRequest) {
        LOGGER.info("[updateRelation] request received for " + userRelationRequest);
        userService.updateUserRelation(userRelationRequest);
        String notificationId = Utils.convertObjectToStringWithNull(userRelationRequest.getOrDefault(NotificationField.NOTIFICATION_ID.getFieldName(), null));
        if (null != notificationId) {
            Long notification = Long.parseLong(notificationId);
            String status = String.valueOf(userRelationRequest.get(NotificationField.STATUS.getFieldName()));
            notificationService.updateNotification(notification, true, status);
        }
        return new ResponseEntity<>(new Response<>(true, "Relation updated"), HttpStatus.OK);
    }

    @RequestMapping(value = "/v1/users/relation", method = RequestMethod.GET)
    public ResponseEntity<Response<List<Map<String, Object>>>> getUserRelationsByPrimaryPhone(@RequestParam("phoneNumber") String phoneNumber, @RequestParam("primaryUserId") Long primaryUserId) throws Exception
    {
        LOGGER.info("[getUserRelationsByPrimaryPhone] request received for phone " + phoneNumber + "primary user id " + primaryUserId);
        return userService.getUserRelationsPrimaryPhone(phoneNumber, primaryUserId);
    }

    @GetMapping(value = "/v1/vitals")
    public ResponseEntity<Response<List<Map<String, Object>>>> getAllVitals() {
        LOGGER.info("[getAllVitals] request received ");
        return userService.getAllVitals();
    }

    @GetMapping(value = "/v1/vitals/{id}")
    public ResponseEntity<Response<Map<String, Object> >> getVitalsById(@PathVariable("id") Long vitalId) {
        LOGGER.info("[getVitalsById] request received for vitalId : " + vitalId);
        return userService.getVitalsById(vitalId);
    }

    @PostMapping(value = "/v1/vitals")
    public ResponseEntity<Response<List<Map<String, Object>>>> saveVitals(@RequestBody  Map<String, Object> vitalBulkRequest) {
        LOGGER.info("[saveVitals] request received : " + vitalBulkRequest);
        return userService.saveVitals(vitalBulkRequest);
    }

    @PutMapping(value = "/v1/vitals/{id}")
    public ResponseEntity<Response<Map<String, Object>>> updateVitalsById(@PathVariable("id") Long vitalId, @RequestBody Map<String, Object> vitalBulkRequest) {
        LOGGER.info("[updateVitalsById] request received for vitalId : " + vitalId + " with body : " + vitalBulkRequest);
        return userService.updateVitalsById(vitalId, vitalBulkRequest);
    }

    @DeleteMapping(value = "/v1/vitals/{id}")
    public ResponseEntity<Response<String>> deleteVitalsById(@PathVariable("id") Long vitalId) {
        LOGGER.info("[deleteVitalsById] request received for vitalId : " + vitalId);
        return userService.deleteVitalsById(vitalId);
    }

    @GetMapping(value = "/v1/users/{userId}/vitals-goal")
    public ResponseEntity<Response<List<Map<String, Object>>>> getVitalsGoalByUserId(@PathVariable("userId") Long userId)
    {
        LOGGER.info("[getVitalsGoalByUserId] request received for userId : " + userId);
        return userService.getVitalsGoalByUserId(userId);
    }

    @PostMapping(value = "/v1/users/{userId}/vitals-goal")
    public ResponseEntity<Response<String>> saveUserVitalsGoal(@PathVariable("userId") Long userId, @RequestBody Map<String, Object> vitalGoalRequest)
    {
        LOGGER.info("[saveUserVitalsGoal] request received for userId : " + userId + " with body : " + vitalGoalRequest);
        return userService.saveUserVitalsGoal(userId, vitalGoalRequest);
    }

    @PutMapping(value = "/v1/users/{userId}/vitals-goal")
    public ResponseEntity<Response<String>> updateUserVitalsGoal(@PathVariable("userId") Long userId,  @RequestBody Map<String, Object> vitalGoalRequest)
    {
        LOGGER.info("[updateUserVitalsGoal] request received for userId : " + userId + " with body : " + vitalGoalRequest);
        return userService.updateUserVitalsGoal(userId, vitalGoalRequest);
    }

    @DeleteMapping(value = "/v1/users/{userId}/vitals-goal")
    public ResponseEntity<Response<String>> deleteUserVitalsGoal(@PathVariable("userId") Long userId)
    {
        LOGGER.info("[deleteUserVitalsGoal] request received for userId : " + userId);
        return userService.deleteUserVitalsGoal(userId);
    }

    @PostMapping(value = "/v1/users/vitals/bulk")
    public ResponseEntity<Response<Map<String, Object>>> getVitalsDetailsByUserId(@RequestBody Map<String, Object> vitalDetailsRequest)
    {
        LOGGER.info("[getVitalsDetailsByUserId] request received for : "+vitalDetailsRequest);
        return userService.getUserVitalsDetails(vitalDetailsRequest);
    }

    @PostMapping(value = "/v1/users/{userId}/vitals")
    public ResponseEntity<Response<String>> saveUserVitalsDetails(@PathVariable("userId") Long userId, @RequestBody Map<String, Object> addVitalDetailsRequest)
    {
        LOGGER.info("[saveUserVitalsDetails] request received for userId : " + userId + " with body : " + addVitalDetailsRequest);
        return userService.saveUserVitalsDetails(userId, addVitalDetailsRequest);
    }
}
