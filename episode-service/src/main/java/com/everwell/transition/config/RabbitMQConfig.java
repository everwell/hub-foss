package com.everwell.transition.config;

import org.springframework.amqp.core.Queue;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import com.everwell.transition.constants.RabbitMQConstants;

@Configuration
public class RabbitMQConfig {
    @Bean
    Queue episodeClientAddEpisodeQueue() {
        return new Queue(RabbitMQConstants.EPISODE_CLIENT_ADD_EPISODE, true, false, false);
    }

    @Bean
    Queue episodeUpdateTrackerQueue() {
        return new Queue(RabbitMQConstants.EPISODE_UPDATE_TRACKER, true, false, false);
    }

    @Bean
    Queue refreshTagQueue() {
        return new Queue(RabbitMQConstants.REFRESH_TAG_FOR_ALL_EPISOES, true, false, false);
    }

    @Bean
    Queue episodeDoseReminderQueue() {
        return new Queue(RabbitMQConstants.EPISODE_DOSE_REMINDER, true, false, false);
    }

}
