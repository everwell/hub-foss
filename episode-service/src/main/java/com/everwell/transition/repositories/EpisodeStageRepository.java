package com.everwell.transition.repositories;

import com.everwell.transition.model.db.EpisodeStage;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.time.LocalDateTime;
import java.util.List;

public interface EpisodeStageRepository extends JpaRepository<EpisodeStage, Long> {

    @Query(value = "Select * from episode_stage where episode_stage.episode_id = :episodeId and end_date is null", nativeQuery = true)
    EpisodeStage getCurrentStageForEpisodeId(Long episodeId);

    List<EpisodeStage> getEpisodeStagesByEpisodeIdInOrderByStartDateAsc(List<Long> episodeIds);

    @Query(value = "SELECT episode_id FROM episode_stage  WHERE start_date >= :startDate and start_date < :endDate and stage_id in :stageId and end_date is null", nativeQuery = true)
    List<Long> getEpisodeIdsByStartDateAndStage(LocalDateTime startDate,LocalDateTime endDate,List<Long> stageId);
}
