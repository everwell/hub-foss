package com.everwell.transition.repositories;

import com.everwell.transition.model.db.CausalityManagement;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CausalityManagementRepository extends JpaRepository<CausalityManagement,Long> {
}
