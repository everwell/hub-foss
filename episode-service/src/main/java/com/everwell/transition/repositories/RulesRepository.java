package com.everwell.transition.repositories;

import com.everwell.transition.model.db.Rules;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

public interface RulesRepository extends JpaRepository<Rules, Long> {
    List<Rules> findAllByRuleNamespaceAndClientId(String nameSpace, Long clientId);
}
