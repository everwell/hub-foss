package com.everwell.transition.repositories;

import com.everwell.transition.model.db.CourseUrlMapping;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CourseUrlMappingRepository extends JpaRepository<CourseUrlMapping,Long> {

}
