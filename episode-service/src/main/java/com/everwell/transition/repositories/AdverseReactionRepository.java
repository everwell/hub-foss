package com.everwell.transition.repositories;

import com.everwell.transition.model.db.AdverseReaction;
import org.springframework.data.jpa.repository.JpaRepository;

public interface AdverseReactionRepository extends JpaRepository<AdverseReaction,Long> {
}
