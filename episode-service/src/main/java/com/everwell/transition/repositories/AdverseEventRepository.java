package com.everwell.transition.repositories;

import com.everwell.transition.model.db.AdverseEvent;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface AdverseEventRepository extends JpaRepository<AdverseEvent,Long> {

    List<AdverseEvent> getAllByEpisodeId(Long id);
}
