package com.everwell.transition.repositories;

import com.everwell.transition.model.db.EpisodeHierarchyLinkage;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface EpisodeHierarchyLinkageRepository extends JpaRepository<EpisodeHierarchyLinkage, Long> {

    List<EpisodeHierarchyLinkage> getAllByEpisodeIdIn(List<Long> episodeIds);
}
