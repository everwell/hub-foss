package com.everwell.transition.repositories;

import com.everwell.transition.model.db.SupportedRelation;
import org.springframework.data.jpa.repository.JpaRepository;

public interface SupportedRelationRepository extends JpaRepository<SupportedRelation, Long> {
}
