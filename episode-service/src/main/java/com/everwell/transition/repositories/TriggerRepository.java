package com.everwell.transition.repositories;

import com.everwell.transition.model.db.Trigger;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface TriggerRepository extends JpaRepository<Trigger, Long> {
    List<Trigger> getAllByCronTimeNotNull();

    Trigger getTriggerByFunctionName(String functionName);

    Trigger getTriggerByEventName(String eventName);
    Trigger getTriggerByEventNameAndClientId(String eventName, Long clientId);
}
