package com.everwell.transition.model.db;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import javax.persistence.*;

@AllArgsConstructor
@NoArgsConstructor
@Table(name = "regimen")
@Entity
@Data
public class Regimen {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "name")
    private String name;

    @Column(name = "dosing_days")
    private Long dosingDays;

    @Column(name = "disease_id")
    private Long diseaseId;
}
