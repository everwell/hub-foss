package com.everwell.transition.model.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class AdherenceCodeExtra {
    String color;
    String icon;
    String iconColor;

}
