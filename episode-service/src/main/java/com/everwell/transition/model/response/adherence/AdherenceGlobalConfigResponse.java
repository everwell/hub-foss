package com.everwell.transition.model.response.adherence;

import com.everwell.transition.model.response.AdherenceCodeConfigResponse;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class AdherenceGlobalConfigResponse {
    List<AdherenceCodeConfigResponse> adherenceCodeConfigResponseList;

    List<GenericConfigResponse> adherenceTechConfigResponseList;

    List<GenericConfigResponse> scheduleTypeConfigResponseList;
}
