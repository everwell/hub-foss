package com.everwell.transition.model.request.adherence;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class RecordAdherenceBulkRequest {
    List<RecordAdherenceRequest> request;
}
