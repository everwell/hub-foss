package com.everwell.transition.model.request.diagnostics;

import lombok.Getter;

@Getter
public class ResultRequestV2 extends ResultRequest {
    private String sampleIndex;
}
