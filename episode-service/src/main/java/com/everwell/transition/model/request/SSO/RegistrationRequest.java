package com.everwell.transition.model.request.SSO;

import lombok.*;

@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class RegistrationRequest {
    private String name;

    private String password;
}
