package com.everwell.transition.model.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class IAMScheduleDto {
    public String scheduleType;

    public String scheduleString;

    public Long firstDoseOffset;

    public Long positiveSensitivity;

    public Long negativeSensitivity;

    public Long scheduleTypeId;

}
