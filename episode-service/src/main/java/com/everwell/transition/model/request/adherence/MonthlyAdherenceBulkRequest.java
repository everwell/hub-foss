package com.everwell.transition.model.request.adherence;

import com.everwell.transition.constants.Constants;
import com.everwell.transition.exceptions.ValidationException;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.util.CollectionUtils;

import java.time.DateTimeException;
import java.time.ZoneId;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class MonthlyAdherenceBulkRequest {
    List<Long> episodeIdList;
    String timezone;

    public void validate() throws ValidationException {
        if (episodeIdList == null || CollectionUtils.isEmpty(episodeIdList)) {
            throw new ValidationException("Invalid episode Id List!");
        }
        if (episodeIdList.size() > Constants.MAX_BULK_ADHERENCE_SIZE)
            throw new ValidationException("Episode Ids should be less than 3000!");
        //validate timezone sent
        try {
            ZoneId.of(this.timezone);
        } catch (DateTimeException e) {
            throw new ValidationException(this.timezone + " - Timezone does not exists!");
        }
    }
}
