package com.everwell.transition.model.db;

import com.everwell.transition.model.dto.SuspectedDrugsDto;
import com.everwell.transition.model.request.aers.CausalityManagementRequest;
import com.everwell.transition.utils.Utils;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.lang.reflect.Field;
import java.time.LocalDateTime;

@Table(name = "causality_management")
@Getter
@Setter
@Entity
@AllArgsConstructor
@NoArgsConstructor
public class CausalityManagement {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "cause_medicine_linkage")
    private String causeMedicineLinkage;

    @Column(name = "suspected_drugs")
    private String suspectedDrugs;

    @Column(name = "dechallenge")
    private String dechallenge;

    @Column(name = "rechallenge")
    private String rechallenge;

    @Column(name = "expectedness")
    private String expectedness;

    @Column(name = "was_action_taken")
    private String actionTakenOrNot;

    @Column(name = "action_taken")
    private String actionTaken;

    @Column(name = "treatment_details")
    private String treatmentDetails;

    @Column(name = "anti_tb_medicines")
    private String antiTbMedicines;

    @Column(name = "other_medicines")
    private String otherMedicines;

    @Column(name = "new_medicines")
    private String newMedicines;

    @Column(name = "created_on")
    private LocalDateTime createdOn;

    @Column(name = "updated_on")
    private LocalDateTime updatedOn;

    public CausalityManagement update(CausalityManagement other) {
        Class klass = getClass();
        for (Field field : klass.getDeclaredFields()) {
            try {
                if (field.get(other) != null) {
                    field.set(this, field.get(other));
                }
            } catch (IllegalAccessException e) {
            }
        }
        return this;
    }

    public CausalityManagement(CausalityManagementRequest causalityManagementRequest, SuspectedDrugsDto suspectedDrugsDto) {
        this.causeMedicineLinkage = causalityManagementRequest.getCauseMedicineLinkage();
        this.actionTakenOrNot = causalityManagementRequest.getActionTakenOrNot();
        this.actionTaken = causalityManagementRequest.getActionTaken() == null ? null : Utils.asJsonString(causalityManagementRequest.getActionTaken());
        this.antiTbMedicines = causalityManagementRequest.getAntiTbMedicines() == null ? null : Utils.asJsonString(causalityManagementRequest.getAntiTbMedicines());
        this.suspectedDrugs = causalityManagementRequest.getCauseMedicineLinkage() == null || causalityManagementRequest.getCauseMedicineLinkage().equals("No") ? null : Utils.asJsonString(suspectedDrugsDto);
        this.dechallenge = causalityManagementRequest.getDechallenge();
        this.rechallenge = causalityManagementRequest.getRechallenge();
        this.expectedness = causalityManagementRequest.getExpectedness();
        this.treatmentDetails = causalityManagementRequest.getTreatmentDetails();
        this.otherMedicines = causalityManagementRequest.getOtherMedicines() == null ? null : Utils.asJsonString(causalityManagementRequest.getOtherMedicines());
        this.newMedicines = causalityManagementRequest.getNewMedicines() == null ? null : Utils.asJsonString(causalityManagementRequest.getNewMedicines());
    }

    public CausalityManagement(String causeMedicineLinkage, String dechallenge) {
        this.causeMedicineLinkage = causeMedicineLinkage;
        this.dechallenge = dechallenge;
    }

    public CausalityManagement(String causeMedicineLinkage, String dechallenge, String suspectedDrugs, String actionTakenOrNot, String actionTaken, String medicines) {
        this.causeMedicineLinkage = causeMedicineLinkage;
        this.dechallenge = dechallenge;
        this.suspectedDrugs = suspectedDrugs;
        this.actionTakenOrNot = actionTakenOrNot;
        this.actionTaken = actionTaken;
        this.antiTbMedicines = medicines;
        this.otherMedicines = medicines;
        this.newMedicines = medicines;
    }

    public CausalityManagement(Long id) {
        this.id = id;
    }

}
