package com.everwell.transition.model.dto;

import com.everwell.transition.enums.Event;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class EventDto<T> {

    Event eventDetails;

    T message;
}
