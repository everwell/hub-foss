package com.everwell.transition.model.db;

import com.everwell.transition.constants.Constants;
import com.everwell.transition.utils.Utils;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateTimeDeserializer;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateTimeSerializer;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Table(name = "episode_stage")
@Getter
@NoArgsConstructor
@AllArgsConstructor
public class EpisodeStage {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "episode_id")
    private Long episodeId;

    @Column(name = "stage_id")
    private Long stageId;

    @Setter
    @Column(name = "start_date")
    private LocalDateTime startDate;

    @Setter
    @Column(name = "end_date")
    private LocalDateTime endDate;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = Constants.DATE_FORMAT)
    @CreationTimestamp
    @Column(name = "created_on")
    private LocalDateTime createdOn;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = Constants.DATE_FORMAT)
    @UpdateTimestamp
    @Column(name = "updated_on")
    private LocalDateTime updatedOn;

    public EpisodeStage(Long episodeId, Long stageId) {
        this.episodeId = episodeId;
        this.stageId = stageId;
        this.startDate = Utils.getCurrentDateNew();
        this.endDate = null;
    }

    public EpisodeStage(Long id, Long episodeId, Long stageId, LocalDateTime startDate, LocalDateTime endDate) {
        this.id = id;
        this.episodeId = episodeId;
        this.stageId = stageId;
        this.startDate = startDate;
        this.endDate = endDate;
    }
}
