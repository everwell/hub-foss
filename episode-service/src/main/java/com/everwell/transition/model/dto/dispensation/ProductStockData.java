package com.everwell.transition.model.dto.dispensation;

import lombok.*;

import java.util.Date;
import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Getter
public class ProductStockData {
    public Long productId;
    public String batchNumber;
    public Date expiryDate ;
    public List<HierarchyData> hierarchyData;


    @Data
    @AllArgsConstructor
    @NoArgsConstructor
    @Getter
    @Setter
    public static class HierarchyData{
        public Long hierarchyId;
        public Long quantity;
    }
}
