package com.everwell.transition.model.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class TabPermissionDto {

    private Long stageId;

    private Long tabId;

    private String tabName;

    private String urlFragment;

    private boolean add;

    private boolean edit;

    private boolean delete;

    private boolean view;

    private Long tabOrder;

    public TabPermissionDto mergeTabPermissionsOnOR(TabPermissionDto tabPermissionDto) {
        this.add |= tabPermissionDto.add;
        this.edit |= tabPermissionDto.isEdit();
        this.delete |= tabPermissionDto.isDelete();
        this.view |= tabPermissionDto.isView();
        return this;
    }
}