package com.everwell.transition.model.request.dispensation;

import lombok.*;

import java.util.List;

@Getter
@Setter
@ToString
@AllArgsConstructor
@NoArgsConstructor
public class ProductStockSearchRequest {
    List<Long> productIds;
    List<Long> hierarchyMappingIds;
}
