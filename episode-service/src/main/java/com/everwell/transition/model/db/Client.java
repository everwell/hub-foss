package com.everwell.transition.model.db;

import com.everwell.transition.utils.Utils;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "client")
@Getter
@NoArgsConstructor
@AllArgsConstructor
public class Client {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String name;

    private String password;

    private Date createdDate;

    private Long eventFlowId;

    @Column(name = "hashcode_android")
    private String hashcodeAndroid;

    public Client(String name, String password) {
        this.name = name;
        this.password = password;
        this.createdDate = Utils.getCurrentDate();
    }

    public Client(Long id, String name, String password, Date createdDate) {
        this(name, password);
        this.id = id;
        this.createdDate = createdDate;
        this.eventFlowId = null;
        this.hashcodeAndroid = null;
    }
}