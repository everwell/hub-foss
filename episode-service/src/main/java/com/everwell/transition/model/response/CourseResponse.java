package com.everwell.transition.model.response;

import com.everwell.transition.model.dto.CourseDto;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@AllArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
@NoArgsConstructor
public class CourseResponse {

    Long episodeId;

    List<CourseDto> courses;
}
