package com.everwell.transition.model.request.adherence;

import com.everwell.transition.model.dto.RangeFilters;
import lombok.*;

import java.util.List;

@Getter
@Setter
@ToString
@AllArgsConstructor
@NoArgsConstructor
public class SearchAdherenceRequest {

    private List<String> entityIdList;
    private List<RangeFilters> filters;

}
