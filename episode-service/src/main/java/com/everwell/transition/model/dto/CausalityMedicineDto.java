package com.everwell.transition.model.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@ToString
@NoArgsConstructor
@Getter
@Setter
public class CausalityMedicineDto {
    String name ;
    String dosage ;
    String unit ;
    String frequency ;
    String route ;
    String startDate ;
    String continues ;
    String stopDate ;
    String reasonForStopping ;
    String action ;
}
