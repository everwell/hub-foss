package com.everwell.transition.model.request.aers;

import com.everwell.transition.exceptions.ValidationException;
import com.everwell.transition.model.dto.CausalityMedicineDto;
import com.everwell.transition.model.dto.KeyValuePair;
import lombok.*;
import org.springframework.util.CollectionUtils;

import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@ToString
public class CausalityManagementRequest {
    Long adverseEventId;
    Long episodeId;
    String causeMedicineLinkage;
    String suspectedDrugs;
    String dechallenge;
    String rechallenge;
    String expectedness;
    String actionTakenOrNot;
    List<KeyValuePair> actionTaken;
    String treatmentDetails;
    List<CausalityMedicineDto> antiTbMedicines;
    List<CausalityMedicineDto> otherMedicines;
    List<CausalityMedicineDto> newMedicines;
    String isoniazid ;
    String rifampicin ;
    String pyrazinamide ;
    String ethambutol ;
    String kanamycin ;
    String amikacin ;
    String capreomycin ;
    String levofloxacin ;
    String moxifloxacin ;
    String cycloserine ;
    String naPAS ;
    String linezolid ;
    String clofazimine ;
    String amxClv ;
    String clarithromycin ;
    String bedaquiline ;
    String delamanid ;
    String pretomonid ;
    String rifapentine ;
    String prothionamide;
    String ethionamde ;

    public CausalityManagementRequest(Long adverseEventId, Long episodeId) {
        this.adverseEventId = adverseEventId;
        this.episodeId = episodeId;
    }

    public CausalityManagementRequest(Long adverseEventId, Long episodeId, String causeMedicineLinkage) {
        this.adverseEventId = adverseEventId;
        this.episodeId = episodeId;
        this.causeMedicineLinkage = causeMedicineLinkage;
    }

    public void validate(CausalityManagementRequest causalityManagementRequest) {
        if (causalityManagementRequest.getEpisodeId() == null) {
            throw new ValidationException("episode id is required");
        }

        if (causalityManagementRequest.getAdverseEventId() == null) {
            throw new ValidationException("Adverse Event Id is required for Causality and Management");
        }
    }

}
