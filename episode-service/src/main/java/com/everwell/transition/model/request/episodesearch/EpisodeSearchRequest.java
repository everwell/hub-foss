package com.everwell.transition.model.request.episodesearch;

import com.everwell.transition.exceptions.ValidationException;
import com.everwell.transition.model.dto.AggregationDto;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.domain.Sort;

import java.util.*;

@Data
@AllArgsConstructor
public class EpisodeSearchRequest {
    SupportedSearchMethods search;
    Map<String, Object> range;
    SupportedAggMethods agg;
    ExtraFilterRequest filters;
    List<String> fieldsToShow = Arrays.asList(
            "id", "typeOfEpisode", "name"
    );
    String sortKey = "id";
    Sort.Direction sortDirection = Sort.Direction.ASC;
    int page = 0;
    int size = 10;
    boolean count;
    String requestIdentifier;
    List<String> keywordFields = new ArrayList<>();
    String collapseField;
    Boolean trackTotalHits = false;
    List<Object> searchAfter;
    boolean scroll = false;
    String scrollId;
    Long scrollTimeOutInSeconds = 60L;

    public EpisodeSearchRequest(SupportedSearchMethods search, Map<String, Object> range) {
        this.search = search;
        this.range = range;
    }

    public EpisodeSearchRequest() {
        this.search = new SupportedSearchMethods(new HashMap<>(), new HashMap<>(), new HashMap<>());
        this.range = new HashMap<>();
    }

    @Data
    @AllArgsConstructor
    @NoArgsConstructor
    public static class SupportedSearchMethods {
        Map<String, Object> must;
        Map<String, Object> mustNot;
        Map<String, Object> contains;
    }

    @Data
    @AllArgsConstructor
    @NoArgsConstructor
    public static class SupportedAggMethods {
        List<AggregationDto> terms;
        List<AggregationDto> date;
    }

    public void validate() {
        if (search == null && range == null)
            throw new ValidationException("Search request is empty!");
    }

    public void mergeFilters () {
        EpisodeSearchRequest searchRequest = filters.convertToEpisodeSearchRequest();
        this.search.must.putAll(searchRequest.getSearch().getMust());
        if (this.search.mustNot == null) {
            this.search.mustNot = new HashMap<>();
        }
        if (this.range == null) {
            this.range = new HashMap<>();
        }
        this.search.mustNot.putAll(searchRequest.getSearch().getMustNot());
        this.range.putAll(searchRequest.getRange());
    }

}
