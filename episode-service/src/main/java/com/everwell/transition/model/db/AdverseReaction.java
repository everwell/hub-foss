package com.everwell.transition.model.db;

import com.everwell.transition.model.dto.KeyValuePair;
import com.everwell.transition.model.request.aers.AdverseReactionRequest;
import com.everwell.transition.utils.Utils;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.core.type.TypeReference;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.lang.reflect.Field;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.List;

@Table(name = "adverse_reaction")
@Getter
@Setter
@Entity
@AllArgsConstructor
@NoArgsConstructor
public class AdverseReaction {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "current_weight")
    private String currentWeight;

    @Column(name = "report_type")
    private String reportType;

    @Column(name = "type_of_case")
    private String typeOfCase;

    @Column(name = "tb_type")
    private String tbType;

    @Column(name = "drtb_type")
    private String drtbType;

    @Column(name = "list_of_drugs")
    private String listOfDrugs;

    @Column(name = "previous_exposure")
    private String previousExposure;

    @Column(name = "pregnancy_status")
    private String pregnancyStatus;

    @Column(name = "breastfeeding_infant")
    private String breastfeedingInfant;

    @Column(name = "bdq_exposure")
    private String bdqExposure;

    @Column(name = "linezolid_exposure")
    private String linezolidExposure;

    @Column(name = "injection_drug_use")
    private String injectionDrugUse;

    @Column(name = "alcohol_use")
    private String alcoholUse;

    @Column(name = "tobacco_use")
    private String tobaccoUse;

    @Column(name = "hiv_reactive")
    private String hivReactive;

    @Column(name = "onset_date")
    private LocalDateTime onsetDate;

    @Column(name = "predominant_condition")
    private String predominantCondition;

    @Column(name = "other_predominant_condition")
    private String otherPredominantCondition;

    @Column(name = "reporter_narrative")
    private String reporterNarrative;

    @Column(name = "severity")
    private String severity;

    @Column(name = "daids_grading")
    private String daidsGrading;

    @Column(name = "adr_seriousness")
    private String adrSeriousness;

    @Column(name = "date_of_death")
    private LocalDateTime dateOfDeath;

    @Column(name = "cause_of_death")
    private String causeOfDeath;

    @Column(name = "autopsy_performed")
    private String autopsyPerformed;

    @Column(name = "hospital_admission_date")
    private LocalDateTime hospitalAdmissionDate;

    @Column(name = "hospital_discharge_data")
    private LocalDateTime hospitalDischargeDate;

    @Column(name = "created_on")
    private LocalDateTime createdOn;

    @Column(name = "updated_on")
    private LocalDateTime updatedOn;

    public AdverseReaction update(AdverseReaction other) {
        Class klass = getClass();
        for (Field field : klass.getDeclaredFields()) {
            try {
                if (field.get(other) != null) {
                    field.set(this, field.get(other));
                }
            } catch (IllegalAccessException e) {
            }
        }
        return this;
    }

    public AdverseReaction(AdverseReactionRequest adverseReactionRequest) {
        this.currentWeight = adverseReactionRequest.getCurrentWeight();
        this.reportType = adverseReactionRequest.getReportType();
        this.tbType = adverseReactionRequest.getTbType();
        this.typeOfCase = adverseReactionRequest.getTypeOfCase();
        this.drtbType = adverseReactionRequest.getDrtbType();
        this.listOfDrugs = adverseReactionRequest.getListOfDrugs() == null ? null : Utils.asJsonString(adverseReactionRequest.getListOfDrugs());
        this.previousExposure = adverseReactionRequest.getPreviousExposure();
        this.pregnancyStatus = adverseReactionRequest.getPregnancyStatus();
        this.breastfeedingInfant = adverseReactionRequest.getBreastfeedingInfant();
        this.bdqExposure = adverseReactionRequest.getBdqExposure();
        this.linezolidExposure = adverseReactionRequest.getLinezolidExposure();
        this.injectionDrugUse = adverseReactionRequest.getInjectionDrugUse();
        this.alcoholUse = adverseReactionRequest.getAlcoholUse();
        this.tobaccoUse = adverseReactionRequest.getTobaccoUse();
        this.hivReactive = adverseReactionRequest.getHivReactive();
        this.onsetDate = adverseReactionRequest.getOnsetDate() == null ? null : Utils.toLocalDateTimeWithNull(adverseReactionRequest.getOnsetDate());
        this.predominantCondition = adverseReactionRequest.getPredominantCondition() == null ? null : Utils.asJsonString(adverseReactionRequest.getPredominantCondition());
        this.otherPredominantCondition = adverseReactionRequest.getOtherPredominantCondition();
        this.reporterNarrative = adverseReactionRequest.getReporterNarrative();
        this.severity = adverseReactionRequest.getSeverity();
        this.daidsGrading = adverseReactionRequest.getDaidsGrading();
        this.adrSeriousness = adverseReactionRequest.getAdrSeriousness();
        this.dateOfDeath = Utils.toLocalDateTimeWithNull(adverseReactionRequest.getDateOfDeath());
        this.autopsyPerformed = adverseReactionRequest.getAutopsyPerformed();
        this.causeOfDeath = adverseReactionRequest.getCauseOfDeath();
        this.hospitalAdmissionDate = Utils.toLocalDateTimeWithNull(adverseReactionRequest.getHospitalAdmissionDate());
        this.hospitalDischargeDate = Utils.toLocalDateTimeWithNull(adverseReactionRequest.getHospitalDischargeDate());
    }

    public AdverseReaction(String currentWeight, String dateOfDeath) {
        this.currentWeight = currentWeight;
        this.dateOfDeath = Utils.toLocalDateTimeWithNull(dateOfDeath);
    }

    public AdverseReaction(String currentWeight, String dateOfDeath, String date, String predominantCondition) {
        this.currentWeight = currentWeight;
        this.dateOfDeath = Utils.toLocalDateTimeWithNull(dateOfDeath);
        this.onsetDate = Utils.toLocalDateTimeWithNull(date);
        this.hospitalAdmissionDate = Utils.toLocalDateTimeWithNull(date);
        this.hospitalDischargeDate = Utils.toLocalDateTimeWithNull(date);
        this.predominantCondition = predominantCondition;
    }

    public AdverseReaction(Long id) {
        this.id = id;
    }


}
