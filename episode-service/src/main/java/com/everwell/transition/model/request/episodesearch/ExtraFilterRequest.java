package com.everwell.transition.model.request.episodesearch;

import com.everwell.transition.constants.FieldConstants;
import com.everwell.transition.elasticsearch.ElasticConstants;
import com.everwell.transition.exceptions.ValidationException;
import com.everwell.transition.utils.Utils;
import com.fasterxml.jackson.core.type.TypeReference;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;

import java.io.IOException;
import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class ExtraFilterRequest {
    public String typeOfPatient;
    public List<String> monitoringMethodNew;
    public Boolean foregoBenefits;
    public String registrationDateRange;
    public Boolean doseNotReportedToday;
    public List<String> stageKey;
    public String endDateBefore;

    public String disease;

    EpisodeSearchRequest convertToEpisodeSearchRequest() {
        EpisodeSearchRequest searchRequest = new EpisodeSearchRequest();
        Map<String, Object> must = searchRequest.getSearch().getMust();
        if (!CollectionUtils.isEmpty(this.stageKey)) {
            must.put(FieldConstants.EPISODE_FIELD_STAGE_KEY, this.stageKey);
        }
        if (this.doseNotReportedToday != null) {
            Map<String, String> rangeQuery = new HashMap<>();
            rangeQuery.put(ElasticConstants.ELASTIC_SEARCH_RANGE_LOWER_THAN, Utils.getFormattedDateNew(Utils.getCurrentDateNew()));
            searchRequest.getRange().put(FieldConstants.EPISODE_FIELD_LAST_DOSAGE, rangeQuery);
        }
        if (!StringUtils.isEmpty(this.endDateBefore)) {
            Map<String, String> rangeQuery = new HashMap<>();
            rangeQuery.put(ElasticConstants.ELASTIC_SEARCH_RANGE_LOWER_THAN, endDateBefore);
            searchRequest.getRange().put(FieldConstants.EPISODE_FIELD_END_DATE, rangeQuery);
        }
        if (this.foregoBenefits != null) {
            must.put(FieldConstants.FIELD_FOREGO_BENEFITS, this.foregoBenefits);
        }
        if (!StringUtils.isEmpty(this.typeOfPatient)) {
            must.put(FieldConstants.EPISODE_FIELD_TYPE_OF_EPISODE, this.typeOfPatient);
        }
        if (!CollectionUtils.isEmpty(this.monitoringMethodNew)) {
            must.put(FieldConstants.MONITORING_METHOD, this.monitoringMethodNew);
        }
        if (!StringUtils.isEmpty(this.disease)) {
            must.put(FieldConstants.FIELD_DISEASE, this.disease);
        }
        if (this.registrationDateRange != null) {
            try {
                Range range = Utils.jsonToObject(this.registrationDateRange, new TypeReference<Range>() {});
                LocalDateTime today = Utils.getCurrentDateNew();
                LocalDateTime newDate = today.minusDays(range.getLowerBound());
                LocalDateTime olderDate = today.minusDays(range.getUpperBound());
                Map<String, Object> rangeSubQuery = new HashMap<>();
                rangeSubQuery.put(ElasticConstants.ELASTIC_SEARCH_RANGE_GREATER_THAN_EQUAL, Utils.formatDateNew(olderDate));
                rangeSubQuery.put(ElasticConstants.ELASTIC_SEARCH_RANGE_LOWER_THAN_EQUAL, Utils.formatDateNew(newDate));
                searchRequest.getRange().put(FieldConstants.EPISODE_FIELD_START_DATE, rangeSubQuery);
            } catch (IOException e) {
                throw new ValidationException("[RegistrationDateRange] filter value unexpected");
            }
        }
        return searchRequest;
    }

    @Data
    @NoArgsConstructor
    @AllArgsConstructor
    static public class Range {
        int lowerBound;
        int upperBound;
    }


}