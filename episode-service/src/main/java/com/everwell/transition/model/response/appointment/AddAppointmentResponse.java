package com.everwell.transition.model.response.appointment;

import com.everwell.transition.model.dto.appointment.Document;
import com.everwell.transition.model.dto.appointment.QuestionAnswers;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;


@Data
@NoArgsConstructor
@AllArgsConstructor
public class AddAppointmentResponse {
    private AppointmentResponse appointment;
    private List<QuestionAnswers> questionnaire;
    private List<Document> documents;
}

