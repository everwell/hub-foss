package com.everwell.transition.model.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Getter
public class DiseaseStageFieldMapDto {

    private Long fieldId;

    private Long stageId;

    private Long diseaseId;

    private Long diseaseStageId;

    private Long diseaseStageKeyMappingId;

    private String key;

    private String module;

    private boolean required;

    private boolean deleted;

    private String defaultValue;
}