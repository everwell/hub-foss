package com.everwell.transition.model.request.episode;

import com.everwell.transition.exceptions.ValidationException;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class UpdateRiskRequest {
    List<Long> highRiskIdList;
    List<Long> lowRiskIdList;
    Long addedBy;

    public void validate()
    {
        if(CollectionUtils.isEmpty(highRiskIdList) && CollectionUtils.isEmpty(lowRiskIdList))
            throw new ValidationException("Episode Id List cannot be empty");
    }
}
