package com.everwell.transition.model.dto;

import com.everwell.transition.exceptions.ValidationException;
import com.everwell.transition.model.request.episodesearch.EpisodeSearchRequest;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class EpisodeUnifiedSearchRequest {
    EpisodeSearchRequest episodeSearchRequest;
    EpisodeUnifiedFilters episodeUnifiedFilters;

    public void validate() {
        if (episodeUnifiedFilters == null)
            throw new ValidationException("Unified filters cannot be empty!");
    }
}
