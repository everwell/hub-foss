package com.everwell.transition.model.request.diagnostics;

import lombok.Getter;
import lombok.Setter;
import org.springframework.util.StringUtils;

import java.util.List;
import java.util.Map;


@Getter
@Setter
public class GenericTestRequest {


    private Long testRequestId;

    private String entityId; // stores the entity associated to the test

    private Long clientId;

    private String reason;

    private String subReason;

    private String status;

    private String previousATT;

    private String type;

    private String dateReported;

    private int testingFacilityHierarchyId;

    private boolean positive;

    private Map<String, Object> typeMap;

    List<String> stagesToExclude;

}
