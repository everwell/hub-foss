package com.everwell.transition.model.dto.treatmentPlan;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.util.Map;
import java.util.Set;

@Getter
@NoArgsConstructor
@AllArgsConstructor
public class TreatmentPlanDoseTimeDto {
    private Set<String> doseTimeSet;

    private Map<String, Integer> doseTimeToDoseCountMap;
}
