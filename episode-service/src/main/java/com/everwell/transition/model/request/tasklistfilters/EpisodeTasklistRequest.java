package com.everwell.transition.model.request.tasklistfilters;

import com.everwell.transition.exceptions.ValidationException;
import com.everwell.transition.model.request.episodesearch.EpisodeSearchRequest;
import lombok.*;
import org.springframework.data.domain.Sort;
import org.springframework.util.CollectionUtils;

import java.util.Collection;
import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class EpisodeTasklistRequest {

    EpisodeSearchRequest episodeSearchRequest;
    List<TaskListFilterRequest> taskListFilterRequests;

    public void validate() {
        if (CollectionUtils.isEmpty(this.taskListFilterRequests))
            throw new ValidationException("Filter list cannot be empty!");
    }

}
