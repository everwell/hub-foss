package com.everwell.transition.model.db;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Entity
@Table(name = "episode_log_store")
@Getter
@NoArgsConstructor
@AllArgsConstructor
public class EpisodeLogStore {


    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "category",columnDefinition = "text")
    String category;

    @Column(name = "category_group",columnDefinition = "text")
    String categoryGroup;


}
