package com.everwell.transition.model.request;


import com.everwell.transition.exceptions.ValidationException;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown=true)
public class AddEpisodeDocumentRequest {

    private Long episodeId;

    private Long diseaseId;

    private List<String> type;

    private String doctorName;

    private String doctorPhone;

    private List<String> dateOfRecord;

    private String notes;

    private String timeZone;

    private String addedOn;

    private Long addedById;

    private String addedByType;

    public void validate() {
        if (null == episodeId) {
            throw new ValidationException("episode id cannot be null");
        }
    }
}


