package com.everwell.transition.model.request.episode;

import com.everwell.transition.exceptions.ValidationException;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.util.CollectionUtils;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class EpisodeBulkRequest {
    List<Long> episodeIds;

    public void validate()
    {
        if(CollectionUtils.isEmpty(episodeIds))
            throw new ValidationException("Episode Id List cannot be empty");
    }
}
