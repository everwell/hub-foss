package com.everwell.transition.model.dto.notification;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.*;

import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class SmsDetails {
    List<UserSmsDetails> personList;
    Long vendorId;
    Long triggerId;
    Long templateId;
    List<Long> templateIds;
    Boolean isMandatory;
    Boolean defaultConsent;
    Boolean isDefaultTime;
    String timeOfSms;
}
