package com.everwell.transition.model.response.ins;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;
import java.util.Map;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class TypeResponse {
    List<Map<String, Object>> typeMappings;
}
