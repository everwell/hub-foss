package com.everwell.transition.model.request.dispensation;

import com.everwell.transition.elasticsearch.indices.EpisodeIndex;
import com.everwell.transition.exceptions.ValidationException;
import com.everwell.transition.model.dto.EpisodeDto;
import com.everwell.transition.utils.Utils;
import lombok.*;

import java.time.LocalDateTime;
import java.util.List;

@Getter
@Setter
@ToString
@AllArgsConstructor
@NoArgsConstructor
public class DispensationRequest {

    public Long entityId;

    public Long weight;

    public String weightBand;

    public String phase;

    public Long addedBy;

    public Long issuingFacility;

    public String issuedDate;

    public String dosingStartDate;

    public Long drugDispensedForDays;

    public String dateOfPrescription;

    public String notes;

    public List<ProductData> productDataList;

    public Boolean inventoryEnabled = false;

    public Boolean extendEndDate = false;

    @Data
    @AllArgsConstructor
    @NoArgsConstructor
    public static class ProductData {
        Long productId;
        Long productConfigId;
        Long unitsIssued;
        String dosingSchedule;
        String refillDate;
        String batchNumber;
        String expiryDate;
    }


    public void validate(DispensationRequest dispensationRequest, EpisodeDto episodeDto) {
        LocalDateTime issuedDate = Utils.convertStringToDateNew(dispensationRequest.getIssuedDate());
        LocalDateTime dateOfPrescription = Utils.convertStringToDateNew(dispensationRequest.getDateOfPrescription());
        LocalDateTime dosingStartDate = Utils.convertStringToDateNew(dispensationRequest.getDosingStartDate());

        LocalDateTime episodeStartDate = episodeDto.getStartDate();
        LocalDateTime episodeEndDate = episodeDto.getEndDate();

        if (issuedDate.isAfter(episodeEndDate)) {
            throw new ValidationException("Date of dispensation shouldn't be greater than the outcome end date.");
        }

        if (dateOfPrescription.isBefore(episodeStartDate)
                || dateOfPrescription.isAfter(episodeEndDate) ||
                dateOfPrescription.isAfter(issuedDate)) {
            throw new ValidationException("Date of Prescription should be greater than/equal to the treatment initiation date and should be lesser than/equal to the Patient's End Date & Dispensation Date.");
        }

        if (dosingStartDate.isBefore(issuedDate) || dosingStartDate.isAfter(episodeEndDate)) {
            throw new ValidationException("Dosing start date should be greater than/equal to the Date of Dispensation date and should be lesser than/equal to the Patient's End Date.");
        }
    }

}
