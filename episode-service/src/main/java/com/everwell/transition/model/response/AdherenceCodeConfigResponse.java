package com.everwell.transition.model.response;

import com.everwell.transition.model.dto.AdherenceCodeExtra;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class AdherenceCodeConfigResponse {
    char code;
    String codeName;
    AdherenceCodeExtra design;
    String legendText;
}
