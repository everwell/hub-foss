package com.everwell.transition.model.dto.notification;

import com.everwell.transition.model.request.episodesearch.EpisodeSearchRequest;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class NotificationTriggerEvent {
    private NotificationTriggerDto notificationTriggerDto;

    private EpisodeSearchRequest episodeSearchRequest;
}
