package com.everwell.transition.model.response.dispensation;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Getter
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class DispensationResponse {
    public Long dispensationId;
}
