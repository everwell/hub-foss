package com.everwell.transition.model.dto.diagnostics;

import com.everwell.transition.model.request.diagnostics.ResultRequestV2;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
public abstract class TypeRequest {

    protected ResultRequestV2 result;

    public abstract List<String> getFinalInterpretationList();

    public boolean hasSampleSupport() {
        return false;
    }

}
