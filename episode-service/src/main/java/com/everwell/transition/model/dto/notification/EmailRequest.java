package com.everwell.transition.model.dto.notification;

import com.everwell.transition.enums.ins.Language;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class EmailRequest {
    String body;
    String subject;
    Boolean isEmailCommon;
    List<EmailTemplateDto> recipientList;
    EmailAttachmentDto attachmentDto;
    String sender;
    Long vendorId;
    Language language = Language.NON_UNICODE;
    Long templateId;
    Long triggerId;
    Long clientId;

    public EmailRequest(Long clientId, Long triggerId, Long templateId, Long vendorId, String sender, List<EmailTemplateDto> recipientList) {
        this.clientId = clientId;
        this.triggerId = triggerId;
        this.templateId = templateId;
        this.vendorId = vendorId;
        this.sender = sender;
        this.recipientList = recipientList;
    }
}
