package com.everwell.transition.model.dto;

import com.everwell.transition.enums.episode.EpisodeValidation;
import com.everwell.transition.exceptions.ValidationException;
import com.everwell.transition.model.request.episodesearch.EpisodeSearchRequest;
import lombok.*;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class EpisodeReportsRequest {
    EpisodeSearchRequest episodeSearchRequest;
    EpisodeReportsFilter episodeReportsFilter;

    public void validate() {
        if (episodeReportsFilter == null)
            throw new ValidationException(EpisodeValidation.INVALID_EPISODE_REPORTS_FILTER.getMessage());
    }
}
