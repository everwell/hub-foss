package com.everwell.transition.model.db;

import com.everwell.transition.constants.Constants;
import com.everwell.transition.model.request.AddEpisodeDocumentRequest;
import com.everwell.transition.utils.Utils;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateTimeDeserializer;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateTimeSerializer;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.*;
import java.time.*;
import java.util.List;

@Entity
@Table(name = "episode_document")
@Data
@NoArgsConstructor
@AllArgsConstructor
public class EpisodeDocument {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "episode_id")
    private Long episodeId;

    @Column(name = "disease_id")
    private Long diseaseId;

    @Column(name = "doctor_name")
    private String doctorName;

    @Column(name = "doctor_phone")
    private String doctorPhone;

    @Column(name = "notes")
    private String notes;

    @Column(name = "added_on")
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime addedOn;

    @Column(name = "time_zone")
    private String timeZone;

    @Column(name = "added_by_id")
    private Long addedById;

    @Column(name = "added_by_type")
    private String addedByType;
    
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = Constants.DATE_FORMAT)
    @UpdateTimestamp
    @Column(name = "updated_on")
    private LocalDateTime updatedOn;

    @OneToMany(cascade = CascadeType.ALL , fetch = FetchType.LAZY)
    @JoinColumn(name = "episode_document_id", referencedColumnName = "id", insertable = false, updatable = false)
    private List<EpisodeDocumentDetails> episodeDocumentDetailsList;

    public EpisodeDocument(AddEpisodeDocumentRequest addEpisodeDocumentRequest) {
        this.episodeId = addEpisodeDocumentRequest.getEpisodeId();
        this.diseaseId = addEpisodeDocumentRequest.getDiseaseId();
        this.doctorName = addEpisodeDocumentRequest.getDoctorName();
        this.doctorPhone = addEpisodeDocumentRequest.getDoctorPhone();
        this.notes = addEpisodeDocumentRequest.getNotes();
        this.addedOn = LocalDateTime.now();
        this.timeZone = addEpisodeDocumentRequest.getTimeZone();
        this.addedById = addEpisodeDocumentRequest.getAddedById();
        this.addedByType = addEpisodeDocumentRequest.getAddedByType();
    }
}
