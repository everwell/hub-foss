package com.everwell.transition.model.request.treatmentPlan;

import com.everwell.transition.constants.TreatmentPlanConstants;
import com.everwell.transition.enums.treamentPlan.TreatmentPlanValidation;
import com.everwell.transition.exceptions.ValidationException;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.apache.commons.lang3.StringUtils;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class TreatmentPlanProductMapRequest {
    Long productId;
    String productName;
    String schedule;
    Integer frequencyInDays;
    String otherDrugName;
    Long treatmentPlanProductMapId;
    List<DoseDetails> doseDetails;
    String otherDosageForm;
    Boolean sos;

    public void validate() {
        if (null == productId) {
            throw new ValidationException(TreatmentPlanValidation.PRODUCT_ID_INVALID.getMessage());
        }
        if (StringUtils.isEmpty(schedule) || !schedule.matches(TreatmentPlanConstants.SCHEDULE_STRING_REGEX)) {
            throw new ValidationException(TreatmentPlanValidation.SCHEDULE_STRING_INVALID.getMessage());
        }
    }

    public TreatmentPlanProductMapRequest(Long productId, String productName, String schedule, Integer frequencyInDays, String otherDrugName) {
        this.productId = productId;
        this.productName = productName;
        this.schedule = schedule;
        this.frequencyInDays = frequencyInDays;
        this.otherDrugName = otherDrugName;
    }

    public TreatmentPlanProductMapRequest(Long treatmentPlanProductMapId, Long productId, String productName, String schedule, Integer frequencyInDays, String otherDrugName, String otherDosageForm, Boolean sos) {
        this.productId = productId;
        this.productName = productName;
        this.schedule = schedule;
        this.frequencyInDays = frequencyInDays;
        this.otherDrugName = otherDrugName;
        this.treatmentPlanProductMapId = treatmentPlanProductMapId;
        this.otherDosageForm = otherDosageForm;
        this.sos = sos;
    }
}