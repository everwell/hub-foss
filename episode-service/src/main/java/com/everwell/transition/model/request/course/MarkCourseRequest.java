package com.everwell.transition.model.request.course;

import com.everwell.transition.exceptions.ValidationException;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.util.CollectionUtils;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class MarkCourseRequest {

    Long episodeId;

    Long courseId;

    public void validate()
    {
        if (episodeId == null || episodeId == 0)
            throw new ValidationException("episodeId must not be null!");
    }
}
