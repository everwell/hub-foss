package com.everwell.transition.model.dto;

import lombok.*;

import java.time.LocalDateTime;

@Data
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class ActivityDto {
    String date;
    Long patientVisits;
    Long meetingCount;
    String otherActivity;
    String uploadedFile;

}
