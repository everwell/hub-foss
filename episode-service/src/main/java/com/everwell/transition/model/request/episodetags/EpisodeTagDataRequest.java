package com.everwell.transition.model.request.episodetags;

import com.everwell.transition.exceptions.ValidationException;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateTimeDeserializer;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateTimeSerializer;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.util.StringUtils;

import java.time.LocalDateTime;

@Data
@AllArgsConstructor
@NoArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class EpisodeTagDataRequest {
    String tagName;
    @JsonFormat(pattern = "dd-MM-yyyy HH:mm:ss")
    @JsonSerialize(using = LocalDateTimeSerializer.class)
    @JsonDeserialize(using = LocalDateTimeDeserializer.class)
    LocalDateTime tagDate;
    Long episodeId;
    Boolean sticky = false;

    public void validate() {
        if (StringUtils.isEmpty(tagName))
            throw new ValidationException("Tag Name cannot be empty!");
        if (episodeId == null)
            throw new ValidationException("Episode Id cannot be empty!");
    }

    public EpisodeTagDataRequest(String tagName, Long episodeId, Boolean sticky) {
        this.tagName = tagName;
        this.episodeId = episodeId;
        this.sticky = sticky;
    }

}
