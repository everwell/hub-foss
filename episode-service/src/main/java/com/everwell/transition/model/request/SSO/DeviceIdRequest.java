package com.everwell.transition.model.request.SSO;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class DeviceIdRequest {

    private List<String> userNames;
}
