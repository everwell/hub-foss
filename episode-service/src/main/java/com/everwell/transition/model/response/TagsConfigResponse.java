package com.everwell.transition.model.response;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class TagsConfigResponse {
    String tagName;
    String tagDisplayName;
    String tagIcon;
    String tagDescription;
    String tagGroup;
}
