package com.everwell.transition.model.response;

import com.everwell.transition.model.db.CausalityManagement;
import com.everwell.transition.model.dto.CausalityMedicineDto;
import com.everwell.transition.model.dto.KeyValuePair;
import com.everwell.transition.model.dto.SuspectedDrugsDto;
import com.everwell.transition.utils.Utils;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.core.type.TypeReference;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.ToString;

import java.io.IOException;
import java.util.List;

@AllArgsConstructor
@ToString
@Getter
@JsonInclude(JsonInclude.Include.NON_NULL)
public class CausalityManagementResponse {
    Long adverseEventId;
    Long episodeId;
    String causeMedicineLinkage;
    String dechallenge;
    String rechallenge;
    String expectedness;
    String actionTakenOrNot;
    List<KeyValuePair> actionTaken;
    String treatmentDetails;
    List<CausalityMedicineDto> antiTbMedicines;
    List<CausalityMedicineDto> otherMedicines;
    List<CausalityMedicineDto> newMedicines;
    String isoniazid ;
    String rifampicin ;
    String pyrazinamide ;
    String ethambutol ;
    String kanamycin ;
    String amikacin ;
    String capreomycin ;
    String levofloxacin ;
    String moxifloxacin ;
    String cycloserine ;
    String naPAS ;
    String linezolid ;
    String clofazimine ;
    String amxClv ;
    String clarithromycin ;
    String bedaquiline ;
    String delamanid ;
    String pretomonid ;
    String rifapentine ;
    String prothionamide;
    String ethionamde ;


    public CausalityManagementResponse(CausalityManagement causalityManagement, Long episodeId, Long adverseEventId, SuspectedDrugsDto suspectedDrugs) throws IOException {
        this.episodeId = episodeId;
        this.adverseEventId = adverseEventId;
        this.causeMedicineLinkage = causalityManagement.getCauseMedicineLinkage();
        this.actionTakenOrNot = causalityManagement.getActionTakenOrNot();
        this.actionTaken = causalityManagement.getActionTaken() == null ? null : Utils.jsonToObject(causalityManagement.getActionTaken(),new TypeReference<List<KeyValuePair>>(){});
        this.antiTbMedicines = causalityManagement.getAntiTbMedicines() == null ? null : Utils.jsonToObject(causalityManagement.getAntiTbMedicines(),new TypeReference<List<CausalityMedicineDto>>(){});
        this.dechallenge = causalityManagement.getDechallenge();
        this.rechallenge = causalityManagement.getRechallenge();
        this.expectedness = causalityManagement.getExpectedness();
        this.treatmentDetails = causalityManagement.getTreatmentDetails();
        this.otherMedicines = causalityManagement.getOtherMedicines() == null ? null : Utils.jsonToObject(causalityManagement.getOtherMedicines(),new TypeReference<List<CausalityMedicineDto>>(){});
        this.newMedicines = causalityManagement.getNewMedicines() == null ? null : Utils.jsonToObject(causalityManagement.getNewMedicines(),new TypeReference<List<CausalityMedicineDto>>(){});
        if(suspectedDrugs != null) {
            this.amikacin = suspectedDrugs.getAmikacin();
            this.amxClv = suspectedDrugs.getAmxClv();
            this.bedaquiline = suspectedDrugs.getBedaquiline();
            this.capreomycin = suspectedDrugs.getCapreomycin();
            this.clarithromycin = suspectedDrugs.getClarithromycin();
            this.clofazimine = suspectedDrugs.getClofazimine();
            this.cycloserine = suspectedDrugs.getCycloserine();
            this.delamanid = suspectedDrugs.getDelamanid();
            this.ethambutol = suspectedDrugs.getEthambutol();
            this.ethionamde = suspectedDrugs.getEthionamde();
            this.isoniazid = suspectedDrugs.getIsoniazid();
            this.kanamycin = suspectedDrugs.getKanamycin();
            this.levofloxacin = suspectedDrugs.getLevofloxacin();
            this.linezolid = suspectedDrugs.getLinezolid();
            this.moxifloxacin = suspectedDrugs.getMoxifloxacin();
            this.naPAS = suspectedDrugs.getNaPAS();
            this.pretomonid = suspectedDrugs.getPretomonid();
            this.prothionamide = suspectedDrugs.getProthionamide();
            this.pyrazinamide = suspectedDrugs.getPyrazinamide();
            this.rifampicin = suspectedDrugs.getRifampicin();
            this.rifapentine = suspectedDrugs.getRifapentine();
        }
    }
}
