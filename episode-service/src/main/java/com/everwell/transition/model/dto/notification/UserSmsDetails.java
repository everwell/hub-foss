package com.everwell.transition.model.dto.notification;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.*;

import java.util.Map;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@JsonInclude(JsonInclude.Include.NON_NULL)
@ToString
public class UserSmsDetails {

    private Long id;
    private Map<String,String> parameters;
    private String phone;
}

