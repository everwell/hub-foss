package com.everwell.transition.model.dto.episode;

import com.everwell.transition.model.dto.DiseaseStageFieldMapDto;
import com.everwell.transition.model.dto.EpisodeDto;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.util.List;

@Getter
@NoArgsConstructor
@AllArgsConstructor
public class EpisodeDetailsDto {
    private EpisodeDto episodeDto;

    private List<DiseaseStageFieldMapDto> diseaseStageFieldMapDtoList;
}
