package com.everwell.transition.model.response;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.Map;

@AllArgsConstructor
@Data
public class EpisodeUpdateDocResponse {
    Map<String, Object> fields;
    String module;
}
