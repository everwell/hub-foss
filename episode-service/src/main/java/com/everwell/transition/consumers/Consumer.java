package com.everwell.transition.consumers;

import org.springframework.amqp.core.Message;

import java.io.IOException;

public interface Consumer {
    void consume(Message message) throws IOException;
}
