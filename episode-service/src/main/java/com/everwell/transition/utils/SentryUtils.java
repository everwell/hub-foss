package com.everwell.transition.utils;

import com.everwell.transition.constants.Constants;
import io.sentry.Sentry;
import io.sentry.event.EventBuilder;
import io.sentry.event.helper.BasicRemoteAddressResolver;
import io.sentry.event.interfaces.ExceptionInterface;
import io.sentry.event.interfaces.HttpInterface;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.util.StringUtils;
import org.springframework.web.context.request.ServletWebRequest;
import org.springframework.web.context.request.WebRequest;

import java.io.IOException;
import java.io.InputStream;

public class SentryUtils {

    private static String releaseVersion;

    @Value("${transition.sentry.release.version}")
    public void setVersion(String version) {
        this.releaseVersion = version;
    }

    public static EventBuilder eventBuilder(Exception ex, WebRequest request, InputStream inputStream) throws IOException {
        EventBuilder builder = new EventBuilder()
                .withSentryInterface(new ExceptionInterface(ex))
                .withSentryInterface(new HttpInterface(((ServletWebRequest) request).getRequest(), new BasicRemoteAddressResolver(), Utils.readRequestBody(inputStream)))
                .withRelease(releaseVersion);

        return builder;
    }

    public static EventBuilder eventBuilder(Exception ex) {
        EventBuilder builder = new EventBuilder()
                .withSentryInterface(new ExceptionInterface(ex));
        return builder;
    }

    public static void captureException(Exception ex, String data) {
        EventBuilder builder = eventBuilder(ex);
        if (StringUtils.hasText(data)) {
            builder.withExtra(Constants.DATA, data);
        }
        Sentry.capture(builder);
    }
}
