package com.everwell.transition.postconstruct;

import com.everwell.transition.model.db.EpisodeLogStore;
import com.everwell.transition.repositories.EpisodeLogStoreRepository;
import lombok.Getter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Component
public class EpisodeLogStoreCreation {

    @Autowired
    private EpisodeLogStoreRepository episodeLogStoreRepository;

    @Getter
    private Map<String, EpisodeLogStore> episodeLogStoreMap = new HashMap<>();

    @PostConstruct
    public void initLogStore() {
        List<EpisodeLogStore> episodeLogStoreList = episodeLogStoreRepository.findAll();
        episodeLogStoreMap = episodeLogStoreList.stream().collect(Collectors.toMap(
            EpisodeLogStore::getCategory,
            v -> v
        ));
    }
}
