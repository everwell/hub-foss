package com.everwell.transition.binders;

import org.springframework.cloud.stream.annotation.Input;
import org.springframework.cloud.stream.annotation.Output;
import org.springframework.messaging.MessageChannel;
import org.springframework.messaging.SubscribableChannel;

public interface ABDMCommunicationBinder {
    String DIAGNOSTICS_ADD_UPDATE_EVENT = "diagnostics_add_update_event";
    String DISPENSATION_ADD_UPDATE_EVENT = "share-dispensation";
    String ADBM_LINK_CARE_CONTEXT = "ex.topic.abdm_link_care_context";
    String ADBM_CARE_CONTEXT_NOTIFY = "ex.topic.abdm_care_context_notify";
    String ABDM_NOTIFY_VIA_SMS = "ex.topic.abdm_notify_via_sms";

    @Input(DIAGNOSTICS_ADD_UPDATE_EVENT)
    SubscribableChannel diagnosticsAddUpdateEventInput();

    @Input(DISPENSATION_ADD_UPDATE_EVENT)
    SubscribableChannel dispensationAddUpdateEventInput();

    @Output(ADBM_LINK_CARE_CONTEXT)
    MessageChannel abdmLinkCareContextOutput();

    @Output(ADBM_CARE_CONTEXT_NOTIFY)
    MessageChannel abdmCareContextNotifyOutput();

    @Output(ABDM_NOTIFY_VIA_SMS)
    MessageChannel abdmNotifyViaSMSOutput();

}
