package com.everwell.transition.binders;

import org.springframework.cloud.stream.annotation.Input;
import org.springframework.cloud.stream.annotation.Output;
import org.springframework.messaging.MessageChannel;
import org.springframework.messaging.SubscribableChannel;

public interface PatientProcessorBinder {
    String STAGE_INPUT = "stageInputChannel";
    String STAGE_OUTPUT = "stageOutputChannel";

    @Input(STAGE_INPUT)
    SubscribableChannel stageInput();

    @Output(STAGE_OUTPUT)
    MessageChannel stageOutput();
}
