package com.everwell.transition.binders;

import org.springframework.cloud.stream.annotation.Output;
import org.springframework.messaging.MessageChannel;

public interface EpisodeEventsBinder {

    String EVENTFLOW_EXCHANGE = "event-input";
    String USER_SERVICE_EXCHANGE = "ex-episode-user";

    @Output(EVENTFLOW_EXCHANGE)
    MessageChannel eventOutput();

    @Output(USER_SERVICE_EXCHANGE)
    MessageChannel userServiceEventOutput();
}
