package com.everwell.transition.binders;

import org.springframework.cloud.stream.annotation.Input;
import org.springframework.messaging.SubscribableChannel;

public interface EpisodeNotificationBinder {

    String EPISODE_NOTIFICATION_INPUT = "episode-notification-input";

    @Input(EPISODE_NOTIFICATION_INPUT)
    SubscribableChannel notificationInput();
}
