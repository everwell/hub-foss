package com.everwell.transition.exceptions;

public class InternalServerErrorException extends RuntimeException {

  public InternalServerErrorException(String message) {
    super(message);
  }

}
