package com.everwell.transition.exceptions;

public class ServiceException extends RuntimeException {

    public ServiceException(String exception) {
        super(exception);
    }
}
