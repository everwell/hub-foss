package com.everwell.transition.filters;

import com.everwell.transition.constants.Constants;
import com.everwell.transition.model.db.Client;
import com.everwell.transition.model.response.Response;
import com.everwell.transition.service.ClientService;
import com.everwell.transition.utils.CacheUtils;
import com.everwell.transition.utils.JwtUtils;
import com.everwell.transition.utils.OutputStreamUtils;
import io.jsonwebtoken.ExpiredJwtException;
import io.jsonwebtoken.MalformedJwtException;
import io.jsonwebtoken.SignatureException;
import io.jsonwebtoken.UnsupportedJwtException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.WebAuthenticationDetailsSource;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

@Component
public class JwtFilters extends OncePerRequestFilter {

    @Autowired
    ClientService clientService;

    @Autowired
    JwtUtils jwtUtils;

    @Autowired
    OutputStreamUtils outputStreamUtils;

    @Override
    public void doFilterInternal(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, FilterChain filterChain) throws ServletException, IOException {
        String authorizationHeader = httpServletRequest.getHeader("Authorization");
        try {
            if (authorizationHeader != null && authorizationHeader.startsWith("Bearer ") && !authorizationHeader.equals("Bearer undefined")) {
                String jwt = authorizationHeader.substring(7);
                String clientId = jwtUtils.getSubjectFromToken(jwt);
                Client client = clientService.getClient(Long.valueOf(clientId));
                String blackListToken = CacheUtils.getFromCache(Constants.BLACKLIST_TOKEN + jwt);
                if (!StringUtils.hasLength(blackListToken) && jwtUtils.validateToken(jwt, client)) {
                    UsernamePasswordAuthenticationToken usernamePasswordAuthenticationToken = new UsernamePasswordAuthenticationToken(client, null, Collections.emptyList());
                    usernamePasswordAuthenticationToken
                            .setDetails(new WebAuthenticationDetailsSource().buildDetails(httpServletRequest));
                    SecurityContextHolder.getContext().setAuthentication(usernamePasswordAuthenticationToken);
                }
            }
        } catch (IllegalArgumentException | UnsupportedJwtException e) {
            outputStreamUtils.writeCustomResponseToOutputStream(httpServletResponse, new Response<>(e.getMessage()), HttpStatus.BAD_REQUEST);
            return;
        } catch (ExpiredJwtException | SignatureException | ArrayIndexOutOfBoundsException e) {
            outputStreamUtils.writeCustomResponseToOutputStream(httpServletResponse, new Response<>(e.getMessage()), HttpStatus.UNAUTHORIZED);
            return;
        } catch (MalformedJwtException e) {
            outputStreamUtils.writeCustomResponseToOutputStream(httpServletResponse, new Response<>(Constants.MALFORMED_JWT), HttpStatus.UNAUTHORIZED);
            return;
        }
        filterChain.doFilter(httpServletRequest, httpServletResponse);
    }

    @Override
    public boolean shouldNotFilter(HttpServletRequest request) {
        List<String> shouldNotFilterUrls = Arrays.asList(Constants.AUTH_WHITELIST);
        return shouldNotFilterUrls.contains(request.getRequestURI());
    }

}
