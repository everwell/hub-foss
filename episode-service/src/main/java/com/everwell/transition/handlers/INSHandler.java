package com.everwell.transition.handlers;

import com.everwell.transition.binders.INSBinder;
import com.everwell.transition.constants.Constants;
import com.everwell.transition.model.dto.GenericEvents;
import com.everwell.transition.model.dto.notification.SmsDetails;
import com.everwell.transition.model.dto.notification.EmailRequest;
import com.everwell.transition.model.dto.notification.PushNotificationDetails;
import com.everwell.transition.model.dto.notification.SmsDetails;
import com.everwell.transition.utils.Utils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.stream.annotation.EnableBinding;
import org.springframework.context.event.EventListener;
import org.springframework.messaging.support.MessageBuilder;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
@EnableBinding(INSBinder.class)
public class INSHandler {

    private static Logger LOGGER = LoggerFactory.getLogger(INSHandler.class);

    @Autowired
    INSBinder insBinder;

    public void sendNotificationToINS(List<GenericEvents<PushNotificationDetails>> notificationDetails, Long clientId) {
        notificationDetails.forEach(notification -> insBinder.insNotificationOutput().send(MessageBuilder.withPayload(Utils.asJsonString(notification)).setHeader(Constants.EVENT_CLIENT_ID, clientId).build()));
    }

    public void sendSmsToINS(List<GenericEvents<SmsDetails>> smsDetails, Long clientId) {
        smsDetails.forEach(sms -> insBinder.insSmsOutput().send(MessageBuilder.withPayload(Utils.asJsonString(sms)).setHeader(Constants.EVENT_CLIENT_ID, String.valueOf(clientId)).build())
        );
    }

    public void sendEmailToINS(GenericEvents<EmailRequest> emailDetails, Long clientId) {
        insBinder.insEmailOutput().send(MessageBuilder.withPayload(Utils.asJsonString(emailDetails)).setHeader(Constants.EVENT_CLIENT_ID, String.valueOf(clientId)).build());
    }
}
