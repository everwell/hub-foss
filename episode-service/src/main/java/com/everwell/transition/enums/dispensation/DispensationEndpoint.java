package com.everwell.transition.enums.dispensation;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public enum DispensationEndpoint {

    SEARCH_PRODUCTS("/dispensation/v1/product/search"),
    PRODUCT_STOCK_SEARCH("/dispensation/v1/product/stocks/search"),
    DISPENSATION_ENTITY("/dispensation/v1/dispensation/entity"),
    GET_DISPENSATION("/dispensation/v1/dispensation"),
    CREATE_DISPENSATION("/dispensation/v1/dispensation"),
    DELETE_DISPENSATION("/dispensation/v1/dispensation"),
    CREATE_DISPENSATION_INVENTORY("/dispensation/v2/dispensation"),
    DISPENSATION_ENTITY_SEARCH("/dispensation/v1/dispensation/entity/search"),
    RETURN_DISPENSATION("/dispensation/v1/dispensation/return"),
    RETURN_DISPENSATION_INVENTORY("/dispensation/v2/dispensation/return"),
    ADD_PRODUCT("/dispensation/v1/product"),
    ADD_PRODUCT_BULK("/dispensation/v1/products"),
    GET_PRODUCT("/dispensation/v1/product"),
    GET_ALL_PRODUCTS("/dispensation/v1/products"),
    STOCK_CREDIT("/dispensation/v1/stock"),
    STOCK_DEBIT("/dispensation/v1/stock"),
    STOCK_UPDATE("/dispensation/v1/stocks"),
    SEARCH_DISPENSATION_ENTITY("/dispensation/v1/dispensation/%s/entity");

    @Getter
    private final String endpointUrl;
}
