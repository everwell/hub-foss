package com.everwell.transition.enums.iam;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public enum IAMValidation {
    UNABLE_TO_CONNECT("An attempt to connect IAM failed via data-gateway!"),
    START_DATE_VALIDATION("start date cannot be null"),
    ERROR_AT_REGISTER_ENTITY("Error during register entity"),
    NEW_MONITORING_METHOD_VALIDATION("New Monitoring method cannot be null or empty"),
    CLOSE_CASE_VALIDATION("Error during close case at IAM"),
    UNABLE_TO_OPEN_REOPEN_CASE("Unable to reopen case"),
    UNABLE_TO_FETCH_ADHERENCE_DETAILS("Unable to fetch adherence details");

    @Getter
    private  final String message;

}
