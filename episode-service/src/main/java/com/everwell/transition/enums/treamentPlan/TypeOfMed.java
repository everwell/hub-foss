package com.everwell.transition.enums.treamentPlan;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
public enum TypeOfMed {
    all (null),
    normal (false),
    sos (true);

    @Getter
    final Boolean state;
}
