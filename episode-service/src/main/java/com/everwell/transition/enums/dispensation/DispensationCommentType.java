package com.everwell.transition.enums.dispensation;

public enum DispensationCommentType {
    REASON_FOR_RETURN,
    REASON_FOR_CREDIT,
    REASON_FOR_DEBIT,
    REASON_FOR_ADD
}
