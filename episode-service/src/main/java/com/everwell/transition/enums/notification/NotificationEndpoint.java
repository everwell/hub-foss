package com.everwell.transition.enums.notification;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public enum NotificationEndpoint {

    UPDATE_NOTIFICATION("/ins/v1/notification"),
    DELETE_NOTIFICATION("/ins/v1/notification"),
    GET_NOTIFICATION("/ins/v1/notification/bulk"),
    UPDATE_NOTIFICATION_BULK("/ins/v1/notification/bulk"),
    GET_UNREAD_NOTIFICATION("/ins/v1/notification/unread/bulk");

    @Getter
    private final String endpointUrl;
}
