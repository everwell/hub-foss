package com.everwell.transition.enums.ins;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public enum INSEndpoint {

    GET_ENGAGEMENT_BULK("/ins/v1/engagement-bulk"),
    SAVE_ENGAGEMENT_BULK("/ins/v1/engagement-bulk"),
    UPDATE_ENGAGEMENT_BULK("/ins/v1/engagement-bulk"),
    GET_NOTIFICATION_TYPES("/ins/v1/allTypes"),
    ADD_NOTIFICATION_LOGS("/ins/v1/notification/logs");

    @Getter
    private final String endpointUrl;
}
