package com.everwell.transition.parser;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;

@Service
public class RuleParser <I, O> {

    @Autowired
    private MVELParser mvelParser;

    private final String INPUT_KEYWORD = "input";
    private final String OUTPUT_KEYWORD = "output";

    public boolean parseCondition(String expression, I inputData) {
        Map<String, I> input = new HashMap<>();
        input.put(INPUT_KEYWORD, inputData);
        return mvelParser.parseMvelExpression(expression, input);
    }

    public O parseAction(String expression, O outputResult, Object inputDataAction) {
        Map<String, O> input = new HashMap<>();
        input.put(OUTPUT_KEYWORD, outputResult);
        Map<String, Object> inputData = new HashMap<>();
        inputData.put(INPUT_KEYWORD, inputDataAction);
        mvelParser.eval(expression, input, inputData);
        return outputResult;
    }

}
