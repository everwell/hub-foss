package com.everwell.transition.service.impl;

import com.everwell.transition.enums.user.UserServiceEndpoint;
import com.everwell.transition.enums.Event;
import com.everwell.transition.enums.user.UserServiceField;
import com.everwell.transition.enums.user.UserServiceValidation;
import com.everwell.transition.model.dto.DiseaseStageFieldMapDto;
import com.everwell.transition.model.dto.EventDto;
import com.everwell.transition.model.response.Response;
import com.everwell.transition.service.UserService;
import com.everwell.transition.utils.Utils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;


import com.everwell.transition.exceptions.ValidationException;
import org.springframework.util.CollectionUtils;

import java.util.*;
import java.util.stream.Collectors;

import static com.everwell.transition.constants.UserServiceConstants.*;



@Service
public class UserServiceImpl extends DataGatewayHelperServiceImpl implements UserService {

    @Autowired
    private ApplicationEventPublisher applicationEventPublisher;

    public  <T> T processResponseEntity(ResponseEntity<Response<T>> responseEntity, String errorResponse)
    {
        if (null == responseEntity.getBody() || !responseEntity.getBody().isSuccess()){
            throw new ValidationException(errorResponse);
        }
        return responseEntity.getBody().getData();
    }

    private <T> void processEvent(T message, Event userServiceRoutingKeys) {
        EventDto<T> eventDto = new EventDto<>(userServiceRoutingKeys, message);
        applicationEventPublisher.publishEvent(eventDto);
    }

    private ResponseEntity<Response<Map<String, Object>>> getUserResponse(Long userId) {
        String endPoint = UserServiceEndpoint.GET_USER.getEndpointUrl();
        Map<String, Object> queryParams = new HashMap<>();
        queryParams.put("id", userId);
        return getExchange(endPoint, new ParameterizedTypeReference<Response<Map<String, Object>>>() {}, null, queryParams);
    }

    private ResponseEntity<Response<Map<String, Object>>> createUserResponse(Map<String, Object> addUserMap) {
        String endPoint = UserServiceEndpoint.CREATE_USER.getEndpointUrl();
        return postExchange(endPoint, addUserMap, new ParameterizedTypeReference<Response<Map<String, Object>>>() {});
    }

    private ResponseEntity<Response<List<Map<String, Object>>>> fetchDuplicatesResponse(Map<String, Object> fetchDuplicatesRequest) {
        String endPoint = UserServiceEndpoint.FETCH_DUPLICATE_USER.getEndpointUrl();
        return postExchange(endPoint, fetchDuplicatesRequest, new ParameterizedTypeReference<Response<List<Map<String, Object>>>>() {});
    }

    @Override
    public  ResponseEntity<Response<List<Map<String, Object>>>> getByPrimaryPhone(String phoneNumber) {
        String endPoint = UserServiceEndpoint.GET_USER_BY_PRIMARY_PHONE.getEndpointUrl();
        return getExchange(endPoint, new ParameterizedTypeReference<Response<List<Map<String, Object>>>>() {}, phoneNumber, null);
    }

    @Override
    public List<Long> fetchDuplicates(Map<String, Object> fetchDuplicatesRequest) {
        ResponseEntity<Response<List<Map<String, Object>>>> responseEntity = fetchDuplicatesResponse(fetchDuplicatesRequest);
        List<Map<String, Object>> duplicateUserResponseList = Utils.processResponseEntity(responseEntity,  UserServiceValidation.FETCH_DUPLICATE_USER_ERROR.getMessage());
        return duplicateUserResponseList
                    .stream()
                    .map(i->Long.parseLong(String.valueOf(i.get(UserServiceField.Id.getFieldName()))))
                    .distinct()
                    .collect(Collectors.toList());
    }

    @Override
    public Map<String, Object> getUserDetails(Long personId) {
        Map<String, Object> processedUserResponseMap = new HashMap<>();
        if (null != personId) {
            ResponseEntity<Response<Map<String, Object>>> responseEntity = getUserResponse(personId);
            processedUserResponseMap = Utils.processResponseEntity(responseEntity, UserServiceValidation.GET_USER_ERROR.getMessage());
        }
        return processedUserResponseMap;
    }

    @Override
    public Long createUser(Map<String, Object> processedRequestMap) {
        ResponseEntity<Response<Map<String, Object>>> responseEntity = createUserResponse(processedRequestMap);
        Map<String, Object> userResponseMap = Utils.processResponseEntity(responseEntity, UserServiceValidation.CREATE_USER_ERROR.getMessage());
        return Long.parseLong(String.valueOf(userResponseMap.get(UserServiceField.Id.getFieldName())));
    }

    @Override
    public void updateUser(Map<String, Object> updateUserMap, Long personId) {
        if (!CollectionUtils.isEmpty(updateUserMap)) {
            updateUserMap.put(UserServiceField.PERSON_ID.getFieldName(), personId);
            ResponseEntity<Response<String>> responseEntity = updateUserResponse(updateUserMap);
            Utils.processResponseEntity(responseEntity, UserServiceValidation.UPDATE_USER_ERROR.getMessage());
            if (null != updateUserMap.get(UserServiceField.MOBILE_LIST.getFieldName())) {
                processEvent(updateUserMap, Event.UPDATE_USER_MOBILE);
            }
            if (null != updateUserMap.get(UserServiceField.EMAIL_LIST.getFieldName())) {
                processEvent(updateUserMap, Event.UPDATE_USER_EMAIL);
            }
        }
    }

    @Override
    public void addUserRelation(Map<String, Object> userRelationRequest) {
        String endpoint = UserServiceEndpoint.ADD_RELATION.getEndpointUrl();
        ResponseEntity<Response<String>> response = postExchange(endpoint, userRelationRequest, new ParameterizedTypeReference<Response<String>>() {});
        Utils.processResponseEntity(response, "Error in adding relation");
    }

    @Override
    public ResponseEntity<Response<List<Map<String, Object>>>> getUserRelations(Long userId, String status) {
        String endPoint = UserServiceEndpoint.GET_RELATIONS.getEndpointUrl();
        Map<String, Object> queryParam = new HashMap<>();
        queryParam.put(UserServiceField.PERSON_ID.getFieldName(), userId);
        queryParam.put(UserServiceField.STATUS.getFieldName(), status);
        return getExchange(endPoint, new ParameterizedTypeReference<Response<List<Map<String, Object>>>>() {}, null, queryParam);
    }

    @Override
    public void deleteUserRelation(Map<String, Object> userRelationRequest) {
        String endpoint = UserServiceEndpoint.DELETE_RELATION.getEndpointUrl();
        ResponseEntity<Response<String>> response = deleteExchange(endpoint, new ParameterizedTypeReference<Response<String>>() {}, null, null, userRelationRequest);
        Utils.processResponseEntity(response, "Unable to delete relation");
    }

    @Override
    public void updateUserRelation(Map<String, Object> userRelationRequest) {
        String endpoint = UserServiceEndpoint.UPDATE_RELATION.getEndpointUrl();
        ResponseEntity<Response<String>> response = putExchange(endpoint, userRelationRequest, new ParameterizedTypeReference<Response<String>>() {});
        Utils.processResponseEntity(response, "Unable to update relation");
    }

    @Override
    public ResponseEntity<Response<List<Map<String, Object>>>> getUserRelationsPrimaryPhone(String phoneNumber, Long primaryUserId) {
        String endPoint = UserServiceEndpoint.GET_RELATIONS_FOR_PHONE.getEndpointUrl();
        Map<String, Object> queryParam = new HashMap<>();
        queryParam.put(UserServiceField.PHONE_NUMBER.getFieldName(), phoneNumber);
        queryParam.put(UserServiceField.PRIMARY_USER_ID.getFieldName(), primaryUserId);
        return getExchange(endPoint, new ParameterizedTypeReference<Response<List<Map<String, Object>>>>() {}, null, queryParam);
    }

    @Override
    public ResponseEntity<Response<List<Map<String, Object>>>> getAllVitals() {
        String endPoint = UserServiceEndpoint.GET_VITALS.getEndpointUrl();
        return getExchange(endPoint, new ParameterizedTypeReference<Response<List<Map<String, Object>>>>() {}, null, null);
    }

    @Override
    public ResponseEntity<Response<Map<String, Object>>> getVitalsById(Long vitalId) {
        String endPoint = UserServiceEndpoint.GET_VITALS_BY_ID.getEndpointUrl();
        return getExchange(endPoint, new ParameterizedTypeReference<Response<Map<String, Object>>>() {}, vitalId, null);
    }

    @Override
    public ResponseEntity<Response<List<Map<String, Object>>>> saveVitals(Map<String, Object> vitalBulkRequest) {
        String endPoint = UserServiceEndpoint.ADD_VITALS.getEndpointUrl();
        return postExchange(endPoint, vitalBulkRequest, new ParameterizedTypeReference<Response<List<Map<String, Object>>>>() {});
    }

    @Override
    public ResponseEntity<Response<Map<String, Object>>> updateVitalsById(Long vitalId, Map<String, Object> vitalBulkRequest) {
        String endPoint = String.format(UserServiceEndpoint.UPDATE_VITALS_BY_ID.getEndpointUrl(), vitalId);
        return putExchange(endPoint, vitalBulkRequest, new ParameterizedTypeReference<Response<Map<String, Object>>>() {});
    }

    @Override
    public ResponseEntity<Response<String>> deleteVitalsById(Long vitalId) {
        String endPoint = UserServiceEndpoint.DELETE_VITALS_BY_ID.getEndpointUrl();
        return deleteExchange(endPoint, new ParameterizedTypeReference<Response<String>>() {}, vitalId, null, null);
    }

    @Override
    public ResponseEntity<Response<List<Map<String, Object>>>> getVitalsGoalByUserId(Long userId) {
        String endPoint = String.format(UserServiceEndpoint.GET_USER_VITALS_GOAL.getEndpointUrl(), userId);
        return getExchange(endPoint, new ParameterizedTypeReference<Response<List<Map<String, Object>>>>() {}, null, null);
    }

    @Override
    public ResponseEntity<Response<String>> saveUserVitalsGoal(Long userId, Map<String, Object> vitalGoalRequest) {
        String endPoint = String.format(UserServiceEndpoint.ADD_USER_VITALS_GOAL.getEndpointUrl(), userId);
        return postExchange(endPoint, vitalGoalRequest, new ParameterizedTypeReference<Response<String>>() {});
    }

    @Override
    public ResponseEntity<Response<String>> updateUserVitalsGoal(Long userId, Map<String, Object> vitalGoalRequest) {
        String endPoint = String.format(UserServiceEndpoint.UPDATE_USER_VITALS_GOAL.getEndpointUrl(), userId);
        return putExchange(endPoint, vitalGoalRequest, new ParameterizedTypeReference<Response<String>>() {});
    }

    @Override
    public ResponseEntity<Response<String>> deleteUserVitalsGoal(Long userId) {
        String endPoint = String.format(UserServiceEndpoint.DELETE_USER_VITALS_GOAL.getEndpointUrl(), userId);
        return deleteExchange(endPoint, new ParameterizedTypeReference<Response<String>>() {}, null, null, null);
    }

    private ResponseEntity<Response<String>> updateUserResponse(Map<String, Object> updateUserMap) {
        String endPoint = UserServiceEndpoint.UPDATE_USER.getEndpointUrl();
        return putExchange(endPoint, updateUserMap, new ParameterizedTypeReference<Response<String>>() {});
    }

    @Override
    public ResponseEntity<Response<Map<String, Object>>> getUserVitalsDetails(Map<String, Object> vitalDetailsRequest) {
        String endPoint = UserServiceEndpoint.GET_USER_VITALS_DETAILS.getEndpointUrl();
        return postExchange(endPoint, vitalDetailsRequest, new ParameterizedTypeReference<Response<Map<String, Object>>>() {});
    }

    @Override
    public ResponseEntity<Response<String>> saveUserVitalsDetails(Long userId, Map<String, Object> addVitalDetailsRequest) {
        String endPoint = String.format(UserServiceEndpoint.ADD_USER_VITALS_DETAILS.getEndpointUrl(), userId);
        return postExchange(endPoint, addVitalDetailsRequest, new ParameterizedTypeReference<Response<String>>() {});
    }

    @Override 
    public void deleteUser(Map<String, Object> deleteUserMap, Long personId) {
        if (!deleteUserMap.isEmpty()) {
            deleteUserMap.put(UserServiceField.PERSON_ID.getFieldName(), personId);
            processEvent(deleteUserMap, Event.DELETE_USER);
        }
    }
}
