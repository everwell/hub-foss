package com.everwell.transition.service;

import com.everwell.transition.model.db.Appointment;
import com.everwell.transition.model.dto.EpisodeDto;
import com.everwell.transition.model.dto.appointment.AppointmentData;
import com.everwell.transition.model.dto.appointment.AppointmentDetails;
import com.everwell.transition.model.request.appointment.AppointmentListRequest;
import com.everwell.transition.model.request.appointment.DeleteAppointmentRequest;
import com.everwell.transition.model.request.appointment.FollowUpRequest;
import com.everwell.transition.model.request.appointment.RescheduleRequest;
import com.everwell.transition.model.response.appointment.GetAppointmentResponse;

import java.util.List;

public interface AppointmentService {

    AppointmentDetails createAppointmentDetail(Appointment appointment, EpisodeDto episode);

    AppointmentData addAppointment(AppointmentData appointment);

    GetAppointmentResponse getAppointmentResponse(Integer appointmentId);

    List<AppointmentDetails> getAppointments(AppointmentListRequest appointmentListRequest);

    Appointment confirmAppointment(Integer appointmentId);

    String endAppointment(Integer appointmentId);

    Appointment  createFollowUpAppointment(FollowUpRequest followUpRequest);

    Appointment  rescheduleAppointment(RescheduleRequest rescheduleRequest);

    void deleteAppointment(DeleteAppointmentRequest deleteRequest);
}
