package com.everwell.transition.service;

import java.util.Map;

public interface LocaliseService {
    Map<String, String> getTranslations(String language, String region);
}
