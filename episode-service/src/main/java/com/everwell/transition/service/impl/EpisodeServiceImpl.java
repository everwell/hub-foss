package com.everwell.transition.service.impl;

import com.everwell.transition.constants.*;
import com.azure.storage.blob.BlobClient;
import com.azure.storage.blob.BlobContainerClient;
import com.azure.storage.blob.BlobContainerClientBuilder;
import com.everwell.transition.constants.Constants;
import com.everwell.transition.constants.EpisodeLogConstants;
import com.everwell.transition.constants.FieldConstants;
import com.everwell.transition.constants.episode.EpisodeDuplicateConstants;
import com.everwell.transition.controller.EpisodeController;
import com.everwell.transition.elasticsearch.ElasticConstants;
import com.everwell.transition.elasticsearch.indices.EpisodeIndex;
import com.everwell.transition.elasticsearch.service.ESEpisodeService;
import com.everwell.transition.constants.NotificationConstants;
import com.everwell.transition.elasticsearch.ElasticConstants;
import com.everwell.transition.constants.IAMConstants;
import com.everwell.transition.constants.SSOConstants;
import com.everwell.transition.constants.UserServiceConstants;
import com.everwell.transition.elasticsearch.service.Impl.ReportFiltersToEpisodeSearchFields;
import com.everwell.transition.elasticsearch.service.Impl.TaskListFiltersToEpisodeSearchFields;
import com.everwell.transition.elasticsearch.service.Impl.UppFiltersToEpisodeSearchFields;
import com.everwell.transition.enums.RangeFilterType;
import com.everwell.transition.enums.ScheduleTimeEnum;
import com.everwell.transition.enums.SupportedRelationType;
import com.everwell.transition.enums.episode.DeduplicationScheme;
import com.everwell.transition.enums.episode.EpisodeField;
import com.everwell.transition.enums.episode.EpisodeValidation;
import com.everwell.transition.enums.episodeLog.EpisodeLogAction;
import com.everwell.transition.enums.episodeLog.EpisodeLogCategory;
import com.everwell.transition.enums.episodeLog.EpisodeLogComments;
import com.everwell.transition.enums.eventStreamingEnums.EventAction;
import com.everwell.transition.enums.eventStreamingEnums.EventCategory;
import com.everwell.transition.enums.user.UserServiceField;
import com.everwell.transition.exceptions.InternalServerErrorException;
import com.everwell.transition.exceptions.NotFoundException;
import com.everwell.transition.exceptions.ValidationException;
import com.everwell.transition.model.db.*;
import com.everwell.transition.model.dto.*;
import com.everwell.transition.model.dto.notification.*;
import com.everwell.transition.model.dto.adherence.DosageCounts;
import com.everwell.transition.model.dto.adherence.DosageCountsDetailed;
import com.everwell.transition.model.dto.episode.EpisodeDetailsDto;
import com.everwell.transition.model.request.EpisodeDocumentSearchRequest;
import com.everwell.transition.model.dto.episode.EpisodeTransitionDetails;
import com.everwell.transition.model.request.dispensation.ProductSearchRequest;
import com.everwell.transition.model.request.episodelogs.EpisodeLogDataRequest;
import com.everwell.transition.model.dto.dtoInterface.EpisodeFieldIdValueDtoInterface;
import com.everwell.transition.model.request.dispensation.RefillDatesRequest;
import com.everwell.transition.model.request.AddEpisodeDocumentRequest;
import com.everwell.transition.model.request.episode.DeleteRequest;
import com.everwell.transition.model.request.episodesearch.EpisodeSearchRequest;
import com.everwell.transition.model.response.AdherenceResponse;
import com.everwell.transition.model.response.EpisodeTagResponse;
import com.everwell.transition.model.response.Response;
import com.everwell.transition.model.dto.notification.PNDtoWithNotificationData;
import com.everwell.transition.model.dto.notification.PushNotificationTemplateDto;
import com.everwell.transition.model.request.adherence.SearchAdherenceRequest;
import com.everwell.transition.model.request.episodetags.SearchTagsRequest;
import com.everwell.transition.model.request.tasklistfilters.EpisodeTasklistRequest;
import com.everwell.transition.model.request.tasklistfilters.TaskListFilterRequest;
import com.everwell.transition.model.response.*;
import com.everwell.transition.model.request.SSO.RegistrationRequest;
import com.everwell.transition.model.request.episode.EpisodeBulkRequest;
import com.everwell.transition.model.request.episode.SearchEpisodesRequest;
import com.everwell.transition.model.request.episode.UpdateRiskRequest;
import com.everwell.transition.model.response.SSO.DeviceIdsMapResponse;
import com.everwell.transition.model.response.dispensation.ProductResponse;
import com.everwell.transition.repositories.*;
import com.everwell.transition.service.*;
import com.everwell.transition.utils.Utils;
import com.fasterxml.jackson.core.type.TypeReference;
import org.elasticsearch.action.bulk.BulkRequest;
import org.elasticsearch.action.update.UpdateRequest;
import org.apache.commons.lang3.math.NumberUtils;
import org.elasticsearch.client.RestHighLevelClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;

import java.time.Duration;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.io.IOException;
import java.time.*;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.stream.Collectors;

import static com.everwell.transition.constants.Constants.*;
import static com.everwell.transition.constants.Constants.MODULE_NAMES;
import static com.everwell.transition.constants.DocumentConstants.*;
import static com.everwell.transition.constants.IAMConstants.IAM_MODULE_NAME;
import static com.everwell.transition.constants.UserServiceConstants.PERSON_MODULE_NAME;

@Service
public class EpisodeServiceImpl implements EpisodeService {

    @Autowired
    private EpisodeRepository episodeRepository;

    @Autowired
    private EpisodeStageRepository episodeStageRepository;

    @Autowired
    private EpisodeStageDataRepository episodeStageDataRepository;

    @Autowired
    private EpisodeAssociationRepository episodeAssociationRepository;

    @Autowired
    private EpisodeHierarchyLinkageRepository episodeHierarchyLinkageRepository;

    @Autowired
    private SupportedRelationRepository supportedRelationRepository;

    @Autowired
    private StageRepository stageRepository;

    @Autowired
    private DiseaseTemplateService diseaseTemplateService;

    @Autowired
    private StageTransitionService stageTransitionService;

    @Autowired
    private ApplicationEventPublisher applicationEventPublisher;

    @Autowired
    private EpisodeTagService episodeTagService;

    @Autowired
    private EpisodeLogService episodeLogService;

    @Autowired
    private ESEpisodeService esEpisodeService;

    @Autowired
    private RegistryService registryService;

    @Autowired
    private NikshayService nikshayService;

    @Autowired
    DispensationService dispensationService;

    @Autowired
    IAMService iamService;

    @Autowired
    private AdherenceService adherenceService;

    @Autowired
    private UserService userService;

    @Autowired
    private ClientService clientService;

    @Autowired
    RestHighLevelClient restHighLevelClient;

    @Autowired
    private FieldRepository fieldRepository;

    @Autowired
    private SSOService ssoService;

    @Autowired
    TriggerRepository triggerRepository;

    @Autowired
    private DiagnosticsService diagnosticsService;

    @Autowired
    TaskListFiltersToEpisodeSearchFields taskListFiltersToEpisodeSearchFields;

    @Autowired
    UppFiltersToEpisodeSearchFields uppFiltersToEpisodeSearchFields;

    @Autowired
    EpisodeDocumentRepository episodeDocumentRepository;

    @Autowired
    TreatmentPlanService treatmentPlanService;

    @Autowired
    EpisodeDocumentDetailsRepository episodeDocumentDetailsRepository;

    @Autowired
    TreatmentPlanProductMapRepository treatmentPlanProductMapRepository;

    @Value("${spring.cloud.azure.storage.blob.account-name:everwell}")
    String accountName;

    @Value("${spring.cloud.azure.storage.blob.container-name:prod}")
    String containerName;

    @Value("${spring.cloud.azure.storage.blob.account-key:defaultAccountKey}")
    String accountKey;

    @Value("${systemAdministrator.userId}")
    Long systemUserId;

    @Autowired
    ReportFiltersToEpisodeSearchFields reportFiltersToEpisodeSearchFields;


    @Override
    public Episode getEpisode(Long episodeId, Long clientId) {
        Episode episode = episodeRepository.findByIdAndClientIdAndDeletedIs(episodeId, clientId, false);
        if (null == episode) {
            throw new NotFoundException("no episode found for Id : " + episodeId);
        }
        return episode;
    }

    @Override
    public Episode getEpisodeWithDeleted(Long episodeId, Long clientId) {
        return episodeRepository.findByIdAndClientId(episodeId, clientId);
    }

    private EpisodeStageDataMergedDto getEpisodeStageDataUnionWithDiseaseTemplate(
            List<DiseaseStageFieldMapDto> allDiseaseStageFieldMapDto,
            Map<Long, Set<Long>> episodeIdToDiseaseIdListMap,
            List<EpisodeStage> episodeStages,
            Map<Long, Map<Long, EpisodeStageData>> episodeStageIdToFieldIdEpisodeStageDataMap) {
        Map<Long, List<EpisodeStage>> stageIdToEpisodeStageIdEntityListMap = new HashMap<>();
        Map<Long, Integer> episodeStageIdOrderMap = new HashMap<>();
        int i = 0;
        for (EpisodeStage episodeStage : episodeStages) {
            stageIdToEpisodeStageIdEntityListMap.putIfAbsent(episodeStage.getStageId(), new ArrayList<>());
            stageIdToEpisodeStageIdEntityListMap.get(episodeStage.getStageId()).add(episodeStage);
            episodeStageIdOrderMap.put(episodeStage.getId(), ++i);
        }
        Map<Long, Map<String, String>> episodeStageIdDataMap = new HashMap<>();
        Map<Long, Map<String, FieldDataDto>> episodeIdToFieldKeyEntityMap = new HashMap<>();
        for (DiseaseStageFieldMapDto diseaseStageFieldMapDto : allDiseaseStageFieldMapDto) {
            List<EpisodeStage> episodeStagesForStage = stageIdToEpisodeStageIdEntityListMap.get(diseaseStageFieldMapDto.getStageId());
            for (EpisodeStage episodeStage : episodeStagesForStage) {
                if (episodeIdToDiseaseIdListMap.get(episodeStage.getEpisodeId()).contains(diseaseStageFieldMapDto.getDiseaseId())) {
                    FieldDataDto fieldDataDto = new FieldDataDto(diseaseStageFieldMapDto.getKey(),
                            diseaseStageFieldMapDto.isRequired() ? diseaseStageFieldMapDto.getDefaultValue() : null, true, episodeStage.getId(), diseaseStageFieldMapDto.getModule());
                    Map<Long, EpisodeStageData> stageKeyIdToEntityMap = episodeStageIdToFieldIdEpisodeStageDataMap.get(episodeStage.getId());
                    EpisodeStageData episodeStageData = null != stageKeyIdToEntityMap ? stageKeyIdToEntityMap.get(diseaseStageFieldMapDto.getFieldId()) : null;
                    if (null != episodeStageData) {
                        fieldDataDto.setIsDefault(false);
                        fieldDataDto.setValue(episodeStageData.getValue());
                    }
                    episodeStageIdDataMap.putIfAbsent(episodeStage.getId(), new HashMap<>());
                    episodeStageIdDataMap.get(episodeStage.getId()).put(diseaseStageFieldMapDto.getKey(), fieldDataDto.getValue());
                    episodeIdToFieldKeyEntityMap.putIfAbsent(episodeStage.getEpisodeId(), new HashMap<>());
                    episodeIdToFieldKeyEntityMap.get(episodeStage.getEpisodeId()).putIfAbsent(diseaseStageFieldMapDto.getKey(), fieldDataDto);
                    Integer currentFieldDataOrder = episodeStageIdOrderMap.get(fieldDataDto.getEpisodeStageId());
                    Integer existingFieldDataOrder = episodeStageIdOrderMap.get(episodeIdToFieldKeyEntityMap.get(episodeStage.getEpisodeId()).get(diseaseStageFieldMapDto.getKey()).getEpisodeStageId());
                    if (currentFieldDataOrder >= existingFieldDataOrder && (!fieldDataDto.getIsDefault()
                            || episodeIdToFieldKeyEntityMap.get(episodeStage.getEpisodeId()).get(diseaseStageFieldMapDto.getKey()).getIsDefault())) {
                        episodeIdToFieldKeyEntityMap.get(episodeStage.getEpisodeId()).put(diseaseStageFieldMapDto.getKey(), fieldDataDto);
                    }
                }
            }
        }
        return new EpisodeStageDataMergedDto(episodeStageIdDataMap, episodeIdToFieldKeyEntityMap);
    }

    private EpisodeStageDataMergedDto getEpisodeStageDataWithMergedMappingsDto(
            List<EpisodeStage> episodeStages,
            Set<Long> diseaseIds,
            Map<Long, Set<Long>> episodeIdToDiseaseIdListMap) {
        List<Long> episodeStageIds = new ArrayList<>();
        List<Long> stageIds = new ArrayList<>();
        for (EpisodeStage episodeStage : episodeStages) {
            episodeStageIds.add(episodeStage.getId());
            stageIds.add(episodeStage.getStageId());
        }
        Map<Long, List<DiseaseStageFieldMapDto>> stageIdStageKeysListMap = diseaseTemplateService
                .getDefinedStageIdKeyMappingsListMap(diseaseIds, stageIds);
        List<DiseaseStageFieldMapDto> allDiseaseStageFieldMapDto = new ArrayList<>();
        for (Map.Entry<Long, List<DiseaseStageFieldMapDto>> entry : stageIdStageKeysListMap.entrySet()) {
            allDiseaseStageFieldMapDto.addAll(entry.getValue());
        }
        List<EpisodeStageData> episodeStageDataList = episodeStageDataRepository
                .findAllByEpisodeStageIdIn(episodeStageIds);

        Map<Long, Map<Long, EpisodeStageData>> episodeStageIdToFieldIdEpisodeStageDataMap = new HashMap<>();
        for (EpisodeStageData episodeStageData : episodeStageDataList) {
            episodeStageIdToFieldIdEpisodeStageDataMap.putIfAbsent(episodeStageData.getEpisodeStageId(), new HashMap<>());
            episodeStageIdToFieldIdEpisodeStageDataMap.get(episodeStageData.getEpisodeStageId()).put(episodeStageData.getFieldId(), episodeStageData);
        }
        return getEpisodeStageDataUnionWithDiseaseTemplate(allDiseaseStageFieldMapDto, episodeIdToDiseaseIdListMap, episodeStages, episodeStageIdToFieldIdEpisodeStageDataMap);
    }

    private EpisodeStageDetails getEpisodeStageDetailsForEpisodes(
            List<Long> episodeIds,
            Set<Long> diseaseIdListUnion,
            Map<Long, Set<Long>> episodeIdToDiseaseIdListMap) {
        List<EpisodeStage> episodeStages = episodeStageRepository.getEpisodeStagesByEpisodeIdInOrderByStartDateAsc(episodeIds);
        List<Long> stageIds = episodeStages.stream().map(EpisodeStage::getStageId).collect(Collectors.toList());
        EpisodeStageDataMergedDto episodeStageDataMergedDto = getEpisodeStageDataWithMergedMappingsDto(episodeStages, diseaseIdListUnion,
                episodeIdToDiseaseIdListMap);
        Map<Long, Map<String, String>> episodeStageIdDataMap = episodeStageDataMergedDto.getEpisodeStageIdToStageDataMappingsListMap();
        Map<Long, Stage> stageIdValueMap = stageRepository.findAllById(stageIds)
                .stream()
                .collect(Collectors.toMap(Stage::getId, s -> s));
        Map<Long, List<EpisodeStageDto>> episodeIdToEpisodeStageDtoListMap = new HashMap<>();
        for (EpisodeStage episodeStage : episodeStages) {
            EpisodeStageDto episodeStageDto = new EpisodeStageDto(episodeStage.getId(), episodeStage.getStageId(),
                    stageIdValueMap.get(episodeStage.getStageId()).getStageName(),
                    stageIdValueMap.get(episodeStage.getStageId()).getStageDisplayName(),
                    episodeStage.getStartDate(), episodeStage.getEndDate(), episodeStageIdDataMap.getOrDefault(episodeStage.getId(), new HashMap<>()));
            episodeIdToEpisodeStageDtoListMap.putIfAbsent(episodeStage.getEpisodeId(), new ArrayList<>());
            episodeIdToEpisodeStageDtoListMap.get(episodeStage.getEpisodeId()).add(episodeStageDto);
        }
        return new EpisodeStageDetails(episodeIdToEpisodeStageDtoListMap, episodeStageDataMergedDto.getEpisodeIdToFieldKeyEntityMap());
    }

    private Map<String, Map<Long, SupportedRelation>> getAllSupportedRelationIdValueMap() {
        List<SupportedRelation> supportedRelationsList = supportedRelationRepository.findAll();
        Map<String, Map<Long, SupportedRelation>> typeToSupportedRelationIdValueMap = new HashMap<>();
        for (SupportedRelation relation : supportedRelationsList) {
            typeToSupportedRelationIdValueMap.putIfAbsent(relation.getType().toString(), new HashMap<>());
            typeToSupportedRelationIdValueMap.get(relation.getType().toString()).put(relation.getId(), relation);
        }
        return typeToSupportedRelationIdValueMap;
    }

    private Map<Long, Map<Long, EpisodeAssociation>> getEpisodeAssociationsByEpisodeId(List<Long> episodeIds) {
        List<EpisodeAssociation> associations = episodeAssociationRepository.getAllByEpisodeIdIn(episodeIds);
        Map<Long, Map<Long, EpisodeAssociation>> episodeIdToAssociationIdValueMap = new HashMap<>();
        for (EpisodeAssociation association : associations) {
            episodeIdToAssociationIdValueMap.putIfAbsent(association.getEpisodeId(), new HashMap<>());
            episodeIdToAssociationIdValueMap.get(association.getEpisodeId()).put(association.getId(), association);
        }
        return episodeIdToAssociationIdValueMap;
    }

    private Map<Long, Map<Long, EpisodeHierarchyLinkage>> getEpisodeHierarchyMappingsByEpisodeId(List<Long> episodeIds) {
        List<EpisodeHierarchyLinkage> hierarchyMappings = episodeHierarchyLinkageRepository.getAllByEpisodeIdIn(episodeIds);
        Map<Long, Map<Long, EpisodeHierarchyLinkage>> episodeIdToHierarchyMappingIdValueMap = new HashMap<>();
        for (EpisodeHierarchyLinkage hierarchyMapping : hierarchyMappings) {
            episodeIdToHierarchyMappingIdValueMap.putIfAbsent(hierarchyMapping.getEpisodeId(), new HashMap<>());
            episodeIdToHierarchyMappingIdValueMap.get(hierarchyMapping.getEpisodeId()).putIfAbsent(hierarchyMapping.getId(), hierarchyMapping);
        }
        return episodeIdToHierarchyMappingIdValueMap;
    }

    private void validateInputForEpisodeCreation(Map<String, Object> requestInput, Long clientId) {
        if (null == requestInput.get(FieldConstants.EPISODE_FIELD_ADDED_BY)) {
            throw new ValidationException("addedBy Id not found");
        }
        validateDiseaseOptions(requestInput, clientId);
        if (StringUtils.isEmpty(requestInput.get(FieldConstants.EPISODE_FIELD_TYPE_OF_EPISODE))) {
            throw new ValidationException("type of episode not found");
        }
        validateDates(requestInput);
    }

    public void validateDates(Map<String, Object> requestInput) {
        if (null != requestInput.get(FieldConstants.EPISODE_FIELD_END_DATE)) {
            LocalDateTime startDate = null != requestInput.get(FieldConstants.EPISODE_FIELD_START_DATE) ? Utils.convertStringToDateNew(String.valueOf(requestInput.get(FieldConstants.EPISODE_FIELD_START_DATE))) : Utils.getCurrentDateNew();
            LocalDateTime endDate = null != requestInput.get(FieldConstants.EPISODE_FIELD_END_DATE) ? Utils.convertStringToDateNew(String.valueOf(requestInput.get(FieldConstants.EPISODE_FIELD_END_DATE))) : null;
            if (null != endDate && startDate.isAfter(endDate)) {
                throw new ValidationException("end date cannot be earlier than start date");
            }
        }
    }

    private void validateDiseaseOptions(Map<String, Object> requestInput, Long clientId) {
        if (null == requestInput.get(FieldConstants.EPISODE_FIELD_DISEASE_ID)) {
            if (StringUtils.isEmpty(requestInput.get(FieldConstants.EPISODE_FIELD_DISEASE_ID_OPTIONS))) {
                throw new ValidationException("disease details not found");
            } else {
                try {
                    List<Long> diseaseOptionIds = Arrays.stream(String.valueOf(requestInput.get(FieldConstants.EPISODE_FIELD_DISEASE_ID_OPTIONS)).split(",")).map(Long::parseLong).collect(Collectors.toList());
                    validateDiseaseClientMapping(diseaseOptionIds, clientId);
                } catch (Exception e) {
                    throw new ValidationException("disease details are invalid");
                }
            }
        } else {
            try {
                Long diseaseId = Long.parseLong(String.valueOf(requestInput.get(FieldConstants.EPISODE_FIELD_DISEASE_ID)));
                validateDiseaseClientMapping(Collections.singletonList(diseaseId), clientId);
            } catch (Exception e) {
                throw new ValidationException("disease details are invalid");
            }
        }
    }

    private void validateDiseaseClientMapping(List<Long> diseaseIds, Long clientId) {
        List<Long> diseaseIdsForClient = diseaseTemplateService.getDiseasesForClient(clientId)
                .getDiseases()
                .stream()
                .map(DiseaseTemplate::getId)
                .collect(Collectors.toList());

        if (!diseaseIdsForClient.containsAll(diseaseIds)) {
            diseaseIds.removeAll(diseaseIdsForClient);
            throw new ValidationException(String.format("Disease IDs - %s can't be accessed by client ID - %s", diseaseIds.toString(), clientId));
        }
    }

    private Episode createEpisode(Long clientId, Map<String, Object> requestInput) {
        Episode episode = new Episode(clientId, Long.parseLong(String.valueOf(requestInput.get(FieldConstants.EPISODE_FIELD_PERSON_ID))), String.valueOf(requestInput.get(FieldConstants.EPISODE_FIELD_DISEASE_ID_OPTIONS)), String.valueOf(requestInput.get(FieldConstants.EPISODE_FIELD_TYPE_OF_EPISODE)),
                null == requestInput.get(FieldConstants.EPISODE_FIELD_START_DATE) ? null : Utils.convertStringToDateNew(String.valueOf(requestInput.get(FieldConstants.EPISODE_FIELD_START_DATE))),
                null == requestInput.get(FieldConstants.EPISODE_FIELD_END_DATE) ? null : Utils.convertStringToDateNew(String.valueOf(requestInput.get(FieldConstants.EPISODE_FIELD_END_DATE))),
                Long.parseLong(String.valueOf(requestInput.get(FieldConstants.EPISODE_FIELD_ADDED_BY))));
        if (null != requestInput.get(FieldConstants.EPISODE_FIELD_DISEASE_ID)) {
            episode.setDiseaseId(Long.parseLong(String.valueOf(requestInput.get(FieldConstants.EPISODE_FIELD_DISEASE_ID))));
        }
        if (null != requestInput.get(FieldConstants.EPISODE_FIELD_RISK_STATUS)) {
            episode.setRiskStatus(String.valueOf(requestInput.get(FieldConstants.EPISODE_FIELD_RISK_STATUS)));
        }
        episode = episodeRepository.save(episode);
        return episode;
    }

    private Episode updateEpisode(Long clientId, Map<String, Object> requestInput, Episode episode) {
        if (null != requestInput.get(FieldConstants.EPISODE_FIELD_PERSON_ID)) {
            episode.setPersonId(Long.parseLong(String.valueOf(requestInput.get(FieldConstants.EPISODE_FIELD_PERSON_ID))));
        }
        if (null != requestInput.get(FieldConstants.EPISODE_FIELD_DISEASE_ID)
                || !StringUtils.isEmpty(requestInput.get(FieldConstants.EPISODE_FIELD_DISEASE_ID_OPTIONS))) {
            validateDiseaseOptions(requestInput, clientId);
        }
        if (null != requestInput.get(FieldConstants.EPISODE_FIELD_START_DATE)) {
            episode.setStartDate(Utils.convertStringToDateNew(String.valueOf(requestInput.get(FieldConstants.EPISODE_FIELD_START_DATE))));
        }
        if (null != requestInput.get(FieldConstants.EPISODE_FIELD_END_DATE)) {
            episode.setEndDate(Utils.convertStringToDateNew(String.valueOf(requestInput.get(FieldConstants.EPISODE_FIELD_END_DATE))));
        }
        if (null != episode.getStartDate() && null != episode.getEndDate() && episode.getStartDate().isAfter(episode.getEndDate())) {
            throw new ValidationException("end date cannot be earlier than start date");
        }
        if (null != requestInput.get(FieldConstants.EPISODE_FIELD_RISK_STATUS)) {
            episode.setRiskStatus(String.valueOf(requestInput.get(FieldConstants.EPISODE_FIELD_RISK_STATUS)));
        }
        if (null != requestInput.get(FieldConstants.EPISODE_FIELD_DISEASE_ID)) {
            episode.setDiseaseId(Long.parseLong(String.valueOf(requestInput.get(FieldConstants.EPISODE_FIELD_DISEASE_ID))));
        }
        if (null != requestInput.get(FieldConstants.EPISODE_FIELD_DISEASE_ID_OPTIONS)) {
            episode.setDiseaseIdOptions(String.valueOf(requestInput.get(FieldConstants.EPISODE_FIELD_DISEASE_ID_OPTIONS)));
        }
        if (null != requestInput.get(FieldConstants.EPISODE_FIELD_DELETED)) {
            episode.setDeleted(true);
        }
        episode.setLastActivityDate(Utils.getCurrentDateNew());
        episode = episodeRepository.save(episode);
        return episode;
    }

    private EpisodeStage createEpisodeStage(Episode episode, Stage stage) {
        EpisodeStage episodeStage = new EpisodeStage(episode.getId(), stage.getId());
        episodeStage = episodeStageRepository.save(episodeStage);
        episode.setCurrentStageId(episodeStage.getStageId());
        return episodeStage;
    }

    public void processEpisodeAssociationsForEpisodeIdAndRequestInput(Long episodeId, Map<String, Object> requestInput) {
        Map<Long, SupportedRelation> supportedAssociationIdAssociationMap = getAllSupportedRelationIdValueMap().get(SupportedRelationType.ASSOCIATION.toString());
        supportedAssociationIdAssociationMap = null == supportedAssociationIdAssociationMap ? new HashMap<>() : supportedAssociationIdAssociationMap;
        Map<Long, EpisodeAssociation> episodeAssociationIdValueMap = getEpisodeAssociationsByEpisodeId(Collections.singletonList(episodeId)).get(episodeId);
        if (null == episodeAssociationIdValueMap) {
            episodeAssociationIdValueMap = new HashMap<>();
        }
        Map<Long, Long> associationIdToEpisodeAssociationIdMap = episodeAssociationIdValueMap.entrySet().stream()
                .collect(Collectors.toMap(entry -> entry.getValue().getAssociationId(), Map.Entry::getKey));
        List<EpisodeAssociation> associations = new ArrayList<>();
        for (Map.Entry<Long, SupportedRelation> supportedAssociation : supportedAssociationIdAssociationMap.entrySet()) {
            EpisodeAssociation association;
            if (requestInput.containsKey(supportedAssociation.getValue().getName())) {
                Object inputValueForAssociation = requestInput.get(supportedAssociation.getValue().getName());
                association = episodeAssociationIdValueMap.get(associationIdToEpisodeAssociationIdMap.get(supportedAssociation.getKey()));
                String stringValueOfInput = null != inputValueForAssociation ? String.valueOf(inputValueForAssociation).trim() : null;
                if (null == association) {
                    association = new EpisodeAssociation(episodeId, supportedAssociation.getKey(), stringValueOfInput);
                } else {
                    association.setValue(stringValueOfInput);
                }
                associations.add(association);
            }
        }
        episodeAssociationRepository.saveAll(associations);
    }

    private boolean processEpisodeHierarchyMappingsForEpisodeIdAndRequestInput(Long episodeId, Map<String, Object> requestInput) {
        Map<Long, SupportedRelation> supportedRelationIdRelationMap = getAllSupportedRelationIdValueMap().get(SupportedRelationType.HIERARCHY.toString());
        supportedRelationIdRelationMap = null == supportedRelationIdRelationMap ? new HashMap<>() : supportedRelationIdRelationMap;
        Map<Long, EpisodeHierarchyLinkage> episodeHierarchyLinkIdValueMap = getEpisodeHierarchyMappingsByEpisodeId(Collections.singletonList(episodeId)).get(episodeId);
        if (null == episodeHierarchyLinkIdValueMap) {
            episodeHierarchyLinkIdValueMap = new HashMap<>();
        }
        Map<Long, Long> relationIdToEpisodeHierarchyMappingIdMap = episodeHierarchyLinkIdValueMap.entrySet().stream()
                .collect(Collectors.toMap(entry -> entry.getValue().getRelationId(), Map.Entry::getKey));
        List<EpisodeHierarchyLinkage> hierarchyMappings = new ArrayList<>();
        for (Map.Entry<Long, SupportedRelation> supportedRelation : supportedRelationIdRelationMap.entrySet()) {
            EpisodeHierarchyLinkage hierarchyMapping;
            if (requestInput.containsKey(supportedRelation.getValue().getName())) {
                Object inputValueForHierarchyMapping = requestInput.get(supportedRelation.getValue().getName());
                Long hierarchyMappingValue = null != inputValueForHierarchyMapping ? Long.parseLong(String.valueOf(inputValueForHierarchyMapping).trim()) : null;
                hierarchyMapping = episodeHierarchyLinkIdValueMap.get(relationIdToEpisodeHierarchyMappingIdMap.get(supportedRelation.getKey()));
                if (null == hierarchyMapping) {
                    hierarchyMapping = new EpisodeHierarchyLinkage(episodeId, supportedRelation.getKey(), hierarchyMappingValue);
                } else {
                    hierarchyMapping.setHierarchyId(hierarchyMappingValue);
                }
                hierarchyMappings.add(hierarchyMapping);
            }
        }
        boolean isHierarchyDataUpdated = !hierarchyMappings.isEmpty();
        if (isHierarchyDataUpdated) {
            episodeHierarchyLinkageRepository.saveAll(hierarchyMappings);
        }
        return isHierarchyDataUpdated;
    }

    public void processEpisodeStageDataForEpisodeAndRequestInput(Map<Long, List<DiseaseStageFieldMapDto>> stageIdFieldListMap, EpisodeStage episodeStage, Map<String, Object> requestInput, boolean isNew) {
        List<DiseaseStageFieldMapDto> allKeyMappings = stageIdFieldListMap.get(episodeStage.getStageId());
        Map<Long, EpisodeStageData> episodeStageDataIdRowMap = new HashMap<>();
        if (!isNew) {
            episodeStageDataIdRowMap = episodeStageDataRepository.findAllByEpisodeStageIdIn(Collections.singletonList(episodeStage.getId()))
                .stream()
                .collect(Collectors.toMap(EpisodeStageData::getFieldId, esd -> esd));
        }
        List<EpisodeStageData> stageDataList = new ArrayList<>();
        HashSet<Long> processedFieldIds = new HashSet<>();
        for (DiseaseStageFieldMapDto diseaseStageFieldMapDto : allKeyMappings) {
            EpisodeStageData episodeStageData;
            if (!MODULE_NAMES.contains(diseaseStageFieldMapDto.getModule())){
                if (!processedFieldIds.contains(diseaseStageFieldMapDto.getFieldId()) && requestInput.containsKey(diseaseStageFieldMapDto.getKey())) {
                    Object inputValueForKey = requestInput.get(diseaseStageFieldMapDto.getKey());
                    String stringValueOfInputValue = null != inputValueForKey ? String.valueOf(inputValueForKey).trim() : null;
                    episodeStageData = episodeStageDataIdRowMap.get(diseaseStageFieldMapDto.getFieldId());
                    if (null == episodeStageData) {
                        episodeStageData = new EpisodeStageData(episodeStage.getId(), diseaseStageFieldMapDto.getFieldId(), stringValueOfInputValue);
                    } else {
                        episodeStageData.setValue(stringValueOfInputValue);
                    }
                    stageDataList.add(episodeStageData);
                    processedFieldIds.add(diseaseStageFieldMapDto.getFieldId());
                }
            }
        }
        if (!CollectionUtils.isEmpty(stageDataList)) {
            episodeStageDataRepository.saveAll(stageDataList);
        }
    }

    private Set<Long> getDiseaseIdListForEpisode(Episode episode) {
        Set<Long> diseaseIds = new HashSet<>();
        if (null != episode.getDiseaseId()) {
            diseaseIds.add(episode.getDiseaseId());
        } else {
            diseaseIds.addAll(Arrays.stream(episode.getDiseaseIdOptions().split(",")).map(Long::parseLong).collect(Collectors.toSet()));
        }
        return diseaseIds;
    }

    private void validateFetchDuplicateRequestInput(Map<String, Object> fetchDuplicatesRequest) {
        if (CollectionUtils.isEmpty(fetchDuplicatesRequest)) {
            throw new ValidationException(EpisodeValidation.INVALID_FETCH_DUPLICATE_REQUEST.getMessage());
        }
    }

    @Override
    public List<EpisodeDto> getDuplicateEpisodesForUser(Long clientId, Map<String, Object> fetchDuplicatesRequest) {
        validateFetchDuplicateRequestInput(fetchDuplicatesRequest);
        List<EpisodeDto> episodeDtoList = new ArrayList<>();
        List<Long> duplicatePersonIdList = userService.fetchDuplicates(fetchDuplicatesRequest);
        if (!duplicatePersonIdList.isEmpty()) {
            episodeDtoList = getEpisodesForPersonListFromElastic(duplicatePersonIdList, clientId);
            List<?> stageIdList = (List<?>) fetchDuplicatesRequest.getOrDefault(EpisodeField.STAGE_ID_LIST.getFieldName(), new ArrayList<>());
            if (!CollectionUtils.isEmpty(stageIdList)) {
                Set<Long> stageSet = stageIdList
                        .stream()
                        .map(i -> NumberUtils.toLong(String.valueOf(i)))
                        .collect(Collectors.toSet());
                episodeDtoList = episodeDtoList
                        .stream()
                        .filter(i -> stageSet.contains(i.getCurrentStageId()))
                        .collect(Collectors.toList());
            }
        }
        int startIndex = fetchDuplicatesRequest.containsKey(EpisodeField.START_INDEX.getFieldName())
                ? Integer.parseInt(String.valueOf(fetchDuplicatesRequest.get(EpisodeField.START_INDEX.getFieldName())))
                : 0;
        int endIndex = fetchDuplicatesRequest.containsKey(EpisodeField.NUMBER_OF_EPISODE_RECORDS.getFieldName())
                ? Integer.parseInt(String.valueOf(fetchDuplicatesRequest.get(EpisodeField.NUMBER_OF_EPISODE_RECORDS.getFieldName())))
                : episodeDtoList.size();
        return episodeDtoList
                .stream()
                .skip(startIndex).
                limit(endIndex)
                .collect(Collectors.toList());
    }

    private EpisodeSearchResult getDuplicateEpisodesFromElasticSearch(Map<String, Object> fetchDuplicatesDataMap, Map<String, Object> episodeStageData, Long episodeId) {
        String duplicationScheme = String.valueOf(fetchDuplicatesDataMap.getOrDefault(RuleConstants.DUPLICATION_SCHEME, null));
        List<String> stageIdList =  null != fetchDuplicatesDataMap.get(RuleConstants.NOTIFIED_STAGE_ID_LIST)
                ? Arrays.asList(String.valueOf(fetchDuplicatesDataMap.get(RuleConstants.NOTIFIED_STAGE_ID_LIST)).split(","))
                : new ArrayList<>();
        EpisodeSearchResult episodeSearchResult = new EpisodeSearchResult(0, new ArrayList<>(), null);
        if (DeduplicationScheme.PRIMARYPHONE_GENDER.toString().equals(duplicationScheme)) {
            EpisodeSearchRequest episodeSearchRequest = new EpisodeSearchRequest();
            episodeSearchRequest.getSearch().getMust().put(Constants.CLIENT_ID, clientService.getClientByTokenOrDefault().getId());
            episodeSearchRequest.getSearch().getMust().put(FieldConstants.PERSON_PRIMARY_PHONE, episodeStageData.get(FieldConstants.PERSON_PRIMARY_PHONE));
            episodeSearchRequest.getSearch().getMust().put(FieldConstants.PERSON_GENDER, episodeStageData.get(FieldConstants.PERSON_GENDER));
            episodeSearchRequest.getSearch().getMustNot().put(FieldConstants.EPISODE_FIELD_ID, episodeId);
            if (!CollectionUtils.isEmpty(stageIdList)) {
                episodeSearchRequest.getSearch().getMust().put(FieldConstants.EPISODE_FIELD_CURRENT_STAGE_ID, stageIdList);
            }
            episodeSearchResult = elasticSearch(episodeSearchRequest);
        }
        return episodeSearchResult;
    }

    @Override
    public EpisodeDto getEpisodeDetails(Long id, Long clientId) {
        return getEpisodeDetails(id, clientId, true, true, true, false);
    }

    private List<TabPermissionDto> getTabPermissionsForEpisode(Set<Long> diseaseIdSet, Long currentStageId, Map<String, Object> episodeStageData, boolean tabsPermissionRequired) {
        List<TabPermissionDto> tabPermissionDtoResponseList = new ArrayList<>();
        if (tabsPermissionRequired) {
            tabPermissionDtoResponseList = Optional.ofNullable(diseaseTemplateService.getStageToTabPermissionsListMapForStageIds(diseaseIdSet, Collections.singletonList(currentStageId), episodeStageData)
                            .get(currentStageId))
                            .orElseGet(Collections::emptyList);
        }
        return tabPermissionDtoResponseList;
    }

    private Map<String, Object> getEpisodeStageData(Episode episode, Map<String, FieldDataDto> fieldKeyEntityMap, boolean fetchExternalData) {
        Map<String, Object> episodeStageData = new HashMap<>();
        for (Map.Entry<String, FieldDataDto> entry : fieldKeyEntityMap.entrySet()) {
            Object value = (null != entry.getValue()) ? entry.getValue().getValue() : null;
            episodeStageData.put(entry.getKey(), value);
        }
        if (fetchExternalData) {
            Map<String, Object> serviceResponseMap = new HashMap<>();
            serviceResponseMap.putAll(userService.getUserDetails(episode.getPersonId()));
            if (episodeStageData.containsKey(FieldConstants.ESD_TREATMENT_PLAN_ID)) {
                if (episodeStageData.get(FieldConstants.ESD_TREATMENT_PLAN_ID) != null) {
                    Long treatmentPlanId = Long.valueOf(String.valueOf(episodeStageData.get(FieldConstants.ESD_TREATMENT_PLAN_ID)));
                    List<String> iamEntityIds =
                            treatmentPlanService
                                    .getTreatmentPlanProductMapList(treatmentPlanId)
                                    .stream()
                                    .map(m -> Constants.IAM_ACCESS_MAPPING_ENTITY_PREFIX_MED + "_" + m.getId())
                                    .collect(Collectors.toList());
                    AllAdherenceResponse allAdherenceResponse = adherenceService.getAdherenceBulk(new SearchAdherenceRequest(iamEntityIds, null), true);
                    serviceResponseMap.put(FieldConstants.FIELD_ADHERENCE_STRING, allAdherenceResponse.getMergedAdherence());
                }
            } else {
                serviceResponseMap.putAll(adherenceService.getAdherenceDetails(episode.getId()));
            }
            episodeStageData.putAll(processServiceResponse(serviceResponseMap, fieldKeyEntityMap));
        }
        return episodeStageData;
    }

    @Override
    public EpisodeDto getEpisodeDetails(Long id, Long clientId, boolean stageLogsRequired, boolean tabsPermissionRequired, boolean fetchExternalData, boolean includeDeleted) {
        if (!tabsPermissionRequired && !stageLogsRequired && !fetchExternalData) {
            return esEpisodeService.findByIdAndClientId(id, clientId).convertToDto();
        }
        Episode episode = includeDeleted ? getEpisodeWithDeleted(id, clientId) : getEpisode(id, clientId);
        Stage currentStage = stageRepository.findById(episode.getCurrentStageId()).get();
        Set<Long> diseaseIds = getDiseaseIdListForEpisode(episode);
        Map<Long, Set<Long>> episodeIdToDiseaseIdListMap = new HashMap<>();
        episodeIdToDiseaseIdListMap.put(episode.getId(), diseaseIds);
        EpisodeStageDetails episodeStageDetails = getEpisodeStageDetailsForEpisodes(Collections.singletonList(episode.getId()), diseaseIds, episodeIdToDiseaseIdListMap);
        Map<String, FieldDataDto> fieldKeyEntityMap = episodeStageDetails.getEpisodeIdToFieldKeyEntityMap().get(id);
        Map<String, Object> episodeStageData = getEpisodeStageData(episode, fieldKeyEntityMap, fetchExternalData);
        List<TabPermissionDto> tabPermissions = getTabPermissionsForEpisode(diseaseIds, episode.getCurrentStageId(), episodeStageData, tabsPermissionRequired);
        Map<Long, DiseaseTemplate> diseaseIdTemplateMap = diseaseTemplateService.getDiseaseIdTemplateMap(new ArrayList<>(diseaseIds));
        EpisodeDto episodeDto = getEpisodeDetails(episode, currentStage, diseaseIdTemplateMap.get(episode.getDiseaseId()),
                episodeStageDetails.getEpisodeIdToEpisodeStageDtoListMap().get(id), getEpisodeAssociationsByEpisodeId(Collections.singletonList(id)).get(id),
                getEpisodeHierarchyMappingsByEpisodeId(Collections.singletonList(id)).get(id), tabPermissions, episodeStageData, stageLogsRequired);
        return episodeDto;
    }

    private EpisodeDto getEpisodeDetails(Episode episode, Stage currentStage, DiseaseTemplate diseaseTemplate, List<EpisodeStageDto> episodeStageDtoList,
                                         Map<Long, EpisodeAssociation> episodeAssociationMap, Map<Long, EpisodeHierarchyLinkage> episodeHierarchyMappings,
                                         List<TabPermissionDto> tabPermissions, Map<String, Object> episodeStageData, boolean stageLogsRequired) {
        Map<String, Object> episodeAssociationKeyValueMap = new HashMap<>();
        Map<String, Map<Long, SupportedRelation>> supportedRelationsTypeToIdValueListMap = getAllSupportedRelationIdValueMap();
        if (!CollectionUtils.isEmpty(episodeAssociationMap)) {
            episodeAssociationKeyValueMap = episodeAssociationMap
                    .values()
                    .stream()
                    .collect(Collectors.toMap(ea -> supportedRelationsTypeToIdValueListMap.get(SupportedRelationType.ASSOCIATION.toString())
                            .get(ea.getAssociationId()).getName(), EpisodeAssociation::getValue));
        }
        Map<String, Object> episodeHierarchyKeyValueMap = new HashMap<>();
        if (!CollectionUtils.isEmpty(episodeHierarchyMappings)) {
            episodeHierarchyKeyValueMap = episodeHierarchyMappings
                    .values()
                    .stream()
                    .collect(Collectors.toMap(eal -> supportedRelationsTypeToIdValueListMap.get(SupportedRelationType.HIERARCHY.toString())
                            .get(eal.getRelationId()).getName(), EpisodeHierarchyLinkage::getHierarchyId));
        }
        EpisodeTagResponse tagResponse = episodeTagService.getEpisodeTags(episode.getId());
        return new EpisodeDto(episode, tagResponse.getCurrentTags(), currentStage, diseaseTemplate, stageLogsRequired ? episodeStageDtoList : null, episodeAssociationKeyValueMap, episodeHierarchyKeyValueMap, tabPermissions, episodeStageData);
    }

    private Map<String, Object> processServiceResponse(Map<String, Object> serviceResponse, Map<String, FieldDataDto> fieldDataDtoMap) {
        Map<String, Object> processedServiceResponseMap = new HashMap<>();
        for (Map.Entry<String, Object> entry : serviceResponse.entrySet()) {
            FieldDataDto fieldDataDto = fieldDataDtoMap.get(entry.getKey());
            if (null != fieldDataDto && Constants.MODULE_NAMES.contains(fieldDataDto.getModule())) {
                processedServiceResponseMap.put(entry.getKey(), entry.getValue());
            }
        }
        return processedServiceResponseMap;
    }

    @Override
    public List<EpisodeDto> getEpisodesForPersonListFromElastic(List<Long> personIdList, Long clientId) {
        List<EpisodeDto> episodeDtoList = new ArrayList<>();
        List<EpisodeIndex> episodeIndexList = esEpisodeService.findAllByPersonIdList(personIdList, clientId);
        episodeIndexList.forEach(episodeIndex -> {
            episodeDtoList.add(episodeIndex.convertToDto());
        });
        return episodeDtoList;
    }

    @Override
    public List<EpisodeDto> getEpisodesForPersonList(List<Long> personIdList, Long clientId, boolean includeDetails) {
        List<Episode> episodes = episodeRepository.findAllByPersonIdInAndClientIdAndDeletedFalse(personIdList, clientId);
        List<EpisodeDto> response = new ArrayList<>();
        if (episodes.isEmpty()) {
            throw new NotFoundException("no episodes found for personIds : " + personIdList.toString());
        }
        if (includeDetails) {
            response = getEpisodeDetailsBulk(episodes, false);
        } else {
            List<Long> currentStageIds = episodes.stream().map(Episode::getCurrentStageId).collect(Collectors.toList());
            Map<Long, Stage> currentStageIdToEntityMap = stageRepository.findAllById(currentStageIds)
                    .stream()
                    .collect(Collectors.toMap(Stage::getId, s -> s));
            for (Episode episode : episodes) {
                Stage stage = currentStageIdToEntityMap.get(episode.getCurrentStageId());
                List<String> episodeTags = episodeTagService.getEpisodeTags(episode.getId()).getCurrentTags();
                response.add(new EpisodeDto(episode, episodeTags, stage));
            }
        }
        return response;
    }

    private List<EpisodeDto> getEpisodeDetailsBulk(List<Episode> episodes, boolean tabPermissionsRequired) {
        List<Long> episodeIds = new ArrayList<>();
        Set<Long> currentStageIds = new HashSet<>();
        Set<Long> diseaseIdListUnion = new HashSet<>();
        Map<Long, Set<Long>> episodeIdToDiseaseIdListMap = new HashMap<>();
        for (Episode episode : episodes) {
            episodeIds.add(episode.getId());
            currentStageIds.add(episode.getCurrentStageId());
            Set<Long> diseaseIdList = getDiseaseIdListForEpisode(episode);
            episodeIdToDiseaseIdListMap.put(episode.getId(), diseaseIdList);
            diseaseIdListUnion.addAll(diseaseIdList);
        }
        Map<Long, Stage> currentStageIdToEntityMap = stageRepository.findAllById(currentStageIds)
                .stream()
                .collect(Collectors.toMap(Stage::getId, s -> s));
        EpisodeStageDetails episodeStageDetails = getEpisodeStageDetailsForEpisodes(episodeIds, diseaseIdListUnion, episodeIdToDiseaseIdListMap);
        Map<Long, Map<Long, EpisodeAssociation>> episodeAssociations = getEpisodeAssociationsByEpisodeId(episodeIds);
        Map<Long, Map<Long, EpisodeHierarchyLinkage>> episodeHierarchyMappings = getEpisodeHierarchyMappingsByEpisodeId(episodeIds);
        Map<Long, DiseaseTemplate> diseaseTemplateMap = diseaseTemplateService.getDiseaseIdTemplateMap(new ArrayList<>(diseaseIdListUnion));
        Map<Long, List<TabPermissionDto>> tabPermissions = new HashMap<>();
        if (tabPermissionsRequired) {
            tabPermissions = diseaseTemplateService.getStageToTabPermissionsListMapForStageIds(diseaseIdListUnion, new ArrayList<>(currentStageIds), new HashMap<>());
        }
        List<EpisodeDto> episodeList = new ArrayList<>();
        for (Episode episode : episodes) {
            Map<String, Object> episodeStageData = getEpisodeStageData(episode, episodeStageDetails.getEpisodeIdToFieldKeyEntityMap().get(episode.getId()), true);
            episodeList.add(getEpisodeDetails(episode, currentStageIdToEntityMap.get(episode.getCurrentStageId()), diseaseTemplateMap.get(episode.getDiseaseId()),
                    episodeStageDetails.getEpisodeIdToEpisodeStageDtoListMap().get(episode.getId()), episodeAssociations.get(episode.getId()),
                    episodeHierarchyMappings.get(episode.getId()), tabPermissions.getOrDefault(episode.getCurrentStageId(), new ArrayList<>()), episodeStageData, true));
        }
        return episodeList;
    }

    private Stage getStageForCreateEpisode(Object stageValue) {
        String toStage = String.valueOf(stageValue);
        Stage stage = stageRepository.findByStageName(toStage);
        if (null == stage) {
            throw new ValidationException(EpisodeValidation.STAGE_DOES_NOT_EXIST.getMessage());
        }
        return stage;
    }

    private boolean getSSORegistrationForCreateEpisode(Object ssoRegistrationValue) {
        boolean ssoRegistration = Boolean.valueOf(String.valueOf(ssoRegistrationValue));
        return ssoRegistration;
    }

    private Set<Long> getDiseaseIdListForCreateEpisode(Map<String, Object> requestInput) {
        Set<Long> diseaseIds = new HashSet<>();
        long diseaseId = NumberUtils.toLong(String.valueOf(requestInput.get(FieldConstants.EPISODE_FIELD_DISEASE_ID)));
        String diseaseIdOptions = String.valueOf(requestInput.get(FieldConstants.EPISODE_FIELD_DISEASE_ID_OPTIONS));
        if (diseaseId != 0L) {
            diseaseIds.add(diseaseId);
        } else {
            diseaseIds.addAll(Arrays.stream(diseaseIdOptions.split(",")).map(Long::parseLong).collect(Collectors.toSet()));
        }
        return diseaseIds;
    }

    private Episode validateInputForEpisodeUpdate(Long clientId, Map<String, Object> requestInput) {
        Object episodeId = requestInput.get(FieldConstants.EPISODE_FIELD_EPISODE_ID);
        if (null == episodeId) {
            throw new ValidationException(EpisodeValidation.EPISODE_ID_NOT_FOUND.getMessage());
        }
        return getEpisode(Long.parseLong(String.valueOf(episodeId)), clientId);
    }

    private Map<String, Map<String, Object>> processDiseaseStageFieldData(Map<String, Object> requestInput, List<DiseaseStageFieldMapDto> stageIdFieldDtoList) {
        Map<String, Map<String, Object>> processedRequestInputMap = new HashMap<>();
        MODULE_NAMES.forEach(moduleName-> { processedRequestInputMap.putIfAbsent(moduleName, new HashMap<>()); });
        stageIdFieldDtoList.forEach(diseaseStageFieldMapDto -> {
            String fieldKey = diseaseStageFieldMapDto.getKey();
            if (requestInput.containsKey(fieldKey)) {
                String moduleName = diseaseStageFieldMapDto.getModule();
                Map<String, Object> moduleDataMap = processedRequestInputMap.get(moduleName);
                if (null != moduleDataMap) {
                    Object fieldValue = requestInput.get(fieldKey);
                    moduleDataMap.put(fieldKey, fieldValue);
                }
            }
        });
        return processedRequestInputMap;
    }

    private void createUser(Map<String, Object> requestInput, Map<String, Map<String, Object>> processedRequestInput) {
        if (!requestInput.containsKey(FieldConstants.EPISODE_FIELD_PERSON_ID)) {
            Long personId = userService.createUser(processedRequestInput.get(PERSON_MODULE_NAME));
            requestInput.put(FieldConstants.EPISODE_FIELD_PERSON_ID, personId);
        }
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public Episode processCreateEpisode(Long clientId, Map<String, Object> requestInput) {
        validateInputForEpisodeCreation(requestInput, clientId);
        Map<String, Object> rulesResult = stageTransitionService.stageTransitionHandler(requestInput, clientId).getField();
        Stage stage = getStageForCreateEpisode(rulesResult.get(FieldConstants.EPISODE_FIELD_TO_STAGE));
        boolean ssoRegistration = getSSORegistrationForCreateEpisode(rulesResult.get(RuleConstants.EPISODE_SSO_REGISTRATION));
        Map<Long, List<DiseaseStageFieldMapDto>> stageIdFieldListMap = diseaseTemplateService
                .getDefinedStageIdKeyMappingsListMap(getDiseaseIdListForCreateEpisode(requestInput), Collections.singletonList(stage.getId()));
        Map<String, Map<String, Object>> processedRequestInput = processDiseaseStageFieldData(requestInput, stageIdFieldListMap.get(stage.getId()));
        createUser(requestInput, processedRequestInput);
        Episode episodeEntity = createEpisode(clientId, requestInput);
        EpisodeStage episodeStage = createEpisodeStage(episodeEntity, stage);
        adherenceService.registerEntity(episodeEntity.getId(), requestInput, processedRequestInput.get(IAM_MODULE_NAME), rulesResult);
        if (Boolean.TRUE.equals(ssoRegistration)) {
            RegistrationRequest registrationRequest = new RegistrationRequest(episodeEntity.getId().toString(), SSOConstants.DEFAULT_PASSWORD);
            Boolean isRegistrationSuccessful = ssoService.registration(registrationRequest);
            if (!isRegistrationSuccessful) {
                throw new InternalServerErrorException("Registration in SSO was unsuccessful");
            }
        }
        processEpisodeAssociationsForEpisodeIdAndRequestInput(episodeEntity.getId(), requestInput);
        processEpisodeHierarchyMappingsForEpisodeIdAndRequestInput(episodeEntity.getId(), requestInput);
        processEpisodeStageDataForEpisodeAndRequestInput(stageIdFieldListMap, episodeStage, requestInput, true);
        writeToElastic(episodeEntity.getId(), clientId, false, true);
        return episodeEntity;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public Episode processUpdateEpisode(Long clientId, Map<String, Object> requestInput) {
        return processUpdateEpisode(clientId, requestInput, false);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public Episode processUpdateEpisode(Long clientId, Map<String, Object> requestInput, boolean isWriteToElastic) {
        Episode episodeEntity = validateInputForEpisodeUpdate(clientId, requestInput);
        EpisodeDto episodeDetails = getEpisodeDetails(episodeEntity.getId(), clientId);
        Map<Long, List<DiseaseStageFieldMapDto>> stageIdFieldListMap = diseaseTemplateService
                .getDefinedStageIdKeyMappingsListMap(getDiseaseIdListForEpisode(episodeEntity), Collections.singletonList(episodeEntity.getCurrentStageId()));
        List<DiseaseStageFieldMapDto> diseaseStageFieldMapDtoList = stageIdFieldListMap.get(episodeEntity.getCurrentStageId());
        EpisodeDetailsDto episodeDetailsDtoBeforeUpdate = new EpisodeDetailsDto(episodeDetails, diseaseStageFieldMapDtoList);
        Map<String, Map<String, Object>> processedRequestInput = processDiseaseStageFieldData(requestInput, diseaseStageFieldMapDtoList);
        userService.updateUser(processedRequestInput.get(PERSON_MODULE_NAME) , episodeEntity.getPersonId());
        episodeEntity = updateEpisode(clientId, requestInput, episodeEntity);
        EpisodeStage episodeStage = episodeStageRepository.getCurrentStageForEpisodeId(episodeEntity.getId());
        processEpisodeAssociationsForEpisodeIdAndRequestInput(episodeEntity.getId(), requestInput);
        boolean isHierarchyDataUpdated = processEpisodeHierarchyMappingsForEpisodeIdAndRequestInput(episodeEntity.getId(), requestInput);
        processEpisodeStageDataForEpisodeAndRequestInput(stageIdFieldListMap, episodeStage, requestInput, false);
        processEpisodeDataWithStageTransitionHandler(episodeEntity, episodeDetailsDtoBeforeUpdate, requestInput, processedRequestInput, clientId);
        if (isWriteToElastic) {
            writeToElastic(episodeEntity.getId(), clientId, true, isHierarchyDataUpdated);
        } else {
            updateElasticDocument(episodeEntity.getId(), clientId, isHierarchyDataUpdated);
        }
        return episodeEntity;
    }
    public void writeToElastic(Long id, Long clientId, boolean isUpdate, boolean isHierarchyDataUpdated) {
        applicationEventPublisher.publishEvent(new EpisodeEsInsertUpdateDto(id, clientId, isUpdate, isHierarchyDataUpdated));
    }

    @Override
    public void processEpisodeDataWithStageTransitionHandler(Episode episode, EpisodeDetailsDto episodeDetailsDtoBeforeUpdate,
                                                             Map<String, Object> requestInput, Map<String, Map<String, Object>> processedRequestInput, Long clientId) {
        EpisodeDto updatedEpisodeDetails = getEpisodeDetails(episode.getId(), episode.getClientId());
        Map<String, Object> episodeData = new HashMap<>();
        episodeData.put(FieldConstants.EPISODE_FIELD_EPISODE_ID, episode.getId());
        episodeData.putAll(updatedEpisodeDetails.getStageData());
        episodeData.putAll(Utils.objectToMap(updatedEpisodeDetails));
        episodeData.putAll(requestInput);
        Map<String, Object> stageTransitionResult = stageTransitionService.stageTransitionHandler(episodeData, clientId).getField();
        adherenceService.updateEntity(new EpisodeTransitionDetails(episodeDetailsDtoBeforeUpdate.getEpisodeDto(), updatedEpisodeDetails), requestInput, processedRequestInput.get(IAM_MODULE_NAME), stageTransitionResult);
        if (!CollectionUtils.isEmpty(stageTransitionResult)) {
            Map<String, Object> updatedEpisodeStageData = processDuplicateEpisode(episode, episodeDetailsDtoBeforeUpdate, updatedEpisodeDetails, stageTransitionResult, processedRequestInput, clientId);
            stageTransitionResult.putAll(updatedEpisodeStageData);
            processStageTransitionResult(episode, stageTransitionResult);
        }
    }

    private void deleteUserForDuplicateEpisode(Episode episode, Map<String, Object> episodeStageData, Map<String, Object> requestInput, Long clientId) {
        long duplicateOf = NumberUtils.toLong(String.valueOf(episodeStageData.get(EpisodeField.DUPLICATE_OF.getFieldName())));
        if (!episode.isDeleted()) {
            Episode originalEpisode = getEpisode(duplicateOf, clientId);
            userService.deleteUser(requestInput, episode.getPersonId());
            Map<String, Object> updateEpisodeRequest = new HashMap<>();
            updateEpisodeRequest.put(FieldConstants.EPISODE_FIELD_PERSON_ID, originalEpisode.getPersonId());
            updateEpisodeRequest.put(FieldConstants.EPISODE_FIELD_DELETED, true);
            updateEpisode(clientId, updateEpisodeRequest, episode);
        }
    }

    private Map<String, Object> processDuplicateEpisode(Episode episode, EpisodeDetailsDto episodeDetailsDtoBeforeUpdate, EpisodeDto updatedEpisodeDetails,
                                                       Map<String, Object> stageTransitionResult, Map<String, Map<String, Object>> processedRequestInput, Long clientId) {
        Map<String, Object> updatedEpisodeStageData = new HashMap<>();
        Map<String, Object> episodeStageData = updatedEpisodeDetails.getStageData();
        String duplicateStatus = null != episodeStageData.get(EpisodeField.DUPLICATE_STATUS.getFieldName()) ? String.valueOf(episodeStageData.get(EpisodeField.DUPLICATE_STATUS.getFieldName())) : null;
        boolean duplicateAndDelete = Boolean.parseBoolean(String.valueOf(stageTransitionResult.getOrDefault(RuleConstants.CHECK_DUPLICATES_AND_DELETE, false)));
        boolean checkDuplicateAndUpdate = Boolean.parseBoolean(String.valueOf(stageTransitionResult.getOrDefault(RuleConstants.CHECK_DUPLICATES_AND_UPDATE, false)));
        if (duplicateAndDelete || EpisodeDuplicateConstants.USER_CONFIRMED_DUPLICATE.equals(duplicateStatus)) {
            deleteUserForDuplicateEpisode(episode, episodeStageData, processedRequestInput.get(PERSON_MODULE_NAME), clientId);
            if (EpisodeDuplicateConstants.UNIQUE_STATUS_WITH_NULL.contains(duplicateStatus)) {
                EpisodeSearchResult episodeSearchResult = getDuplicateEpisodesFromElasticSearch(stageTransitionResult, episodeStageData, episode.getId());
                if (episodeSearchResult.getEpisodeIndexList().size() == 1) {
                    EpisodeDto duplicateEpisodeDto = episodeSearchResult.getEpisodeIndexList().get(0).convertToDto();
                    Map<String, Object> duplicateEpisodeStageData = duplicateEpisodeDto.getStageData();
                    String duplicateEpisodeDuplicateStatus = null != duplicateEpisodeStageData.get(EpisodeField.DUPLICATE_STATUS.getFieldName())
                            ? String.valueOf(duplicateEpisodeStageData.get(EpisodeField.DUPLICATE_STATUS.getFieldName()))
                            : null;
                    updatedEpisodeStageData = EpisodeDuplicateConstants.VALID_STATUSES.contains(duplicateEpisodeDuplicateStatus) ?
                            checkForDuplicateAndUpdateStatus(duplicateEpisodeDto, stageTransitionResult, clientId) : new HashMap<>();
                }
            }
        } else if (checkDuplicateAndUpdate) {
            updatedEpisodeStageData = checkForDuplicateAndUpdateStatus(episodeDetailsDtoBeforeUpdate.getEpisodeDto(), stageTransitionResult, clientId);
        } else {
            updatedEpisodeStageData = new HashMap<>();
        }
        return updatedEpisodeStageData;
    }

    private Map<String, Object> checkForDuplicateAndUpdateStatus(EpisodeDto episodeDto, Map<String, Object> stageTransitionResult,  Long clientId)
    {
        Map<String, Object> updatedEpisodeStageData = new HashMap<>();
        String oldDuplicateStatus = null != episodeDto.getStageData().get(EpisodeField.DUPLICATE_STATUS.getFieldName()) ? String.valueOf(episodeDto.getStageData().get(EpisodeField.DUPLICATE_STATUS.getFieldName())) : null;
        String newDuplicateStatus = EpisodeDuplicateConstants.SYSTEM_IDENTIFIED_UNIQUE;
        if (!EpisodeDuplicateConstants.ALLOWED_UNIQUE_STATUSES.contains(oldDuplicateStatus)) {
            List<EpisodeDto> allLinkedEpisodeList = getEpisodesForPersonList(Collections.singletonList(episodeDto.getPersonId()), clientId, false);
            if (allLinkedEpisodeList.size() == 1) {
                EpisodeSearchResult episodeSearchResult = getDuplicateEpisodesFromElasticSearch(stageTransitionResult, episodeDto.getStageData(), episodeDto.getId());
                if (!episodeSearchResult.getEpisodeIndexList().isEmpty()) {
                    newDuplicateStatus = EpisodeDuplicateConstants.SYSTEM_IDENTIFIED_DUPLICATE;
                    updatedEpisodeStageData.put(EpisodeField.IS_DUPLICATE.getFieldName(), true);
                }
            }
            if (!newDuplicateStatus.equals(oldDuplicateStatus)) {
                updatedEpisodeStageData.put(EpisodeField.DUPLICATE_STATUS.getFieldName(), newDuplicateStatus);
                EpisodeLogDataRequest episodeLogDataRequest = new EpisodeLogDataRequest(episodeDto.getId(), EpisodeLogCategory.DUPLICATE_STATUS_UPDATED.getCategoryName(),
                        systemUserId, EpisodeLogAction.DUPLICATE_STATUS_UPDATED_By_SYSTEM.getActionName(), String.format(EpisodeLogComments.DUPLICATE_STATUS_CHANGED.getText(),
                        oldDuplicateStatus, newDuplicateStatus));
                episodeLogService.save(Collections.singletonList(episodeLogDataRequest));
            }
        }
        return updatedEpisodeStageData;
    }

    @Override
    public void processStageTransitionResult(Episode episode, Map<String, Object> stageTransitionResult) {
        String toStage = null != stageTransitionResult ? String.valueOf(stageTransitionResult.get("ToStage")) : null;
        Stage stage = stageRepository.findByStageName(toStage);
        EpisodeStage currentStage = episodeStageRepository.getCurrentStageForEpisodeId(episode.getId());
        if (null != stage && !(currentStage.getStageId().equals(stage.getId()))) {
            currentStage.setEndDate(Utils.getCurrentDateNew());
            episodeStageRepository.save(currentStage);
            EpisodeStage episodeStageNew = new EpisodeStage(episode.getId(), stage.getId());
            episodeStageRepository.save(episodeStageNew);
            episode.setCurrentStageId(episodeStageNew.getStageId());
            episodeRepository.save(episode);
            if (stageTransitionResult != null && stageTransitionResult.size() > 1) {
                Map<Long, List<DiseaseStageFieldMapDto>> stageIdFieldListMap = diseaseTemplateService
                        .getDefinedStageIdKeyMappingsListMap(getDiseaseIdListForEpisode(episode), Collections.singletonList(episode.getCurrentStageId()));
                processEpisodeAssociationsForEpisodeIdAndRequestInput(episode.getId(), stageTransitionResult);
                processEpisodeHierarchyMappingsForEpisodeIdAndRequestInput(episode.getId(), stageTransitionResult);
                processEpisodeStageDataForEpisodeAndRequestInput(stageIdFieldListMap, episodeStageNew, stageTransitionResult, false);
            }
        }
    }

    @Transactional(rollbackFor = Exception.class)
    public Episode processEpisodeRequestInputForStageTransition(Long clientId, Map<String, Object> requestInput){
        return processEpisodeRequestInputForStageTransition(clientId, requestInput, true);
    }

    @Transactional(rollbackFor = Exception.class)
    public Episode processEpisodeRequestInputForStageTransition(Long clientId, Map<String, Object> requestInput,boolean isWriteToElastic){
        EpisodeDto existingEpisodeDetails = getEpisodeDetails(Long.parseLong(String.valueOf(requestInput.get(FieldConstants.EPISODE_FIELD_EPISODE_ID))), clientId);
        Map<String, Object> stageData = new HashMap<>();
        stageData.putAll(existingEpisodeDetails.getStageData());
        stageData.putAll(requestInput);
        //episode level fields
        stageData.put(FieldConstants.EPISODE_FIELD_TYPE_OF_EPISODE, existingEpisodeDetails.getTypeOfEpisode());
        stageData.put(FieldConstants.EPISODE_FIELD_CURRENT_STAGE_ID, existingEpisodeDetails.getCurrentStageId());
        Map<String, Object> stageTransitionResult = stageTransitionService.stageTransitionHandler(stageData, clientId).getField();
        if(null == stageTransitionResult || !stageTransitionResult.containsKey(FieldConstants.EPISODE_FIELD_TO_STAGE)) {
            throw new ValidationException("No valid stage transition found for the given input");
        }
        return processUpdateEpisode(clientId, requestInput, isWriteToElastic);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public Episode deleteEpisode(Long clientId, DeleteRequest request){
        Long episodeId = request.getEpisodeId();
        Episode episode = getEpisode(episodeId, clientId);
        episode.setDeleted(true);
        episodeRepository.save(episode);
        esEpisodeService.delete(episode.getId().toString(), clientId);

        List<TabPermissionDto> tabPermissionDtoList = getTabPermissionsForEpisode(Collections.singleton(episode.getDiseaseId()), episode.getCurrentStageId(), new HashMap<>(), true);
        boolean hasAdherenceTab = tabPermissionDtoList.stream().anyMatch(t -> t.getTabName().equals(SupportedTabConstants.ADHERENCE));
        if (hasAdherenceTab) {
            iamService.deleteEntity(String.valueOf(episodeId));
        }

        episodeLogService.addEpisodeDeletionLog(request);

        applicationEventPublisher.publishEvent(new EventStreamingAnalyticsDto(EventCategory.EPISODE.getName(),episodeId.toString(), EventAction.EPISODE_DELETE.getName()));
        return episode;
    }

    @Override
    public EpisodeIndex saveToElastic(Long id, Long clientId) {
        return saveToElastic(id, clientId, 1);
    }

    public EpisodeIndex saveToElastic(Long id, Long clientId, int retry) {
        EpisodeDto episodeDto = null;
        try {
            episodeDto = getEpisodeDetails(id, clientId);
            if (episodeDto.getHierarchyMappings() != null) {
                Map<String, Object> hMappings = new HashMap<>(episodeDto.getHierarchyMappings());
                for (Map.Entry<String, Object> map : hMappings.entrySet()) {
                    String key = map.getKey();
                    Object val = map.getValue();
                    if (val != null)
                        episodeDto.getHierarchyMappings().put(key + ElasticConstants.HIERARCHY_MAPPING_ALL_ELASTIC_NOMENCLATURE, registryService.getAllLevelIdsForHierarchy(Long.parseLong(String.valueOf(val)), false));
                }
            }
            Map<String, String> fieldMap = getFieldToTableTypeMap();
            Map<String, Object> stageData = new HashMap<>();
            episodeDto.getStageData().forEach((k, v) -> {
                if (fieldMap.containsKey(k) && v != null) {
                    stageData.put(k, v);
                }
            });
            stageData.put(FieldConstants.PERSON_FIELD_NAME, stageData.get(FieldConstants.PERSON_FIELD_FIRST_NAME) + " " + stageData.getOrDefault(FieldConstants.PERSON_FIELD_LAST_NAME, ""));
            stageData.put(FieldConstants.IS_EPISODE_ENABLED, true);
            episodeDto.setStageData(stageData);
        } catch (NotFoundException e) {
            if (retry == 3) {
                throw e;
            }
            try {
                Thread.sleep(retry * 1000L);
                return saveToElastic(id, clientId, ++retry);
            } catch (InterruptedException ex) {
                throw new InternalServerErrorException("[saveToElastic] Failed retrying at save to elastic!");
            }
        }
        return esEpisodeService.save(EpisodeIndex.convert(episodeDto, clientId));
    }

    @Override
    public EpisodeIndex getEpisodeDocument(Long id, Long clientId) {
        return esEpisodeService.findByIdAndClientId(id, clientId);
    }

    @Override
    public void updateElasticDocument(Long id, Long clientId, boolean isHierarchyDataUpdated) {
        EpisodeDto episodeFromDb = getEpisodeDetails(id, clientId, false, true, false, true);  // tabpermission = true to force fetching from DB
        if (episodeFromDb.isDeleted()) {
            esEpisodeService.delete(String.valueOf(id), clientId); // hard delete from elastic
            return;
        }
        episodeFromDb.setTabPermissions(null);
        Map<String, Object> updatedVals = Utils.objectToMap(episodeFromDb);
        Map<String, Object> episodeDataToUpdate = new HashMap<>(episodeFromDb.getStageData());
        Map<String, Object> hierarchyMappings = episodeFromDb.getHierarchyMappings();
        if (isHierarchyDataUpdated && hierarchyMappings != null) {
            episodeDataToUpdate.putAll(hierarchyMappings);
        }
        Map<String, Object> newDoc = clientObjToElasticDoc(episodeDataToUpdate, true, Constants.MY_MODULE);
        updatedVals.put(ElasticConstants.ASSOCIATIONS_ELASTIC_NOMENCLATURE, episodeFromDb.getAssociations());
        updatedVals.put(ElasticConstants.STAGE_DATA_ELASTIC_NOMENCLATURE, newDoc.get(ElasticConstants.STAGE_DATA_ELASTIC_NOMENCLATURE));
        Object hMappings =  newDoc.get(ElasticConstants.HIERARCHY_MAPPING_ELASTIC_NOMENCLATURE);
        if (isHierarchyDataUpdated && null != hMappings) {
            updatedVals.put(ElasticConstants.HIERARCHY_MAPPING_ELASTIC_NOMENCLATURE, hMappings);
        }
        UpdateRequest updateRequest = new UpdateRequest(ElasticConstants.EPISODE_INDEX, String.valueOf(id)).doc(updatedVals);
        esEpisodeService.updateToElastic(updateRequest);
    }

    @Override
    public EpisodeSearchResult elasticSearch(EpisodeSearchRequest searchReq) {
        boolean fetchFull = searchReq.getFieldsToShow().contains(ElasticConstants.HIERARCHY_MAPPING_ALL_ELASTIC_NOMENCLATURE) || searchReq.getFieldsToShow().size() > 20;
        return elasticSearch(searchReq, fetchFull);
    }

    @Override
    public EpisodeSearchResult elasticSearch(EpisodeSearchRequest searchReq, boolean fetchFullDoc) {
        return esEpisodeService.search(searchReq, getFieldToTableTypeMap(), fetchFullDoc);
    }

    @Override
    public Map<String, String> getFieldToTableTypeMap() {
        return getFieldToTableTypeMap(null);
    }

    @Override
    public Map<String, String> getFieldToTableTypeMap(String module) {
        Map<String, String> fieldToTableMap = new HashMap<>();
        List<SupportedRelation> supportedRelationsList = supportedRelationRepository.findAll();
        List<Field> fieldList = diseaseTemplateService.getAllFields();
        supportedRelationsList.forEach(relation -> {
            if (relation.getType() == SupportedRelationType.HIERARCHY) {
                fieldToTableMap.put(relation.getName(), ElasticConstants.HIERARCHY_MAPPING_ELASTIC_NOMENCLATURE);
                fieldToTableMap.put(relation.getName() + ElasticConstants.HIERARCHY_MAPPING_ALL_ELASTIC_NOMENCLATURE, ElasticConstants.HIERARCHY_MAPPING_ELASTIC_NOMENCLATURE);
                // adding this only for Nikshay use cases (since to convert nikshay patient obj it sends all
                // it's hierarchies as HierarchyMapping_<>.
                fieldToTableMap.put(ElasticConstants.HIERARCHY_MAPPING_CLIENT_NOMENCLATURE + StringUtils.capitalize(relation.getName()), ElasticConstants.HIERARCHY_MAPPING_ELASTIC_NOMENCLATURE);
            } else {
                fieldToTableMap.put(relation.getName(), ElasticConstants.ASSOCIATIONS_ELASTIC_NOMENCLATURE);
            }
        });
        fieldList.forEach(field -> {
            if (module == null || module.equalsIgnoreCase(field.getModule()))
                fieldToTableMap.put(field.getKey(), ElasticConstants.STAGE_DATA_ELASTIC_NOMENCLATURE);
        });
        fieldToTableMap.put(FieldConstants.PERSON_FIELD_NAME, ElasticConstants.STAGE_DATA_ELASTIC_NOMENCLATURE);
        fieldToTableMap.put(FieldConstants.IS_EPISODE_ENABLED, ElasticConstants.STAGE_DATA_ELASTIC_NOMENCLATURE);
        return fieldToTableMap;
    }

    @Override
    public Map<String, Object> clientObjToElasticDoc(Map<String, Object> episodeObj) {
        return clientObjToElasticDoc(episodeObj, false, null);
    }

    @Override
    public Map<String, Object> clientObjToElasticDoc(Map<String, Object> episodeObj, boolean onlyStageData, String module) {
        Map<String, String> fieldToTableMap = getFieldToTableTypeMap(module);
        Map<String, Object> newDoc = new HashMap<>();
        episodeObj.forEach((k, v) -> {
            k = StringUtils.uncapitalize(k);
            if (fieldToTableMap.containsKey(k)) {
                String val = fieldToTableMap.get(k);
                if (val.equals(ElasticConstants.HIERARCHY_MAPPING_ELASTIC_NOMENCLATURE)) {
                    String[] kArray = k.split("_");
                    k = (kArray.length > 1) ? kArray[1] : k;
                }
                newDoc.putIfAbsent(val, new HashMap<>());
                if (v != null || module != null) { // we update to null values while updating a doc
                    ((Map<String, Object>) newDoc.get(val)).put(StringUtils.uncapitalize(k), v);
                }
            } else {
                // if updating values only from stage data, then if not present in map
                // indicates that some incorrect value has been sent to episode.
                if (!onlyStageData && v != null) newDoc.put(k, v);
            }
        });
        if (newDoc.containsKey(ElasticConstants.HIERARCHY_MAPPING_ELASTIC_NOMENCLATURE)) {
            Map<String, Object> hMappings = new HashMap<>((Map<String, Object>) newDoc.get(ElasticConstants.HIERARCHY_MAPPING_ELASTIC_NOMENCLATURE));
            hMappings.forEach((key, val) -> {
                if (val != null)
                    ((Map<String, Object>) newDoc.get(ElasticConstants.HIERARCHY_MAPPING_ELASTIC_NOMENCLATURE)).put(key + ElasticConstants.HIERARCHY_MAPPING_ALL_ELASTIC_NOMENCLATURE, registryService.getAllLevelIdsForHierarchy(Long.parseLong(String.valueOf(val)), false));
            });
        }
        return newDoc;
    }

    @Override
    public void convertAndSavetoElastic(Map<String, Object> episodeObj, Long clientId) {
        EpisodeIndex episodeIndex = EpisodeIndex.convert(clientObjToElasticDoc(episodeObj), clientId);
        Stage stage = stageRepository.findByStageName(episodeIndex.getStageKey());
        episodeIndex.setStageKey(stage.getStageName());
        episodeIndex.setStageName(stage.getStageDisplayName());
        episodeIndex.setCurrentStageId(stage.getId());
        episodeIndex.getStageData().put(FieldConstants.PERSON_FIELD_NAME, episodeIndex.getStageData().getOrDefault(FieldConstants.PERSON_FIELD_FIRST_NAME, "") + " " + episodeIndex.getStageData().getOrDefault(FieldConstants.PERSON_FIELD_LAST_NAME, ""));
        episodeIndex.getStageData().put(FieldConstants.IS_EPISODE_ENABLED, false);
        esEpisodeService.save(episodeIndex);
    }

    @Override
    public void updateClientEpisodeToElastic(Map<String, Object> episodeObj, Long clientId, String module) {
        Map<String, Object> newDoc = clientObjToElasticDoc(episodeObj, true, module);
        List<Long> eIds = new ArrayList<>();
        if (episodeObj.containsKey(FieldConstants.DELETED)
                && (Boolean) episodeObj.get(FieldConstants.DELETED)
                && episodeObj.containsKey(FieldConstants.EPISODE_FIELD_ID)) {
            esEpisodeService.delete(episodeObj.get(FieldConstants.EPISODE_FIELD_ID).toString(), clientId); // since it's a nikshay patient, we delete from elastic and return.
            return;
        }
        if (episodeObj.containsKey(FieldConstants.EPISODE_FIELD_PERSON_ID)) {
            Long pId = Long.parseLong(String.valueOf(episodeObj.get(FieldConstants.EPISODE_FIELD_PERSON_ID)));
            eIds = esEpisodeService.findAllByPersonId(pId, clientId)
                    .stream().map(m -> Long.valueOf(m.getId())).collect(Collectors.toList());
        } else if (episodeObj.containsKey(FieldConstants.EPISODE_FIELD_ID)) {
            // new multi-dosing flow has introduced entity ids (strings) which are != episode ids
            try {
                eIds.add(Long.valueOf(String.valueOf(episodeObj.get(FieldConstants.EPISODE_FIELD_ID))));
            } catch (NumberFormatException ex) {
                return;
            }
        }
        if (module.equalsIgnoreCase(UserServiceConstants.PERSON_MODULE_NAME)) {
            newDoc.putIfAbsent(ElasticConstants.STAGE_DATA_ELASTIC_NOMENCLATURE, new HashMap<String, Object>());
            if (episodeObj.containsKey(FieldConstants.PERSON_FIELD_FIRST_NAME)) {
                ((Map<String, Object>) newDoc.get(ElasticConstants.STAGE_DATA_ELASTIC_NOMENCLATURE)).put(FieldConstants.PERSON_FIELD_NAME, episodeObj.get(FieldConstants.PERSON_FIELD_FIRST_NAME) + " " + episodeObj.get(FieldConstants.PERSON_FIELD_LAST_NAME));
            }
        } else if (module.equalsIgnoreCase(Constants.MY_MODULE)) {
            Utils.getClassFieldName(Episode.class).forEach(f -> {
                if (episodeObj.containsKey(f)) {
                    newDoc.put(f, episodeObj.get(f));
                }
            });
            if (episodeObj.containsKey(FieldConstants.FIELD_STAGE)) {
                String initStage = (String) episodeObj.get(FieldConstants.FIELD_STAGE);
                Stage stage = stageRepository.findByStageName(initStage);
                newDoc.put(FieldConstants.EPISODE_FIELD_STAGE_KEY, stage.getStageName());
                newDoc.put(FieldConstants.EPISODE_FIELD_STAGE_NAME, stage.getStageDisplayName());
                newDoc.put(FieldConstants.EPISODE_FIELD_CURRENT_STAGE_ID, stage.getId());
            }
            if (episodeObj.containsKey(FieldConstants.TB_TREATMENT_START_DATE)) {
                String startDate = (String) episodeObj.get(FieldConstants.TB_TREATMENT_START_DATE);
                newDoc.put(
                        FieldConstants.EPISODE_FIELD_START_DATE,
                        Utils.getFormattedDateNew(Utils.convertStringToDateNew(startDate))
                );
            }
            if (episodeObj.containsKey(FieldConstants.CLIENT_END_DATE)) {
                String endDate = (String) episodeObj.get(FieldConstants.CLIENT_END_DATE);
                newDoc.put(
                        FieldConstants.EPISODE_FIELD_END_DATE,
                        Utils.getFormattedDateNew(Utils.convertStringToDateNew(endDate))
                );
            }
            if (episodeObj.containsKey(FieldConstants.TYPE_OF_PATIENT)) {
                newDoc.put(FieldConstants.EPISODE_FIELD_TYPE_OF_EPISODE, episodeObj.get(FieldConstants.TYPE_OF_PATIENT));
            }
        }
        if (eIds.size() > 0) {
            BulkRequest bulkRequest = new BulkRequest();
            eIds.forEach(id -> {
                UpdateRequest updateRequest = new UpdateRequest(ElasticConstants.EPISODE_INDEX, String.valueOf(id)).doc(newDoc);
                bulkRequest.add(updateRequest);
            });
            esEpisodeService.updateBulkElastic(bulkRequest);
        }
    }

    public List<Episode> getEpisodeByPhoneNumber(String phoneNumber) {
        ResponseEntity<Response<List<Map<String, Object>>>> userResponse = userService.getByPrimaryPhone(phoneNumber);
        List<Map<String, Object>> personIdsMap = userService.processResponseEntity(userResponse, String.format("Unable to fetch user details for phoneNumber ", phoneNumber));
        List<Episode> episodes = new ArrayList<>();
        if (null != personIdsMap && personIdsMap.size() != 0) {
            List<Long> personIdList = personIdsMap.stream().map(m -> Long.parseLong(String.valueOf(m.get(FieldConstants.EPISODE_FIELD_ID)))).collect(Collectors.toList());
            Long clientId = clientService.getClientByTokenOrDefault().getId();
            episodes = episodeRepository.findAllByPersonIdInAndClientIdAndDeletedFalse(personIdList, clientId);
            if (episodes == null || CollectionUtils.isEmpty(episodes)) {
                throw new NotFoundException("No episode found corresponding to person ids " + personIdList.toString());
            }
        }
        return episodes;
    }


    @Override
    public List<Long> search(SearchEpisodesRequest searchEpisodesRequest) {
        LocalDateTime startDate = StringUtils.hasText(searchEpisodesRequest.getFrom()) ? Utils.convertStringToDateNew(searchEpisodesRequest.getFrom()) : LocalDateTime.of(2017, 1, 1, 1, 1, 1);
        LocalDateTime endDate = StringUtils.hasText(searchEpisodesRequest.getTo()) ? Utils.convertStringToDateNew(searchEpisodesRequest.getTo()) : LocalDateTime.now().plusDays(1);
        List<Long> stageIds = !CollectionUtils.isEmpty(searchEpisodesRequest.getEpisodeStageIdList()) ? searchEpisodesRequest.getEpisodeStageIdList() : stageRepository.findAll().stream().map(Stage::getId).collect(Collectors.toList());
        List<Long> episodeIds = episodeStageRepository.getEpisodeIdsByStartDateAndStage(startDate, endDate, stageIds);
        Client client = clientService.getClientByTokenOrDefault();
        List<Episode> episodes = episodeRepository.getAllEpisodes(episodeIds, client.getId());
        episodes = episodes.stream().filter(episode -> CollectionUtils.isEmpty(searchEpisodesRequest.getDiseaseTemplateIds()) || searchEpisodesRequest.getDiseaseTemplateIds().contains(episode.getDiseaseId())).collect(Collectors.toList());
        List<EpisodeDto> episodeDtos = getEpisodeDetailsBulk(episodes, false);
        episodeDtos = episodeDtos.stream().filter(episodeDto -> CollectionUtils.isEmpty(searchEpisodesRequest.getHiearchyIdList()) || searchEpisodesRequest.getHiearchyIdList().contains(episodeDto.getHierarchyMappings().get(searchEpisodesRequest.getHierarchyType()))).collect(Collectors.toList());
        if (searchEpisodesRequest.getIsRiskStatusOptional() != null && !searchEpisodesRequest.getIsRiskStatusOptional()) {
            episodeDtos = episodeDtos.stream().filter(episodeDto -> StringUtils.hasText(episodeDto.getRiskStatus())).collect(Collectors.toList());
        }
        return episodeDtos.stream().map(EpisodeDto::getId).collect(Collectors.toList());
    }

    @Override
    public void updateRiskStatus(UpdateRiskRequest updateRiskRequest) {
        episodeRepository.updateRiskStatusForEpisodeIdWithStatus(updateRiskRequest.getHighRiskIdList(), HIGH);
        episodeRepository.updateRiskStatusForEpisodeIdWithStatus(updateRiskRequest.getLowRiskIdList(), LOW);
        Map<String, Object> valsToUpdate = new HashMap<>();
        valsToUpdate.put(FieldConstants.EPISODE_FIELD_RISK_STATUS, HIGH);
        updateValuesToElasticBulk(updateRiskRequest.getHighRiskIdList(), valsToUpdate);
        valsToUpdate.put(FieldConstants.EPISODE_FIELD_RISK_STATUS, LOW);
        updateValuesToElasticBulk(updateRiskRequest.getLowRiskIdList(), valsToUpdate);
    }

    @Override
    public void updateValuesToElasticBulk(List<Long> episodeIds, Map<String, Object> valuesToUpdate) {
        BulkRequest bulkRequest = new BulkRequest();
        episodeIds.forEach(id -> {
            UpdateRequest updateRequest = new UpdateRequest(ElasticConstants.EPISODE_INDEX, String.valueOf(id))
                    .doc(valuesToUpdate);
            bulkRequest.add(updateRequest);
        });
        esEpisodeService.updateBulkElastic(bulkRequest);
    }

    @Override
    public List<EpisodeDto> getEpisodesBulk(EpisodeBulkRequest episodeBulkRequest) {
        Client client = clientService.getClientByTokenOrDefault();
        List<Long> episodeIds = episodeBulkRequest.getEpisodeIds();
        List<Episode> episodes = episodeRepository.getAllEpisodes(episodeIds, client.getId());
        return getEpisodeDetailsBulk(episodes, false);
    }

    @Override
    public PNDtoWithNotificationData getDoseReminderPNDtosForClientId(Long clientId, String timeZone) {
        List<Episode> episodes = episodeRepository.findAllByClientIdAndDeletedFalse(clientId);
        List<Long> episodeIdList = episodes.stream().map(Episode::getId).collect(Collectors.toList());
        List<Field> tempFields = diseaseTemplateService.getAllFieldsByNameIn(Collections.singletonList(FieldConstants.ESD_TREATMENT_PLAN_ID));
        Field treatmentPlanField = tempFields.size() > 0 ? tempFields.get(0) : null;
        if (null != treatmentPlanField) {
            List<Long> episodeIdsWithTreatmentPlan = episodeStageDataRepository.getEpisodeFieldIdValueMapForFieldIdsInAndClientId(Collections.singletonList(treatmentPlanField.getId()), clientId)
                    .stream().map(EpisodeFieldIdValueDtoInterface::getEpisodeId).collect(Collectors.toList());
            episodeIdList.removeAll(episodeIdsWithTreatmentPlan);
        }
        LocalDate today = Utils.getCurrentDateInGivenTimeZone(timeZone);
        ZonedDateTime dayStartTime = today.atStartOfDay(ZoneId.of(timeZone));
        ZonedDateTime dayEndTime = today.plusDays(1).atStartOfDay(ZoneId.of(timeZone));
        String currentDayStartTimeInUTC = Utils.convertDateTimeToSpecificZoneDateTime(dayStartTime, ZoneOffset.UTC);
        String currentDayEndTimeInUTC = Utils.convertDateTimeToSpecificZoneDateTime(dayEndTime, ZoneOffset.UTC);
        List<String> episodeIdStringList = episodeIdList.stream().map(String::valueOf).collect(Collectors.toList());
        episodeIdStringList = getEpisodeIdListWhoHaveNotMarkedAdherenceForToday(episodeIdStringList, currentDayStartTimeInUTC, currentDayEndTimeInUTC);
        return getPNDtoWithNotificationDataForEpisodeIds(episodeIdStringList, new HashMap<>());
    }

    private PNDtoWithNotificationData getPNDtoWithNotificationDataForEpisodeIds(List<String> episodeIds, Map<String, String> contentParameters) {
        List<PushNotificationTemplateDto> pushNotificationTemplateDtoList = new ArrayList<>();
        Map<String, List<String>> userDeviceIdsMap = getDeviceIdList(episodeIds);
        if (!CollectionUtils.isEmpty(userDeviceIdsMap)) {
            userDeviceIdsMap.forEach((episodeId, deviceIdList) -> deviceIdList.forEach(deviceId -> pushNotificationTemplateDtoList.add(new PushNotificationTemplateDto(episodeId, deviceId, null, contentParameters))));
        }
        return new PNDtoWithNotificationData(pushNotificationTemplateDtoList, false, null, true);
    }

    public PNDtoWithNotificationData getMorningDoseReminderPNDtosForClientIdAndSchedule(Long clientId, String timeZone) {
        List<String> episodesToReceiveNotification = getEpisodeIdsWithClientIdAndScheduleTimeIndex(clientId, ScheduleTimeEnum.MORNING.getScheduleStringIndex(), timeZone);
        return getPNDtoWithNotificationDataForEpisodeIds(episodesToReceiveNotification, new HashMap<>());
    }

    public PNDtoWithNotificationData getAfternoonDoseReminderPNDtosForClientIdAndSchedule(Long clientId, String timeZone) {
        List<String> episodesToReceiveNotification = getEpisodeIdsWithClientIdAndScheduleTimeIndex(clientId, ScheduleTimeEnum.AFTERNOON.getScheduleStringIndex(), timeZone);
        return getPNDtoWithNotificationDataForEpisodeIds(episodesToReceiveNotification, new HashMap<>());
    }

    public PNDtoWithNotificationData getEveningDoseReminderPNDtosForClientIdAndSchedule(Long clientId, String timeZone) {
        List<String> episodesToReceiveNotification = getEpisodeIdsWithClientIdAndScheduleTimeIndex(clientId, ScheduleTimeEnum.EVENING.getScheduleStringIndex(), timeZone);
        return getPNDtoWithNotificationDataForEpisodeIds(episodesToReceiveNotification, new HashMap<>());
    }

    public PNDtoWithNotificationData getNightDoseReminderPNDtosForClientIdAndSchedule(Long clientId, String timeZone) {
        List<String> episodesToReceiveNotification = getEpisodeIdsWithClientIdAndScheduleTimeIndex(clientId, ScheduleTimeEnum.NIGHT.getScheduleStringIndex(), timeZone);
        return getPNDtoWithNotificationDataForEpisodeIds(episodesToReceiveNotification, new HashMap<>());
    }

    public List<String> getEpisodeIdsWithClientIdAndScheduleTimeIndex(Long clientId, Integer index, String timeZone) {
        Field treatmentPlanField = diseaseTemplateService.getAllFieldsByNameIn(Collections.singletonList(FieldConstants.ESD_TREATMENT_PLAN_ID)).get(0);
        List<EpisodeFieldIdValueDtoInterface> episodeIdsWithTreatmentPlan = episodeStageDataRepository.getEpisodeFieldIdValueMapForFieldIdsInAndClientId(Collections.singletonList(treatmentPlanField.getId()), clientId);
        List<Long> treatmentPlanIdList = episodeIdsWithTreatmentPlan.stream().
                map(episodeFieldIdValueMap -> NumberUtils.toLong(episodeFieldIdValueMap.getFieldValue()))
                .collect(Collectors.toList());
        Map<Long, String> treatmentPlanToAggregatedSchedule = treatmentPlanService.getTreatmentPlanAggregatedScheduleStringMapForTreatmentPlanIds(treatmentPlanIdList,
                Utils.getCurrentLocalDateUTCInGivenTimeZone(timeZone).toLocalDate());
        List<String> episodeIdsToReceiveNotification = new ArrayList<>();
        for (EpisodeFieldIdValueDtoInterface episodeFieldIdValueMap : episodeIdsWithTreatmentPlan) {
            String schedule = treatmentPlanToAggregatedSchedule.get(NumberUtils.toLong(episodeFieldIdValueMap.getFieldValue()));
            if (null != schedule && EPISODE_SCHEDULE_TIME_TRUE_BIT == schedule.charAt(index)) {
                episodeIdsToReceiveNotification.add(String.valueOf(episodeFieldIdValueMap.getEpisodeId()));
            }
        }
        return episodeIdsToReceiveNotification;
    }

    @Override
    public PNDtoWithNotificationData getAddUserRelationPnDtos(Long clientId, EpisodeDto episodeDto, Map<String, Object> userRelationRequest) {
        List<PushNotificationTemplateDto> pushNotificationTemplateDtoList = new ArrayList<>();
        //Fetching deviceID from SSO
        ResponseEntity<Response<DeviceIdsMapResponse>> deviceIdResponse = ssoService.fetchDeviceIdUserMap(Arrays.asList(String.valueOf(episodeDto.getId())));
        Map<String, List<String>> userDeviceIdsMap = deviceIdResponse.getBody() != null && deviceIdResponse.getBody().isSuccess() ? deviceIdResponse.getBody().getData().getUserDeviceIdsMap() : null;
        Long primaryUserId = Long.parseLong(String.valueOf(userRelationRequest.get(UserServiceField.PRIMARY_USER_ID.getFieldName())));
        List<EpisodeDto> primaryUser = getEpisodesForPersonList(Collections.singletonList(primaryUserId), clientId, true);
        Map<String, String> contentParameters = new HashMap<>();
        Object mobileList = primaryUser.get(0).getStageData().get(FieldConstants.PERSON_MOBILE_LIST);
        List<Map<String, String>> mobileListMap = (List<Map<String, String>>) mobileList;
        contentParameters.put(NotificationConstants.PARAM_NAME, String.valueOf(primaryUser.get(0).getStageData().get(FieldConstants.PERSON_FIELD_FIRST_NAME)));
        contentParameters.put(NotificationConstants.PARAM_PHONE, String.valueOf(mobileListMap.get(0).get(FieldConstants.NUMBER)));
        String episodeId = String.valueOf(episodeDto.getId());
        if(!CollectionUtils.isEmpty(userDeviceIdsMap)) {
            userDeviceIdsMap.forEach((e, deviceIdList) -> deviceIdList.forEach(deviceId -> pushNotificationTemplateDtoList.add(new PushNotificationTemplateDto(episodeId, deviceId, null, contentParameters))));
        } else {
            // adding notification in episode logs if no active session found
            pushNotificationTemplateDtoList.add(new PushNotificationTemplateDto(episodeId, "", null, contentParameters));
        }
        String extras = Utils.asJsonString(userRelationRequest);
        PNDtoWithNotificationData pnDtoWithNotificationData = new PNDtoWithNotificationData(pushNotificationTemplateDtoList, true, extras, true);

        return pnDtoWithNotificationData;
    }

    @Override
    public List<String> getEpisodeIdListWhoHaveNotMarkedAdherenceForToday(List<String> episodeIdList, String currentDayStartTimeInUTC, String currentDayEndTimeInUTC) {
        List<RangeFilters> filters = Collections.singletonList(new RangeFilters(RangeFilterType.LAST_DOSAGE, currentDayStartTimeInUTC, currentDayEndTimeInUTC));
        SearchAdherenceRequest adherenceRequest = new SearchAdherenceRequest(episodeIdList, filters);
        ResponseEntity<Response<AllAdherenceResponse>> allAdherenceResponse = iamService.getAdherenceBulk(adherenceRequest);
        List<AdherenceResponse> adherenceResponse = Utils.processResponseEntity(allAdherenceResponse, "Adherence not found").getAdherenceResponseList();
        List<String> patientsWhoHaveMarkedAdherence = adherenceResponse.stream().map(AdherenceResponse::getEntityId).collect(Collectors.toList());
        episodeIdList.removeAll(patientsWhoHaveMarkedAdherence);
        return episodeIdList;
    }

    @Override
    public Map<String, List<String>> getDeviceIdList(List<String> episodeIdList) {
        ResponseEntity<Response<DeviceIdsMapResponse>> deviceIdResponse = ssoService.fetchDeviceIdUserMap(episodeIdList);
        return Utils.processResponseEntity(deviceIdResponse, "No deviceId found").getUserDeviceIdsMap();
    }

    @Override
    public PNDtoWithNotificationData getRefillReminderPNDtosForClientId(Long clientId, String timeZone) {
        List<Episode> episodes = episodeRepository.findAllByClientIdAndDeletedFalse(clientId);
        List<EpisodeDto> episodeDtoList = getEpisodeDetailsBulk(episodes, false);
        List<String> episodeIdList = new ArrayList<>();
        episodeDtoList.forEach(episodeDto -> {
            Object numberOfDaysOfMedication = episodeDto.getStageData().get(FieldConstants.NUMBER_OF_DAYS_OF_MEDICATION);
            if (null != numberOfDaysOfMedication) {
                ZonedDateTime refillReminderDateInUTC = episodeDto.getStartDate().plusDays(Long.parseLong(String.valueOf(numberOfDaysOfMedication))).atZone(ZoneId.systemDefault());
                ZoneOffset zoneOffset = ZoneId.of(timeZone).getRules().getOffset(episodeDto.getStartDate());
                String refillReminderDateInGivenTimeZone = Instant.parse(refillReminderDateInUTC.toInstant().toString()).atOffset(zoneOffset).format(DateTimeFormatter.ofPattern(Constants.DATE_FORMAT_WITHOUT_TIME));
                LocalDate currentDateInGivenTimeZone = LocalDateTime.ofInstant(Utils.getCurrentDate().toInstant(), ZoneId.of(timeZone)).toLocalDate();
                if (refillReminderDateInGivenTimeZone.equals(currentDateInGivenTimeZone.toString())) {
                    episodeIdList.add(episodeDto.getId().toString());
                }
            }
        });
        List<PushNotificationTemplateDto> pushNotificationTemplateDtoList = new ArrayList<>();
        if (!CollectionUtils.isEmpty(episodeIdList)) {
            Map<String, List<String>> userDeviceIdsMap = getDeviceIdList(episodeIdList);
            if (!CollectionUtils.isEmpty(userDeviceIdsMap)) {
                userDeviceIdsMap.forEach((episodeId, deviceIdList) -> deviceIdList.forEach(deviceId -> pushNotificationTemplateDtoList.add(new PushNotificationTemplateDto(episodeId, deviceId, null, null))));
            }
        }
        return new PNDtoWithNotificationData(pushNotificationTemplateDtoList, true, null, true);
    }

    @Override
    public EpisodeSearchResult taskListSearch(EpisodeTasklistRequest episodeTasklistRequest) {
        EpisodeSearchRequest episodeSearchRequest = taskListFiltersToEpisodeSearchFields.transform(episodeTasklistRequest);
        boolean isRefillDue = episodeTasklistRequest.getTaskListFilterRequests().stream().anyMatch(f -> f.getFilterName().equalsIgnoreCase(TaskListFields.TASK_LIST_REFILL_DUE));
        boolean consecutiveMissedDoseFromYesterdayTaskList = episodeTasklistRequest.getTaskListFilterRequests().stream().anyMatch(f -> f.getFilterName().equalsIgnoreCase(TaskListFields.TASK_LIST_ALLOWED_RANGE_JSON));
        boolean isTagListRequired = episodeTasklistRequest.getTaskListFilterRequests().stream().anyMatch(f -> f.getFilterName().equalsIgnoreCase(TaskListFields.TASK_LIST_TAG_LIST));
        boolean positiveDosingDaysTaskList = episodeTasklistRequest.getTaskListFilterRequests().stream().anyMatch(f -> f.getFilterName().equalsIgnoreCase(TaskListFields.TASK_LIST_POSITIVE_DOSING_DAYS));
        int pageSize = episodeSearchRequest.getSize();
        int page = episodeSearchRequest.getPage();
        //get all docs for proper pagination done by external microservices.
        if (isRefillDue || consecutiveMissedDoseFromYesterdayTaskList || isTagListRequired || positiveDosingDaysTaskList) {
            episodeSearchRequest.setSize(10000);
            episodeSearchRequest.setPage(0);
        }
        episodeSearchRequest.getSearch().getMust().put(CLIENT_ID, clientService.getClientByTokenOrDefault().getId());
        EpisodeSearchResult episodeSearchResult = elasticSearch(episodeSearchRequest);

        if (isRefillDue) {
            List<EpisodeIndex> episodeIndexList = episodeSearchResult.getEpisodeIndexList();
            List<Long> episodeIds = episodeIndexList.stream().map(m -> Long.parseLong(m.getId())).collect(Collectors.toList());
            Map<Long, String> entityIdRefillDateMap = dispensationService.getEpisodeIdsWithRefillDueDate(new RefillDatesRequest(
                    episodeIds, Utils.getFormattedDateNew(Utils.getCurrentDateNew(), "dd-MM-yyyy HH:mm:ss"), Utils.getFormattedDateNew(Utils.getCurrentDateNew().plusDays(2), "dd-MM-yyyy HH:mm:ss")
            ));
            List<EpisodeIndex> episodeIndexListWithRefillDate = new ArrayList<>();
            HashSet<Long> refillDueIdsToFilter = new HashSet<>(entityIdRefillDateMap.keySet());
            episodeIndexList.forEach(e -> {
                if (refillDueIdsToFilter.contains(Long.parseLong(e.getId()))) {
                    String refillDate = entityIdRefillDateMap.get(Long.parseLong(e.getId()));
                    if (StringUtils.isEmpty(refillDate)) {
                        refillDate = e.getStartDate();
                    }
                    e.getStageData().put(FieldConstants.FIELD_REFILL_DATE, refillDate);
                    episodeIndexListWithRefillDate.add(e);
                }
            });
            episodeSearchResult.setTotalHits(episodeIndexListWithRefillDate.size());
            int startIndex = page * pageSize;
            episodeSearchResult.setEpisodeIndexList(episodeIndexListWithRefillDate.subList(startIndex, Math.min(startIndex + pageSize, episodeIndexListWithRefillDate.size())));
        }
        if (consecutiveMissedDoseFromYesterdayTaskList) {
            List<EpisodeIndex> episodeIndexList = episodeSearchResult.getEpisodeIndexList();
            List<String> episodeIds = episodeIndexList.stream().map(EpisodeIndex::getId).collect(Collectors.toList());
            TaskListFilterRequest filter = episodeTasklistRequest.getTaskListFilterRequests().stream().filter(f -> f.getFilterName().equalsIgnoreCase(TaskListFields.TASK_LIST_ALLOWED_RANGE_JSON)).findFirst().orElse(null);
            if (filter != null) {
                List<TaskListFiltersToEpisodeSearchFields.Range> rangeList;
                try {
                    rangeList = Utils.jsonToObject(filter.getValue(), new TypeReference<List<TaskListFiltersToEpisodeSearchFields.Range>>() {
                    });
                } catch (IOException e) {
                    throw new ValidationException("Invalid AllowedRangeJsonString value sent");
                }
                HashSet<Long> allIds = new HashSet<>();
                rangeList.forEach(r -> {
                    String lowerBound = r.getLowerBound() == 0 ? null : String.valueOf(r.getLowerBound());
                    String upperBound = r.getUpperBound() == 0 ? null : String.valueOf(r.getUpperBound());
                    ResponseEntity<Response<AllAdherenceResponse>> response = iamService.getAdherenceBulk(new SearchAdherenceRequest(episodeIds, Collections.singletonList(new RangeFilters(RangeFilterType.CONSECUTIVE_MISSED_DOSES_FROM_YESTERDAY, lowerBound, upperBound))));
                    allIds.addAll(response.getBody().getData().getAdherenceResponseList().stream().map(m -> Long.valueOf(m.getEntityId())).collect(Collectors.toList()));
                });
                episodeIndexList = episodeIndexList.stream().filter(
                        f -> allIds.contains(Long.parseLong(f.getId()))
                ).collect(Collectors.toList());
                episodeSearchResult.setTotalHits(episodeIndexList.size());
                int startIndex = page * pageSize;
                if (episodeIndexList.size() >= startIndex)
                    episodeSearchResult.setEpisodeIndexList(episodeIndexList.subList(startIndex, Math.min(startIndex + pageSize, episodeIndexList.size())));
                else // since this "page" does not exists in list, we will return empty array
                    episodeSearchResult.setEpisodeIndexList(new ArrayList<>());
            }
        }
        if (isTagListRequired) {
            List<EpisodeIndex> episodeIndexList = episodeSearchResult.getEpisodeIndexList();
            TaskListFilterRequest tagsFilter = episodeTasklistRequest.getTaskListFilterRequests().stream().filter(e -> e.getFilterName().equalsIgnoreCase(TaskListFields.TASK_LIST_TAG_LIST)).findFirst().get();
            List<String> tagList;
            try {
                tagList = Utils.jsonToObject(tagsFilter.getValue(), new TypeReference<List<String>>() {
                });
            } catch (IOException e) {
                throw new ValidationException("Invalid tag list value" + tagsFilter.getValue());
            }
            List<Long> episodeIdToSearch = episodeIndexList.stream().map(e -> Long.valueOf(e.getId())).collect(Collectors.toList());
            List<EpisodeTagResponse> episodeTagResponse = episodeTagService.search(new SearchTagsRequest(episodeIdToSearch, tagList));
            HashSet<String> episodeWithTagsFilter = episodeTagResponse.stream().map(e -> String.valueOf(e.getEpisodeId())).collect(Collectors.toCollection(HashSet::new));
            List<EpisodeIndex> episodeIndexFinal = episodeIndexList.stream().filter(e -> episodeWithTagsFilter.contains(e.getId())).collect(Collectors.toList());
            episodeSearchResult.setTotalHits(episodeIndexFinal.size());
            int startIndex = page * pageSize;
            episodeSearchResult.setEpisodeIndexList(episodeIndexFinal.subList(startIndex, Math.min(startIndex + pageSize, episodeIndexFinal.size())));
        }
        if(positiveDosingDaysTaskList) {
            List<EpisodeIndex> episodeIndexList = episodeSearchResult.getEpisodeIndexList();
            List<String> episodeIds = episodeIndexList.stream().map(EpisodeIndex::getId).collect(Collectors.toList());
            ResponseEntity<Response<AllAdherenceResponse>> response = iamService.getAdherenceBulk(new SearchAdherenceRequest(episodeIds, null));
            AllAdherenceResponse allAdherenceResponse = response.getBody().getData();
            List<AdherenceResponse> adherenceResponseList = allAdherenceResponse.getAdherenceResponseList();
            Map<String, String> adherenceStringMap = adherenceResponseList.stream().collect(Collectors.toMap(AdherenceResponse::getEntityId, AdherenceResponse::getAdherenceString));

            TaskListFilterRequest filter = episodeTasklistRequest.getTaskListFilterRequests().stream().filter(f -> f.getFilterName().equalsIgnoreCase(TaskListFields.TASK_LIST_MIN_DAYS_SINCE_TREATMENT_START)).findFirst().orElse(null);
            LocalDateTime maxStartDate = LocalDateTime.now().minusDays(Integer.parseInt(filter.getValue()));

            TaskListFilterRequest positiveDosingDaysFilter = episodeTasklistRequest.getTaskListFilterRequests().stream().filter(f -> f.getFilterName().equalsIgnoreCase(TaskListFields.TASK_LIST_POSITIVE_DOSING_DAYS)).findFirst().orElse(null);
            int positiveDosingDays = Integer.parseInt(positiveDosingDaysFilter.getValue());

            List<EpisodeIndex> episodeIndicesFinalList = new ArrayList<>();
            for(EpisodeIndex episodeIndex: episodeIndexList) {
                LocalDateTime startDate = Utils.convertStringToDateNew(episodeIndex.getStartDate());
                boolean includeEpisode = startDate.isBefore(maxStartDate);
                if(!includeEpisode && adherenceStringMap.containsKey(episodeIndex.getId())) {
                    String adherenceString = adherenceStringMap.get(episodeIndex.getId());
                    DosageCounts dosageCounts = new DosageCounts(new DosageCountsDetailed(adherenceString));

                    includeEpisode = (dosageCounts.getCalledAndManual() >= positiveDosingDays);
                }

                if(includeEpisode) {
                    episodeIndicesFinalList.add(episodeIndex);
                }
            }

            int startIndex = page * pageSize;
            episodeSearchResult.setTotalHits(episodeIndicesFinalList.size());
            episodeSearchResult.setEpisodeIndexList(episodeIndicesFinalList.subList(startIndex, Math.min(startIndex + pageSize, episodeIndicesFinalList.size())));
        }
        return episodeSearchResult;
    }

    @Override
    public EpisodeSearchResult episodeUnifiedSearch(EpisodeUnifiedSearchRequest episodeUnifiedSearchRequest) {
        EpisodeSearchRequest episodeSearchRequest = uppFiltersToEpisodeSearchFields.convert(episodeUnifiedSearchRequest);
        episodeSearchRequest.getSearch().getMust().put(CLIENT_ID, clientService.getClientByTokenOrDefault().getId());
        EpisodeSearchResult episodeSearchResult = elasticSearch(episodeSearchRequest);
        List<EpisodeIndex> episodeIndexList = episodeSearchResult.getEpisodeIndexList();

        //Fetch Adherence String from IAM
        List<String> episodeIds = episodeIndexList.stream().map(EpisodeIndex::getId).collect(Collectors.toList());
        SearchAdherenceRequest searchAdherenceRequest = new SearchAdherenceRequest(episodeIds, null);
        ResponseEntity<Response<AllAdherenceResponse>> response = iamService.getAdherenceBulk(searchAdherenceRequest);
        Map<String, String> episodeAdherenceMap = response.getBody().getData().getAdherenceResponseList().stream().collect(Collectors.toMap(AdherenceResponse::getEntityId, AdherenceResponse::getAdherenceString));

        //Fetch Tags
        List<Long> episodeIdForTags = episodeIndexList.stream().map(e -> Long.parseLong(e.getId())).collect(Collectors.toList());
        List<EpisodeTagResponse> episodeTagResponse = episodeTagService.search(new SearchTagsRequest(episodeIdForTags, null));
        Map<String, EpisodeTagResponse> episodeTagMap = episodeTagResponse.stream().collect(Collectors.toMap(e -> String.valueOf(e.getEpisodeId()), e -> e));

        episodeIndexList.forEach(e -> {
            if (episodeAdherenceMap.containsKey(e.getId())) {
                e.getStageData().put(FieldConstants.FIELD_ADHERENCE_STRING, episodeAdherenceMap.get(e.getId()));
            }
            if (episodeTagMap.containsKey(e.getId())) {
                e.getStageData().put(FieldConstants.FIELD_EPISODE_TAGS, episodeTagMap.get(e.getId()));
            }
        });

        episodeSearchResult.setEpisodeIndexList(episodeIndexList);
        return episodeSearchResult;
    }

    @Override
    public void uploadEpisodeDocument(List<MultipartFile> file, AddEpisodeDocumentRequest addEpisodeDocumentRequest) {
        EpisodeDocument episodeDocument = new EpisodeDocument(addEpisodeDocumentRequest);
        Long id = episodeDocumentRepository.save(episodeDocument).getId();

        String connectionString = DEFAULT_ENDPOINTS_PROTOCOL + accountName + ACCOUNT_KEY + accountKey + ENDPOINT_SUFFIX;

        BlobContainerClient container = new BlobContainerClientBuilder()
                .connectionString(connectionString)
                .containerName(containerName)
                .buildClient();
        List<EpisodeDocumentDetails> episodeDocumentDetailsList = new ArrayList<>();
        for (int i = 0; i < file.size(); i++) {
            BlobClient blob = container.getBlobClient(episodeDocument.getEpisodeId() + "_" + id + "_" + file.get(i).getOriginalFilename());
            try {
                blob.upload(file.get(i).getInputStream(), file.get(i).getSize(),true);
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
            String imageUrl = blob.getBlobUrl();
            EpisodeDocumentDetails episodeDocumentDetails = new EpisodeDocumentDetails(addEpisodeDocumentRequest, i, id, imageUrl);
            episodeDocumentDetailsList.add(episodeDocumentDetails);
        }
        episodeDocumentDetailsRepository.saveAll(episodeDocumentDetailsList);
    }

    @Override
    public List<EpisodeDocumentResponseDto> getPersonDocumentDetails(Long personId, boolean fetchAll) {
        List<Long> personIdList = Collections.singletonList(personId);
        if (fetchAll) {
            ResponseEntity<Response<List<Map<String, Object>>>> users = userService.getUserRelations(personId, UserServiceConstants.PERSON_RELATION_ACCEPT);
            List<Map<String, Object>> userList = Utils.processResponseEntity(users, "Unable to fetch users");
            personIdList = userList.stream().map(e -> Long.parseLong(String.valueOf(e.get(UserServiceField.Id.getFieldName())))).collect(Collectors.toList());
        }
        List<EpisodeDocumentResponseDto> episodeDocumentResponseList = new ArrayList<>();
        List<Episode> episodes = episodeRepository.findAllByPersonIdInAndClientIdAndDeletedFalse(personIdList, clientService.getClientByTokenOrDefault().getId());
        if (!CollectionUtils.isEmpty(episodes)) {
            List<Long> episodeIds = episodes.stream().map(Episode::getId).collect(Collectors.toList());
            EpisodeDocumentSearchRequest episodeDocumentSearchRequest = new EpisodeDocumentSearchRequest();
            episodeDocumentSearchRequest.setEpisodeIds(episodeIds);
            episodeDocumentResponseList = getBulkEpisodeDocumentDetails(episodeDocumentSearchRequest);
        }
        return episodeDocumentResponseList;
    }

    public List<UserSmsDetails> getUserSmsDetails(List<EpisodeIndex> episodesToSendSms, Map<String,String> templateParameters) {
        List<UserSmsDetails> userSmsDetails = new ArrayList<>();
        episodesToSendSms.forEach(e -> {
            Map<String, String> userParameters = new HashMap<>();
            userParameters.put(NotificationConstants.TEMPLATE_NAME, String.valueOf(e.getStageData().get(FieldConstants.PERSON_FIELD_NAME)));
            userParameters.put(NotificationConstants.TEMPLATE_ID, e.getId());
            userParameters.putAll(templateParameters);
            userSmsDetails.add(new UserSmsDetails(Long.parseLong(e.getId()), userParameters, String.valueOf(e.getStageData().get(FieldConstants.PERSON_PRIMARY_PHONE))));
        });

        return userSmsDetails;
    }

    @Override
    public List<UserSmsDetails> getEpisodesForTreatmentInitiation(NotificationTriggerEvent notificationTriggerEvent, String clientId) {
        EpisodeSearchRequest episodeSearchRequest = notificationTriggerEvent.getEpisodeSearchRequest();

        List<EpisodeIndex> episodeIndexList = elasticSearch(episodeSearchRequest).getEpisodeIndexList();
        List<EpisodeIndex> episodeIndexListToSendSms = episodeIndexList.stream().filter(e -> !CollectionUtils.containsAny(Arrays.asList(String.valueOf(e.getStageData().get(FieldConstants.EPISODE_FIELD_KEY_POPULATION)).split(",")), Arrays.asList(NotificationConstants.Key_Population_Not_applicable)) || NotificationConstants.HIV_STATUS_FOR_TPT.contains(String.valueOf(e.getStageData().get(FieldConstants.EPISODE_FILED_HIV_TEST_STATUS)))).collect(Collectors.toList());
        List<Long> entityIdTemp =  episodeIndexListToSendSms.stream().map(e -> Long.parseLong(e.getId())).collect(Collectors.toList());
        List<Long> episodesWithTb = diagnosticsService.episodesWithTbTest(entityIdTemp);
        episodeIndexListToSendSms = episodeIndexListToSendSms.stream().filter(e -> !episodesWithTb.contains(Long.parseLong(e.getId()))).collect(Collectors.toList());
        Map<String, String> templateParameters = Collections.singletonMap(NotificationConstants.TEMPLATE_PREFIX, NotificationConstants.NIKSHAY_PREFIX);
        return getUserSmsDetails(episodeIndexListToSendSms, templateParameters);
    }

    @Override
    public List<UserSmsDetails> getEpisodesForAdherenceReminder(NotificationTriggerEvent notificationTriggerEvent, String clientId) {
        EpisodeSearchRequest episodeSearchRequest = notificationTriggerEvent.getEpisodeSearchRequest();
        List<EpisodeIndex> episodeIndexList = elasticSearch(episodeSearchRequest).getEpisodeIndexList();
        Map<String, String> templateParameters = Collections.singletonMap(NotificationConstants.TEMPLATE_PREFIX, NotificationConstants.NIKSHAY_PREFIX);
        return getUserSmsDetails(episodeIndexList, templateParameters);
    }

    @Override
    public List<UserSmsDetails> getEpisodesForFollowUp(NotificationTriggerEvent notificationTriggerEvent, String clientId) {
        LocalDate dstbIpDays = LocalDate.now().minusDays(NotificationConstants.DSTB_IP);
        LocalDate dstbCpDays = LocalDate.now().minusDays(NotificationConstants.DSTB_CP);
        LocalDate dstbIpBeforeTwoDays = dstbIpDays.plusDays(2);
        LocalDate dstbCpBeforeTwoDays = dstbCpDays.plusDays(2);
        LocalDate today = LocalDate.now();
        LocalDate twoDaysAfter = today.plusDays(2);
        EpisodeSearchRequest episodeSearchRequest = notificationTriggerEvent.getEpisodeSearchRequest();
        List<EpisodeIndex> episodeIndexList = elasticSearch(episodeSearchRequest).getEpisodeIndexList();
        List<EpisodeIndex> episodeWithExactDays = episodeIndexList.stream().filter(e -> Utils.convertStringToDateNew(e.getStartDate()).toLocalDate().equals(dstbIpDays) || Utils.convertStringToDateNew(e.getStartDate()).toLocalDate().equals(dstbCpDays)).collect(Collectors.toList());
        List<EpisodeIndex> episodeWithTwoDaysAfter = episodeIndexList.stream().filter(e -> Utils.convertStringToDateNew(e.getStartDate()).toLocalDate().equals(dstbIpBeforeTwoDays) || Utils.convertStringToDateNew(e.getStartDate()).toLocalDate().equals(dstbCpBeforeTwoDays)).collect(Collectors.toList());

        Map<String, String> templateParametersExactDays = new HashMap<String, String>() {{
            put(NotificationConstants.TEMPLATE_PREFIX, NotificationConstants.NIKSHAY_PREFIX);
            put(NotificationConstants.TEMPLATE_DATE, today.toString());
        }};
        List<UserSmsDetails> userDetailsExact = getUserSmsDetails(episodeWithExactDays, templateParametersExactDays);

        Map<String, String> templateParametersTwoDaysAfter = new HashMap<String, String>() {{
            put(NotificationConstants.TEMPLATE_PREFIX, NotificationConstants.NIKSHAY_PREFIX);
            put(NotificationConstants.TEMPLATE_DATE, twoDaysAfter.toString());
        }};
        List<UserSmsDetails> userDetailsTwoDaysAfter = getUserSmsDetails(episodeWithTwoDaysAfter, templateParametersTwoDaysAfter);

        List<UserSmsDetails> finalUserSmsDetailsList = new ArrayList<>();
        finalUserSmsDetailsList.addAll(userDetailsExact);
        finalUserSmsDetailsList.addAll(userDetailsTwoDaysAfter);
        return finalUserSmsDetailsList;
    }

    @Override
    public List<UserSmsDetails> getEpisodesForPostTreatmentReminder(NotificationTriggerEvent notificationTriggerEvent, String clientId) {
        EpisodeSearchRequest episodeSearchRequest = notificationTriggerEvent.getEpisodeSearchRequest();
        List<EpisodeIndex> episodeIndexList = elasticSearch(episodeSearchRequest).getEpisodeIndexList();
        LocalDateTime presentDay = LocalDateTime.now();
        List<EpisodeIndex> episodeIndexListToSendSms = episodeIndexList.stream().filter(e -> NotificationConstants.POST_TREATMENT_REMINDER_MONTH_LIST.contains(Duration.between(Utils.convertStringToDateNew(e.getEndDate()), presentDay).toDays())).collect(Collectors.toList());

        Map<String, String> templateParametersExactDays = new HashMap<String, String>() {{
            put(NotificationConstants.TEMPLATE_PREFIX, NotificationConstants.NIKSHAY_PREFIX);
            put(NotificationConstants.TEMPLATE_DATE, Utils.getFormattedDateNew(presentDay));
        }};
        return getUserSmsDetails(episodeIndexListToSendSms, templateParametersExactDays);
    }

    @Override
    public List<UserSmsDetails> getEpisodesForEmptyContactTracing(NotificationTriggerEvent notificationTriggerEvent, String clientId) {
        EpisodeSearchRequest searchRequest = notificationTriggerEvent.getEpisodeSearchRequest();

        List<EpisodeIndex> episodeIndexList = elasticSearch(searchRequest).getEpisodeIndexList();
        List<Long> episodeIdToFilter = episodeIndexList.stream().map(e -> Long.parseLong(e.getId())).collect(Collectors.toList());
        List<Long> episodeIdWithContactTracingAdded = nikshayService.getEpisodesWithContactTracingFilter(episodeIdToFilter);
        List<EpisodeIndex> episodeListToSendSms = episodeIndexList.stream().filter(e -> !episodeIdWithContactTracingAdded.contains(Long.parseLong(e.getId()))).collect(Collectors.toList());
        Map<String, String> templateParameters = Collections.singletonMap(NotificationConstants.TEMPLATE_PREFIX, NotificationConstants.NIKSHAY_PREFIX);
        return getUserSmsDetails(episodeListToSendSms, templateParameters);
    }

    @Override
    public EmptyBankDetailsDto getEpisodesForEmptyBankDetails(EpisodeSearchRequest episodeSearchRequest) {
        LocalDateTime presentDay = LocalDateTime.now();
        List<EpisodeIndex> episodeIndexList = elasticSearch(episodeSearchRequest).getEpisodeIndexList();
        List<EpisodeIndex> episodesListOnDiagnosisDateBasis = episodeIndexList.stream().filter(e -> e.getStageData().get(FieldConstants.EPISODE_FIELD_DIAGNOSIS_DATE) != null && NotificationConstants.BANK_DETAILS_REMINDER_DAY_LIST.contains(Duration.between(Utils.convertStringToDateNew(String.valueOf(e.getStageData().get(FieldConstants.EPISODE_FIELD_DIAGNOSIS_DATE))), presentDay).toDays())).collect(Collectors.toList());
        // used to include private patients without tests. check for enrollment day
        List<EpisodeIndex> episodesListOnEnrollmentDateBasis = episodeIndexList.stream().filter(e -> e.getTypeOfEpisode().equals(Constants.PRIVATE_EPISODE_TYPE) && e.getStageData().get(FieldConstants.EPISODE_FIELD_DIAGNOSIS_DATE) == null && NotificationConstants.BANK_DETAILS_REMINDER_DAY_LIST.contains(Duration.between(Utils.convertStringToDateNew(String.valueOf(e.getStartDate())), presentDay).toDays())).collect(Collectors.toList());
        episodesListOnEnrollmentDateBasis.addAll(episodesListOnDiagnosisDateBasis);

        List<Long> entityIdTemp =  episodesListOnEnrollmentDateBasis.stream().map(e -> Long.parseLong(e.getId())).collect(Collectors.toList());
        List<Long> episodesWithTb = diagnosticsService.episodesWithTbTest(entityIdTemp);
        episodesListOnEnrollmentDateBasis = episodesListOnEnrollmentDateBasis.stream().filter(e -> !episodesWithTb.contains(Long.parseLong(e.getId()))).collect(Collectors.toList());

        Map<String, String> templateParameters = Collections.singletonMap(NotificationConstants.TEMPLATE_PREFIX, NotificationConstants.NIKSHAY_PREFIX);
        Map<Long, UserSmsDetails> personIdSmsDetailsMap = new HashMap<>();
        episodesListOnEnrollmentDateBasis.forEach(e -> {
            Map<String, String> userParameters = new HashMap<>();
            userParameters.put(NotificationConstants.TEMPLATE_NAME, String.valueOf(e.getStageData().get(FieldConstants.PERSON_FIELD_NAME)));
            userParameters.put(NotificationConstants.TEMPLATE_ID, e.getId());
            userParameters.putAll(templateParameters);
            personIdSmsDetailsMap.put(e.getPersonId(), new UserSmsDetails(Long.parseLong(e.getId()), userParameters, String.valueOf(e.getStageData().get(FieldConstants.PERSON_PRIMARY_PHONE))));
        });

        return new EmptyBankDetailsDto(personIdSmsDetailsMap);
    }

    @Override
    public List<BeneficiaryDetailsDto> getEpisodesForBenefitCreated(EpisodeSearchRequest episodeSearchRequest) {
        List<BeneficiaryDetailsDto> beneficiaryDetailsDtoList = new ArrayList<>();
        List<EpisodeIndex> episodeIndexList =  elasticSearch(episodeSearchRequest).getEpisodeIndexList();
        episodeIndexList.forEach(e -> {
            if (StringUtils.hasText((String) e.getStageData().get(FieldConstants.PERSON_PRIMARY_PHONE))) {
                beneficiaryDetailsDtoList.add(new BeneficiaryDetailsDto(Long.valueOf(e.getId()), (String) e.getStageData().get(FieldConstants.PERSON_FIELD_NAME), (String) e.getStageData().get(FieldConstants.PERSON_PRIMARY_PHONE), e.getPersonId()));
            }
        });

        return beneficiaryDetailsDtoList;
    }

    @Override
    public List<UserSmsDetails> getEpisodesForInvalidBankDetails(EpisodeSearchRequest episodeSearchRequest) {
        List<EpisodeIndex> episodeIndexList = elasticSearch(episodeSearchRequest).getEpisodeIndexList();
        Map<String, String> templateParameters = Collections.singletonMap(NotificationConstants.TEMPLATE_PREFIX, NotificationConstants.NIKSHAY_PREFIX);
        return getUserSmsDetails(episodeIndexList, templateParameters);
    }

    
    @Override
    public void updateOnTreatmentEpisodeEndDate(EpisodeDto episodeDto, Long addedBy, String reason, String comment, LocalDateTime newEndDate) {
        LocalDateTime oldEndDate = episodeDto.getEndDate();
        if (!newEndDate.isAfter(oldEndDate))
            throw new ValidationException("End date cannot be sooner or equal to existing.");
        Long clientId = clientService.getClientByTokenOrDefault().getId();
        Map<String, Object> updateInput = new HashMap<>();
        updateInput.put(FieldConstants.EPISODE_FIELD_END_DATE, Utils.getFormattedDateNew(newEndDate));
        updateInput.put(FieldConstants.EPISODE_FIELD_ID, episodeDto.getId());
        processUpdateEpisode(clientId, updateInput);
        String comments = String.format(EpisodeLogConstants.EPISODE_END_DATE_UPDATE_COMMENT, Utils.getFormattedDateNew(oldEndDate), Utils.getFormattedDateNew(newEndDate), Constants.USERNAME_NIKSHAY, Constants.DESIGNATION_ADMIN, reason, comment);
        EpisodeLogDataRequest episodeLogDataRequest = new EpisodeLogDataRequest(episodeDto.getId(), Constants.CATEGORY_EndDate_Updated, addedBy, Constants.ACTION_END_DATE_EXTENDED, comments);
        episodeLogService.save(Collections.singletonList(episodeLogDataRequest));
    }

    @Override
    public EpisodeSearchResult episodeReports(EpisodeReportsRequest episodeReportsRequest) {
        EpisodeSearchRequest episodeSearchRequest = reportFiltersToEpisodeSearchFields.convert(episodeReportsRequest);
        episodeSearchRequest.getSearch().getMust().put(CLIENT_ID, clientService.getClientByTokenOrDefault().getId());
        return elasticSearch(episodeSearchRequest);
    }

    @Override
    public PNDtoWithNotificationData getAaroRefillReminderPNDTOsForClientId(Long clientId, String timeZone) {
        if(LocalDateTime.ofInstant(Utils.getCurrentDate().toInstant(), ZoneId.of(timeZone)).getDayOfWeek() != DayOfWeek.FRIDAY) {
            return new PNDtoWithNotificationData(new ArrayList<PushNotificationTemplateDto>(){}, false, null, true);
        }
        LocalDate today = Utils.getCurrentDateInGivenTimeZone(timeZone);
        ZonedDateTime istZonedTime = today.atStartOfDay(ZoneId.of(timeZone));
        LocalDateTime istDayStartTimeInUtc = Utils.convertDateTimeToSpecificZoneLocalDateTime(istZonedTime, ZoneOffset.UTC);
        LocalDateTime dayEndDate = istDayStartTimeInUtc.plusDays(6).minusSeconds(1);
        List<PushNotificationTemplateDto> pushNotificationTemplateDtoList = getAaroRefillPN(clientId, istDayStartTimeInUtc, dayEndDate);
        return new PNDtoWithNotificationData(pushNotificationTemplateDtoList, false, null, true);
    }

    @Override
    public PNDtoWithNotificationData getAaroRefillTodayReminderPNDTOs(Long clientId, String timeZone) {
        LocalDate today = Utils.getCurrentDateInGivenTimeZone(timeZone);
        ZonedDateTime istZonedTime = today.atStartOfDay(ZoneId.of(timeZone));
        LocalDateTime istDayStartTimeInUtc = Utils.convertDateTimeToSpecificZoneLocalDateTime(istZonedTime, ZoneOffset.UTC);
        LocalDateTime dayEndDate = istDayStartTimeInUtc.plusDays(1).minusSeconds(1);
        List<PushNotificationTemplateDto> pushNotificationTemplateDtoList = getAaroRefillPN(clientId, istDayStartTimeInUtc, dayEndDate);
        return new PNDtoWithNotificationData(pushNotificationTemplateDtoList, false, null, true);
    }

    public List<PushNotificationTemplateDto> getAaroRefillPN(Long clientId, LocalDateTime startDate, LocalDateTime endDate) {
        List<TreatmentPlanProductMap> allTreatmentPlans = treatmentPlanProductMapRepository.findAllActiveInRefillDateRange(startDate, endDate);;
        Map<Long, List<Long>> treatmentPlanRefillProductsList = new HashMap<>();
        List<Long> ProductIdsList = new ArrayList<>();
        List<PushNotificationTemplateDto> pushNotificationTemplateDtoList = new ArrayList<>();
        allTreatmentPlans.forEach(treatmentPlan -> {
            if (treatmentPlanRefillProductsList.containsKey(treatmentPlan.getTreatmentPlanId())) {
                treatmentPlanRefillProductsList.get(treatmentPlan.getTreatmentPlanId()).add(treatmentPlan.getProductId());
            } else {
                List<Long> productIds = new ArrayList<>();
                productIds.add(treatmentPlan.getProductId());
                treatmentPlanRefillProductsList.put(treatmentPlan.getTreatmentPlanId(), productIds);
            }
            if(!ProductIdsList.contains(treatmentPlan.getProductId())) {
                ProductIdsList.add(treatmentPlan.getProductId());
            }
        });

        EpisodeSearchRequest episodeSearchRequest = new EpisodeSearchRequest();
        episodeSearchRequest.getSearch().getMust().put(FieldConstants.ESD_TREATMENT_PLAN_ID, new ArrayList<>(treatmentPlanRefillProductsList.keySet()));
        episodeSearchRequest.getSearch().getMust().put(Constants.CLIENT_ID, clientId);
        episodeSearchRequest.setFieldsToShow(Arrays.asList(
                FieldConstants.EPISODE_FIELD_ID, FieldConstants.ESD_TREATMENT_PLAN_ID
        ));
        EpisodeSearchResult episodeSearchResult = elasticSearch(episodeSearchRequest);
        List<String> episodeIdList = episodeSearchResult.getEpisodeIndexList().stream().map(EpisodeIndex::getId).collect(Collectors.toList());
        Map<String, List<String>> userDeviceIdsMap = getDeviceIdList(episodeIdList);

        ProductSearchRequest productSearchRequest = new ProductSearchRequest();
        productSearchRequest.setProductIds(ProductIdsList);
        List<ProductResponse> products = dispensationService.searchProducts(productSearchRequest).getBody().getData();
        Map<Long, String> productIdNameMap = products.stream().collect(
                Collectors.toMap(ProductResponse::getProductId, ProductResponse::getProductName));

        episodeSearchResult.getEpisodeIndexList().forEach(episodeData -> {
            String episodeId = episodeData.getId();
            if (!CollectionUtils.isEmpty(userDeviceIdsMap) && userDeviceIdsMap.containsKey(episodeId)) {
                List<String> deviceIdList = userDeviceIdsMap.get(episodeId);
                Map<String, String> parameters = new HashMap<>();
                List<String> medicineNames = new ArrayList<>();
                List<Long> medicines = treatmentPlanRefillProductsList.get(Long.valueOf(String.valueOf(episodeData.getStageData().get(FieldConstants.ESD_TREATMENT_PLAN_ID))));
                medicines.forEach(medicineId -> medicineNames.add(productIdNameMap.get(medicineId)));
                String paramValue = String.join(", ", medicineNames).length() < 25
                        ? String.join(", ", medicineNames)
                        : String.valueOf(medicines.stream().count());
                parameters.put(NotificationConstants.PARAM_COUNT_OR_MEDICINE, paramValue);
                deviceIdList.forEach(deviceId ->
                        pushNotificationTemplateDtoList.add(
                                new PushNotificationTemplateDto(episodeId, deviceId, null, parameters)
                        ));
            }
        });

        return pushNotificationTemplateDtoList;
    }


    @Override
    public PNDtoWithNotificationData getAppointmentConfirmationPNDto(Long clientId, EpisodeDto episodeDto, Map<String, Object> extraData) {
        Trigger trigger = triggerRepository.getTriggerByEventName(NotificationConstants.APPOINTMENT_CONFIRMATION_PN);
        String timeZone = trigger.getTimeZone();
        String patientName = episodeDto.getStageData().get(FieldConstants.PERSON_FIELD_FIRST_NAME).toString() + " " + episodeDto.getStageData().getOrDefault(FieldConstants.PERSON_FIELD_LAST_NAME, "").toString();
        LocalDateTime appointmentDate = (LocalDateTime) extraData.get(AppointmentConstants.APPOINTMENT_DATE);
        String appointmentDateInRequiredTimeZone = Utils.convertLocalDateTimeToGivenTimeZone(appointmentDate, timeZone);
        Map<String, String> contentParameters = new HashMap<>();
        contentParameters.put(AppointmentConstants.PATIENT_NAME, patientName);
        contentParameters.put(AppointmentConstants.DOCTOR_NAME, String.valueOf(extraData.get(AppointmentConstants.DOCTOR_NAME)));
        contentParameters.put(AppointmentConstants.APPOINTMENT_DATE, appointmentDateInRequiredTimeZone);
        contentParameters.put(NotificationConstants.HERE, NotificationConstants.AARO_DOWNLOAD_LINK);
        return getPNDtoWithNotificationDataForEpisodeIds(Collections.singletonList(episodeDto.getId().toString()), contentParameters);
    }

    @Override
    public List<EpisodeDocumentResponseDto> getEpisodeDocumentDetails(EpisodeDocumentSearchRequest episodeDocumentSearchRequest) {
        Client client = clientService.getClientByTokenOrDefault();
        List<Long> episodeIds = episodeDocumentSearchRequest.getEpisodeIds();
        List<EpisodeIndex> episodes = esEpisodeService.findAllByEpisodeIdInAndClientId(episodeIds.stream().map(Object::toString).collect(Collectors.toList()), client.getId());
        List<Long> episodeIdsFromElastic = episodes.stream().map(episodeIndex -> Long.valueOf(episodeIndex.getId())).collect(Collectors.toList());
        episodeDocumentSearchRequest.setEpisodeIds(episodeIdsFromElastic);
        return getBulkEpisodeDocumentDetails(episodeDocumentSearchRequest);
    }

    @Override
    public List<EpisodeDocumentResponseDto> getEpisodeDocumentDetailsByEpisodeDocumentDetailsId(Long episodeDocumentDetailsId) {
        EpisodeDocumentDetails episodeDocumentDetails = episodeDocumentDetailsRepository.findById(episodeDocumentDetailsId).orElse(null);
        if(null == episodeDocumentDetails) {
            throw new NotFoundException("Episode document Details with id " + episodeDocumentDetailsId + " not found");
        }
        EpisodeDocument episodeDocument = episodeDocumentRepository.findById(episodeDocumentDetails.getEpisodeDocumentId()).orElse(null);
        return Arrays.asList(new EpisodeDocumentResponseDto(episodeDocument.getEpisodeId(), Arrays.asList(episodeDocument)));
    }

    @Override
    public void updateEpisodeDocumentDetails(Long episodeDocumentDetailsId, Boolean approved, Long validatedBy) {
        EpisodeDocumentDetails episodeDocumentDetails = episodeDocumentDetailsRepository.findById(episodeDocumentDetailsId).orElse(null);
        if (null == episodeDocumentDetails) {
            throw new NotFoundException("Episode Document Details with id " + episodeDocumentDetailsId + " not found");
        }
        if (null != episodeDocumentDetails.getIsApproved()) {
            throw new ValidationException("Document Details with id " +  episodeDocumentDetails + " is already updated with approval status " + episodeDocumentDetails.getIsApproved());
        }
        episodeDocumentDetails.setIsApproved(approved);
        episodeDocumentDetails.setValidatedBy(validatedBy);
        episodeDocumentDetails.setValidatedAt(LocalDateTime.now());
        episodeDocumentDetailsRepository.save(episodeDocumentDetails);
    }

    private List<EpisodeDocumentResponseDto> getBulkEpisodeDocumentDetails(EpisodeDocumentSearchRequest episodeDocumentSearchRequest) {
        Map<Long, List<EpisodeDocument>> episodeDocumentMap = new HashMap<>();
        List<EpisodeDocumentResponseDto> episodeDocumentResponseList = new ArrayList<>();
        List<Long> episodeIds = episodeDocumentSearchRequest.getEpisodeIds();
        List<EpisodeDocument> episodeDocumentList;
        if (null != episodeDocumentSearchRequest.getFromDate() && null != episodeDocumentSearchRequest.getEndDate() && null != episodeDocumentSearchRequest.getApproved()) {
            episodeDocumentList = episodeDocumentRepository.findAllByEpisodeIdAndAddedOnBetweenAndApprovedCustom(episodeIds, Utils.toLocalDateTimeWithNull(episodeDocumentSearchRequest.getFromDate(), "yyyy-MM-dd HH:mm:ss"), Utils.toLocalDateTimeWithNull(episodeDocumentSearchRequest.getEndDate(), "yyyy-MM-dd HH:mm:ss"), episodeDocumentSearchRequest.getApproved());
        } else if (null != episodeDocumentSearchRequest.getFromDate() && null != episodeDocumentSearchRequest.getEndDate() ) {
            episodeDocumentList = episodeDocumentRepository.findAllByEpisodeIdAndAddedOnBetweenCustom(episodeIds, Utils.toLocalDateTimeWithNull(episodeDocumentSearchRequest.getFromDate(), "yyyy-MM-dd HH:mm:ss"), Utils.toLocalDateTimeWithNull(episodeDocumentSearchRequest.getEndDate(), "yyyy-MM-dd HH:mm:ss"));
        }   else {
            episodeDocumentList = episodeDocumentRepository.findAllByEpisodeIdCustom(episodeIds);
        }
        episodeDocumentList = episodeDocumentList.stream().distinct().collect(Collectors.toList());
        episodeDocumentList.forEach( ed -> {
            episodeDocumentMap.putIfAbsent(ed.getEpisodeId(), new ArrayList<>());
            episodeDocumentMap.get(ed.getEpisodeId()).add(ed);
        });
        episodeDocumentMap.forEach((k,v) -> {
            episodeDocumentResponseList.add(new EpisodeDocumentResponseDto(k,v));
        });
        return episodeDocumentResponseList;
    }
}