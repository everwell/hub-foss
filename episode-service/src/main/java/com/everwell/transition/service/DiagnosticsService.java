package com.everwell.transition.service;

import com.everwell.transition.model.request.diagnostics.DiagnosticsFilterDataRequest;
import com.everwell.transition.model.response.Response;
import com.everwell.transition.model.response.diagnostics.DiagnosticsFilterDataResponse;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;

import com.everwell.transition.model.request.diagnostics.TestRequestV2;
import com.everwell.transition.model.response.Response;
import com.everwell.transition.model.response.diagnostics.AddTestResponseV2;
import com.everwell.transition.model.request.diagnostics.GetSampleIdListRequest;
import com.everwell.transition.model.response.Response;
import com.everwell.transition.model.response.diagnostics.EpisodeTestsResponse;
import com.everwell.transition.model.response.diagnostics.TestResponse;
import javassist.NotFoundException;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;


import java.util.List;
import java.util.Map;

public interface DiagnosticsService {

    ResponseEntity<Response<String>> testConnection();

    ResponseEntity<Response<AddTestResponseV2>> addTest(TestRequestV2 genericTestRequest, Long clientId);

    EpisodeTestsResponse getTestsByEpisode(Long episodeId);

    List<Long> getSampleIdListByEpisodeId(GetSampleIdListRequest sampleIdListRequest);

    TestResponse getTestById(Long testRequestId);

    Map<String, Object> editTest(Map<String, Object> requestInput);

    String deleteTest(Long testRequestId);

    ResponseEntity<Response<Map<String, Object>>> filterKPI(@RequestBody Map<String, Object> kpiFilterRequest);

    ResponseEntity<Response<Map<String, Object>>> qrCodeValidation(String qrCode);

    ResponseEntity<Response<Map<String, Object>>> generateQrPdf(@RequestBody Map<String, Object> request);
    
    ResponseEntity<Response<Map<String, Object>>> getSampleByIdOrQRCode(String by, Long sampleId, String qrCode, Boolean journeyDetails);

    ResponseEntity<Response<DiagnosticsFilterDataResponse>> filterTest(DiagnosticsFilterDataRequest diagnosticsFilterDataRequest);

    List<Long> episodesWithTbTest(List<Long> episodeIds);
}
