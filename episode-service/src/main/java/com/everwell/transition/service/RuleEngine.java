package com.everwell.transition.service;

import com.everwell.transition.model.Rule;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;

@Service
public class RuleEngine {

    public <I, O> O run(InferenceEngine<I, O> inferenceEngine, I conditionInputData, Long fromId, Boolean shouldEvaluateAll, Long clientId) {
        return run(inferenceEngine, conditionInputData, fromId, shouldEvaluateAll, new HashMap<>(), clientId);
    }

    public <I, O> O run(InferenceEngine<I, O> inferenceEngine, I conditionInputData, Long fromId, Boolean shouldEvaluateAll, Object inputDataAction, Long clientId) {
        List<Rule> rules = inferenceEngine.getRules(fromId, shouldEvaluateAll, clientId);
        return inferenceEngine.run(rules, conditionInputData, inputDataAction);
    }
}
