package com.everwell.transition.service;

import com.everwell.transition.model.request.course.MarkCourseRequest;
import com.everwell.transition.model.response.CourseResponse;
import com.everwell.transition.model.response.MarkCourseResponse;

public interface CourseService {
    CourseResponse getAllCourses(Long episodeId);
    MarkCourseResponse markCourseAsComplete(MarkCourseRequest markCourseRequest);
}
