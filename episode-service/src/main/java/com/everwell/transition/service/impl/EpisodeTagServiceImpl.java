package com.everwell.transition.service.impl;

import com.everwell.transition.model.db.EpisodeTag;
import com.everwell.transition.model.db.EpisodeTagStore;
import com.everwell.transition.model.request.episodetags.EpisodeTagBulkDeletionRequest;
import com.everwell.transition.model.request.episodetags.EpisodeTagDataRequest;
import com.everwell.transition.model.request.episodetags.EpisodeTagDeletionRequest;
import com.everwell.transition.model.request.episodetags.SearchTagsRequest;
import com.everwell.transition.model.response.EpisodeTagResponse;
import com.everwell.transition.model.response.TagsConfigResponse;
import com.everwell.transition.postconstruct.EpisodeTagStoreCreation;
import com.everwell.transition.repositories.EpisodeTagRepository;
import com.everwell.transition.service.EpisodeTagService;
import com.everwell.transition.utils.Utils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Service
public class EpisodeTagServiceImpl implements EpisodeTagService {

    @Autowired
    private EpisodeTagRepository episodeTagRepository;

    @Autowired
    private EpisodeTagStoreCreation episodeTagStoreCreation;

    @Override
    public EpisodeTagResponse getEpisodeTags(Long episodeId) {
        List<EpisodeTag> allTags = episodeTagRepository.getAllByEpisodeId(episodeId);
        List<String> stickyTags = allTags.stream().filter(EpisodeTag::getSticky).map(EpisodeTag::getTagName).collect(Collectors.toList());
        List<EpisodeTag> nonStickyTags = allTags.stream().filter(t -> !t.getSticky()).collect(Collectors.toList());
        return new EpisodeTagResponse(episodeId, nonStickyTags, stickyTags);
    }

    @Override
    public List<EpisodeTagResponse> save(List<EpisodeTagDataRequest> episodeTagRequest) {
        List<EpisodeTag> episodeTags = episodeTagRequest
            .stream()
            .map(e -> new EpisodeTag(e.getEpisodeId(), e.getTagName(), e.getTagDate(), e.getSticky()))
            .collect(Collectors.toList());
        return groupTags(episodeTagRepository.saveAll(episodeTags));
    }

    @Override
    public List<EpisodeTagResponse> search(SearchTagsRequest searchTagsRequest) {
        return groupTags(searchTagsRequest.getTagsToFilter() == null ?
            episodeTagRepository.getAllByEpisodeIdIn(searchTagsRequest.getEpisodeIdList()) :
            episodeTagRepository.getTagsWithFilters(searchTagsRequest.getEpisodeIdList(), searchTagsRequest.getTagsToFilter()));
    }

    @Override
    public void delete(EpisodeTagDeletionRequest episodeTagDeletionRequest) {
        episodeTagRepository.deleteTags(
                episodeTagDeletionRequest.getEpisodeId(),
                episodeTagDeletionRequest.getTagList(),
                episodeTagDeletionRequest.getTagDates().stream().map(Utils::toLocalDateTimeWithNull).collect(Collectors.toList())
        );
    }


    @Override
    public void deleteBulk(EpisodeTagBulkDeletionRequest episodeTagBulkDeletionRequest) {
        if (!episodeTagBulkDeletionRequest.getSticky()) {
            episodeTagRepository.deleteByEpisodeIdIsInAndTagNameIsInAndTagDateIsIn(
                    episodeTagBulkDeletionRequest.getEpisodeId(),
                    episodeTagBulkDeletionRequest.getTagList(),
                    episodeTagBulkDeletionRequest.getTagDates().stream().map(Utils::toLocalDateTimeWithNull).collect(Collectors.toList())
            );
        } else {
            if (CollectionUtils.isEmpty(episodeTagBulkDeletionRequest.getTagList())) {
                episodeTagRepository.deleteByEpisodeIdIsInAndStickyIsTrue(episodeTagBulkDeletionRequest.getEpisodeId());
            } else {
                episodeTagRepository.deleteByEpisodeIdIsInAndTagNameIsInAndStickyIsTrue(episodeTagBulkDeletionRequest.getEpisodeId(), episodeTagBulkDeletionRequest.getTagList());
            }
        }
    }

    @Override
    public List<TagsConfigResponse> getEpisodeConfig() {
        List<EpisodeTagStore> config = new ArrayList<>(episodeTagStoreCreation.getEpisodeTagStoreMap().values());
        List<TagsConfigResponse> configResponse = new ArrayList<>();
        config.forEach(s -> configResponse.add(new TagsConfigResponse(s.getTagName(), s.getTagDisplayName(), s.getTagIcon(), s.getTagDescription(), s.getTagGroup())));
        return configResponse;
    }

    @Override
    public List<EpisodeTagResponse> groupTags(List<EpisodeTag> tagList) {
        Map<Long, List<EpisodeTag>> map = new HashMap<>();
        tagList.forEach(t -> {
            map.putIfAbsent(t.getEpisodeId(), new ArrayList<>());
            map.get(t.getEpisodeId()).add(t);
        });
        List<EpisodeTagResponse> list = new ArrayList<>();
        map.forEach((k, v) -> {
            List<String> stickyTags = v.stream().filter(EpisodeTag::getSticky).map(EpisodeTag::getTagName).collect(Collectors.toList());
            List<EpisodeTag> nonStickyTags = v.stream().filter(t -> !t.getSticky()).collect(Collectors.toList());
            list.add(new EpisodeTagResponse(k, nonStickyTags, stickyTags));
        });
        return list;
    }

    public void deleteAllTagsAfterEndDate(Long episodeId, String endDate) {
        episodeTagRepository.deleteAllByEpisodeIdEqualsAndTagDateAfter(episodeId, Utils.toLocalDateTimeWithNull(endDate));
    }
}
