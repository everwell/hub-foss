package com.everwell.transition.service;

import com.everwell.transition.model.dto.EpisodeDto;
import com.everwell.transition.model.dto.notification.NotificationEventDto;
import com.everwell.transition.model.dto.notification.NotificationTriggerDto;
import com.everwell.transition.model.dto.notification.NotificationTriggerEvent;
import com.everwell.transition.model.response.EpisodeUnreadNotificationResponse;
import com.everwell.transition.model.dto.notification.UserSmsDetails;
import com.everwell.transition.model.response.Response;
import org.springframework.http.ResponseEntity;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Map;

public interface NotificationService {

    void sendPushNotificationGeneric(NotificationTriggerDto notificationTriggerDto, Long clientId, String eventName);

    void sendRegistrationOtp(Long clientId, String phoneNumber, String purposeText, String source);

    Boolean validateOtp(String phoneNumber, String otp);

    void sendAppointmentConfirmationSms(EpisodeDto episodeDto, String doctorName, LocalDateTime appointmentDate);

    void sendPushNotificationForEpisodeWithExtraData(EpisodeDto episodeDto, Map<String, Object> extraData, String eventName);

    ResponseEntity<Response<String>> updateNotification(Long notificationId, Boolean read, String status);

    ResponseEntity<Response<List<Map<String, Object>>>> getEpisodeNotifications(List<Long> episodeIdList);

    void sendSms(NotificationTriggerEvent notificationTriggerEvent, String idHeader, String eventName) throws  IllegalAccessException;
    
    ResponseEntity<Response<String>> deleteNotification(Map<String, Object> deleteUserRelationRequest);

    void notificationAndSmsEvent(NotificationEventDto notificationEventDto);

    void sendSmsToIns(List<UserSmsDetails> userSmsDetails, NotificationTriggerDto notificationTriggerDto, String eventName, Long clientId);

    ResponseEntity<Response<String>> updateNotificationBulk(List<Long> episodeIdList);

    ResponseEntity<Response<EpisodeUnreadNotificationResponse>> getUnreadEpisodeNotifications(List<Long> episodeIdList, Boolean count);

}
