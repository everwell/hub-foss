package com.everwell.transition.service;

import com.everwell.transition.model.Rule;
import com.everwell.transition.model.RuleNamespace;
import com.everwell.transition.model.db.Rules;
import com.everwell.transition.repositories.RulesRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.stream.Collectors;

public abstract class KeyValueInferenceEngine extends InferenceEngine<Map<String, Object>, Map<String, Object>> {

    @Autowired
    RulesRepository rulesRepository;

    @Qualifier("DataGateway")
    @Autowired
    AggregationService aggregationService;

    @Override
    public List<Rule> getRules(Long fromId, boolean shouldEvaluateAll, Long clientId) {
            return rulesRepository
                .findAllByRuleNamespaceAndClientId(getRuleNamespace(), clientId)
                .stream()
                .filter(r -> shouldEvaluateAll || Objects.equals(r.getFromId(), fromId))
                .map(r -> new Rule(
                    RuleNamespace.valueOf(r.getRuleNamespace()),
                    r.getId(), r.getCondition(), r.getAction(), r.getPriority(), r.getDescription())
            ).collect(Collectors.toList());
    }

    @Override
    public Map<String, Object> initializeOutputResult() {
        return new HashMap<>();
    }

    @Override
    public Map<String, Object> resolveMissingData(Map<String, Object> input, List<Rule> rules) {

        List<String> missingFields = new ArrayList<>();
        Set<String> requiredFields = new HashSet<>();
        for (Rule rule : rules) {
            requiredFields.addAll(getRequiredFields(rule));
        }
        for (String requiredField : requiredFields) {
            if (!input.containsKey(requiredField)) {
                missingFields.add(requiredField);
            }
        }
        Object entityId = input.get("Id") == null ? input.get("EntityId") : input.get("Id");
        Map<String, Object> missingFieldsData = aggregationService.resolveFields(missingFields, Long.parseLong(String.valueOf(entityId)));
        missingFieldsData.forEach(input::put);
        return input;
    }
}