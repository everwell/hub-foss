package com.everwell.transition.service;

import com.everwell.transition.model.db.TBChampionActivity;
import com.everwell.transition.model.request.AddActivityRequest;
import com.everwell.transition.model.response.TBChampionActivityResponse;

public interface TBChampionActivityService {
    TBChampionActivityResponse getActivities(Long episodeId);
    void saveActivity(AddActivityRequest addActivityRequest);
}
