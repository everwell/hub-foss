package com.everwell.transition.service;


import com.everwell.transition.model.db.TreatmentPlan;
import com.everwell.transition.model.db.TreatmentPlanProductMap;
import com.everwell.transition.model.dto.treatmentPlan.TreatmentPlanDto;
import com.everwell.transition.model.dto.treatmentPlan.TreatmentPlanProductMapDto;
import com.everwell.transition.model.request.treatmentPlan.*;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDate;
import java.util.List;
import java.util.Map;

public interface TreatmentPlanService {

    TreatmentPlan createPlan(AddTreatmentPlanRequest request, Long clientId);

    TreatmentPlan addToTreatmentPlan(AddTreatmentPlanRequest request, Long clientId, TreatmentPlan treatmentPlan);

    TreatmentPlanDto getPlanDetails(Long id, Long clientId);

    TreatmentPlanDto getPlanDetails(Long id, Long clientId, Boolean onlySos);

    Map<Long, String> getTreatmentPlanAggregatedScheduleStringMapForTreatmentPlanIds(List<Long> treatmentPlanIds, LocalDate date);

    List<TreatmentPlanProductMapDto> processAddProductMappingsForTreatmentPlan(Long treatmentPlanId, Long episodeId, Long clientId, EditTreatmentPlanRequest editTreatmentPlanRequest);

    List<TreatmentPlanProductMapDto> processDeleteProductMappingsForTreatmentPlan(Long treatmentPlanId, Long episodeId, Long clientId, DeleteProductsMappingRequest deleteProductsMappingRequest);

    List<TreatmentPlanProductMap> getMedicinesForSchedule(List<String> scheduleTime, Long treatmentPlanId);

    List<String> getDoseTimesForTreatmentPlan(Long treatmentPlanId);

    void validateMedicine(Long treatmentPlanId, List<Long> medicineIdList);

    List<TreatmentPlanProductMap> getTreatmentPlanProductMapList(Long treatmentPlanId);
}
