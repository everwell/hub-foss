package com.everwell.transition.service.impl;

import com.everwell.transition.constants.FieldConstants;
import com.everwell.transition.constants.SupportedTabConstants;
import com.everwell.transition.exceptions.NotFoundException;
import com.everwell.transition.exceptions.ValidationException;
import com.everwell.transition.model.db.*;
import com.everwell.transition.model.dto.DiseaseStageFieldMapDto;
import com.everwell.transition.model.dto.TabPermissionDto;
import com.everwell.transition.model.response.DiseaseTemplateResponse;
import com.everwell.transition.postconstruct.EpisodeStaticTableCacheConstruction;
import com.everwell.transition.repositories.*;
import com.everwell.transition.service.DiseaseTemplateService;
import com.everwell.transition.utils.Utils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.stream.Collectors;

@Service
public class DiseaseTemplateServiceImpl implements DiseaseTemplateService {

    @Autowired
    private RegimenRepository regimenRepository;

    @Autowired
    EpisodeStaticTableCacheConstruction episodeStaticTableCacheConstruction;

    @Override
    public Map<Long, List<DiseaseStageFieldMapDto>> getDefinedStageIdKeyMappingsListMap(Set<Long> diseaseIds, List<Long> stageIds) {
        List<DiseaseStageMapping> diseaseStageMappings = getDiseaseStageMappingFromCache(diseaseIds, stageIds);
        Map<Long, Long> diseaseStageIdToStageIdMap = new HashMap<>();
        Map<Long, Long> diseaseStageIdToDiseaseTemplateIdMap = new HashMap<>();
        for(DiseaseStageMapping diseaseStageMapping : diseaseStageMappings) {
            diseaseStageIdToStageIdMap.put(diseaseStageMapping.getId(), diseaseStageMapping.getStageId());
            diseaseStageIdToDiseaseTemplateIdMap.put(diseaseStageMapping.getId(), diseaseStageMapping.getDiseaseTemplateId());
        }
        List<DiseaseStageKeyMapping> diseaseStageKeyMappingList = getDiseaseKeyMappings(diseaseStageIdToStageIdMap.keySet());
        Map<Long, Field> idFieldMap = getFieldByIds(diseaseStageKeyMappingList.stream().map(DiseaseStageKeyMapping::getFieldId).collect(Collectors.toSet()))
                .stream()
                .collect(Collectors.toMap(Field::getId, field -> field));
        Map<Long, List<DiseaseStageFieldMapDto>> stageIdToFieldListsMap = new HashMap<>();
        for(DiseaseStageKeyMapping diseaseStageKeyMapping : diseaseStageKeyMappingList) {
            if(!diseaseStageKeyMapping.isDeleted()) {
                Long stageId = diseaseStageIdToStageIdMap.get(diseaseStageKeyMapping.getDiseaseStageId());
                List<DiseaseStageFieldMapDto> fields = stageIdToFieldListsMap.get(stageId);
                if(null == fields) {
                    fields = new ArrayList<>();
                }
                Field field = idFieldMap.get(diseaseStageKeyMapping.getFieldId());
                fields.add(new DiseaseStageFieldMapDto(field.getId(), stageId, diseaseStageIdToDiseaseTemplateIdMap.get(diseaseStageKeyMapping.getDiseaseStageId()),
                        diseaseStageKeyMapping.getDiseaseStageId(), diseaseStageKeyMapping.getId(),
                        field.getKey(), field.getModule(), diseaseStageKeyMapping.isRequired(), diseaseStageKeyMapping.isDeleted(),
                        diseaseStageKeyMapping.getDefaultValue()));
                stageIdToFieldListsMap.put(stageId, fields);
            }
        }
        return stageIdToFieldListsMap;
    }

    private List<DiseaseStageKeyMapping> getDiseaseKeyMappings(Set<Long> diseaseStageIds) {
        return episodeStaticTableCacheConstruction.getDiseaseStageKeyMappingList()
                    .stream().filter(f -> diseaseStageIds.contains(f.getDiseaseStageId())).collect(Collectors.toList());
    }

    List<DiseaseStageMapping> getDiseaseStageMappingFromCache(Set<Long> diseaseIds, List<Long> stageIds) {
        List<DiseaseStageMapping> filteredMappings = new ArrayList<>();
        for (Long diseaseId : diseaseIds) {
            Set<DiseaseStageMapping> diseaseStageMappings = episodeStaticTableCacheConstruction
                    .getDiseaseStageMappingMap().get(diseaseId);
            filteredMappings.addAll(diseaseStageMappings.stream().filter(f -> stageIds.contains(f.getStageId())).collect(Collectors.toList()));
        }
        return filteredMappings;
    }

    private Map<Long, SupportedTab> getSupportedTabIdToSupportedTabMap(Map<String, Object> episodeStageData) {
        // gets the current monitoring method of an episode
        String monitoringMethod = Utils.convertObjectToStringWithEmpty(episodeStageData.get(FieldConstants.MONITORING_METHOD));
        List<SupportedTab> supportedTabList = getSupportedTabs();
        Map<Long, SupportedTab> supportedTabIdToSupportedTabMap = new HashMap<>();
        supportedTabList.forEach(supportedTab -> {
            String supportedType = StringUtils.lowerCase(supportedTab.getType());
            // if supported tab is a type of monitoring method then show it only when the episode monitoring method is equal to supported tab
            // for ex if supported tab is merm and episode monitoring method is vot then dont show merm as it is not a valid supported tab
            if (!SupportedTabConstants.SUPPORTED_TAB_TO_MONITORING_METHOD_MAP.containsKey(supportedType)
                    || (SupportedTabConstants.SUPPORTED_TAB_TO_MONITORING_METHOD_MAP.get(supportedType).equals(monitoringMethod))) {
                Long tabId = supportedTab.getId();
                supportedTabIdToSupportedTabMap.put(tabId, new SupportedTab(supportedTab.getId(), supportedTab.getType(), supportedTab.getDescription(), supportedTab.getUrlFragment()));
            }
        });
        return supportedTabIdToSupportedTabMap;
    }

    @Override
    public Map<Long, List<TabPermissionDto>> getStageToTabPermissionsListMapForStageIds(Set<Long> diseaseIds, List<Long> stageIds, Map<String, Object> episodeStageData) {
        Map<Long, SupportedTab> supportedTabIdToSupportedTabMap = getSupportedTabIdToSupportedTabMap(episodeStageData);
        Map<Long, Long> diseaseStageIdToStageIdMap = getDiseaseStageMappingFromCache(diseaseIds, stageIds)
                .stream()
                .collect(Collectors.toMap(DiseaseStageMapping::getId, DiseaseStageMapping::getStageId));
        List<TabPermission> tabPermissions = getTabPermissionByDiseaseStageIds(diseaseStageIdToStageIdMap.keySet());
        Map<Long, List<TabPermissionDto>> stageToTabPermissionsListMap = new HashMap<>();
        for (TabPermission tabPermission : tabPermissions) {
            Long stageId = diseaseStageIdToStageIdMap.get(tabPermission.getDiseaseStageMappingId());
            List<TabPermissionDto> permissionsList = stageToTabPermissionsListMap.getOrDefault(stageId, new ArrayList<>());
            SupportedTab supportedTab = supportedTabIdToSupportedTabMap.get(tabPermission.getTabId());
            if (null != supportedTab) {
                permissionsList.add(new TabPermissionDto(stageId, tabPermission.getTabId(), supportedTab.getType(), supportedTab.getUrlFragment(), tabPermission.isAdd(), tabPermission.isEdit(), tabPermission.isDelete(), tabPermission.isView(), tabPermission.getTabOrder()));
                stageToTabPermissionsListMap.put(stageId, permissionsList);
            }
        }
        Map<Long, List<TabPermissionDto>> processedStageToTabPermissionsListMap = new HashMap<>();
        for (Map.Entry<Long, List<TabPermissionDto>> stageToTabPermissionEntry : stageToTabPermissionsListMap.entrySet()) {
            List<TabPermissionDto> tabPermissionDtoList = new ArrayList<>(stageToTabPermissionEntry.getValue()
                    .stream()
                    .collect(Collectors.toMap(TabPermissionDto::getTabId, i -> i, TabPermissionDto::mergeTabPermissionsOnOR))
                    .values());
            processedStageToTabPermissionsListMap.put(stageToTabPermissionEntry.getKey(), tabPermissionDtoList);
        }
        return processedStageToTabPermissionsListMap;
    }

    @Override
    public Map<Long, DiseaseTemplate> getDiseaseIdTemplateMap(List<Long> diseaseIds) {
        return episodeStaticTableCacheConstruction.getDiseaseTemplateList()
                .stream()
                .filter(f -> diseaseIds.contains(f.getId()))
                .collect(Collectors.toMap(DiseaseTemplate::getId, dt -> dt));
    }

    List<SupportedTab> getSupportedTabs() {
        return episodeStaticTableCacheConstruction.getSupportedTabList();
    }

    List<TabPermission> getTabPermissionByDiseaseStageIds(Set<Long> diseaseStageIds) {
        return episodeStaticTableCacheConstruction.getTabPermissionList().stream().filter(f -> diseaseStageIds.contains(f.getDiseaseStageMappingId())).collect(Collectors.toList());
    }

    List<Field> getFieldByIds(Set<Long> ids) {
        return getAllFields().stream().filter(f -> ids.contains(f.getId())).collect(Collectors.toList());
    }

    @Override
    public List<Field> getAllFieldsByNameIn(List<String> fieldNames) {
        return getAllFields().stream().filter(f -> fieldNames.contains(f.getKey())).collect(Collectors.toList());
    }

    @Override
    public List<Field> getAllFields() {
        return episodeStaticTableCacheConstruction.getFieldList();
    }

    @Override
    public List<Regimen> getRegimensForDisease(Long diseaseId, final String diseaseName, final Long clientId) {
        if(null != diseaseId) {
            final Long diseaseIdConst = diseaseId;
            Optional<DiseaseTemplate> diseaseTemplate = episodeStaticTableCacheConstruction
                    .getDiseaseTemplateList()
                    .stream()
                    .filter(f -> f.getId().equals(diseaseIdConst) && f.getClientId().equals(clientId))
                    .findFirst();
            if(!diseaseTemplate.isPresent()) {
                throw new ValidationException("invalid disease id or disease can't be accessed by the current client");
            }
        } else if(null != diseaseName) {
            Optional<DiseaseTemplate> diseaseTemplate = episodeStaticTableCacheConstruction
                    .getDiseaseTemplateList()
                    .stream()
                    .filter(f -> f.getDiseaseName().equals(diseaseName) && f.getClientId().equals(clientId))
                    .findFirst();
            if(!diseaseTemplate.isPresent()) {
                throw new ValidationException("invalid disease name or disease can't be accessed by the current client");
            }
            diseaseId = diseaseTemplate.get().getId();
        }
        return regimenRepository.findAllByDiseaseId(diseaseId);
    }

    @Override
    public DiseaseTemplateResponse getDiseasesForClient(Long clientId) {
        return new DiseaseTemplateResponse(episodeStaticTableCacheConstruction.getDiseaseTemplateList().stream().filter(f -> f.getClientId().equals(clientId)).collect(Collectors.toList()));
    }

    @Override
    public DiseaseTemplate getDefaultDiseaseForClientId(Long clientId) {
        Optional<DiseaseTemplate> diseaseTemplate = episodeStaticTableCacheConstruction
                .getDiseaseTemplateList()
                .stream()
                .filter(f -> f.getClientId().equals(clientId) && f.getIsDefault())
                .findFirst();
        if (!diseaseTemplate.isPresent()) {
            throw new NotFoundException("Unable to find diseases for this client: " + clientId);
        }
        return diseaseTemplate.get();
    }
}