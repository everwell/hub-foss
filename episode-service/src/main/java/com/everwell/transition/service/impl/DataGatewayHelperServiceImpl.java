package com.everwell.transition.service.impl;

import com.everwell.transition.exceptions.NotFoundException;
import com.everwell.transition.exceptions.ValidationException;
import com.everwell.transition.model.db.Field;
import com.everwell.transition.model.response.Response;
import com.everwell.transition.service.AggregationService;
import com.everwell.transition.service.ClientService;
import com.everwell.transition.service.DiseaseTemplateService;
import com.everwell.transition.utils.Utils;
import com.fasterxml.jackson.core.type.TypeReference;
import net.minidev.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.*;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.DefaultUriBuilderFactory;
import org.springframework.web.util.UriComponentsBuilder;

import javax.annotation.PostConstruct;
import java.io.IOException;
import java.util.*;

@Service("DataGateway")
public class DataGatewayHelperServiceImpl implements AggregationService {

    private static Logger LOGGER = LoggerFactory.getLogger(DataGatewayHelperServiceImpl.class);

    @Autowired
    private DiseaseTemplateService diseaseTemplateService;

    public RestTemplate restTemplateForGateway = new RestTemplate();

    protected static HttpEntity httpEntity;

    private static String AUTH_TOKEN = "";

    private static final String AUTH_TOKEN_PREFIX = "Bearer ";

    private static final String AUTH_HEADER = "Authorization";

    @Value("${dataGateway.url}")
    public String dataGatewayURL;

    @Value("${dataGateway.username}")
    public String username;

    @Value("${dataGateway.password}")
    public String password;

    @Autowired
    ClientService clientService;


    @PostConstruct
    public void init() {
        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.add(AUTH_HEADER, AUTH_TOKEN_PREFIX + AUTH_TOKEN);
        httpHeaders.add("Accept", MediaType.APPLICATION_JSON.toString());
        httpHeaders.add("Content-Type", MediaType.APPLICATION_JSON.toString());
        httpHeaders.add("client-id", String.valueOf(clientService.getClientByTokenOrDefault().getId()));
        httpEntity = new HttpEntity<>(httpHeaders);

        DefaultUriBuilderFactory defaultUriBuilderFactory = new DefaultUriBuilderFactory();
        defaultUriBuilderFactory.setEncodingMode(DefaultUriBuilderFactory.EncodingMode.NONE);
        restTemplateForGateway.setUriTemplateHandler(defaultUriBuilderFactory);
    }

    @Override
    public void authenticate() {
        String endPoint = "/token";
        JSONObject map= new JSONObject();
        map.put("username", username);
        map.put("password", password);
        HttpHeaders headers = new HttpHeaders();
        headers.add("Accept", MediaType.APPLICATION_JSON.toString());
        headers.add("Content-Type", MediaType.APPLICATION_JSON.toString());
        httpEntity = new HttpEntity<>(map.toString(), headers);
        ResponseEntity<Map> responseEntity = restTemplateForGateway.exchange(dataGatewayURL+endPoint, HttpMethod.POST, httpEntity, Map.class );

        if(responseEntity.getStatusCode() == HttpStatus.OK) {
            AUTH_TOKEN = responseEntity.getBody().get("data").toString();
            init();
        }
    }

    @Override
    public Map<String, Object> getByEntityIdAndType(String module, Long entityId, String entityType) {
        String endPoint = "/aggregationService";
        String requestUrl = dataGatewayURL + endPoint + "/" + module + "/" + entityType;
        int retryCount = 1;
        boolean retry = false;
        Map<String, Object> response = null;
        do {
            UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(requestUrl)
                .queryParam("id", entityId.toString());
            ResponseEntity<Map> responseEntity;
            try{
                responseEntity = restTemplateForGateway.exchange(builder.toUriString(), HttpMethod.GET, httpEntity, Map.class);
                response = responseEntity.getBody();
            } catch (HttpClientErrorException ex) {
                if(ex.getStatusCode() == HttpStatus.FORBIDDEN) {
                    authenticate();
                    retry = true;
                }
            }
        } while (retry && retryCount-- > 0);
        return response;
    }

    @Override
    public Map<String, Object> resolveFields(List<String> inputFields, Long entityId) {
        List<Field> fieldEntities = diseaseTemplateService.getAllFieldsByNameIn(inputFields);
        Map<String, String> fieldToModuleMap = new HashMap<>();
        Map<String, String> fieldToEntityMap = new HashMap<>();
        for(Field field : fieldEntities) {
            fieldToModuleMap.put(field.getKey(), field.getModule());
            fieldToEntityMap.put(field.getKey(), field.getEntity());
        }
        Map<String, Map<String, List<String>>> moduleToEntityTypeFieldMap = new HashMap<>();
        for(String field : inputFields) {
            String module = fieldToModuleMap.get(field);
            String entityType = fieldToEntityMap.get(field);
            List<String> fields;
            Map<String, List<String>> entityTypeFieldMap;
            if(null != module && null != entityType) {
                if(moduleToEntityTypeFieldMap.containsKey(module)) {
                    entityTypeFieldMap = moduleToEntityTypeFieldMap.get(module);
                    if(entityTypeFieldMap.containsKey(entityType)) {
                        fields = entityTypeFieldMap.get(entityType);
                    }
                    else {
                        fields = new ArrayList<>();
                    }
                }
                else {
                    entityTypeFieldMap = new HashMap<>();
                    fields = new ArrayList<>();
                }
                fields.add(field);
                entityTypeFieldMap.put(entityType, fields);
                moduleToEntityTypeFieldMap.put(module,entityTypeFieldMap);
            }
        }
        Map<String, Object> fieldsValueMap = new HashMap<>();
        for(Map.Entry<String, Map<String, List<String>>> entry : moduleToEntityTypeFieldMap.entrySet()) {
            for(Map.Entry<String, List<String>> childEntry : entry.getValue().entrySet()) {
                Map entityData = getByEntityIdAndType(entry.getKey(), entityId, childEntry.getKey());
                if(null == entityData) {
                    throw new NotFoundException("Unable to fetch data for Entity id " + entityId + " of type " + childEntry.getKey() + " from " + entry.getKey());
                }
                for(String field : childEntry.getValue()) {
                    fieldsValueMap.put(field, entityData.get(field));
                }
            }
        }
        return fieldsValueMap;
    }

    @Override
    public <T> ResponseEntity<Response<T>> genericExchange(String url, HttpMethod method, HttpEntity<?> requestEntity, ParameterizedTypeReference typeReference) {
        ResponseEntity<Response<T>> response = null;
        return genericExchange(url, method, requestEntity, typeReference, new HashMap<>());
    }

    @Override
    public <T> ResponseEntity<Response<T>> genericExchange(String url, HttpMethod method, HttpEntity<?> requestEntity, ParameterizedTypeReference typeReference, Map<String,String> customHeaders) {
        ResponseEntity response = null;
        int retryCount = 3;
        boolean retry = false;
        do {
            try {
                if (!String.valueOf(clientService.getClientByTokenOrDefault().getId()).equals(requestEntity.getHeaders().get("client-id").get(0))) {
                    init();
                    HttpHeaders httpHeaders = getHeaders(customHeaders);
                    requestEntity = new HttpEntity<>(requestEntity.getBody(), httpHeaders);
                }
                LOGGER.info("[genericExchange] >>> " + method + " " + url);
                LOGGER.info("[genericExchange] request body: " + requestEntity);
                response = restTemplateForGateway.exchange(url, method, requestEntity, typeReference);
                LOGGER.info("[genericExchange] <<< response: " + response);
                retry = false;
            } catch (HttpClientErrorException ex) {
                if (ex.getStatusCode() == HttpStatus.FORBIDDEN) {
                    authenticate();
                    retry = true;
                    requestEntity = new HttpEntity<>(requestEntity.getBody(), httpEntity.getHeaders());
                } else {
                    try {
                        Response<T> failedResponse = Utils.jsonToObject(ex.getResponseBodyAsString(), new TypeReference<Response<T>>() {});
                        return new ResponseEntity<>(failedResponse, ex.getStatusCode());
                    } catch (IOException e) {
                        throw new ValidationException("Invalid response type received for " + url);
                    }
                }
            }
        } while (retry && retryCount-- > 0);
        return response;
    }

    @Override
    public <T> ResponseEntity<Response<T>> getExchange(String endpoint, ParameterizedTypeReference typeReference, Object pathVariable, Map<String ,Object> queryParams) {
        String requestUrl = dataGatewayURL + endpoint;
        UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(requestUrl);
        if (null != pathVariable) {
            builder.path("/" + pathVariable);
        }
        if (!CollectionUtils.isEmpty(queryParams)) {
            queryParams.forEach(builder::queryParam);
        }

        return genericExchange(builder.toUriString(), HttpMethod.GET, httpEntity, typeReference);
    }

    @Override
    public <T> ResponseEntity<Response<T>> deleteExchange(String endpoint, ParameterizedTypeReference typeReference, Long pathVariable, Map<String,Object> queryParams, Object body) {
        String requestUrl = dataGatewayURL + endpoint;
        HttpEntity<?> requestEntity = new HttpEntity<>(body, httpEntity.getHeaders());
        UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(requestUrl);
        if (pathVariable != null) {
            builder.path("/" + pathVariable);
        }
        if (!CollectionUtils.isEmpty(queryParams)) {
            queryParams.forEach(builder::queryParam);
        }
        return genericExchange(builder.toUriString(), HttpMethod.DELETE, requestEntity, typeReference);
    }

    @Override
    public <T> ResponseEntity<Response<T>> postExchange(String endpoint, Object body, ParameterizedTypeReference typeReference) {
        return postExchange(endpoint, body, typeReference, new HashMap<>());
    }

    @Override
    public <T> ResponseEntity<Response<T>> postExchange(String endpoint, Object body, ParameterizedTypeReference typeReference, Map<String,String> customHeaders ) {
        String requestUrl = dataGatewayURL + endpoint;
        HttpHeaders httpHeaders = getHeaders(customHeaders);
        HttpEntity<?> requestEntity = new HttpEntity<>(body, httpHeaders);
        UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(requestUrl);
        return genericExchange(builder.toUriString(), HttpMethod.POST, requestEntity, typeReference, customHeaders);
    }

    @Override
    public <T> ResponseEntity<Response<T>> putExchange(String endpoint, Object body, ParameterizedTypeReference typeReference) {
        String requestUrl = dataGatewayURL + endpoint;
        HttpEntity<?> requestEntity = new HttpEntity<>(body, httpEntity.getHeaders());
        UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(requestUrl);
        return genericExchange(builder.toUriString(), HttpMethod.PUT, requestEntity, typeReference);
    }

    public <T> HttpHeaders getHeaders(Map<String,String> customHeaders) {
        HttpHeaders httpHeaders = new HttpHeaders();
        httpEntity.getHeaders().forEach((key, value) -> httpHeaders.add(key, value.get(0)));
        if (!customHeaders.isEmpty())
            customHeaders.forEach((key, value) -> httpHeaders.add(key, value));
        if(customHeaders.containsKey("client-id")) {
            httpHeaders.put("client-id", Arrays.asList(customHeaders.get("client-id")));
        }
        return httpHeaders;
    }
}



