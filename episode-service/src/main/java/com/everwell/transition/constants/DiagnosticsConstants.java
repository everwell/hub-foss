package com.everwell.transition.constants;

public class DiagnosticsConstants {

    public static final String FIELD_EPISODE_ID = "entityId";
    public static final String FIELD_TEST_REQUEST_ID = "testRequestId";
    public static final String FIELD_NON_EDITABLE_STAGES = "nonEditableStages";
    public static final String INVALID_STAGE_EDIT = "Sorry, test cannot be edited for the patient stage %s";
    public static final String RESULT_AVAILABLE = "Results Available";

    public static final String FIELD_STATUS = "status";
}
