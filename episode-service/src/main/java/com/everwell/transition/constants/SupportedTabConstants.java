package com.everwell.transition.constants;

import java.util.HashMap;
import java.util.Map;

public class SupportedTabConstants {
    public static final Map<String, String> SUPPORTED_TAB_TO_MONITORING_METHOD_MAP = new HashMap<String, String>() {
        {
            put("vot_review", "VOT");
            put("merm", "MERM");
        }
    };
    public static final String ADHERENCE = "Adherence";
}
