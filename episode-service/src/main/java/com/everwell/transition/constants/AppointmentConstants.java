package com.everwell.transition.constants;

public class AppointmentConstants {
    public static final String DEFAULT_STATUS = "Pending";
    public static final String STATUS_CONFIRMED= "Confirmed";
    public static final String STATUS_ENDED = "Completed";
    public static final String STATUS_CANCELLED = "Cancelled";

    public static final String TYPE_NEW = "New";
    public static final String TYPE_FOLLOW_UP = "Follow-Up";

    public static final String SUCCESS = "Success";
    public static final String DELETE_SUCCESS = "Deleted Appointment Successfully!";
    public static final String CANCEL_SUCCESS = "Appointment Cancelled";
    public static final String END_SUCCESS = "Appointment Ended Successfully!";
    public static final String CONFIRM_SUCCESS = "Appointment Confirmed.";

    //error messages
    public static final String COULD_NOT_SAVE_APPOINTMENT_TRY_AGAIN = "Could not add appointment at Registry, try again after some time!";
    public static final String MISSING_EPISODE_ID = "EpisodeId is missing";
    public static final String MISSING_STAFF_ID = "StaffId is missing";
    public static final String WRONG_APPOINTMENT_ID = "AppointmentId is wrong";
    public static final String DELETED_APPOINTMENT_ID = "This appointment is deleted.";
    public static final String APPOINTMENT_NOT_EXIST = "Appointment does not exist.";
    public static final String SEND_APPOINTMENT_CONFIRMATION_PN = "appointmentConfirmationPN";
    public static final String DOCTOR_NAME = "doctorName";
    public static final String APPOINTMENT_DATE = "appointmentDate";
    public static final String PATIENT_NAME = "patientName";

}
