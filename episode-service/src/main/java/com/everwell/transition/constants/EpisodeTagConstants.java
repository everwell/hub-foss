package com.everwell.transition.constants;

import java.util.Arrays;
import java.util.List;

public class EpisodeTagConstants {
    public static final String NEW_ENROLLMENT = "New_Enrollment";
    public static final String END_DATE_PASSED = "End_Date_Passed";
    public static final List<String> AUTOMATIC_TAGS = Arrays.asList(EpisodeTagConstants.END_DATE_PASSED, EpisodeTagConstants.NEW_ENROLLMENT);
    public static final int NEW_ENROLLMENT_TAG_MAX_DAYS = 7;
    public static final String REFRESH_TAGS_JOB_SUCCESS = "SUCCESSFUL: Refresh Tags hangfire job ran successfully";
    public static final String REFRESH_TAGS_JOB_SUCCESS_BODY = "Tags refreshed for %d Ids";
    public static final String REFRESH_TAGS_JOB_FAILURE = "FAILED: Refresh Tags hangfire job";
    public static final String REFRESH_TAGS_JOB_ERROR_BODY = "Error in refreshing tags for %d episode Ids. Successful Id count: %d. Failed Ids are %s";
    public static final String REFRESH_TAGS_JOB_ERROR = "ERROR: Refresh Tags %s hangfire job";
    public static final String REFRESH_TAGS_JOB_FAILURE_BODY = "Exception while refreshing tags ";
}
