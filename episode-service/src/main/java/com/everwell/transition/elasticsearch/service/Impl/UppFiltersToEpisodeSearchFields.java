package com.everwell.transition.elasticsearch.service.Impl;

import com.everwell.transition.constants.FieldConstants;
import com.everwell.transition.constants.UppFieldsConstants;
import com.everwell.transition.elasticsearch.ElasticConstants;
import com.everwell.transition.exceptions.ValidationException;
import com.everwell.transition.model.dto.EpisodeUnifiedFilters;
import com.everwell.transition.model.dto.EpisodeUnifiedSearchRequest;
import com.everwell.transition.model.request.episodesearch.EpisodeSearchRequest;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;

import java.util.*;

@Component
public class UppFiltersToEpisodeSearchFields {

    public EpisodeSearchRequest convert(EpisodeUnifiedSearchRequest episodeUnifiedSearchRequest) {
        EpisodeSearchRequest searchRequest = episodeUnifiedSearchRequest.getEpisodeSearchRequest();
        EpisodeUnifiedFilters filters = episodeUnifiedSearchRequest.getEpisodeUnifiedFilters();
        filters.transformMonitoringMethod();
        if (!StringUtils.isEmpty(filters.getDateType())) {
            Map<String, String> rangeQuery = new HashMap<>();
            rangeQuery.put(ElasticConstants.ELASTIC_SEARCH_RANGE_GREATER_THAN_EQUAL, filters.getDateStart());
            rangeQuery.put(ElasticConstants.ELASTIC_SEARCH_RANGE_LOWER_THAN_EQUAL, filters.getDateEnd());
            String dateType;
            if (filters.getDateType().equalsIgnoreCase(UppFieldsConstants.FIELD_NOTIFICATION_DATE)) {
                dateType = FieldConstants.EPISODE_FIELD_DIAGNOSIS_DATE;
            } else if (filters.getDateType().equalsIgnoreCase(UppFieldsConstants.FIELD_REGISTRATION_DATE)) {
                dateType = FieldConstants.EPISODE_FIELD_CREATED_DATE;
            } else if (filters.getDateType().equalsIgnoreCase(FieldConstants.TB_TREATMENT_START_DATE)) {
                dateType = FieldConstants.EPISODE_FIELD_START_DATE;
            } else if (filters.getDateType().equalsIgnoreCase(FieldConstants.EPISODE_FIELD_TREATMENT_START_TIME_STAMP)) {
                dateType = FieldConstants.EPISODE_FIELD_TREATMENT_START_TIME_STAMP;
            } else {
                throw new ValidationException("invalid date type" + filters.getDateType());
            }
            if (null == searchRequest.getRange()) {
                searchRequest.setRange(new HashMap<>());
            }
            searchRequest.getRange().put(dateType, rangeQuery);
        }
        if (!CollectionUtils.isEmpty(filters.getMonitoringMethod())) {
            searchRequest.getSearch().getMust().put(FieldConstants.MONITORING_METHOD, filters.getMonitoringMethod());
        }
        if (!CollectionUtils.isEmpty(filters.getTypes())) {
            searchRequest.getSearch().getMust().put(FieldConstants.EPISODE_FIELD_TYPE_OF_EPISODE, filters.getTypes());
        }
        if (!CollectionUtils.isEmpty(filters.getDiseaseId())) {
            searchRequest.getSearch().getMust().put(FieldConstants.EPISODE_FIELD_DISEASE_ID, filters.getDiseaseId());
        } else if (StringUtils.hasLength(filters.getDiseaseIdOptions())) {
            searchRequest.getSearch().getMust().put(FieldConstants.EPISODE_FIELD_DISEASE_ID_OPTIONS, filters.getDiseaseIdOptions());
        }

        return searchRequest;
    }

}