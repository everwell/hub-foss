package com.everwell.transition.elasticsearch.data;

import com.everwell.transition.elasticsearch.indices.EpisodeIndex;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

import java.util.List;
import java.util.Optional;

public interface ESEpisodeRepository extends ElasticsearchRepository<EpisodeIndex, String> {

    List<EpisodeIndex> findAllByPersonIdAndClientId(Long personId, Long clientId);


    Optional<EpisodeIndex> findAllByIdAndClientId(String id, Long clientId);

    void deleteByIdAndClientId(String id, Long clientId);

    List<EpisodeIndex> findAllByPersonIdInAndClientId(List<Long> personId, Long clientId);

    List<EpisodeIndex> findAllByIdInAndClientId(List<String> episodeIdList, Long clientId);
}
