package com.everwell.transition.elasticsearch.service.Impl;

import com.everwell.transition.constants.Constants;
import com.everwell.transition.constants.FieldConstants;
import com.everwell.transition.constants.TaskListFields;
import com.everwell.transition.elasticsearch.ElasticConstants;
import com.everwell.transition.exceptions.ValidationException;
import com.everwell.transition.model.request.episodesearch.EpisodeSearchRequest;
import com.everwell.transition.model.request.tasklistfilters.EpisodeTasklistRequest;
import com.everwell.transition.model.request.tasklistfilters.TaskListFilterRequest;
import com.everwell.transition.utils.Utils;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.core.type.TypeReference;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.time.LocalDateTime;
import java.util.*;
import java.util.stream.Collectors;

@Component
public class TaskListFiltersToEpisodeSearchFields {

    public EpisodeSearchRequest transform(EpisodeTasklistRequest episodeTasklistRequest) {
        EpisodeSearchRequest searchRequest = taskListFiltersToElasticSearchRequest(episodeTasklistRequest);
        searchRequest.setSortKey(taskListFiltersSortKeyToElasticSortKey(episodeTasklistRequest.getEpisodeSearchRequest().getSortKey()));
        searchRequest.setSortDirection(episodeTasklistRequest.getEpisodeSearchRequest().getSortDirection());
        searchRequest.setPage(episodeTasklistRequest.getEpisodeSearchRequest().getPage());
        searchRequest.setSize(episodeTasklistRequest.getEpisodeSearchRequest().getSize());
        searchRequest.setFieldsToShow(episodeTasklistRequest.getEpisodeSearchRequest().getFieldsToShow());
        searchRequest.setKeywordFields(episodeTasklistRequest.getEpisodeSearchRequest().getKeywordFields());
        return searchRequest;
    }

    public String taskListFiltersSortKeyToElasticSortKey(String sortKey) {
        String episodeSortKey = FieldConstants.EPISODE_FIELD_ID;
        if (sortKey.equalsIgnoreCase(FieldConstants.PERSON_FIELD_NAME)) {
            episodeSortKey = FieldConstants.PERSON_FIELD_NAME;
        } else if (sortKey.equalsIgnoreCase(TaskListFields.TASK_LIST_FIELD_CONTACT_NUMBER)) {
           episodeSortKey = FieldConstants.EPISODE_FIELD_PHONE_NUMBER;
        } else if (sortKey.equalsIgnoreCase(TaskListFields.TASK_LIST_FIELD_LAST_CALLED_ON)) {
            episodeSortKey = FieldConstants.EPISODE_FIELD_LAST_DOSAGE;
        } else if (sortKey.equalsIgnoreCase(TaskListFields.TASK_LIST_FIELD_DIAGNOSIS_DATE)) {
            episodeSortKey = FieldConstants.EPISODE_FIELD_DIAGNOSIS_DATE;
        } else if (sortKey.equalsIgnoreCase(FieldConstants.CLIENT_TB_TREATMENT_START_DATE_FIELD)) {
            episodeSortKey = FieldConstants.EPISODE_FIELD_START_DATE;
        } else if (sortKey.equalsIgnoreCase(TaskListFields.TASK_LIST_FIELD_ADHERENCE_TECH)) {
            episodeSortKey = FieldConstants.MONITORING_METHOD;
        } else if (sortKey.equalsIgnoreCase(TaskListFields.TASK_LIST_FIELD_PATIENT_TYPE)) {
            episodeSortKey = FieldConstants.EPISODE_FIELD_TYPE_OF_EPISODE;
        }
        return episodeSortKey;
    }

    public EpisodeSearchRequest taskListFiltersToElasticSearchRequest(EpisodeTasklistRequest episodeTasklistRequest) {
        List<TaskListFilterRequest> taskListFilterRequest = episodeTasklistRequest.getTaskListFilterRequests();
        EpisodeSearchRequest searchRequest = new EpisodeSearchRequest();
        searchRequest.setSearch(episodeTasklistRequest.getEpisodeSearchRequest().getSearch());
        List<String> filterNames = taskListFilterRequest.stream().map(TaskListFilterRequest::getFilterName).collect(Collectors.toList());
        taskListFilterRequest.forEach(tl -> {
            if (tl.getFilterName().equalsIgnoreCase(TaskListFields.TASK_LIST_MONITORING_METHOD)) {
                searchRequest.getSearch().getMust().put(FieldConstants.MONITORING_METHOD, tl.getValue());
            } else if (tl.getFilterName().equalsIgnoreCase(TaskListFields.TASK_LIST_REGISTRATION_DATE_RANGE)) {
                try {
                    Range range = Utils.jsonToObject(tl.getValue(), new TypeReference<Range>() {});
                    LocalDateTime today = Utils.getCurrentDateNew();
                    LocalDateTime newDate = today.minusDays(range.getLowerBound());
                    LocalDateTime olderDate = today.minusDays(range.getUpperBound());
                    Map<String, Object> rangeSubQuery = new HashMap<>();
                    rangeSubQuery.put(ElasticConstants.ELASTIC_SEARCH_RANGE_GREATER_THAN_EQUAL, Utils.getFormattedDateNew(olderDate));
                    rangeSubQuery.put(ElasticConstants.ELASTIC_SEARCH_RANGE_LOWER_THAN_EQUAL, Utils.getFormattedDateNew(newDate));
                    searchRequest.getRange().put(FieldConstants.EPISODE_FIELD_CREATED_DATE, rangeSubQuery);
                } catch (IOException e) {
                    throw new ValidationException("[RegistrationDateRange] filter value unexpected");
                }
            } else if (tl.getFilterName().equalsIgnoreCase(TaskListFields.TASK_LIST_END_DATE_PASSED)) {
                LocalDateTime today = Utils.getCurrentDateNew();
                Map<String, Object> rangeSubQuery = new HashMap<>();
                rangeSubQuery.put(ElasticConstants.ELASTIC_SEARCH_RANGE_LOWER_THAN, Utils.getFormattedDateNew(today));
                searchRequest.getRange().put(FieldConstants.EPISODE_FIELD_END_DATE, rangeSubQuery);
            } else if (tl.getFilterName().equalsIgnoreCase(TaskListFields.TASK_LIST_DOSE_NOT_REPORTED_TODAY)) {
                LocalDateTime today = Utils.getCurrentDateNew().minusDays(1L);
                Map<String, Object> rangeSubQuery = new HashMap<>();
                rangeSubQuery.put(ElasticConstants.ELASTIC_SEARCH_RANGE_LOWER_THAN, Utils.getFormattedDateNew(today));
                searchRequest.getRange().put(FieldConstants.EPISODE_FIELD_LAST_DOSAGE, rangeSubQuery);
            } else if (tl.getFilterName().equalsIgnoreCase(TaskListFields.TASK_LIST_REFILL_DUE)) {
                Map<String, Object> rangeSubQuery = new HashMap<>();
                rangeSubQuery.put(ElasticConstants.ELASTIC_SEARCH_RANGE_GREATER_THAN_EQUAL, Utils.getFormattedDateNew(Constants.DISPENSATION_MODULE_ACTUAL_START_DATE));
                searchRequest.getRange().put(FieldConstants.EPISODE_FIELD_START_DATE, rangeSubQuery);
            } else if (tl.getFilterName().equalsIgnoreCase(TaskListFields.TASK_LIST_TREATMENT_OUTCOME)) {
                searchRequest.getSearch().getMust().put(FieldConstants.FIELD_TREATMENT_OUTCOME, tl.getValue());
            } else if (tl.getFilterName().equalsIgnoreCase(TaskListFields.TASK_LIST_MAX_DAYS_SINCE_TREATMENT_START)) {
                LocalDateTime today = Utils.getCurrentDateNew();
                LocalDateTime minDateOfEnrollment = today.minusDays(Integer.parseInt(tl.getValue()));
                Map<String, Object> rangeSubQuery = new HashMap<>();
                if(searchRequest.getRange().containsKey(FieldConstants.EPISODE_FIELD_START_DATE)) {
                    rangeSubQuery = (Map<String, Object>) searchRequest.getRange().get(FieldConstants.EPISODE_FIELD_START_DATE);
                }
                rangeSubQuery.put(ElasticConstants.ELASTIC_SEARCH_RANGE_GREATER_THAN_EQUAL, Utils.getFormattedDateNew(minDateOfEnrollment));
                searchRequest.getRange().put(FieldConstants.EPISODE_FIELD_START_DATE, rangeSubQuery);
            } else if (tl.getFilterName().equalsIgnoreCase(TaskListFields.TASK_LIST_MIN_DAYS_SINCE_TREATMENT_START) && !filterNames.contains(TaskListFields.TASK_LIST_POSITIVE_DOSING_DAYS)) {
                LocalDateTime today = Utils.getCurrentDateNew();
                LocalDateTime maxDateOfEnrollment = today.minusDays(Integer.parseInt(tl.getValue()));
                Map<String, Object> rangeSubQuery = new HashMap<>();
                if(searchRequest.getRange().containsKey(FieldConstants.EPISODE_FIELD_START_DATE)) {
                    rangeSubQuery = (Map<String, Object>) searchRequest.getRange().get(FieldConstants.EPISODE_FIELD_START_DATE);
                }
                rangeSubQuery.put(ElasticConstants.ELASTIC_SEARCH_RANGE_LOWER_THAN_EQUAL, Utils.getFormattedDateNew(maxDateOfEnrollment));
                searchRequest.getRange().put(FieldConstants.EPISODE_FIELD_START_DATE, rangeSubQuery);
            }
        });
        return searchRequest;
    }

    @Data
    @NoArgsConstructor
    @AllArgsConstructor
    static public class Range {
        @JsonProperty("LowerBound")
        int lowerBound;
        @JsonProperty("UpperBound")
        int upperBound;
    }

}
