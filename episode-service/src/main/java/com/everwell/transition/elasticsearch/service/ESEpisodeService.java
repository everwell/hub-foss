package com.everwell.transition.elasticsearch.service;

import com.everwell.transition.elasticsearch.indices.EpisodeIndex;
import com.everwell.transition.model.dto.EpisodeSearchResult;
import com.everwell.transition.model.request.episodesearch.EpisodeSearchRequest;
import org.elasticsearch.action.bulk.BulkRequest;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.action.update.UpdateRequest;
import org.elasticsearch.index.reindex.DeleteByQueryRequest;
import org.springframework.data.elasticsearch.core.query.NativeSearchQuery;

import java.util.List;
import java.util.Map;

public interface ESEpisodeService {

    EpisodeIndex save (EpisodeIndex episodeIndex);

    EpisodeIndex findByIdAndClientId (Long id, Long clientId);

    List<EpisodeIndex> findAllByPersonId(Long id, Long clientId);

    List<EpisodeIndex> findAllByPersonIdList(List<Long> personIdList, Long clientId);

    void delete(String id, Long clientId);

    EpisodeSearchResult search(EpisodeSearchRequest searchReq, Map<String, String> fieldToTableMap, boolean fetchFullDoc);

    EpisodeSearchResult collectDataFromSearchResponse(SearchResponse searchResponse);

    EpisodeSearchResult performSearch(NativeSearchQuery nativeSearchQuery);

    EpisodeSearchResult performCount(NativeSearchQuery nativeSearchQuery);

    void updateToElastic(UpdateRequest updateRequest);

    void updateBulkElastic(BulkRequest bulkRequest);

    void deleteByQuery(DeleteByQueryRequest request);

    List<EpisodeIndex> findAllByEpisodeIdInAndClientId(List<String> episodeIdList, Long clientId);
}
