import { mount, createLocalVue } from '@vue/test-utils'
import Dashboard from '@/app/Pages/dashboard/Dashboard.vue'
import Vuex from 'vuex'

const localVue = createLocalVue()

localVue.use(Vuex)
const getStore = () => {
  return new Vuex.Store({
  })
}
const $route = {
  fullpath: '/some/path',
}
describe('Dashboard.vue', () => {
  it('renders dashboard', () => {
    const wrapper = mount(Dashboard, {
      store: getStore(),
      localVue,
      stubs: ['router-view'],
      mocks: {
        $t: (key) => key,
        $route
      }
    })
  })
})
