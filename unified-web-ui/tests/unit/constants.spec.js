import { defaultEarliestEnrollment, defaultPageSize } from '../../src/constants/index'

describe('constants Tests', () => {
  it('defaultPageSize test', () => {
    expect(defaultPageSize).toEqual(20)
  })

  it('defaultEarliestEnrollment test', () => {
    expect(defaultEarliestEnrollment).toEqual(new Date('2017-05-10'))
  })
})
