import { mount, createLocalVue } from '@vue/test-utils'
import StaffDetails from '../../src/app/Pages/dashboard/StaffDetails/StaffDetails'
import Vuex from 'vuex'

const localVue = createLocalVue()
localVue.use(Vuex)
const $t = () => {}

describe('Staff Details Tests', () => {
    let store
    let actions
    beforeEach(() => {
      actions = {
        loadViewParams: jest.fn()
      }
      store = new Vuex.Store({
        modules: {
          StaffDetails: {
            namespaced: true,
            state: {
                staffDetailsLoading: true,
                currentHierarchyId: -1,
                currentHierarchyName: '',
                currentHierarchyType: '',
                currentHierarchyDisplayType: '',
                currentHierarchyCanStaffChoosePatients: false,
                subHierarchies: [],
                currentHierarchyDisplayTypePlural: '',
                currentHierarchyHaveFieldStaff: '',
                subHierarchyType: '',
                subHierarchyDisplayTypePlural: '',
                artHIVLogins: false,
                typeSpecificMessage: '',
                summary: [],
                districtSummary: '',
                stateSummary: '',
                allowedToAddStaff: false,
                isViewOnly: false,
                canHierarchyHaveStaffPopulated: false,
                breadcrumbs: [],
                columnsData: [],
                columns: []
            },
            actions
          }
        }
      })
    })

    it('calls loadOptions on load', () => {
        const wrapper = mount(StaffDetails, { store, localVue, mocks: { $t } })
        wrapper.vm.$nextTick()
        expect(actions.loadViewParams.mock.calls).toHaveLength(1)
    })
})