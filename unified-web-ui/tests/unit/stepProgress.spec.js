import {
  mount
} from '@vue/test-utils'
import stepProgress from '@/app/shared/components/stepProgress.vue'

const TestSteps = {
  1: {
    title: 'Step 1',
    url: 'http://localhost:1/'
  },
  2: {
    title: 'Step 2',
    url: 'http://localhost:2/'
  },
  3: {
    title: 'Step 3',
    url: 'http://localhost:3/'
  },
  4: {
    title: 'Step 4',
    url: 'http://localhost:4/'
  },
  5: {
    title: 'Step 5',
    url: 'http://localhost:5/'
  }
}

describe('Step progress unit test', () => {
  test('is a Vue instance', () => {
    const wrapper = mount(stepProgress)
    expect(wrapper.isVueInstance()).toBeTruthy()
  })

  test('Rendering props', () => {
    const wrapper = mount(stepProgress, {
      propsData: {
        steps: TestSteps,
        currentStep: 4
      },
      mocks: {
        $t: (key) => key
      }
    })
    expect(wrapper.props().currentStep).toBe(4)
    expect(wrapper.props().steps).toBe(TestSteps)
  })

  test('Calls onStepClick', () => {
    const onStepClick = jest.fn()
    const wrapper = mount(stepProgress, {
      propsData: {
        steps: TestSteps,
        currentStep: 4
      },
      mocks: {
        $t: (key) => key
      }
    })
    wrapper.setMethods({
      onStepClick: onStepClick
    })
    wrapper.find('li').trigger('click')
    /* eslint-disable */
    expect(onStepClick).toHaveBeenCalled
    /* eslint-enable */
  })

  test('emit event test', async () => {
    const wrapper = mount(stepProgress, {
      propsData: {
        steps: TestSteps,
        currentStep: 4
      },
      mocks: {
        $t: (key) => key
      }
    })
    wrapper.vm.$emit('event')
    wrapper.vm.$emit('event', 2)
    await wrapper.vm.$nextTick() // Wait until $emits have been handled
    // assert event has been emitted
    expect(wrapper.emitted().event).toBeTruthy()
    expect(wrapper.emitted().event.length).toBe(2)
    expect(wrapper.emitted().event[1]).toEqual([2])
  })
})
