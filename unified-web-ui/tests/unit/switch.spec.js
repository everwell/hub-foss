import { shallowMount, mount } from '@vue/test-utils'
import Switch from '@/app/shared/components/Switch.vue'

describe('Switch.vue', () => {
  it('renders props.label when passed', () => {
    const label = 'new label'
    const wrapper = shallowMount(Switch, {
      propsData: { label },
      mocks: {
        $t: () => {
          return {
            label: label,
            mocks: {
              $t: (key) => key
            }
          }
        }
      }
    })
    expect(wrapper.text()).toMatch(label)
  })

  it('renders as switched on when passed', () => {
    const value = true
    const wrapper = mount(Switch, {
      propsData: { value },
      mocks: {
        $t: (key) => key
      }
    })
    expect(wrapper.find('.switch-ellipse').classes('on-ellipse')).toBeTruthy()
    expect(wrapper.find('.switch-circle').classes('on-circle')).toBeTruthy()
  })

  it('renders as switched off when passed', () => {
    const value = false
    const wrapper = mount(Switch, {
      propsData: { value },
      mocks: {
        $t: (key) => key
      }
    })
    expect(wrapper.find('.switch-ellipse').classes('off-ellipse')).toBeTruthy()
    expect(wrapper.find('.switch-circle').classes('off-circle')).toBeTruthy()
  })

  it('renders as switched disabled on when passed', () => {
    const value = true
    const disabled = true
    const wrapper = mount(Switch, {
      propsData: { value, disabled },
      mocks: {
        $t: (key) => key
      }
    })
    expect(wrapper.find('.switch-ellipse').classes('disabled-on-ellipse')).toBeTruthy()
    expect(wrapper.find('.switch-circle').classes('off-circle')).toBeTruthy()
  })

  it('renders as switched disabled off when passed', () => {
    const value = false
    const disabled = true
    const wrapper = mount(Switch, {
      propsData: { value, disabled },
      mocks: {
        $t: (key) => key
      }
    })
    expect(wrapper.find('.switch-ellipse').classes('off-ellipse')).toBeTruthy()
    expect(wrapper.find('.switch-circle').classes('off-circle')).toBeTruthy()
  })

  it('emits event as switched on when passed', () => {
    const value = true
    const wrapper = mount(Switch, {
      propsData: { value },
      mocks: {
        $t: (key) => key
      }
    })
    wrapper.find('.switch-ellipse').trigger('click')
    expect(wrapper.emitted('input')).toHaveLength(1)
  })

  it('emits event as switched off when passed', () => {
    const value = false
    const wrapper = mount(Switch, {
      propsData: { value },
      mocks: {
        $t: (key) => key
      }
    })
    wrapper.find('.switch-ellipse').trigger('click')
    expect(wrapper.emitted('input')).toHaveLength(1)
  })

  it('does not emit event as switched disabled on when passed', () => {
    const value = true
    const disabled = true
    const wrapper = mount(Switch, {
      propsData: { value, disabled },
      mocks: {
        $t: (key) => key
      }
    })
    wrapper.find('.switch-ellipse').trigger('click')
    expect(wrapper.emitted('input')).toBeFalsy()
  })

  it('does not emit event as switched disabled off when passed', () => {
    const value = false
    const disabled = true
    const wrapper = mount(Switch, {
      propsData: { value, disabled },
      mocks: {
        $t: (key) => key
      }
    })
    wrapper.find('.switch-ellipse').trigger('click')
    expect(wrapper.emitted('input')).toBeFalsy()
  })
})
