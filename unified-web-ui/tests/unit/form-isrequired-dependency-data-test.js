const formData = {
    "Success": true,
    "Data": {
        "Form": {
            "Name": "CloseCase",
            "Title": "CloseCase",
            "Parts": [
                {
                    "FormName": "CloseCase",
                    "Name": "TreatmentInformation",
                    "Title": "",
                    "TitleKey": "",
                    "Order": 1,
                    "IsVisible": true,
                    "Id": 61,
                    "Type": "vertical-form-part",
                    "Rows": null,
                    "Columns": null,
                    "RowDataName": "TreatmentInformation",
                    "AllowRowOpen": false,
                    "AllowRowDelete": false,
                    "ListItemTitle": null,
                    "ItemDescriptionField": null,
                    "IsRepeatable": false,
                    "RecordId": null
                }
            ],
            "PartOptions": null,
            "Fields": [
                {
                    "Value": null,
                    "PartName": "TreatmentInformation",
                    "Placeholder": null,
                    "Component": "app-radio",
                    "IsDisabled": false,
                    "IsVisible": true,
                    "LabelKey": "treatment_outcome",
                    "Label": "Treatment Outcome*",
                    "Name": "TreatmentOutcome",
                    "IsHierarchySelector": false,
                    "IsRequired": true,
                    "RemoteUrl": null,
                    "Type": null,
                    "AddOn": null,
                    "Options": null,
                    "HierarchyOptions": null,
                    "OptionsWithKeyValue": [
                        {
                            "Key": "CURED",
                            "Value": "Cured"
                        },
                        {
                            "Key": "TREATMENT_FAILURE",
                            "Value": "Treatment Failure"
                        },
                        {
                            "Key": "TREATMENT_COMPLETE",
                            "Value": "Treatment Complete"
                        },
                        {
                            "Key": "NOT_EVALUATED",
                            "Value": "Not Evaluated"
                        },
                        {
                            "Key": "LOST_TO_FOLLOW_UP",
                            "Value": "Lost To Follow Up"
                        },
                        {
                            "Key": "TREATMENT_REGIMEN_CHANGED",
                            "Value": "Treatment Regimen Changed"
                        },
                        {
                            "Key": "OPTED_OUT",
                            "Value": "Opted Out"
                        },
                        {
                            "Key": "TRANSFERRED_OUT",
                            "Value": "Transferred Out"
                        },
                        {
                            "Key": "DIED",
                            "Value": "Died"
                        },
                        {
                            "Key": "OTHER",
                            "Value": "Other"
                        }
                    ],
                    "AdditionalInfo": null,
                    "DefaultVisibilty": false,
                    "Order": 0,
                    "OptionsWithLabel": null,
                    "Validations": {
                        "Or": null,
                        "And": [
                            {
                                "Type": "Required",
                                "Max": 0,
                                "Min": 0,
                                "IsBackendValidation": false,
                                "ValidationUrl": null,
                                "ShowServerError": false,
                                "ErrorTargetField": null,
                                "ValidationParams": null,
                                "ErrorMessage": "This field is required",
                                "ErrorMessageKey": "error_field_required",
                                "RequiredOnlyWhen": null,
                                "Regex": null
                            }
                        ]
                    },
                    "ResponseDataPath": null,
                    "OptionDisplayKeys": null,
                    "OptionValueKey": null,
                    "LoadImmediately": false,
                    "HierarchySelectionConfigs": null,
                    "DisabledDateConfig": null,
                    "RemoteUpdateConfig": null,
                    "RowNumber": 3,
                    "ColumnNumber": null,
                    "Id": 11155,
                    "ParentType": "PART",
                    "ParentId": 61,
                    "FieldOptions": "[{\"Key\":\"CURED\", \"Value\":\"_cured\"}, {\"Key\":\"TREATMENT_FAILURE\", \"Value\":\"_treatment_failure\"}, {\"Key\":\"TREATMENT_COMPLETE\", \"Value\":\"_treatment_complete\"}, {\"Key\":\"NOT_EVALUATED\", \"Value\":\"_not_evaluated\"},{\"Key\":\"LOST_TO_FOLLOW_UP\", \"Value\":\"_lost_to_follow_up\"},{\"Key\":\"TREATMENT_REGIMEN_CHANGED\", \"Value\":\"_treatment_regimen_changed\"},{\"Key\":\"OPTED_OUT\", \"Value\":\"_opted_out\"},{\"Key\":\"TRANSFERRED_OUT\", \"Value\":\"_transferred_out\"},{\"Key\":\"DIED\", \"Value\":\"_died\"},{\"Key\":\"OTHER\", \"Value\":\"_other\"}]",
                    "ValidationList": null,
                    "DefaultValue": null,
                    "Key": "TreatmentInformationTreatmentOutcome",
                    "HasInfo": false,
                    "InfoText": null,
                    "Config": null,
                    "ColumnWidth": 6,
                    "HasToggleButton": false
                },
                {
                    "Value": null,
                    "PartName": "TreatmentInformation",
                    "Placeholder": null,
                    "Component": "app-textarea",
                    "IsDisabled": false,
                    "IsVisible": true,
                    "LabelKey": "note_text",
                    "Label": "Add a Note",
                    "Name": "NoteText",
                    "IsHierarchySelector": false,
                    "IsRequired": false,
                    "RemoteUrl": null,
                    "Type": null,
                    "AddOn": null,
                    "Options": null,
                    "HierarchyOptions": null,
                    "OptionsWithKeyValue": null,
                    "AdditionalInfo": null,
                    "DefaultVisibilty": false,
                    "Order": 0,
                    "OptionsWithLabel": null,
                    "Validations": {
                        "Or": null,
                        "And": []
                    },
                    "ResponseDataPath": null,
                    "OptionDisplayKeys": null,
                    "OptionValueKey": null,
                    "LoadImmediately": false,
                    "HierarchySelectionConfigs": null,
                    "DisabledDateConfig": null,
                    "RemoteUpdateConfig": null,
                    "RowNumber": 4,
                    "ColumnNumber": null,
                    "Id": 11156,
                    "ParentType": "PART",
                    "ParentId": 61,
                    "FieldOptions": null,
                    "ValidationList": null,
                    "DefaultValue": null,
                    "Key": "TreatmentInformationNoteText",
                    "HasInfo": false,
                    "InfoText": null,
                    "Config": null,
                    "ColumnWidth": 6,
                    "HasToggleButton": false
                }
            ],
            "Triggers": null,
            "TriggerConfigs": null,
            "ValueDependencies": [],
            "FilterDependencies": [],
            "VisibilityDependencies": [],
            "DateConstraintDependencies": [
            ],
            "IsRequiredDependencies": [
                {
                    "Lookups": [
                        {
                            "FormPart": "TreatmentInformation",
                            "Field": "TreatmentOutcome",
                            "ExpectedValue": null,
                            "ExpectedValues": [
                                "OTHER"
                            ],
                            "IsObject": false,
                            "ObjectKeyAndValues": null,
                            "ObjectKeyAndValue": null,
                            "AttributeName": null,
                            "ConstraintName": null,
                            "RestrictedValue": null,
                            "ComparisonOperator": "EQ",
                            "AnyObjectKeyValue": false,
                            "FilteredKeys": null,
                            "LookupFormPart": "TreatmentInformation",
                            "LookupField": "TreatmentOutcome"
                        }
                    ],
                    "FormPart": "TreatmentInformation",
                    "Field": "NoteText",
                    "FormPartId": null,
                    "FieldId": null,
                    "Type": "IS_REQUIRED",
                    "IsForm": false
                }
            ],
            "RemoteUpdateConfigs": [],
            "ValuePropertyDependencies": [],
            "CompoundValueDependencies": null,
            "SaveEndpoint": "/API/Patients/CloseCase",
            "SaveText": "Close Case",
            "SaveTextKey": "close_case"
        },
        "ExistingData": {
            "PatientId": 1,
            "EndDate": null,
            "PatientName": "test patient",
            "StartDate": "2021-12-01T00:00:00"
        }
    },
    "Error": null
}

export default formData