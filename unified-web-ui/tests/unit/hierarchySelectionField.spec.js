
import { createLocalVue,mount, shallowMount } from '@vue/test-utils'
import { createMapOf } from '../../src/utils/utils'
import Vuex from 'vuex'
import FormStore from '@/app/shared/store/modules/form'
import HierarchySelectionStore from '@/app/shared/store/modules/HierarchySelectionField/index'
import flushPromises from 'flush-promises'
import Vue from 'vue'
import HierarchySelectionField from '../../src/app/shared/components/HierarchySelectionField.vue'
import Button from '@/app/shared/components/Button.vue'
import Select from '@/app/shared/components/Select.vue'
import Modal from '@/app/shared/components/Modal.vue'
import Form from '@/app/shared/components/Form.vue'
import VerticalFormPart from '@/app/shared/components/VerticalFormPart.vue'


jest.mock("../../src/app/shared/store/Api", () => ({
  ApiServerClient: {get: jest.fn(() => Promise.resolve(
    [
      {
        Has99DOTS: true,
        Has99DOTSLite: true,
        HasChildren: false,
        HasMERM: true,
        HasNONE: true,
        HasVOT: true,
        Id: 266953,
        Level: 2,
        Name: 'DemoSt1',
        Code: '23124',
        Order: 0,
        ParentId: 15317,
        Type: 'STATE',
        AdherenceTechOptions: ['99DOTS', '99DOTSLite', 'MERM', 'VOT', 'NONE']
      },
      {
        Has99DOTS: true,
        Has99DOTSLite: true,
        HasChildren: false,
        HasMERM: true,
        HasNONE: true,
        HasVOT: true,
        Id: 15318,
        Level: 2,
        Name: 'DemoState',
        Code: 'DEMO',
        Order: 0,
        ParentId: 15317,
        Type: 'STATE',
        AdherenceTechOptions: ['99DOTS', '99DOTSLite', 'MERM', 'VOT', 'NONE']
      }
    ]
  ))
}}));

const localVue = createLocalVue()

localVue.use(Vuex)
window.EventBus = new Vue()

const getStore = (actions,actions2 = {}) => {
  const formattedActions = {
    ...FormStore.actions,
    ...actions
  }
  const formattedActions2 = {
    ...HierarchySelectionStore.actions,
    ...actions2
  }
  return new Vuex.Store({
    modules: {
      Form: {
        state: FormStore.state,
        actions: formattedActions,
        mutations: FormStore.mutations,
        namespaced: true
      },
      HierarchySelectionField: {
        state: HierarchySelectionStore.state,
        actions: formattedActions2,
        mutations: HierarchySelectionStore.mutations,
        namespaced: true
      }  
    }
  })
}

const errmsg = 'all_fields_are_required'

const formWithHierarchySelector = {
  "Success": true,
  "Data": {
      "Form": {
          "Name": "SingleInputForm1",
          "Title": "_dummy_input_form",
          "Parts": [
              {
                  "FormName": "SingleInputForm1",
                  "Name": "input_form_01",
                  "Title": "",
                  "TitleKey": "input_form_01",
                  "Order": 1,
                  "IsVisible": true,
                  "Id": 119,
                  "Type": "vertical-form-part",
                  "Rows": null,
                  "Columns": null,
                  "RowDataName": "input_form_01",
                  "AllowRowOpen": false,
                  "AllowRowDelete": false,
                  "ListItemTitle": null,
                  "ItemDescriptionField": null,
                  "IsRepeatable": false,
                  "RecordId": null
              }
          ],
          "PartOptions": null,
          "Fields": [
              {
                  "Value": null,
                  "PartName": "input_form_01",
                  "Placeholder": null,
                  "Component": "app-hierarchy-selection-field",
                  "IsDisabled": false,
                  "IsVisible": true,
                  "LabelKey": "Validate_input__Required_01",
                  "Label": null,
                  "Name": "Validate_input__Required_01",
                  "IsHierarchySelector": false,
                  "IsRequired": false,
                  "RemoteUrl": null,
                  "Type": null,
                  "AddOn": null,
                  "Options": null,
                  "HierarchyOptions": null,
                  "OptionsWithKeyValue": null,
                  "AdditionalInfo": null,
                  "DefaultVisibilty": false,
                  "Order": 0,
                  "OptionsWithLabel": null,
                  "Validations": {
                      "Or": null,
                      "And": [
                      ]
                  },
                  "ResponseDataPath": null,
                  "OptionDisplayKeys": null,
                  "OptionValueKey": null,
                  "LoadImmediately": false,
                  "HierarchySelectionConfigs":  [{ "Types": ['STATE'], "Order": 1, "Level": 2, "OptionDisplayKeys": ['Name', 'Id', 'Code'], "Placeholder": null, "Label": 'Select STATE', "Options": [], "RemoteUrl": '/api/Hierarchy/GetHierarchiesByTypeParent' }],
                  "DisabledDateConfig": null,
                  "RemoteUpdateConfig": null,
                  "RowNumber": 16,
                  "ColumnNumber": null,
                  "Id": 11407,
                  "ParentType": "PART",
                  "ParentId": 119,
                  "FieldOptions": null,
                  "ValidationList": "Required",
                  "DefaultValue": null,
                  "Key": "input_form_01Validate_input__Required_01",
                  "HasInfo": false,
                  "InfoText": null,
                  "Config": null,
                  "ColumnWidth": 6,
                  "HasToggleButton": false
              }
          ],
          "Triggers": null,
          "TriggerConfigs": null,
          "ValueDependencies": [],
          "FilterDependencies": [],
          "VisibilityDependencies": [],
          "DateConstraintDependencies": [],
          "IsRequiredDependencies": [],
          "RemoteUpdateConfigs": [],
          "ValuePropertyDependencies": [],
          "CompoundValueDependencies": null,
          "SaveEndpoint": "/api/patients",
          "SaveText": null,
          "SaveTextKey": "_submit"
      },
      "ExistingData": {}
  },
  "Error": null
} 

const getNewForm = () => {
  return JSON.parse(JSON.stringify(formWithHierarchySelector))
}


describe('HierarchySelectionField.vue', () => {
  it('renders props.label when passed', () => {
    const labelKey = 'new label'
    const showLabel = true
    const wrapper = shallowMount(HierarchySelectionField, {
      store: getStore({}),
      propsData: { label: labelKey, showLabel },
      mocks: {
        $t: (key) => key
      }
    })
    expect(wrapper.text()).toMatch(labelKey)
  })
})

describe('HierarchySelectionField.vue', () => {
  it('render field when props.isEditiing is passed', async () => {
    const labelKey = 'new label'
    const isEditing = true
    const wrapper = shallowMount(HierarchySelectionField, {
      store: getStore({}),
      propsData: { isEditing, label: labelKey },
      mocks: {
        $t: (key) => key
      }
    })
    expect(wrapper.find('.ew-field-label').exists()).toBe(true)
  })
})

describe('HierarchySelectionField.vue', () => {
  it('render field when props.showLabel is passed', async () => {
    const labelKey = 'new label'
    const isEditing = true
    const showLabel = true
    const wrapper = shallowMount(HierarchySelectionField, {
      store: getStore({}),
      propsData: { isEditing,  label: labelKey, showLabel },
      mocks: {
        $t: (key) => key
      }
    })
    expect(wrapper.find('label').exists()).toBe(true)
  })
})

describe('HierarchySelectionField.vue', () => {
  it('render modal when button is clicked', async () => {
    const labelKey = 'new label'
    const isEditing = true
    const wrapper = mount(HierarchySelectionField, {
      store: getStore({}),
      propsData: { isEditing, labelKey: labelKey, label: labelKey},
      mocks: {
        $t: (key) => key
      }
    })
    wrapper.findComponent(Button).vm.$emit('click')
    await wrapper.setData({ isModalVisible: true, hasError: true })
    expect(wrapper.find('.error-message').exists()).toBe(true)
    expect(wrapper.find('.modal-backdrop').exists()).toBe(true)
    expect(wrapper.find('.btn-close').exists()).toBe(true)
    expect(wrapper.find('.modal-header').text()).toMatch(labelKey)
    expect(wrapper.find('.error-message').text()).toMatch(errmsg)

  })
})

describe('HierarchySelectionField.vue', () => {
  it('on closing model', async () => {
    const labelKey = 'new label'
    const isEditing = true
    const wrapper = mount(HierarchySelectionField, {
      store: getStore({}),
      propsData: { isEditing, label: labelKey },
      mocks: {
        $t: (key) => key
      }
    })
    wrapper.findComponent(Button).vm.$emit('click')
    await wrapper.setData({ isModalVisible: true })
    expect(wrapper.find('.modal-footer').exists()).toBe(true)
    expect(wrapper.find('.btn-close').exists()).toBe(true)
    expect(wrapper.find('.confirm-btn').exists()).toBe(true)
    wrapper.findComponent(Modal).vm.$emit('close')
    await wrapper.setData({ isModalVisible: false })
    expect(wrapper.find('.modal-backdrop').exists()).toBe(false)
    expect(wrapper.find('.btn-close').exists()).toBe(false)
    expect(wrapper.find('.btn-primary').exists()).toBe(false)
  })
})

describe('HierarchySelectionField.vue', () => {
  it('On selection', async () => {
    //const labelKey = 'new label'
    const isEditing = true
    const formData = getNewForm()
    const wrapper = mount(Form, {
      store: getStore({getFormData: async () => (
        Promise.resolve(
          formData
        )
      )
    },{} ),
    localVue,
      propsData: { isEditing },
      mocks: {
        $t: (key) => key
      }
    })
    await flushPromises()
    wrapper.findComponent(VerticalFormPart).findComponent(HierarchySelectionField).findComponent(Button).vm.$emit('click')
    await wrapper.findComponent(VerticalFormPart).findComponent(HierarchySelectionField).setData({ isModalVisible: true})
    await wrapper.findComponent(VerticalFormPart).findComponent(HierarchySelectionField).findComponent(Select).findAll('.dropdown ul li').at(1).trigger('click')
    expect(wrapper.findComponent(VerticalFormPart).findComponent(HierarchySelectionField).find('input').element.value).toBe('DemoState 15318 DEMO')
  })
})

describe('HierarchySelectionField.vue', () => {
  it('Next button is clicked', async () => {
    const formData = getNewForm()
    const hierarchySelectionConfigs = [{ Types: ['STATE'], Order: 1, Level: 2, OptionDisplayKeys: ['Name', 'Id', 'Code'], Placeholder: null, Label: 'Select STATE', Options: [], RemoteUrl: '' },
      { Types: ['DISTRICT'], Order: 1, Level: 3, OptionDisplayKeys: ['Name', 'Id', 'Code'], Placeholder: null, Label: 'Select DISTRICT', Options: [], RemoteUrl: '' },
      { Types: ['TU'], Order: 1, Level: 4, OptionDisplayKeys: ['Name', 'Id', 'Code'], Placeholder: null, Label: 'Select TU', Options: [], RemoteUrl: '' }]
    formData['Data']['Form']['Fields'][0]['HierarchySelectionConfigs'] = hierarchySelectionConfigs
    const wrapper = mount(Form, {
      store: getStore({getFormData: async () => (
        Promise.resolve(
          formData
        )
      )
    },{} ),
    localVue,
      propsData: { isEditing: true },
      mocks: {
        $t: (key) => key
      }
    })
    await flushPromises()
    wrapper.findComponent(VerticalFormPart).findComponent(HierarchySelectionField).findComponent(Button).vm.$emit('click')
    const selectedValuesMap = new Map()
    const selectedval ={
      Has99DOTS: true,
      Has99DOTSLite: true,
      HasChildren: false,
      HasMERM: true,
      HasNONE: true,
      HasVOT: true,
      Id: 266953,
      Level: 2,
      Name: 'DemoSt1',
      Code: '23124',
      Order: 0,
      ParentId: 15317,
      Type: 'STATE',
      AdherenceTechOptions: ['99DOTS', '99DOTSLite', 'MERM', 'VOT', 'NONE']
    }
    selectedValuesMap.set(0, selectedval)
    await wrapper.findComponent(VerticalFormPart).findComponent(HierarchySelectionField).setData({ isModalVisible: true, hasError: true, selectedValues: selectedValuesMap })
    await wrapper.findComponent(VerticalFormPart).findComponent(HierarchySelectionField).findComponent(Select).findAll('.dropdown ul li').at(0).trigger('click')
    expect(wrapper.findComponent(VerticalFormPart).findComponent(HierarchySelectionField).find('input').element.value).toBe('DemoSt1 266953 23124')
  })
})

describe('HierarchySelectionField.vue', () => {
  it('On Next button click', async () => {
    const isEditing = true
    const formData = getNewForm()
    const wrapper = mount(Form, {
      store: getStore({getFormData: async () => (
        Promise.resolve(
          formData
        )
      )
    },{} ),
    localVue,
      propsData: { isEditing },
      mocks: {
        $t: (key) => key
      }
    })
    await flushPromises()
    const hierarchySelectionFieldWrapper = wrapper.findComponent(VerticalFormPart).findComponent(HierarchySelectionField)
    hierarchySelectionFieldWrapper.findComponent(Button).vm.$emit('click')
    await  hierarchySelectionFieldWrapper.setData({ isModalVisible: true, hasError: true, finalValueSelected: true })
    await hierarchySelectionFieldWrapper.findComponent(Select).findAll('.dropdown ul li').at(1).trigger('click')
    expect(hierarchySelectionFieldWrapper.find('input').element.value).toBe('DemoState 15318 DEMO')
    expect(hierarchySelectionFieldWrapper.find('.confirm-btn').exists()).toBe(true)
    expect(hierarchySelectionFieldWrapper.find('.confirm-btn').props().disabled).toBe(false)
    hierarchySelectionFieldWrapper.findComponent(Modal).vm.$emit('click')
    await hierarchySelectionFieldWrapper.setData({ finalValueSelected: true })
    expect(hierarchySelectionFieldWrapper.find('.error-container').exists()).toBe(false)
    expect(hierarchySelectionFieldWrapper.find('.modal-backdrop').exists()).toBe(false)
    expect(hierarchySelectionFieldWrapper.find('#inputField').exists()).toBe(true)
  })
})
