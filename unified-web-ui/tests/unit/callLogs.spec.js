import { mount, createLocalVue } from '@vue/test-utils'
import CallLogs from '../../src/app/Pages/dashboard/patient/components/CallLogs'
import Table from '@/app/shared/components/Table'
import Vuex from 'vuex'
const localVue = createLocalVue()
localVue.use(Vuex)

describe('Call Logs page Tests', () => {
  let store
  let actions
  beforeEach(() => {
    actions = {
      getPatientCallLogs: jest.fn()
    }
    store = new Vuex.Store({
      modules: {
        CallLogs: {
          namespaced: true,
          actions
        }
      }
    })
  })

  it('call logs loaded correctly', async () => {
    var sampleResponse = { Success: true, tableList: [{ id: '1', value: 'Option 1' }, { id: '2', value: 'Option 2' }, { id: '3', value: 'Option 3' }, { id: '4', value: 'Option 4' }, { id: '5', value: 'Option 5' }, { id: '6', value: 'Option 6' }, { id: '7', value: 'Option 7' }, { id: '8', value: 'Option 8' }, { id: '9', value: 'Option 4' }, { id: '10', value: 'Option 5' }, { id: '11', value: 'Option 6' }, { id: '12', value: 'Option 7' }, { id: '13', value: 'Option 8' }], Error: null }
    actions.getPatientCallLogs.mockReturnValue(sampleResponse)
    mount(CallLogs, {
      store,
      localVue,
      mocks: {
        $t: (key) => key
      }
    })
    expect(actions.getPatientCallLogs.mock.calls).toHaveLength(1)
  })

  it('page turner event', async () => {
    var wrapper = mount(CallLogs, {
      store,
      localVue,
      mocks: {
        $t: (key) => key
      }
    })
    await wrapper.setData({ loading: false })
    await wrapper.setData({ tableList: 'test' })
    expect(wrapper.findComponent(Table).exists()).toBe(true)
  })
})
