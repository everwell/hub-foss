import { mount, createLocalVue } from '@vue/test-utils'
import Form from '@/app/shared/components/Form.vue'
import Vuex from 'vuex'
import FormStore from '@/app/shared/store/modules/form'
import flushPromises from 'flush-promises'
import GridFormPart from '@/app/shared/components/GridFormPart'
import Input from '@/app/shared/components/Input'
import Select from '@/app/shared/components/Select'
import DatePicker from '@/app/shared/components/DatePicker'
import Button from '@/app/shared/components/Button'
import Checkbox from '@/app/shared/components/CheckboxGroup'
import Vue from 'vue'
import { trackEvent } from '../../src/utils/matomoTracking'

const localVue = createLocalVue()

localVue.use(Vuex)
window.EventBus = new Vue()

jest.mock('../../src/utils/toastUtils', () => {
  return {
    __esModule: true,
    defaultToast: jest.fn(),
    ToastType: {
      Success: 'Success',
      Error: 'Error',
      Warning: 'Warning',
      Neutal: 'Neutal'
    }
  }
})

jest.mock('../../src/utils/matomoTracking', () => {
  return {
    __esModule: true,
    initializeMatomo: jest.fn(),
    trackEvent: jest.fn()
  }
})

const getStore = (actions) => {
  const formattedActions = {
    ...FormStore.actions,
    ...actions
  }

  return new Vuex.Store({
    modules: {
      Form: {
        state: FormStore.state,
        actions: formattedActions,
        mutations: FormStore.mutations,
        namespaced: true
      }
    }
  })
}

describe('Form.vue', () => {
  afterEach(() => {
    jest.clearAllMocks()
  })
  it('submit is not present if isEditing is false', async () => {
    const wrapper = mount(Form, {
      store: getStore({
        getFormData: async () => (
          Promise.resolve(
            JSON.parse('{"Success":true,"Error":null,"Data":{"Form":{"Name":"FormName","Title":"FormName","Parts":[],"PartOptions":null,"Fields":[],"Triggers":null,"TriggerConfigs":null,"ValueDependencies":[],"FilterDependencies":[],"VisibilityDependencies":[],"DateConstraintDependencies":[],"IsRequiredDependencies":[],"RemoteUpdateConfigs":null,"ValuePropertyDependencies":[],"CompoundValueDependencies":null,"SaveEndpoint":"/API/Dispensation/AddDispensation/","SaveText":"Submit","SaveTextKey":"submit"},"ExistingData":{}}}')
          )
        )
      }),
      localVue,
      propsData: { isEditing: false, name: 'AddDispensation' },
      mocks: {
        $t: (key) => key
      }
    })

    await flushPromises()
    expect(wrapper.find('.submit').exists()).toBe(false)
    expect(trackEvent.mock.calls).toHaveLength(0)
  })

  it('submit is present if isEditing is true', async () => {
    const wrapper = mount(Form, {
      store: getStore({
        getFormData: async () => (
          Promise.resolve(
            JSON.parse('{"Success":true,"Error":null,"Data":{"Form":{"Name":"FormName","Title":"FormName","Parts":[],"PartOptions":null,"Fields":[],"Triggers":null,"TriggerConfigs":null,"ValueDependencies":[],"FilterDependencies":[],"VisibilityDependencies":[],"DateConstraintDependencies":[],"IsRequiredDependencies":[],"RemoteUpdateConfigs":null,"ValuePropertyDependencies":[],"CompoundValueDependencies":null,"SaveEndpoint":"/API/Dispensation/AddDispensation/","SaveText":"Submit","SaveTextKey":"submit"},"ExistingData":{}}}')
          )
        )
      }),
      localVue,
      propsData: { isEditing: true, name: 'AddDispensation' },
      mocks: {
        $t: (key) => key
      }
    })

    await flushPromises()

    expect(wrapper.find('.submit').exists()).toBe(true)
    expect(trackEvent.mock.calls).toHaveLength(0)
  })

  it('form is loaded with required config', async () => {
    const wrapper = mount(Form, {
      store: getStore({
        getFormData: async () => (
          Promise.resolve(
            JSON.parse('{"Success":true,"Error":null,"Data":{"Form":{"Name":"FormName","Title":"FormName","Parts":[{"FormName":"FormName","Name":"PartName1","Title":"PartName 1","TitleKey":"PartName1","Order":1,"IsVisible":true,"Id":1,"Type":"grid-form-part","Rows":4,"Columns":3,"RowDataName":"PartName 1","AllowRowOpen":false,"AllowRowDelete":false,"ListItemTitle":null,"ItemDescriptionField":null,"IsRepeatable":false,"RecordId":null}],"PartOptions":null,"Fields":[{"Value":null,"PartName":"PartName1","Placeholder":null,"Component":"app-input-field","IsDisabled":false,"IsVisible":true,"LabelKey":"Field1","Label":"Field 1","Name":"Field1","IsHierarchySelector":false,"IsRequired":false,"RemoteUrl":"","Type":null,"AddOn":null,"Options":null,"HierarchyOptions":null,"OptionsWithKeyValue":null,"AdditionalInfo":null,"DefaultVisibilty":false,"Order":0,"OptionsWithLabel":null,"Validations":{"Or":null,"And":[]},"ResponseDataPath":null,"OptionDisplayKeys":null,"OptionValueKey":null,"LoadImmediately":false,"HierarchySelectionConfigs":null,"DisabledDateConfig":null,"RemoteUpdateConfig":null,"RowNumber":1,"ColumnNumber":null,"Id":1,"ParentType":"PART","ParentId":1,"FieldOptions":"","ValidationList":"","DefaultValue":"","Key":"PartName1Field1","HasInfo":false,"InfoText":null,"Config":null,"ColumnWidth":1,"HasToggleButton":false},{"Value":null,"PartName":"PartName1","Placeholder":null,"Component":"app-input-field","IsDisabled":false,"IsVisible":true,"LabelKey":"Field2","Label":"Field 2","Name":"Field2","IsHierarchySelector":false,"IsRequired":false,"RemoteUrl":"","Type":null,"AddOn":null,"Options":null,"HierarchyOptions":null,"OptionsWithKeyValue":null,"AdditionalInfo":null,"DefaultVisibilty":false,"Order":0,"OptionsWithLabel":null,"Validations":{"Or":null,"And":[]},"ResponseDataPath":null,"OptionDisplayKeys":null,"OptionValueKey":null,"LoadImmediately":false,"HierarchySelectionConfigs":null,"DisabledDateConfig":null,"RemoteUpdateConfig":null,"RowNumber":1,"ColumnNumber":null,"Id":1,"ParentType":"PART","ParentId":1,"FieldOptions":"","ValidationList":"","DefaultValue":"","Key":"PartName1Field2","HasInfo":false,"InfoText":null,"Config":null,"ColumnWidth":1,"HasToggleButton":false}],"Triggers":null,"TriggerConfigs":null,"ValueDependencies":[],"FilterDependencies":[],"VisibilityDependencies":[],"DateConstraintDependencies":[],"IsRequiredDependencies":[],"RemoteUpdateConfigs":null,"ValuePropertyDependencies":[],"CompoundValueDependencies":null,"SaveEndpoint":"/API/Dispensation/AddDispensation/","SaveText":"Submit","SaveTextKey":"submit"},"ExistingData":{}}}')
          )
        )
      }),
      localVue,
      propsData: { isEditing: true },
      mocks: {
        $t: (key) => key
      }
    })

    await flushPromises()

    expect(wrapper.findAllComponents(GridFormPart).length).toBe(1)
    expect(wrapper.findComponent(GridFormPart).findAllComponents(Input).length).toBe(2)
    expect(trackEvent.mock.calls).toHaveLength(0)
  })

  it('testing visibility dependencies for fields', async () => {
    const wrapper = mount(Form, {
      store: getStore({
        getFormData: async () => (
          Promise.resolve(
            JSON.parse('{"Success":true,"Error":null,"Data":{"Form":{"Name":"FormName","Title":"FormName","Parts":[{"FormName":"FormName","Name":"PartName1","Title":"PartName 1","TitleKey":"PartName1","Order":1,"IsVisible":true,"Id":1,"Type":"grid-form-part","Rows":4,"Columns":3,"RowDataName":"PartName 1","AllowRowOpen":false,"AllowRowDelete":false,"ListItemTitle":null,"ItemDescriptionField":null,"IsRepeatable":false,"RecordId":null}],"PartOptions":null,"Fields":[{"Value":null,"PartName":"PartName1","Placeholder":null,"Component":"app-input-field","IsDisabled":false,"IsVisible":true,"LabelKey":"Field1","Label":"Field 1","Name":"Field1","IsHierarchySelector":false,"IsRequired":false,"RemoteUrl":"","Type":null,"AddOn":null,"Options":null,"HierarchyOptions":null,"OptionsWithKeyValue":null,"AdditionalInfo":null,"DefaultVisibilty":false,"Order":0,"OptionsWithLabel":null,"Validations":{"Or":null,"And":[]},"ResponseDataPath":null,"OptionDisplayKeys":null,"OptionValueKey":null,"LoadImmediately":false,"HierarchySelectionConfigs":null,"DisabledDateConfig":null,"RemoteUpdateConfig":null,"RowNumber":1,"ColumnNumber":null,"Id":1,"ParentType":"PART","ParentId":1,"FieldOptions":"","ValidationList":"","DefaultValue":"","Key":"PartName1Field1","HasInfo":false,"InfoText":null,"Config":null,"ColumnWidth":1,"HasToggleButton":false},{"Value":null,"PartName":"PartName1","Placeholder":null,"Component":"app-input-field","IsDisabled":false,"IsVisible":false,"LabelKey":"Field2","Label":"Field 2","Name":"Field2","IsHierarchySelector":false,"IsRequired":false,"RemoteUrl":"","Type":null,"AddOn":null,"Options":null,"HierarchyOptions":null,"OptionsWithKeyValue":null,"AdditionalInfo":null,"DefaultVisibilty":false,"Order":0,"OptionsWithLabel":null,"Validations":{"Or":null,"And":[]},"ResponseDataPath":null,"OptionDisplayKeys":null,"OptionValueKey":null,"LoadImmediately":false,"HierarchySelectionConfigs":null,"DisabledDateConfig":null,"RemoteUpdateConfig":null,"RowNumber":1,"ColumnNumber":null,"Id":1,"ParentType":"PART","ParentId":1,"FieldOptions":"","ValidationList":"","DefaultValue":"","Key":"PartName1Field2","HasInfo":false,"InfoText":null,"Config":null,"ColumnWidth":1,"HasToggleButton":false}],"Triggers":null,"TriggerConfigs":null,"ValueDependencies":[],"FilterDependencies":[],"VisibilityDependencies":[{"Lookups":[{"FormPart":"PartName1","Field":"Field1","ExpectedValues":["ABC"]}],"FormPart":"PartName1","Field":"Field2","FormPartId":null,"FieldId":null,"Type":"VISIBILITY","IsForm":false}],"DateConstraintDependencies":[],"IsRequiredDependencies":[],"RemoteUpdateConfigs":null,"ValuePropertyDependencies":[],"CompoundValueDependencies":null,"SaveEndpoint":"/API/Dispensation/AddDispensation/","SaveText":"Submit","SaveTextKey":"submit"},"ExistingData":{}}}')
          )
        )
      }),
      localVue,
      mocks: {
        $t: (key) => key
      }
    })

    await flushPromises()

    expect(wrapper.findComponent(GridFormPart).findAllComponents(Input).length).toBe(1)

    const input = wrapper.findAll('input').at(0)
    await input.setValue('ABC')
    await input.trigger('change')

    expect(wrapper.findComponent(GridFormPart).findAllComponents(Input).length).toBe(2)

    await input.setValue('AB')
    await input.trigger('change')

    expect(wrapper.findComponent(GridFormPart).findAllComponents(Input).length).toBe(1)
    expect(trackEvent.mock.calls).toHaveLength(0)
  })

  it('testing visibility dependencies for select field', async () => {
    const wrapper = mount(Form, {
      store: getStore({
        getFormData: async () => (
          Promise.resolve(
            JSON.parse('{"Success":true,"Error":null,"Data":{"Form":{"Name":"FormName","Title":"FormName","Parts":[{"FormName":"FormName","Name":"PartName1","Title":"PartName 1","TitleKey":"PartName1","Order":1,"IsVisible":true,"Id":1,"Type":"grid-form-part","Rows":4,"Columns":3,"RowDataName":"PartName 1","AllowRowOpen":false,"AllowRowDelete":false,"ListItemTitle":null,"ItemDescriptionField":null,"IsRepeatable":false,"RecordId":null}],"PartOptions":null,"Fields":[{"Value":null,"PartName":"PartName1","Placeholder":null,"Component":"app-select","IsDisabled":false,"IsVisible":true,"LabelKey":"Field1","Label":"Field 1","Name":"Field1","IsHierarchySelector":false,"IsRequired":false,"RemoteUrl":"","Type":null,"AddOn":null,"Options":null,"HierarchyOptions":null,"OptionsWithKeyValue":[{"Value":"Value 1","Key":"Key1"},{"Value":"Value 2","Key":"Key2"}],"AdditionalInfo":null,"DefaultVisibilty":false,"Order":0,"OptionsWithLabel":null,"Validations":{"Or":null,"And":[{"Type":"Required","Max":0,"Min":0,"IsBackendValidation":false,"ValidationUrl":null,"ShowServerError":false,"ErrorTargetField":null,"ValidationParams":null,"ErrorMessage":"This field is required","ErrorMessageKey":"error_field_required","RequiredOnlyWhen":null,"Regex":null}]},"ResponseDataPath":null,"OptionDisplayKeys":null,"OptionValueKey":null,"LoadImmediately":false,"HierarchySelectionConfigs":null,"DisabledDateConfig":null,"RemoteUpdateConfig":null,"RowNumber":1,"ColumnNumber":null,"Id":1,"ParentType":"PART","ParentId":1,"FieldOptions":"","ValidationList":"","DefaultValue":"","Key":"PartName1Field1","HasInfo":false,"InfoText":null,"Config":null,"ColumnWidth":1,"HasToggleButton":false},{"Value":null,"PartName":"PartName1","Placeholder":null,"Component":"app-input-field","IsDisabled":false,"IsVisible":false,"LabelKey":"Field2","Label":"Field 2","Name":"Field2","IsHierarchySelector":false,"IsRequired":false,"RemoteUrl":"","Type":null,"AddOn":null,"Options":null,"HierarchyOptions":null,"OptionsWithKeyValue":null,"AdditionalInfo":null,"DefaultVisibilty":false,"Order":0,"OptionsWithLabel":null,"Validations":{"Or":null,"And":[]},"ResponseDataPath":null,"OptionDisplayKeys":null,"OptionValueKey":null,"LoadImmediately":false,"HierarchySelectionConfigs":null,"DisabledDateConfig":null,"RemoteUpdateConfig":null,"RowNumber":1,"ColumnNumber":null,"Id":2,"ParentType":"PART","ParentId":1,"FieldOptions":"","ValidationList":"","DefaultValue":"","Key":"PartName1Field2","HasInfo":false,"InfoText":null,"Config":null,"ColumnWidth":1,"HasToggleButton":false}],"Triggers":null,"TriggerConfigs":null,"ValueDependencies":[],"FilterDependencies":[],"VisibilityDependencies":[{"Lookups":[{"FormPart":"PartName1","Field":"Field1","ExpectedValues":["Key2"]}],"FormPart":"PartName1","Field":"Field2","FormPartId":null,"FieldId":null,"Type":"VISIBILITY","IsForm":false}],"DateConstraintDependencies":[],"IsRequiredDependencies":[],"RemoteUpdateConfigs":null,"ValuePropertyDependencies":[],"CompoundValueDependencies":null,"SaveEndpoint":"/API/Dispensation/AddDispensation/","SaveText":"Submit","SaveTextKey":"submit"},"ExistingData":{}}}')
          )
        )
      }),
      localVue,
      mocks: {
        $t: (key) => key
      }
    })

    await flushPromises()

    expect(wrapper.findComponent(GridFormPart).findAllComponents(Input).length).toBe(0)

    const input = wrapper.findComponent(Select)
    await input.findAll('ul li').at(1).trigger('click')

    expect(wrapper.findComponent(GridFormPart).findAllComponents(Input).length).toBe(1)

    await input.findAll('ul li').at(0).trigger('click')

    expect(wrapper.findComponent(GridFormPart).findAllComponents(Input).length).toBe(0)
  })

  it('testing visibility dependencies for parts', async () => {
    const wrapper = mount(Form, {
      store: getStore({
        getFormData: async () => (
          Promise.resolve(
            JSON.parse('{    "Success": true,    "Error": null,    "Data": {        "Form": {            "Name": "FormName",            "Title": "FormName",            "Parts": [                {                    "FormName": "FormName",                    "Name": "PartName1",                    "Title": "PartName 1",                    "TitleKey": "PartName1",                    "Order": 1,                    "IsVisible": true,                    "Id": 1,                    "Type": "grid-form-part",                    "Rows": 4,                    "Columns": 3,                    "RowDataName": "PartName 1",                    "AllowRowOpen": false,                    "AllowRowDelete": false,                    "ListItemTitle": null,                    "ItemDescriptionField": null,                    "IsRepeatable": false,                    "RecordId": null                },                {                    "FormName": "FormName",                    "Name": "PartName2",                    "Title": "PartName 2",                    "TitleKey": "PartName2",                    "Order": 1,                    "IsVisible": false,                    "Id": 1,                    "Type": "grid-form-part",                    "Rows": 4,                    "Columns": 3,                    "RowDataName": "PartName 2",                    "AllowRowOpen": false,                    "AllowRowDelete": false,                    "ListItemTitle": null,                    "ItemDescriptionField": null,                    "IsRepeatable": false,                    "RecordId": null                }            ],            "PartOptions": null,            "Fields": [                {                    "Value": null,                    "PartName": "PartName1",                    "Placeholder": null,                    "Component": "app-input-field",                    "IsDisabled": false,                    "IsVisible": true,                    "LabelKey": "Field1",                    "Label": "Field 1",                    "Name": "Field1",                    "IsHierarchySelector": false,                    "IsRequired": false,                    "RemoteUrl": "",                    "Type": null,                    "AddOn": null,                    "Options": null,                    "HierarchyOptions": null,                    "OptionsWithKeyValue": null,                    "AdditionalInfo": null,                    "DefaultVisibilty": false,                    "Order": 0,                    "OptionsWithLabel": null,                    "Validations": {                        "Or": null,                        "And": []                    },                    "ResponseDataPath": null,                    "OptionDisplayKeys": null,                    "OptionValueKey": null,                    "LoadImmediately": false,                    "HierarchySelectionConfigs": null,                    "DisabledDateConfig": null,                    "RemoteUpdateConfig": null,                    "RowNumber": 1,                    "ColumnNumber": null,                    "Id": 1,                    "ParentType": "PART",                    "ParentId": 1,                    "FieldOptions": "",                    "ValidationList": "",                    "DefaultValue": "",                    "Key": "PartName1Field1",                    "HasInfo": false,                    "InfoText": null,                    "Config": null,                    "ColumnWidth": 1,                    "HasToggleButton": false                },                {                    "Value": null,                    "PartName": "PartName2",                    "Placeholder": null,                    "Component": "app-input-field",                    "IsDisabled": false,                    "IsVisible": true,                    "LabelKey": "Field2",                    "Label": "Field 2",                    "Name": "Field2",                    "IsHierarchySelector": false,                    "IsRequired": false,                    "RemoteUrl": "",                    "Type": null,                    "AddOn": null,                    "Options": null,                    "HierarchyOptions": null,                    "OptionsWithKeyValue": null,                    "AdditionalInfo": null,                    "DefaultVisibilty": false,                    "Order": 0,                    "OptionsWithLabel": null,                    "Validations": {                        "Or": null,                        "And": []                    },                    "ResponseDataPath": null,                    "OptionDisplayKeys": null,                    "OptionValueKey": null,                    "LoadImmediately": false,                    "HierarchySelectionConfigs": null,                    "DisabledDateConfig": null,                    "RemoteUpdateConfig": null,                    "RowNumber": 1,                    "ColumnNumber": null,                    "Id": 1,                    "ParentType": "PART",                    "ParentId": 1,                    "FieldOptions": "",                    "ValidationList": "",                    "DefaultValue": "",                    "Key": "PartName2Field2",                    "HasInfo": false,                    "InfoText": null,                    "Config": null,                    "ColumnWidth": 1,                    "HasToggleButton": false                }            ],            "Triggers": null,            "TriggerConfigs": null,            "ValueDependencies": [],            "FilterDependencies": [],            "VisibilityDependencies": [                {                    "Lookups": [                        {                            "FormPart": "PartName1",                            "Field": "Field1",                            "ExpectedValues": [                                "ABCD"                            ]                        }                    ],                    "FormPart": "PartName2",                    "Field": null,                    "FormPartId": null,                    "FieldId": null,                    "Type": "VISIBILITY",                    "IsForm": false                }            ],            "DateConstraintDependencies": [],            "IsRequiredDependencies": [],            "RemoteUpdateConfigs": null,            "ValuePropertyDependencies": [],            "CompoundValueDependencies": null,            "SaveEndpoint": "/API/Dispensation/AddDispensation/",            "SaveText": "Submit",            "SaveTextKey": "submit"        },        "ExistingData": {}    }}')
          )
        )
      }),
      localVue,
      mocks: {
        $t: (key) => key
      }
    })

    await flushPromises()

    expect(wrapper.findAllComponents(GridFormPart).length).toBe(1)

    const input = wrapper.findAll('input').at(0)
    await input.setValue('ABCD')
    await input.trigger('change')

    expect(wrapper.findAllComponents(GridFormPart).length).toBe(2)

    await input.setValue('ABC')
    await input.trigger('change')

    expect(wrapper.findAllComponents(GridFormPart).length).toBe(1)
    expect(trackEvent.mock.calls).toHaveLength(0)
  })

  it('testing value dependencies for Object', async () => {
    const wrapper = mount(Form, {
      store: getStore({
        getFormData: async () => (
          Promise.resolve(
            JSON.parse('{"Success":true,"Error":null,"Data":{"Form":{"Name":"FormName","Title":"FormName","Parts":[{"FormName":"FormName","Name":"PartName1","Title":"PartName 1","TitleKey":"PartName1","Order":1,"IsVisible":true,"Id":1,"Type":"grid-form-part","Rows":4,"Columns":3,"RowDataName":"PartName 1","AllowRowOpen":false,"AllowRowDelete":false,"ListItemTitle":null,"ItemDescriptionField":null,"IsRepeatable":false,"RecordId":null}],"PartOptions":null,"Fields":[{"Value":null,"PartName":"PartName1","Placeholder":null,"Component":"app-input-field","IsDisabled":false,"IsVisible":true,"LabelKey":"Field1","Label":"Field 1","Name":"Field1","IsHierarchySelector":false,"IsRequired":false,"RemoteUrl":"","Type":null,"AddOn":null,"Options":null,"HierarchyOptions":null,"OptionsWithKeyValue":null,"AdditionalInfo":null,"DefaultVisibilty":false,"Order":0,"OptionsWithLabel":null,"Validations":{"Or":null,"And":[]},"ResponseDataPath":null,"OptionDisplayKeys":null,"OptionValueKey":null,"LoadImmediately":false,"HierarchySelectionConfigs":null,"DisabledDateConfig":null,"RemoteUpdateConfig":null,"RowNumber":1,"ColumnNumber":null,"Id":1,"ParentType":"PART","ParentId":1,"FieldOptions":"","ValidationList":"","DefaultValue":"","Key":"PartName1Field1","HasInfo":false,"InfoText":null,"Config":null,"ColumnWidth":1,"HasToggleButton":false},{"Value":null,"PartName":"PartName1","Placeholder":null,"Component":"app-input-field","IsDisabled":false,"IsVisible":true,"LabelKey":"Field2","Label":"Field 2","Name":"Field2","IsHierarchySelector":false,"IsRequired":false,"RemoteUrl":"","Type":null,"AddOn":null,"Options":null,"HierarchyOptions":null,"OptionsWithKeyValue":null,"AdditionalInfo":null,"DefaultVisibilty":false,"Order":0,"OptionsWithLabel":null,"Validations":{"Or":null,"And":[]},"ResponseDataPath":null,"OptionDisplayKeys":null,"OptionValueKey":null,"LoadImmediately":false,"HierarchySelectionConfigs":null,"DisabledDateConfig":null,"RemoteUpdateConfig":null,"RowNumber":1,"ColumnNumber":null,"Id":1,"ParentType":"PART","ParentId":1,"FieldOptions":"","ValidationList":"","DefaultValue":"","Key":"PartName1Field2","HasInfo":false,"InfoText":null,"Config":null,"ColumnWidth":1,"HasToggleButton":false}],"Triggers":null,"TriggerConfigs":null,"ValueDependencies":[{"Lookups":[{"FormPart":"PartName1","Field":"Field1","IsObject":true,"ObjectKeyAndValue":{"Value 1":"Val 1","Value 2":"Val 2"}}],"FormPart":"PartName1","Field":"Field2","FormPartId":null,"FieldId":null,"Type":"Value","IsForm":false}],"FilterDependencies":[],"VisibilityDependencies":[],"DateConstraintDependencies":[],"IsRequiredDependencies":[],"RemoteUpdateConfigs":null,"ValuePropertyDependencies":[],"CompoundValueDependencies":null,"SaveEndpoint":"/API/Dispensation/AddDispensation/","SaveText":"Submit","SaveTextKey":"submit"},"ExistingData":{}}}')
          )
        )
      }),
      localVue,
      mocks: {
        $t: (key) => key
      }
    })

    await flushPromises()

    const dependantInput = wrapper.findAllComponents(Input).at(1).find('input')
    expect(dependantInput.element.value).toBe('')

    const dependedInput = wrapper.findAllComponents(Input).at(0).find('input')
    await dependedInput.setValue('Value 1')
    await dependedInput.trigger('change')

    expect(dependantInput.element.value).toBe('Val 1')
    expect(trackEvent.mock.calls).toHaveLength(0)
  })

  it('testing value dependencies for Object for select', async () => {
    const wrapper = mount(Form, {
      store: getStore({
        getFormData: async () => (
          Promise.resolve(
            JSON.parse('{"Success":true,"Error":null,"Data":{"Form":{"Name":"FormName","Title":"FormName","Parts":[{"FormName":"FormName","Name":"PartName1","Title":"PartName 1","TitleKey":"PartName1","Order":1,"IsVisible":true,"Id":1,"Type":"grid-form-part","Rows":4,"Columns":3,"RowDataName":"PartName 1","AllowRowOpen":false,"AllowRowDelete":false,"ListItemTitle":null,"ItemDescriptionField":null,"IsRepeatable":false,"RecordId":null}],"PartOptions":null,"Fields":[{"Value":null,"PartName":"PartName1","Placeholder":null,"Component":"app-input-field","IsDisabled":false,"IsVisible":true,"LabelKey":"Field1","Label":"Field 1","Name":"Field1","IsHierarchySelector":false,"IsRequired":false,"RemoteUrl":"","Type":null,"AddOn":null,"Options":null,"HierarchyOptions":null,"OptionsWithKeyValue":null,"AdditionalInfo":null,"DefaultVisibilty":false,"Order":0,"OptionsWithLabel":null,"Validations":{"Or":null,"And":[]},"ResponseDataPath":null,"OptionDisplayKeys":null,"OptionValueKey":null,"LoadImmediately":false,"HierarchySelectionConfigs":null,"DisabledDateConfig":null,"RemoteUpdateConfig":null,"RowNumber":1,"ColumnNumber":null,"Id":1,"ParentType":"PART","ParentId":1,"FieldOptions":"","ValidationList":"","DefaultValue":"","Key":"PartName1Field1","HasInfo":false,"InfoText":null,"Config":null,"ColumnWidth":1,"HasToggleButton":false},{"Value":null,"PartName":"PartName1","Placeholder":null,"Component":"app-select","IsDisabled":false,"IsVisible":true,"LabelKey":"Field2","Label":"Field 2","Name":"Field2","IsHierarchySelector":false,"IsRequired":false,"RemoteUrl":"","Type":null,"AddOn":null,"Options":null,"HierarchyOptions":null,"OptionsWithKeyValue":[{"Value":"Value 1","Key":"Value1"},{"Value":"Value 2","Key":"Value2"}],"AdditionalInfo":null,"DefaultVisibilty":false,"Order":0,"OptionsWithLabel":null,"Validations":{"Or":null,"And":[{"Type":"Required","Max":0,"Min":0,"IsBackendValidation":false,"ValidationUrl":null,"ShowServerError":false,"ErrorTargetField":null,"ValidationParams":null,"ErrorMessage":"This field is required","ErrorMessageKey":"error_field_required","RequiredOnlyWhen":null,"Regex":null}]},"ResponseDataPath":null,"OptionDisplayKeys":null,"OptionValueKey":null,"LoadImmediately":false,"HierarchySelectionConfigs":null,"DisabledDateConfig":null,"RemoteUpdateConfig":null,"RowNumber":1,"ColumnNumber":null,"Id":2,"ParentType":"PART","ParentId":1,"FieldOptions":"","ValidationList":"","DefaultValue":"","Key":"PartName1Field2","HasInfo":false,"InfoText":null,"Config":null,"ColumnWidth":1,"HasToggleButton":false}],"Triggers":null,"TriggerConfigs":null,"ValueDependencies":[{"Lookups":[{"FormPart":"PartName1","Field":"Field2","IsObject":true,"ObjectKeyAndValue":{"Value1":"Val 1","Value2":"Val 2"}}],"FormPart":"PartName1","Field":"Field1","FormPartId":null,"FieldId":null,"Type":"Value","IsForm":false}],"FilterDependencies":[],"VisibilityDependencies":[],"DateConstraintDependencies":[],"IsRequiredDependencies":[],"RemoteUpdateConfigs":null,"ValuePropertyDependencies":[],"CompoundValueDependencies":null,"SaveEndpoint":"/API/Dispensation/AddDispensation/","SaveText":"Submit","SaveTextKey":"submit"},"ExistingData":{}}}')
          )
        )
      }),
      localVue,
      mocks: {
        $t: (key) => key
      }
    })

    await flushPromises()

    const dependantInput = wrapper.findAllComponents(Input).at(0).find('input')
    expect(dependantInput.element.value).toBe('')

    const dependedSelect = wrapper.findAllComponents(Select).at(0)
    await dependedSelect.findAll('ul li').at(0).trigger('click')

    expect(dependantInput.element.value).toBe('Val 1')
    expect(trackEvent.mock.calls).toHaveLength(0)
  })

  it('testing value dependencies for AttributeName', async () => {
    const wrapper = mount(Form, {
      store: getStore({
        getFormData: async () => (
          Promise.resolve(
            JSON.parse('{"Success":true,"Error":null,"Data":{"Form":{"Name":"FormName","Title":"FormName","Parts":[{"FormName":"FormName","Name":"PartName1","Title":"PartName 1","TitleKey":"PartName1","Order":1,"IsVisible":true,"Id":1,"Type":"grid-form-part","Rows":4,"Columns":3,"RowDataName":"PartName 1","AllowRowOpen":false,"AllowRowDelete":false,"ListItemTitle":null,"ItemDescriptionField":null,"IsRepeatable":false,"RecordId":null}],"PartOptions":null,"Fields":[{"Value":null,"PartName":"PartName1","Placeholder":null,"Component":"app-input-field","IsDisabled":false,"IsVisible":true,"LabelKey":"Field1","Label":"Field 1","Name":"Field1","IsHierarchySelector":false,"IsRequired":false,"RemoteUrl":"","Type":null,"AddOn":null,"Options":null,"HierarchyOptions":null,"OptionsWithKeyValue":null,"AdditionalInfo":null,"DefaultVisibilty":false,"Order":0,"OptionsWithLabel":null,"Validations":{"Or":null,"And":[]},"ResponseDataPath":null,"OptionDisplayKeys":null,"OptionValueKey":null,"LoadImmediately":false,"HierarchySelectionConfigs":null,"DisabledDateConfig":null,"RemoteUpdateConfig":null,"RowNumber":1,"ColumnNumber":null,"Id":1,"ParentType":"PART","ParentId":1,"FieldOptions":"","ValidationList":"","DefaultValue":"","Key":"PartName1Field1","HasInfo":false,"InfoText":null,"Config":null,"ColumnWidth":1,"HasToggleButton":false},{"Value":null,"PartName":"PartName1","Placeholder":null,"Component":"app-select","IsDisabled":false,"IsVisible":true,"LabelKey":"Field2","Label":"Field 2","Name":"Field2","IsHierarchySelector":false,"IsRequired":false,"RemoteUrl":"","Type":null,"AddOn":null,"Options":null,"HierarchyOptions":null,"OptionsWithKeyValue":[{"Value":"Value 1","Key":"Value1","Attribute":"Attr 1"},{"Value":"Value 2","Key":"Value2","Attribute":"Attr 2"}],"AdditionalInfo":null,"DefaultVisibilty":false,"Order":0,"OptionsWithLabel":null,"Validations":{"Or":null,"And":[{"Type":"Required","Max":0,"Min":0,"IsBackendValidation":false,"ValidationUrl":null,"ShowServerError":false,"ErrorTargetField":null,"ValidationParams":null,"ErrorMessage":"This field is required","ErrorMessageKey":"error_field_required","RequiredOnlyWhen":null,"Regex":null}]},"ResponseDataPath":null,"OptionDisplayKeys":null,"OptionValueKey":null,"LoadImmediately":false,"HierarchySelectionConfigs":null,"DisabledDateConfig":null,"RemoteUpdateConfig":null,"RowNumber":1,"ColumnNumber":null,"Id":2,"ParentType":"PART","ParentId":1,"FieldOptions":"","ValidationList":"","DefaultValue":"","Key":"PartName1Field2","HasInfo":false,"InfoText":null,"Config":null,"ColumnWidth":1,"HasToggleButton":false}],"Triggers":null,"TriggerConfigs":null,"ValueDependencies":[{"Lookups":[{"FormPart":"PartName1","Field":"Field2","IsObject":false,"AttributeName":"Attribute"}],"FormPart":"PartName1","Field":"Field1","FormPartId":null,"FieldId":null,"Type":"Value","IsForm":false}],"FilterDependencies":[],"VisibilityDependencies":[],"DateConstraintDependencies":[],"IsRequiredDependencies":[],"RemoteUpdateConfigs":null,"ValuePropertyDependencies":[],"CompoundValueDependencies":null,"SaveEndpoint":"/API/Dispensation/AddDispensation/","SaveText":"Submit","SaveTextKey":"submit"},"ExistingData":{}}}')
          )
        )
      }),
      localVue,
      mocks: {
        $t: (key) => key
      }
    })

    await flushPromises()

    const dependantInput = wrapper.findAllComponents(Input).at(0).find('input')
    expect(dependantInput.element.value).toBe('')

    const dependedSelect = wrapper.findAllComponents(Select).at(0)
    await dependedSelect.findAll('ul li').at(0).trigger('click')

    expect(dependantInput.element.value).toBe('Attr 1')
    expect(trackEvent.mock.calls).toHaveLength(0)
  })

  it('do not apply value dependancy if the dependant field is already filled with user value', async () => {
    const wrapper = mount(Form, {
      store: getStore({
        getFormData: async () => (
          Promise.resolve(
            JSON.parse('{"Success":true,"Error":null,"Data":{"Form":{"Name":"FormName","Title":"FormName","Parts":[{"FormName":"FormName","Name":"PartName1","Title":"PartName 1","TitleKey":"PartName1","Order":1,"IsVisible":true,"Id":1,"Type":"grid-form-part","Rows":4,"Columns":3,"RowDataName":"PartName 1","AllowRowOpen":false,"AllowRowDelete":false,"ListItemTitle":null,"ItemDescriptionField":null,"IsRepeatable":false,"RecordId":null}],"PartOptions":null,"Fields":[{"Value":null,"PartName":"PartName1","Placeholder":null,"Component":"app-input-field","IsDisabled":false,"IsVisible":true,"LabelKey":"Field1","Label":"Field 1","Name":"Field1","IsHierarchySelector":false,"IsRequired":false,"RemoteUrl":"","Type":null,"AddOn":null,"Options":null,"HierarchyOptions":null,"OptionsWithKeyValue":null,"AdditionalInfo":null,"DefaultVisibilty":false,"Order":0,"OptionsWithLabel":null,"Validations":{"Or":null,"And":[]},"ResponseDataPath":null,"OptionDisplayKeys":null,"OptionValueKey":null,"LoadImmediately":false,"HierarchySelectionConfigs":null,"DisabledDateConfig":null,"RemoteUpdateConfig":null,"RowNumber":1,"ColumnNumber":null,"Id":1,"ParentType":"PART","ParentId":1,"FieldOptions":"","ValidationList":"","DefaultValue":"","Key":"PartName1Field1","HasInfo":false,"InfoText":null,"Config":null,"ColumnWidth":1,"HasToggleButton":false},{"Value":null,"PartName":"PartName1","Placeholder":null,"Component":"app-input-field","IsDisabled":false,"IsVisible":true,"LabelKey":"Field2","Label":"Field 2","Name":"Field2","IsHierarchySelector":false,"IsRequired":false,"RemoteUrl":"","Type":null,"AddOn":null,"Options":null,"HierarchyOptions":null,"OptionsWithKeyValue":null,"AdditionalInfo":null,"DefaultVisibilty":false,"Order":0,"OptionsWithLabel":null,"Validations":{"Or":null,"And":[]},"ResponseDataPath":null,"OptionDisplayKeys":null,"OptionValueKey":null,"LoadImmediately":false,"HierarchySelectionConfigs":null,"DisabledDateConfig":null,"RemoteUpdateConfig":null,"RowNumber":1,"ColumnNumber":null,"Id":1,"ParentType":"PART","ParentId":1,"FieldOptions":"","ValidationList":"","DefaultValue":"","Key":"PartName1Field2","HasInfo":false,"InfoText":null,"Config":null,"ColumnWidth":1,"HasToggleButton":false}],"Triggers":null,"TriggerConfigs":null,"ValueDependencies":[{"Lookups":[{"FormPart":"PartName1","Field":"Field1","IsObject":true,"ObjectKeyAndValue":{"Value 1":"Val 1","Value 2":"Val 2"}}],"FormPart":"PartName1","Field":"Field2","FormPartId":null,"FieldId":null,"Type":"Value","IsForm":false}],"FilterDependencies":[],"VisibilityDependencies":[],"DateConstraintDependencies":[],"IsRequiredDependencies":[],"RemoteUpdateConfigs":null,"ValuePropertyDependencies":[],"CompoundValueDependencies":null,"SaveEndpoint":"/API/Dispensation/AddDispensation/","SaveText":"Submit","SaveTextKey":"submit"},"ExistingData":{}}}')
          )
        )
      }),
      localVue,
      mocks: {
        $t: (key) => key
      }
    })

    await flushPromises()

    const dependantInput = wrapper.findAllComponents(Input).at(1).find('input')
    const userValue = 'ABC'
    await dependantInput.setValue(userValue)
    await dependantInput.trigger('change')

    const dependedInput = wrapper.findAllComponents(Input).at(0).find('input')
    await dependedInput.setValue('Value 1')
    await dependedInput.trigger('change')

    expect(dependantInput.element.value).toBe(userValue)
    expect(trackEvent.mock.calls).toHaveLength(0)
  })

  it('testing date constraint dependencies', async () => {
    const wrapper = mount(Form, {
      store: getStore({
        getFormData: async () => (
          Promise.resolve(
            JSON.parse('{"Success":true,"Error":null,"Data":{"Form":{"Name":"FormName","Title":"FormName","Parts":[{"FormName":"FormName","Name":"PartName1","Title":"PartName 1","TitleKey":"PartName1","Order":1,"IsVisible":true,"Id":1,"Type":"grid-form-part","Rows":4,"Columns":3,"RowDataName":"PartName 1","AllowRowOpen":false,"AllowRowDelete":false,"ListItemTitle":null,"ItemDescriptionField":null,"IsRepeatable":false,"RecordId":null}],"PartOptions":null,"Fields":[{"Value":null,"PartName":"PartName1","Placeholder":null,"Component":"app-datepicker","IsDisabled":false,"IsVisible":true,"LabelKey":"Field1","Label":"Field 1","Name":"Field1","IsHierarchySelector":false,"IsRequired":false,"RemoteUrl":"","Type":null,"AddOn":null,"Options":null,"HierarchyOptions":null,"OptionsWithKeyValue":null,"AdditionalInfo":null,"DefaultVisibilty":false,"Order":0,"OptionsWithLabel":null,"Validations":{"Or":null,"And":[]},"ResponseDataPath":null,"OptionDisplayKeys":null,"OptionValueKey":null,"LoadImmediately":false,"HierarchySelectionConfigs":null,"DisabledDateConfig":null,"RemoteUpdateConfig":null,"RowNumber":1,"ColumnNumber":null,"Id":1,"ParentType":"PART","ParentId":1,"FieldOptions":"","ValidationList":"","DefaultValue":"","Key":"PartName1Field1","HasInfo":false,"InfoText":null,"Config":null,"ColumnWidth":1,"HasToggleButton":false},{"Value":null,"PartName":"PartName1","Placeholder":null,"Component":"app-datepicker","IsDisabled":false,"IsVisible":true,"LabelKey":"Field2","Label":"Field 2","Name":"Field2","IsHierarchySelector":false,"IsRequired":false,"RemoteUrl":"","Type":null,"AddOn":null,"Options":null,"HierarchyOptions":null,"OptionsWithKeyValue":null,"AdditionalInfo":null,"DefaultVisibilty":false,"Order":0,"OptionsWithLabel":null,"Validations":{"Or":null,"And":[{"Type":"Required","Max":0,"Min":0,"IsBackendValidation":false,"ValidationUrl":null,"ShowServerError":false,"ErrorTargetField":null,"ValidationParams":null,"ErrorMessage":"This field is required","ErrorMessageKey":"error_field_required","RequiredOnlyWhen":null,"Regex":null}]},"ResponseDataPath":null,"OptionDisplayKeys":null,"OptionValueKey":null,"LoadImmediately":false,"HierarchySelectionConfigs":null,"DisabledDateConfig":null,"RemoteUpdateConfig":null,"RowNumber":1,"ColumnNumber":null,"Id":2,"ParentType":"PART","ParentId":1,"FieldOptions":"","ValidationList":"","DefaultValue":"","Key":"PartName1Field2","HasInfo":false,"InfoText":null,"Config":null,"ColumnWidth":1,"HasToggleButton":false}],"Triggers":null,"TriggerConfigs":null,"ValueDependencies":[],"FilterDependencies":[],"VisibilityDependencies":[],"DateConstraintDependencies":[{"Lookups":[{"FormPart":"PartName1","Field":"Field1","ConstraintName":"To"}],"FormPart":"PartName1","Field":"Field2","FormPartId":null,"FieldId":null,"Type":"Value","IsForm":false}],"IsRequiredDependencies":[],"RemoteUpdateConfigs":null,"ValuePropertyDependencies":[],"CompoundValueDependencies":null,"SaveEndpoint":"/API/Dispensation/AddDispensation/","SaveText":"Submit","SaveTextKey":"submit"},"ExistingData":{}}}')
          )
        )
      }),
      localVue,
      mocks: {
        $t: (key) => key
      }
    })

    await flushPromises()

    const dateToBeSelected = 15

    const dependedComponent = wrapper.findAllComponents(DatePicker).at(0)

    const dependantComponent = wrapper.findAllComponents(DatePicker).at(1)

    await dependedComponent.find('input').trigger('click')
    await dependedComponent.findAll('.date.filled').at(dateToBeSelected - 1).trigger('click')

    expect(dependantComponent.findAll('.date.filled.disabled').length).toBe(dateToBeSelected - 1)
    expect(trackEvent.mock.calls).toHaveLength(0)
  })

  it('testing submit', async () => {
    const store = getStore({
      getFormData: async () => (
        Promise.resolve(
          JSON.parse('{"Success":true,"Error":null,"Data":{"Form":{"Name":"FormName","Title":"FormName","Parts":[{"FormName":"FormName","Name":"PartName1","Title":"PartName 1","TitleKey":"PartName1","Order":1,"IsVisible":true,"Id":1,"Type":"grid-form-part","Rows":4,"Columns":3,"RowDataName":"PartName 1","AllowRowOpen":false,"AllowRowDelete":false,"ListItemTitle":null,"ItemDescriptionField":null,"IsRepeatable":false,"RecordId":null}],"PartOptions":null,"Fields":[{"Value":null,"PartName":"PartName1","Placeholder":null,"Component":"app-input-field","IsDisabled":false,"IsVisible":true,"LabelKey":"Field1","Label":"Field 1","Name":"Field1","IsHierarchySelector":false,"IsRequired":false,"RemoteUrl":"","Type":null,"AddOn":null,"Options":null,"HierarchyOptions":null,"OptionsWithKeyValue":null,"AdditionalInfo":null,"DefaultVisibilty":false,"Order":0,"OptionsWithLabel":null,"Validations":{"Or":null,"And":[]},"ResponseDataPath":null,"OptionDisplayKeys":null,"OptionValueKey":null,"LoadImmediately":false,"HierarchySelectionConfigs":null,"DisabledDateConfig":null,"RemoteUpdateConfig":null,"RowNumber":1,"ColumnNumber":null,"Id":1,"ParentType":"PART","ParentId":1,"FieldOptions":"","ValidationList":"","DefaultValue":"","Key":"PartName1Field1","HasInfo":false,"InfoText":null,"Config":null,"ColumnWidth":1,"HasToggleButton":false},{"Value":null,"PartName":"PartName1","Placeholder":null,"Component":"app-select","IsDisabled":false,"IsVisible":true,"LabelKey":"Field2","Label":"Field 2","Name":"Field2","IsHierarchySelector":false,"IsRequired":false,"RemoteUrl":"","Type":null,"AddOn":null,"Options":null,"HierarchyOptions":null,"OptionsWithKeyValue":[{"Key":"Key1","Value":"Value1"},{"Key":"Key2","Value":"Value2"}],"AdditionalInfo":null,"DefaultVisibilty":false,"Order":0,"OptionsWithLabel":null,"Validations":{"Or":null,"And":[]},"ResponseDataPath":null,"OptionDisplayKeys":null,"OptionValueKey":null,"LoadImmediately":false,"HierarchySelectionConfigs":null,"DisabledDateConfig":null,"RemoteUpdateConfig":null,"RowNumber":1,"ColumnNumber":null,"Id":1,"ParentType":"PART","ParentId":1,"FieldOptions":"","ValidationList":"","DefaultValue":"","Key":"PartName1Field2","HasInfo":false,"InfoText":null,"Config":null,"ColumnWidth":1,"HasToggleButton":false}],"Triggers":null,"TriggerConfigs":null,"ValueDependencies":[],"FilterDependencies":[],"VisibilityDependencies":[],"DateConstraintDependencies":[],"IsRequiredDependencies":[],"RemoteUpdateConfigs":null,"ValuePropertyDependencies":[],"CompoundValueDependencies":null,"SaveEndpoint":"/API/Dispensation/AddDispensation/","SaveText":"Submit","SaveTextKey":"submit"},"ExistingData":{}}}')
        )
      ),
      save: async () => (
        Promise.resolve({
          success: true,
          Error: null,
          Data: 'Success'
        })
      )
    })

    const wrapper = mount(Form, {
      store,
      localVue,
      propsData: { isEditing: true },
      mocks: {
        $t: (key) => key
      }
    })

    await flushPromises()

    await wrapper.findComponent(Select).findAll('li').at(0).trigger('click')

    await wrapper.findAllComponents(Button).at(0).trigger('click')
    await new Promise(process.nextTick)
    expect(trackEvent.mock.calls).toHaveLength(2)
  })

  it('testing submit with no success message', async () => {
    const store = getStore({
      getFormData: async () => (
        Promise.resolve(
          JSON.parse('{"Success":true,"Error":null,"Data":{"Form":{"Name":"FormName","Title":"FormName","Parts":[{"FormName":"FormName","Name":"PartName1","Title":"PartName 1","TitleKey":"PartName1","Order":1,"IsVisible":true,"Id":1,"Type":"grid-form-part","Rows":4,"Columns":3,"RowDataName":"PartName 1","AllowRowOpen":false,"AllowRowDelete":false,"ListItemTitle":null,"ItemDescriptionField":null,"IsRepeatable":false,"RecordId":null}],"PartOptions":null,"Fields":[{"Value":null,"PartName":"PartName1","Placeholder":null,"Component":"app-input-field","IsDisabled":false,"IsVisible":true,"LabelKey":"Field1","Label":"Field 1","Name":"Field1","IsHierarchySelector":false,"IsRequired":false,"RemoteUrl":"","Type":null,"AddOn":null,"Options":null,"HierarchyOptions":null,"OptionsWithKeyValue":null,"AdditionalInfo":null,"DefaultVisibilty":false,"Order":0,"OptionsWithLabel":null,"Validations":{"Or":null,"And":[]},"ResponseDataPath":null,"OptionDisplayKeys":null,"OptionValueKey":null,"LoadImmediately":false,"HierarchySelectionConfigs":null,"DisabledDateConfig":null,"RemoteUpdateConfig":null,"RowNumber":1,"ColumnNumber":null,"Id":1,"ParentType":"PART","ParentId":1,"FieldOptions":"","ValidationList":"","DefaultValue":"","Key":"PartName1Field1","HasInfo":false,"InfoText":null,"Config":null,"ColumnWidth":1,"HasToggleButton":false},{"Value":null,"PartName":"PartName1","Placeholder":null,"Component":"app-input-field","IsDisabled":false,"IsVisible":true,"LabelKey":"Field2","Label":"Field 2","Name":"Field2","IsHierarchySelector":false,"IsRequired":false,"RemoteUrl":"","Type":null,"AddOn":null,"Options":null,"HierarchyOptions":null,"OptionsWithKeyValue":null,"AdditionalInfo":null,"DefaultVisibilty":false,"Order":0,"OptionsWithLabel":null,"Validations":{"Or":null,"And":[]},"ResponseDataPath":null,"OptionDisplayKeys":null,"OptionValueKey":null,"LoadImmediately":false,"HierarchySelectionConfigs":null,"DisabledDateConfig":null,"RemoteUpdateConfig":null,"RowNumber":1,"ColumnNumber":null,"Id":1,"ParentType":"PART","ParentId":1,"FieldOptions":"","ValidationList":"","DefaultValue":"","Key":"PartName1Field2","HasInfo":false,"InfoText":null,"Config":null,"ColumnWidth":1,"HasToggleButton":false}],"Triggers":null,"TriggerConfigs":null,"ValueDependencies":[],"FilterDependencies":[],"VisibilityDependencies":[],"DateConstraintDependencies":[],"IsRequiredDependencies":[],"RemoteUpdateConfigs":null,"ValuePropertyDependencies":[],"CompoundValueDependencies":null,"SaveEndpoint":"/API/Dispensation/AddDispensation/","SaveText":"Submit","SaveTextKey":"submit"},"ExistingData":{}}}')
        )
      ),
      save: async () => (
        Promise.resolve({
          success: true,
          Error: null
        })
      )
    })

    const wrapper = mount(Form, {
      store,
      localVue,
      propsData: { isEditing: true },
      mocks: {
        $t: (key) => key
      }
    })

    await flushPromises()

    await wrapper.findAllComponents(Button).at(0).trigger('click')
    await new Promise(process.nextTick)
    expect(trackEvent.mock.calls).toHaveLength(2)
  })

  it('testing submit with error message', async () => {
    const store = getStore({
      getFormData: async () => (
        Promise.resolve(
          JSON.parse('{"Success":true,"Error":null,"Data":{"Form":{"Name":"FormName","Title":"FormName","Parts":[{"FormName":"FormName","Name":"PartName1","Title":"PartName 1","TitleKey":"PartName1","Order":1,"IsVisible":true,"Id":1,"Type":"grid-form-part","Rows":4,"Columns":3,"RowDataName":"PartName 1","AllowRowOpen":false,"AllowRowDelete":false,"ListItemTitle":null,"ItemDescriptionField":null,"IsRepeatable":false,"RecordId":null}],"PartOptions":null,"Fields":[{"Value":null,"PartName":"PartName1","Placeholder":null,"Component":"app-input-field","IsDisabled":false,"IsVisible":true,"LabelKey":"Field1","Label":"Field 1","Name":"Field1","IsHierarchySelector":false,"IsRequired":false,"RemoteUrl":"","Type":null,"AddOn":null,"Options":null,"HierarchyOptions":null,"OptionsWithKeyValue":null,"AdditionalInfo":null,"DefaultVisibilty":false,"Order":0,"OptionsWithLabel":null,"Validations":{"Or":null,"And":[]},"ResponseDataPath":null,"OptionDisplayKeys":null,"OptionValueKey":null,"LoadImmediately":false,"HierarchySelectionConfigs":null,"DisabledDateConfig":null,"RemoteUpdateConfig":null,"RowNumber":1,"ColumnNumber":null,"Id":1,"ParentType":"PART","ParentId":1,"FieldOptions":"","ValidationList":"","DefaultValue":"","Key":"PartName1Field1","HasInfo":false,"InfoText":null,"Config":null,"ColumnWidth":1,"HasToggleButton":false},{"Value":null,"PartName":"PartName1","Placeholder":null,"Component":"app-input-field","IsDisabled":false,"IsVisible":true,"LabelKey":"Field2","Label":"Field 2","Name":"Field2","IsHierarchySelector":false,"IsRequired":false,"RemoteUrl":"","Type":null,"AddOn":null,"Options":null,"HierarchyOptions":null,"OptionsWithKeyValue":null,"AdditionalInfo":null,"DefaultVisibilty":false,"Order":0,"OptionsWithLabel":null,"Validations":{"Or":null,"And":[]},"ResponseDataPath":null,"OptionDisplayKeys":null,"OptionValueKey":null,"LoadImmediately":false,"HierarchySelectionConfigs":null,"DisabledDateConfig":null,"RemoteUpdateConfig":null,"RowNumber":1,"ColumnNumber":null,"Id":1,"ParentType":"PART","ParentId":1,"FieldOptions":"","ValidationList":"","DefaultValue":"","Key":"PartName1Field2","HasInfo":false,"InfoText":null,"Config":null,"ColumnWidth":1,"HasToggleButton":false}],"Triggers":null,"TriggerConfigs":null,"ValueDependencies":[],"FilterDependencies":[],"VisibilityDependencies":[],"DateConstraintDependencies":[],"IsRequiredDependencies":[],"RemoteUpdateConfigs":null,"ValuePropertyDependencies":[],"CompoundValueDependencies":null,"SaveEndpoint":"/API/Dispensation/AddDispensation/","SaveText":"Submit","SaveTextKey":"submit"},"ExistingData":{}}}')
        )
      ),
      save: async () => (
        Promise.resolve({
          Success: false,
          Error: {
            Message: 'Error'
          }
        })
      )
    })

    const wrapper = mount(Form, {
      store,
      localVue,
      propsData: { isEditing: true },
      mocks: {
        $t: (key) => key
      }
    })

    await flushPromises()

    await wrapper.findAllComponents(Button).at(0).trigger('click')
    await new Promise(process.nextTick)
    expect(trackEvent.mock.calls).toHaveLength(1)
  })

  it('testing submit for table-form-part', async () => {
    const tableFormPartFieldLabels = [{
      Field1: null,
      Field2: null
    }]
    const inputValues = {
      PartName1: tableFormPartFieldLabels
    }
    const inputData = {
      PartName1: inputValues
    }
    const store = getStore({
      getFormData: async () => (
        Promise.resolve(
          JSON.parse('{"Success":true,"Error":null,"Data":{"Form":{"Name":"FormName","Title":"FormName","Parts":[{"FormName":"FormName","Name":"PartName1","Title":"PartName 1","TitleKey":"PartName1","Order":1,"IsVisible":true,"Id":1,"Type":"table-form-part","Rows":4,"Columns":3,"RowDataName":"PartName 1","AllowRowOpen":false,"AllowRowDelete":false,"ListItemTitle":null,"ItemDescriptionField":null,"IsRepeatable":false,"RecordId":null}],"PartOptions":null,"Fields":[{"Value":null,"PartName":"PartName1","Placeholder":null,"Component":"app-datepicker","IsDisabled":false,"IsVisible":true,"LabelKey":"Field1","Label":"Field 1","Name":"Field1","IsHierarchySelector":false,"IsRequired":false,"RemoteUrl":"","Type":null,"AddOn":null,"Options":null,"HierarchyOptions":null,"OptionsWithKeyValue":null,"AdditionalInfo":null,"DefaultVisibilty":false,"Order":0,"OptionsWithLabel":null,"Validations":{"Or":null,"And":[]},"ResponseDataPath":null,"OptionDisplayKeys":null,"OptionValueKey":null,"LoadImmediately":false,"HierarchySelectionConfigs":null,"DisabledDateConfig":null,"RemoteUpdateConfig":null,"RowNumber":1,"ColumnNumber":null,"Id":1,"ParentType":"PART","ParentId":1,"FieldOptions":"","ValidationList":"","DefaultValue":"","Key":"PartName1Field1","HasInfo":false,"InfoText":null,"Config":null,"ColumnWidth":1,"HasToggleButton":false},{"Value":null,"PartName":"PartName1","Placeholder":null,"Component":"app-select","IsDisabled":false,"IsVisible":true,"LabelKey":"Field2","Label":"Field 2","Name":"Field2","IsHierarchySelector":false,"IsRequired":false,"RemoteUrl":"","Type":null,"AddOn":null,"Options":null,"HierarchyOptions":null,"OptionsWithKeyValue":[{"Key":"Key1","Value":"Value1"},{"Key":"Key2","Value":"Value2"}],"AdditionalInfo":null,"DefaultVisibilty":false,"Order":0,"OptionsWithLabel":null,"Validations":{"Or":null,"And":[]},"ResponseDataPath":null,"OptionDisplayKeys":null,"OptionValueKey":null,"LoadImmediately":false,"HierarchySelectionConfigs":null,"DisabledDateConfig":null,"RemoteUpdateConfig":null,"RowNumber":1,"ColumnNumber":null,"Id":1,"ParentType":"PART","ParentId":1,"FieldOptions":"","ValidationList":"","DefaultValue":"","Key":"PartName1Field2","HasInfo":false,"InfoText":null,"Config":null,"ColumnWidth":1,"HasToggleButton":false}],"Triggers":null,"TriggerConfigs":null,"ValueDependencies":[],"FilterDependencies":[],"VisibilityDependencies":[],"DateConstraintDependencies":[],"IsRequiredDependencies":[],"RemoteUpdateConfigs":null,"ValuePropertyDependencies":[],"CompoundValueDependencies":null,"SaveEndpoint":"/API/Dispensation/AddDispensation/","SaveText":"Submit","SaveTextKey":"submit"},"ExistingData":{}}}')
        )
      ),
      save: async () => (
        Promise.resolve({
          Success: true,
          Error: null,
          Data: 'Success'
        })
      )
    })

    const wrapper = mount(Form, {
      store,
      localVue,
      propsData: { isEditing: true },
      data () {
        return {
          input: inputData
        }
      },
      mocks: {
        $t: (key) => key
      }
    })

    await flushPromises()
    await wrapper.findAllComponents(Button).at(0).trigger('click')
  })

  it('testing visibility dependencies for checkbox field', async () => {
    const wrapper = mount(Form, {
      store: getStore({
        getFormData: async () => (
          Promise.resolve(
            JSON.parse('{"Success":true,"Error":null,"Data":{"Form":{"Name":"FormName","Title":"FormName","Parts":[{"FormName":"FormName","Name":"PartName1","Title":"PartName 1","TitleKey":"PartName1","Order":1,"IsVisible":true,"Id":1,"Type":"grid-form-part","Rows":4,"Columns":3,"RowDataName":"PartName 1","AllowRowOpen":false,"AllowRowDelete":false,"ListItemTitle":null,"ItemDescriptionField":null,"IsRepeatable":false,"RecordId":null}],"PartOptions":null,"Fields":[{"Value":null,"PartName":"PartName1","Placeholder":null,"Component":"app-checkbox-group","IsDisabled":false,"IsVisible":true,"LabelKey":"Field1","Label":"Field 1","Name":"Field1","IsHierarchySelector":false,"IsRequired":false,"RemoteUrl":"","Type":null,"AddOn":null,"Options":null,"HierarchyOptions":null,"OptionsWithKeyValue":[{"Value":"","Key":"true"}],"AdditionalInfo":null,"DefaultVisibilty":false,"Order":0,"OptionsWithLabel":null,"Validations":{"Or":null,"And":[]},"ResponseDataPath":null,"OptionDisplayKeys":null,"OptionValueKey":null,"LoadImmediately":false,"HierarchySelectionConfigs":null,"DisabledDateConfig":null,"RemoteUpdateConfig":null,"RowNumber":1,"ColumnNumber":null,"Id":1,"ParentType":"PART","ParentId":1,"FieldOptions":"","ValidationList":"","DefaultValue":"","Key":"PartName1Field1","HasInfo":false,"InfoText":null,"Config":null,"ColumnWidth":1,"HasToggleButton":false},{"Value":null,"PartName":"PartName1","Placeholder":null,"Component":"app-input-field","IsDisabled":false,"IsVisible":false,"LabelKey":"Field2","Label":"Field 2","Name":"Field2","IsHierarchySelector":false,"IsRequired":false,"RemoteUrl":"","Type":null,"AddOn":null,"Options":null,"HierarchyOptions":null,"OptionsWithKeyValue":null,"AdditionalInfo":null,"DefaultVisibilty":false,"Order":0,"OptionsWithLabel":null,"Validations":{"Or":null,"And":[]},"ResponseDataPath":null,"OptionDisplayKeys":null,"OptionValueKey":null,"LoadImmediately":false,"HierarchySelectionConfigs":null,"DisabledDateConfig":null,"RemoteUpdateConfig":null,"RowNumber":1,"ColumnNumber":null,"Id":1,"ParentType":"PART","ParentId":1,"FieldOptions":"","ValidationList":"","DefaultValue":"","Key":"PartName1Field2","HasInfo":false,"InfoText":null,"Config":null,"ColumnWidth":1,"HasToggleButton":false}],"Triggers":null,"TriggerConfigs":null,"ValueDependencies":[],"FilterDependencies":[],"VisibilityDependencies":[{"Lookups":[{"FormPart":"PartName1","Field":"Field1","ExpectedValues":["true"]}],"FormPart":"PartName1","Field":"Field2","FormPartId":null,"FieldId":null,"Type":"VISIBILITY","IsForm":false}],"DateConstraintDependencies":[],"IsRequiredDependencies":[],"RemoteUpdateConfigs":null,"ValuePropertyDependencies":[],"CompoundValueDependencies":null,"SaveEndpoint":"/API/Dispensation/AddDispensation/","SaveText":"Submit","SaveTextKey":"submit"},"ExistingData":{}}}')
          )
        )
      }),
      localVue,
      mocks: {
        $t: (key) => key
      }
    })

    await flushPromises()

    expect(wrapper.findComponent(GridFormPart).findAllComponents(Input).length).toBe(0)

    const checkbox = wrapper.findComponent(GridFormPart).findComponent(Checkbox).find('input')
    await checkbox.trigger('click')

    expect(wrapper.findComponent(GridFormPart).findAllComponents(Input).length).toBe(1)

    await checkbox.trigger('click')

    expect(wrapper.findComponent(GridFormPart).findAllComponents(Input).length).toBe(0)
  })

  it('testing visibility dependencies for select field', async () => {
    const wrapper = mount(Form, {
      store: getStore({
        getFormData: async () => (
          Promise.resolve(
            JSON.parse('{"Success":true,"Error":null,"Data":{"Form":{"Name":"FormName","Title":"FormName","Parts":[{"FormName":"FormName","Name":"PartName1","Title":"PartName 1","TitleKey":"PartName1","Order":1,"IsVisible":true,"Id":1,"Type":"grid-form-part","Rows":4,"Columns":3,"RowDataName":"PartName 1","AllowRowOpen":false,"AllowRowDelete":false,"ListItemTitle":null,"ItemDescriptionField":null,"IsRepeatable":false,"RecordId":null}],"PartOptions":null,"Fields":[{"Value":null,"PartName":"PartName1","Placeholder":null,"Component":"app-select","IsDisabled":false,"IsVisible":true,"LabelKey":"Field1","Label":"Field 1","Name":"Field1","IsHierarchySelector":false,"IsRequired":false,"RemoteUrl":"","Type":null,"AddOn":null,"Options":null,"HierarchyOptions":null,"OptionsWithKeyValue":[{"Value":"Value1","Key":"Key1"},{"Value":"Value2","Key":"Key2"}],"AdditionalInfo":null,"DefaultVisibilty":false,"Order":0,"OptionsWithLabel":null,"Validations":{"Or":null,"And":[]},"ResponseDataPath":null,"OptionDisplayKeys":null,"OptionValueKey":null,"LoadImmediately":false,"HierarchySelectionConfigs":null,"DisabledDateConfig":null,"RemoteUpdateConfig":null,"RowNumber":1,"ColumnNumber":null,"Id":1,"ParentType":"PART","ParentId":1,"FieldOptions":"","ValidationList":"","DefaultValue":"","Key":"PartName1Field1","HasInfo":false,"InfoText":null,"Config":null,"ColumnWidth":1,"HasToggleButton":false},{"Value":null,"PartName":"PartName1","Placeholder":null,"Component":"app-input-field","IsDisabled":false,"IsVisible":false,"LabelKey":"Field2","Label":"Field 2","Name":"Field2","IsHierarchySelector":false,"IsRequired":false,"RemoteUrl":"","Type":null,"AddOn":null,"Options":null,"HierarchyOptions":null,"OptionsWithKeyValue":null,"AdditionalInfo":null,"DefaultVisibilty":false,"Order":0,"OptionsWithLabel":null,"Validations":{"Or":null,"And":[]},"ResponseDataPath":null,"OptionDisplayKeys":null,"OptionValueKey":null,"LoadImmediately":false,"HierarchySelectionConfigs":null,"DisabledDateConfig":null,"RemoteUpdateConfig":null,"RowNumber":1,"ColumnNumber":null,"Id":1,"ParentType":"PART","ParentId":1,"FieldOptions":"","ValidationList":"","DefaultValue":"","Key":"PartName1Field2","HasInfo":false,"InfoText":null,"Config":null,"ColumnWidth":1,"HasToggleButton":false}],"Triggers":null,"TriggerConfigs":null,"ValueDependencies":[],"FilterDependencies":[],"VisibilityDependencies":[{"Lookups":[{"FormPart":"PartName1","Field":"Field1","ExpectedValues":["Key1"]}],"FormPart":"PartName1","Field":"Field2","FormPartId":null,"FieldId":null,"Type":"VISIBILITY","IsForm":false}],"DateConstraintDependencies":[],"IsRequiredDependencies":[],"RemoteUpdateConfigs":null,"ValuePropertyDependencies":[],"CompoundValueDependencies":null,"SaveEndpoint":"/API/Dispensation/AddDispensation/","SaveText":"Submit","SaveTextKey":"submit"},"ExistingData":{}}}')
          )
        )
      }),
      localVue,
      mocks: {
        $t: (key) => key
      }
    })

    await flushPromises()

    expect(wrapper.findComponent(GridFormPart).findAllComponents(Input).length).toBe(0)

    const select = wrapper.findComponent(GridFormPart).findComponent(Select)
    await select.findAll('ul li').at(0).trigger('click')

    expect(wrapper.findComponent(GridFormPart).findAllComponents(Input).length).toBe(1)

    await select.findAll('ul li').at(1).trigger('click')

    expect(wrapper.findComponent(GridFormPart).findAllComponents(Input).length).toBe(0)
  })
})
