import Header from '../../src/app/shared/components/Header/Header.vue'
import HeaderSearchBar from '../../src/app/shared/components/Header/HeaderSearchBar.vue'
import Vuex from 'vuex'
import VueRouter from 'vue-router'
import { shallowMount, createLocalVue } from '@vue/test-utils'

const localVue = createLocalVue()
localVue.use(Vuex)
localVue.use(VueRouter)

describe('Header.vue', () => {
  let globalGetters
  let store
  let globalActions
  let globalStates
  let headerStates
  let headerActions

  beforeEach(() => {
    globalGetters = {
      darkModeState: jest.fn(),
      accessibilityModeState: jest.fn()
    }
    globalActions = {
      toggleDarkMode: jest.fn(),
      toggleAccessibilityMode: jest.fn(),
      logout: jest.fn(),
      login: jest.fn()
    }
    globalStates = {
      userName: 'demo'
    }
    headerStates = {
      userAuthenticatedVatient: true,
      headerLinks: [],
      dropDownItems: [],
      headerUserName: 'name'
    }
    headerActions = {
      getHeaderDetails: jest.fn()
    }
    store = new Vuex.Store({
      modules: {
        Header: {
          namespaced: true,
          state: headerStates,
          actions: headerActions
        }
      },
      getters: globalGetters,
      actions: globalActions,
      state: globalStates
    })
  })

  it('main app to check child component visibility', () => {
    const wrapper = shallowMount(Header, {
      localVue,
      store,
      mocks: {
        $t: (key) => key
      }
    })
    expect(wrapper.findComponent(HeaderSearchBar).isVisible()).toBe(true)
  })
})
