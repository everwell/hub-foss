import SUPPORTED_LANGUAGES from '@/utils/languages/index.js'

describe('languages', () => {
  it('always has fallback language', () => {
    // en (English) will always be the fallback language
    expect(Object.keys(SUPPORTED_LANGUAGES)).toContain('en')
  })
})
