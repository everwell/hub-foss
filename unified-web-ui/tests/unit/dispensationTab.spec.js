import { mount, createLocalVue } from '@vue/test-utils'
import Dispensation from '../../src/app/Pages/dashboard/patient/components/Dispensation'
import Vuex from 'vuex'
import Vue from 'vue'
import { Map, List } from 'immutable'
import flushPromises from 'flush-promises'
import Form from '@/app/shared/components/Form.vue'
import Button from '@/app/shared/components/Button.vue'

const localVue = createLocalVue()
localVue.use(Vuex)
window.EventBus = new Vue()

jest.mock('../../src/utils/toastUtils', () => {
  return {
    __esModule: true,
    defaultToast: jest.fn(),
    ToastType: {
      Success: 'Success',
      Error: 'Error',
      Warning: 'Warning',
      Neutal: 'Neutal'
    }
  }
})

jest.mock('../../src/utils/matomoTracking', () => {
  return {
    __esModule: true,
    initializeMatomo: jest.fn(),
    trackEvent: jest.fn()
  }
})

describe('Dispensation Page tests ', () => {
  let store
  let actions
  afterEach(() => {
    jest.clearAllMocks()
  })
  beforeEach(() => {
    actions = {
      getDispensationTabPermission: jest.fn(),
      getPatientDispensationData: jest.fn(),
      removeDispensation: jest.fn(),
      patientPreviousData: jest.fn(),
      getDeploymentCode: jest.fn(),
      updateExistingData: jest.fn(),
      getFormData: jest.fn(),
      setIsEditing: jest.fn(),
      loadForm: jest.fn(),
      startSaving: jest.fn(),
      save: async () => (
        Promise.resolve({
          Success: true,
          Error: null,
          Data: 'Success'
        })
      ),
      endSaving: jest.fn()
    }
    store = new Vuex.Store({
      modules: {
        Dispensation: {
          namespaced: true,
          actions
        },
        Form: {
          namespaced: true,
          state: {
            isSaving: false,
            isLoading: false,
            isEditing: true,
            hasFormError: false,
            adherenceTechnology: { MERM: false, VOT: false },
            existingData: { DispensationId: 1 },
            existingVModel: {},
            form: {},
            title: '',
            remoteUpdateConfigsMappedByField: Map(),
            remoteUpdateConfigs: [],
            hierarchyConfigs: Map(),
            valueDependencies: List(),
            formPartsMappedByName: Map(),
            fieldsMappedByFormPartName: Map(),
            fieldsMappedByNameOriginal: Map(),
            fieldsMappedByName: null,
            allFields: null,
            visibilityDependencies: List(),
            isRequiredDependencies: List(),
            filterDependencies: List(),
            visibilityDependenciesMappedByFormPartName: null,
            dateConstraintDependency: List(),
            formFieldsByFormPartName: {},
            saveEndpoint: '',
            saveText: '',
            saveUrlParams: '',
            getFormUrlParams: '',
            replaceExistingDataFromServer: true,
            recaptchaVerified: false,
            otp: '',
            editAccess: false,
            valuePropertyDependencies: List()
          },
          actions
        }

      }
    })
  })

  var patientResponse = {
    Success: true,
    Data: {
      PatientData: { PatientId: 10722905, EpisodeId: 10722905, PatientName: 'TestDemo PleaseIgnore', Age: 55, Gender: 'Male', MobileNo: '1234567770', Address: 'asdf sdvsa vdadfsvdfv', PatientStatus: 'On Treatment (Notified)', TreatmentStartDate: '30-11-2021', DiagnosisDate: '07-11-2021', CurrentFacilityType: 'PHI', CurrentFacility: 'BARUKAL', State: 'Karnataka', District: 'Dharwad', Tu: 'DHARWAD', Phi: null },
      DrugDispensationData:
      {
        Source: 'NTEP',
        AgeCategory: null,
        DateOfPrescription: '02-12-2021',
        TypeOfRegimen: 'DSTB',
        InitialWeight: '23',
        WeightBand: 'DRTB: 46-70 Kg',
        IssuingFacilityType: 'PHI',
        IssuingFacilityName: 'ModifiedtestPhI',
        DateOfDispensation: '03-12-2021',
        DosingStartDate: '04-12-2021',
        NoOfDaysOfDispensation: '7',
        Phase: 'IP',
        Remarks: 'N/A',
        Products: 'Amikacin 500mg ( Vial/Ampules , 2)'
      },
      NextRefillDate: '01-04-2022',
      PrintedDate: '29-03-2022'
    },
    Error: null
  }

  it('starting the dispensation tab with click on add button', async () => {
    store.state.Form.isLoading = false
    var sampleResponse = { Success: true, Data: { Permissions: { Add: true, Edit: false, View: false, Delete: false } }, Error: null }
    actions.getDispensationTabPermission.mockReturnValue(sampleResponse)
    const wrapper = mount(Dispensation, {
      store,
      localVue,
      mocks: {
        $t: (key) => key
      }
    })
    await flushPromises()
    expect(wrapper.find('#addDispensationBtn').exists()).toBe(true)
    await wrapper.find('#addDispensationBtn').trigger('click')
    await wrapper.vm.$nextTick()
    expect(wrapper.find('#cancelBtn').exists()).toBe(true)
    expect(wrapper.find('#copyFromPreviousBtn').exists()).toBe(true)
    await wrapper.find('#cancelBtn').trigger('click')
    await wrapper.vm.$nextTick()
    expect(actions.getDispensationTabPermission.mock.calls).toHaveLength(1)
  })

  it('starting the dispensation tab with click on add button if already loading', async () => {
    store.state.Form.isLoading = true
    var sampleResponse = { Success: true, Data: { Permissions: { Add: true, Edit: false, View: false, Delete: false } }, Error: null }
    actions.getDispensationTabPermission.mockReturnValue(sampleResponse)
    const wrapper = mount(Dispensation, {
      store,
      localVue,
      mocks: {
        $t: (key) => key
      }
    })
    await flushPromises()
    expect(wrapper.find('#addDispensationBtn').exists()).toBe(true)
    await wrapper.find('#addDispensationBtn').trigger('click')
    await wrapper.vm.$nextTick()
    expect(wrapper.find('#cancelBtn').exists()).toBe(false)
    expect(actions.getDispensationTabPermission.mock.calls).toHaveLength(1)
  })

  it('starting the dispensation tab and open return view ', async () => {
    store.state.Form.isLoading = false
    var sampleResponse = { Success: true, Data: { Permissions: { Add: true, Edit: false, View: false, Delete: false } }, Error: null }
    actions.getDispensationTabPermission.mockReturnValue(sampleResponse)
    const wrapper = mount(Dispensation, {
      store,
      localVue,
      mocks: {
        $t: (key) => key
      }
    })
    await flushPromises()
    expect(wrapper.find('#addDispensationBtn').exists()).toBe(true)
    var data = { listName: 'DispensationDetailsList', rowData: { DispensationId: '1' } }
    wrapper.vm.returnItem(data)
    await wrapper.vm.$nextTick()
    expect(wrapper.find('#cancelBtn').exists()).toBe(true)
  })

  it('starting the dispensation tab and open return view if already loading', async () => {
    store.state.Form.isLoading = true
    var sampleResponse = { Success: true, Data: { Permissions: { Add: true, Edit: false, View: false, Delete: false } }, Error: null }
    actions.getDispensationTabPermission.mockReturnValue(sampleResponse)
    const wrapper = mount(Dispensation, {
      store,
      localVue,
      mocks: {
        $t: (key) => key
      }
    })
    await flushPromises()
    expect(wrapper.find('#addDispensationBtn').exists()).toBe(true)
    var data = { listName: 'DispensationDetailsList', rowData: { DispensationId: '1' } }
    wrapper.vm.returnItem(data)
    await wrapper.vm.$nextTick()
    expect(wrapper.find('#cancelBtn').exists()).toBe(false)
  })

  it('starting the dispensation tab with click on cancel button if already loading', async () => {
    store.state.Form.isLoading = false
    var sampleResponse = { Success: true, Data: { Permissions: { Add: true, Edit: false, View: false, Delete: false } }, Error: null }
    actions.getDispensationTabPermission.mockReturnValue(sampleResponse)
    const wrapper = mount(Dispensation, {
      store,
      localVue,
      mocks: {
        $t: (key) => key
      }
    })
    await flushPromises()
    expect(wrapper.find('#addDispensationBtn').exists()).toBe(true)
    await wrapper.find('#addDispensationBtn').trigger('click')
    await wrapper.vm.$nextTick()
    expect(wrapper.find('#cancelBtn').exists()).toBe(true)
    expect(wrapper.find('#copyFromPreviousBtn').exists()).toBe(true)
    store.state.Form.isLoading = true
    await wrapper.find('#cancelBtn').trigger('click')
    await wrapper.vm.$nextTick()
    expect(actions.getDispensationTabPermission.mock.calls).toHaveLength(1)
  })

  it('starting the dispensation tab and open dispensation id view ', async () => {
    store.state.Form.isLoading = false
    var sampleResponse = { Success: true, Data: { Permissions: { Add: true, Edit: false, View: false, Delete: false } }, Error: null }
    actions.getDispensationTabPermission.mockReturnValue(sampleResponse)
    const wrapper = mount(Dispensation, {
      store,
      localVue,
      mocks: {
        $t: (key) => key
      }
    })
    await flushPromises()
    expect(wrapper.find('#addDispensationBtn').exists()).toBe(true)
    wrapper.vm.openForm('DispensationDetailsList')
    await wrapper.vm.$nextTick()
    expect(wrapper.find('#addDispensationBtn').exists()).toBe(false)
    expect(wrapper.find('#copyFromPreviousBtn').exists()).toBe(false)
  })

  it('starting the dispensation tab and open dispensation id view if already loading ', async () => {
    store.state.Form.isLoading = true
    var sampleResponse = { Success: true, Data: { Permissions: { Add: true, Edit: false, View: false, Delete: false } }, Error: null }
    actions.getDispensationTabPermission.mockReturnValue(sampleResponse)
    const wrapper = mount(Dispensation, {
      store,
      localVue,
      mocks: {
        $t: (key) => key
      }
    })
    await flushPromises()
    expect(wrapper.find('#addDispensationBtn').exists()).toBe(true)
    wrapper.vm.openForm('DispensationDetailsList')
    await wrapper.vm.$nextTick()
    expect(wrapper.find('#addDispensationBtn').exists()).toBe(true)
  })

  it('starting the dispensation tab with click on copy from last dispensation button', async () => {
    store.state.Form.isLoading = false
    var sampleResponse = { Success: true, Data: { Permissions: { Add: true, Edit: false, View: false, Delete: false } }, Error: null }
    actions.getDispensationTabPermission.mockReturnValue(sampleResponse)
    const wrapper = mount(Dispensation, {
      store,
      localVue,
      mocks: {
        $t: (key) => key
      }
    })
    await flushPromises()
    var dispensationResponse = { Success: true, Data: { dispensationId: 1 }, Error: null }
    actions.patientPreviousData.mockReturnValue(dispensationResponse)
    expect(wrapper.find('#addDispensationBtn').exists()).toBe(true)
    await wrapper.find('#addDispensationBtn').trigger('click')
    await wrapper.vm.$nextTick()
    expect(wrapper.find('#cancelBtn').exists()).toBe(true)
    expect(wrapper.find('#copyFromPreviousBtn').exists()).toBe(true)
    await wrapper.find('#copyFromPreviousBtn').trigger('click')
    await wrapper.vm.$nextTick()
    expect(actions.getDispensationTabPermission.mock.calls).toHaveLength(1)
    expect(actions.patientPreviousData.mock.calls).toHaveLength(1)
  })

  it('starting the dispensation tab and delete item ', async () => {
    store.state.Form.isLoading = false
    var sampleResponse = { Success: true, Data: { Permissions: { Add: true, Edit: false, View: false, Delete: false } }, Error: null }
    actions.getDispensationTabPermission.mockReturnValue(sampleResponse)
    const wrapper = mount(Dispensation, {
      store,
      localVue,
      mocks: {
        $t: (key) => key
      }
    })
    await flushPromises()
    var dispensationResponse = { Success: true, Data: { dispensationId: 1 }, Error: null }
    actions.removeDispensation.mockReturnValue(dispensationResponse)
    expect(wrapper.find('#addDispensationBtn').exists()).toBe(true)
    var data = { listName: 'DispensationDetailsList', rowData: { DispensationId: 1 } }
    wrapper.vm.deleteItem(data)
    await wrapper.vm.$nextTick()
    expect(actions.removeDispensation.mock.calls).toHaveLength(1)
  })

  it('starting the dispensation tab and delete item while already loading', async () => {
    store.state.Form.isLoading = true
    var sampleResponse = { Success: true, Data: { Permissions: { Add: true, Edit: false, View: false, Delete: false } }, Error: null }
    actions.getDispensationTabPermission.mockReturnValue(sampleResponse)
    const wrapper = mount(Dispensation, {
      store,
      localVue,
      mocks: {
        $t: (key) => key
      }
    })
    await flushPromises()
    var dispensationResponse = { Success: true, Data: { dispensationId: 1 }, Error: null }
    actions.removeDispensation.mockReturnValue(dispensationResponse)
    expect(wrapper.find('#addDispensationBtn').exists()).toBe(true)
    var data = { listName: 'DispensationDetailsList', rowData: { DispensationId: 1 } }
    wrapper.vm.deleteItem(data)
    await wrapper.vm.$nextTick()
    expect(actions.removeDispensation.mock.calls).toHaveLength(0)
  })

  it('On clicking on the add button, add form opens and then click on submit button', async () => {
    store.state.Form.isLoading = false
    var sampleResponse = { Success: true, Data: { Permissions: { Add: true, Edit: false, View: false, Delete: false } }, Error: null }
    actions.getDispensationTabPermission.mockReturnValue(sampleResponse)
    const wrapper = mount(Dispensation, {
      store,
      localVue,
      mocks: {
        $t: (key) => key
      }
    })
    await flushPromises()
    await wrapper.find('#addDispensationBtn').trigger('click')
    await wrapper.vm.$nextTick()
    expect(wrapper.findComponent(Form).exists()).toBe(true)
    await wrapper.findComponent(Form).findComponent(Button).trigger('click')
    await wrapper.vm.$nextTick()
    expect(actions.getDispensationTabPermission.mock.calls).toHaveLength(1)
  })
})
