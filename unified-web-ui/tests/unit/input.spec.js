import { mount, createLocalVue } from '@vue/test-utils'
import Vuex from 'vuex'
import FormStore from '@/app/shared/store/modules/form'
import flushPromises from 'flush-promises'
import Vue from 'vue'
import Form from '@/app/shared/components/Form.vue'
import Input from '@/app/shared/components/Input.vue'

const localVue = createLocalVue()

localVue.use(Vuex)
window.EventBus = new Vue()

jest.mock('../../src/utils/toastUtils', () => {
  return {
    __esModule: true,
    defaultToast: jest.fn(),
    ToastType: {
      Success: 'Success',
      Error: 'Error',
      Warning: 'Warning',
      Neutal: 'Neutal'
    }
  }
})

const getStore = (actions) => {
  const formattedActions = {
    ...FormStore.actions,
    ...actions
  }

  return new Vuex.Store({
    modules: {
      Form: {
        state: FormStore.state,
        actions: formattedActions,
        mutations: FormStore.mutations,
        namespaced: true
      }
    }
  })
}




const isRequiredErrorMsg = 'error_field_required'

const formWithOneInput = {
  "Success": true,
  "Data": {
      "Form": {
          "Name": "SingleInputForm1",
          "Title": "_dummy_input_form",
          "Parts": [
              {
                  "FormName": "SingleInputForm1",
                  "Name": "input_form_01",
                  "Title": "",
                  "TitleKey": "input_form_01",
                  "Order": 1,
                  "IsVisible": true,
                  "Id": 119,
                  "Type": "vertical-form-part",
                  "Rows": null,
                  "Columns": null,
                  "RowDataName": "input_form_01",
                  "AllowRowOpen": false,
                  "AllowRowDelete": false,
                  "ListItemTitle": null,
                  "ItemDescriptionField": null,
                  "IsRepeatable": false,
                  "RecordId": null
              }
          ],
          "PartOptions": null,
          "Fields": [
              {
                  "Value": null,
                  "PartName": "input_form_01",
                  "Placeholder": null,
                  "Component": "app-input-field",
                  "IsDisabled": false,
                  "IsVisible": true,
                  "LabelKey": "Validate_input__Required_01",
                  "Label": null,
                  "Name": "Validate_input__Required_01",
                  "IsHierarchySelector": false,
                  "IsRequired": false,
                  "RemoteUrl": null,
                  "Type": null,
                  "AddOn": null,
                  "Options": null,
                  "HierarchyOptions": null,
                  "OptionsWithKeyValue": null,
                  "AdditionalInfo": null,
                  "DefaultVisibilty": false,
                  "Order": 0,
                  "OptionsWithLabel": null,
                  "Validations": {
                      "Or": null,
                      "And": [
                      ]
                  },
                  "ResponseDataPath": null,
                  "OptionDisplayKeys": null,
                  "OptionValueKey": null,
                  "LoadImmediately": false,
                  "HierarchySelectionConfigs": null,
                  "DisabledDateConfig": null,
                  "RemoteUpdateConfig": null,
                  "RowNumber": 16,
                  "ColumnNumber": null,
                  "Id": 11407,
                  "ParentType": "PART",
                  "ParentId": 119,
                  "FieldOptions": null,
                  "ValidationList": "Required",
                  "DefaultValue": null,
                  "Key": "input_form_01Validate_input__Required_01",
                  "HasInfo": false,
                  "InfoText": null,
                  "Config": null,
                  "ColumnWidth": 6,
                  "HasToggleButton": false
              }
          ],
          "Triggers": null,
          "TriggerConfigs": null,
          "ValueDependencies": [],
          "FilterDependencies": [],
          "VisibilityDependencies": [],
          "DateConstraintDependencies": [],
          "IsRequiredDependencies": [],
          "RemoteUpdateConfigs": [],
          "ValuePropertyDependencies": [],
          "CompoundValueDependencies": null,
          "SaveEndpoint": "/api/patients",
          "SaveText": null,
          "SaveTextKey": "_submit"
      },
      "ExistingData": {}
  },
  "Error": null
} 

const getNewForm = () => {
  return JSON.parse(JSON.stringify(formWithOneInput))
}
const dummyValidation = {
  "Type": "Dummy",
  "Max": 0,
  "Min": 0,
  "IsBackendValidation": false,
  "ValidationUrl": null,
  "ShowServerError": false,
  "ErrorTargetField": null,
  "ValidationParams": null,
  "ErrorMessage": "Dummy validation message",
  "ErrorMessageKey": "error_dummy",
  "RequiredOnlyWhen": null,
  "Regex": null
}

const getDummyValidation = () => {
  return [JSON.parse(JSON.stringify(dummyValidation))]
}

describe('Input.vue', () => {
  it('renders props.label when passed', async () => {
    const label = 'new label'
    const formData = getNewForm()
    formData['Data']['Form']['Fields'][0]['Label'] = label
    const wrapper = mount(Form, {
      store: getStore({
        getFormData: async () => (
          Promise.resolve(
            formData
          )
        )
      }),
      localVue,
      propsData: { isEditing: true, name: 'DummyForm' },
      mocks: {
        $t: (key) => key
      }
    })

    await flushPromises()

    expect(wrapper.findComponent(Input).text()).toMatch(label)
  })
})

describe('Input.vue', () => {
  it('display isRequiredErrorMsg when props.isRequired is passed', async () => {
    const label = 'new_label'
    const translatedLabel = 'New label'
    const formData = getNewForm()
    formData['Data']['Form']['Fields'][0]['Label'] = label
    formData['Data']['Form']['Fields'][0]['IsRequired'] = true
    const wrapper = mount(Form, {
      store: getStore({
        getFormData: async () => (
          Promise.resolve(
            formData
          )
        )
      }),
      localVue,
      propsData: { isEditing: true, name: 'DummyForm' },
      mocks: {
        $t: (key) => {
          return {
                      label: translatedLabel,
                      error_field_required: 'This field is required'
                    }
          }
      }
    })
    await flushPromises()
    await wrapper.vm.$nextTick()
    expect(wrapper.find('.error-message').exists()).toBe(true)
    expect(wrapper.find('.error-message').text()).toMatch(isRequiredErrorMsg)
  })
})

describe('Input.vue', () => {
  it('dont display isRequiredErrorMsg when props.isRequired is passed and value is entered', async () => {
    const label = 'new_label'
    const translatedLabel = 'New label'
    const formData = getNewForm()
    formData['Data']['Form']['Fields'][0]['Label'] = label
    formData['Data']['Form']['Fields'][0]['IsRequired'] = true
    const wrapper = mount(Form, {
      store: getStore({
        getFormData: async () => (
          Promise.resolve(
            formData
          )
        )
      }),
      localVue,
      propsData: { isEditing: true, name: 'DummyForm' },
      mocks: {
        $t: (key) => {
          return {
                      label: translatedLabel,
                      error_field_required: 'This field is required'
                    }
          }
      }
    })

    await flushPromises()
    expect(wrapper.findComponent(Input).find('.error-message').text()).toMatch(isRequiredErrorMsg)
    const textInput = wrapper.findComponent(Input).find('input')
    await textInput.setValue('n')
    wrapper.findComponent(Input).vm.$emit('input','n')
    await wrapper.findComponent(Input).vm.$nextTick()
    expect(wrapper.findComponent(Input).find('.error-message').exists()).toBe(false)
  })
})

describe('Input.vue', () => {
  it('display error message when props.minLength is passed', async () => {
    const label = 'new label'
    const minLength = 3
    const formData = getNewForm()
    formData['Data']['Form']['Fields'][0]['Label'] = label
    const wrapper = mount(Form, {
      store: getStore({
        getFormData: async () => (
          Promise.resolve(
            formData
          )
        )
      }),
      localVue,
      propsData: { isEditing: true, name: 'DummyForm' },
      mocks: {
        $t: (key) => key
      }
    })

    await flushPromises()
    wrapper.findComponent(Input).vm.$emit('input','n')
    await wrapper.vm.$nextTick()
    wrapper.findComponent(Input).vm.$props.minLength = minLength    
    await wrapper.vm.$nextTick()
    await wrapper.vm.$nextTick()
    expect(wrapper.findComponent(Input).find('.error-message').exists()).toBe(true)
    expect(wrapper.findComponent(Input).find('.error-message').text()).toMatch('Minimum length should be ' + minLength)
  })
})

describe('Input.vue', () => {
  it('dont display error message when props.minLength is passed and input length is greater than equal to minLength', async () => {
    const label = 'new label'
    const minLength = 3
    const formData = getNewForm()
    formData['Data']['Form']['Fields'][0]['Label'] = label
    const wrapper = mount(Form, {
      store: getStore({
        getFormData: async () => (
          Promise.resolve(
            formData
          )
        )
      }),
      localVue,
      propsData: { isEditing: true, name: 'DummyForm' },
      mocks: {
        $t: (key) => key
      }
    })

    await flushPromises()
    //below line will raise warning as props being changed but can't set child component value any other way, dynamic form does not support 'minLength' property
    wrapper.findComponent(Input).vm.$props.minLength = minLength
    const textInput = wrapper.findComponent(Input).find('input')
    await textInput.setValue('nnn')
    expect(wrapper.findComponent(Input).find('.error-container').exists()).toBe(false)
  })
})

describe('Input.vue', () => {
  it('check if validation is invoked on value change and error message displayed', async () => {
    const label = 'new_label'
    const formData = getNewForm()
    const dummyValidation = getDummyValidation()
    formData['Data']['Form']['Fields'][0]['Label'] = label
    formData['Data']['Form']['Fields'][0]['Validations']['And'] = dummyValidation
    const negativeDummy = (_) => false
    const wrapper = mount(Form, {
      store: getStore({
        getFormData: async () => (
          Promise.resolve(
            formData
          )
        )
      }),
      localVue,
      propsData: { isEditing: true, name: 'DummyForm' },
      mocks: {
        $t: (key) => key
      }
    })
    await flushPromises() 
    await wrapper.findComponent(Input).setData({validationMapper: {Dummy:negativeDummy}})
    wrapper.findComponent(Input).vm.$emit('input','nnn')
    await wrapper.findComponent(Input).vm.$nextTick()
    expect(wrapper.findComponent(Input).find('.error-message').text()).toMatch(dummyValidation[0]['ErrorMessage'])
  })
})

describe('Input.vue', () => {
  it('check if validation is invoked on value change and error message not displayed if validation return true', async () => {
    const label = 'new_label'
    const formData = getNewForm()
    const dummyValidation = getDummyValidation()
    formData['Data']['Form']['Fields'][0]['Label'] = label
    formData['Data']['Form']['Fields'][0]['Validations']['And'] = dummyValidation
    const postiveDummy = (_) => true
    const wrapper = mount(Form, {
      store: getStore({
        getFormData: async () => (
          Promise.resolve(
            formData
          )
        )
      }),
      localVue,
      propsData: { isEditing: true, name: 'DummyForm' },
      mocks: {
        $t: (key) => key
      }
    })
    await flushPromises() 
    await wrapper.findComponent(Input).setData({validationMapper: {Dummy:postiveDummy}})
    await wrapper.findComponent(Input).find('input').setValue('n')
    await wrapper.findComponent(Input).find('input').trigger('change','a')
    expect(wrapper.findComponent(Input).find('error-message').exists()).toBeFalsy()
  })
})