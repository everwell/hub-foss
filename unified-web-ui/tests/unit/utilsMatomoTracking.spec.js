import { initializeMatomo, trackEvent } from '../../src/utils/matomoTracking'

describe('matomo tracking', () => {
  it('testing initialize matomo', () => {
    initializeMatomo()
  })

  it('testing track event', () => {
    trackEvent('Add Test Form', 'FORM_SAVE_SUCCESS', 'Submit')
  })
})
