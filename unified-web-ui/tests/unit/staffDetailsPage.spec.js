import { mount, createLocalVue } from '@vue/test-utils'
import StaffDetails from '../../src/app/Pages/dashboard/StaffDetails/StaffDetails'
import Table from '@/app/shared/components/Table.vue'
import Button from '@/app/shared/components/Button.vue'
import Breadcrumbs from '@/app/shared/components/Breadcrumbs.vue'
import Vuex from 'vuex'
import StaffDetailsPage from '../../src/app/Pages/dashboard/StaffDetails/components/StaffDetailsPage'

const localVue = createLocalVue()
localVue.use(Vuex)
const $t = () => {}
const stateData = {
    "CurrentHierarchyId": 268117,
    "CurrentHierarchyName": "DEMO_STATE",
    "CurrentHierarchyType": "STATE",
    "CurrentHierarchyDisplayType": "State",
    "CurrentHierarchyCanStaffChoosePatients": true,
    "CurrentHierarchyHaveFieldStaff": false,
    "CurrentHierarchyDisplayTypePlural": "States",
    "SubHierarchyType": "DISTRICT",
    "SubHierarchies": [
        {
            "Id": 268123,
            "Code": "987678",
            "Name": "DEMO_DISTRICT"
        }
    ],
    "SubHierarchyDisplayTypePlural": "Districts",
    "ArtHIVLogins": false,
    "typeSpecificMessage": "",
    "Summary": [],
    "DistrictSummary": null,
    "StateSummary": {
        "268123": {
            "DistrictId": 268123,
            "DistrictStaffCount": 0,
            "TuStaffAddedProportion": "100%"
        }
    },
    "AllowedToAddStaff": true,
    "IsViewOnly": false,
    "CanHierarchyHaveStaffPopulated": true,
    "breadcrumbs": [
        {
            "Item1": 15317,
            "Item2": "DemoCountry"
        },
        {
            "Item1": 268117,
            "Item2": "DEMO_STATE"
        }
    ],
    "Columns": [
        {
            "name": "Name",
            "key": "StaffName",
            "sortable": true,
            "clickable": true
        },
        {
            "name": "Code",
            "key": "Code",
            "sortable": false,
            "clickable": false
        },
        {
            "name": "District Staff Count",
            "key": "DistrictStaffCount",
            "sortable": false,
            "clickable": false
        },
        {
            "name": "% of TUs with Staff Details",
            "key": "TUswithStaffDetails",
            "sortable": false,
            "clickable": false
        }
    ],
    "ColumnsData": [
        {
            "StaffName": "DEMO_DISTRICT",
            "Code": "987678",
            "DistrictStaffCount": 0,
            "TUswithStaffDetails": "100%",
            "TUStaffCount": null,
            "Clickable": true,
            "ClickableLink": 268123
        }
    ]
}
describe('Staff Details Tests', () => {
    let store
    let actions
    beforeEach(() => {
      actions = {
        deleteFieldStaff: jest.fn(),
        setViewOnly: jest.fn(),
        setPreviousHierarchyId: jest.fn()
      }
      store = new Vuex.Store({
        modules: {
          StaffDetails: {
            namespaced: true,
            state: {
                staffDetailsLoading: true,
                currentHierarchyId: -1,
                currentHierarchyName: '',
                currentHierarchyType: '',
                currentHierarchyDisplayType: '',
                currentHierarchyCanStaffChoosePatients: false,
                subHierarchies: [],
                currentHierarchyDisplayTypePlural: '',
                currentHierarchyHaveFieldStaff: '',
                subHierarchyType: '',
                subHierarchyDisplayTypePlural: '',
                artHIVLogins: false,
                typeSpecificMessage: '',
                summary: [],
                districtSummary: '',
                stateSummary: '',
                allowedToAddStaff: false,
                isViewOnly: false,
                canHierarchyHaveStaffPopulated: false,
                breadcrumbs: [],
                columnsData: [],
                columns: []
            },
            actions
          },
          AddStaff: {
            namespaced: true,
            state: {},
            actions
          }
        }
      })
    })

    it('State data display', () => {
        store.state.StaffDetails.subHierarchyType = stateData.SubHierarchyType
        store.state.StaffDetails.breadcrumbs = stateData.breadcrumbs
        store.state.StaffDetails.canHierarchyHaveStaffPopulated = stateData.CanHierarchyHaveStaffPopulated
        const wrapper = mount(StaffDetailsPage, { store, localVue, mocks: { $t } })
        expect(wrapper.find('.breadcrumb').exists()).toBe(true)
        expect(wrapper.find('.alert-info').exists()).toBe(true)
        expect(wrapper.findComponent(Button).exists()).toBe(true)
        wrapper.findAllComponents(Button).at(0).vm.$emit('click')
    })

    it('load table for State data', async () => {
        store.state.StaffDetails.columnsData = stateData.ColumnsData
        store.state.StaffDetails.columns = stateData.Columns
        store.state.StaffDetails.subHierarchies = stateData.SubHierarchies
        store.state.StaffDetails.subHierarchyType = stateData.SubHierarchyType
        store.state.StaffDetails.canHierarchyHaveStaffPopulated = false
        const wrapper = mount(StaffDetailsPage, { store, localVue, mocks: { $t }, computed: {} })
        expect(wrapper.find('.card').exists()).toBe(true)
        expect(wrapper.findComponent(Table).exists()).toBe(true)
        global.confirm = () => true
        wrapper.find(Table).vm.$emit('clickTableEvent','StaffName','')
        wrapper.find(Table).vm.$emit('deleteRowEvent', '')
        await wrapper.vm.$nextTick()
    })
    it('click table event', async () => {
        store.state.StaffDetails.columnsData = stateData.ColumnsData
        store.state.StaffDetails.columns = stateData.Columns
        store.state.StaffDetails.subHierarchies = stateData.SubHierarchies
        store.state.StaffDetails.subHierarchyType = stateData.SubHierarchyType
        store.state.StaffDetails.canHierarchyHaveStaffPopulated = true
        const wrapper = mount(StaffDetailsPage, { store, localVue, mocks: { $t }, computed: {} })
        global.confirm = () => true
        wrapper.find(Table).vm.$emit('clickTableEvent','HierarchyName','')
        await wrapper.vm.$nextTick()
    })
})