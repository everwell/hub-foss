import { mount, createLocalVue } from '@vue/test-utils'
import { Map, List } from 'immutable'
import Form from '@/app/shared/components/Form.vue'
import flushPromises from 'flush-promises'
import Reports from '../../src/app/Pages/dashboard/Reports/Reports'
import Vuex from 'vuex'
import Vue from 'vue'

const localVue = createLocalVue()

localVue.use(Vuex)
window.EventBus = new Vue()
const $t = () => {}

describe('Reports.vue', () => {
  let store
  let actions
  beforeEach(() => {
    actions = {
      getReportData: jest.fn(),
      endReportActive: jest.fn(),
    }
    store = new Vuex.Store({
      modules: {
        Reports: {
          namespaced: true,
            state: {
            isReportsLoading: false,
            reportsObject: List(),
            isFormActive: false,
            activeReportTitle: {},
            isReportDownloading: false
          },
          actions
        },
        Form: {
          namespaced: true,
          state: {
            isLoading: false,
          }
        }
      }
    })
  })
  it('calls loadOptions on load', () => {
        const wrapper = mount(Reports, { store, localVue, mocks: { $t } })
        wrapper.vm.$nextTick()
        expect(actions.getReportData.mock.calls).toHaveLength(1)
    })
  
  it('render reports page', async () => {
    const wrapper = mount(Reports, {
      store,
      localVue,
      mocks: {
        $t: (key) => key
      }
    })
    await flushPromises()
    expect(wrapper.find('.reports-page').exists()).toBe(true)
  })

  it('Loading to be true', async () => {
    store.state.Reports.isReportsLoading = true
    const wrapper = mount(Reports, {
      store,
      localVue,
      mocks: {
        $t: (key) => key
      }
    })

    await flushPromises()
    expect(wrapper.find('.container').exists()).toBe(false)
  })
  
  it('Buttons rendered', async () => {
    store.state.Form.isLoading = false
    store.state.Reports.isFormActive = true
    store.state.Reports.isReportsLoading = false
    const wrapper = mount(Reports, {
      store,
      localVue,
      mocks: {
        $t: (key) => key
      }
    })

    await flushPromises()
    expect(wrapper.find('#ReportsFilterCancelBtn').exists()).toBe(true)
    await wrapper.find('#ReportsFilterCancelBtn').trigger('click')
    wrapper.vm.$nextTick()
    expect(wrapper.find('#ReportsFiltersSubmitBtn').exists()).toBe(false)
  })
  
  it('Download Report', async () => {
    store.state.Form.isLoading = false
    store.state.Reports.isFormActive = true
    store.state.Reports.isReportsLoading = false
    const formSavedSuccessfully = jest.fn()
    const wrapper = mount(Reports, {
      store,
      localVue,
      mocks: {
        $t: (key) => key
      }
    })
    wrapper.setMethods({
      formSavedSuccessfully: formSavedSuccessfully
    })
    await flushPromises()
    expect(wrapper.findComponent(Form).exists()).toBe(true)
    expect(wrapper.find('#nullSubmitBtn').exists()).toBe(true)
    await wrapper.find('#nullSubmitBtn').trigger('click')
    wrapper.vm.$nextTick()
    /* eslint-disable */
    expect(formSavedSuccessfully).toHaveBeenCalled
    /* eslint-enable */
    
  })
  
})
