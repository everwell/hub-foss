
import {createLocalVue, mount, shallowMount } from '@vue/test-utils'
import Vuex from 'vuex'
import flushPromises from 'flush-promises'
import Vue from 'vue'
import {hierarchyDetailsResponse, parentMonitoringMethodsResponse, communicationConfigResponse, allHierarchyResponse} from './hierarchy-management-data' 
import i18n from '@/plugins/i18n'
import HierarchyManagementStore from '@/app/shared/store/modules/HiearachyManagement/index.js'
import HierarchyManagement from '@/app/Pages/dashboard/HierarchyManagement/HierarchyManagement.vue'
import TreeView from '@/app/Pages/dashboard/HierarchyManagement/components/TreeView.vue'
import HierarchyDetails from '@/app/Pages/dashboard/HierarchyManagement/components/HierarchyDetails.vue'
import Vuetify from 'vuetify'
// import Vuetify from 'vuetify/lib/framework'
import  VueRouter  from 'vue-router'
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome'
import { library } from '@fortawesome/fontawesome-svg-core'
import { faTrash } from '@fortawesome/free-solid-svg-icons/faTrash'
import { faSearch } from '@fortawesome/free-solid-svg-icons/faSearch'
import Tabs from '@/app/shared/components/Tabs.vue'
import { homeRoutes } from '@/app/Pages/home'
import { dashboardRoutes } from '@/app/Pages/dashboard'
import Button from '@/app/shared/components/Button.vue'
import Checkbox from '@/app/shared/components/CheckboxGroup.vue'
import Input from '@/app/shared/components/Input.vue'
import Modal from '@/app/shared/components/Modal.vue'
import AddNewHierarchy from '@/app/Pages/dashboard/HierarchyManagement/components/AddNewHierarchy.vue'
import LoginsManagement from '@/app/Pages/dashboard/HierarchyManagement/components/LoginsManagement.vue'
import Communications from '@/app/Pages/dashboard/HierarchyManagement/components/Communications.vue'
jest.mock("../../src/app/shared/store/Api", () => ({
    errorCallback: jest.fn(( params ) => {
    }),
    ApiServerClient: {get: jest.fn(( url, params ) => {
        if(url === 'api/Hierarchy/GetHierarchies') {
            return Promise.resolve(
                // [{ "Id": 15317, "Level": 1, "Name": "DemoCountry", "Type": "COUNTRY", "Code": "DEMOC", "ParentId": null, "DrugRegimen": "FDC", "CountryCode": "DEMOC", "Level1Id": null, "Level1Name": null, "Level1Type": null, "Level1Code": null, "Level2Id": null, "Level2Name": null, "Level2Type": null, "Level2Code": null, "Level3Id": null, "Level3Name": null, "Level3Type": null, "Level3Code": null, "Level4Id": null, "Level4Name": null, "Level4Type": null, "Level4Code": null, "Level5Id": null, "Level5Name": null, "Level5Type": null, "Level5Code": null, "Level6Id": null, "Level6Name": null, "Level6Type": null, "Level6Code": null, "ShowChildrenInPatientList": true, "HasChildren": true, "HasMerm": true, "Has99DOTS": true, "Has99DOTSLite": true, "HasNONE": true, "HasVOT": true }, { "Id": 15318, "Level": 2, "Name": "DemoState", "Type": "STATE", "Code": "DEMO", "ParentId": 15317, "DrugRegimen": "FDC", "CountryCode": "DEMOC", "Level1Id": 15317, "Level1Name": "DemoCountry", "Level1Type": "COUNTRY", "Level1Code": "DEMOC", "Level2Id": null, "Level2Name": null, "Level2Type": null, "Level2Code": null, "Level3Id": null, "Level3Name": null, "Level3Type": null, "Level3Code": null, "Level4Id": null, "Level4Name": null, "Level4Type": null, "Level4Code": null, "Level5Id": null, "Level5Name": null, "Level5Type": null, "Level5Code": null, "Level6Id": null, "Level6Name": null, "Level6Type": null, "Level6Code": null, "ShowChildrenInPatientList": true, "HasChildren": true, "HasMerm": true, "Has99DOTS": true, "Has99DOTSLite": true, "HasNONE": true, "HasVOT": true }]
                [{ "Id": 15317, "Level": 1, "Name": "DemoCountry", "Type": "COUNTRY", "Code": "DEMOC", "ParentId": null, "DrugRegimen": "FDC", "CountryCode": "DEMOC", "Level1Id": null, "Level1Name": null, "Level1Type": null, "Level1Code": null, "Level2Id": null, "Level2Name": null, "Level2Type": null, "Level2Code": null, "Level3Id": null, "Level3Name": null, "Level3Type": null, "Level3Code": null, "Level4Id": null, "Level4Name": null, "Level4Type": null, "Level4Code": null, "Level5Id": null, "Level5Name": null, "Level5Type": null, "Level5Code": null, "Level6Id": null, "Level6Name": null, "Level6Type": null, "Level6Code": null, "ShowChildrenInPatientList": true, "HasChildren": true, "HasMerm": true, "Has99DOTS": true, "Has99DOTSLite": true, "HasNONE": true, "HasVOT": true }, { "Id": 15318, "Level": 2, "Name": "DemoState", "Type": "STATE", "Code": "DEMO", "ParentId": 15317, "DrugRegimen": "FDC", "CountryCode": "DEMOC", "Level1Id": 15317, "Level1Name": "DemoCountry", "Level1Type": "COUNTRY", "Level1Code": "DEMOC", "Level2Id": null, "Level2Name": null, "Level2Type": null, "Level2Code": null, "Level3Id": null, "Level3Name": null, "Level3Type": null, "Level3Code": null, "Level4Id": null, "Level4Name": null, "Level4Type": null, "Level4Code": null, "Level5Id": null, "Level5Name": null, "Level5Type": null, "Level5Code": null, "Level6Id": null, "Level6Name": null, "Level6Type": null, "Level6Code": null, "ShowChildrenInPatientList": true, "HasChildren": true, "HasMerm": true, "Has99DOTS": true, "Has99DOTSLite": true, "HasNONE": true, "HasVOT": true },{ "Id": 15319, "Level": 3, "Name": "DemoState", "Type": "STATE", "Code": "DEMO", "ParentId": 15318, "DrugRegimen": "FDC", "CountryCode": "DEMOC", "Level1Id": 15318, "Level1Name": "DemoCountry", "Level1Type": "COUNTRY", "Level1Code": "DEMOC", "Level2Id": null, "Level2Name": null, "Level2Type": null, "Level2Code": null, "Level3Id": null, "Level3Name": null, "Level3Type": null, "Level3Code": null, "Level4Id": null, "Level4Name": null, "Level4Type": null, "Level4Code": null, "Level5Id": null, "Level5Name": null, "Level5Type": null, "Level5Code": null, "Level6Id": null, "Level6Name": null, "Level6Type": null, "Level6Code": null, "ShowChildrenInPatientList": true, "HasChildren": true, "HasMerm": true, "Has99DOTS": true, "Has99DOTSLite": true, "HasNONE": true, "HasVOT": true }]
            )
        } else if (url === 'api/Admin/HierarchyDetailsJSON/15317') {
            return Promise.resolve(
                {"success":true,"result":{"basicDetails":"{\"name\":\"DemoState\",\"id\":15318,\"parentId\":15317,\"level\":2,\"code\":\"DEMO\",\"type\":\"STATE\",\"cases\":2,\"children\":9,\"siblings\":16,\"hasMERM\":\"true\",\"hasVOT\":\"true\",\"has99DOTS\":\"true\",\"has99DOTSLite\":\"true\",\"hasNONE\":\"true\"}","loginDetails":[{"text":"james_demo_state","id":16730},{"text":"test-demo-login","id":163640},{"text":"ttttttttt","id":163657},{"text":"testtest","id":163749},{"text":"testtele","id":163879}]}})
        } else if (url === 'api/Admin/HierarchyDetailsJSON/15318') {
          return Promise.resolve(
              {"success":true,"result":{"basicDetails":"{\"name\":\"DemoState\",\"id\":15318,\"parentId\":15317,\"level\":2,\"code\":\"DEMO\",\"type\":\"STATE\",\"cases\":2,\"children\":9,\"siblings\":16,\"hasMERM\":\"true\",\"hasVOT\":\"true\",\"has99DOTS\":\"true\",\"has99DOTSLite\":\"true\",\"hasNONE\":\"true\"}","loginDetails":[{"text":"james_demo_state","id":16730},{"text":"test-demo-login","id":163640},{"text":"ttttttttt","id":163657},{"text":"testtest","id":163749},{"text":"testtele","id":163879}]}})
        }else if (url === 'api/Admin/ParentMonitoringMethods'){
            return Promise.resolve(
                {"success":true,"result":"{\"hasMERM\":true,\"hasVOT\":true,\"has99DOTS\":true,\"has99DOTSLite\":true,\"hasNONE\":true}"}
            )
        } else if (url === 'api/Admin/HierarchyConfigValues'){
          if (params.configName === 'TypeOfPatient') {
            return Promise.resolve(
              ["ART","NONE","RNTCP","CommunityDOTS","Private","PublicPrivatePartnership"]
            )
          } else if (params.configName === 'AccessType') {
            return Promise.resolve(
              ["TB"]
            )
          } else {
            console.log('config fetch for '+params.id)
            if (params.id === 15319){
              return Promise.resolve(
                []
              )
            }
          return Promise.resolve(
            ["ART","BMU","CENTER","CONSULTANT","DEPLOYMENT","DISTRICT","DIVISION","FACILITY","HUB","KNCVDEMOCOUNTRY","KNCVDEMODISTRICT","NGO","OBLAST","PHI","PROVIDER","PROVINCE","PVTLAB","RAYON","REGION","STATE","TOWNSHIP","TU","UPAZILLA","ZONE","WOREDA"]
          )
        }
      } else if (url === 'api/Admin/CommunicationConfig') {
        return Promise.resolve( {"success":true,"data":{"availableLanguages":{"1":"ENGLISH","2":"BANGLA","3":"SWAHILI"},"selectedLanguages":["1","2","3"]},"error":null})
      }  
    }),
    put: jest.fn(( url ) => {
        console.log('logging url-------')
        console.log(url)
        if (url === 'api/Hierarchy/UpdateHierarchy') {
            return {Success: true}
        }
    }),
    post: jest.fn(( url ) => {
        console.log('logging url-------')
        console.log(url)
        if (url === 'api/Hierarchy/CreateHierarchy') {
          return {Success: true}
        } else if (url === 'Api/Account/ResetPasswordByAdmin') {
          return {success: true}
        } else if (url === 'Api/Account/CreateLogin/') {
          return {success: true}
        } else if (url === 'api/Admin/UpdateCommunicationConfig') {
          return {success: true, data: {}}
        }
    })
  }}));
const localVue = createLocalVue()
library.add(faSearch)
library.add(faTrash)
Vue.use(Vuetify)
Vue.use(VueRouter)
Vue.component('font-awesome-icon', FontAwesomeIcon)
// const routes = [...homeRoutes, ...dashboardRoutes]
dashboardRoutes[0].children.splice(0,8)
const routes= dashboardRoutes
localVue.use(Vuex)
window.EventBus = new Vue()


jest.mock('../../src/utils/toastUtils', () => {
  return {
    __esModule: true,
    defaultToast: jest.fn(),
    ToastType: {
      Success: 'Success',
      Error: 'Error',
      Warning: 'Warning',
      Neutal: 'Neutal'
    }
  }
})

const getStore = (actions) => {
    const formattedActions = {
      ...HierarchyManagementStore.actions,
      ...actions
    }
  
    return new Vuex.Store({
      modules: {
        HiearachyManagement: {
          state: HierarchyManagementStore.state,
          actions: formattedActions,
          mutations: HierarchyManagementStore.mutations,
          namespaced: true
        }
      }
    })
}


describe('HierarchyManagment.vue', () => {
    it('renders hierarchy details on node click', async () => {
        const router = new VueRouter({routes})
        const wrapper = mount(HierarchyManagement, {
            router,
            store: getStore({}),
            localVue,
            mocks: {
                $t: (key) => key
              },
            i18n  
        })
        await flushPromises()
        expect(wrapper.find('.v-treeview').exists()).toBeTruthy()
        wrapper.findComponent(TreeView).vm.$options.watch.allHierarchyResponse.call(wrapper.findComponent(TreeView).vm)
        await wrapper.vm.$nextTick()
        expect(wrapper.findComponent(TreeView).find('.v-treeview-node').exists()).toBeTruthy()
        await wrapper.find('.v-treeview-node__root').trigger('click')
        expect(wrapper.findComponent(Tabs).exists()).toBeTruthy()
        await wrapper.findComponent(Tabs).findAll('.tab_title').at(0).trigger('click.native')
        await wrapper.vm.$nextTick()
        expect(wrapper.findComponent(HierarchyDetails).exists()).toBeTruthy()
      })
})
describe('HierarchyManagment.vue', () => {
  it('searches for hierarchy in the tree by name ', async () => {
      const router = new VueRouter({routes})
      const wrapper = mount(HierarchyManagement, {
          router,
          store: getStore({}),
          localVue,
          mocks: {
              $t: (key) => key
            }
      })
      await flushPromises()
      expect(wrapper.find('.v-treeview').exists()).toBeTruthy()
      wrapper.findComponent(TreeView).vm.$options.watch.allHierarchyResponse.call(wrapper.findComponent(TreeView).vm)
      await wrapper.vm.$nextTick()
      expect(wrapper.findComponent(TreeView).find('.v-treeview-node').exists()).toBeTruthy()
      expect(wrapper.findComponent(TreeView).findComponent(Input).exists()).toBeTruthy()
      wrapper.findComponent(TreeView).findComponent(Input).vm.$emit('change','demo')
      await wrapper.vm.$nextTick()
      await wrapper.vm.$nextTick()
      expect(wrapper.findComponent(TreeView).find('.v-treeview-node').exists()).toBeTruthy()
    })
})
describe('HierarchyManagment.vue', () => {
    it('updates and adds new hierarchy', async () => {
        const router = new VueRouter({routes})
        const wrapper = mount(HierarchyManagement, {
            router,
            store: getStore({}),
            localVue,
            mocks: {
                $t: (key) => key
              }
        })
        await flushPromises()
        expect(wrapper.find('.v-treeview').exists()).toBeTruthy()
        wrapper.findComponent(TreeView).vm.$options.watch.allHierarchyResponse.call(wrapper.findComponent(TreeView).vm)
        await wrapper.vm.$nextTick()
        expect(wrapper.findComponent(TreeView).find('.v-treeview-node').exists()).toBeTruthy()
        await wrapper.find('.v-treeview-node__root').trigger('click')
        expect(wrapper.findComponent(Tabs).exists()).toBeTruthy()
        await wrapper.findComponent(Tabs).findAll('.tab_title').at(0).trigger('click.native')
        await wrapper.vm.$nextTick()
        expect(wrapper.findComponent(HierarchyDetails).exists()).toBeTruthy()
        expect(wrapper.findComponent(HierarchyDetails).findComponent(Checkbox).props('isDisabled')).toBe(true)
        expect(wrapper.findComponent(HierarchyDetails).findAllComponents(Button).at(0).exists()).toBeTruthy()
        expect(wrapper.findComponent(HierarchyDetails).findAllComponents(Button).at(0).props('label')).toBe('edit_details')
        await wrapper.findComponent(HierarchyDetails).findAllComponents(Button).at(0).find('button').trigger('click')
        expect(wrapper.findComponent(HierarchyDetails).findAllComponents(Button).at(0).props('label')).toBe('save_changes')
        expect(wrapper.findComponent(HierarchyDetails).findAllComponents(Button).at(1).props('label')).toBe('cancel')
        await wrapper.findComponent(HierarchyDetails).findAllComponents(Button).at(1).find('button').trigger('click')
        expect(wrapper.findComponent(HierarchyDetails).findComponent(Checkbox).props('isDisabled')).toBe(true)
        expect(wrapper.findComponent(HierarchyDetails).findAllComponents(Button).at(0).props('label')).toBe('edit_details')
        await wrapper.findComponent(HierarchyDetails).findAllComponents(Button).at(0).find('button').trigger('click')
        expect(wrapper.findComponent(HierarchyDetails).findComponent(Checkbox).props('isDisabled')).toBe(false)
        await wrapper.findComponent(HierarchyDetails).setData({localHierarchyBasicDetailsName: '', localHierarchyBasicDetailsCode: 'abc11'})
        await wrapper.vm.$nextTick()
        expect(wrapper.findComponent(HierarchyDetails).findAllComponents(Button).at(0).props('disabled')).toBe(true)
        await wrapper.findComponent(HierarchyDetails).setData({localHierarchyBasicDetailsName: 'democ', localHierarchyBasicDetailsCode: 'DEMOC'})
        await wrapper.vm.$nextTick()
        expect(wrapper.findComponent(HierarchyDetails).findAllComponents(Button).at(0).props('disabled')).toBe(false)
        await wrapper.findComponent(HierarchyDetails).findAllComponents(Button).at(0).find('button').trigger('click')
        await wrapper.vm.$nextTick()
        expect(wrapper.findComponent(HierarchyDetails).findAllComponents(Button).at(0).props('label')).toBe('edit_details')
        expect(wrapper.findComponent(HierarchyDetails).findComponent(Checkbox).props('isDisabled')).toBe(true)
        expect(wrapper.findComponent(HierarchyDetails).findAllComponents(Button).at(1).props('label')).toBe('add_hierarchy_at_this_level')
        await wrapper.findComponent(HierarchyDetails).findAllComponents(Button).at(1).find('button').trigger('click')
        await wrapper.vm.$nextTick()
        await wrapper.vm.$nextTick()
        await wrapper.vm.$nextTick()
        expect(wrapper.findComponent(Modal).exists()).toBeTruthy()
        expect(wrapper.findComponent(AddNewHierarchy).exists()).toBeTruthy()
        wrapper.findComponent(AddNewHierarchy).setData({type: 'ART', name: 'dfg', code:'ABCDEFG10',monitoringMethods: ['has99DOTS']})
        await wrapper.vm.$nextTick()
        await wrapper.vm.$nextTick()
        await wrapper.findComponent(AddNewHierarchy).findComponent(Modal).vm.$emit('click')
        await wrapper.vm.$nextTick()
        await wrapper.vm.$nextTick()
        expect(wrapper.findComponent(Modal).exists()).toBeFalsy()
        expect(wrapper.findComponent(HierarchyDetails).findAllComponents(Button).at(2).props('label')).toBe('add_hierarchy_below_this')
        await wrapper.findComponent(HierarchyDetails).findAllComponents(Button).at(2).find('button').trigger('click')
        await wrapper.vm.$nextTick()
        await wrapper.vm.$nextTick()
        await wrapper.vm.$nextTick()
        expect(wrapper.findComponent(Modal).exists()).toBeTruthy()
        expect(wrapper.findComponent(AddNewHierarchy).exists()).toBeTruthy()
        await wrapper.findComponent(AddNewHierarchy).setData({type: 'ART', name: 'dfg', code:'ABCDEFG10',monitoringMethods: ['has99DOTS']})
        await wrapper.vm.$nextTick()
        await wrapper.vm.$nextTick()
        await wrapper.findComponent(AddNewHierarchy).findComponent(Modal).vm.$emit('click')
        await wrapper.vm.$nextTick()
        await wrapper.vm.$nextTick()
        expect(wrapper.findComponent(Modal).exists()).toBeFalsy()
      })
    it('Edits and Adds new logins', async () => {
      const router = new VueRouter({routes})
      const wrapper = mount(HierarchyManagement, {
          router,
          store: getStore({}),
          localVue,
          mocks: {
              $t: (key) => key
            }
      })
      await flushPromises()
      router.push('/dashboard/hierarchymanagement/logins/15318')
      expect(wrapper.find('.v-treeview').exists()).toBeTruthy()
      wrapper.findComponent(TreeView).vm.$options.watch.allHierarchyResponse.call(wrapper.findComponent(TreeView).vm)
      await wrapper.vm.$nextTick()
      expect(wrapper.findComponent(TreeView).find('.v-treeview-node').exists()).toBeTruthy()
      await wrapper.find('.v-treeview-node__root').trigger('click')
      expect(wrapper.findComponent(Tabs).exists()).toBeTruthy()
      await wrapper.findComponent(Tabs).findAll('.tab_title').at(1).trigger('click.native')
      await wrapper.vm.$nextTick()
      expect(wrapper.findComponent(LoginsManagement).exists()).toBeTruthy()
      await wrapper.vm.$nextTick()
      await wrapper.vm.$nextTick()
      await wrapper.vm.$nextTick()
      expect(wrapper.findComponent(LoginsManagement).find('.right-input').findAll('.display-inlined').at(0).exists()).toBeTruthy()
      await wrapper.findComponent(LoginsManagement).find('.right-input').findAll('.display-inlined').at(0).trigger('click')
      expect(wrapper.findComponent(LoginsManagement).findComponent(Modal).exists()).toBeTruthy()
      expect(wrapper.findComponent(LoginsManagement).findComponent(Modal).props('headingText')).toBe('change_password')
      await wrapper.findComponent(LoginsManagement).setData({resetPassword: 'abcde', reResetPassword: 'abcdef'})
      await wrapper.vm.$nextTick()
      expect(wrapper.findComponent(LoginsManagement).findComponent(Modal).props('disableConfirmBtn')).toBe(true)
      await wrapper.findComponent(LoginsManagement).setData({resetPassword: 'abcde', reResetPassword: 'abcde'})
      await wrapper.vm.$nextTick()
      expect(wrapper.findComponent(LoginsManagement).findComponent(Modal).props('disableConfirmBtn')).toBe(true)
      await wrapper.findComponent(LoginsManagement).setData({resetPassword: 'abcde1', reResetPassword: 'abcde1'})
      await wrapper.vm.$nextTick()
      expect(wrapper.findComponent(LoginsManagement).findComponent(Modal).props('disableConfirmBtn')).toBe(true)
      await wrapper.findComponent(LoginsManagement).setData({resetPassword: 'abcde1@', reResetPassword: 'abcde1@'})
      await wrapper.vm.$nextTick()
      expect(wrapper.findComponent(LoginsManagement).findComponent(Modal).props('disableConfirmBtn')).toBe(true)
      await wrapper.findComponent(LoginsManagement).setData({resetPassword: 'abcde1@XZ', reResetPassword: 'abcde1@XZ'})
      await wrapper.vm.$nextTick()
      expect(wrapper.findComponent(LoginsManagement).findComponent(Modal).props('disableConfirmBtn')).toBe(false)
      wrapper.findComponent(LoginsManagement).findComponent(Modal).vm.$emit('click')
      await wrapper.vm.$nextTick()
      await wrapper.vm.$nextTick()
      expect(wrapper.findComponent(LoginsManagement).findComponent(Modal).exists()).toBeFalsy()
      expect(wrapper.findComponent(LoginsManagement).findComponent(Button).props('label')).toBe('create_login')
      await wrapper.findComponent(LoginsManagement).findComponent(Button).find('button').trigger('click')
      await wrapper.vm.$nextTick()
      await wrapper.vm.$nextTick()
      expect(wrapper.findComponent(LoginsManagement).findComponent(Modal).exists()).toBeTruthy()
      expect(wrapper.findComponent(LoginsManagement).findComponent(Modal).props('headingText')).toBe('create_login')
      await wrapper.findComponent(LoginsManagement).setData({newUsername: '',newPassword: 'abcde1@', rePassword: 'abcde1@', typeOfPatients: []})
      await wrapper.vm.$nextTick()
      expect(wrapper.findComponent(LoginsManagement).findComponent(Modal).props('disableConfirmBtn')).toBe(true)
      await wrapper.findComponent(LoginsManagement).setData({newUsername: 'akdsh',newPassword: 'abcde1@XY', rePassword: 'abcde1@XY', typeOfPatients: ['ART']})
      await wrapper.vm.$nextTick()
      expect(wrapper.findComponent(LoginsManagement).findComponent(Modal).props('disableConfirmBtn')).toBe(false)
      wrapper.findComponent(LoginsManagement).findComponent(Modal).vm.$emit('click')
      await wrapper.vm.$nextTick()
      await wrapper.vm.$nextTick()
      await wrapper.vm.$nextTick()
      await wrapper.vm.$nextTick()
      expect(wrapper.findComponent(LoginsManagement).findComponent(Modal).exists()).toBeFalsy()
    })
    it('Edits Communication', async () => {
      const router = new VueRouter({routes})
      const wrapper = mount(HierarchyManagement, {
          router,
          store: getStore({}),
          localVue,
          mocks: {
              $t: (key) => key
            }
      })
      await flushPromises()
      router.push('/dashboard/hierarchymanagement/communication/15318')
      expect(wrapper.find('.v-treeview').exists()).toBeTruthy()
      wrapper.findComponent(TreeView).vm.$options.watch.allHierarchyResponse.call(wrapper.findComponent(TreeView).vm)
      await wrapper.vm.$nextTick()
      expect(wrapper.findComponent(TreeView).find('.v-treeview-node').exists()).toBeTruthy()
      await wrapper.find('.v-treeview-node__root').trigger('click')
      await wrapper.vm.$nextTick()
      await wrapper.vm.$nextTick()
      await wrapper.vm.$nextTick()
      expect(wrapper.findComponent(Tabs).exists()).toBeTruthy()
      await wrapper.findComponent(Tabs).findAll('.tab_title').at(2).trigger('click.native')
      await wrapper.vm.$nextTick()
      expect(wrapper.findComponent(Communications).exists()).toBeTruthy()
      await wrapper.vm.$nextTick()
      expect(wrapper.findComponent(Communications).findComponent(Button).exists()).toBeTruthy()
      expect(wrapper.findComponent(Communications).findComponent(Checkbox).props('isDisabled')).toBeTruthy()
      expect(wrapper.findComponent(Communications).findComponent(Button).props('label')).toBe('edit')
      wrapper.findComponent(Communications).findComponent(Button).find('button').trigger('click')
      await wrapper.vm.$nextTick()
      await wrapper.vm.$nextTick()
      expect(wrapper.findComponent(Communications).findComponent(Checkbox).props('isDisabled')).toBeFalsy()
      await wrapper.findComponent(Communications).setData({selectedCommunications: []})
      await wrapper.vm.$nextTick()
      expect(wrapper.findComponent(Communications).findComponent(Button).props('label')).toBe('save_changes')
      expect(wrapper.findComponent(Communications).findComponent(Button).props('disabled')).toBe(true)
      await wrapper.findComponent(Communications).setData({selectedCommunications: [1]})
      await wrapper.vm.$nextTick()
      expect(wrapper.findComponent(Communications).findComponent(Button).props('label')).toBe('save_changes')
      expect(wrapper.findComponent(Communications).findComponent(Button).props('disabled')).toBe(false)
      wrapper.findComponent(Communications).findComponent(Button).find('button').trigger('click')
      await wrapper.vm.$nextTick()
      await wrapper.vm.$nextTick()
      expect(wrapper.findComponent(Communications).findComponent(Button).props('label')).toBe('edit')
    })
})