import Vue from 'vue'
import VueMatomo from 'vue-matomo'

export const initMatomo = (host, siteId) => {
  Vue.use(VueMatomo, {
    host: host,
    siteId: siteId,
    trackerFileName: 'matomo',
    enableLinkTracking: true,
    requireConsent: false,
    trackInitialView: true,
    disableCookies: false,
    enableHeartBeatTimer: false,
    heartBeatTimerInterval: 15,
    debug: false,
    userId: undefined,
    cookieDomain: undefined,
    domains: undefined,
    preInitActions: []
  })
}

export const trackEvent = (eventCategory, eventAction, eventName) => {
  window._paq.push(['trackEvent', eventCategory, eventAction, eventName])
}

export const initializeMatomo = () => {
  initMatomo(process.env.VUE_APP_MATOMO_URL, process.env.VUE_APP_MATOMO_SITE_ID)
}

export default {
  name: 'MatomoTracking',
  initializeMatomo,
  trackEvent
}
