import { ApiServerClient, errorCallback } from '../../Api'
import { defaultToast, ToastType } from '../../../../../utils/toastUtils'

export default {
  namespaced: true,
  state: {
    isViewOnly: false,
    addStaffObj: {},
    formloading: true,
    editMode: false,
    previousHierarchyId: 0
  },
  mutations: {
    SET_PREVIOUS_HIERARCHY_ID (state, id) {
      state.previousHierarchyId = id
    },
    SET_IS_VIEW_ONLY (state, breadcrumbs) {
      state.isViewOnly = breadcrumbs
    },
    SET_ADD_STAFF_OBJ (state, obj) {
      state.addStaffObj = obj
    },
    SET_FORM_LOADING (state, status) {
      state.formloading = status
    },
    SET_EDIT_MODE (state, status) {
      state.editMode = status
    }
  },
  actions: {
    async deleteFieldStaff ({ commit }, { params }) {
      const url = '/dashboard/DeleteFieldStaff'
      try {
        const deleted = await ApiServerClient.get(url, params, errorCallback)
        if (!deleted) {
          defaultToast(ToastType.Error, 'Something went wrong. Please try again later.')
        }
      } catch (e) {
        errorCallback(e)
      }
    },
    async fieldStaffDetails ({ commit }, { params }) {
      const url = '/api/fieldstaff/Staff'
      commit('SET_FORM_LOADING', true)
      try {
        const staffObj = await ApiServerClient.get(url, params, errorCallback)
        commit('SET_ADD_STAFF_OBJ', staffObj)
        if (!staffObj) {
          defaultToast(ToastType.Error, 'Something went wrong. Please try again later.')
        }
      } catch (e) {
        errorCallback(e)
      }
      commit('SET_FORM_LOADING', false)
    },
    async updateFieldStaff ({ commit }, { params }) {
      const url = '/dashboard/UpdateStaff'
      try {
        const updated = await ApiServerClient.post(url, params, errorCallback)
        if (!updated.Success) {
          defaultToast(ToastType.Error, 'Something went wrong. Please try again later.')
        }
      } catch (e) {
        errorCallback(e)
      }
    },
    async addFieldStaff ({ commit }, { params }) {
      const url = '/dashboard/AddStaff'
      try {
        const added = await ApiServerClient.post(url, params, errorCallback)
        if (!added.Success) {
          defaultToast(ToastType.Error, 'Something went wrong. Please try again later.')
        }
      } catch (e) {
        errorCallback(e)
      }
    },
    setPreviousHierarchyId ({ commit }, previousHierarchyId) {
      commit('SET_PREVIOUS_HIERARCHY_ID', previousHierarchyId)
    },
    setViewOnly ({ commit }, isViewOnly) {
      commit('SET_IS_VIEW_ONLY', isViewOnly)
    }
  }
}
