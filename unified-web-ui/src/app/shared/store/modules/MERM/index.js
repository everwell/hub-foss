import { ApiServerClient, errorCallback } from '../../Api'

export default {
  namespaced: true,
  state: {
    mermDetails: [],
    mermDetailsLoading: false,
    mermDetailsLoaded: false
  },
  mutations: {
    START_LOADING_MERM_DETAILS (state) {
      state.mermDetails = []
      state.mermDetailsLoading = true
      state.mermDetailsLoaded = false
    },

    END_LOADING_MERM_DETAILS (state) {
      state.mermDetailsLoading = false
      state.mermDetailsLoaded = true
    },

    SET_MERM_DETAILS (state, data) {
      state.mermDetails = data
    }
  },
  actions: {
    async getMermDetails ({ commit, state }, patientId) {
      commit('START_LOADING_MERM_DETAILS')
      const url = '/api/Merm/GetMermDetailsForPatientV2?id=' + patientId
      const response = await ApiServerClient.get(url, null, errorCallback)
      if (response.Success) {
        commit('SET_MERM_DETAILS', response.MermDetails)
      } else {
        // toastError('Unable to load Merm Details')
      }
      commit('END_LOADING_MERM_DETAILS')
    },

    async updateMermDetails ({ commit, state }, inputData) {
      const url = 'api/Merm/Update'
      const response = await ApiServerClient.put(url, { patientId: inputData.patientId, imei: inputData.imei, alarmEnabled: inputData.alarmEnabled, alarmTime: inputData.alarmTime, refillAlarmEnabled: inputData.refillAlarmEnabled, refillAlarmDateTime: inputData.refillAlarmDate + ' ' + inputData.refillAlarmTime }, errorCallback)
      return response.Message
    },

    async getLastSeenAndBattery ({ commit, state }, imei) {
      const url = 'api/Merm/GetLastSeenAndBattery?imei=' + imei
      const response = await ApiServerClient.get(url, null, errorCallback)
      if (response.Success) {
        return response.Data
      }
    }
  }
}
