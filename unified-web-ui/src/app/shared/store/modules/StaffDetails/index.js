import { ApiServerClient, errorCallback } from '../../Api'
import { defaultToast, ToastType } from '../../../../../utils/toastUtils'

export default {
  namespaced: true,
  state: {
    staffDetailsLoading: true,
    currentHierarchyId: -1,
    currentHierarchyName: '',
    currentHierarchyType: '',
    currentHierarchyDisplayType: '',
    currentHierarchyCanStaffChoosePatients: false,
    subHierarchies: [],
    currentHierarchyDisplayTypePlural: '',
    currentHierarchyHaveFieldStaff: '',
    subHierarchyType: '',
    subHierarchyDisplayTypePlural: '',
    artHIVLogins: false,
    typeSpecificMessage: '',
    summary: [],
    districtSummary: '',
    stateSummary: '',
    allowedToAddStaff: false,
    isViewOnly: false,
    canHierarchyHaveStaffPopulated: false,
    breadcrumbs: [],
    staffColumnsData: [],
    staffColumns: [],
    hierarchyColumnsData: [],
    hierarchyColumns: []
  },
  mutations: {
    SET_STAFF_DETAILS_LOADING_STATE (state, status) {
      state.staffDetailsLoading = status
    },
    SET_CURRENT_HIERARCHY_ID (state, id) {
      state.currentHierarchyId = id
    },
    SET_CURRENT_HIERARCHY_NAME (state, name) {
      state.currentHierarchyName = name
    },
    SET_CURRENT_HIERARCHY_TYPE (state, type) {
      state.currentHierarchyType = type
    },
    SET_CURRENT_HIERARCHY_DISPLAY_TYPE (state, value) {
      state.currentHierarchyDisplayType = value
    },
    SET_CURRENT_HIERARCHY_CAN_STAFF_CHOOSE_PATIENTS (state, status) {
      state.currentHierarchyCanStaffChoosePatients = status
    },
    SET_CURRENT_HIERARCHY_CAN_HAVE_FIELD_STAFF (state, status) {
      state.currentHierarchyHaveFieldStaff = status
    },
    SET_SUB_HIERARCHIES (state, value) {
      state.subHierarchies = value
    },
    SET_CURRENT_HIERARCHY_DISPLAY_TYPE_PLURAL (state, id) {
      state.currentHierarchyDisplayTypePlural = id
    },
    SET_SUB_HIERARCHY_TYPE (state, name) {
      state.subHierarchyType = name
    },
    SET_SUB_HIERARCHY_DISPLAY_TYPE_PLURAL (state, type) {
      state.subHierarchyDisplayTypePlural = type
    },
    SET_ART_HIV_LOGIN (state, value) {
      state.artHIVLogins = value
    },
    SET_TYPE_SPECIFIC_MESSAGE (state, value) {
      state.typeSpecificMessage = value
    },
    SET_SUMMARY (state, value) {
      state.summary = value
    },
    SET_DISTRICT_SUMMARY (state, breadcrumbs) {
      state.districtSummary = breadcrumbs
    },
    SET_STATE_SUMMARY (state, value) {
      state.stateSummary = value
    },
    SET_ALLOWED_TO_ADD_STAFF (state, value) {
      state.allowedToAddStaff = value
    },
    SET_IS_VIEW_ONLY (state, breadcrumbs) {
      state.isViewOnly = breadcrumbs
    },
    SET_CAN_HAVE_STAFF_POPULATED (state, value) {
      state.canHierarchyHaveStaffPopulated = value
    },
    SET_BREADCRUMBS (state, breadcrumbs) {
      state.breadcrumbs = breadcrumbs
    },
    SET_STAFF_COLUMNS (state, value) {
      state.staffColumns = value
    },
    SET_STAFF_COLUMNS_Data (state, value) {
      state.staffColumnsData = value
    },
    SET_HIERARCHY_COLUMNS (state, value) {
      state.hierarchyColumns = value
    },
    SET_HIERARCGY_COLUMNS_Data (state, value) {
      state.hierarchyColumnsData = value
    }
  },
  actions: {
    async loadViewParams ({ commit, state }) {
      try {
        const pageUrl = window.location.href
        let urlPattern = /(.*)\/dashboard\/StaffDetails(\/)?(\d+)?/

        const staffDeailsBaseUrlPattern = /\/dashboard\/StaffDetails/
        if (pageUrl.match(staffDeailsBaseUrlPattern)) {
          state.currentHierarchyId = -1
        }
        if (pageUrl.match(urlPattern)) {
          const matches = pageUrl.match(urlPattern)
          if (matches.length === 4) {
            state.currentHierarchyId = matches[3]
          }
        } else {
          urlPattern = /(.*)\/dashboard\/AddStaff(\/)?(\d+)?/
          const matches = pageUrl.match(urlPattern)
          if (matches.length === 4) {
            state.currentHierarchyId = matches[3]
          }
        }
      } catch (e) {
      }
      const url = '/api/fieldstaff/GetViewParamsForStaffDetails?id=' + state.currentHierarchyId
      commit('SET_STAFF_DETAILS_LOADING_STATE', true)
      try {
        const viewParams = await ApiServerClient.get(url, null, errorCallback)
        const breadcrumbs = viewParams.Data.breadcrumbs.map(key => ({
          label: key.Item2, link: '/dashboard/StaffDetails/' + key.Item1, class: state.currentHierarchyId === '' + key.Item1 ? 'active' : ''
        }))
        commit('SET_CURRENT_HIERARCHY_ID', viewParams.Data.CurrentHierarchyId)
        commit('SET_CURRENT_HIERARCHY_NAME', viewParams.Data.CurrentHierarchyName)
        commit('SET_CURRENT_HIERARCHY_TYPE', viewParams.Data.CurrentHierarchyType)
        commit('SET_CURRENT_HIERARCHY_DISPLAY_TYPE', viewParams.Data.CurrentHierarchyDisplayType)
        commit('SET_CURRENT_HIERARCHY_CAN_STAFF_CHOOSE_PATIENTS', viewParams.Data.CurrentHierarchyCanStaffChoosePatients)
        commit('SET_CURRENT_HIERARCHY_CAN_HAVE_FIELD_STAFF', viewParams.Data.CurrentHierarchyHaveFieldStaff)
        commit('SET_SUB_HIERARCHIES', viewParams.Data.SubHierarchies)
        commit('SET_CURRENT_HIERARCHY_DISPLAY_TYPE_PLURAL', viewParams.Data.CurrentHierarchyDisplayTypePlural)
        commit('SET_SUB_HIERARCHY_TYPE', viewParams.Data.SubHierarchyType)
        commit('SET_SUB_HIERARCHY_DISPLAY_TYPE_PLURAL', viewParams.Data.SubHierarchyDisplayTypePlural)
        commit('SET_ART_HIV_LOGIN', viewParams.Data.ArtHIVLogins)
        commit('SET_TYPE_SPECIFIC_MESSAGE', viewParams.Data.typeSpecificMessage)
        commit('SET_SUMMARY', viewParams.Data.Summary)
        commit('SET_DISTRICT_SUMMARY', viewParams.Data.DistrictSummary)
        commit('SET_STATE_SUMMARY', viewParams.Data.StateSummary)
        commit('SET_ALLOWED_TO_ADD_STAFF', viewParams.Data.AllowedToAddStaff)
        commit('SET_IS_VIEW_ONLY', viewParams.Data.IsViewOnly)
        commit('SET_CAN_HAVE_STAFF_POPULATED', viewParams.Data.CanHierarchyHaveStaffPopulated)
        commit('SET_BREADCRUMBS', breadcrumbs)
        commit('SET_STAFF_COLUMNS', viewParams.Data.StaffColumns)
        commit('SET_STAFF_COLUMNS_Data', viewParams.Data.StaffColumnsData)
        commit('SET_HIERARCHY_COLUMNS', viewParams.Data.HierarchyColumns)
        commit('SET_HIERARCGY_COLUMNS_Data', viewParams.Data.HierarchyColumnsData)
        commit('SET_STAFF_DETAILS_LOADING_STATE', false)
      } catch (e) {
        defaultToast(ToastType.Error, 'Something went wrong. Please try again later.')
      }
    }
  }
}
