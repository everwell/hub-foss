import { ApiServerClient, errorCallback } from '../../Api'
import { defaultToast, ToastType } from '../../../../../utils/toastUtils'

export default {
  namespaced: true,
  actions: {
    async getStaffMappingTab ({ commit, state }, patientId) {
      const url = 'api/Patients/GetStaffMappingViewConfig?formName=StaffMapping&idName=PATIENT&id=' + patientId
      const response = await ApiServerClient.get(url, null, errorCallback)
      if (response.Success) {
        return response
      } else {
        defaultToast(ToastType.Error, 'Unable to load  Details')
      }
    }
  }
}
