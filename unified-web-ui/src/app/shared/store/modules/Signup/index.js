import { RegistryServerClient, registryErrorCallback } from '../../RegistryApi'
export default {
  namespaced: true,
  actions: {
    async registerUser ({ commit }, { params }) {
      const url = '/v1/users'
      try {
        return await RegistryServerClient.post(url, params, registryErrorCallback)
      } catch (e) {
        registryErrorCallback(e)
      }
    }
  }
}
