# Data Gateway

The repository has the code for standard event guidelines for data inflow and outflow into the Everwell System


## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. See deployment for notes on how to deploy the project on a live system.

### Prerequisites

What things you need to install the software and how to install them

1. Java 11
2. IntelliJ IDEA / Eclipse or any editor capable of running Java
3. Install Maven
4. Add Lombok Plugin
5. Install Postgres (for running a pure development environment)
6. [Installing and Setting up RabbitMQ for Data-Gateway](https://www.rabbitmq.com/download.html)
7. [Install Redis](https://redis.io)

While running the project for the first time, go to application.properties and make spring.flyway.enabled = false. It tries to make migrations on tables which are not created yet. 
Running it as false the first time will allow the creation of tables in local db. After which making spring.flyway.enabled = true will allow the migrations to be performed successfully.

While running the project for the first time, add 'q.dg.dlq','q.dg.ref_id_reply_queue' queues in local Rabbitmq.
											
### Installing and Deployment

A step by step series of examples that tell you how to get a development env running

Use the deployment commands for easy startup options.
The options for env are among local, oncall, beta, nikshay-prod and hub-india-prod.
```
mvn clean compile -P$env package
java -jar target/data-gateway-0.0.1-SNAPSHOT.jar > console.log&
```

The deployment commands builds a new package of the java project and runs the unit tests.
Once the deployment is done, we can look at the log to ascertain the application has started
```
2022-10-26 16:34:49.916  INFO 7292 --- [           main] o.s.b.w.embedded.tomcat.TomcatWebServer  : Tomcat started on port(s): 8080 (http) with context path ''
2022-10-26 16:34:50.104  INFO 7292 --- [           main] c.e.datagateway.DataGatewayApplication   : Started DataGatewayApplication in 19.716 seconds (JVM running for 20.519)
```

### Building, Compilation and Packaging

For individual compiling of the project, we run the maven commands to build and compile

```
mvn clean compile
```

For packaging of the project in an executable format.
This would execute all the compilation build and test commands before packaging
```
mvn clean compile package
```

For individual running of the project, we run the maven commands

```
mvn spring-boot:run
```

## Running the tests

Data gateway uses JUnit to run tests

Run the following command to evaluate the entire test suite
```
mvn test
```

Run the following command to evaluate individual unit tests
```
mvn test -Dtest=<testfilename>
```

## Built With

* [Springboot](https://spring.io/projects/spring-boot) - The Java framework used
* [Maven](https://maven.apache.org/) - Dependency Management
* [Mockito](https://github.com/mockito/mockito) - Used to generate mocks for testing independently

## License

This project is licensed under the MIT License
