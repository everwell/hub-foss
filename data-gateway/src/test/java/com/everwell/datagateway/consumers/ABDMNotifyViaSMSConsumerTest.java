package com.everwell.datagateway.consumers;

import com.everwell.datagateway.BaseTest;
import com.everwell.datagateway.apiHelpers.ABDMHelper;
import com.everwell.datagateway.exceptions.NotFoundException;
import com.everwell.datagateway.exceptions.URLDisabledException;
import com.everwell.datagateway.models.dto.EventStreamingDTO;
import com.everwell.datagateway.models.request.abdm.ABDMNotifyViaSMSRequest;
import com.everwell.datagateway.models.request.abdm.ABDMSMSNotifyCareContextEvent;
import com.everwell.datagateway.models.response.RestServiceResponse;
import com.everwell.datagateway.service.ConsumerService;
import com.everwell.datagateway.utils.Utils;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.core.MessageProperties;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

public class ABDMNotifyViaSMSConsumerTest extends BaseTest {
    @InjectMocks
    ABDMNotifyViaSMSConsumer abdmNotifyViaSMSConsumer;

    @Mock
    ABDMHelper abdmHelperHelper;

    @Test
    void consume() {
        byte[] byteArray = {'T', 'E', 'S', 'T'};
        ABDMNotifyViaSMSConsumer abdmNotifyViaSMSConsumer = Mockito.spy(ABDMNotifyViaSMSConsumer.class);
        doNothing().when(abdmNotifyViaSMSConsumer).outgoingWebhook(any(), any());
        abdmNotifyViaSMSConsumer.consume(new Message(byteArray, new MessageProperties()));
        verify(abdmNotifyViaSMSConsumer, times(1)).outgoingWebhook(any(), any());
    }

    @Test
    void outgoingWebhookSuccess() {
        EventStreamingDTO eventStreamingDTO = new EventStreamingDTO();
        eventStreamingDTO.setEventName("notify-careContext");
        eventStreamingDTO.setField(new ABDMSMSNotifyCareContextEvent("8186435356",1L,"IN6565678098"));
        RestServiceResponse success = new RestServiceResponse(202, "Success");
        when(abdmHelperHelper.publishToABDM(any(), any())).thenReturn(success);
        byte[] byteArray = Utils.asJsonString(eventStreamingDTO).getBytes();
        abdmNotifyViaSMSConsumer.outgoingWebhook(Utils.asJsonString(eventStreamingDTO), new Message(byteArray, new MessageProperties()));
        verify(abdmHelperHelper, times(1)).publishToABDM(any(), any());
    }

    @Test
    void outgoingWebhookFailure() {
        EventStreamingDTO eventStreamingDTO = new EventStreamingDTO();
        eventStreamingDTO.setEventName("notify-careContext");
        eventStreamingDTO.setField(new ABDMSMSNotifyCareContextEvent("8186435356",1L,"IN6565678098"));
        RestServiceResponse failure = new RestServiceResponse(500, "Failure");
        when(abdmHelperHelper.publishToABDM(any(), any())).thenReturn(failure);
        byte[] byteArray = Utils.asJsonString(eventStreamingDTO).getBytes();
        abdmNotifyViaSMSConsumer.outgoingWebhook(Utils.asJsonString(new ABDMSMSNotifyCareContextEvent("8186435356",1L,"IN6565678098")), new Message(byteArray, new MessageProperties()));
        verify(abdmHelperHelper, times(1)).publishToABDM(any(), any());
    }

}
