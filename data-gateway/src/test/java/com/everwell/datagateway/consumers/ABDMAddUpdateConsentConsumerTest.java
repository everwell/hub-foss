package com.everwell.datagateway.consumers;

import com.everwell.datagateway.BaseTest;
import com.everwell.datagateway.entities.Client;
import com.everwell.datagateway.entities.ClientRequests;
import com.everwell.datagateway.entities.Event;
import com.everwell.datagateway.exceptions.NotFoundException;
import com.everwell.datagateway.exceptions.RestTemplateException;
import com.everwell.datagateway.exceptions.URLDisabledException;
import com.everwell.datagateway.models.response.ABDMConsumerResponse;
import com.everwell.datagateway.repositories.ClientRepository;
import com.everwell.datagateway.repositories.ClientRequestsRepository;
import com.everwell.datagateway.repositories.EventRepository;
import com.everwell.datagateway.service.ABDMConsumerService;
import com.everwell.datagateway.service.ClientRequestsService;
import com.everwell.datagateway.utils.Utils;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.core.MessageProperties;
import org.springframework.http.HttpStatus;

import java.util.Optional;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

class ABDMAddUpdateConsentConsumerTest extends BaseTest {

    @InjectMocks
    ABDMAddUpdateConsentConsumer abdmAddUpdateConsentConsumer;

    @Mock
    ClientRequestsService clientRequestsService;

    @Mock
    ABDMConsumerService abdmConsumerService;

    @Mock
    ClientRequestsRepository clientRequestsRepository;

    @Mock
    private ClientRepository clientRepository;

    @Mock
    private EventRepository eventRepository;

    @Test
    void consume() {
        byte[] byteArray = {'T', 'E', 'S', 'T'};
        ABDMAddUpdateConsentConsumer abdmAddUpdateConsentConsumer = Mockito.spy(ABDMAddUpdateConsentConsumer.class);
        doNothing().when(abdmAddUpdateConsentConsumer).outgoingWebhook(any(), (Message) any());
        abdmAddUpdateConsentConsumer.consume(new Message(byteArray, new MessageProperties()));
        verify(abdmAddUpdateConsentConsumer, times(1)).outgoingWebhook(any(), (Message) any());
    }

    @Test
    void outgoingWebhook() {
        String abdmConsumerResponse = getABDMConsumerResponse();
        byte[] byteArray = abdmConsumerResponse.getBytes();
        when(clientRequestsService.updateClientRequestbasedonRefId(any(), any())).thenReturn(getClientRequest());
        doNothing().when(abdmConsumerService).publishToABDMSubscriberUrl(any(), any(), any());
        doNothing().when(clientRequestsService).setIsDeliveredTrueAndDeliveredAtTime(any(),any());

        Event event = new Event();
        event.setId(1L);
        Client client = new Client();
        client.id = 1L;

        when(clientRepository.findById(1L)).thenReturn(Optional.of(client));
        when(eventRepository.findById(1L)).thenReturn(Optional.of(event));

        abdmAddUpdateConsentConsumer.outgoingWebhook("test", new Message(byteArray, new MessageProperties()));
        verify(abdmConsumerService, times(1)).publishToABDMSubscriberUrl(any(), any(), any());
    }

    @Test
    void outgoingWebhookRestTemplateException() {
        String abdmConsumerResponse = getABDMConsumerResponse();
        byte[] byteArray = abdmConsumerResponse.getBytes();
        when(clientRequestsService.updateClientRequestbasedonRefId(any(), any())).thenReturn(getClientRequest());
        doThrow(new RestTemplateException(HttpStatus.INTERNAL_SERVER_ERROR.value(),HttpStatus.INTERNAL_SERVER_ERROR.getReasonPhrase())).when(abdmConsumerService).publishToABDMSubscriberUrl(any(), any(), any());

        Event event = new Event();
        event.setId(1L);
        Client client = new Client();
        client.id = 1L;

        when(clientRepository.findById(1L)).thenReturn(Optional.of(client));
        when(eventRepository.findById(1L)).thenReturn(Optional.of(event));

        abdmAddUpdateConsentConsumer.outgoingWebhook("test", new Message(byteArray, new MessageProperties()));
        verify(abdmConsumerService, times(1)).publishToABDMSubscriberUrl(any(), any(), any());
    }

    @Test
    void outgoingWebhookNotFoundException() {
        String abdmConsumerResponse = getABDMConsumerResponse();
        byte[] byteArray = abdmConsumerResponse.getBytes();
        when(clientRequestsService.updateClientRequestbasedonRefId(any(), any())).thenReturn(getClientRequest());
        doThrow(NotFoundException.class).when(abdmConsumerService).publishToABDMSubscriberUrl(any(), any(), any());

        Event event = new Event();
        event.setId(1L);
        Client client = new Client();
        client.id = 1L;

        when(clientRepository.findById(1L)).thenReturn(Optional.of(client));
        when(eventRepository.findById(1L)).thenReturn(Optional.of(event));

        abdmAddUpdateConsentConsumer.outgoingWebhook("test", new Message(byteArray, new MessageProperties()));
        verify(abdmConsumerService, times(1)).publishToABDMSubscriberUrl(any(), any(), any());
    }

    @Test
    void outgoingWebhookUrlDisabledException() {
        String abdmConsumerResponse = getABDMConsumerResponse();
        byte[] byteArray = abdmConsumerResponse.getBytes();
        when(clientRequestsService.updateClientRequestbasedonRefId(any(), any())).thenReturn(getClientRequest());
        doThrow(URLDisabledException.class).when(abdmConsumerService).publishToABDMSubscriberUrl(any(), any(), any());

        Event event = new Event();
        event.setId(1L);
        Client client = new Client();
        client.id = 1L;

        when(clientRepository.findById(1L)).thenReturn(Optional.of(client));
        when(eventRepository.findById(1L)).thenReturn(Optional.of(event));

        abdmAddUpdateConsentConsumer.outgoingWebhook("test", new Message(byteArray, new MessageProperties()));
        verify(abdmConsumerService, times(1)).publishToABDMSubscriberUrl(any(), any(), any());
    }

    @Test
    void outgoingWebhookException() {
        String abdmConsumerResponse = getABDMConsumerResponse();
        byte[] byteArray = abdmConsumerResponse.getBytes();
        when(clientRequestsService.updateClientRequestbasedonRefId(any(), any())).thenReturn(new ClientRequests());
        abdmAddUpdateConsentConsumer.outgoingWebhook("test", new Message(byteArray, new MessageProperties()));
        verify(clientRequestsService, times(1)).updateClientRequestbasedonRefId(any(), any());
    }

    private String getABDMConsumerResponse()
    {
        ABDMConsumerResponse abdmConsumerResponse = new ABDMConsumerResponse();
        abdmConsumerResponse.setRefId(1L);
        abdmConsumerResponse.setResponse("Test");
        abdmConsumerResponse.setSuccess(true);
        abdmConsumerResponse.setUsername("Test");
        return Utils.asJsonString(abdmConsumerResponse);
    }

    private ClientRequests getClientRequest()
    {
        ClientRequests clientRequests = new ClientRequests();
        clientRequests.setClientId(1L);
        clientRequests.setEventId(1L);
        return clientRequests;
    }

}