package com.everwell.datagateway.controllers;

import com.everwell.datagateway.BaseTest;
import com.everwell.datagateway.config.CustomExceptionHandler;
import com.everwell.datagateway.models.response.PublisherResponse;
import com.everwell.datagateway.service.PublisherService;
import com.everwell.datagateway.utils.Utils;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

class MermControllerTest extends BaseTest {

    private MockMvc mockMvc;

    @InjectMocks
    private MermController mermController;

    @Mock
    private PublisherService publisherService;

    private final String addUpdateEntityUri = "/v1/imei/entity";
    private final String deallocateEntityUri = "/v1/imei/entity/deallocate";
    private final String testString = "testString";

    @BeforeEach
    public void setup() {

        mockMvc = MockMvcBuilders.standaloneSetup(mermController)
                .setControllerAdvice(new CustomExceptionHandler())
                .build();
    }

    @Test
    void addMermEntityTest() throws Exception {
        when(publisherService.publishRequest(any(), any())).thenReturn(new PublisherResponse());
        mockMvc.
                perform(
                        MockMvcRequestBuilders.
                                post(addUpdateEntityUri)
                                .content(testString)
                                .contentType(MediaType.APPLICATION_JSON)
                                .accept(MediaType.APPLICATION_JSON_VALUE)
                )
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.Success").value("true"));
    }

    @Test
    void updateMermEntityTest() throws Exception {
        when(publisherService.publishRequest(any(), any())).thenReturn(new PublisherResponse());
        mockMvc.
                perform(
                        MockMvcRequestBuilders.
                                put(addUpdateEntityUri)
                                .content(testString)
                                .contentType(MediaType.APPLICATION_JSON)
                                .accept(MediaType.APPLICATION_JSON_VALUE)
                )
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.Success").value("true"));
    }

    @Test
    void deallocateMermEntityTest() throws Exception {
        when(publisherService.publishRequest(any(), any())).thenReturn(new PublisherResponse());
        mockMvc.
                perform(
                        MockMvcRequestBuilders.
                                post(deallocateEntityUri)
                                .content(Utils.asJsonString(testString))
                                .contentType(MediaType.APPLICATION_JSON)
                                .accept(MediaType.APPLICATION_JSON_VALUE)
                )
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.Success").value("true"));
    }
}