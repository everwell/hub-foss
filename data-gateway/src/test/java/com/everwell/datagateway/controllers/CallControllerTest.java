package com.everwell.datagateway.controllers;

import com.everwell.datagateway.BaseTest;
import com.everwell.datagateway.config.CustomExceptionHandler;
import com.everwell.datagateway.entities.Client;
import com.everwell.datagateway.enums.EventEnum;
import com.everwell.datagateway.exceptions.ValidationException;
import com.everwell.datagateway.models.request.CallRequest;
import com.everwell.datagateway.models.response.MessageApiResponse;
import com.everwell.datagateway.models.response.PublisherResponse;
import com.everwell.datagateway.service.ClientService;
import com.everwell.datagateway.service.PublisherService;
import com.everwell.datagateway.utils.Utils;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

class CallControllerTest extends BaseTest {

    private MockMvc mockMvc;

    @InjectMocks
    private CallController callController;

    @Mock
    private PublisherService publisherService;

    @Mock
    private ClientService clientService;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
        mockMvc = MockMvcBuilders.standaloneSetup(callController).setControllerAdvice(new CustomExceptionHandler()).build();
    }

    @Test
    public void testProcessCall() {
        CallRequest callRequest = new CallRequest();
        callRequest.setWho("09755654451");
        callRequest.setChannelID("09778043831");
        callRequest.setDateTime("2024-02-14T07:36:09.954Z");
        callRequest.setDeployment("MYAPUB");
        String queryParamsString = "?who=" + callRequest.getWho() + "&ChannelID=" + callRequest.getChannelID() + "&DateTime=" + callRequest.getDateTime() + "&deployment=" + callRequest.getDeployment();
        callRequest.setQueryParamsString(queryParamsString);
        when(publisherService.publishRequest(anyString(), anyString())).thenReturn(new PublisherResponse());
        ResponseEntity responseEntity = callController.processCall(callRequest);

        verify(publisherService, times(1)).publishRequest(anyString(), anyString());
        assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
        assertEquals("SUCCESS", ((MessageApiResponse) responseEntity.getBody()).getMessage());
    }

    @Test
    public void testProcessCallValidationException() throws Exception {
        CallRequest callRequest = new CallRequest();
        callRequest.setWho("09755654451");
        callRequest.setChannelID("09778043831");
        callRequest.setDateTime("2024-02-14T07:36:09.954Z");
        mockMvc.
                perform(
                        MockMvcRequestBuilders.
                                get("/process-call")
                                .content(Utils.asJsonString(callRequest))
                                .contentType(MediaType.APPLICATION_JSON)
                                .accept(MediaType.APPLICATION_JSON_VALUE)
                )
                .andExpect(status().isBadRequest());
    }
}