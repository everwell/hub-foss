package com.everwell.datagateway.controllers;

import com.everwell.datagateway.BaseTest;
import com.everwell.datagateway.models.request.SubscribeEventRequest;
import com.everwell.datagateway.service.ClientService;
import com.everwell.datagateway.service.EventService;
import com.everwell.datagateway.utils.TypeConverterUtil;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;


import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


class ClientControllerTest extends BaseTest {


    private MockMvc mockMvc;


    @Mock
    private ClientService clientService;

    @Mock
    private EventService eventService;

    @InjectMocks
    private ClientController clientController;

    @BeforeEach
    public void setup() {

        mockMvc = MockMvcBuilders.standaloneSetup(clientController)
                .build();
    }

    @Test
    void testConnection() throws Exception {
        String uri = "/test-connection";
        mockMvc.perform(MockMvcRequestBuilders.get(uri))
                .andExpect(status().isOk());
    }

    @Test
    void subscribeEvent() throws Exception {
        String uri = "/subscribe-event";
        SubscribeEventRequest subscribeEventRequestSuccess = new SubscribeEventRequest("test1", "www.test1.com");
        when(eventService.isEventExists(subscribeEventRequestSuccess.getEventName())).thenReturn(true);
        when(clientService.subscribeEvent(subscribeEventRequestSuccess)).thenReturn(true);  //success Mock
        mockMvc.
                perform(
                        MockMvcRequestBuilders
                                .post(uri)
                                .contentType(MediaType.APPLICATION_JSON)
                                .content(TypeConverterUtil.convertToJsonString(subscribeEventRequestSuccess))
                )
                .andExpect(status().isOk());

        verify(clientService, Mockito.times(1)).subscribeEvent(any());
    }

    @Test
    void subscribeEventFailureEventDoesntExists() throws Exception {
        String uri = "/subscribe-event";
        SubscribeEventRequest subscribeEventRequestFailure = new SubscribeEventRequest("test2", "www.test2.com");
        when(eventService.isEventExists(subscribeEventRequestFailure.getEventName())).thenReturn(false);
        mockMvc.perform(
                MockMvcRequestBuilders.post(uri)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(TypeConverterUtil.convertToJsonString(subscribeEventRequestFailure)))
                .andExpect(status().isBadRequest());
    }


    @Test
    void subscribeEventFailure() throws Exception {
        String uri = "/subscribe-event";
        SubscribeEventRequest subscribeEventRequestFailure = new SubscribeEventRequest("test2", "www.test2.com");
        when(eventService.isEventExists(subscribeEventRequestFailure.getEventName())).thenReturn(true); // Event exits
        when(clientService.subscribeEvent(subscribeEventRequestFailure)).thenReturn(false);  //failure Mock , means already subscribed
        mockMvc.perform(
                MockMvcRequestBuilders.post(uri)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(TypeConverterUtil.convertToJsonString(subscribeEventRequestFailure)))
                .andExpect(status().isBadRequest());
    }

}