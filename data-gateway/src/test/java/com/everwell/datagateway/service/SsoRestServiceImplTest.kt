package com.everwell.datagateway.service

import com.everwell.datagateway.BaseTest
import com.everwell.datagateway.component.AppProperties
import com.everwell.datagateway.models.response.ApiResponse
import com.everwell.datagateway.models.response.MicroServiceGenericAuthResponse
import com.nhaarman.mockitokotlin2.mock
import org.junit.Assert
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.InjectMocks
import org.mockito.Mockito
import org.mockito.junit.MockitoJUnitRunner
import org.springframework.core.ParameterizedTypeReference
import org.springframework.http.HttpMethod
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.util.LinkedMultiValueMap
import org.springframework.web.client.RestTemplate


@RunWith(MockitoJUnitRunner::class)
class SsoRestServiceImplTest : BaseTest(){

    @InjectMocks
    lateinit var ssoRestServiceImpl: SsoRestServiceImpl

    @Test
    fun testAuthentication() {
        val appProperties = AppProperties()
        val restTemplate : RestTemplate = mock()
        val restService : RestService = mock()

        ssoRestServiceImpl.restService = restService
        ssoRestServiceImpl.restTemplateForAuthentication = restTemplate
        ssoRestServiceImpl.appProperties = appProperties
        appProperties.ssoClientIdUrlMap = mapOf("29" to "http://localhost:9090")

        val genericAuthResponseMock = ApiResponse<MicroServiceGenericAuthResponse>(
            "true", MicroServiceGenericAuthResponse(1L, "sso", "auth_token", 1234L), "");
        val responseEntity = ResponseEntity(genericAuthResponseMock, HttpStatus.OK)
        val headers = mutableMapOf<String, String>()
        headers["X-SSO-Client-Id"] = "29"
        val params: LinkedMultiValueMap<String, String> = LinkedMultiValueMap()
        Mockito.`when`(restTemplate.exchange(appProperties.ssoClientIdUrlMap.get("29") + "/v1/ssoserver/client", HttpMethod.GET, ssoRestServiceImpl.getHttpEntity(headers, params), object : ParameterizedTypeReference<ApiResponse<MicroServiceGenericAuthResponse>>() {}))
            .thenAnswer { responseEntity }
        val genericAuthResponse = ssoRestServiceImpl.getAuthToken("29")
        Assert.assertEquals(genericAuthResponseMock.data?.authToken, genericAuthResponse);
    }
}