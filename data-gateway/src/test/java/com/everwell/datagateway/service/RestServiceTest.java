package com.everwell.datagateway.service;

import com.everwell.datagateway.BaseTest;
import com.everwell.datagateway.entities.SubscriberUrl;
import com.everwell.datagateway.models.dto.AuthenticationDetailsDto;
import com.everwell.datagateway.models.response.RestServiceResponse;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.http.HttpMethod;

import static org.junit.jupiter.api.Assertions.assertEquals;

class RestServiceTest extends BaseTest {
    private RestService restService;

    @BeforeEach
    void setup() {
        RestTemplateBuilder restTemplateBuilder = new RestTemplateBuilder();
        restService = new RestService(restTemplateBuilder);
    }

    @Test
    void httpPostRequest() {
        String url = "https://www.google.com/";
        SubscriberUrl subscriberUrl = new SubscriberUrl();
        subscriberUrl.setUrl(url);
        RestServiceResponse restServiceResponse = restService.httpRequest("test", subscriberUrl, new AuthenticationDetailsDto());
        assertEquals(restServiceResponse.getCode(), 405);
    }
}