package com.everwell.datagateway.service

import com.everwell.datagateway.BaseTest
import com.everwell.datagateway.component.AppProperties
import com.everwell.datagateway.service.impl.VotServiceImpl
import com.nhaarman.mockitokotlin2.any
import com.nhaarman.mockitokotlin2.mock
import org.junit.Assert
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.ArgumentMatchers.anyString
import org.mockito.InjectMocks
import org.mockito.Mockito.`when`
import org.mockito.junit.MockitoJUnitRunner
import org.springframework.core.ParameterizedTypeReference
import org.springframework.http.ResponseEntity
import org.springframework.web.client.RestTemplate

@RunWith(MockitoJUnitRunner::class)
class VotServiceImplTest : BaseTest() {
    @InjectMocks
    lateinit var votServiceImpl: VotServiceImpl

    @Test
    fun testAuthentication() {
        val appProperties = AppProperties()
        val restTemplate : RestTemplate = mock()
        val restService : RestService = mock()

        votServiceImpl.restService = restService
        votServiceImpl.restTemplateForAuthentication = restTemplate
        votServiceImpl.appProperties = appProperties
        appProperties.votPassword = "password"
        appProperties.votUsername = "username"
        appProperties.votServerUrl = "https://vot.test.com"
        var votResponse: MutableMap<String, Any> = mutableMapOf()
        votResponse["token"] = "token"
        `when`(restTemplate.exchange(
                anyString(),
                any(),
                any(),
                any<ParameterizedTypeReference<Map<String, Any>>>()
        )).thenReturn(ResponseEntity.ok(votResponse))
        val genericAuthResponse = votServiceImpl.getAuthToken("1")
        Assert.assertEquals(genericAuthResponse, votResponse["token"])
    }
}