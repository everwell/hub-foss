package com.everwell.datagateway.service;

import com.everwell.datagateway.constants.Constants;
import com.everwell.datagateway.models.request.*;
import com.everwell.datagateway.models.response.ApiResponse;
import com.everwell.datagateway.models.response.DiseaseTemplate;
import com.everwell.datagateway.utils.FhirUtil;
import org.hl7.fhir.r4.model.Coding;
import org.hl7.fhir.r4.model.Patient;
import org.junit.Before;
import org.junit.Test;
import org.mockito.*;

import java.time.LocalDateTime;
import java.util.*;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class PMIRServiceImplTest {

    @Spy
    @InjectMocks
    PMIRServiceImpl pmirService;

    @Mock
    EpisodeServiceRestService episodeService;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void createEpisodeTest() {
        DiseaseTemplate diseaseTemplate = new DiseaseTemplate(1L, "test disease", 1L, true, "test disease", LocalDateTime.now(), LocalDateTime.now());
        diseaseTemplate.setId(1L);
        diseaseTemplate.setDiseaseName("test disease");
        String patientJson = "{\n" +
                "  \"resourceType\": \"Patient\",\n" +
                "  \"id\": \"ex-patient-create1\",\n" +
                "  \"active\": true,\n" +
                "  \"name\": [\n" +
                "    {\n" +
                "      \"use\": \"official\",\n" +
                "      \"family\": \"last\",\n" +
                "      \"given\": [\n" +
                "        \"first\",\n" +
                "        \"name\"\n" +
                "      ]\n" +
                "    }\n" +
                "  ],\n" +
                "  \"telecom\": [\n" +
                "    {\n" +
                "      \"system\": \"phone\",\n" +
                "      \"value\": \"+1-734-942-9512\",\n" +
                "      \"use\": \"work\"\n" +
                "    },\n" +
                "    {\n" +
                "      \"system\": \"email\",\n" +
                "      \"value\": \"DavidARiegel@jourrapide.com\",\n" +
                "      \"use\": \"work\"\n" +
                "    }\n" +
                "  ],\n" +
                "  \"gender\": \"male\",\n" +
                "  \"birthDate\": \"1990-01-01\",\n" +
                "  \"address\": [\n" +
                "    {\n" +
                "      \"use\": \"home\",\n" +
                "      \"type\": \"both\",\n" +
                "      \"text\": \"4512 Bombardier Way\\nRomulus, MI 48174\",\n" +
                "      \"line\": [\n" +
                "        \"4512 Bombardier Way\"\n" +
                "      ],\n" +
                "      \"city\": \"Romulus\",\n" +
                "      \"state\": \"MI\",\n" +
                "      \"postalCode\": \"48174\",\n" +
                "      \"country\": \"US\"\n" +
                "    }\n" +
                "  ],\n" +
                "  \"maritalStatus\": {\n" +
                "    \"coding\": [\n" +
                "      {\n" +
                "        \"system\": \"http://hl7.org/fhir/v3/MaritalStatus\",\n" +
                "        \"code\": \"M\",\n" +
                "        \"display\": \"Married\"\n" +
                "      }\n" +
                "    ]\n" +
                "  },\n" +
                "  \"contact\": [\n" +
                "    {\n" +
                "      \"relationship\": [\n" +
                "        {\n" +
                "          \"coding\": [\n" +
                "            {\n" +
                "              \"system\": \"http://terminology.hl7.org/CodeSystem/v2-0131\",\n" +
                "              \"code\": \"NOK\",\n" +
                "              \"display\": \"Next Of Kin\"\n" +
                "            }\n" +
                "          ]\n" +
                "        }\n" +
                "      ],\n" +
                "      \"name\": {\n" +
                "        \"family\": \"Doe\",\n" +
                "        \"given\": [\n" +
                "          \"John\"\n" +
                "        ]\n" +
                "      },\n" +
                "      \"telecom\": [\n" +
                "        {\n" +
                "          \"system\": \"phone\",\n" +
                "          \"value\": \"1234567890\",\n" +
                "          \"use\": \"home\"\n" +
                "        }\n" +
                "      ]\n" +
                "    }\n" +
                "  ],\n" +
                "  \"communication\": [\n" +
                "    {\n" +
                "      \"language\": {\n" +
                "        \"coding\": [\n" +
                "          {\n" +
                "            \"system\": \"urn:ietf:bcp:47\",\n" +
                "            \"code\": \"en\",\n" +
                "            \"display\": \"English\"\n" +
                "          }\n" +
                "        ]\n" +
                "      }\n" +
                "    }\n" +
                "  ]\n" +
                "}";
        Patient patient = FhirUtil.getPatientFromRequest(patientJson);
        Mockito.when(episodeService.getDefaultDiseaseForClientId(Mockito.anyLong())).thenReturn(new ApiResponse<>("true", diseaseTemplate, null));
        Map<String, Object> result = pmirService.createUpdateEpisodeRequest(patient, true, 1L);
        assertEquals("first name", result.get(Constants.FIRST_NAME));
        assertEquals("last", result.get(Constants.LAST_NAME));
        assertEquals("Male", result.get(Constants.GENDER));
        assertEquals("1990-01-01", result.get(Constants.DATE_OF_BIRTH));
        assertEquals("Married", result.get(Constants.MARITAL_STATUS));
        assertEquals("English", result.get(Constants.LANGUAGE));
    }
}
