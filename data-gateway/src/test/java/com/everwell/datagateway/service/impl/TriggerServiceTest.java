package com.everwell.datagateway.service.impl;

import com.everwell.datagateway.BaseTest;
import com.everwell.datagateway.component.AppProperties;
import com.everwell.datagateway.entities.Trigger;
import com.everwell.datagateway.models.response.ApiResponse;
import com.everwell.datagateway.models.response.KpiDataResponse;
import com.everwell.datagateway.repositories.TriggerRepository;
import com.everwell.datagateway.service.KpiRestService;
import com.everwell.datagateway.service.KpiRestServiceImpl;
import org.jobrunr.scheduling.JobScheduler;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.Spy;

import java.util.Collections;

import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.*;

class TriggerServiceTest extends BaseTest {

    @Spy
    @InjectMocks
    TriggerServiceImpl triggerService;

    @Mock
    TriggerRepository triggerRepository;

    @Mock
    JobScheduler jobScheduler;
    @Mock
    KpiRestService kpiRestService;

    @Mock
    private AppProperties appProperties;
    @Test
    public void readTriggerTableTest() {
        Trigger trigger = new Trigger(1L, 1L, 1L, "", "", "SampleJob", "", false, "");

        when(triggerRepository.getAllByCronTimeNotNullAndActiveTrue()).thenReturn(Collections.singletonList(trigger));
        doNothing().when(jobScheduler).delete(eq("SampleJob"));
        triggerService.readTriggerTable();

        verify(jobScheduler, Mockito.times(1)).delete(eq("SampleJob"));
    }

    @Test
    public void invokeFunctionTest() throws Exception {
        Trigger trigger = new Trigger(1L, 1L, 1L, "com.everwell.datagateway.service.impl.TriggerServiceImpl", "SampleJob", "SampleJob", "", false, "{\"datasetId\":1,\"hierarchyLevel\":2,\"hierarchyId\":19,\"year\":2023,\"month\":3}");
        when(kpiRestService.getKPIData(any(),anyInt(),any())).thenReturn(new ApiResponse<>("true", new KpiDataResponse(), null));
        triggerService.invokeFuncFromFunctionName(trigger);
    }
}