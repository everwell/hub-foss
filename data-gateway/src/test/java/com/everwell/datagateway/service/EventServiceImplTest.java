package com.everwell.datagateway.service;

import com.everwell.datagateway.BaseTest;
import com.everwell.datagateway.entities.Event;
import com.everwell.datagateway.repositories.EventRepository;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.mockito.Mockito.when;

class EventServiceImplTest extends BaseTest {

    @InjectMocks
    EventServiceImpl eventService;

    @Mock
    private EventRepository eventRepository;

    @Test
    void findEventByEventNameNullInput() {
        Event event = eventService.findEventByEventName(null);
        assertNull(event);
    }

    @Test
    void findEventByEventName() {
        Event event1 = new Event();
        event1.setEventName("test");
        when(eventRepository.findByEventName("test")).thenReturn(Optional.of(event1));
        Event eventGenerated = eventService.findEventByEventName("test");
        assertEquals("test", eventGenerated.getEventName());
    }

    @Test
    void isEventExistsNull() {
        Boolean answer = eventService.isEventExists(null);
        assertEquals(false, answer);
    }

}