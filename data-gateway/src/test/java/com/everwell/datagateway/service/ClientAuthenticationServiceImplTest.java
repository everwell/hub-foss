package com.everwell.datagateway.service;

import com.everwell.datagateway.BaseTest;
import com.everwell.datagateway.entities.ClientAuthentication;
import com.everwell.datagateway.enums.AuthenticationCredentialTypeEnum;
import com.everwell.datagateway.enums.AuthenticationTypeEnum;
import com.everwell.datagateway.exceptions.ValidationException;
import com.everwell.datagateway.handlers.AuthenticationHandlerMap;
import com.everwell.datagateway.handlers.impl.publishers.BasicAuthenticationHandler;
import com.everwell.datagateway.models.dto.AuthenticationDetailsDto;
import com.everwell.datagateway.models.dto.BasicAuthCredDto;
import com.everwell.datagateway.models.dto.ClientAuthenticationDto;
import com.everwell.datagateway.models.dto.ClientCredentialBase;
import com.everwell.datagateway.repositories.ClientAuthenticationRepository;
import com.everwell.datagateway.service.impl.ClientAuthenticationServiceImpl;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.Spy;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

public class ClientAuthenticationServiceImplTest extends BaseTest {
    @Spy
    @InjectMocks
    private ClientAuthenticationServiceImpl clientAuthenticationService;

    @Mock
    private ClientAuthenticationRepository clientAuthenticationRepository;

    @Mock
    AuthenticationHandlerMap authenticationHandlerMap;

    public List<ClientAuthentication> getClientAuthenticationList() {
        String credentials = "\"credentials\": {\"username\" : \"test\",\"password\" : \"test@123\"}";
        List<ClientAuthentication> clientAuthenticationList = new ArrayList<>();
        clientAuthenticationList.add(new ClientAuthentication(1L,credentials, AuthenticationCredentialTypeEnum.BASIC_AUTH.toString(), AuthenticationTypeEnum.CLIENT, 1L));
        clientAuthenticationList.add(new ClientAuthentication(2L,credentials, AuthenticationCredentialTypeEnum.BASIC_AUTH.toString(), AuthenticationTypeEnum.SUBSCRIBER_URL, 2L));
        return clientAuthenticationList;
    }

    public Map<String, AuthenticationDetailsDto> getTypeToClientAuthenticationMap() {
        String credentials = "\"credentials\": {\"username\" : \"test\",\"password\" : \"test@123\"}";
        Map<String, AuthenticationDetailsDto> typeToClientAuthenticationMap = new HashMap<>();
        typeToClientAuthenticationMap.put("CLIENT1", new AuthenticationDetailsDto(credentials, AuthenticationCredentialTypeEnum.BASIC_AUTH.toString()));
        typeToClientAuthenticationMap.put("SUBSCRIBER_URL2", new AuthenticationDetailsDto(credentials, AuthenticationCredentialTypeEnum.BASIC_AUTH.toString()));
        return typeToClientAuthenticationMap;
    }

    @Test
    public void validateCredentialsSuccess(){
        ClientCredentialBase credentials = new BasicAuthCredDto("Test", "Test@123");
        ClientAuthenticationDto clientAuthenticationDto = new ClientAuthenticationDto(AuthenticationCredentialTypeEnum.BASIC_AUTH.toString(), credentials);
        BasicAuthenticationHandler basicAuthenticationHandler = new BasicAuthenticationHandler();
        when(authenticationHandlerMap.getAuthentication(anyString())).thenReturn(basicAuthenticationHandler);
        clientAuthenticationService.validateCredentials(clientAuthenticationDto);
        verify(authenticationHandlerMap, Mockito.times(1)).getAuthentication(anyString());
    }

    @Test
    public void validateCredentialsThrowValidationException() {
        ClientCredentialBase credentials = new BasicAuthCredDto(null, "Test@123");
        ClientAuthenticationDto clientAuthenticationDto = new ClientAuthenticationDto(AuthenticationCredentialTypeEnum.BASIC_AUTH.toString(), credentials);
        BasicAuthenticationHandler basicAuthenticationHandler = new BasicAuthenticationHandler();
        when(authenticationHandlerMap.getAuthentication(anyString())).thenReturn(basicAuthenticationHandler);
        Assertions.assertThrows(ValidationException.class, () -> {
            clientAuthenticationService.validateCredentials(clientAuthenticationDto);
        });
    }

    @Test
    public void saveAuthenticationDetailsWithNullHandler() {
        ClientCredentialBase credentials = new BasicAuthCredDto("Test", "Test@123");
        String credentialType = "RANDOM";
        clientAuthenticationService.saveAuthenticationDetails(new ClientAuthenticationDto(credentialType, credentials));
        verify(clientAuthenticationRepository, Mockito.times(0)).save(any());
    }

    @Test
    public void saveAuthenticationDetailsWithHandler() {
        ClientCredentialBase credentials = new BasicAuthCredDto("Test", "Test@123");
        String credentialType = AuthenticationCredentialTypeEnum.BASIC_AUTH.toString();
        BasicAuthenticationHandler basicAuthenticationHandler = new BasicAuthenticationHandler();
        when(authenticationHandlerMap.getAuthentication(anyString())).thenReturn(basicAuthenticationHandler);
        clientAuthenticationService.saveAuthenticationDetails(new ClientAuthenticationDto(credentialType, credentials));
        verify(clientAuthenticationRepository, Mockito.times(1)).save(any());
    }

    @Test
    public void saveAuthenticationDetailsThrowException() {
        Assertions.assertThrows(ValidationException.class, () -> {
            clientAuthenticationService.saveAuthenticationDetails(null);
        });
    }

    @Test
    public void getSpecificAuthenticationDetails() {
        doReturn(getTypeToClientAuthenticationMap()).when(clientAuthenticationService).getAuthenticationDetails(any());
        AuthenticationDetailsDto authenticationDetailsDto = clientAuthenticationService.getSpecificAuthenticationDetails(1L, 2L);
        assertEquals(AuthenticationCredentialTypeEnum.BASIC_AUTH.toString(), authenticationDetailsDto.getCredentialType());
        verify(clientAuthenticationService, Mockito.times(1)).getAuthenticationDetails(any());
    }

    @Test
    public void getSpecificAuthenticationDetailsClientKey() {
        doReturn(getTypeToClientAuthenticationMap()).when(clientAuthenticationService).getAuthenticationDetails(any());
        AuthenticationDetailsDto authenticationDetailsDto = clientAuthenticationService.getSpecificAuthenticationDetails(1L, 1L);
        assertEquals(AuthenticationCredentialTypeEnum.BASIC_AUTH.toString(), authenticationDetailsDto.getCredentialType());
        verify(clientAuthenticationService, Mockito.times(1)).getAuthenticationDetails(any());
    }

    @Test
    public void getSpecificAuthenticationDetailsThrowException() {
        doReturn(null).when(clientAuthenticationService).getAuthenticationDetails(any());
        Assertions.assertThrows(ValidationException.class, () -> {
            clientAuthenticationService.getSpecificAuthenticationDetails(1L, 2L);
        });
    }

    @Test
    public void getAuthenticationDetails() {
        List<Long> typeIdList = new ArrayList<>();
        typeIdList.add(1L);
        typeIdList.add(2L);
        when(clientAuthenticationRepository.findAllByTypeIdIn(any())).thenReturn(getClientAuthenticationList());
        Map<String, AuthenticationDetailsDto> typeToClientAuthenticationMap = clientAuthenticationService.getAuthenticationDetails(typeIdList);
        assertEquals(typeIdList.size(), typeToClientAuthenticationMap.size());
        assertTrue(typeToClientAuthenticationMap.containsKey("CLIENT1"));
        assertTrue(typeToClientAuthenticationMap.containsKey("SUBSCRIBER_URL2"));
        verify(clientAuthenticationRepository, Mockito.times(1)).findAllByTypeIdIn(any());
    }

    @Test
    public void getAuthenticationDetailsThrowException() {
        List<Long> typeIdList = new ArrayList<>();
        typeIdList.add(1L);
        typeIdList.add(2L);
        when(clientAuthenticationRepository.findAllByTypeIdIn(any())).thenReturn(null);
        Assertions.assertThrows(ValidationException.class, () -> {
            clientAuthenticationService.getAuthenticationDetails(typeIdList);
        });;
    }

}
