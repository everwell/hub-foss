package com.everwell.datagateway.service;

import com.everwell.datagateway.BaseTest;
import com.everwell.datagateway.entities.OutgoingWebhookLog;
import com.everwell.datagateway.models.dto.OutgoingWebhookLogDTO;
import com.everwell.datagateway.repositories.OutgoingWebhookLogRepository;
import com.everwell.datagateway.service.impl.OutgoingWebhookLogServiceImpl;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.Spy;

import java.util.Date;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.mockito.ArgumentMatchers.any;

public class OutgoingWebhookLogServiceTest extends BaseTest {

    @Spy
    @InjectMocks
    OutgoingWebhookLogServiceImpl outgoingWebhookLogService;

    @Mock
    OutgoingWebhookLogRepository outgoingWebhookLogRepository;

    @Test
    public void saveOutgoingWebhookLogTest() {
        OutgoingWebhookLogDTO  outgoingWebhookLogDTO= new OutgoingWebhookLogDTO("Test");
        OutgoingWebhookLog outgoingWebhookLog = new OutgoingWebhookLog();
        outgoingWebhookLog.setId(1L);
        Mockito.when(outgoingWebhookLogRepository.save(any(OutgoingWebhookLog.class))).thenReturn(outgoingWebhookLog);
        Long response =  outgoingWebhookLogService.saveOutgoingWebhookLog(outgoingWebhookLogDTO);
        assertEquals(1L, response);
    }

    @Test
    public void saveOutgoingWebhookLogTestNull() {
        Long response = outgoingWebhookLogService.saveOutgoingWebhookLog(null);
        assertNull(response);
    }

    @Test
    public void updateOutgoingWebhookLogTest() {
        OutgoingWebhookLogDTO  outgoingWebhookLogDTO= new OutgoingWebhookLogDTO(1L, 1L, 1L, 200, new Date(), "Test");
        OutgoingWebhookLog outgoingWebhookLog = new OutgoingWebhookLog();
        outgoingWebhookLog.setId(1L);
        Mockito.when(outgoingWebhookLogRepository.findById(any())).thenReturn(Optional.of(outgoingWebhookLog));
        Mockito.when(outgoingWebhookLogRepository.save(any(OutgoingWebhookLog.class))).thenReturn(outgoingWebhookLog);
        outgoingWebhookLogService.updateOutgoingWebhookLog(1L, outgoingWebhookLogDTO);
        Mockito.verify(outgoingWebhookLogRepository, Mockito.times(1)).save(any(OutgoingWebhookLog.class));
    }

    @Test
    public void updateOutgoingWebhookLogTestNull() {
        OutgoingWebhookLogDTO  outgoingWebhookLogDTO= new OutgoingWebhookLogDTO("Test");
        OutgoingWebhookLog outgoingWebhookLog = new OutgoingWebhookLog();
        outgoingWebhookLog.setId(1L);
        Mockito.when(outgoingWebhookLogRepository.findById(any())).thenReturn(Optional.empty());
        outgoingWebhookLogService.updateOutgoingWebhookLog(1L, outgoingWebhookLogDTO);
        Mockito.verify(outgoingWebhookLogRepository, Mockito.times(0)).save(any(OutgoingWebhookLog.class));
    }
}
