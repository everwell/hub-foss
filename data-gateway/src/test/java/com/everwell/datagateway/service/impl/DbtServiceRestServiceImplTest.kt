package com.everwell.datagateway.service.impl

import com.everwell.datagateway.BaseTest
import com.everwell.datagateway.component.AppProperties
import com.everwell.datagateway.models.response.ApiResponse
import com.everwell.datagateway.models.response.MicroServiceGenericAuthResponse
import com.everwell.datagateway.service.RestService
import com.nhaarman.mockitokotlin2.mock
import org.junit.Assert
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.InjectMocks
import org.mockito.Mockito
import org.mockito.junit.MockitoJUnitRunner
import org.springframework.core.ParameterizedTypeReference
import org.springframework.http.HttpMethod
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.util.LinkedMultiValueMap
import org.springframework.web.client.RestTemplate

@RunWith(MockitoJUnitRunner::class)
class DbtServiceRestServiceImplTest : BaseTest() {

    @InjectMocks
    lateinit var dbtServiceRestServiceImpl: DBTServiceRestServiceImpl

    @Test
    fun testAuthentication() {
        val appProperties = AppProperties()
        appProperties.dbtServiceUrl = "http://localhost:9310"

        val restTemplate : RestTemplate = mock()
        val restService : RestService = mock()

        dbtServiceRestServiceImpl.restService = restService
        dbtServiceRestServiceImpl.restTemplateForAuthentication = restTemplate
        dbtServiceRestServiceImpl.appProperties = appProperties

        val genericAuthResponseMock = ApiResponse<MicroServiceGenericAuthResponse>(
            "true", MicroServiceGenericAuthResponse(1L, "dbtService", "auth_token", 1234L), "");
        val responseEntity = ResponseEntity(genericAuthResponseMock, HttpStatus.OK)
        val headers = mutableMapOf<String, String>()
        headers["X-Client-Id"] = "29"
        val params: LinkedMultiValueMap<String, String> = LinkedMultiValueMap()
        Mockito.`when`(restTemplate.exchange(appProperties.dbtServiceUrl + "/v1/client", HttpMethod.GET, dbtServiceRestServiceImpl.getHttpEntity(headers, params), object : ParameterizedTypeReference<ApiResponse<MicroServiceGenericAuthResponse>>() {}))
            .thenAnswer { responseEntity }
        val genericAuthResponse = dbtServiceRestServiceImpl.getAuthToken("29")
        Assert.assertEquals(genericAuthResponseMock.data?.authToken, genericAuthResponse);
    }
}