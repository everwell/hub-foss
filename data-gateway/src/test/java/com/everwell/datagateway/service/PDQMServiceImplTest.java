package com.everwell.datagateway.service;

import ca.uhn.fhir.context.FhirContext;
import com.everwell.datagateway.BaseTest;
import com.everwell.datagateway.constants.Constants;
import com.everwell.datagateway.enums.TypeOfEpisodeEnum;
import com.everwell.datagateway.exceptions.PDQMCustomException;
import com.everwell.datagateway.models.request.EpisodeSearchRequest;
import com.everwell.datagateway.models.response.EpisodeIndex;
import com.everwell.datagateway.models.response.EpisodeSearchResult;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.hl7.fhir.r4.model.Bundle;
import org.hl7.fhir.r4.model.Enumerations;
import org.hl7.fhir.r4.model.Patient;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;

import javax.servlet.http.HttpServletRequest;
import java.util.*;

import static org.hibernate.validator.internal.util.Contracts.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class PDQMServiceImplTest extends BaseTest {

    @Spy
    @InjectMocks
    PDQMServiceImpl pdqmService;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
    }
    @Test
    public void getEpisodeSearchRequestFromRequestTestWithoutModifier() throws PDQMCustomException {
        Map<String, Object> parameterMap = new HashMap<>();
        parameterMap.put("active", new String[]{"true"});
        parameterMap.put("birthdate", new String[]{"ne1997-01-01"});
        parameterMap.put("birthdate", new String[]{"lt1997-01-01"});
        parameterMap.put("telecom", new String[]{"1111111111"});
        parameterMap.put("given", new String[]{"test"});
        parameterMap.put("gender", new String[]{"male"});
        parameterMap.put("_count", new String[]{"10"});
        parameterMap.put("_page", new String[]{"0"});
        parameterMap.put("_sort", new String[]{"-_id"});
        parameterMap.put("address-postalcode", new String[]{"111111"});
        EpisodeSearchRequest episodeSearchRequest = pdqmService.getEpisodeSearchRequestFromRequest(parameterMap);
        Assert.assertEquals(episodeSearchRequest.getSearch().getMust().get("firstName"), parameterMap.get("given"));
    }

    @Test
    public void getEpisodeSearchRequestFromRequestWithId() throws PDQMCustomException {
        Map<String, Object> parameterMap = new HashMap<>();
        parameterMap.put("_id", new String[]{"1"});
        parameterMap.put("_count", new String[]{"10"});
        parameterMap.put("_page", new String[]{"0"});
        parameterMap.put("_sort", new String[]{"_id"});
        EpisodeSearchRequest episodeSearchRequest = pdqmService.getEpisodeSearchRequestFromRequest(parameterMap);
        Assert.assertEquals(episodeSearchRequest.getSearch().getMust().get("id"), parameterMap.get("_id"));
    }

    @Test
    public void getEpisodeSearchRequestFromRequestWithModifier1() throws PDQMCustomException {
        Map<String, Object> parameterMap = new HashMap<>();
        parameterMap.put("active", new String[]{"true"});
        parameterMap.put("birthdate", new String[]{"gt1997-01-01"});
        parameterMap.put("birthdate", new String[]{"lt1997-01-01"});
        parameterMap.put("telecom", new String[]{"1111111111"});
        parameterMap.put("given:exact", new String[]{"test"});
        parameterMap.put("_count", new String[]{"10"});
        parameterMap.put("_page", new String[]{"0"});
        EpisodeSearchRequest episodeSearchRequest = pdqmService.getEpisodeSearchRequestFromRequest(parameterMap);
        Assert.assertEquals(episodeSearchRequest.getSearch().getMust().get("firstName"), parameterMap.get("given"));
    }

    @Test
    public void getEpisodeSearchRequestFromRequestWithModifier2() throws PDQMCustomException {
        Map<String, Object> parameterMap = new HashMap<>();
        parameterMap.put("active", new String[]{"true"});
        parameterMap.put("birthdate", new String[]{"gt1997-01-01", "lt1997-01-01"});
        parameterMap.put("telecom", new String[]{"1111111111"});
        parameterMap.put("given:contains", new String[]{"test"});
        parameterMap.put("gender:missing", new String[]{"true"});
        parameterMap.put("_count", new String[]{"10"});
        parameterMap.put("_page", new String[]{"0"});
        EpisodeSearchRequest episodeSearchRequest = pdqmService.getEpisodeSearchRequestFromRequest(parameterMap);
        Assert.assertEquals(episodeSearchRequest.getSearch().getMust().get("firstName"), parameterMap.get("given"));
    }

    @Test
    public void getEpisodeSearchRequestFromRequestWithModifier3() throws PDQMCustomException {
        Map<String, Object> parameterMap = new HashMap<>();
        parameterMap.put("active", new String[]{"true"});
        parameterMap.put("birthdate", new String[]{"gt1997-01-01"});
        parameterMap.put("birthdate", new String[]{"lt1997-01-01"});
        parameterMap.put("telecom", new String[]{"email:1111111111"});
        parameterMap.put("given:contains", new String[]{"test"});
        parameterMap.put("gender:missing", new String[]{"false"});
        parameterMap.put("_count", new String[]{"10"});
        parameterMap.put("_page", new String[]{"0"});
        EpisodeSearchRequest episodeSearchRequest = pdqmService.getEpisodeSearchRequestFromRequest(parameterMap);
        Assert.assertEquals(episodeSearchRequest.getSearch().getMust().get("firstName"), parameterMap.get("given"));
    }

    @Test
    public void getPatientBundleTest() throws JsonProcessingException {
        EpisodeIndex episodeIndex = new EpisodeIndex("1");
        episodeIndex.setDeleted(false);
        Map<String, Object> stageData = new HashMap<>();
        stageData.put("firstName", "test");
        episodeIndex.setStageData(stageData);
        List<EpisodeIndex> episodeIndexList = new ArrayList<>();
        episodeIndexList.add(episodeIndex);
        Patient patient = new Patient();
        patient.setGender(Enumerations.AdministrativeGender.FEMALE);
        List<Patient> patientList = new ArrayList<>();
        patientList.add(patient);
        Bundle bundle = new Bundle();
        bundle.setType(Bundle.BundleType.SEARCHSET);
        bundle.setTotal(5);
        FhirContext fhirContext = FhirContext.forR4();
        String responseString = fhirContext.newJsonParser().encodeResourceToString(bundle);
        ObjectMapper objectMapper = new ObjectMapper();
        JsonNode jsonNode = objectMapper.readTree(responseString);
        HttpServletRequest request = mock(HttpServletRequest.class);
        Map<String, String[]> parameterMap = new HashMap<>();
        parameterMap.put("_id", new String[]{"1"});
        EpisodeSearchRequest searchRequest = new EpisodeSearchRequest(10000, 0, Arrays.asList(
                Constants.ID, Constants.DELETED, Constants.FIRST_NAME, Constants.LAST_NAME, Constants.PRIMARY_PHONE_NUMBER, Constants.EMAIL_LIST, Constants.DATE_OF_BIRTH, Constants.TREATMENT_OUTCOME, Constants.ADDRESS, Constants.PIN_CODE, Constants.GENDER
        ), TypeOfEpisodeEnum.getTypeOfEpisodeList());
        searchRequest.setSearch(new EpisodeSearchRequest.SupportedSearchMethods(new HashMap<>(), new HashMap<>(), new HashMap<>()));
        JsonNode response = pdqmService.getPatientBundle(request, searchRequest, new EpisodeSearchResult(episodeIndexList));
        Assert.assertEquals(response.get("total").toString(), "1");
    }

    @Test
    public void testGetOperationOutcomeForExceptionWithException() {
        Exception ex = new Exception("Test Exception");
        JsonNode jsonNode = pdqmService.getOperationOutcomeForException(ex);
        assertEquals("{\"resourceType\":\"OperationOutcome\",\"issue\":[{\"severity\":\"error\",\"code\":\"exception\",\"details\":{\"text\":\"Test Exception\"}}]}", jsonNode.toString());
    }

    @Test
    public void testGetOperationOutcomeForExceptionWithCustomExceptionAndUnsupportedModifier() {
        PDQMCustomException exception = mock(PDQMCustomException.class);
        when(exception.getModifier()).thenReturn("exacte");
        when(exception.getRequestParam()).thenReturn("gender");
        JsonNode jsonNode = pdqmService.getOperationOutcomeForPDQMException(exception);
        assertEquals("{\"resourceType\":\"OperationOutcome\",\"issue\":[{\"severity\":\"fatal\",\"code\":\"code-invalid\",\"details\":{\"text\":\"The \\\"gender\\\" parameter has the modifier \\\"exacte\\\" which is not supported by the server\"},\"location\":[\"http.name:exacte\"]}]}", jsonNode.toString());
    }

    @Test
    public void testGetOperationOutcomeForExceptionWithCustomExceptionAndUnsupportedCode() {
        PDQMCustomException exception = mock(PDQMCustomException.class);
        when(exception.getMessage()).thenReturn(Constants.CODE_NOT_SUPPORTED);
        when(exception.getRequestParam()).thenReturn("testParam");
        when(exception.getParamValue()).thenReturn("testCode");
        JsonNode jsonNode = pdqmService.getOperationOutcomeForPDQMException(exception);
        assertEquals("{\"resourceType\":\"OperationOutcome\",\"issue\":[{\"severity\":\"fatal\",\"code\":\"code-invalid\",\"details\":{\"text\":\"The code \\\"testCode\\\" is not known and not legal in this context\"},\"location\":[\"http.name:testParam\"],\"expression\":[\"Patient.testParam\"]}]}", jsonNode.toString());
    }

    @Test
    public void testGetOperationOutcomeForExceptionWithCustomExceptionAndUnsupportedParameter() {
        PDQMCustomException exception = mock(PDQMCustomException.class);
        when(exception.getRequestParam()).thenReturn("unsupportedParam");
        JsonNode jsonNode = pdqmService.getOperationOutcomeForPDQMException(exception);
        assertEquals("{\"resourceType\":\"OperationOutcome\",\"issue\":[{\"severity\":\"fatal\",\"code\":\"code-invalid\",\"details\":{\"text\":\"The \\\"unsupportedParam\\\" parameter is not supported by the server\"},\"location\":[\"http.name:unsupportedParam\"]}]}", jsonNode.toString());
    }

    @Test
    public void getCapabilityStatementTest() {
        String capabilityStatement = pdqmService.getCapabilityStatement();
        assertNotNull(capabilityStatement);
    } 

}
