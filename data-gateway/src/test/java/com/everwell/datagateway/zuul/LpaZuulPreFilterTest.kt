package com.everwell.datagateway.zuul

import com.everwell.datagateway.filters.zuul.lpa.LpaZuulPreFilter
import com.netflix.zuul.ExecutionStatus
import com.netflix.zuul.context.RequestContext
import com.netflix.zuul.monitoring.MonitoringHelper
import junit.framework.TestCase
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.MockitoAnnotations
import org.mockito.junit.MockitoJUnitRunner
import org.springframework.mock.web.MockHttpServletRequest

@RunWith(MockitoJUnitRunner::class)
class LpaZuulPreFilterTest {
    lateinit var lpaZuulPreFilter: LpaZuulPreFilter

    @Before
    fun before() {
        MockitoAnnotations.initMocks(this)
        MonitoringHelper.initMocks()
        lpaZuulPreFilter = LpaZuulPreFilter()
    }

    @Test
    fun testZuulPreFilterSuccessGetToPost() {
        val request =
            MockHttpServletRequest("GET", "http://localhost:8080/lpa/v1/available-lpa-lab?hierarchyId=1&pageNo=1")
        request.queryString = "hierarchyId=1&pageNo=1"
        val context = RequestContext()
        context.setRequest(request)
        RequestContext.testSetCurrentContext(context)
        val result = lpaZuulPreFilter.runFilter()
        TestCase.assertEquals(ExecutionStatus.SUCCESS, result.getStatus())
    }

    @Test
    fun testZuulPreFilterSuccess() {
        val request = MockHttpServletRequest("GET", "http://localhost:8080/lpa/v1/available-lpa-lab")
        val context = RequestContext()
        context.setRequest(request)
        RequestContext.testSetCurrentContext(context)
        val result = lpaZuulPreFilter.runFilter()
        TestCase.assertEquals(ExecutionStatus.SUCCESS, result.getStatus())
    }
}