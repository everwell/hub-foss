package com.everwell.datagateway.zuul

import com.everwell.datagateway.filters.zuul.dure.DureZuulPostFilter
import com.everwell.datagateway.service.NikshayReportsRestService
import com.netflix.zuul.ExecutionStatus
import com.netflix.zuul.context.RequestContext
import com.netflix.zuul.monitoring.MonitoringHelper
import com.nhaarman.mockitokotlin2.mock
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mockito
import org.mockito.MockitoAnnotations
import org.mockito.junit.MockitoJUnitRunner
import org.springframework.cloud.netflix.zuul.filters.ProxyRequestHelper
import org.springframework.cloud.netflix.zuul.filters.ZuulProperties
import org.springframework.http.HttpStatus
import org.springframework.mock.web.MockHttpServletRequest
import org.springframework.mock.web.MockHttpServletResponse
import org.springframework.web.client.RestTemplate
import java.net.URL
import kotlin.test.assertEquals
import com.nhaarman.mockitokotlin2.any

@RunWith(MockitoJUnitRunner::class)
class DureZuulPostFilterTest {

    private lateinit var dureZuulPostFilter: DureZuulPostFilter

    val mockRestTemplate: RestTemplate = mock()
    val mockNikshayRestService: NikshayReportsRestService = mock()
    val proxyRequestHelper: ProxyRequestHelper = ProxyRequestHelper(ZuulProperties())
    private val nikshayReportsAuthToken = "abcd1234"


    @Before
    fun before() {
        MockitoAnnotations.initMocks(this)
        MonitoringHelper.initMocks()
        dureZuulPostFilter = DureZuulPostFilter()
        dureZuulPostFilter.helper = proxyRequestHelper
        dureZuulPostFilter.nikshayReportsRestService = mockNikshayRestService
        dureZuulPostFilter.restTemplateForApi = mockRestTemplate
        Mockito.`when`(mockNikshayRestService.getAuthToken(any())).thenAnswer { nikshayReportsAuthToken }
    }
    @Test
    fun testZuulPostFilterForUnAuthorised(){
        val request = MockHttpServletRequest("GET", "/dure/mstrState")
        val response = MockHttpServletResponse()
        val context = RequestContext()
        context.setRequest(request)
        context.response = response
        RequestContext.testSetCurrentContext(context)
        context.routeHost = URL("https://reports.nikshay.in")
        context["requestURI"] ="/nikshayapiv2/api/masters/mstrState"
        context.setResponseBody("Not authorised to call")
        context.setResponseStatusCode(HttpStatus.UNAUTHORIZED.value())
        val result = dureZuulPostFilter.runFilter()
        assertEquals(ExecutionStatus.SUCCESS, result.getStatus())
    }

    @Test
    fun testZuulPostFilterForAuthorised(){
        val successBodyString = "Success"
        val request = MockHttpServletRequest("GET", "/dure/mstrState")
        val response = MockHttpServletResponse()
        val context = RequestContext()
        context.setRequest(request)
        context.response = response
        RequestContext.testSetCurrentContext(context)
        context.routeHost = URL("https://reports.nikshay.in")
        context["requestURI"] ="/nikshayapiv2/api/masters/mstrState"
        context.setResponseBody(successBodyString)
        context.setResponseStatusCode(HttpStatus.OK.value())
        val result = dureZuulPostFilter.runFilter()
        assertEquals(ExecutionStatus.SUCCESS, result.getStatus())
        assertEquals(context.responseBody, successBodyString)
    }

}