package com.everwell.datagateway.zuul

import com.everwell.datagateway.filters.zuul.callLogs.CallLogsZuulPreFilter
import com.netflix.zuul.ExecutionStatus
import com.netflix.zuul.context.RequestContext
import com.netflix.zuul.monitoring.MonitoringHelper
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.MockitoAnnotations
import org.mockito.junit.MockitoJUnitRunner
import org.springframework.http.HttpMethod
import org.springframework.mock.web.MockHttpServletRequest
import kotlin.test.assertEquals

@RunWith(MockitoJUnitRunner::class)
class CallLogsZuulPreFilterTest {
    private lateinit var callLogsZuulPreFilter: CallLogsZuulPreFilter

    @Before
    fun before() {
        MockitoAnnotations.initMocks(this)
        MonitoringHelper.initMocks()
        callLogsZuulPreFilter = CallLogsZuulPreFilter()
    }

    @Test
    fun testZuulPretFilterSuccess() {
        val request =
            MockHttpServletRequest(HttpMethod.POST.name, "http://localhost:8080/call-logs?deploymentCode=ASCETH")
        request.queryString = "deploymentCode=ASCETH"
        val context = RequestContext()
        context.setRequest(request)
        RequestContext.testSetCurrentContext(context)
        val result = callLogsZuulPreFilter.runFilter()
        assertEquals(ExecutionStatus.SUCCESS, result.getStatus())
    }

}