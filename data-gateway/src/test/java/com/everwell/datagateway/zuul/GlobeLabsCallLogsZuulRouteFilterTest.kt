package com.everwell.datagateway.zuul

import com.everwell.datagateway.component.AppProperties
import com.everwell.datagateway.enums.LegacyRoutesEnum
import com.everwell.datagateway.filters.zuul.globalLabsCallLogs.GlobeLabsCallLogsZuulRouteFilter
import com.everwell.datagateway.service.RegistryRestService
import com.netflix.zuul.ExecutionStatus
import com.netflix.zuul.context.RequestContext
import com.netflix.zuul.monitoring.MonitoringHelper
import com.nhaarman.mockitokotlin2.any
import com.nhaarman.mockitokotlin2.mock
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mockito
import org.mockito.MockitoAnnotations
import org.mockito.junit.MockitoJUnitRunner
import org.springframework.http.HttpMethod
import org.springframework.mock.web.MockHttpServletRequest
import org.springframework.web.client.RestTemplate
import java.net.URL
import kotlin.test.assertEquals

@RunWith(MockitoJUnitRunner::class)
class GlobeLabsCallLogsZuulRouteFilterTest {

    private lateinit var globeLabsCallLogsZuulRouteFilter: GlobeLabsCallLogsZuulRouteFilter
    val registryRestService: RegistryRestService = mock()

    val mockRestTemplate: RestTemplate = mock()
    private val authToken = "a3456f"

    @Before
    fun before() {
        MockitoAnnotations.initMocks(this)
        MonitoringHelper.initMocks()
        globeLabsCallLogsZuulRouteFilter = GlobeLabsCallLogsZuulRouteFilter()
        globeLabsCallLogsZuulRouteFilter.registryRestService = registryRestService
        Mockito.`when`(registryRestService.getAuthToken(any())).thenAnswer { authToken }
        globeLabsCallLogsZuulRouteFilter.restTemplateForApi = mockRestTemplate
    }

    @Test
    fun testZuulRouteFilterSuccessTestFetchClientIdFromRegistry() {
        val appProperties = AppProperties()
        appProperties.registryServerUrl = "http://localhost:9210"
        appProperties.clientIdGlobeLabsCallLogs = "63"
        globeLabsCallLogsZuulRouteFilter.appProperties = appProperties
        val request = MockHttpServletRequest(HttpMethod.POST.name, LegacyRoutesEnum.GLOBAL_LABS_CALL_LOGS.path)
        val context = RequestContext()
        context.setRequest(request)
        context.routeHost = URL("http://localhost:9210")
        RequestContext.testSetCurrentContext(context)
        val result = globeLabsCallLogsZuulRouteFilter.runFilter();
        assertEquals(context["requestURI"], "/v1/deployment-call-back")
        assertEquals(ExecutionStatus.SUCCESS, result.getStatus())
    }

}