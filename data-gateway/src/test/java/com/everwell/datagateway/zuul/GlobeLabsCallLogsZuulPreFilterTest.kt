package com.everwell.datagateway.zuul

import com.everwell.datagateway.filters.zuul.globalLabsCallLogs.GlobeLabsCallLogsZuulPreFilter
import com.netflix.zuul.ExecutionStatus
import com.netflix.zuul.context.RequestContext
import com.netflix.zuul.monitoring.MonitoringHelper
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.MockitoAnnotations
import org.mockito.junit.MockitoJUnitRunner
import org.springframework.http.HttpMethod
import org.springframework.mock.web.MockHttpServletRequest
import kotlin.test.assertEquals

@RunWith(MockitoJUnitRunner::class)
class GlobeLabsCallLogsZuulPreFilterTest {
    private lateinit var globeLabsCallLogsZuulPreFilter: GlobeLabsCallLogsZuulPreFilter

    @Before
    fun before() {
        MockitoAnnotations.initMocks(this)
        MonitoringHelper.initMocks()
        globeLabsCallLogsZuulPreFilter = GlobeLabsCallLogsZuulPreFilter()
    }

    @Test
    fun testZuulPreFilterSuccess() {
        val jsonBody = """{"inboundSMSMessageList":{"inboundSMSMessage":[{"dateTime": "Fri Mar 08 2024 03:50:19 GMT+0000 (UTC)", "destinationAddress": "tel:21580270", "messageId": "5e87caa666bf661a83b76e96", "message": "153", "resourceURL": null, "senderAddress": "tel:+63123123233"}], "numberOfMessagesInThisBatch": 1, "resourceURL": null, "totalNumberOfPendingMessages": 0}}"""
        val request = MockHttpServletRequest(HttpMethod.POST.name, "http://localhost:8080/global-labs-call-logs)")
        request.contentType = "application/json"
        request.setContent(jsonBody.toByteArray(Charsets.UTF_8))
        val context = RequestContext()
        context.setRequest(request)
        RequestContext.testSetCurrentContext(context)
        val result = globeLabsCallLogsZuulPreFilter.runFilter()
        assertEquals(ExecutionStatus.SUCCESS, result.getStatus())
    }

}