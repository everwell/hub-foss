package com.everwell.datagateway.zuul

import com.everwell.datagateway.filters.zuul.mermConfig.MermConfigZuulPreFilter
import com.netflix.zuul.ExecutionStatus
import com.netflix.zuul.context.RequestContext
import com.netflix.zuul.monitoring.MonitoringHelper
import junit.framework.TestCase
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.MockitoAnnotations
import org.mockito.junit.MockitoJUnitRunner
import org.springframework.mock.web.MockHttpServletRequest

@RunWith(MockitoJUnitRunner::class)
class MermConfigZuulPreFilterTest {

    lateinit var mermConfigZuulPreFilter: MermConfigZuulPreFilter

    @Before
    fun before() {
        MockitoAnnotations.initMocks(this)
        MonitoringHelper.initMocks()
        mermConfigZuulPreFilter = MermConfigZuulPreFilter()
    }

    @Test
    fun testZuulPreFilterSuccessPostToGet(){
        val request = MockHttpServletRequest("POST", "http://localhost:8080/get-merm-config?SN=861833048438982")
        request.queryString = "SN=861833048438982"
        val context = RequestContext()
        context.setRequest(request)
        RequestContext.testSetCurrentContext(context)
        val result = mermConfigZuulPreFilter.runFilter()
        TestCase.assertEquals(ExecutionStatus.SUCCESS, result.getStatus())
    }
}