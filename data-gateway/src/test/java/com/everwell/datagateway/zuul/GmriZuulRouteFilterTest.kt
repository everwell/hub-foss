package com.everwell.datagateway.zuul

import com.everwell.datagateway.component.AppProperties
import com.everwell.datagateway.entities.Client
import com.everwell.datagateway.filters.zuul.gmri.GmriZuulRouteFilter
import com.everwell.datagateway.service.AuthenticationService
import com.everwell.datagateway.service.ClientService
import com.everwell.datagateway.service.RegistryRestService
import com.netflix.zuul.ExecutionStatus
import com.netflix.zuul.context.RequestContext
import com.netflix.zuul.monitoring.MonitoringHelper
import com.nhaarman.mockitokotlin2.any
import com.nhaarman.mockitokotlin2.anyOrNull
import com.nhaarman.mockitokotlin2.mock
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mockito
import org.mockito.MockitoAnnotations
import org.mockito.junit.MockitoJUnitRunner
import org.springframework.mock.web.MockHttpServletRequest
import kotlin.test.assertEquals

@RunWith(MockitoJUnitRunner::class)
class GmriZuulRouteFilterTest {
    private lateinit var gmriZuulRouteFilter: GmriZuulRouteFilter
    val registryRestService: RegistryRestService = mock()

    val clientService : ClientService = mock()
    val authenticationService : AuthenticationService = mock()

    private val authToken = "abcd1234"

    @Before
    fun before() {
        MockitoAnnotations.initMocks(this)
        MonitoringHelper.initMocks()
        gmriZuulRouteFilter = GmriZuulRouteFilter()
        gmriZuulRouteFilter.registryRestService = registryRestService
        Mockito.`when`(registryRestService.getAuthToken(any())).thenAnswer { authToken }
        val appProperties = AppProperties()
        gmriZuulRouteFilter.appProperties = appProperties
        gmriZuulRouteFilter.clientService = clientService
        gmriZuulRouteFilter.authenticationService = authenticationService

        val client: Client = Client()
        client.username = "gmri"
        client.id = 420

        Mockito.`when`(clientService.findByUsername(anyOrNull())).thenAnswer { client }
        Mockito.`when`(authenticationService.getCurrentUsername(anyOrNull())).thenAnswer { "gmri" }
    }

    @Test
    fun testZuulRouteFilterSuccess() {
        val request = MockHttpServletRequest("POST", "/gmri/merm/imei/mapping")
        val context = RequestContext()
        context.setRequest(request)
        RequestContext.testSetCurrentContext(context)
        val result = gmriZuulRouteFilter.runFilter();
        assertEquals(context["requestURI"], "/v1/merm/imei/mapping")
        assertEquals(ExecutionStatus.SUCCESS, result.getStatus())
    }
}