package com.everwell.datagateway.zuul

import com.everwell.datagateway.component.AppProperties
import com.everwell.datagateway.entities.Client
import com.everwell.datagateway.enums.ValidationStatusEnum
import com.everwell.datagateway.filters.zuul.kpi.KpiZuulRouteFilter
import com.everwell.datagateway.filters.zuul.registry.RegistryZuulRouteFilter
import com.everwell.datagateway.service.AuthenticationService
import com.everwell.datagateway.service.ClientService
import com.everwell.datagateway.service.KpiRestService
import com.netflix.zuul.ExecutionStatus
import com.netflix.zuul.context.RequestContext
import com.netflix.zuul.monitoring.MonitoringHelper
import com.nhaarman.mockitokotlin2.mock
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mockito
import org.mockito.MockitoAnnotations
import org.mockito.junit.MockitoJUnitRunner
import org.springframework.mock.web.MockHttpServletRequest
import kotlin.test.assertEquals
import com.nhaarman.mockitokotlin2.any
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken
import org.springframework.security.core.context.SecurityContextHolder

@RunWith(MockitoJUnitRunner::class)
class KpiZuulRouteFilterTest {

    private lateinit var kpiZuulRouteFilter: KpiZuulRouteFilter
    val kpiRestService: KpiRestService = mock()
    val authenticationService: AuthenticationService = mock()
    val clientService: ClientService = mock()

    private val authToken = "abcd1234"

    @Before
    fun before() {
        MockitoAnnotations.initMocks(this)
        MonitoringHelper.initMocks()
        kpiZuulRouteFilter = KpiZuulRouteFilter()
        kpiZuulRouteFilter.kpiRestService = kpiRestService
        kpiZuulRouteFilter.authenticationService = authenticationService
        kpiZuulRouteFilter.clientService = clientService
        Mockito.`when`(kpiRestService.getAuthToken(any())).thenAnswer { authToken }
        val appProperties = AppProperties()
        kpiZuulRouteFilter.appProperties = appProperties
    }

    @Test
    fun testZuulRouteFilterSuccess() {
        val request = MockHttpServletRequest("GET", "/kpi/v1/test")
//        request.addHeader("client-id", "29")
        val context = RequestContext()
        context.request = request
        RequestContext.testSetCurrentContext(context)

        val clientName = "external-client"
        SecurityContextHolder.getContext().authentication = UsernamePasswordAuthenticationToken(clientName, null)
        val client = Client()
        client.accessibleClient = 1
        client.username = clientName

        Mockito.`when`(authenticationService.getCurrentUsername(any())).thenAnswer { clientName }
        Mockito.`when`(clientService.findByUsername(clientName)).thenAnswer { client }

        val result = kpiZuulRouteFilter.runFilter();
        assertEquals(context["requestURI"], "/v1/test")
        assertEquals(ExecutionStatus.SUCCESS, result.status)
    }

    @Test
    fun testZuulRouteFilterSuccess_ClientIdInHeader() {
        val request = MockHttpServletRequest("GET", "/kpi/v1/test")
        request.addHeader("client-id", "1")
        val context = RequestContext()
        context.request = request
        RequestContext.testSetCurrentContext(context)

        val clientName = "external-client"
        SecurityContextHolder.getContext().authentication = UsernamePasswordAuthenticationToken(clientName, null)
        val client = Client()
        client.username = clientName

        Mockito.`when`(authenticationService.getCurrentUsername(any())).thenAnswer { clientName }
        Mockito.`when`(clientService.findByUsername(clientName)).thenAnswer { client }

        val result = kpiZuulRouteFilter.runFilter();
        assertEquals(context["requestURI"], "/v1/test")
        assertEquals(ExecutionStatus.SUCCESS, result.status)
    }

    @Test
    fun testZuulRouteFilterSuccess_ClientIdUnavailable() {
        val request = MockHttpServletRequest("GET", "/kpi/v1/test")
        val context = RequestContext()
        context.request = request
        RequestContext.testSetCurrentContext(context)

        val clientName = "external-client"
        SecurityContextHolder.getContext().authentication = UsernamePasswordAuthenticationToken(clientName, null)
        val client = Client()
        client.username = clientName

        Mockito.`when`(authenticationService.getCurrentUsername(any())).thenAnswer { clientName }
        Mockito.`when`(clientService.findByUsername(clientName)).thenAnswer { client }

        val result = kpiZuulRouteFilter.runFilter();
        assertEquals(context["requestURI"], "/v1/test")
        assertEquals(ExecutionStatus.FAILED, result.status)
        assertEquals(ValidationStatusEnum.CLIENT_ID_NOT_PRESENT.message, result.exception.message)
    }

}