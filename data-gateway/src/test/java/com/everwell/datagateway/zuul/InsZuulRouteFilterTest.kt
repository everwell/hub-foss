package com.everwell.datagateway.zuul

import com.everwell.datagateway.component.AppProperties
import com.everwell.datagateway.filters.zuul.ins.InsZuulRouteFilter
import com.everwell.datagateway.service.InsRestService
import com.netflix.zuul.ExecutionStatus
import com.netflix.zuul.context.RequestContext
import com.netflix.zuul.monitoring.MonitoringHelper
import com.nhaarman.mockitokotlin2.mock
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mockito
import org.mockito.MockitoAnnotations
import org.mockito.junit.MockitoJUnitRunner
import org.springframework.mock.web.MockHttpServletRequest
import kotlin.test.assertEquals
import com.nhaarman.mockitokotlin2.any

@RunWith(MockitoJUnitRunner::class)
class InsZuulRouteFilterTest {

    private lateinit var insZuulRouteFilter: InsZuulRouteFilter
    val insRestService: InsRestService = mock()

    private val authToken = "abcd1234"

    @Before
    fun before() {
        MockitoAnnotations.initMocks(this)
        MonitoringHelper.initMocks()
        insZuulRouteFilter = InsZuulRouteFilter()
        insZuulRouteFilter.insRestService = insRestService
        Mockito.`when`(insRestService.getAuthToken(any())).thenAnswer { authToken }
        val appProperties = AppProperties()
        insZuulRouteFilter.appProperties = appProperties
    }

    @Test
    fun testZuulRouteFilterSuccess() {
        val request = MockHttpServletRequest("GET", "/ins/v1/template")
        request.addHeader("client-id", "29")
        val context = RequestContext()
        context.request = request
        RequestContext.testSetCurrentContext(context)
        val result = insZuulRouteFilter.runFilter();
        assertEquals(context["requestURI"], "/v1/template")
        assertEquals(ExecutionStatus.SUCCESS, result.getStatus())
    }

}