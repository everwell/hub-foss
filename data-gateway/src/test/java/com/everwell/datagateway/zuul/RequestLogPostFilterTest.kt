package com.everwell.datagateway.zuul

import com.everwell.datagateway.constants.Constants
import com.everwell.datagateway.constants.Constants.INTERNAL_CLIENT_USERNAME
import com.everwell.datagateway.entities.*
import com.everwell.datagateway.filters.zuul.RequestLogPostFilter
import com.everwell.datagateway.service.*
import com.netflix.zuul.ExecutionStatus
import com.netflix.zuul.context.RequestContext
import com.netflix.zuul.monitoring.MonitoringHelper
import com.nhaarman.mockitokotlin2.any
import com.nhaarman.mockitokotlin2.eq
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import com.nhaarman.mockitokotlin2.mock
import org.mockito.Mockito
import org.mockito.MockitoAnnotations
import org.mockito.junit.MockitoJUnitRunner
import org.springframework.mock.web.MockHttpServletRequest
import org.springframework.mock.web.MockHttpServletResponse
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken
import org.springframework.security.core.context.SecurityContextHolder
import kotlin.test.assertEquals

@RunWith(MockitoJUnitRunner::class)
class RequestLogPostFilterTest {
    private lateinit var requestLogPostFilter: RequestLogPostFilter

    private val clientRequestsService: ClientRequestsService = mock()

    val authenticationService: AuthenticationService = mock()

    @Before
    fun before() {
        MockitoAnnotations.initMocks(this)
        MonitoringHelper.initMocks()
        requestLogPostFilter = RequestLogPostFilter()
        requestLogPostFilter.clientRequestsService = clientRequestsService
        requestLogPostFilter.authenticationService = authenticationService
    }

    @Test
    fun testRun() {
        val url = "/kpi/v1/kpi/data/1"
        val request = MockHttpServletRequest("POST", url)
        val response = MockHttpServletResponse()
        val context = RequestContext()
        context.request = request
        context.response = response
        context.responseDataStream = "{\"success\":true, \"data\": {\"a\": \"A\"}}".byteInputStream()
        context.responseGZipped = false
        val requestLogId = 1L
        context.set(Constants.REQUEST_LOG_ID_KEY, requestLogId)
        RequestContext.testSetCurrentContext(context)

        val result = requestLogPostFilter.runFilter()
        assertEquals(ExecutionStatus.SUCCESS, result.status)
        Mockito.verify(clientRequestsService, Mockito.times(1)).updateResponseData(eq(requestLogId), any())
    }

    @Test
    fun testRun_PrefetchedResponse() {
        val url = "/kpi/v1/kpi/data/1"
        val request = MockHttpServletRequest("POST", url)
        val response = MockHttpServletResponse()
        val context = RequestContext()
        context.request = request
        context.response = response
        var responseJson = "{\"success\":true, \"data\": {\"a\": \"A\"}}"
        context.responseDataStream = responseJson.byteInputStream()
        context.set(Constants.REQUEST_LOG_RESPONSE, responseJson)
        val requestLogId = 1L
        context.set(Constants.REQUEST_LOG_ID_KEY, requestLogId)
        RequestContext.testSetCurrentContext(context)

        val result = requestLogPostFilter.runFilter()
        assertEquals(ExecutionStatus.SUCCESS, result.status)
        Mockito.verify(clientRequestsService, Mockito.times(1)).updateResponseData(eq(requestLogId), any())
    }
}