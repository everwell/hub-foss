package com.everwell.datagateway.zuul

import com.everwell.datagateway.component.AppProperties
import com.everwell.datagateway.filters.zuul.lpa.LpaZuulRouteFilter
import com.everwell.datagateway.service.AuthenticationService
import com.everwell.datagateway.service.RegistryRestService
import com.netflix.zuul.ExecutionStatus
import com.netflix.zuul.context.RequestContext
import com.netflix.zuul.monitoring.MonitoringHelper
import com.nhaarman.mockitokotlin2.any
import com.nhaarman.mockitokotlin2.mock
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mockito
import org.mockito.MockitoAnnotations
import org.mockito.junit.MockitoJUnitRunner
import org.springframework.mock.web.MockHttpServletRequest
import kotlin.test.assertEquals

@RunWith(MockitoJUnitRunner::class)
class LpaZuulRouteFilterTest {
    private lateinit var lpaZuulRouteFilter: LpaZuulRouteFilter
    val registryRestService: RegistryRestService = mock()

    val authenticationService : AuthenticationService = mock()

    private val authToken = "abcd1234"

    @Before
    fun before() {
        MockitoAnnotations.initMocks(this)
        MonitoringHelper.initMocks()
        lpaZuulRouteFilter = LpaZuulRouteFilter()
        lpaZuulRouteFilter.registryRestService = registryRestService
        Mockito.`when`(registryRestService.getAuthToken(any())).thenAnswer { authToken }
        val appProperties = AppProperties()
        lpaZuulRouteFilter.appProperties = appProperties
        lpaZuulRouteFilter.authenticationService = authenticationService
        lpaZuulRouteFilter.appProperties.clientIdLpa = "29"
    }

    @Test
    fun testZuulRouteFilterSuccess() {
        val request = MockHttpServletRequest("GET", "/lpa/v1/user/validate-episode")
        val context = RequestContext()
        context.setRequest(request)
        RequestContext.testSetCurrentContext(context)
        val result = lpaZuulRouteFilter.runFilter();
        assertEquals(context["requestURI"], "/v1/user/validate-episode")
        assertEquals(ExecutionStatus.SUCCESS, result.getStatus())
    }
}