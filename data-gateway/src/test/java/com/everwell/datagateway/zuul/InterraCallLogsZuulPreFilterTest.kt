package com.everwell.datagateway.zuul

import com.everwell.datagateway.filters.zuul.interraCallLogs.InterraCallLogsZuulPreFilter
import com.netflix.zuul.ExecutionStatus
import com.netflix.zuul.context.RequestContext
import com.netflix.zuul.monitoring.MonitoringHelper
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.MockitoAnnotations
import org.mockito.junit.MockitoJUnitRunner
import org.springframework.http.HttpMethod
import org.springframework.mock.web.MockHttpServletRequest
import java.io.ByteArrayInputStream
import kotlin.test.assertEquals

@RunWith(MockitoJUnitRunner::class)
class InterraCallLogsZuulPreFilterTest {
    private lateinit var interraCallLogsZuulPreFilter: InterraCallLogsZuulPreFilter

    @Before
    fun before() {
        MockitoAnnotations.initMocks(this)
        MonitoringHelper.initMocks()
        interraCallLogsZuulPreFilter = InterraCallLogsZuulPreFilter()
    }

    @Test
    fun testZuulPreFilterSuccess() {
        val jsonBody = """{"inboundSMSMessageList":{"inboundSMSMessage":[{"from":"+2341223234455","to":"53895","utcDateTime":"23-03-2023 01:09:00","dateTime":"23-03-2023 02:09:00","message":"121","deploymentCode":"TOOLNGA"}]}}"""
        val request = MockHttpServletRequest(HttpMethod.POST.name, "http://localhost:8080/interra/call-logs)")
        request.contentType = "application/json"
        request.setContent(jsonBody.toByteArray(Charsets.UTF_8))
        val context = RequestContext()
        context.setRequest(request)
        RequestContext.testSetCurrentContext(context)
        val result = interraCallLogsZuulPreFilter.runFilter()
        assertEquals(ExecutionStatus.SUCCESS, result.getStatus())
    }

}