package com.everwell.datagateway.utils;

import com.everwell.datagateway.BaseTest;
import com.everwell.datagateway.exceptions.ValidationException;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.nio.charset.StandardCharsets;

public class EncryptUtilTest extends BaseTest {

    @Test
    void encryptDecryptText() throws Exception {
        String plainText = "Test";
        String password = "Test@123";
        String encryptedString = EncryptUtil.encrypt(plainText, password);
        String decryptedString = EncryptUtil.decrypt(encryptedString, password);
        Assertions.assertEquals(plainText, decryptedString);
    }

    @Test
    void encryptThrowException() {
        String plainText = "Test";
        Assertions.assertThrows(ValidationException.class, () -> {
            EncryptUtil.encrypt(plainText.getBytes(StandardCharsets.UTF_8), null);
        });
    }

    @Test
    void decryptThrowException() {
        String encodedText = "Test";
        Assertions.assertThrows(ValidationException.class, () -> {
            EncryptUtil.decrypt(encodedText, null);
        });
    }

    @Test
    void getRandomNonceThrowException() {
        Assertions.assertThrows(ValidationException.class, () -> {
            EncryptUtil.getRandomNonce(-1);
        });
    }

    @Test
    void getSecretKeyThrowException() {
        Assertions.assertThrows(ValidationException.class, () -> {
            EncryptUtil.getSecretKeyFromPassword(null, null);
        });
    }

}
