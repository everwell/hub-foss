package com.everwell.datagateway.utils;

import com.everwell.datagateway.BaseTest;
import com.everwell.datagateway.models.dto.EncryptionRequest;
import com.everwell.datagateway.models.dto.EncryptionResponse;
import com.everwell.datagateway.models.dto.KeyMaterial;

import org.junit.Assert;
import org.junit.Test;

import java.nio.charset.StandardCharsets;
import java.util.Base64;

public class EncyptUtilsTest extends BaseTest {


    @Test
    public void encryptTest() throws Exception {

        //prepare keys
        KeyMaterial senderKeyMaterial = EncryptionUtil.generate();
        KeyMaterial receiverKeyMaterial = EncryptionUtil.generate();
        EncryptionRequest encryptionRequest = new EncryptionRequest(receiverKeyMaterial.getPublicKey(),receiverKeyMaterial.getNonce(),senderKeyMaterial,"test");

        //encrypt & decrypt
        EncryptionResponse encryptionResponse = EncryptionUtil.encrypt(encryptionRequest);
        byte[] xorOfRandom = EncryptionUtil.xorOfRandom(senderKeyMaterial.getNonce(),receiverKeyMaterial.getNonce());
        String decryptedData = EncryptionUtil.decrypt(xorOfRandom,receiverKeyMaterial.getPrivateKey(),senderKeyMaterial.getPublicKey(),encryptionResponse.getEncryptedData());

        // Assert
        Assert.assertEquals(EncryptionUtil.getBase64String(EncryptionUtil.getEncodedHIPPublicKey(EncryptionUtil.getKey(senderKeyMaterial.getPublicKey()))),encryptionResponse.getKeyToShare());
        Assert.assertEquals("test",decryptedData);
    }

    @Test
    public void encryptAesTest() throws Exception {
        String plainText = "{\"vaccinatedBeneficiariesList\":[{\"State\":\"Karnataka\",\"StateLGD_Code\":123,\"District\":\"Dharwad\",\"DistrictLGD_Code\":456,\"SubDistrict\":\"SubDistrictName\",\"SubDistrictLGD_Code\":789,\"Village\":\"VillageName\",\"VillageLGD_Code\":1234,\"Address\":\"Address details\",\"Pincode\":123456,\"BeneficiaryType\":\"Type\",\"SubType\":\"SubType\",\"NikshayId\":12345678,\"HealthFacilityName\":\"FacilityName\",\"CVC_Id\":\"12345\",\"SessionSiteName\":\"Site Name\",\"FirstName\":\"Test\",\"LastName\":\"Test uwin\",\"Gender\":\"Male\",\"YearOfBirth\":\"1997\",\"DateOfBirth\":\"1997-10-12\",\"ReferenceId\":\"1\",\"PhotoIdType\":\"Type\",\"PhotoIdNumber\":123456,\"AbhaNumber\":789234,\"PhoneNumber\":987654320,\"VaccinationStatus\":\"Status\",\"MaterialId\":\"12345\",\"MaterialName\":\"Name\",\"VaccinationDoseOneDate\":\"2023-01-01\",\"AEFI_Id\":\"12345678\",\"DateOfAEFI\":\"2023-02-01\",\"AEFI_SymptomSign\":\"Sign\",\"AEFI_Outcome\":\"Outcome\",\"AEFI_Details\":\"Details\",\"OutcomeOfFollowUp\":\"Outcome\"}]}";
        String privateKey = "abcdefghijklmnop";
        String base64EncodePrivateKey = Base64.getEncoder().encodeToString(privateKey.getBytes(StandardCharsets.UTF_8));
        String iv = "zyxwvutsrqponmlk";
        String base64EncodedIv = Base64.getEncoder().encodeToString(iv.getBytes(StandardCharsets.UTF_8));

        String encryptedString = EncryptionUtil.encryptAES(base64EncodePrivateKey, base64EncodedIv, plainText);
        String decryptedString = EncryptionUtil.decryptAES(base64EncodePrivateKey, base64EncodedIv, encryptedString);

        Assert.assertEquals(plainText, decryptedString);
    }
}
