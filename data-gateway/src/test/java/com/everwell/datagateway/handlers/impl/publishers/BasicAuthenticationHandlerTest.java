package com.everwell.datagateway.handlers.impl.publishers;

import com.everwell.datagateway.enums.AuthenticationCredentialTypeEnum;
import com.everwell.datagateway.exceptions.ValidationException;
import com.everwell.datagateway.handlers.AuthenticationHandlerTest;
import com.everwell.datagateway.models.dto.BasicAuthCredDto;
import com.everwell.datagateway.utils.Utils;
import com.fasterxml.jackson.core.JsonProcessingException;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Spy;
import org.springframework.http.HttpHeaders;

import static org.junit.jupiter.api.Assertions.*;


public class BasicAuthenticationHandlerTest extends AuthenticationHandlerTest {

    @Spy
    @InjectMocks
    BasicAuthenticationHandler basicAuthenticationHandler;

    @Test
    void getEventEnumerator() {
        AuthenticationCredentialTypeEnum authenticationCredentialTypeEnum = basicAuthenticationHandler.getEventEnumerator();
        assertEquals(AuthenticationCredentialTypeEnum.BASIC_AUTH, authenticationCredentialTypeEnum);
    }

    @Test
    void encryptSensitiveDetailsInObject() throws Exception {
        String credentials = "{\"username\" : \"test\",\"password\" : \"test@123\"}";
        String encryptedPassword = basicAuthenticationHandler.encryptSensitiveDetailsInObject(credentials);
        assertNotEquals(credentials, encryptedPassword);
    }

    @Test
    void setUpAuthentication() throws Exception {
        String credentials = "{\"username\" : \"test\",\"password\" : \"test\"}";
        String credentialType = AuthenticationCredentialTypeEnum.BASIC_AUTH.toString();
        String encryptedCredentials = basicAuthenticationHandler.encryptSensitiveDetailsInObject(credentials);
        BasicAuthCredDto basicAuthCredentials = Utils.convertStrToObject(encryptedCredentials, BasicAuthCredDto.class);
        HttpHeaders httpHeaders = basicAuthenticationHandler.setUpAuthentication(Utils.asJsonString(basicAuthCredentials), credentialType);
        assertNotNull(httpHeaders);
        assertTrue(httpHeaders.containsKey("Authorization"));
    }

    @Test
    void setUpAuthenticationThrowValidationException() {
        Assertions.assertThrows(ValidationException.class, () -> {
            basicAuthenticationHandler.setUpAuthentication(null, null);
        });
    }

    @Test
    void validateCredentialDto() throws JsonProcessingException {
        String credentials = "{\"username\" : \"test\",\"password\" : \"test\"}";
        basicAuthenticationHandler.validateCredentialDto(credentials);
    }

    @Test
    void validateCredentialDtoThrowException() {
        String credentials = "{\"username\" : null,\"password\" : \"test\"}";
        Assertions.assertThrows(ValidationException.class, () -> {
            basicAuthenticationHandler.validateCredentialDto(credentials);
        });
    }


}
