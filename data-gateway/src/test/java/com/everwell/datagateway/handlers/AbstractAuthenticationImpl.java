package com.everwell.datagateway.handlers;

import com.everwell.datagateway.enums.AuthenticationCredentialTypeEnum;
import com.fasterxml.jackson.core.JsonProcessingException;
import org.springframework.http.HttpHeaders;

public class AbstractAuthenticationImpl extends AuthenticationHandler {

    @Override
    public AuthenticationCredentialTypeEnum getEventEnumerator() {
        return null;
    }

    @Override
    public String encryptSensitiveDetailsInObject(String credentials) throws Exception {
        return null;
    }

    @Override
    public HttpHeaders setUpAuthentication(String credentials, String credentialType) {
        return null;
    }

    @Override
    public void validateCredentialDto(String credentials) throws JsonProcessingException {

    }
}
