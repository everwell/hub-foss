package com.everwell.datagateway.handlers.impl.publishers;

import com.everwell.datagateway.enums.EventEnum;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertTrue;

class ABDMAddUpdateConsentEventHandlerTest {

    @Test
    void getEventEnumerator() {
        ABDMAddUpdateConsentEventHandler abdmAddUpdateConsentEventHandler = new ABDMAddUpdateConsentEventHandler();
        assertTrue(abdmAddUpdateConsentEventHandler.getEventEnumerator().equals(EventEnum.ABDM_ADD_UPDATE_CONSENT));
    }
}