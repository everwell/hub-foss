package com.everwell.datagateway.handlers.impl.publishers;

import com.everwell.datagateway.enums.EventEnum;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertTrue;

class DeallocateMermEntityMappingHandlerTest {

    @Test
    void getEventEnumerator() {
        DeallocateMermEntityMappingHandler deallocateMermEntityMappingHandler = new DeallocateMermEntityMappingHandler();
        assertTrue(deallocateMermEntityMappingHandler.getEventEnumerator().equals(EventEnum.DEALLOCATE_MERM_ENTITY));
    }
}