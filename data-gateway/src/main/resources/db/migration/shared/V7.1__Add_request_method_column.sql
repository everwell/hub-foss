DO
$do$

BEGIN

ALTER TABLE subscriber_url
ADD COLUMN IF NOT EXISTS request_method varchar(10);

IF EXISTS(SELECT * FROM subscriber_url WHERE request_method is NULL) THEN
    UPDATE public.subscriber_url
    SET request_method = 'POST'
    WHERE request_method is NULL;
END IF;

END;
$do$