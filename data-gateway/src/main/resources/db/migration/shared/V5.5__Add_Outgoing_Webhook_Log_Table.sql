create table if not exists outgoing_webhook_log
(
    id                      bigserial not null,
    created_at              timestamp,
    result_delivered        boolean,
    client_id               bigint,
    event_id                bigint,
    subscriber_url_id       bigint,
    data                    text,
    response                text,
    delivered_at            timestamp,
    callback_response_code  INTEGER,
    updated_at              timestamp,
    primary key (id)
);