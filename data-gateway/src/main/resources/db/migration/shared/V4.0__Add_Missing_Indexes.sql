create index if not exists event_client_client_id_and_event_id on event_client(client_id,event_id);

create index if not exists event_event_name on event(event_name);

create index if not exists publisher_queue_info_event_name on publisher_queue_info(event_name);

create index if not exists subscriber_url_url on subscriber_url(url);