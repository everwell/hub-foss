DO
$do$

DECLARE
    eventName VARCHAR := 'UPDATE_EPISODE';
    exchangeName VARCHAR := 'direct-incoming';
    queueName VARCHAR := 'q.dg.update_episode';
    replyRoutingKey VARCHAR := 'update-episode';
BEGIN

IF NOT EXISTS (SELECT * FROM event WHERE event_name = eventName) THEN
    INSERT INTO event (event_name, created_at, updated_at) VALUES (eventName, NOW(), NOW());
END IF;

IF NOT EXISTS (SELECT * FROM publisher_queue_info WHERE event_name = eventName) THEN
    INSERT INTO publisher_queue_info (event_name, exchange, queue_name, routing_key, reply_to_routing_key) VALUES (eventName, exchangeName, queueName, queueName, replyRoutingKey);
END IF;

end;
$do$