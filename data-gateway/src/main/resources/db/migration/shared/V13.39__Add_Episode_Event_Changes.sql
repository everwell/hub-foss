DO
$do$
BEGIN
    IF NOT EXISTS(SELECT id FROM event WHERE event_name = 'ADD_EPISODE') THEN
        INSERT INTO event(created_at, event_name, updated_at)
        VALUES (NOW(), 'ADD_EPISODE', NOW());
    END IF;

    IF NOT EXISTS(SELECT id FROM public.publisher_queue_info WHERE queue_name = 'q.dg.add_patient' AND event_name = 'ADD_EPISODE') THEN
        INSERT INTO public.publisher_queue_info (queue_name, event_name, exchange, routing_key, reply_to_routing_key)
        VALUES ('q.dg.add_patient', 'ADD_EPISODE', 'direct-incoming', 'q.dg.add_patient', 'add-patient');
    END IF;
END
$do$;
