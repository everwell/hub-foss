DO
$do$
BEGIN
    IF NOT EXISTS(SELECT id FROM event WHERE event_name = 'GET_EPISODE') THEN
        INSERT INTO event(created_at, event_name, updated_at)
        VALUES (NOW(), 'GET_EPISODE', NOW());
    END IF;
END
$do$;