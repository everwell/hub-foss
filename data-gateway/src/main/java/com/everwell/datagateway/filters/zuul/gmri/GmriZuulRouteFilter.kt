package com.everwell.datagateway.filters.zuul.gmri

import com.everwell.datagateway.constants.SecurityConstants
import com.everwell.datagateway.filters.zuul.base.BaseZuulRouteFilter
import com.everwell.datagateway.service.BaseRestService
import com.everwell.datagateway.service.ClientService
import com.everwell.datagateway.service.RegistryRestService
import com.netflix.zuul.context.RequestContext
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.cloud.netflix.zuul.filters.support.FilterConstants

open class GmriZuulRouteFilter() : BaseZuulRouteFilter() {

    @Autowired
    lateinit var registryRestService: RegistryRestService

    @Autowired
    lateinit var clientService : ClientService

    override var thisURI: String
        get() = "/gmri"
        set(value) {}

    init {
        // ToDo: Add / Change endpoints as GMRI knows which endpoints to be called
        urlMapping.put("/gmri/merm/imei/mapping","/v1/merm/imei/mapping")
        urlMapping.put("/gmri/v1/imei", "/v1/merm/imeis/filter")
        urlMapping.put("/gmri/v1/all-imei/available", "/v1/merm/imeis/filter")
        urlMapping.put("/gmri/v1/all-imei", "/v1/imei")
    }

    override fun getHeadersForAuth(context: RequestContext): MutableMap<String, String> {
        val map = mutableMapOf<String, String>()
        val userName = getUserName()
        val clientId = clientService.findByUsername(userName).id.toString()
        map["Authorization"] = SecurityConstants.TOKEN_PREFIX + " " + registryRestService.getAuthToken(clientId)!!
        map["X-Client-Id"] = clientId
        return map
    }

    override fun getRestService(): BaseRestService {
        return registryRestService
    }

    override fun filterType(): String {
        return FilterConstants.ROUTE_TYPE
    }

    override fun filterOrder(): Int {
        return FilterConstants.PRE_DECORATION_FILTER_ORDER + 12
    }
}