package com.everwell.datagateway.filters.zuul.base

import com.everwell.datagateway.service.AuthenticationService
import com.netflix.zuul.ZuulFilter
import com.netflix.zuul.context.RequestContext
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.security.core.context.SecurityContextHolder

open abstract class BaseZuulFilter : ZuulFilter(){

    abstract protected var thisURI: String

    @Autowired
    lateinit var authenticationService: AuthenticationService

    override fun shouldFilter():Boolean{
        return RequestContext.getCurrentContext().getRequest().requestURI.split("/").get(1).equals(thisURI.substring(1))
    }

    fun getUserName():String?{
        val authentication = SecurityContextHolder.getContext().authentication
        return authenticationService.getCurrentUsername(authentication)
    }

}