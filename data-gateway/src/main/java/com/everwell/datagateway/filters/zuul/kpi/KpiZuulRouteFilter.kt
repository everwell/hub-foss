package com.everwell.datagateway.filters.zuul.kpi

import com.everwell.datagateway.constants.SecurityConstants
import com.everwell.datagateway.enums.ValidationStatusEnum
import com.everwell.datagateway.exceptions.NotFoundException
import com.everwell.datagateway.exceptions.ValidationException
import com.everwell.datagateway.filters.zuul.base.BaseZuulRouteFilter
import com.everwell.datagateway.service.BaseRestService
import com.everwell.datagateway.service.ClientService
import com.everwell.datagateway.service.KpiRestService
import com.netflix.zuul.context.RequestContext
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.cloud.netflix.zuul.filters.support.FilterConstants
import org.springframework.security.core.context.SecurityContextHolder

open class KpiZuulRouteFilter() :  BaseZuulRouteFilter(){

    @Autowired
    lateinit var kpiRestService: KpiRestService

    @Autowired
    lateinit var clientService: ClientService

    override var thisURI: String
        get() = "/kpi"
        set(value) {}

    override fun getHeadersForAuth(context: RequestContext): MutableMap<String, String> {
        val map = mutableMapOf<String, String>()
        val authentication = SecurityContextHolder.getContext().authentication
        val username = authenticationService.getCurrentUsername(authentication)
        val client = clientService.findByUsername(username)
        var clientId: String? = null

        if (client.accessibleClient != null) {
            clientId = client.accessibleClient.toString()
        } else {
            clientId = context.request.getHeader("client-id")
            if (clientId == null) {
                throw ValidationException(ValidationStatusEnum.CLIENT_ID_NOT_PRESENT.message)
            }
        }

        map["Authorization"] = SecurityConstants.TOKEN_PREFIX + kpiRestService.getAuthToken(clientId)!!
        return map
    }

    override fun getUrlMapping(context: RequestContext): String? {
        return context.request.requestURI.substring(thisURI.length)
    }

    override fun getRestService(): BaseRestService {
        return kpiRestService
    }

    override fun filterType(): String {
        return FilterConstants.ROUTE_TYPE
    }

    override fun filterOrder(): Int {
        return FilterConstants.PRE_DECORATION_FILTER_ORDER + 15
    }
}