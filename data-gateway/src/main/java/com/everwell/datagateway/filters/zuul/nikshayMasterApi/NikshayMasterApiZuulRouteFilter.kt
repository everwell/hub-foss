package com.everwell.datagateway.filters.zuul.nikshayMasterApi

import com.everwell.datagateway.filters.zuul.base.BaseZuulRouteFilter
import com.everwell.datagateway.service.BaseRestService
import com.everwell.datagateway.service.NikshayMasterApiRestService
import com.netflix.zuul.context.RequestContext
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.cloud.netflix.zuul.filters.support.FilterConstants

open class NikshayMasterApiZuulRouteFilter() :  BaseZuulRouteFilter(){

    @Autowired
    lateinit var nikshayMasterApiRestService: NikshayMasterApiRestService

    override var thisURI: String
        get() = "/nikshayMasterApi"
        set(value) {}

    override fun getHeadersForAuth(context: RequestContext): MutableMap<String, String> {
        val map = mutableMapOf<String, String>()
        map["Authorization"] = "Bearer " + nikshayMasterApiRestService.getAuthToken(null)!!
        return map
    }

    override fun getUrlMapping(context: RequestContext): String? {
        return context.request.requestURI.substring(thisURI.length)
    }

    override fun getRestService(): BaseRestService {
        return nikshayMasterApiRestService
    }

    override fun filterType(): String {
        return FilterConstants.ROUTE_TYPE
    }

    override fun filterOrder(): Int {
        return FilterConstants.PRE_DECORATION_FILTER_ORDER + 14
    }
}