package com.everwell.datagateway.filters.zuul.dbtservice;

import com.everwell.datagateway.filters.zuul.base.BaseZuulRouteFilter
import com.everwell.datagateway.service.BaseRestService
import com.everwell.datagateway.service.DBTServiceRestService
import com.netflix.zuul.context.RequestContext
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.cloud.netflix.zuul.filters.support.FilterConstants

open class DBTServiceZuulRouteFilter(

): BaseZuulRouteFilter() {

    @Autowired
    lateinit var dbtServiceRestService: DBTServiceRestService

    override fun getHeadersForAuth(context: RequestContext): MutableMap<String, String> {
        val map = mutableMapOf<String, String>()
        map["Authorization"] = "Bearer " + dbtServiceRestService.getAuthToken(context.request.getHeader("client-id"))!!
        map["x-client-id"] = context.request.getHeader("client-id")
        return map
    }

    override fun getUrlMapping(context: RequestContext): String? {
        return context.request.requestURI.substring(thisURI.length)
    }

    override fun getRestService(): BaseRestService {
        return dbtServiceRestService
    }

    override var thisURI: String
        get() = "/dbtservice"
        set(value) {}

    override fun filterType(): String {
        return FilterConstants.ROUTE_TYPE
    }

    override fun filterOrder(): Int {
        return FilterConstants.PRE_DECORATION_FILTER_ORDER + 11
    }
}
