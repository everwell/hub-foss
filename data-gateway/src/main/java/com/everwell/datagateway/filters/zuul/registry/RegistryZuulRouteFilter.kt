package com.everwell.datagateway.filters.zuul.registry

import com.everwell.datagateway.constants.Constants
import com.everwell.datagateway.constants.SecurityConstants.TOKEN_PREFIX
import com.everwell.datagateway.enums.ValidationStatusEnum
import com.everwell.datagateway.exceptions.ValidationException
import com.everwell.datagateway.filters.zuul.base.BaseZuulRouteFilter
import com.everwell.datagateway.service.BaseRestService
import com.everwell.datagateway.service.ClientService
import com.everwell.datagateway.service.RegistryRestService
import com.netflix.zuul.context.RequestContext
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.cloud.netflix.zuul.filters.support.FilterConstants
import org.springframework.security.core.context.SecurityContextHolder

open class RegistryZuulRouteFilter (

): BaseZuulRouteFilter() {

    @Autowired
    lateinit var registryRestService: RegistryRestService

    @Autowired
    lateinit var clientService: ClientService

    override var thisURI: String
        get() = "/registry"
        set(value) {}

    override fun getHeadersForAuth(context: RequestContext): MutableMap<String, String> {
        val map = mutableMapOf<String, String>()
        val authentication = SecurityContextHolder.getContext().authentication
        val username = authenticationService.getCurrentUsername(authentication)
        val client = clientService.findByUsername(username)
        var clientId: String
        if (client.accessibleClient != null) {
            clientId = client.accessibleClient.toString()
        } else {
            clientId = context.request.getHeader("client-id")
            if (clientId == null) {
                throw ValidationException(ValidationStatusEnum.CLIENT_ID_NOT_PRESENT.message)
            }
        }

        val generateUserAccessToken = context[Constants.GENERATE_USER_ACCESS_TOKEN]
        if (generateUserAccessToken == true) {
            val loginRequestDto = mapOf<String, String>("username" to client.username, "password" to client.password, "ipAddress" to client.ip)
            map["Authorization"] = TOKEN_PREFIX + " " + registryRestService.getUserAccessClientToken(loginRequestDto, clientId)
        } else {
            map["Authorization"] = TOKEN_PREFIX + " " + registryRestService.getAuthToken(clientId)!!
        }
        map["X-Client-Id"] = clientId
        return map
    }

    override fun getUrlMapping(context: RequestContext): String? {
        return context.request.requestURI.substring(thisURI.length)
    }

    override fun getRestService(): BaseRestService {
        return registryRestService
    }

    override fun filterType(): String {
        return FilterConstants.ROUTE_TYPE
    }

    override fun filterOrder(): Int {
        return FilterConstants.PRE_DECORATION_FILTER_ORDER + 12
    }
}