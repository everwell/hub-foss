package com.everwell.datagateway.filters.zuul.episode

import com.everwell.datagateway.filters.zuul.base.BaseZuulRouteFilter
import com.everwell.datagateway.service.BaseRestService
import com.everwell.datagateway.service.IamRestService
import com.netflix.zuul.context.RequestContext
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.cloud.netflix.zuul.filters.support.FilterConstants
import com.everwell.datagateway.constants.Constants.CLIENT_ID
import com.everwell.datagateway.service.EpisodeServiceRestService

open class EpisodeZuulRouteFilter (

): BaseZuulRouteFilter() {

    @Autowired
    lateinit var episodeRestService: EpisodeServiceRestService

    override var thisURI: String
        get() = "/episode"
        set(value) {}

    override fun getHeadersForAuth(context: RequestContext): MutableMap<String, String> {
        val map = mutableMapOf<String, String>()
        map["Authorization"] = "Bearer " + episodeRestService.getAuthToken(context.request.getHeader("client-id"))!!
        map["x-client-id"] = context.request.getHeader("client-id")
        return map
    }

    override fun getUrlMapping(context: RequestContext): String? {
        return context.request.requestURI.substring(thisURI.length)
    }

    override fun getRestService(): BaseRestService {
        return episodeRestService
    }

    override fun filterType(): String {
        return FilterConstants.ROUTE_TYPE
    }

    override fun filterOrder(): Int {
        return FilterConstants.PRE_DECORATION_FILTER_ORDER + 10
    }
}