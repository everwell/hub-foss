package com.everwell.datagateway.filters.zuul

import com.everwell.datagateway.filters.zuul.base.BaseZuulPostFilter
import org.springframework.cloud.netflix.zuul.filters.support.FilterConstants

abstract class GenericZuulPostFilter(

) : BaseZuulPostFilter(){

    override fun filterType(): String {
        return FilterConstants.POST_TYPE
    }

    override fun filterOrder(): Int {
        return FilterConstants.SEND_RESPONSE_FILTER_ORDER -2
    }
}