package com.everwell.datagateway.filters.zuul.globalLabsCallLogs

import com.everwell.datagateway.constants.CallConstants
import com.everwell.datagateway.constants.Constants
import com.everwell.datagateway.constants.SecurityConstants
import com.everwell.datagateway.enums.LegacyRoutesEnum
import com.everwell.datagateway.filters.zuul.base.BaseZuulRouteFilter
import com.everwell.datagateway.service.BaseRestService
import com.everwell.datagateway.service.RegistryRestService
import com.netflix.zuul.context.RequestContext
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.cloud.netflix.zuul.filters.support.FilterConstants
import org.springframework.http.HttpMethod
import org.springframework.http.client.BufferingClientHttpRequestFactory
import org.springframework.http.client.SimpleClientHttpRequestFactory
import org.springframework.web.client.RestTemplate

open class GlobeLabsCallLogsZuulRouteFilter(

) : BaseZuulRouteFilter() {

    @Autowired
    lateinit var registryRestService: RegistryRestService

    var restTemplateForApi: RestTemplate =
        RestTemplate(BufferingClientHttpRequestFactory(SimpleClientHttpRequestFactory()))

    override var thisURI: String
        get() = LegacyRoutesEnum.GLOBAL_LABS_CALL_LOGS.path
        set(value) {}

    override fun shouldFilter(): Boolean {
        val request = RequestContext.getCurrentContext().request
        return (request.requestURI.contains(
            LegacyRoutesEnum.GLOBAL_LABS_CALL_LOGS.path,
            true
        ) && request.method.equals(HttpMethod.POST.name))
    }

    init {
        urlMapping.put(LegacyRoutesEnum.GLOBAL_LABS_CALL_LOGS.path, CallConstants.CALL_LOGS_RE_ROUTE_URI)
    }

    override fun getHeadersForAuth(context: RequestContext): MutableMap<String, String> {
        val map = mutableMapOf<String, String>()
        var clientId: String = appProperties.clientIdGlobeLabsCallLogs
        map[SecurityConstants.HEADER_STRING] = SecurityConstants.TOKEN_PREFIX + " " + registryRestService.getAuthToken(clientId)!!
        map[Constants.X_Client_Id] = clientId
        return map
    }

    override fun getRestService(): BaseRestService {
        return registryRestService
    }

    override fun filterType(): String {
        return FilterConstants.ROUTE_TYPE
    }

    override fun filterOrder(): Int {
        return FilterConstants.PRE_DECORATION_FILTER_ORDER + 3
    }
}