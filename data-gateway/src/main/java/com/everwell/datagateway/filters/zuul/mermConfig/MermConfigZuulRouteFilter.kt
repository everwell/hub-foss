package com.everwell.datagateway.filters.zuul.mermConfig

import com.everwell.datagateway.constants.SecurityConstants
import com.everwell.datagateway.filters.zuul.base.BaseZuulRouteFilter
import com.everwell.datagateway.models.response.ApiResponse
import com.everwell.datagateway.models.response.ImeiClientResponse
import com.everwell.datagateway.service.BaseRestService
import com.everwell.datagateway.service.RegistryRestService
import com.everwell.datagateway.utils.Utils
import com.netflix.zuul.context.RequestContext
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.cloud.netflix.zuul.filters.support.FilterConstants
import org.springframework.core.ParameterizedTypeReference
import org.springframework.http.HttpEntity
import org.springframework.http.HttpHeaders
import org.springframework.http.HttpMethod
import org.springframework.http.MediaType
import org.springframework.http.client.BufferingClientHttpRequestFactory
import org.springframework.http.client.SimpleClientHttpRequestFactory
import org.springframework.web.client.RestTemplate

open class MermConfigZuulRouteFilter (

): BaseZuulRouteFilter() {

    @Autowired
    lateinit var registryRestService: RegistryRestService

    var restTemplateForApi: RestTemplate = RestTemplate(BufferingClientHttpRequestFactory(SimpleClientHttpRequestFactory()))

    override var thisURI: String
        get() = "/get-merm-config"
        set(value) {}

    init {
        urlMapping.put("/get-merm-config","/v1/merm/event")
    }

    override fun getHeadersForAuth(context: RequestContext): MutableMap<String, String> {
        val map = mutableMapOf<String, String>()
        var clientId : String = fetchClientId(context)
        map["Authorization"] = SecurityConstants.TOKEN_PREFIX + " " + registryRestService.getAuthToken(clientId)!!
        map["X-Client-Id"] = clientId
        return map
    }

    override fun getRestService(): BaseRestService {
        return registryRestService
    }

    override fun filterType(): String {
        return FilterConstants.ROUTE_TYPE
    }

    override fun filterOrder(): Int {
        return FilterConstants.PRE_DECORATION_FILTER_ORDER -2
    }

    private fun fetchClientId(context: RequestContext) : String {
        val headers = HttpHeaders()
        val request = "SN=" + context.requestQueryParams.getValue("SN").get(0)
        headers.add("Content-Type", MediaType.APPLICATION_JSON.toString())
        var clientId: String = appProperties.registryDefaultClientId;
        try {
            clientId = restTemplateForApi.exchange(
                appProperties.registryServerUrl + "/v1/imei/client-id", HttpMethod.POST, HttpEntity<Any>(request, headers),
                object : ParameterizedTypeReference<ApiResponse<ImeiClientResponse>>() {} ).body?.data?.clientId.toString()
        } catch (e : Exception) {
            // If IMEI is unknown don't route request
            context.setSendZuulResponse(false)
        }
        return clientId;
    }
}