package com.everwell.datagateway.filters.zuul.callLogs

import com.everwell.datagateway.constants.SecurityConstants
import com.everwell.datagateway.enums.LegacyRoutesEnum
import com.everwell.datagateway.filters.zuul.httpServlet.GenericHttpServletRequest
import com.everwell.datagateway.models.request.AfricasTalkingAscentCallBackRequest
import com.everwell.datagateway.utils.Utils
import com.netflix.zuul.ZuulFilter
import com.netflix.zuul.context.RequestContext
import org.springframework.cloud.netflix.zuul.filters.support.FilterConstants
import org.springframework.http.HttpHeaders
import org.springframework.http.HttpMethod
import org.springframework.http.MediaType

class CallLogsZuulPreFilter() : ZuulFilter() {

    override fun shouldFilter(): Boolean {
        val request = RequestContext.getCurrentContext().request
        return (request.requestURI.contains(
            LegacyRoutesEnum.CALL_LOGS.path,
            true
        ) && request.method.equals(HttpMethod.POST.name))
    }

    override fun filterType(): String {
        return FilterConstants.PRE_TYPE
    }

    override fun filterOrder(): Int {
        return 1
    }

    override fun run(): Any? {
        val ctx: RequestContext = RequestContext.getCurrentContext()
        val mp = mutableMapOf<String, String>()
        for (name in ctx.request.parameterNames) {
            mp[name] = ctx.request.getParameter(name)
        }
        val africasTalkingAscentCallBackRequest = AfricasTalkingAscentCallBackRequest(mp)
        convertToPost(africasTalkingAscentCallBackRequest, ctx)
        ctx.zuulRequestHeaders.clear()
        ctx.zuulRequestHeaders.put(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
        return null
    }

    private fun convertToPost(input: Any, ctx: RequestContext) {
        val requestData = Utils.asJsonString(input)
        val request = GenericHttpServletRequest(
            ctx.request,
            requestData.toByteArray(),
            HttpMethod.POST.name,
            MediaType.APPLICATION_JSON_VALUE
        )
        ctx.setRequest(request)
    }

}