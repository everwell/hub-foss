package com.everwell.datagateway.filters.zuul

import com.everwell.datagateway.constants.Constants
import com.everwell.datagateway.service.AuthenticationService
import com.everwell.datagateway.service.ClientRequestsService
import com.netflix.zuul.ZuulFilter
import com.netflix.zuul.context.RequestContext
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.security.core.context.SecurityContextHolder

abstract class RequestLogFilterBase: ZuulFilter() {
    @Autowired
    lateinit var authenticationService: AuthenticationService

    @Autowired
    lateinit var clientRequestsService: ClientRequestsService

    protected var disabledEndpoints: Map<String, List<Regex>> = mapOf()

    override fun shouldFilter():Boolean{
        val request = RequestContext.getCurrentContext().request
        val prefix = request.requestURI.split("/")[1]
        val path = request.requestURI.substring(prefix.length + 1) // adding 1 to accommodate the / at the beginning of requestURI
        val authentication = SecurityContextHolder.getContext().authentication
        val clientName = authenticationService.getCurrentUsername(authentication)
        return (Constants.INTERNAL_CLIENT_USERNAME != clientName && !(this.disabledEndpoints.containsKey(prefix) && this.disabledEndpoints[prefix]!!.any { it.matches(path) }))
    }
}