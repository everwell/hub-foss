package com.everwell.datagateway.utils;

import com.everwell.datagateway.constants.Constants;
import com.everwell.datagateway.exceptions.ValidationException;
import com.everwell.datagateway.filters.zuul.httpServlet.GenericHttpServletRequest;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.netflix.zuul.context.RequestContext;
import lombok.Getter;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.util.CollectionUtils;
import org.springframework.web.util.UriComponentsBuilder;

import javax.crypto.Cipher;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;
import javax.servlet.http.HttpServletRequest;
import java.io.*;
import java.nio.charset.StandardCharsets;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.zip.GZIPInputStream;
import java.util.zip.GZIPOutputStream;

import static com.everwell.datagateway.constants.Constants.SCHEME_DETAILS;
import static com.everwell.datagateway.constants.Constants.ZONE_DATE_FORMAT;

public class Utils {
    @Getter
    private static ObjectMapper objectMapper = new ObjectMapper();


    private static final Logger LOGGER = LoggerFactory.getLogger(Utils.class);

    public static String readRequestBody(InputStream inputStream) throws IOException {
        StringWriter writer = null;
        if (inputStream != null) {
            writer = new StringWriter();
            IOUtils.copy(inputStream, writer, "UTF-8");
        }
        return writer != null ? writer.toString() : null;
    }

    /**
     * This method is used to read the response body from the RequestContext object.
     * @param ctx
     * @return
     * @throws IOException
     */
    public static String readResponseBody(RequestContext ctx) throws IOException {
        if(ctx.getResponseGZipped())
        {
            return readGzipRequestBody(ctx);
        }
        else
        {
            InputStream inputStream = ctx.getResponseDataStream();
            StringWriter writer = null;
            if (inputStream != null) {
                writer = new StringWriter();
                IOUtils.copy(inputStream, writer, "UTF-8");
            }
            String requestBody = "";
            if(null != writer)
            {
                requestBody = writer.toString();
                ctx.setResponseDataStream(new ByteArrayInputStream(requestBody.getBytes()));
            }
            return requestBody;
        }
    }

    public static String asJsonString(final Object obj) {
        try {
            return objectMapper.writeValueAsString(obj);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    public static <T> T convertStrToObject(String jsonArray, Class<T> type) throws JsonProcessingException {
        return objectMapper.readValue(jsonArray, type);
    }

    public static <T> T convertStrToObject(String json, TypeReference<T> typeReference) throws IOException {
//      Disabling the FAIL_ON_UNKNOWN_PROPERTIES so that in case the input json has extra properties
//      other than the properties in the Class that needs to be converted to, jackson mapper doesn't throw exception
        objectMapper.disable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES);
        return objectMapper.readValue(json, typeReference);
    }

    public static Integer getRemainingTime() {  // Returns remaining time in day in seconds
        Integer secondsInADay = 86400;
        Calendar c = Calendar.getInstance();
        Integer nowTimeElapsedInSeconds = (c.get(Calendar.HOUR_OF_DAY) * 60 + c.get(Calendar.MINUTE)) * 60;
        LOGGER.debug("Current time in s " + nowTimeElapsedInSeconds);
        LOGGER.debug("TTL :" + (secondsInADay - nowTimeElapsedInSeconds));
        return (secondsInADay - nowTimeElapsedInSeconds);
    }

    public static String getTimeinFormat(String format, Long seconds) {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern(format);
        LocalDateTime utcNow = LocalDateTime.now(ZoneOffset.UTC);
        LocalDateTime utcNowPlusSec = utcNow.plusSeconds(seconds);
        return formatter.format(utcNowPlusSec);
    }

    public static String readGzipRequestBody(RequestContext ctx) throws IOException {
        InputStream inputStream = ctx.getResponseDataStream();
        StringWriter writer = null;
        if (inputStream != null) {
            writer = new StringWriter();
            GZIPInputStream gzipInput = new GZIPInputStream(inputStream);
            IOUtils.copy(gzipInput, writer, StandardCharsets.UTF_8);
        }
        String requestBody = "";
        if(null != writer)
        {
            requestBody = writer.toString();
            // Compress the data and set it back to the response data stream
            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
            try (GZIPOutputStream gzipOutputStream = new GZIPOutputStream(byteArrayOutputStream)) {
                gzipOutputStream.write(requestBody.getBytes(StandardCharsets.UTF_8));
            }
            ctx.setResponseDataStream(new ByteArrayInputStream(byteArrayOutputStream.toByteArray()));
        }
        return requestBody;
    }

    public static String convertToZoneDateTime(String date, ZoneId z,String format) {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern(format);
        LocalDateTime localDate = LocalDateTime.parse(date, formatter);
        ZonedDateTime zonedDateTime = localDate.atZone(z);
        DateTimeFormatter outputFormatter = DateTimeFormatter.ofPattern(ZONE_DATE_FORMAT);
        return zonedDateTime.format(outputFormatter);
    }
    /**
     * Method to convert input request Method
     *
     * @param input  - request body which is to be sent
     * @param ctx    - context of given request
     * @param method - HTTP method to which input request is to be converted
     */
    public static void convertRequest(Object input, RequestContext ctx, HttpMethod method) {
        String requestData = asJsonString(input);
        HttpServletRequest request = new GenericHttpServletRequest(
                ctx.getRequest(),
                requestData.getBytes(StandardCharsets.UTF_8),
                method.name(),
                MediaType.APPLICATION_JSON_VALUE
        );
        ctx.setRequest(request);
    }

    public static String convertDateToTimeStamp(String dateString) {
        String format = "yyyy-MM-dd"; // Date format
        SimpleDateFormat formatter = new SimpleDateFormat(format);
        try {
            formatter.setTimeZone(TimeZone.getTimeZone("UTC"));
            Date date = formatter.parse(dateString);
            long unixTimestamp = date.getTime();
            return String.valueOf(unixTimestamp);
        } catch (Exception e) {
            SentryUtils.captureException(e);
        }
        return null;
    }

    public static void capitalizeString(String[] values) {
        for (int index = 0; index < values.length; index++) {
            values[index] = StringUtils.capitalize(values[index]);
        }
    }

    public static boolean checkStringContainsOnlyNumbers(String value) {
        String pattern = Constants.NUMERIC_PATTERN;
        return value.matches(pattern);
    }

    public static <T> T processResponseEntity(ResponseEntity<String> responseEntity, int statusCode, Class<T> type, String errorMessage) throws JsonProcessingException {
        if (null == responseEntity || null == responseEntity.getBody()) {
            throw new ValidationException(errorMessage);
        } else if (!responseEntity.getStatusCode().is2xxSuccessful()) {
            throw new ValidationException(responseEntity.getBody());
        }
        return statusCode == responseEntity.getStatusCodeValue() ? convertStrToObject(responseEntity.getBody(), type) : null;
    }

    public static String buildURIString(String url, Map<String, Object> queryParams) {
        UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(url);
        if (!CollectionUtils.isEmpty(queryParams)) {
            queryParams.forEach(builder::queryParam);
        }
        return builder.toUriString();
    }


    public static String getFormattedDateNew(LocalDateTime localDateTime) {
        if (localDateTime == null) return null;
        return getFormattedDateNew(localDateTime, Constants.DATE_TIME_FORMAT);
    }

    public static String getFormattedDateNew(LocalDateTime localDateTime, String format) {
        try {
            DateTimeFormatter formatter = DateTimeFormatter.ofPattern(format);
            return localDateTime.format(formatter);
        } catch (Exception ex){
            return null;
        }
    }

    public static String convertDateToString(Date date, String format) {
        if(null == date) {
            return  null;
        }
        SimpleDateFormat dateFormat = new SimpleDateFormat(format);
        return dateFormat.format(date);
    }

    public static final String getFullName(String firstName, String lastName) {
        return firstName + " " + Objects.toString(lastName, "");
    }

    public static String convertDateStringFormat(String date, String currentFormat, String requiredFormat) {
        LocalDateTime localDateTime = convertStringToLocalDateTime(date, currentFormat);
        return convertLocalDateTimeToString(localDateTime, requiredFormat);
    }

    public static LocalDateTime convertStringToLocalDateTime(String date, String format) {
        try {
            DateTimeFormatter formatter = DateTimeFormatter.ofPattern(format);
            return LocalDateTime.parse(date, formatter);
        } catch (Exception e) {
            throw new IllegalArgumentException("[convertStringToLocalDateTime] Unable to parse date: " + date + " format: " + format);
        }
    }

    public static String convertLocalDateTimeToString(LocalDateTime date, String format) {
        try {
            DateTimeFormatter formatter = DateTimeFormatter.ofPattern(format);
            return date.format(formatter);
        } catch (Exception e) {
            throw new IllegalArgumentException("[convertLocalDateTimeToString] Unable to convert LocalDateTime: " + date + " format: " + format);
        }
    }
    public static String encryptData(String data, String privateKey) throws Exception {
        byte[] keyBytes = Base64.getDecoder().decode(privateKey);
        SecretKeySpec key = new SecretKeySpec(keyBytes, "AES");

        Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
        byte[] iv = new byte[16]; // Initialization Vector
        IvParameterSpec ivSpec = new IvParameterSpec(keyBytes, 0, 16);
        cipher.init(Cipher.ENCRYPT_MODE, key, ivSpec);

        byte[] encryptedBytes = cipher.doFinal(data.getBytes("UTF-8"));

        return Base64.getEncoder().encodeToString(encryptedBytes);
    }
    public static Map<String,Object> formatIntoRequestBodySchema(List<Map<String, Object>> kpiData, Map<String,Object> schema) {
        Map<String,Object> finalJson = new HashMap<>();
        List<Map<String,Object>> schemeDetails = new ArrayList<>();
        for(Map<String, Object> entry : kpiData) {
            Map<String,Object> fieldMap  = new HashMap<>();
            for(String field : (List<String>) schema.get(SCHEME_DETAILS)){
                if(schema.containsKey(field)) {
                    Map<String,Object> innerFieldMap = new HashMap<>();
                    for(String innerField :(List<String>) schema.get(field)){
                        innerFieldMap.put(innerField,entry.get(innerField));
                    }
                    fieldMap.put(field,innerFieldMap);
                }
                else {
                    fieldMap.put(field,entry.get(field));
                }
            }
            schemeDetails.add(fieldMap);
        }
        finalJson.put(SCHEME_DETAILS,schemeDetails);
        return finalJson;
    }

    public static String convertAndFormatDateStringForTasker(String inputDate, String outputFormat) {
        try {
            SimpleDateFormat inputFormat = new SimpleDateFormat(Constants.TASKER_DATE_FORMAT);
            SimpleDateFormat outputFormatObj = new SimpleDateFormat(outputFormat);
            Date date = inputFormat.parse(inputDate);
            return outputFormatObj.format(date);
        } catch (Exception e) {
            SentryUtils.captureException(e);
        }
        return inputDate;
    }
}

