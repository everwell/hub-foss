package com.everwell.datagateway.utils;

import com.everwell.datagateway.exceptions.RedisException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.serializer.StringRedisSerializer;
import org.springframework.stereotype.Component;

import java.util.concurrent.TimeUnit;

@Component
public class CacheUtils {

    private static RedisTemplate<String, Object> redisTemplate;

    @Autowired
    public CacheUtils(RedisTemplate<String, Object> redisTemplate) {

        CacheUtils.redisTemplate = redisTemplate;
        redisTemplate.setValueSerializer(new StringRedisSerializer());
    }

    public static void putIntoCache(String key, Object value) {
        try {
            redisTemplate.opsForValue().set(key, value);
        } catch (Exception e) {
            throw new RedisException(e);
        }

    }

    public static void putIntoCache(String key, Object value, Integer ttl) {
        try {
            redisTemplate.opsForValue().set(key, value, ttl, TimeUnit.SECONDS);
        } catch (Exception e) {
            throw new RedisException(e);
        }
    }

    public static <T> T getFromCache(String key) {
        try {
            Object obj = redisTemplate.opsForValue().get(key);
            return (null == obj) ? null : (T) (obj);
        } catch (Exception e) {
            throw new RedisException(e);
        }
    }

    public static Long updateCacheByValue(String key, long value) {
        try {
            return redisTemplate.opsForValue().increment(key, value);
        } catch (Exception e) {
            throw new RedisException(e);
        }
    }

    public static boolean hasKey(String key) {
        try {
            return redisTemplate.hasKey(key);
        } catch (Exception e) {
            throw new RedisException(e);
        }
    }
}
