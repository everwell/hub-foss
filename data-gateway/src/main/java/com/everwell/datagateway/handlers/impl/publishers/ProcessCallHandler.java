package com.everwell.datagateway.handlers.impl.publishers;

import com.everwell.datagateway.entities.PublisherQueueInfo;
import com.everwell.datagateway.enums.EventEnum;
import com.everwell.datagateway.handlers.EventHandler;
import com.everwell.datagateway.models.dto.EventStreamingDTO;
import com.everwell.datagateway.models.dto.ProcessCallDto;
import com.everwell.datagateway.models.dto.PublisherData;
import com.everwell.datagateway.publishers.RMQPublisher;
import com.everwell.datagateway.utils.SentryUtils;
import com.everwell.datagateway.utils.Utils;
import com.fasterxml.jackson.core.JsonProcessingException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class ProcessCallHandler extends EventHandler {

    @Override
    public EventEnum getEventEnumerator() {
        return EventEnum.PROCESS_CALL;
    }

    @Autowired
    private RMQPublisher rmqPublisher;

    @Override
    public void publishEvent(PublisherData publisherData, PublisherQueueInfo publisherQueueInfo)  {
        try {
            EventStreamingDTO eventStreamingDTO = new EventStreamingDTO();
            eventStreamingDTO.setEventName(publisherQueueInfo.getEventName());
            ProcessCallDto processCallDto = Utils.convertStrToObject(publisherData.getData(), ProcessCallDto.class);
            eventStreamingDTO.setField(processCallDto);
            rmqPublisher.publish(eventStreamingDTO,publisherQueueInfo.getExchange(),publisherQueueInfo.getRoutingKey(),publisherQueueInfo.getReplyToRoutingKey(),publisherData.getReferenceId().toString());
        } catch (Exception e) {
            SentryUtils.captureException(e);
        }
    }
}
