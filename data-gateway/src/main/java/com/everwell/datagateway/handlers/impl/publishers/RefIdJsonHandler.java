package com.everwell.datagateway.handlers.impl.publishers;

import com.everwell.datagateway.enums.ResponseTypeEnum;
import com.everwell.datagateway.handlers.ResponseHandler;
import com.everwell.datagateway.models.dto.EventStreamingDTO;
import com.everwell.datagateway.models.response.ReferenceIdRMQResponse;
import com.everwell.datagateway.models.response.Response;
import com.everwell.datagateway.models.response.TestReplyQueueResponse;
import com.everwell.datagateway.utils.Utils;
import com.fasterxml.jackson.core.JsonProcessingException;
import org.springframework.stereotype.Component;

@Component
public class RefIdJsonHandler extends ResponseHandler {
    @Override
    public ResponseTypeEnum getResponseEnumerator() {
        return ResponseTypeEnum.SIMPLE_JSON_RESPONSE;
    }

    @Override
    public String convertToResponseType(Response<ReferenceIdRMQResponse> response) {
        return Utils.asJsonString(response);
    }
}
