package com.everwell.datagateway.constants;

public class CallConstants {
    public static final String DEPLOYMENT_CODE = "deploymentCode";
    public static final String LINK_ID = "linkId";
    public static final String TEXT = "text";
    public static final String MESSAGE = "message";
    public static final String TO = "to";
    public static final String FROM = "from";
    public static final String ID = "id";
    public static final String DATE = "date";
    public static final String DATE_TIME = "dateTime";
    public static final String CALL_LOGS_RE_ROUTE_URI = "/v1/deployment-call-back";
    public static final String DATE_TIME_FORMAT = "dd-MM-yyyy HH:mm:ss";
    public static final String DATE_TIME_FORMAT_2 = "yyyy-MM-dd HH:mm:ss";

    public static final String DATE_TIME_FORMAT_3 = "EEE MMM dd yyyy HH:mm:ss 'GMT'Z (z)";
    public static final String INBOUND_SMS_MESSAGE = "inboundSMSMessage";
    public static final String INBOUND_SMS_MESSAGE_LIST = "inboundSMSMessageList";
    public static final String MESSAGE_ID = "messageId";
    public static final String DESTINATION_ADDRESS = "destinationAddress";
    public static final String SENDER_ADDRESS = "senderAddress";

    public static final String GLOBE_LABS_DEPLOYMENT_CODE = "ASCPHL";
}
