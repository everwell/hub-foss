package com.everwell.datagateway.constants;

public class SecurityConstants {
    public static final String TOKEN_PREFIX = "Bearer ";
    public static final String HEADER_STRING = "Authorization";
    public static final String SIGN_UP_URL = "/register";
    public static final String GENERATE_TOKEN_URL="/token";
    public static final long EXPIRATION_TIME = 864_000_000;
    public static final String SECRET = "EverwellSecretJWTs";
    public static final String TEST_CONNECTION_URL="/test-connection";

    public static final String PROCESS_CALL_URL="/process-call";
    public static final String ACTUATOR_URL="/actuator/**";
    public static final String ACTUATOR_PROMETHEUS="/actuator/prometheus";
    public static final String PROCESS_MERM_EVENT = "/process-merm-event";
    public static final String GET_MERM_CONFIG = "/get-merm-config";
    public static final String HI_DATA_TRANSFER = "/v0.5/health-information/transfer";
    public static final String WIX_POST_CREATION_EVENT="/event/new/blog";
    public static final String[] AUTH_WHITELIST = {
            // -- swagger ui
            "/v2/api-docs",
            "/configuration/ui",
            "/swagger-resources/**",
            "/configuration/security",
            "/swagger-ui.html",
            "/webjars/**"
    };

}
