package com.everwell.datagateway.constants.routes;

import lombok.AllArgsConstructor;
import lombok.Getter;
    @Getter
    @AllArgsConstructor
    public enum EpisodeRoutes {
        EPISODE_SEARCH("/v2/episode/search"),
        CREATE_EPISODE("/v1/episode"),
        DEFAULT_DISEASE("/v1/disease/default"),
        UPDATE_EPISODE("/v1/episode"),
        DELETE_EPISODE("/v1/episode/delete"),
        NEW_POST_BLOG_EVENT("/v1/event/new/blog"),
        EPISODE_SEARCH_NON_ELASTIC("/v1/episode/search"),
        GET_DISEASES("/v1/disease");

        public final String path;
    }
