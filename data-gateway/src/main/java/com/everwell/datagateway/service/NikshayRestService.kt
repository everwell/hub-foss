package com.everwell.datagateway.service

import com.everwell.datagateway.models.response.ApiResponse
import com.everwell.datagateway.models.response.LinkInitiationOtpResponse
import org.json.simple.JSONObject

abstract class NikshayRestService : BaseRestService() {
    abstract fun sendOtpLinkInitiation(token: String,userId : String) : ApiResponse<LinkInitiationOtpResponse>
    abstract fun getAuthTokenForLims(client_id: String?): String?
}