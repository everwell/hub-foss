package com.everwell.datagateway.service;

import ca.uhn.fhir.context.FhirContext;
import ca.uhn.fhir.parser.IParser;
import com.everwell.datagateway.constants.Constants;
import com.everwell.datagateway.enums.FhirMaritalStatusEnum;
import com.everwell.datagateway.exceptions.ValidationException;
import com.everwell.datagateway.models.request.*;
import com.everwell.datagateway.models.response.ApiResponse;
import com.everwell.datagateway.models.response.DiseaseTemplate;
import com.everwell.datagateway.models.response.Episode;
import com.everwell.datagateway.utils.FhirUtil;
import com.everwell.datagateway.utils.SentryUtils;
import com.everwell.datagateway.utils.Utils;
import io.sentry.Sentry;
import org.hl7.fhir.r4.model.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;
import springfox.documentation.service.Contact;

import java.time.LocalDateTime;
import java.util.*;
import java.util.stream.Collectors;

import static com.everwell.datagateway.enums.StatusCodeEnum.*;
import static com.everwell.datagateway.enums.StatusCodeEnum.*;

@Service
public class PMIRServiceImpl implements PMIRService{

    @Autowired
    EpisodeServiceRestService episodeService;

    public Map<String, Object>  createUpdateEpisodeRequest(Patient patient, boolean isUpdate, Long clientId) {
        Map<String, Object> createUpdateEpisodeRequest = new HashMap<>();
        if(!isUpdate) {
            validateInputForPatientCreation(patient);
            createUpdateEpisodeRequest.put(Constants.DELETED, !patient.getActive());
        } else{
            createUpdateEpisodeRequest.put(Constants.EPISODE_ID, patient.getIdElement().getIdPart());
        }
        createUpdateEpisodeRequest.put(Constants.DATE_OF_BIRTH, Utils.convertDateToString(patient.getBirthDate(), Constants.DATE_FORMAT));
        createUpdateEpisodeRequest.put(Constants.GENDER, null != patient.getGender() ? patient.getGender().getDisplay() : null);
        setPersonName(createUpdateEpisodeRequest, patient);
        setEpisodeDefaultValues(createUpdateEpisodeRequest, clientId);
        setPersonAddressDetails(createUpdateEpisodeRequest, patient);
        setPersonTelecomDetails(createUpdateEpisodeRequest, patient);
        setPersonContactDetails(createUpdateEpisodeRequest, patient);
        setPersonCommunicationLanguage(createUpdateEpisodeRequest, patient);
        createUpdateEpisodeRequest.put(Constants.TREATMENT_OUTCOME, null != patient.getDeceasedBooleanType().getValue() && patient.getDeceasedBooleanType().booleanValue() ? Constants.DIED : null);
        if (null != patient.getMaritalStatus() && !CollectionUtils.isEmpty(patient.getMaritalStatus().getCoding())) {
            createUpdateEpisodeRequest.put(Constants.MARITAL_STATUS, FhirMaritalStatusEnum.getValueByCode(patient.getMaritalStatus().getCoding().get(0).getCode()));
        }
        if(!CollectionUtils.isEmpty(patient.getIdentifier())) {
            createUpdateEpisodeRequest.put(Constants.EXTERNAL_ID1, patient.getIdentifier().get(0).getValue());
        }
        return createUpdateEpisodeRequest;
    }

    private void setPersonTelecomDetails(Map<String, Object> createEpisodeRequest, Patient patient) {
        if(null != patient.getTelecom()) {
            patient.getTelecom().forEach(t -> {
                if (Constants.PHONE.equals(t.getSystem().getDisplay())) {
                    UserMobileRequest mobileList = new UserMobileRequest(t.getValue(), true, false);
                    createEpisodeRequest.put(Constants.MOBILE_LIST, Collections.singletonList(mobileList));
                } else if (Constants.EMAIL.equals(t.getSystem().getDisplay())) {
                    UserEmailRequest emailList = new UserEmailRequest(t.getValue(), true);
                    createEpisodeRequest.put(Constants.EMAIL_LIST, Collections.singletonList(emailList));
                }
            });
            if (!createEpisodeRequest.containsKey(Constants.MOBILE_LIST)) {
                throw new ValidationException(MOBILE_NUMBER_REQUIRED.message); // phone number is mandatory for person creation
            }
        }
    }

    private void setPersonContactDetails(Map<String, Object> createEpisodeRequest, Patient patient) {
        List<Patient.ContactComponent> patientContacts = patient.getContact();
        if(CollectionUtils.isEmpty(patientContacts)) {
            return;
        }
        Patient.ContactComponent patientContact = patientContacts.get(0);
        HumanName contactName = patientContact.getName();
        Address contactAddress = patientContact.getAddress();
        List<ContactPoint> contactTelecom = patientContact.getTelecom();
        if (contactName != null && !CollectionUtils.isEmpty(contactName.getGiven())) {
            String firstName = FhirUtil.getStringFromList(contactName.getGiven());
            createEpisodeRequest.put(Constants.CONTACT_PERSON_NAME, Utils.getFullName(firstName, contactName.getFamily()));
        }
        if (contactAddress != null) {
            createEpisodeRequest.put(Constants.CONTACT_PERSON_ADDRESS, contactAddress.getText());
        }
        if (contactTelecom != null) {
            ContactPoint phone = contactTelecom.stream().filter(t -> Constants.PHONE.equals(t.getSystem().getDisplay())).findFirst().orElse(null);
            createEpisodeRequest.put(Constants.CONTACT_PERSON_PHONE, null != phone ? phone.getValue() : null);
        }
    }

    private void setPersonAddressDetails(Map<String, Object> createEpisodeRequest, Patient patient) {
        List<Address> patientAddresses = patient.getAddress();
        if (!CollectionUtils.isEmpty(patientAddresses)) {
            Address address = patient.getAddress().get(0);
            createEpisodeRequest.put(Constants.ADDRESS, address.getText());
            createEpisodeRequest.put(Constants.CITY, address.getCity());
            createEpisodeRequest.put(Constants.PIN_CODE, address.getPostalCode());
        }
    }

    private void setEpisodeDefaultValues(Map<String, Object> createEpisodeRequest, Long clientId) {
        createEpisodeRequest.put(Constants.TYPE_OF_EPISODE, Constants.F2_PUBLIC);
        ApiResponse<DiseaseTemplate> defaultDisease = episodeService.getDefaultDiseaseForClientId(clientId);
        createEpisodeRequest.put(Constants.DISEASE_ID, defaultDisease.getData().getId());
        createEpisodeRequest.put(Constants.DISEASE_ID_OPTIONS, defaultDisease.getData().getId());
        createEpisodeRequest.put(Constants.ADDED_BY, "2");
        createEpisodeRequest.put(Constants.START_DATE, Utils.getFormattedDateNew(LocalDateTime.now()));
        createEpisodeRequest.put(Constants.TREATMENT_START_TIME_STAMP, Utils.getFormattedDateNew(LocalDateTime.now()));
        createEpisodeRequest.put(Constants.IAM_START_DATE,Utils.getFormattedDateNew(LocalDateTime.now()));
    }

    private void setPersonName(Map<String, Object> createUpdateEpisodeRequest, Patient patient) {
        List<HumanName> patientNames = patient.getName();
        if (!CollectionUtils.isEmpty(patientNames)) {
            HumanName name = patientNames.get(0);
            createUpdateEpisodeRequest.put(Constants.FIRST_NAME, FhirUtil.getStringFromList(name.getGiven()));
            createUpdateEpisodeRequest.put(Constants.LAST_NAME, name.getFamily());
        }
    }

    private void setPersonCommunicationLanguage(Map<String, Object> createUpdateEpisodeRequest, Patient patient) {
        List<Patient.PatientCommunicationComponent> patientCommunications = patient.getCommunication();
        if (!CollectionUtils.isEmpty(patientCommunications) && null != patientCommunications.get(0).getLanguage()) {
            createUpdateEpisodeRequest.put(Constants.LANGUAGE, patient.getCommunication().get(0).getLanguage().getCoding().get(0).getDisplay());
        }
    }

    private void validateInputForPatientCreation(Patient patient) {
        if (patient == null) {
            throw new ValidationException(BAD_REQUEST.getMessage());
        }
        List<HumanName> patientNames = patient.getName();
        if (CollectionUtils.isEmpty(patientNames) || patientNames.get(0) == null || CollectionUtils.isEmpty(patientNames.get(0).getGiven())) {
            throw new ValidationException(FIRST_NAME_REQUIRED.message);
        }
    }

    public List<Patient> getPatientList(String request) {
        Bundle bundle = FhirUtil.getBundleFromRequest(request);
        List<Patient> patientList = FhirUtil.getPatientListFromBundle(bundle);
        return patientList;
    }

    public List<Episode> addUpdateMultiplePatients(List<Patient> patientList, Long accessibleClientId, boolean update) {
        List<Episode> episodeList = new ArrayList<>();
        patientList.forEach( p -> {
            Map<String, Object> createEpisodeRequest = createUpdateEpisodeRequest(p, update, accessibleClientId);
            ApiResponse<Episode> episode = update ? episodeService.updateEpisode(createEpisodeRequest, accessibleClientId) : episodeService.createEpisode(createEpisodeRequest, accessibleClientId);
            episodeList.add(episode.getData());
        });
        return episodeList;
    }
    
}
