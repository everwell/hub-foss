package com.everwell.datagateway.service.impl;

import com.everwell.datagateway.entities.ClientAuthentication;
import com.everwell.datagateway.exceptions.ValidationException;
import com.everwell.datagateway.handlers.AuthenticationHandler;
import com.everwell.datagateway.handlers.AuthenticationHandlerMap;
import com.everwell.datagateway.models.dto.*;
import com.everwell.datagateway.repositories.ClientAuthenticationRepository;
import com.everwell.datagateway.service.ClientAuthenticationService;
import com.everwell.datagateway.utils.Utils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.everwell.datagateway.constants.Constants.CLIENT;
import static com.everwell.datagateway.constants.Constants.SUBSCRIBER_URL;

@Service
public class ClientAuthenticationServiceImpl implements ClientAuthenticationService {

    @Autowired
    private ClientAuthenticationRepository clientAuthenticationRepository;

    @Autowired
    private AuthenticationHandlerMap authenticationHandlerMap;

    private static final Logger LOGGER = LoggerFactory.getLogger(ClientAuthenticationServiceImpl.class);

    @Override
    public void validateCredentials(ClientAuthenticationDto clientAuthenticationDto) {
        try {
            if(!StringUtils.isEmpty(clientAuthenticationDto.getCredentials()) && !StringUtils.isEmpty(clientAuthenticationDto.getCredentialType())) {
                AuthenticationHandler authenticationHandler = authenticationHandlerMap.getAuthentication(clientAuthenticationDto.getCredentialType());
                if(null != authenticationHandler) {
                    authenticationHandler.validateCredentialDto(Utils.asJsonString(clientAuthenticationDto.getCredentials()));
                }
            }
        }catch (Exception ex) {
            throw new ValidationException("Invalid credential format. "+ex.getMessage());
        }
    }

    @Override
    public void saveAuthenticationDetails(ClientAuthenticationDto clientAuthenticationDto) {
       try {
           if(!StringUtils.isEmpty(clientAuthenticationDto.getCredentials()) && !StringUtils.isEmpty(clientAuthenticationDto.getCredentialType())) {
               ClientCredentialBase credentials = clientAuthenticationDto.getCredentials();
               String credentialType = clientAuthenticationDto.getCredentialType();
               AuthenticationHandler authenticationHandler = authenticationHandlerMap.getAuthentication(credentialType);
               if(null != authenticationHandler) {
                   String formattedCredentials = authenticationHandler.encryptSensitiveDetailsInObject(Utils.asJsonString(credentials));
                   ClientAuthentication clientAuthorization = new ClientAuthentication(formattedCredentials, credentialType.toUpperCase(), clientAuthenticationDto.getType(), clientAuthenticationDto.getTypeId());
                   clientAuthenticationRepository.save(clientAuthorization);
               }
           }
       }catch (Exception ex) {
           LOGGER.error("[saveAuthenticationDetails] unable to save Authentication Details");
           throw new ValidationException("Unable to save Authentication Details" + ex);
       }
    }

    @Override
    public AuthenticationDetailsDto getSpecificAuthenticationDetails(Long clientId, Long eventClientId) {
        try {
            AuthenticationDetailsDto clientAuthenticationDto = new AuthenticationDetailsDto();
            List<Long> typeIdList = new ArrayList<>();
            typeIdList.add(clientId);
            typeIdList.add(eventClientId);
            Map<String, AuthenticationDetailsDto> typeToCredentialMap = getAuthenticationDetails(typeIdList);
            String eventClientKey = SUBSCRIBER_URL + eventClientId;
            String clientKey = CLIENT + clientId;
            if(typeToCredentialMap.containsKey(eventClientKey)) {
                clientAuthenticationDto = typeToCredentialMap.get(eventClientKey);
            } else if (typeToCredentialMap.containsKey(clientKey)){
                clientAuthenticationDto = typeToCredentialMap.get(clientKey);
            }
            return clientAuthenticationDto;
        }catch (Exception ex) {
            LOGGER.error("[getSpecificAuthenticationDetails] unable to get specific Authentication Details");
            throw new ValidationException("Unable to get specific Authentication Details" + ex);
        }
    }

    @Override
    public Map<String, AuthenticationDetailsDto> getAuthenticationDetails(List<Long> typeIdList) {
        try {
            Map<String, AuthenticationDetailsDto> typeToClientAuthenticationMap = new HashMap<>();
            List<ClientAuthentication> clientAuthenticationList = clientAuthenticationRepository.findAllByTypeIdIn(typeIdList);
            for(ClientAuthentication clientAuthentication : clientAuthenticationList) {
                AuthenticationDetailsDto authenticationDetailsDto = new AuthenticationDetailsDto(clientAuthentication.getCredentials(), clientAuthentication.getCredentialsType());
                typeToClientAuthenticationMap.put(clientAuthentication.getType().toString()+clientAuthentication.getTypeId(), authenticationDetailsDto);
            }
            return  typeToClientAuthenticationMap;
        }catch (Exception ex) {
            LOGGER.error("[getAuthenticationDetails] unable to get Authentication Details");
            throw new ValidationException("Unable to get Authentication Details" + ex);
        }
    }

}
