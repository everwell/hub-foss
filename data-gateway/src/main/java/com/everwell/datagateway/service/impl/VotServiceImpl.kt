package com.everwell.datagateway.service.impl

import com.everwell.datagateway.constants.Constants
import com.everwell.datagateway.enums.VotRoutes
import com.everwell.datagateway.service.VotService
import org.slf4j.LoggerFactory
import org.springframework.core.ParameterizedTypeReference
import org.springframework.stereotype.Service
import org.springframework.util.LinkedMultiValueMap
import org.springframework.util.StringUtils
import java.util.*

@Service
public class VotServiceImpl : VotService() {

    private val LOGGER = LoggerFactory.getLogger(VotServiceImpl::class.simpleName)


    override fun getAuthToken(clientId: String?): String? {
        val userName = appProperties.votUsername
        if (!clientIdAccessTokenMap.contains(userName) || Date().time >= clientIdRefreshTimeMap[userName]!!) {
            LOGGER.debug("[VotRestServiceImpl] Fetching auth token...")
            val headers = mutableMapOf<String, String>()
            val body: LinkedMultiValueMap<String, String> = LinkedMultiValueMap()
            body[Constants.USERNAME] = userName
            body[Constants.PASSWORD] = appProperties.votPassword
            val resp = authenticate(
                    appProperties.votServerUrl + VotRoutes.VOT_TOKEN.path,
                    headers,
                    body,
                    object : ParameterizedTypeReference<Map<String, Any>>() {})
            clientIdAccessTokenMap[userName] = resp.body?.get(Constants.TOKEN)?.toString() ?: ""
            if(!StringUtils.isEmpty(clientIdAccessTokenMap[userName])) {
                clientIdRefreshTimeMap[userName] = Date().time + Constants.MICROSERVICE_REFRESH_EXPIRY
            }
        }
        return clientIdAccessTokenMap[userName]
    }
}