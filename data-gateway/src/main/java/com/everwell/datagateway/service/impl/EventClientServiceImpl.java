package com.everwell.datagateway.service.impl;

import com.everwell.datagateway.entities.EventClient;
import com.everwell.datagateway.repositories.EventClientRepository;
import com.everwell.datagateway.service.EventClientService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
@Service
public class EventClientServiceImpl implements EventClientService {

    @Autowired
    EventClientRepository eventClientRepository;
    @Override
    public List<EventClient> getEventClientByClientId(Long clientId) {
        return eventClientRepository.findAllByClientId(clientId);
    }
}
