package com.everwell.datagateway.service;

import com.everwell.datagateway.models.request.AbdmClientDto.HipOnRequestDto;
import com.everwell.datagateway.models.request.abdm.ABDMConsentInitiationRequest;
import org.springframework.http.HttpEntity;

import java.util.Map;

public interface ABDMHelperService {
    void hipOnRequestCall(HipOnRequestDto data);
    boolean pushToCallBackUrl(String url, String data);
    void hipHiTransferNotify(String data);
    Map<Integer,String> exchange(String url, HttpEntity<String> request);
    void getToken();
    void consentRequestInit(ABDMConsentInitiationRequest req);
}
