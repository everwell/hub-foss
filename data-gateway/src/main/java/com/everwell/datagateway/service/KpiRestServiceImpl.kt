package com.everwell.datagateway.service

import com.everwell.datagateway.constants.Constants
import com.everwell.datagateway.models.request.KpiDataRequest
import com.everwell.datagateway.models.response.ApiResponse
import com.everwell.datagateway.models.response.KpiDataResponse
import com.everwell.datagateway.models.response.MicroServiceGenericAuthResponse
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.core.ParameterizedTypeReference
import org.springframework.http.HttpMethod
import org.springframework.http.ResponseEntity
import org.springframework.stereotype.Service
import java.util.*

@Service
class KpiRestServiceImpl: KpiRestService() {

    private val LOGGER = LoggerFactory.getLogger(KpiRestServiceImpl::class.simpleName)

    override fun getAuthToken(client_id: String?): String? {
        if (!clientIdAccessTokenMap.contains(client_id) || Date().time >= clientIdRefreshTimeMap[client_id]!!) {
            LOGGER.debug("[KpiRestServiceImpl] Fetching auth token...")
            val headers = mutableMapOf<String, String>()
            headers["X-Client-Id"] = client_id!!
            val resp = authenticate(
                    appProperties.kpiServerUrl + "/v1/client",
                    headers,
                    object : ParameterizedTypeReference<ApiResponse<MicroServiceGenericAuthResponse>>() {})
            clientIdAccessTokenMap[client_id] = resp.body?.data?.authToken!!
            clientIdRefreshTimeMap[client_id] = resp.body?.data?.nextRefresh!!
        }
        return clientIdAccessTokenMap[client_id]
    }

    private val logger : Logger = LoggerFactory.getLogger(KpiRestServiceImpl::class.simpleName)
    override fun getKPIData(kpiDataRequest: KpiDataRequest, datasetId: Int, clientId: String): ApiResponse<KpiDataResponse> {
        logger.debug("[KPIServiceImpl] Fetching KPI data...")
        val headers = getAuthorizationHeaders(clientId)
        var response : ResponseEntity<ApiResponse<KpiDataResponse>>? = null
        try {
            response = authenticate(
                    appProperties.kpiServerUrl + "/v1/kpi/data/" + datasetId,
                    headers,
                    kpiDataRequest,
                    HttpMethod.POST,
                    object:  ParameterizedTypeReference<ApiResponse<KpiDataResponse>>() {})
        } catch (ex: Exception) {
            throw Exception(ex.message)
        }
        return response.body!!
    }

    private fun getAuthorizationHeaders(clientId : String) : MutableMap<String, String> {
        val headers = mutableMapOf<String, String>()
        val token = getAuthToken(clientId)
        headers[Constants.AUTHORIZATION] = Constants.BEARER + " " + "$token"
        return headers
    }
}