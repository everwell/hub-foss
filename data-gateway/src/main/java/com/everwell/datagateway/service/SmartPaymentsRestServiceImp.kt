package com.everwell.datagateway.service

import com.everwell.datagateway.constants.Constants
import com.everwell.datagateway.models.response.GenericAuthResponse
import org.slf4j.LoggerFactory
import org.springframework.core.ParameterizedTypeReference
import org.springframework.stereotype.Service
import org.springframework.util.LinkedMultiValueMap
import java.util.*

@Service
open class SmartPaymentsRestServiceImp : SmartPaymentsRestService() {

    private val LOGGER = LoggerFactory.getLogger(SmartPaymentsRestServiceImp::class.simpleName)

    override fun getAuthToken(client_id: String?): String? {
        if (!clientIdAccessTokenMap.contains(client_id!!) || Date().time >= clientIdRefreshTimeMap[client_id]!!) {
            LOGGER.debug("[Smart Payments] Fetching auth token...")
            val params: LinkedMultiValueMap<String, String> = LinkedMultiValueMap()
            params["grant_type"] = appProperties.smartPaymentUserGrantType
            params["client_id"] = appProperties.smartPaymentsClientIdMap.get(client_id)
            params["client_secret"] = appProperties.smartPaymentsClientIdSecretMap.get(client_id)
            val resp = authenticate(
                appProperties.smartPaymentsUrl + "/oauth2/token",
                HashMap(),
                params,
                object : ParameterizedTypeReference<GenericAuthResponse>() {})

            clientIdAccessTokenMap[client_id] = resp.body?.access_token!!
            clientIdRefreshTimeMap[client_id] = Date().time + Constants.AUTH_SERVER_REFRESH_EXPIRY
        }
        return clientIdAccessTokenMap[client_id]
    }
}