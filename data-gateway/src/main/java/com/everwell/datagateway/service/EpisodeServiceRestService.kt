package com.everwell.datagateway.service

import com.everwell.datagateway.models.request.EpisodeSearchRequest
import com.everwell.datagateway.models.request.SearchEpisodesRequest
import com.everwell.datagateway.models.response.*

abstract class EpisodeServiceRestService: BaseRestService() {
    abstract fun getEpisodesFromPerson(token: String, userId: Int): ApiResponse<List<GetEpisodeByPersonResponse>>
    abstract fun updateEpisodeDetails(token: String, data: Map<String, Any>): ApiResponse<GetEpisodeByPersonResponse>
    abstract fun episodeSearch(episodeSearchRequest: EpisodeSearchRequest, clientId: Long): EpisodeSearchResult
    abstract fun createEpisode(createEpisodeRequest: Map<String, Object>, clientId: Long): ApiResponse<Episode>
    abstract fun getDefaultDiseaseForClientId(clientId: Long): ApiResponse<DiseaseTemplate>
    abstract fun updateEpisode(updateEpisodeRequest: Map<String, Object>, clientId: Long): ApiResponse<Episode>
    abstract fun deleteEpisode(episodeId: Long, clientId: Long): ApiResponse<String>
    abstract fun episodeSearch(url: String, episodeSearchRequest: EpisodeSearchRequest, clientId: Long): EpisodeSearchResult
    abstract fun createEpisode(url: String, createEpisodeRequest: Map<String, Object>, clientId: Long): ApiResponse<Episode>
    abstract fun updateEpisode(url: String, updateEpisodeRequest: Map<String, Object>, clientId: Long): ApiResponse<Episode>
    abstract fun getDiseases(url: String, clientId: Long): ApiResponse<DiseaseTemplateResponse>
    abstract fun episodeSearchV1(url: String, searchEpisodesRequest: SearchEpisodesRequest, clientId: Long): List<Long>
}