package com.everwell.datagateway.service;

import com.everwell.datagateway.entities.Client;
import com.everwell.datagateway.entities.Event;
import com.everwell.datagateway.entities.EventClient;
import com.everwell.datagateway.entities.SubscriberUrl;
import com.everwell.datagateway.enums.AuthenticationTypeEnum;
import com.everwell.datagateway.enums.ResponseTypeEnum;
import com.everwell.datagateway.models.dto.ClientAuthenticationDto;
import com.everwell.datagateway.models.request.SubscribeEventRequest;
import com.everwell.datagateway.repositories.ClientRepository;
import com.everwell.datagateway.repositories.EventClientRepository;
import com.everwell.datagateway.repositories.SubscriberUrlRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.util.Optional;


@Service
public class ClientServiceImpl implements ClientService {

    private static Logger logger = LoggerFactory.getLogger(ClientService.class);

    @Autowired
    private ClientRepository clientRepository;


    @Autowired
    private EventService eventService;


    @Autowired
    private AuthenticationService authenticationService;

    @Autowired
    private ClientAuthenticationService clientAuthenticationService;


    @Autowired
    private EventClientRepository eventClientRepository;


    @Autowired
    private SubscriberUrlRepository subscriberUrlRepository;


    @Override
    public Client findByUsername(String username) {
        if (username == null)
            return null;
        return clientRepository.findByUsername(username.toLowerCase());
    }


    @Override
    public Client findByIp(String IP) {
        if (IP == null)
            return null;
        return clientRepository.findByIp(IP);
    }

    @Override
    public Boolean isClientExists(String username) {
        return findByUsername(username) != null;
    }

    @Override
    public void saveClient(Client client) {
        if (client.getUsername() == null)  //To Save from Null Ptr exception from .toLowerCase
            return;
        client.setUsername(client.getUsername().toLowerCase()); //Converting to LowerCase While Saving
        clientRepository.save(client);
    }


    @Override
    public Client getClientById(Long id) {
        Optional<Client> optionalClient = clientRepository.findById(id);
        return optionalClient.orElse(null);
    }

    /**
     * Method to check if client is subscribed to given event and is active
     *
     * @param clientId - Client id whose subscription is to be checked
     * @param eventId  - Event id for which subscription is to be checked
     * @return Whether client is subscribed to given event and subscription is active
     */
    @Override
    public Boolean isClientSubscribedToEvent(Long clientId, Long eventId) {
        EventClient eventClient = eventClientRepository.findByClientIdAndEventIdAndActiveTrue(clientId, eventId);
        return null != eventClient;
    }

    @Override
    public Boolean subscribeEvent(SubscribeEventRequest subscribeEventRequest) {
        if (eventService.isEventExists(subscribeEventRequest.getEventName())) {
            ClientAuthenticationDto clientAuthenticationDto = new ClientAuthenticationDto(subscribeEventRequest.getCredentialType(), subscribeEventRequest.getCredentials(), AuthenticationTypeEnum.SUBSCRIBER_URL, 0L);
            clientAuthenticationService.validateCredentials(clientAuthenticationDto);
            Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
            String username = authenticationService.getCurrentUsername(authentication);
            Client client = findByUsername(username);
            if (client == null) {
                logger.debug("Cannot Subscribe event for " + subscribeEventRequest.getEventName() + "as username is null");
                return false;
            }
            Event event = eventService.findEventByEventName(subscribeEventRequest.getEventName());
            //Check if client is already subscribed to that event or not
            boolean isAlreadySubscribed = null != eventClientRepository.findByClientIdAndEventId(client.getId(), event.getId());
            if (isAlreadySubscribed) {
                //To prevent Multiple entries , eg 1:5:1 and , 2:5:1
                logger.debug("Client is already subscribed to this event");
                return false;
            }
            EventClient eventClient = new EventClient();
            eventClient.setClientId(client.getId());
            eventClient.setEventId(event.getId());
            if (StringUtils.hasText(subscribeEventRequest.getResponseType())) {
                eventClient.setResponseType(subscribeEventRequest.getResponseType());
            } else {
                eventClient.setResponseType(ResponseTypeEnum.SIMPLE_JSON_RESPONSE.toString());
            }
            eventClientRepository.save(eventClient);
            SubscriberUrl subscriberUrl = new SubscriberUrl();
            subscriberUrl.setUrl(subscribeEventRequest.getSubscriberUrl());
            subscriberUrl.setEventClient(eventClient);
            subscriberUrl.setIsEnabled(true);
            String requestMethod = subscribeEventRequest.getRequestMethod();
            subscriberUrl.setRequestMethod(StringUtils.hasText(requestMethod) ? requestMethod : HttpMethod.POST.name());
            subscriberUrlRepository.save(subscriberUrl);
            clientAuthenticationDto.setTypeId(eventClient.getId());
            clientAuthenticationService.saveAuthenticationDetails(clientAuthenticationDto);
            return true;

        } else {

            logger.error("Event doesn't exists in the database");
            return false;


        }
    }


}
