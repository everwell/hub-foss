package com.everwell.datagateway.service;

import com.everwell.datagateway.models.request.AuthenticationRequest;
import com.everwell.datagateway.models.request.RegisterRequest;
import org.springframework.security.core.Authentication;

public interface AuthenticationService {
    String generateToken(AuthenticationRequest authenticationRequest);
    Boolean registerClient(RegisterRequest registerRequest);
    String getCurrentUsername(Authentication authentication);
}
