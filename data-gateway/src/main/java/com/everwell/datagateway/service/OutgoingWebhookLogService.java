package com.everwell.datagateway.service;

import com.everwell.datagateway.models.dto.OutgoingWebhookLogDTO;

public interface OutgoingWebhookLogService {
    Long saveOutgoingWebhookLog(OutgoingWebhookLogDTO outgoingWebhookLogDTO);

    void updateOutgoingWebhookLog(Long id, OutgoingWebhookLogDTO outgoingWebhookLogDTO);
}
