package com.everwell.datagateway.repositories;

import com.everwell.datagateway.entities.ClientRequests;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;

@Repository
public interface ClientRequestsRepository extends JpaRepository<ClientRequests, Long> {


    ClientRequests findTopById(Long id);

    List<ClientRequests> findByDeliveredAtLessThanEqual(Date deliveredAt);

    List<ClientRequests> findByCreatedAtGreaterThanEqualAndResultDeliveredFalse(Date createdAt);

    @Modifying
    @Transactional
    void deleteByDeliveredAtLessThanEqual(Date deliveredAt);

    @Modifying
    @Transactional
    @Query(value = "UPDATE client_requests SET response_data = :responseData WHERE id = :id", nativeQuery = true)
    int updateResponseData(@Param("id") Long id, @Param("responseData") String responseData);
}
