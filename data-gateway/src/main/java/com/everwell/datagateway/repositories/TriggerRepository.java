package com.everwell.datagateway.repositories;

import com.everwell.datagateway.entities.Trigger;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface TriggerRepository extends JpaRepository<Trigger,Long> {

    List<Trigger> getAllByCronTimeNotNullAndActiveTrue();
}
