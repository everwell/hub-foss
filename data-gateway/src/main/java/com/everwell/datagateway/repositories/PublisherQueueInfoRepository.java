package com.everwell.datagateway.repositories;

import com.everwell.datagateway.entities.PublisherQueueInfo;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface PublisherQueueInfoRepository extends JpaRepository<PublisherQueueInfo, Long> {


    @Override
    Optional<PublisherQueueInfo> findById(Long aLong);

    PublisherQueueInfo findFirstByEventName(String event);
}
