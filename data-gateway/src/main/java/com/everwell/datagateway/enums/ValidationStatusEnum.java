package com.everwell.datagateway.enums;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

@Getter
@RequiredArgsConstructor
public enum ValidationStatusEnum {
    REFERENCE_ID_MANDATORY("ReferenceId is mandatory"),
    FIRSTNAME_MANDATORY("FirstName is mandatory"),
    PHONE_NUMBER_MANDATORY("PhoneNumber is mandatory"),
    DATA_NOT_PRESENT_FOR_GIVEN_REFERENCE_ID("Referenced entry not found"),
    DATA_PRESENT_FOR_GIVEN_REFERENCE_ID("Referenced entry already exists"),
    DATA_ADD_UPDATE_FAILED("Unable to store data"),
    INVALID_ENCRYPTED_REQUEST("Invalid encrypted request"),
    CLIENT_ID_NOT_PRESENT("Could not extract client id");
    public final String message;
}
