package com.everwell.datagateway.enums;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

import java.util.ArrayList;
import java.util.List;

@Getter
@RequiredArgsConstructor
public enum TypeOfEpisodeEnum {
    INDIA_TB_PUBLIC("IndiaTbPublic"),
    INDIA_TB_PRIVATE("IndiaTbPrivate"),
    PUBLIC_PRIVATE_PARTNERSHIP("PublicPrivatePartnership");

    private final String name;

    private  static final List<String> typeOfEpisodeList = new ArrayList<>();

    static {
        for (TypeOfEpisodeEnum typeOfEpisodeEnum : TypeOfEpisodeEnum.values()) {
            typeOfEpisodeList.add(typeOfEpisodeEnum.getName());
        }
    }

    public static List<String> getTypeOfEpisodeList() {
        return typeOfEpisodeList;
    }

}