package com.everwell.datagateway.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum VotRoutes {
    VOT_TOKEN("/api-token-auth/");

    public final String path;

}
