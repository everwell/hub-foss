package com.everwell.datagateway.enums;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public enum FHIRFieldMappingEnum {

    DOSES_GIVEN("No. of Doses : "),
    DOSING_DATE("Dosing Date : "),
    DOSING_DAYS("No. of Days : "),
    CASE_TYPE("Type of Case : "),
    REGIMEN_TYPE("Type of Regimen : "),
    DIAGNOSIS_DATE("Diagnosis Date : "),
    DIAGNOSIS_BASIS("Basis of Diagnosis : "),
    WEIGHT_BAND("Weight Band : "),
    TREATMENT_PHASE("Phase : ");

    @Getter
    private final String label;
}
