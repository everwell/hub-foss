package com.everwell.datagateway.entities;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.CreationTimestamp;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "client_requests")
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class ClientRequests {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private Long subscriberUrlId;
    @CreationTimestamp
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdAt;
    private Boolean resultDelivered = false;
    @Column(columnDefinition = "TEXT")
    public String responseData;
    private Long eventId;
    private Long clientId;
    @Column(columnDefinition = "TEXT")
    private String requestData;
    private Date deliveredAt;
    private Date refIdDeliveredAt;
    private Integer callbackResponseCode;
    private String responsePayload;
    @Column(columnDefinition = "TEXT")
    private String requestUrl;
    @Column(columnDefinition = "TEXT")
    private String requestHeaders;

    public ClientRequests(Long clientId, String requestData, Long eventId, Long subscriberUrlId, String requestURI, String requestHeaders)
    {
        this.clientId = clientId;
        this.requestData = requestData;
        this.eventId = eventId;
        this.subscriberUrlId = subscriberUrlId;
        this.requestUrl = requestURI;
        this.requestHeaders = requestHeaders;
    }
}
