
package com.everwell.datagateway.entities;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

@Entity
@Table(name = "event_client")
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class EventClient {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public Long id;

    @Column(name = "event_id")
    public Long eventId;

    @Column(name = "client_id")
    public Long clientId;

    @Column(name = "response_type")
    public String responseType;

    private boolean active = true;
}

