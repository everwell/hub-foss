package com.everwell.datagateway.entities;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

@Entity
@Table(name = "trigger")
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class Trigger {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public Long id;
    @Column(name = "event_id")
    private Long eventId;
    @Column(name = "client_id")
    private Long clientId;
    @Column(name = "class_name")
    private String className;
    @Column(name = "function_name")
    private String functionName;
    @Column(name = "event_name")
    private String eventName;
    @Column(name = "cron_time")
    private String cronTime;
    private Boolean active;
    @Column(name = "extra_params")
    private String extraParams;

}
