package com.everwell.datagateway.models;

import com.everwell.datagateway.models.request.abdm.ABDMOnDiscoveryRequest;
import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class DiscoveryCacheDto {
    ABDMOnDiscoveryRequest abdmOnDiscoveryRequest;
    String healthNumber;
}
