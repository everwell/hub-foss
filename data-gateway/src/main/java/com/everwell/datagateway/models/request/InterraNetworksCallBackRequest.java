package com.everwell.datagateway.models.request;

import com.everwell.datagateway.constants.CallConstants;
import com.everwell.datagateway.utils.Utils;
import com.fasterxml.jackson.core.type.TypeReference;
import lombok.*;
import org.springframework.util.CollectionUtils;

import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@NoArgsConstructor
@Getter
@Setter
public class InterraNetworksCallBackRequest extends DeploymentCallBackRequest{
    public InterraNetworksCallBackRequest(String requestSMSList) throws IOException {
        Map<String, List<Map<String, String>>> inboundSMSMessageInput = Utils.convertStrToObject(requestSMSList, new TypeReference<Map<String, Map<String, List<Map<String, String>>>>>() {
        }).get(CallConstants.INBOUND_SMS_MESSAGE_LIST);
        List<Map<String, String>> inboundSMSMessageList = inboundSMSMessageInput.get(CallConstants.INBOUND_SMS_MESSAGE);
        if(!CollectionUtils.isEmpty(inboundSMSMessageList)){
            Map<String, String> inboundSMSMessage = inboundSMSMessageList.get(0);
            this.deploymentCode = inboundSMSMessage.get(CallConstants.DEPLOYMENT_CODE);
            this.linkId = inboundSMSMessage.get(CallConstants.LINK_ID);
            this.text = inboundSMSMessage.get(CallConstants.MESSAGE);
            this.to = inboundSMSMessage.get(CallConstants.TO);
            this.id = inboundSMSMessage.get(CallConstants.ID);
            this.date = Utils.convertDateStringFormat(inboundSMSMessage.get(CallConstants.DATE_TIME), CallConstants.DATE_TIME_FORMAT, CallConstants.DATE_TIME_FORMAT_2);
            this.from = inboundSMSMessage.get(CallConstants.FROM);
            this.body = inboundSMSMessage.entrySet().stream()
                    .map(entry -> entry.getKey() + "=" + entry.getValue())
                    .collect(Collectors.joining("&"));
        }
    }
}
