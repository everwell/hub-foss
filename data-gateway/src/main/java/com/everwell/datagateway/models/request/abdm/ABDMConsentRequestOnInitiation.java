package com.everwell.datagateway.models.request.abdm;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
public class ABDMConsentRequestOnInitiation {
    public String requestId;
    public String timestamp;
    public ConsentRequest consentRequest;
    public Object error;
    public Resp resp;

    @Getter
    @Setter
    @AllArgsConstructor
    @NoArgsConstructor
    public static class ConsentRequest{
        public String id;
    }

    @Getter
    @Setter
    @AllArgsConstructor
    @NoArgsConstructor
    public static class Resp{
        public String requestId;
    }
}
