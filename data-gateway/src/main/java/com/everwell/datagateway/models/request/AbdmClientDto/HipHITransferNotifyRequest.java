package com.everwell.datagateway.models.request.AbdmClientDto;

import com.everwell.datagateway.models.dto.PublisherData;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

import static com.everwell.datagateway.constants.Constants.*;

@Getter
@Setter
public class HipHITransferNotifyRequest {
    public String requestId;
    public String timestamp;
    public Notification notification;

    public HipHITransferNotifyRequest(String transactionId, String consentId, List<String> episodeIds, String hipId) {
        DateTimeFormatter formatter =  DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss.SSSSSS");
        LocalDateTime utcNow =  LocalDateTime.now(ZoneOffset.UTC);
        this.requestId = UUID.randomUUID().toString();
        this.timestamp = formatter.format(utcNow);
        this.notification = new Notification(consentId,transactionId,hipId,episodeIds);
    }

    @Getter
    @Setter
    public static class StatusNotification{
        public String sessionStatus;
        public String hipId;
        public List<StatusResponse> statusResponses;
        public StatusNotification(String hipId, List<String> episodeIds) {
            this.sessionStatus = ABDM_HI_TRANSFER_SESSION_STATUS;
            this.hipId = hipId;
            this.statusResponses = episodeIds.stream().map(StatusResponse::new).collect(Collectors.toList());
        }
    }

    @Getter
    @Setter
    public static class StatusResponse{
        public String careContextReference;
        public String hiStatus;
        public String description;
        public StatusResponse(String careContextReference){
            this.careContextReference = careContextReference;
            this.hiStatus = ABDM_HI_TRANSFER_STATUS;
            this.description = ABDM_HI_TRANSFER_STATUS;
        }
    }

    @Getter
    @Setter
    public static class Notification{
        public String consentId;
        public String transactionId;
        public String doneAt;
        public Notifier notifier;
        public StatusNotification statusNotification;
        public Notification(String consentId,String transactionId,String id, List<String> episodeIds) {
            this.notifier = new Notifier(id);
            this.consentId = consentId;
            this.transactionId = transactionId;
            this.doneAt = LocalDateTime.now(ZoneOffset.UTC).toString();
            this.statusNotification =  new StatusNotification(id,episodeIds);
        }
    }

    @Getter
    @Setter
    public static class Notifier{
        public String type;
        public String id;
        public Notifier(String id) {
            this.type = ABDM_TRANSFER_NOTIFIER_TYPE;
            this.id = id;
        }
    }
}
