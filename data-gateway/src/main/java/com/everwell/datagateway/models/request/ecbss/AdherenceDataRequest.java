package com.everwell.datagateway.models.request.ecbss;

import com.everwell.datagateway.models.dto.ecbss.AdherenceDetailsDto;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class AdherenceDataRequest {
    String entityId;
    String externalId1;
    String externalId2;
    AdherenceDetailsDto ecbssAdherenceDetailsDto;
}
