package com.everwell.datagateway.models.request.abdm;

import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

import static com.everwell.datagateway.constants.Constants.*;

public class HiuHITransferNotifyRequest {
    public String requestId;
    public String timestamp;
    public Notification notification;

    public HiuHITransferNotifyRequest(String transactionId, String consentId, List<String> episodeIds, String hipId,String status,String hiuId) {
        DateTimeFormatter formatter =  DateTimeFormatter.ofPattern(ABDM_TIMESTAMP_FORMAT);
        LocalDateTime utcNow =  LocalDateTime.now(ZoneOffset.UTC);
        this.requestId = UUID.randomUUID().toString();
        this.timestamp = formatter.format(utcNow);
        this.notification = new Notification(consentId,transactionId,hipId,hiuId,episodeIds,status);
    }

    public static class Notification{
        public String consentId;
        public String transactionId;
        public String doneAt;
        public Notifier notifier;
        public StatusNotification statusNotification;

        public Notification(String consentId,String transactionId,String hipId,String hiuId, List<String> episodeIds,String status) {
            this.notifier = new Notifier(hiuId);
            this.consentId = consentId;
            this.transactionId = transactionId;
            this.doneAt = LocalDateTime.now(ZoneOffset.UTC).toString();
            this.statusNotification =  new StatusNotification(hipId,episodeIds,status);
        }

    }

    public static class Notifier{
        public String type;
        public String id;

        public Notifier(String id) {
            this.type = ABDM_RECEIVE_NOTIFIER_TYPE;
            this.id = id;
        }
    }

    public static class StatusNotification{
        public String sessionStatus;
        public String hipId;
        public List<StatusResponse> statusResponses;

        public StatusNotification(String hipId, List<String> episodeIds,String status) {
            this.hipId = hipId;
            this.sessionStatus = status;
            this.statusResponses = episodeIds.stream().map(StatusResponse::new).collect(Collectors.toList());
        }
    }

    public static class StatusResponse{
        public String careContextReference;
        public String hiStatus;
        public String description;

        public StatusResponse(String careContextReference){
            this.careContextReference = careContextReference;
            this.hiStatus = ABDM_HI_RECEIVE_STATUS;
            this.description = ABDM_HI_RECEIVE_STATUS ;
        }
    }
}
