package com.everwell.datagateway.models.request.abdm;

import org.springframework.beans.factory.annotation.Value;

import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;
import java.util.UUID;

import static com.everwell.datagateway.constants.Constants.*;

public class HIDataRequest {

    public String requestId;
    public String timestamp;
    public HiRequest hiRequest;

    public HIDataRequest(String consentId,String from, String to, String url,String keyValue,String expiry,String nonce) {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern(ABDM_TIMESTAMP_FORMAT);
        LocalDateTime utcNow = LocalDateTime.now(ZoneOffset.UTC);
        this.requestId = UUID.randomUUID().toString();
        this.timestamp = formatter.format(utcNow);
        this.hiRequest = new HiRequest(consentId,from,to,url,keyValue,expiry,nonce);
    }

    public static class Consent{
        public String id;

        public Consent(String id) {
            this.id = id;
        }
    }

    public static class DateRange{
        public String from;
        public String to;

        public DateRange(String from, String to) {
            this.from = from;
            this.to = to;
        }
    }

    public static class DhPublicKey{
        public String expiry;
        public String parameters;
        public String keyValue;

        public DhPublicKey(String keyValue,String expiry) {
            this.expiry = expiry;
            this.keyValue = keyValue;
            this.parameters = DATA_TRANSFER_ENCRYPTION_PARAMETERS ;
        }
    }

    public static class HiRequest{
        public Consent consent;
        public DateRange dateRange;
        public String dataPushUrl;
        public KeyMaterial keyMaterial;

        public HiRequest(String id, String from, String to, String url,String keyValue,String expiry,String nonce) {
            this.consent = new Consent(id);
            this.dateRange = new DateRange(from,to);
            this.dataPushUrl = url;
            this.keyMaterial = new KeyMaterial(keyValue, expiry, nonce);
        }
    }

    public static class KeyMaterial{
        public String cryptoAlg;
        public String curve;
        public DhPublicKey dhPublicKey;
        public String nonce;

        public KeyMaterial(String keyValue,String expiry,String nonce) {
            this.dhPublicKey = new DhPublicKey(keyValue, expiry);
            this.cryptoAlg = ALGORITHM;
            this.curve = CURVE;
            this.nonce = nonce;
        }
    }
}

