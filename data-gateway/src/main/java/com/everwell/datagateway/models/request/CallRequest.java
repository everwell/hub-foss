package com.everwell.datagateway.models.request;

import com.everwell.datagateway.constants.Constants;
import com.everwell.datagateway.utils.Utils;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;
import java.util.StringJoiner;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class CallRequest {

    @NotBlank
    private String who;

    @NotBlank
    private String channelID;

    @NotBlank
    private String dateTime;

    @NotBlank
    private String deployment;

    private String queryParamsString;

    @Override
    public String toString() {
        StringJoiner queryString = new StringJoiner("&", "?", "");
        queryString.add("who=" + who);
        queryString.add("ChannelID=" + channelID);
        queryString.add("DateTime=" + dateTime);
        queryString.add("deployment=" + deployment);
        return queryString.toString();
    }

    public void formatRequest() {
        if (who.startsWith("0")) {
            who = who.substring(1);
        }
        dateTime = Utils.convertAndFormatDateStringForTasker(dateTime, Constants.DATE_TIME_FORMAT);
    }
}
