package com.everwell.datagateway.models.request.abdm;

import lombok.*;

import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;
import java.util.UUID;

import static com.everwell.datagateway.constants.Constants.ABDM_TIMESTAMP_FORMAT;

@Getter
public class ABDMDemographicLinkingTokenRequest {
    public String requestId;
    public String timestamp;
    public String transactionId;
    public Credential credential;

    public ABDMDemographicLinkingTokenRequest(String txnID, Credential cred)
    {
        DateTimeFormatter formatter =  DateTimeFormatter.ofPattern(ABDM_TIMESTAMP_FORMAT);
        LocalDateTime utcNow =  LocalDateTime.now(ZoneOffset.UTC);
        this.requestId = UUID.randomUUID().toString();
        this.timestamp = formatter.format(utcNow);
        this.transactionId = txnID;
        this.credential = cred;
    }

    @Getter
    public static class Credential {
        public Demographic demographic;

        @Getter
        @AllArgsConstructor
        public static class Demographic {

            public String name;
            public String gender;
            public String dateOfBirth;
            public ABDMIdentifiers identifier;
        }
        public Credential(String name,String gender,String yob,String identifierType,String identifierValue)
        {
            ABDMIdentifiers idf = new ABDMIdentifiers(identifierType,identifierValue);
            Demographic dgm =  new Demographic(name,gender,yob,idf);
            this.demographic = dgm;
        }
    }
}
