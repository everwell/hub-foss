package com.everwell.datagateway.models.request;

import com.everwell.datagateway.enums.ResponseTypeEnum;
import com.everwell.datagateway.exceptions.ValidationException;
import com.everwell.datagateway.models.dto.BasicAuthCredDto;
import com.everwell.datagateway.models.dto.ClientCredentialBase;
import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import com.fasterxml.jackson.databind.annotation.NoClass;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.http.HttpMethod;
import org.springframework.util.StringUtils;

import javax.validation.constraints.NotBlank;
import java.util.Arrays;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class SubscribeEventRequest {
    @NotBlank
    String eventName;
    @NotBlank
    String subscriberUrl;

    private String credentialType;
    private String responseType;

    @JsonTypeInfo(use = JsonTypeInfo.Id.NAME, property = "credentialType", include = JsonTypeInfo.As.EXTERNAL_PROPERTY, defaultImpl = NoClass.class)
    @JsonSubTypes(value = {
            @JsonSubTypes.Type(value = BasicAuthCredDto.class, name = "BASIC_AUTH")
    })
    private ClientCredentialBase credentials;

    private String requestMethod;

    public SubscribeEventRequest(String eventName, String subscriberUrl) {
        this.eventName = eventName;
        this.subscriberUrl = subscriberUrl;
    }

    public void validate()  {
        RegisterRequest.validateAuthenticationDetails(credentialType, credentials);
        if (!StringUtils.isEmpty(responseType) && Arrays.stream(ResponseTypeEnum.values())
                .noneMatch(type -> responseType.equalsIgnoreCase(type.toString()))){
            throw new ValidationException("ResponseType value can be only from "+ Arrays.toString(ResponseTypeEnum.values()));
        }
        if(!StringUtils.isEmpty(requestMethod) && Arrays.stream(HttpMethod.values()).noneMatch(httpMethod -> httpMethod.name().equals(requestMethod))) {
            throw new ValidationException("requestMethod value can be only from " + Arrays.toString(HttpMethod.values()));
        }
    }
}
