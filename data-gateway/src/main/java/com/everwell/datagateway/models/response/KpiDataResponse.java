package com.everwell.datagateway.models.response;

import lombok.Data;

import java.util.List;
import java.util.Map;

@Data
public class KpiDataResponse {
    private List<Map<String, Object>> kpiData;
}
