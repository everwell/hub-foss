package com.everwell.datagateway.models.request;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ReferenceIdRMQRequest {
    private Long referenceId;
    private String userName;
    private Long userId;
    private String data;
    private Long microServiceClientId;
}
