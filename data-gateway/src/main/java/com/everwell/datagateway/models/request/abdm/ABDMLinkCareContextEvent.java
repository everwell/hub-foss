package com.everwell.datagateway.models.request.abdm;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.*;

import java.io.Serializable;
import java.time.LocalDateTime;

@NoArgsConstructor
@AllArgsConstructor
@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class ABDMLinkCareContextEvent implements Serializable {
    public long episodeId;
    public long personId;
    public String hiTypes;
    public String personName;
    public String enrollmentDate;
    public String identifierValue;
    public char gender;
    public String identifierType;
    public String abhaAddress;
    public String yearOfBirth;
    private String hipId;
}
