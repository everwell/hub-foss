package com.everwell.datagateway.models.request.abdm;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;
import java.util.UUID;

import static com.everwell.datagateway.constants.Constants.ABDM_TIMESTAMP_FORMAT;

@Getter
@Setter
public class ABDMAuthOnNotifyRequest {
    public String requestId;
    public String timestamp;
    public Acknowledgement acknowledgement;
    public Resp resp;

    public ABDMAuthOnNotifyRequest(String requestId) {
        DateTimeFormatter formatter =  DateTimeFormatter.ofPattern(ABDM_TIMESTAMP_FORMAT);
        LocalDateTime utcNow =  LocalDateTime.now(ZoneOffset.UTC);
        this.requestId = UUID.randomUUID().toString();
        this.timestamp = formatter.format(utcNow);
        this.acknowledgement = new Acknowledgement("OK");
        this.resp = new Resp(requestId);
    }

    @Getter
    @Setter
    @AllArgsConstructor
    public static class Acknowledgement{
        public String status;
    }

    @Getter
    @Setter
    @AllArgsConstructor
    public static class Resp{
        public String requestId;
    }
}
