package com.everwell.datagateway.models.request;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@Data
public class DeploymentCallBackRequest {
    String deploymentCode;
    String linkId;
    String text;
    String to;
    String id;
    String date;
    String from;
    String body;
}
