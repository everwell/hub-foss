package com.everwell.datagateway.models.request.abdm;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Data
public class ABDMConsentNotification {
    public String requestId;
    public String timestamp;
    public Notification notification;

    @Data
    public static class Notification{
        public String consentRequestId;
        public String status;
        public List<ConsentArtefact> consentArtefacts;
    }

    @Data
    public static class ConsentArtefact{
        public String id;
    }
}
