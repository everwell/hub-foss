package com.everwell.datagateway.models.dto;

import com.everwell.datagateway.exceptions.ValidationException;
import com.fasterxml.jackson.annotation.JsonTypeName;
import lombok.*;
import org.springframework.util.StringUtils;


@Data
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class BasicAuthCredDto extends ClientCredentialBase {
    String username;
    String password;

    public void validateBasicAuthCredDto(String username, String password) {
        if(StringUtils.isEmpty(username) || StringUtils.isEmpty(password)) {
            throw new ValidationException("Basic Auth require username and password");
        }
    }
}
