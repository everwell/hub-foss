package com.everwell.datagateway.models.response.ecbss;

import lombok.Data;

import java.util.ArrayList;
import java.util.Date;

@Data
public class TrackedEntityInstancesResponse {

    public ArrayList<TrackedEntityInstance> trackedEntityInstances;

    @Data
    public static class TrackedEntityInstance{
        public Date created;
        public String orgUnit;
        public Date createdAtClient;
        public String trackedEntityInstance;
        public Date lastUpdated;
        public String trackedEntityType;
        public Date lastUpdatedAtClient;
        public boolean potentialDuplicate;
        public boolean inactive;
        public boolean deleted;
        public String featureType;
        public ArrayList<Object> programOwners;
        public ArrayList<Object> enrollments;
        public ArrayList<Object> relationships;
        public ArrayList<Object> attributes;
    }
}
