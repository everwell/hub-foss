package com.everwell.datagateway.models.request;

import com.everwell.datagateway.enums.StatusCodeEnum;
import lombok.Data;

@Data
public class UpdateTestRequest implements DataGatewayRequest {

    private String patientId;

    @Override
    public StatusCodeEnum isValid(){
        return StatusCodeEnum.SUCCESS;
    }
}
