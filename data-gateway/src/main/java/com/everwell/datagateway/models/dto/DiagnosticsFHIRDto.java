package com.everwell.datagateway.models.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import lombok.*;

import java.util.Map;

@Data
@NoArgsConstructor
@AllArgsConstructor
@JsonNaming(PropertyNamingStrategy.UpperCamelCaseStrategy.class)
public class DiagnosticsFHIRDto {

    public int episodeId;

    public int hierarchyId;

    public int userId;

    public String hierarchyName;

    public Map<String,TestDetails> testResultMap;
    public String name;
    public String gender;
    public String dateOfBirth;

    @Data
    @NoArgsConstructor
    @AllArgsConstructor
    @JsonNaming(PropertyNamingStrategy.UpperCamelCaseStrategy.class)
    static
    public class TestDetails
    {
        public String testType;
        public String finalInterpretation;
        public boolean positive;
        public String requestDate;
        public String reasonForTesting;
    }
}
