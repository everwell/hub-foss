package com.everwell.datagateway.models.request.abdm;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ABDMSMSNotifyCareContextEvent {
    private String phoneNo;
    private long hierarchyId;
    public String hipId;
}
