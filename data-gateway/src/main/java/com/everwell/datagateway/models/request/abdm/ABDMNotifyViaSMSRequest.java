package com.everwell.datagateway.models.request.abdm;

import com.everwell.datagateway.constants.Constants;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;
import java.util.UUID;

@Getter
@Setter
@NoArgsConstructor
public class ABDMNotifyViaSMSRequest {
    public String requestId;
    public String timestamp;
    public ABDMNotifyViaSMSNotificationDto notification;

    public ABDMNotifyViaSMSRequest(String phoneNo,String name,String id)
    {
        DateTimeFormatter formatter =  DateTimeFormatter.ofPattern(Constants.ABDM_TIMESTAMP_FORMAT);
        LocalDateTime utcNow =  LocalDateTime.now(ZoneOffset.UTC);
        this.requestId = UUID.randomUUID().toString();
        this.timestamp = formatter.format(utcNow);
        this.notification = new ABDMNotifyViaSMSNotificationDto(phoneNo,name,id);
    }

    @Getter
    @Setter
    public static class ABDMNotifyViaSMSNotificationDto {
        public String phoneNo;
        public ABDMNotifyViaSMSHipDto hip;

        public ABDMNotifyViaSMSNotificationDto(String phoneNo,String name,String id)
        {
            this.phoneNo = phoneNo;
            this.hip = new ABDMNotifyViaSMSHipDto(name,id);
        }

        @Getter
        @Setter
        @AllArgsConstructor
        public static class ABDMNotifyViaSMSHipDto {
            public String name;
            public String id;
        }
    }
}
