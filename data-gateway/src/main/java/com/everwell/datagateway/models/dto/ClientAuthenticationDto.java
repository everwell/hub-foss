package com.everwell.datagateway.models.dto;

import com.everwell.datagateway.enums.AuthenticationTypeEnum;
import lombok.*;


@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@ToString
public class ClientAuthenticationDto {

    private String credentialType;

    private ClientCredentialBase credentials;

    private AuthenticationTypeEnum type;

    private Long typeId;

    public ClientAuthenticationDto(String credentialType, ClientCredentialBase credentials) {
      this.credentialType =  credentialType;
      this.credentials = credentials;
    }

}
