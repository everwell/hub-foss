package com.everwell.datagateway.models.request;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ChildrenLabRequest {
    private Long hierarchyId;
    private String extraDataField;
    private List<String> type;
    private int pageNo = 0;
    private int pageSize = 200;

    public ChildrenLabRequest(Long hierarchyId, String extraDataField, List<String> type, int pageNo) {
        this.hierarchyId = hierarchyId;
        this.extraDataField = extraDataField;
        this.type = type;
        this.pageNo = pageNo;
    }
}
