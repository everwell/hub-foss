package com.everwell.datagateway.models.request.abdm;

import lombok.Getter;

import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;
import java.util.UUID;

import static com.everwell.datagateway.constants.Constants.ABDM_TIMESTAMP_FORMAT;

@Getter
public class ABDMLinkCareContextRequest {
    public String requestId;
    public String timestamp;
    public Link link;

    public ABDMLinkCareContextRequest (String token, String personID, String episodeId, String name, String enrollmentDate)
    {
        DateTimeFormatter formatter =  DateTimeFormatter.ofPattern(ABDM_TIMESTAMP_FORMAT);
        LocalDateTime utcNow =  LocalDateTime.now(ZoneOffset.UTC);
        this.requestId = UUID.randomUUID().toString();
        this.timestamp = formatter.format(utcNow);
        this.link = new Link(token, personID, episodeId, name, enrollmentDate);

    }

    public static class Link {

        public String accessToken;
        public ABDMCareContextDTO patient;

        public Link(String token, String personID, String episodeId, String name, String enrollmentDate)
        {
            this.accessToken = token;
            this.patient = new ABDMCareContextDTO(personID,episodeId,name,enrollmentDate);
        }
    }
}
