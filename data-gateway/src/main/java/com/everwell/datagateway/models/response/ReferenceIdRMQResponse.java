package com.everwell.datagateway.models.response;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@JsonIgnoreProperties(ignoreUnknown = true)
@Builder
@JsonDeserialize(builder = ReferenceIdRMQResponse.ReferenceIdRMQResponseBuilder.class)
public class ReferenceIdRMQResponse {
    @JsonProperty("RefId")
    private Long RefId;
    @JsonProperty("Response")
    private String Response;
}
