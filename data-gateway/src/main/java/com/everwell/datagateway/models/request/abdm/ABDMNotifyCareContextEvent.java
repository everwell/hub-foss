package com.everwell.datagateway.models.request.abdm;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.*;

import java.io.Serializable;

@AllArgsConstructor
@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class ABDMNotifyCareContextEvent implements Serializable {
    public long episodeId;
    public long personId;
    public String hiTypes;
    public String personName;
    public String abhaAddress;
    private String hipId;
}
