package com.everwell.datagateway.models.request.abdm;

import com.everwell.datagateway.models.response.abdm.Error;
import com.everwell.datagateway.models.response.abdm.Response;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;
import java.util.UUID;

import static com.everwell.datagateway.constants.Constants.ABDM_TIMESTAMP_FORMAT;

@Data
@NoArgsConstructor
@Getter
@Setter
public class ABDMOnLinkConfirmationRequest {

    private ABDMCareContextDTO patient;
    private String requestId;
    private String timestamp;
    private Error error;
    private Response resp;

    public ABDMOnLinkConfirmationRequest(ABDMCareContextDTO patient,String requestId) {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern(ABDM_TIMESTAMP_FORMAT);
        LocalDateTime utcNow = LocalDateTime.now(ZoneOffset.UTC);
        this.requestId = UUID.randomUUID().toString();
        this.timestamp = formatter.format(utcNow);
        this.resp = new Response(requestId);
        this.patient = patient;
    }

    public ABDMOnLinkConfirmationRequest(String requestId, String message) {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern(ABDM_TIMESTAMP_FORMAT);
        LocalDateTime utcNow = LocalDateTime.now(ZoneOffset.UTC);
        this.requestId = UUID.randomUUID().toString();
        this.timestamp = formatter.format(utcNow);
        this.resp = new Response(requestId);
        this.error = new Error(1000,message);
    }
}
