package com.everwell.datagateway.models.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class KeyMaterial {
    private String privateKey;
    private String publicKey;
    private String nonce;

    public KeyMaterial(String privateKey, String publicKey, String nonce) {
        this.privateKey = privateKey;
        this.publicKey = publicKey;
        this.nonce = nonce;
    }
}
