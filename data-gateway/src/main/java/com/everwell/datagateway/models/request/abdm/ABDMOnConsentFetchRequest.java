package com.everwell.datagateway.models.request.abdm;

import lombok.Data;

import java.util.List;

@Data
public class ABDMOnConsentFetchRequest {
    public String requestId;
    public String timestamp;
    public Consent consent;
    public Error error;
    public Resp resp;

    @Data
    public static class Consent{
        public String status;
        public ConsentDetail consentDetail;
        public String signature;
    }

    @Data
    public static class ConsentDetail{
        public String schemaVersion;
        public String consentId;
        public String createdAt;
        public Patient patient;
        public List<CareContext> careContexts;
        public Purpose purpose;
        public Hip hip;
        public Hiu hiu;
        public ConsentManager consentManager;
        public Requester requester;
        public List<String> hiTypes;
        public Permission permission;
    }

    @Data
    public static class CareContext{
        public String patientReference;
        public String careContextReference;
    }

    @Data
    public static class ConsentManager{
        public String id;
    }

    @Data
    public static class DateRange{
        public String from;
        public String to;
    }

    @Data
    public static class Error{
        public int code;
        public String message;
    }

    @Data
    public static class Frequency{
        public String unit;
        public int value;
        public int repeats;
    }

    @Data
    public static class Hip{
        public String id;
        public String name;
    }

    @Data
    public static class Hiu{
        public String id;
        public String name;
    }

    @Data
    public static class Identifier{
        public String type;
        public String value;
        public String system;
    }

    @Data
    public static class Patient{
        public String id;
    }

    @Data
    public static class Permission{
        public String accessMode;
        public DateRange dateRange;
        public String dataEraseAt;
        public Frequency frequency;
    }

    @Data
    public static class Purpose{
        public String text;
        public String code;
        public String refUri;
    }

    @Data
    public static class Requester{
        public String name;
        public Identifier identifier;
    }

    @Data
    public static class Resp{
        public String requestId;
    }
}
