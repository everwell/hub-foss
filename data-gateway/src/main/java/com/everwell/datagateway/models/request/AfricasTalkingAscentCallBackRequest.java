package com.everwell.datagateway.models.request;

import com.everwell.datagateway.constants.CallConstants;
import lombok.*;

import java.util.Map;
import java.util.stream.Collectors;

@NoArgsConstructor
@Getter
@Setter
public class AfricasTalkingAscentCallBackRequest extends DeploymentCallBackRequest {

    public AfricasTalkingAscentCallBackRequest(Map<String, String> request) {
        this.deploymentCode = request.get(CallConstants.DEPLOYMENT_CODE);
        this.linkId = request.get(CallConstants.LINK_ID);
        this.text = request.get(CallConstants.TEXT);
        this.to = request.get(CallConstants.TO);
        this.id = request.get(CallConstants.ID);
        this.date = request.get(CallConstants.DATE);
        this.from = request.get(CallConstants.FROM);
        this.body = request.entrySet().stream()
                .map(entry -> entry.getKey() + "=" + entry.getValue())
                .collect(Collectors.joining("&"));
    }
}
