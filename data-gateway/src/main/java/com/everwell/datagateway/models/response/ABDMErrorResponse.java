package com.everwell.datagateway.models.response;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class ABDMErrorResponse {
    MessageApiResponse error;

    public ABDMErrorResponse(MessageApiResponse error)
    {
        this.error = error;
    }
}
