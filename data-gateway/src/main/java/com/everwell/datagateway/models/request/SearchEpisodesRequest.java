package com.everwell.datagateway.models.request;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;
import java.util.Map;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class SearchEpisodesRequest {
    List<Long> hiearchyIdList;
    String from;
    String to;
    Boolean isRiskStatusOptional;
    List<Long> episodeStageIdList;
    String hierarchyType;
    List<Long> diseaseTemplateIds;
    Map<String, List<String>> stageData;
}
