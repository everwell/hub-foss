package com.everwell.datagateway.consumers;

import com.everwell.datagateway.entities.*;
import com.everwell.datagateway.exceptions.NotFoundException;
import com.everwell.datagateway.exceptions.RestTemplateException;
import com.everwell.datagateway.exceptions.URLDisabledException;
import com.everwell.datagateway.models.response.ABDMConsumerResponse;
import com.everwell.datagateway.repositories.ClientRepository;
import com.everwell.datagateway.repositories.ClientRequestsRepository;
import com.everwell.datagateway.repositories.EventRepository;
import com.everwell.datagateway.service.ABDMConsumerService;
import com.everwell.datagateway.service.ClientRequestsService;
import com.everwell.datagateway.utils.SentryUtils;
import com.everwell.datagateway.utils.Utils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Date;

import static com.everwell.datagateway.constants.QueueConstants.ABDM_ADD_UPDATE_CONSENT_QUEUE;

@Component
public class ABDMAddUpdateConsentConsumer implements Consumer, ConsumerWebhook {


    private static Logger LOGGER = LoggerFactory.getLogger(ABDMAddUpdateConsentConsumer.class);


    @Autowired
    private ABDMConsumerService abdmConsumerService;

    @Autowired
    private ClientRequestsService clientRequestsService;

    @Autowired
    private ClientRequestsRepository clientRequestsRepository;

    @Autowired
    private ClientRepository clientRepository;

    @Autowired
    private EventRepository eventRepository;


    @Override
    @RabbitListener(queues = ABDM_ADD_UPDATE_CONSENT_QUEUE, containerFactory = "rmqPrefetchCount20", concurrency = "3")
    public void consume(Message message) {
        LOGGER.info("Message from queue is " + new String(message.getBody()));
        outgoingWebhook(ABDM_ADD_UPDATE_CONSENT_QUEUE, message);
    }

    @Override
    public void outgoingWebhook(String consumer, Message messageRmq) {
        ClientRequests clientRequests = null;
        try {
            String message = new String(messageRmq.getBody());
            ABDMConsumerResponse abdmConsumerResponse = Utils.convertStrToObject(message, ABDMConsumerResponse.class);
            Long referenceId = abdmConsumerResponse.getRefId();
            String payload = abdmConsumerResponse.getResponse();
            clientRequests = clientRequestsService.updateClientRequestbasedonRefId(referenceId, payload);
            Client client = clientRepository.findById(clientRequests.getClientId()).orElse(null);
            Event event = eventRepository.findById(clientRequests.getEventId()).orElse(null);
            String clientName = client.getUsername();
            String eventName = event.getEventName();
            abdmConsumerService.publishToABDMSubscriberUrl(clientName, eventName, payload);
            clientRequestsService.setIsDeliveredTrueAndDeliveredAtTime(referenceId,new Date());
            LOGGER.info("ABDM Add Update Consent outgoingWebhook: Success");
        }
        catch (RestTemplateException e) {
            String responseData = e.getError();
            LOGGER.warn("RestTemplate Exception thrown :" + responseData);
            String subResponseData = responseData == null ? null : responseData.substring(0, Math.min(responseData.length(),255));
            clientRequests.setResponsePayload(subResponseData);
            clientRequestsRepository.save(clientRequests);
        } catch (NotFoundException e) {
            LOGGER.warn("NotFoundException Exception thrown :" + e.getError());
            SentryUtils.captureException(e);
        } catch (URLDisabledException e) {
            LOGGER.warn("URLDisabledException Exception thrown :" + e.getError());
            SentryUtils.captureException(e);
        } catch (Exception e) {
            LOGGER.warn("Exception thrown :" + e.getMessage());
            SentryUtils.captureException(e);
        }
    }
}
