package com.everwell.datagateway.consumers;

import org.springframework.amqp.core.Message;

public interface ConsumerWebhook {
    void outgoingWebhook(String queueName, Message message);
}
