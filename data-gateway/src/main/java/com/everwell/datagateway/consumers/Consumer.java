package com.everwell.datagateway.consumers;

import org.springframework.amqp.core.Message;

public interface Consumer {
    //A method to receive byte messages from RabbitMQ
    //Use  @RabbitListener(queueNames) to receive messages
    void consume(Message message);
}
