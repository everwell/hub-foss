package com.everwell.datagateway.publishers;

import com.everwell.datagateway.utils.Utils;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.core.MessagePostProcessor;
import org.springframework.amqp.core.MessageProperties;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Map;

@Component
public class RMQPublisher {

    private static Logger LOGGER = LoggerFactory.getLogger(RMQPublisher.class);

    @Autowired
    private RabbitTemplate rabbitTemplate;

    public void publish(Object obj, String exchange, String routingKey, String replyToRoutingKey, String correlationId) {
        MessagePostProcessor messagePostProcessor = message -> {
            MessageProperties messageProperties
                    = message.getMessageProperties();
            messageProperties.setReplyTo(replyToRoutingKey);
            messageProperties.setCorrelationId(correlationId);
            return message;
        };
        rabbitTemplate.convertAndSend(exchange, routingKey, Utils.asJsonString(obj), messagePostProcessor);//object is sent as it is, we are using java models to talk to RMQ in data-gateway
        LOGGER.debug("Message published successfully to exchange : " + exchange + ", routing key : " + routingKey + ", Contents : " + Utils.asJsonString(obj) + ", messagePostProcessorObj : " + Utils.asJsonString(messagePostProcessor));
    }

    public void send(String message, String routingKey, String exchange, String expirationTime, Map<String, Object> map) {
        if (!StringUtils.isEmpty(message)) {
            this.rabbitTemplate.convertAndSend(exchange, routingKey, message, m -> {
                map.forEach((k, v) -> m.getMessageProperties().setHeader(k, v));
                if (!StringUtils.isEmpty(expirationTime))
                    m.getMessageProperties().setExpiration(expirationTime);
                return m;
            });
            LOGGER.debug("[publish] eventName: " + routingKey + " message: " + message);
        }
    }

    public void send(Object obj, String routingKey, String exchange, Map<String,Object> headers) {
        if(null != obj) {
            this.rabbitTemplate.convertAndSend(exchange, routingKey, Utils.asJsonString(obj), m -> {
                headers.forEach((k, v) -> m.getMessageProperties().setHeader(k, v));
                return m;
            });
        }
        LOGGER.info("[publish] eventName: " + routingKey + " message: " + Utils.asJsonString(obj));
    }

}
