package com.everwell.datagateway.controllers;

import com.everwell.datagateway.constants.Constants;
import com.everwell.datagateway.enums.EventEnum;
import com.everwell.datagateway.enums.StatusCodeEnum;
import com.everwell.datagateway.models.request.SmsDetailedRequest;
import com.everwell.datagateway.models.response.MessageApiResponse;
import com.everwell.datagateway.service.PublisherService;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.Authorization;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;
import java.util.Objects;

@RestController
public class SMSController {
    private static Logger LOGGER = LoggerFactory.getLogger(SMSController.class);

    @Autowired
    private PublisherService publisherService;

    @ApiOperation(value = "Send BULK SMS", authorizations = {@Authorization(value = "jwtToken")})
    @PostMapping("/sms/bulk")
    public ResponseEntity sendBulkSms(@RequestBody SmsDetailedRequest sendBulkSMSRequest) {
        LOGGER.debug("***Send Bulk Sms Request received***");
        sendBulkSMSRequest.validate(sendBulkSMSRequest);
        Map<String, String> publisherResponse = publisherService.publishRequestForINS(sendBulkSMSRequest, EventEnum.SEND_BULK_SMS.getEventName());
        Map.Entry<String, String> responseMap = publisherResponse.entrySet().iterator().next();
        if (Objects.equals(responseMap.getKey(), Constants.ERROR)) {
            return new ResponseEntity<>(new MessageApiResponse<>(responseMap.getValue()), StatusCodeEnum.INTERNAL_SERVER_ERROR.getCode());
        }
        return new ResponseEntity<>(new MessageApiResponse<>(StatusCodeEnum.SUCCESS.getMessage()),HttpStatus.ACCEPTED);
    }
}
