package com.everwell.datagateway.controllers;

import com.everwell.datagateway.enums.EventEnum;
import com.everwell.datagateway.models.response.PublisherResponse;
import com.everwell.datagateway.models.response.Response;
import com.everwell.datagateway.service.PublisherService;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.Authorization;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class MermController {

    @Autowired
    private PublisherService publisherService;

    @ApiOperation(value = "Add IMEI Patient Mapping", authorizations = {@Authorization(value = "jwtToken")})
    @RequestMapping(value = "/v1/imei/entity", method = RequestMethod.POST)
    public ResponseEntity<Response<PublisherResponse>> registerEntity(@RequestBody String addMermPatientMapping) {
        PublisherResponse publisherResponse = publisherService.publishRequest(addMermPatientMapping, EventEnum.ADD_MERM_ENTITY_MAPPING.getEventName());
        return new ResponseEntity<>(new Response<>(true, publisherResponse), HttpStatus.OK);
    }

    @ApiOperation(value = "Update IMEI Patient Mapping", authorizations = {@Authorization(value = "jwtToken")})
    @RequestMapping(value = "/v1/imei/entity", method = RequestMethod.PUT)
    public ResponseEntity<Response<PublisherResponse>> updateMerm(@RequestBody String updateMermPatientMapping) {
        PublisherResponse publisherResponse = publisherService.publishRequest(updateMermPatientMapping, EventEnum.UPDATE_MERM_ENTITY_MAPPING.getEventName());
        return new ResponseEntity<>(new Response<>(true, publisherResponse), HttpStatus.OK);
    }

    @ApiOperation(value = "Deallocate IMEI Patient Mapping with valid status", authorizations = {@Authorization(value = "jwtToken")})
    @RequestMapping(value = "/v1/imei/entity/deallocate", method = RequestMethod.POST)
    public ResponseEntity<Response<PublisherResponse>> deallocateMerm(@RequestBody String deallocateImeiRequest) {
        PublisherResponse publisherResponse = publisherService.publishRequest(deallocateImeiRequest, EventEnum.DEALLOCATE_MERM_ENTITY.getEventName());
        return new ResponseEntity<>(new Response<>(true, publisherResponse), HttpStatus.OK);
    }
}
