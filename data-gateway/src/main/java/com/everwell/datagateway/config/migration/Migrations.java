package com.everwell.datagateway.config.migration;

import org.flywaydb.core.Flyway;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;

@ConditionalOnProperty(prefix = "spring", name = "flyway.enabled", havingValue = "true")
@Service
public class Migrations {

    @Autowired
    private Flyway flyway;

    @PostConstruct
    public void runMigrations() {
        flyway.migrate();
    }
}
