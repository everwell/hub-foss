package com.everwell.datagateway.config

import com.everwell.datagateway.filters.CphcZuulRouteFilter
import com.everwell.datagateway.filters.LalPathLabsZuulRouteFilter
import com.everwell.datagateway.filters.zuul.callLogs.CallLogsZuulRouteFilter
import com.everwell.datagateway.filters.zuul.dbtservice.DBTServiceZuulRouteFilter
import com.everwell.datagateway.filters.zuul.interraCallLogs.InterraCallLogsZuulRouteFilter
import com.everwell.datagateway.filters.zuul.dell.DellZuulRouteFilter
import com.everwell.datagateway.filters.zuul.diagnostics.DiagnosticsZuulRouteFilter
import com.everwell.datagateway.filters.zuul.dispensation.DispensationZuulRouteFilter
import com.everwell.datagateway.filters.zuul.dure.DureZuulRouteFilter
import com.everwell.datagateway.filters.zuul.egs.EgsZuulRouteFilter
import com.everwell.datagateway.filters.zuul.episode.EpisodeZuulRouteFilter
import com.everwell.datagateway.filters.zuul.globalLabsCallLogs.GlobeLabsCallLogsZuulRouteFilter
import com.everwell.datagateway.filters.zuul.gmri.GmriZuulRouteFilter
import com.everwell.datagateway.filters.zuul.hub.HubZuulRouteFilter
import com.everwell.datagateway.filters.zuul.hubReports.HubReportsZuulRouteFilter
import com.everwell.datagateway.filters.zuul.iam.IamZuulRouteFilter
import com.everwell.datagateway.filters.zuul.ins.InsZuulRouteFilter
import com.everwell.datagateway.filters.zuul.lims.LimsZuulRouteFilter
import com.everwell.datagateway.filters.zuul.lpa.LpaZuulRouteFilter
import com.everwell.datagateway.filters.zuul.merm.MermZuulRouteFilter
import com.everwell.datagateway.filters.zuul.mermConfig.MermConfigZuulRouteFilter
import com.everwell.datagateway.filters.zuul.nikshayMasterApi.NikshayMasterApiZuulRouteFilter
import com.everwell.datagateway.filters.zuul.nikshay.NikshayZuulRouteFilter
import com.everwell.datagateway.filters.zuul.kpi.KpiZuulRouteFilter
import com.everwell.datagateway.filters.zuul.registry.RegistryZuulRouteFilter
import com.everwell.datagateway.filters.zuul.smartPayments.SmartPaymentsZuulRouteFilter
import com.everwell.datagateway.filters.zuul.soch.SochZuulRouteFilter
import com.everwell.datagateway.filters.zuul.sso.SsoZuulRouteFilter
import com.everwell.datagateway.filters.zuul.umang.UmangZuulRouteFilter
import com.everwell.datagateway.filters.zuul.userService.UserServiceZuulRouteFilter
import com.everwell.datagateway.filters.zuul.vot.VotZuulRouteFilter
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration


@Configuration
open class ZuulRouteFilterConfig {
    @Bean
    open fun umangZuulRouteFilter() : UmangZuulRouteFilter {
        return UmangZuulRouteFilter()
    }

    @Bean
    open fun limsZuulRouteFilter() : LimsZuulRouteFilter {
        return LimsZuulRouteFilter()
    }

    @Bean
    open fun sochZuulRouteFilter() : SochZuulRouteFilter {
        return SochZuulRouteFilter()
    }

    @Bean
    open fun dellZuulRouteFilter() : DellZuulRouteFilter {
        return DellZuulRouteFilter()
    }

    @Bean
    open fun dureZuulRouteFilter() : DureZuulRouteFilter {
        return DureZuulRouteFilter()
    }
    @Bean
    open fun genericZuulRouteFilter() : NikshayZuulRouteFilter {
        return NikshayZuulRouteFilter()
    }

    @Bean
    open fun hubZuulRouteFilter() : HubZuulRouteFilter {
        return HubZuulRouteFilter()
    }    

    @Bean
    open fun lalPathLabsZuulRouteFilter() : LalPathLabsZuulRouteFilter {
        return LalPathLabsZuulRouteFilter()
    }

    @Bean
    open fun cphcZuulRouteFilter() : CphcZuulRouteFilter {
        return CphcZuulRouteFilter()
    }

    @Bean
    open fun iamZuulRouteFilter() : IamZuulRouteFilter {
        return IamZuulRouteFilter()
    }

    @Bean
    open fun registryZuulRouteFilter() : RegistryZuulRouteFilter {
        return RegistryZuulRouteFilter()
    }

    @Bean
    open fun dispensationZuulRouteFilter() : DispensationZuulRouteFilter {
        return DispensationZuulRouteFilter()
    }

    @Bean
    open fun insZuulRouteFilter() : InsZuulRouteFilter {
        return InsZuulRouteFilter()
    }

    @Bean
    open fun diagnosticsZuulRouteFilter() : DiagnosticsZuulRouteFilter {
        return DiagnosticsZuulRouteFilter()
    }

    @Bean
    open fun userServiceZuulRouteFilter() : UserServiceZuulRouteFilter {
        return UserServiceZuulRouteFilter()
    }

    @Bean
    open fun ssoZuulRouteFilter() : SsoZuulRouteFilter {
        return SsoZuulRouteFilter()
    }

    @Bean
    open fun gmriZuulRouteFilter() : GmriZuulRouteFilter {
        return GmriZuulRouteFilter()
    }

    @Bean
    open fun mermZuulRouteFilter() : MermZuulRouteFilter {
        return MermZuulRouteFilter()
    }

    @Bean
    open fun mermConfigZuulRouteFilter() : MermConfigZuulRouteFilter {
        return MermConfigZuulRouteFilter()
    }

    @Bean
    open fun smartPaymentsZuulRouteFilter() : SmartPaymentsZuulRouteFilter {
        return SmartPaymentsZuulRouteFilter()
    }
    
    @Bean
    open fun hubReportsZuulRouteFilter() : HubReportsZuulRouteFilter {
        return HubReportsZuulRouteFilter()
    }

    @Bean
    open fun callLogsZuulRouteFilter() : CallLogsZuulRouteFilter {
        return CallLogsZuulRouteFilter()
    }

    @Bean
    open fun lpaZuulRouteFilter() : LpaZuulRouteFilter {
        return LpaZuulRouteFilter()
    }

    @Bean
    open fun masterApiZuulRouteFilter() : NikshayMasterApiZuulRouteFilter {
        return NikshayMasterApiZuulRouteFilter()
    }

    @Bean
    open fun episodeZuulRouteFilter() : EpisodeZuulRouteFilter {
        return EpisodeZuulRouteFilter()
    }

    @Bean
    open fun kpiZuulRouteFilter() : KpiZuulRouteFilter {
        return KpiZuulRouteFilter()
    }

    @Bean
    open fun callLogsInterraZuulRouteFilter() : InterraCallLogsZuulRouteFilter {
        return InterraCallLogsZuulRouteFilter()
    }

    @Bean
    open fun egsZuulRouteFilter() : EgsZuulRouteFilter {
        return EgsZuulRouteFilter()
    }

    @Bean
    open fun votZuulRouteFilter() : VotZuulRouteFilter {
        return VotZuulRouteFilter()
    }

    @Bean
    open fun dbtServiceZuulRouteFilter() : DBTServiceZuulRouteFilter {
        return DBTServiceZuulRouteFilter();
    }

    @Bean
    open fun callLogsGlobeLabsZuulRouteFilter() : GlobeLabsCallLogsZuulRouteFilter {
        return GlobeLabsCallLogsZuulRouteFilter()
    }
}