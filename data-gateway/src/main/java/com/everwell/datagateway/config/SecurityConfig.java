package com.everwell.datagateway.config;


import com.everwell.datagateway.enums.LegacyRoutesEnum;
import com.everwell.datagateway.filters.JWTAuthorizationFilter;
import com.everwell.datagateway.service.UserDetailsServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.http.HttpMethod;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.CorsConfigurationSource;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static com.everwell.datagateway.constants.SecurityConstants.*;

@EnableWebSecurity
public class SecurityConfig extends WebSecurityConfigurerAdapter {


    @Autowired
    private UserDetailsServiceImpl userDetailsService;
    @Autowired
    private BCryptPasswordEncoder bCryptPasswordEncoder;

    @Bean
    public BCryptPasswordEncoder bCryptPasswordEncoder() {
        return new BCryptPasswordEncoder();
    }

    @Bean
    @Override
    public AuthenticationManager authenticationManagerBean() throws Exception {
        return super.authenticationManagerBean();
    }

    private String[] getCombinedPaths(String... additionalPaths) {
        List<String> paths = new ArrayList<>();
        paths.addAll(Arrays.asList(additionalPaths));
        paths.addAll(Arrays.asList(LegacyRoutesEnum.getAllPaths()));
        return paths.toArray(new String[0]);
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        String[] allPaths = LegacyRoutesEnum.getAllPaths();
        http.cors().and().csrf().disable().authorizeRequests()
                .antMatchers(HttpMethod.POST, getCombinedPaths(SIGN_UP_URL, GENERATE_TOKEN_URL,HI_DATA_TRANSFER)).permitAll()
                .antMatchers(HttpMethod.GET,TEST_CONNECTION_URL, PROCESS_CALL_URL, LegacyRoutesEnum.GET_MERM_CONFIG.getPath(), "/test-umang","/").permitAll()
                .antMatchers(HttpMethod.POST,WIX_POST_CREATION_EVENT).permitAll()
                .antMatchers(AUTH_WHITELIST).permitAll()
                .antMatchers(ACTUATOR_URL).permitAll()
                .antMatchers(ACTUATOR_PROMETHEUS).permitAll()
                .anyRequest().authenticated()
                .and()
                .addFilter(new JWTAuthorizationFilter(authenticationManager()))
                // this disables session creation on Spring Security
                .sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS);
    }

    @Override
    public void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.userDetailsService(userDetailsService).passwordEncoder(bCryptPasswordEncoder);
    }

    @Bean
    CorsConfigurationSource corsConfigurationSource() {
        final UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource();
        source.registerCorsConfiguration("/**", new CorsConfiguration().applyPermitDefaultValues());
        return source;
    }


}



