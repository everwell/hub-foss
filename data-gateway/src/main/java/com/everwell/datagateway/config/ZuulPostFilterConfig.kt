package com.everwell.datagateway.config

import com.everwell.datagateway.filters.CphcZuulPostFilter
import com.everwell.datagateway.filters.LalPathLabsZuulPostFilter
import com.everwell.datagateway.filters.zuul.RequestLogPostFilter
import com.everwell.datagateway.filters.zuul.dell.DellZuulPostFilter
import com.everwell.datagateway.filters.zuul.dure.DureZuulPostFilter
import com.everwell.datagateway.filters.zuul.egs.EgsZuulPostFilter
import com.everwell.datagateway.filters.zuul.hub.HubZuulPostFilter
import com.everwell.datagateway.filters.zuul.lims.LimsZuulPostFilter
import com.everwell.datagateway.filters.zuul.registry.RegistryZuulPostFilter
import com.everwell.datagateway.filters.zuul.soch.SochZuulPostFilter
import com.everwell.datagateway.filters.zuul.umang.UmangZuulPostFilter
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration

@Configuration
open class ZuulPostFilterConfig {
    @Bean
    open fun umangZuulPostFilter() : UmangZuulPostFilter {
        return UmangZuulPostFilter()
    }

    @Bean
    open fun limsZuulPostFilter() : LimsZuulPostFilter {
        return LimsZuulPostFilter()
    }

    @Bean
    open fun sochZuulPostFilter() : SochZuulPostFilter {
        return SochZuulPostFilter()
    }

    @Bean
    open fun dellZuulPostFilter() : DellZuulPostFilter {
        return DellZuulPostFilter()
    }

    @Bean
    open fun dureZuulPostFilter() : DureZuulPostFilter {
        return DureZuulPostFilter()
    }

    @Bean
    open fun hubZuulPostFilter() : HubZuulPostFilter {
        return HubZuulPostFilter()
    }   

    @Bean
    open fun lalPathLabsZuulPostFilter() : LalPathLabsZuulPostFilter {
        return LalPathLabsZuulPostFilter()
    }

    @Bean
    open fun cphcZuulPostFilter() : CphcZuulPostFilter {
        return CphcZuulPostFilter()
    }

    @Bean
    open fun registryZuulPostFilter() : RegistryZuulPostFilter {
        return RegistryZuulPostFilter()
    }

    @Bean
    open fun requestLogFilter() : RequestLogPostFilter {
        return RequestLogPostFilter()
    }

    @Bean
    open fun egsZuulPostFilter() : EgsZuulPostFilter {
        return EgsZuulPostFilter()
    }
}