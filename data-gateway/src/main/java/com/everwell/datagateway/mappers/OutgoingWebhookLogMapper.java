package com.everwell.datagateway.mappers;

import com.everwell.datagateway.entities.OutgoingWebhookLog;
import com.everwell.datagateway.models.dto.OutgoingWebhookLogDTO;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

@Mapper
public interface OutgoingWebhookLogMapper {

    OutgoingWebhookLogMapper INSTANCE = Mappers.getMapper(OutgoingWebhookLogMapper.class);

    OutgoingWebhookLog outgoingWebhookLogDTOToOutgoingWebhookLog(OutgoingWebhookLogDTO outgoingWebhookLogDTO);
}
