package com.everwell.datagateway.exceptions;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class RestTemplateException extends RuntimeException {

    private Integer statusCode;
    private String error;
    private String url;
    private String request;

    public RestTemplateException(Integer statusCode, String error) {
        this.statusCode = statusCode;
        this.error = error;
    }

    public RestTemplateException(Integer statusCode, String error, String url, String request) {
        this(statusCode, error);
        this.url = url;
        this.request = request;
    }
}