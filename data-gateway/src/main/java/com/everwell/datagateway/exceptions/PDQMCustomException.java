package com.everwell.datagateway.exceptions;

import lombok.Data;

@Data
public class PDQMCustomException extends Exception{
    private String errorCode;
    private String requestParam;
    private String modifier;
    private String paramValue;

    public PDQMCustomException(String message, String requestParam, String modifier, String paramValue) {
        super(message);
        this.requestParam = requestParam;
        this.modifier = modifier;
        this.paramValue = paramValue;
    }
}
