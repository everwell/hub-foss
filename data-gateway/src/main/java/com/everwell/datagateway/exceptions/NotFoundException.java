package com.everwell.datagateway.exceptions;

import com.everwell.datagateway.enums.StatusCodeEnum;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class NotFoundException extends RuntimeException {
    private Integer statusCode;
    private String error;

    public NotFoundException(Integer statusCode, String error) {
        this.statusCode = statusCode;
        this.error = error;
    }

    public NotFoundException(StatusCodeEnum status) {
        super(status.getMessage());
        this.statusCode = status.getCode().value();
    }
}
