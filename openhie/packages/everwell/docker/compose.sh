DOCKER_COMPOSE_PATH=$PATH
composeFilePath=$( cd "$(dirname "${BASH_SOURCE[0]}")" ; pwd -P )

get_os () {
    if [ "$(uname)" == "Darwin" ]; then
        echo "darwin"
    elif [ "$(expr substr $(uname -s) 1 5)" == "Linux" ]; then
        echo "linux"
    else
        # default
        echo 'unsupported os, falling back to default os "linux"' > /dev/stderr
        echo "linux"
    fi
}

get_architecture() {
    arch 2>/dev/null
    if [ $? -ne 0 ]; then
        # default architecture
        echo "failed to get system architecture failling back to default x86_64 architecture" > /dev/stderr
        echo "x86_64"
    fi
}

check_compose_version() {
    installed_version=$(docker-compose --version | egrep -o "[0-9]+\.[0-9]+\.[0-9]+")
    # minimum 2.1.0 version of docker-compose is required
    required_version="2.1.0"
    min_of_installed_and_required=$(printf "${installed_version}\n${required_version}" | sort -V | head -n 1)
    if [ "${min_of_installed_and_required}" == "${installed_version}" ]; then
        echo "[Warning] installed docker compose versoin \"${installed_version}\" is not supported."
        echo "[Warning] downloading newer docker compose version..."
        curl --help > /dev/null || (apt update -qq && apt install -y curl -qq)
        curl "https://github.com/docker/compose/releases/download/v2.15.1/docker-compose-`get_os`-`get_architecture`" --fail --location --output /tmp/docker-compose --silent
        if [ $? -ne 0 ]; then
            echo "[Error] docker-compose binary download failed"
            exit 1
        fi
        chmod +x /tmp/docker-compose
        DOCKER_COMPOSE_PATH=/tmp/
    else
        echo "installed docker comose version is compatible"
    fi
}

init_default_values() {
    export DEFAULT_USERNAME=${DATAGATEWAY_USERNAME:-INTERNAL_CLIENT}
    export DEFAULT_PASSWORD=${DATAGATEWAY_PASSWORD:-WMfP5YbwCZcxJY@Mqy-r}
    export NAMESPACE=${NAMESPACE:-everwell}
    export POSTGRESQL_APPLICATION_USER=${POSTGRESQL_APPLICATION_USER:-everwell}
    export POSTGRESQL_APPLICATION_USER_PASSWORD=${POSTGRESQL_APPLICATION_USER_PASSWORD:-TX_UFEBptLH8P@yDBTJu}
    export POSTGRESQL_PASSWORD=${POSTGRESQL_PASSWORD:-BCxrxSRcSLxV63ntsby4}
    export RABBITMQ_USERNAME=${RABBITMQ_USERNAME:-user}
    export RABBITMQ_PASSWORD=${RABBITMQ_PASSWORD:-NTZSxK37vWPTEGa-7HVt}
    export REDIS_PASSWORD=${REDIS_PASSWORD:-bCuSNGAnYBvjXeSeTZ4y}
    export JWT_SECRET=${JWT_SECRET:-tDYp44Fe9UwmMbkI5J9x}
    export SENTRY_VERSION=${SENTRY_VERSION:-0.1.0}
    export ELASTIC_USERNAME=${ELASTIC_USERNAME:-elastic}
    export ELASTIC_PASSWORD=${ELASTIC_PASSWORD:-EedS_8jId8TaNpMrnmwV}
    export DATAGATEWAY_USERNAME=${DATAGATEWAY_USERNAME:-${DEFAULT_USERNAME}}
    export DATAGATEWAY_PASSWORD=${DATAGATEWAY_PASSWORD:-${DEFAULT_PASSWORD}}
    export EPISODE_USERNAME=${EPISODE_USERNAME:-${DEFAULT_USERNAME}}
    export EPISODE_PASSWORD=${EPISODE_PASSWORD:-${DEFAULT_PASSWORD}}
    export EPISODE_CLIENT_ID=${EPISODE_CLIENT_ID:-1}
    export INS_USERNAME=${INS_USERNAME:-${DEFAULT_USERNAME}}
    export INS_PASSWORD=${INS_PASSWORD:-${DEFAULT_PASSWORD}}
    export IAM_USERNAME=${IAM_USERNAME:-${DEFAULT_USERNAME}}
    export IAM_PASSWORD=${IAM_PASSWORD:-${DEFAULT_PASSWORD}}
}

init_config_volumes() {
    # config volume creation, can not use normal volume mount due to use of dind
    docker volume create --name ${NAMESPACE}_postgres_config || true
    docker run -d --rm --name ${NAMESPACE}_postgres_config -v ${NAMESPACE}_postgres_config:/config alpine:3 sleep infinity
    docker cp ${composeFilePath}/postgres/initdb.sql ${NAMESPACE}_postgres_config:/config/initdb.sql
    docker stop ${NAMESPACE}_postgres_config
    echo "postgres config volume created"
    docker volume create --name ${NAMESPACE}_episode_scripts || true
    docker run -d --rm --name ${NAMESPACE}_episode_scripts -v ${NAMESPACE}_episode_scripts:/config alpine:3 sleep infinity
    docker cp ${composeFilePath}/episode-service/register-clients.sh ${NAMESPACE}_episode_scripts:/config/register-clients.sh
    docker stop ${NAMESPACE}_episode_scripts
    docker volume create --name ${NAMESPACE}_elastic_scripts || true
    docker run -d --rm --name ${NAMESPACE}_elastic_scripts -v ${NAMESPACE}_elastic_scripts:/config alpine:3 sleep infinity
    docker cp ${composeFilePath}/elastic-search/change-mappings.sh ${NAMESPACE}_elastic_scripts:/config/change-mappings.sh
    docker stop ${NAMESPACE}_elastic_scripts
    echo "application config volume created"
}

check_compose_version
init_default_values

exitCode=0
if [ "$1" == "init" ]; then
    init_config_volumes
    PATH=$DOCKER_COMPOSE_PATH docker-compose -p ${NAMESPACE} -f "$composeFilePath"/docker-compose.yml -f "$composeFilePath"/elastic-search/docker-compose.yml up -d
    exitCode=$?
elif [ "$1" == "up" ]; then
    init_config_volumes
    PATH=$DOCKER_COMPOSE_PATH docker-compose -p ${NAMESPACE} -f "$composeFilePath"/docker-compose.yml -f "$composeFilePath"/elastic-search/docker-compose.yml up -d
    exitCode=$?
elif [ "$1" == "down" ]; then
    PATH=$DOCKER_COMPOSE_PATH docker-compose -p ${NAMESPACE} -f "$composeFilePath"/docker-compose.yml -f "$composeFilePath"/elastic-search/docker-compose.yml stop
    exitCode=$?
elif [ "$1" == "destroy" ]; then
    PATH=$DOCKER_COMPOSE_PATH docker-compose -p ${NAMESPACE} -f "$composeFilePath"/docker-compose.yml -f "$composeFilePath"/elastic-search/docker-compose.yml down
    exitCode=$?
    docker volume rm ${NAMESPACE}_redis-data ${NAMESPACE}_postgres-data ${NAMESPACE}_rabbitmq-data
    docker volume rm ${NAMESPACE}_postgres_config
    docker volume rm ${NAMESPACE}_episode_scripts
    # if elastic search enabled 
    docker volume rm ${NAMESPACE}_certs ${NAMESPACE}_esdata
else
    echo "Valid options are: init, up, down, or destroy"
    exitCode=1
fi

exit $exitCode
